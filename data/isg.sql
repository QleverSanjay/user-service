CREATE DATABASE IF NOT EXISTS isg;
USE isg;

DROP TABLE IF EXISTS `state`;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `district`;
CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_STATE_ID_idx` (`state_id`),
  CONSTRAINT `FK_STATE_ID` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `taluka`;
CREATE TABLE `taluka` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 NOT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `district_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DISTRICT_idx` (`district_id`),
  CONSTRAINT `FK_DISTRICT` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` varchar(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `taluka_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_taluka1_idx` (`taluka_id`),
  CONSTRAINT `fk_city_taluka1` FOREIGN KEY (`taluka_id`) REFERENCES `taluka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS `religion`;
CREATE TABLE `religion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `branch`;
CREATE TABLE `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address_line` varchar(255) DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pincode` varchar(6) CHARACTER SET latin1 NOT NULL,
  `contactNumber` varchar(14) CHARACTER SET latin1 NOT NULL,
  `contactEmail` varchar(64) DEFAULT NULL,
  `websiteUrl` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `institute_id` int(11) NOT NULL,
  `taluka_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `fk_branches_institute1_idx` (`institute_id`),
  KEY `fk_branches_taluka1_idx` (`taluka_id`),
  KEY `fk_branches_district1_idx` (`district_id`),
  KEY `fk_branch_state1_idx` (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `caste`;
CREATE TABLE `caste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) DEFAULT NULL,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  `role_id` int(10) unsigned NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `FK_user_roll1` (`role_id`) USING BTREE,
  CONSTRAINT `FK_user_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_courses_branches1_idx` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;





DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) CHARACTER SET latin1 NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `venue` varchar(100) CHARACTER SET latin1 NOT NULL,
  `startdate` date NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `alldayevent` char(1) DEFAULT 'N',
  `reminderSet` char(1) NOT NULL DEFAULT 'N',
  `image_url` varchar(255) DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `reccuring` char(1) DEFAULT 'N',
  `reminderbefore` int(10) DEFAULT NULL COMMENT 'reminder before will be stored in minutes basis',
  `board` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `year` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `semester` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `standard` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `division` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `summary` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `forparent` varchar(1) DEFAULT 'N',
  `forstudent` varchar(1) DEFAULT 'N',
  `forteacher` varchar(1) DEFAULT 'N',
  `course_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_branch1_idx` (`branch_id`),
  KEY `fk_event_course_idx` (`course_id`),
  CONSTRAINT `fk_event_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `fees`;
CREATE TABLE `fees` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `head_id` int(10) NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `amount` varchar(45) CHARACTER SET latin1 NOT NULL,
  `duedate` date NOT NULL,
  `fromdate` date NOT NULL,
  `todate` date NOT NULL,
  `standard` varchar(45) NOT NULL,
  `caste_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fees_branch_idx` (`branch_id`),
  KEY `fk_fees_caste_idx` (`caste_id`),
  CONSTRAINT `fk_fees_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fees_caste` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) CHARACTER SET latin1 NOT NULL,
  `description` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `chat_dialogue_id` varchar(45) DEFAULT NULL,
  `standard` int(11) DEFAULT NULL,
  `division` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_group_branch1_idx` (`branch_id`),
  CONSTRAINT `fk_group_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `group_event`;
CREATE TABLE `group_event` (
  `group_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`event_id`),
  KEY `fk_event_group_group1_idx` (`group_id`),
  KEY `fk_event_group_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_group_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `group_tags`;
CREATE TABLE `group_tags` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `tag` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_grouptags_goup_id_idx` (`group_id`),
  CONSTRAINT `FK_grouptags_goup_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `guardian_type`;
CREATE TABLE `guardian_type` (
  `guardianTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `guardianTypeName` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`guardianTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `head`;
CREATE TABLE `head` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `institute`;
CREATE TABLE `institute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `institute_branch`;
CREATE TABLE `institute_branch` (
  `instituteBranchId` int(11) NOT NULL AUTO_INCREMENT,
  `branchId` int(11) NOT NULL,
  `instituteId` int(11) NOT NULL,
  `active` char(1) DEFAULT NULL,
  PRIMARY KEY (`instituteBranchId`),
  KEY `fk_instituebranch_branch_idx` (`branchId`),
  KEY `fk_instituebranch_institute_idx` (`instituteId`),
  CONSTRAINT `fk_instituebranch_branch` FOREIGN KEY (`branchId`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instituebranch_institute` FOREIGN KEY (`instituteId`) REFERENCES `institute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `priority` varchar(45) NOT NULL COMMENT 'High and Low',
  `date` datetime NOT NULL,
  `image_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `standard` int(11) DEFAULT NULL,
  `detail_description` text,
  PRIMARY KEY (`id`),
  KEY `fk_notification_branch1_idx` (`branch_id`),
  CONSTRAINT `fk_notice_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `notice_group`;
CREATE TABLE `notice_group` (
  `notice_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`notice_id`,`group_id`),
  KEY `fk_noticegroup_notice_idx` (`notice_id`),
  KEY `fk_noticegroup_group_idx` (`group_id`),
  CONSTRAINT `fk_noticegroup_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_noticegroup_notice` FOREIGN KEY (`notice_id`) REFERENCES `notice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `notice_individual`;
CREATE TABLE `notice_individual` (
  `notice_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`notice_id`,`user_id`),
  KEY `fk_noticeindividual_noticeId_idx` (`notice_id`),
  KEY `fk_noticeindividual_userid_idx` (`user_id`),
  CONSTRAINT `fk_noticeindividual_noticeId` FOREIGN KEY (`notice_id`) REFERENCES `notice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_noticeindividual_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `parent`;
CREATE TABLE `parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `middlename` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `primaryContact` varchar(10) NOT NULL,
  `secondaryContact` varchar(10) DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `pincode` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `taluka_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `active` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_parent_user1_idx` (`user_id`),
  KEY `fk_parent_taluka1_idx` (`taluka_id`),
  KEY `fk_parent_district1_idx` (`district_id`),
  KEY `fk_parent_state1_idx` (`state_id`),
  KEY `fk_parent_caste1_idx` (`caste_id`),
  KEY `fk_parent_religion1_idx` (`religion_id`),
  CONSTRAINT `fk_parent_caste1` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_religion1` FOREIGN KEY (`religion_id`) REFERENCES `religion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_state1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_taluka1` FOREIGN KEY (`taluka_id`) REFERENCES `taluka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `parent_otp`;
CREATE TABLE `parent_otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otp` varchar(6) NOT NULL,
  `generatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `primaryContact` varchar(25) NOT NULL,
  `active` varchar(1) DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `student_upload`;
CREATE TABLE `student_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `father_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mother_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dob` date NOT NULL,
  `relation_with_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `mobile` varchar(10) CHARACTER SET latin1 NOT NULL,
  `registration_code` varchar(10) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(10) NOT NULL,
  `line1` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `pincode` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `religion` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `standard` int(11) NOT NULL,
  `standard_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `division` varchar(45) CHARACTER SET latin1 NOT NULL,
  `roll_number` varchar(45) CHARACTER SET latin1 NOT NULL,
  `goal_child` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `branch_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_studentupload_branchId_idx` (`branch_id`),
  KEY `fk_studentupload_userId_idx` (`user_id`),
  KEY `fk_studentupload_casteId_idx` (`caste_id`),
  CONSTRAINT `fk_studentupload_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentupload_casteId` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentupload_userId` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `father_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mother_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `dob` date NOT NULL,
  `relation_with_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `email_address` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `mobile` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `registration_code` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `line1` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `area` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `pincode` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `religion` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `standard` int(11) DEFAULT NULL,
  `standard_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `division` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `roll_number` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_child` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `goal_parent` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  `user_id` int(11) NOT NULL,
  `student_upload_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_student_branch_idx` (`branch_id`),
  KEY `fk_student_casteId_idx` (`caste_id`),
  KEY `FK_student_uploadstudent` (`student_upload_id`),
  CONSTRAINT `FK_student_uploadstudent` FOREIGN KEY (`student_upload_id`) REFERENCES `student_upload` (`id`),
  CONSTRAINT `fk_student_branch` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_casteId` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `parent_id` int(11) NOT NULL,
  `fees_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `paid_date` datetime NOT NULL,
  PRIMARY KEY (`parent_id`,`fees_id`,`student_id`),
  KEY `parent_id_idx` (`parent_id`),
  KEY `fees_id_fk_idx` (`student_id`),
  KEY `fk_idx` (`fees_id`),
  CONSTRAINT `fees_id_fk` FOREIGN KEY (`fees_id`) REFERENCES `fees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parent_id_fk` FOREIGN KEY (`parent_id`) REFERENCES `parent` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `student_id_fk` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;






DROP TABLE IF EXISTS `school_admin`;
CREATE TABLE `school_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) NOT NULL,
  `dateOfBirth` varchar(45) DEFAULT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `primaryContact` varchar(45) NOT NULL,
  `secondaryContact` varchar(45) DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `pinCode` varchar(45) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_schooladminuser_1_idx` (`user_id`),
  CONSTRAINT `FK_schooladminuser_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;










DROP TABLE IF EXISTS `student_event`;
CREATE TABLE `student_event` (
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  KEY `fk_event_student_event1_idx` (`event_id`),
  KEY `fk_event_user_event1_idx` (`user_id`),
  CONSTRAINT `fk_event_user_event1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_event_student_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `student_group`;
CREATE TABLE `student_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `joined` char(1) DEFAULT 'N' COMMENT 'if value set to ''N'' means group is suggested group.\nelse if value set to ''Y'' means group is joined by student.',
  `group_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_student_group_group1_idx` (`group_id`),
  KEY `fk_student_group_student1_idx` (`student_id`),
  CONSTRAINT `FK_student_group_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`),
  CONSTRAINT `fk_student_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;











DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `emailAddress` varchar(45) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(1) CHARACTER SET latin1 NOT NULL,
  `primaryContact` varchar(45) CHARACTER SET latin1 NOT NULL,
  `secondaryContact` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pinCode` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_teacher_1` (`user_id`),
  CONSTRAINT `FK_teacher_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





DROP TABLE IF EXISTS `user_chat_profile`;
CREATE TABLE `user_chat_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `chat_user_profile_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_user_profile_id_UNIQUE` (`chat_user_profile_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `vendor`;
CREATE TABLE `vendor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `emailAddress` varchar(45) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `primaryContact` varchar(10) CHARACTER SET latin1 NOT NULL,
  `secondaryContact` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `addressLine` varchar(45) CHARACTER SET latin1 NOT NULL,
  `area` varchar(45) CHARACTER SET latin1 NOT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pinCode` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` varchar(1) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_vendor_1` (`user_id`),
  CONSTRAINT `FK_vendor_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Added on 04-12-2015 12:00PM start
ALTER TABLE `isg`.`group` DROP COLUMN `division`;
ALTER TABLE `isg`.`group` DROP COLUMN `standard`;

ALTER TABLE `isg`.`group` ADD COLUMN `active` CHAR(1) NOT NULL DEFAULT 'Y' AFTER `chat_dialogue_id`,
 ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `active`;

CREATE TABLE `isg`.`group_standard` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `group_id` INTEGER NOT NULL,
  `standard` INTEGER,
  `division` VARCHAR(45),
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_group_standard_group_1` FOREIGN KEY `FK_group_standard_group_1` (`group_id`)
    REFERENCES `group` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


CREATE TABLE `isg`.`group_user` (
  `group_id` INTEGER NOT NULL,
  `user_id` INTEGER NOT NULL,
  PRIMARY KEY (`group_id`, `user_id`),
  CONSTRAINT `FK_groupuser_group` FOREIGN KEY `FK_groupuser_group` (`group_id`)
    REFERENCES `group` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_groupuser_user` FOREIGN KEY `FK_groupuser_user` (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


ALTER TABLE `isg`.`user` CHANGE COLUMN `branch_id` `institute_id` INTEGER DEFAULT NULL,
 ADD CONSTRAINT `FK_user_institute` FOREIGN KEY `FK_user_institute` (`institute_id`)
    REFERENCES `institute` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
-- -- Added on 04-12-2015 12:00PM End

-- Added on 05-12-2015 11:00PM Start
ALTER TABLE `isg`.`event` ADD COLUMN `frequency` VARCHAR(15) AFTER `branch_id`,
 ADD COLUMN `by_day` VARCHAR(45) AFTER `frequency`,
 ADD COLUMN `by_date` VARCHAR(45) AFTER `by_day`;

-- Added on 05-12-2015 11:00PM End


-- Added on 06-12-2015 01:00PM Start
ALTER TABLE `isg`.`group_event` RENAME TO `isg`.`event_group`;

CREATE TABLE `isg`.`user_event` (
  `user_id` INTEGER NOT NULL,
  `role_id` INTEGER UNSIGNED NOT NULL,
  `entity_id` INTEGER NOT NULL,
  `event_id` INTEGER NOT NULL,
  CONSTRAINT `FK_userevent_user` FOREIGN KEY `FK_userevent_user` (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_userevent_role` FOREIGN KEY `FK_userevent_role` (`role_id`)
    REFERENCES `role` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_userevent_event` FOREIGN KEY `FK_userevent_event` (`event_id`)
    REFERENCES `event` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;

-- Added on 06-12-2015 01:00PM End

-- Added on 06-12-2015 02:00PM Start
ALTER TABLE `isg`.`event_group` ADD COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT AFTER `event_id`,
 DROP PRIMARY KEY,
 ADD PRIMARY KEY  USING BTREE(`id`);
 
 ALTER TABLE `isg`.`user_event` ADD COLUMN `event_group_id` INTEGER AFTER `event_id`,
 ADD CONSTRAINT `FK_userevent_eventgroup` FOREIGN KEY `FK_userevent_eventgroup` (`event_group_id`)
    REFERENCES `event_group` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
-- Added on 06-12-2015 02:00PM End

-- Added on 07-12-2015 07:00 PM

ALTER TABLE `isg`.`user_event` ADD COLUMN `id` INT(11) NOT NULL AUTO_INCREMENT  AFTER `event_group_id`
, ADD PRIMARY KEY (`id`) ;

-- End Added on 07-12-2015 07:00 PM

-- Added on 08-12-2015 12:00 PM

ALTER TABLE `isg`.`user` ADD COLUMN `gcm_id` VARCHAR(255) AFTER `institute_id`;

-- End Added on 08-12-2015 12:00 PM

-- Added on 09-12-2015

ALTER TABLE `isg`.`course` CHANGE COLUMN `branch_id` `branch_id` INT(11) NULL  ;

ALTER TABLE `isg`.`parent` ADD COLUMN `photo` VARCHAR(255) NULL  AFTER `active` ;
-- End added on 09-12-2015

-- 10 Dec 2015
ALTER TABLE `isg`.`institute` ADD COLUMN `logo_url` VARCHAR(255) AFTER `active`;
--10 Dec 2015

-- 23 Dec 2015

CREATE TABLE `isg`.`group_individual` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `group_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `role_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_groupindividual_groupid` FOREIGN KEY `FK_groupindividual_groupid` (`group_id`)
    REFERENCES `group` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_groupindividual_userid` FOREIGN KEY `FK_groupindividual_userid` (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_groupindividual_roleid` FOREIGN KEY `FK_groupindividual_roleid` (`role_id`)
    REFERENCES `role` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


ALTER TABLE `isg`.`fees` ADD COLUMN `isemail` CHAR(1) AFTER `branch_id`,
 ADD COLUMN `issms` CHAR(1) AFTER `isemail`,
 ADD COLUMN `isnotification` CHAR(1) AFTER `issms`,
 ADD COLUMN `frequency` VARCHAR(45) NOT NULL AFTER `isnotification`;

-- 23 Dec 2015 

--25 Dec 2015

CREATE TABLE `personal_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `venue` varchar(100) DEFAULT NULL,
  `startdate` date NOT NULL,
  `enddate` date DEFAULT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `alldayevent` char(1) DEFAULT 'N',
  `reminder` char(1) DEFAULT 'N',
  `reminderbefore` int(10) DEFAULT NULL COMMENT 'reminder before will be stored in minutes basis',
  `forstudent` varchar(1) DEFAULT 'N',
  `student_id` int(11) DEFAULT NULL,
  `recurring` char(1) DEFAULT 'N',
  `frequency` varchar(15) DEFAULT NULL,
  `by_day` varchar(45) DEFAULT NULL,
  `by_date` varchar(45) DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_student_id_idx` (`student_id`),
  KEY `fk_role_id_idx` (`role_id`),
  CONSTRAINT `fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- 25 Dec 2015

-- 28 Dec 2015
ALTER TABLE `isg`.`teacher` ADD COLUMN `photo` VARCHAR(255) AFTER `active`,
 ADD COLUMN `branch_id` INTEGER AFTER `photo`;

--28 Dec 2015

-- 30 Dec 2015

CREATE TABLE `token` (
  `token` varchar(30) CHARACTER SET latin1 NOT NULL,
  `created_date` datetime NOT NULL,
  `valid_for` int(11) NOT NULL DEFAULT '60' COMMENT 'Valid for in minutes after creation',
  `for_entity_id` int(11) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 30 Dec 2015

-- 02-01-2016
ALTER TABLE `isg`.`parent_otp` RENAME TO  `isg`.`otp` ;
--02-01-2016


--03-01-2016

CREATE TABLE `passbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `date` date NOT NULL,
  `is_debit` char(1) CHARACTER SET latin1 NOT NULL,
  `is_credit` char(45) CHARACTER SET latin1 NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_transaction_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_created_by_user_id_idx` (`user_id`),
  KEY `fk_parent_transaction_id_idx` (`parent_transaction_id`),
  CONSTRAINT `fk_created_by_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_parent_transaction_id` FOREIGN KEY (`parent_transaction_id`) REFERENCES `passbook` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--03-01-2016

-- 04-01-2016
DROP TABLE IF EXISTS `vendor`;
CREATE TABLE  `vendor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `middleName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) CHARACTER SET latin1 NOT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `emailAddress` varchar(45) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(1) CHARACTER SET latin1 NOT NULL,
  `primaryContact` varchar(45) CHARACTER SET latin1 NOT NULL,
  `secondaryContact` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `addressLine` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pinCode` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` char(1) CHARACTER SET latin1 NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_teacher_1` (`user_id`),
  CONSTRAINT `FK_vendor_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- 04-01-2016


--11-01-2016
CREATE TABLE `isg`.`standard` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `displayName` VARCHAR(45) NOT NULL,
  `code` VARCHAR(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_standard_branch_1` FOREIGN KEY `FK_standard_branch_1` (`branch_id`)
    REFERENCES `branch` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;


CREATE TABLE `isg`.`division` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `displayName` VARCHAR(45) NOT NULL,
  `code` VARCHAR(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_division_branch_1` FOREIGN KEY `FK_division_branch_1` (`branch_id`)
    REFERENCES `branch` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;

CREATE TABLE `isg`.`standard_division` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `standard_id` INTEGER UNSIGNED NOT NULL,
  `division_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_standard_division_branch` FOREIGN KEY `FK_standard_division_branch` (`branch_id`)
    REFERENCES `branch` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_standard_division_standard` FOREIGN KEY `FK_standard_division_standard`  (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_standard_division_division` FOREIGN KEY `FK_standard_division_division`  (`division_id`)
    REFERENCES `division` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;



ALTER TABLE `isg`.`standard` ADD COLUMN `active` VARCHAR(1) NOT NULL DEFAULT 'Y' AFTER `branch_id`;
ALTER TABLE `isg`.`division` ADD COLUMN `active` VARCHAR(1) NOT NULL DEFAULT 'Y' AFTER `branch_id`;


ALTER TABLE `isg`.`teacher` MODIFY COLUMN `branch_id` INTEGER NOT NULL;

DROP TABLE IF EXISTS `isg`.`sms_config`;
CREATE TABLE  `isg`.`sms_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vendorUrl` varchar(255) NOT NULL,
  `username` varchar(65) NOT NULL,
  `password` varchar(45) NOT NULL,
  `sid` varchar(45) NOT NULL,
  `flash_message` varchar(1) NOT NULL DEFAULT 'N',
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `isg`.`email_config`;
CREATE TABLE  `isg`.`email_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serverName` varchar(255) NOT NULL,
  `port` varchar(45) NOT NULL,
  `connectionType` varchar(45) NOT NULL,
  `security` varchar(45) NOT NULL,
  `authenticationMethod` varchar(45) NOT NULL,
  `username` varchar(65) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `isg`.`passbook` ADD COLUMN `for_entity_name` VARCHAR(100) NOT NULL  AFTER `parent_transaction_id` ;



--11-01-2016

--12-01-2016
ALTER TABLE `isg`.`event` ADD COLUMN `created_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP  
AFTER `by_date` , ADD COLUMN `updated_date` TIMESTAMP NULL  AFTER `created_date` , 
ADD COLUMN `is_schedule_updated` CHAR(1) NULL DEFAULT 'N'  AFTER `updated_date` , 
ADD COLUMN `is_deleted` CHAR(1) NULL DEFAULT 'N'  AFTER `is_schedule_updated` ;

ALTER TABLE `isg`.`event` 
CHANGE COLUMN `is_deleted` `status` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to N for new, M for Modified and D for deleted'  ;

ALTER TABLE `isg`.`user_event` 
ADD COLUMN `status` CHAR(1) NOT NULL COMMENT 'Set it to N for new, M for Modified and D for deleted'  AFTER `id` ;


ALTER TABLE `isg`.`personal_task` ADD COLUMN `created_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP  
AFTER `entity_id` , ADD COLUMN `updated_date` TIMESTAMP NULL  AFTER `created_date` , 
ADD COLUMN `is_schedule_updated` CHAR(1) NULL DEFAULT 'N'  AFTER `updated_date` , 
ADD COLUMN `is_deleted` CHAR(1) NULL DEFAULT 'N'  AFTER `is_schedule_updated` ;

ALTER TABLE `isg`.`personal_task` ADD COLUMN `status` CHAR(1) NOT NULL  AFTER `is_deleted` ;
ALTER TABLE `isg`.`personal_task` DROP COLUMN `is_deleted` ;
--12-01-2016


--13-01-2016
DROP TABLE IF EXISTS `goal_status`;
CREATE TABLE `goal_status` (
  `code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `description` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `goal_reward_type`;
CREATE TABLE `goal_reward_type` (
  `code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `description` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `goals`;


CREATE TABLE `goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) CHARACTER SET latin1 NOT NULL,
  `description` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `reward_type` varchar(15) CHARACTER SET latin1 NOT NULL,
  `reward_amount` decimal(10,2) DEFAULT NULL,
  `reward_description` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `feedback` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `student_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status_code` varchar(15) CHARACTER SET latin1 NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_parent_id_idx` (`parent_id`),
  KEY `fk_status_code_idx` (`status_code`),
  KEY `fk_goal_type_idx` (`reward_type`),
  CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `parent` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reward_type` FOREIGN KEY (`reward_type`) REFERENCES `goal_reward_type` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_code` FOREIGN KEY (`status_code`) REFERENCES `goal_status` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--13-01-2016

--15-01-2016
ALTER TABLE `isg`.`vendor` ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `photo`;

ALTER TABLE `isg`.`user` ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `gcm_id`;

ALTER TABLE `isg`.`teacher` ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `branch_id`;

ALTER TABLE `isg`.`student` ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `student_upload_id`;

ALTER TABLE `isg`.`student_upload` ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `user_id`;

ALTER TABLE `isg`.`institute` ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `logo_url`;

ALTER TABLE `isg`.`school_admin` ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `active`;

ALTER TABLE `isg`.`user_chat_profile` ADD COLUMN `active` CHAR(1) NOT NULL DEFAULT 'Y' AFTER `user_id`,
 ADD COLUMN `isDelete` CHAR(1) NOT NULL DEFAULT 'N' AFTER `active`;
 
--15-01-2016

--16-01-2016
ALTER TABLE `isg`.`passbook` ADD COLUMN `created_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP  AFTER `for_entity_name` , 
ADD COLUMN `updated_date` TIMESTAMP NULL  AFTER `created_date` ;

ALTER TABLE `isg`.`goals` ADD COLUMN `updated_date` TIMESTAMP NULL  AFTER `created_date` ;
--16-01-2016

--17-01-2016
ALTER TABLE `isg`.`personal_task` CHANGE COLUMN `status` `status` CHAR(1) NOT NULL DEFAULT 'N'  ;

ALTER TABLE `isg`.`user_event` CHANGE COLUMN `status` `status` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to N for new, M for Modified and D for deleted'  ;
--17-01-2016

--18-01-2016

CREATE TABLE `group_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `student_comments_allowed` char(1) NOT NULL DEFAULT 'Y',
  `is_active` char(1) NOT NULL DEFAULT 'Y',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gorup_group_id_idx` (`group_id`),
  KEY `branch_branch_id_idx` (`branch_id`),
  CONSTRAINT `branch_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `gorup_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--18-01-2016

--19-01-2016
ALTER TABLE `isg`.`standard_division` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT,
 MODIFY COLUMN `standard_id` INTEGER NOT NULL,
 MODIFY COLUMN `division_id` INTEGER NOT NULL,
 DROP FOREIGN KEY `FK_standard_division_division`,
 DROP FOREIGN KEY `FK_standard_division_standard`;


ALTER TABLE `isg`.`standard` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT;

ALTER TABLE `isg`.`division` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT;


ALTER TABLE `isg`.`standard_division` ADD CONSTRAINT `FK_standard_division_standard` FOREIGN KEY  `FK_standard_division_standard` (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_standard_division_division` FOREIGN KEY `FK_standard_division_division`  (`division_id`)
    REFERENCES `division` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;





ALTER TABLE `isg`.`fees` MODIFY COLUMN `id` INT(10) NOT NULL,
 CHANGE COLUMN `standard` `standard_id` INTEGER NOT NULL;


ALTER TABLE `isg`.`fees` ADD CONSTRAINT `FK_fees_standard` FOREIGN KEY `FK_fees_standard`  (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;



ALTER TABLE `isg`.`group_standard` CHANGE COLUMN `standard` `standard_id` INTEGER NOT NULL,
 CHANGE COLUMN `division` `division_id` INTEGER NOT NULL,
 ADD CONSTRAINT `FK_group_standard_standard` FOREIGN KEY `FK_group_standard_standard`  (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_group_standard_division` FOREIGN KEY `FK_group_standard_division`  (`division_id`)
    REFERENCES `division` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;



ALTER TABLE `isg`.`student` CHANGE COLUMN `standard` `standard_id` INTEGER DEFAULT NULL,
 CHANGE COLUMN `division` `division_id` INTEGER,
 ADD CONSTRAINT `FK_student_standard` FOREIGN KEY `FK_student_standard` (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `FK_student_division` FOREIGN KEY `FK_student_division` (`division_id`)
    REFERENCES `division` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;


ALTER TABLE `isg`.`student_upload` CHANGE COLUMN `standard` `standard_id` INTEGER NOT NULL,
 MODIFY COLUMN `standard_name` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 CHANGE COLUMN `division` `division_id` INTEGER NOT NULL,
 ADD CONSTRAINT `fk_student_upload_standard` FOREIGN KEY `fk_student_upload_standard`  (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
 ADD CONSTRAINT `fk_student_upload_division` FOREIGN KEY `fk_student_upload_division`  (`division_id`)
    REFERENCES `division` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;





DROP TABLE IF EXISTS `isg`.`institute_vendor`;
CREATE TABLE  `isg`.`institute_vendor` (
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`branch_id`) USING BTREE,
  KEY `FK_institute_vendor_2` (`branch_id`),
  CONSTRAINT `FK_institute_vendor_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_institute_vendor_2` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--19-01-2016

--20-01-2016

ALTER TABLE `isg`.`event_group` ADD COLUMN `status` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'Set it to  N for new, M for Modified and D for deleted'AFTER `id`;

ALTER TABLE `isg`.`student_event` ADD COLUMN `status` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'Set it  to N for new, M for Modified and D for deleted' AFTER `event_id`;

ALTER TABLE `isg`.`notice` MODIFY COLUMN `id` INTEGER NOT NULL,
 ADD COLUMN `sent_by` INTEGER NOT NULL COMMENT 'user id ' AFTER `detail_description`,
 ADD CONSTRAINT `FK_notice_user` FOREIGN KEY `FK_notice_user` (`sent_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `isg`.`notice` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT;

ALTER TABLE `isg`.`fees` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT;

ALTER TABLE `isg`.`group_tags` MODIFY COLUMN `id` INTEGER NOT NULL AUTO_INCREMENT;



ALTER TABLE `isg`.`notice` DROP COLUMN `standard` ;
ALTER TABLE `isg`.`student` DROP COLUMN `standard_name` ;
ALTER TABLE `isg`.`event` DROP COLUMN `division` , DROP COLUMN `standard` , DROP COLUMN `semester` , DROP COLUMN `year` , DROP COLUMN `board` ;
ALTER TABLE `isg`.`student_upload` DROP COLUMN `standard_name` ;

ALTER TABLE `isg`.`user` MODIFY COLUMN `username` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `isg`.`student` ADD COLUMN `hobbies` VARCHAR(100) NULL  AFTER `isDelete` ;
ALTER TABLE `isg`.`student` CHANGE COLUMN `hobbies` `hobbies` VARCHAR(100) NULL DEFAULT NULL  AFTER `goal_parent` ;
--20-01-2016

--31-01-2016
DROP TABLE IF EXISTS `isg`.`branch_forum_detail`;

CREATE  TABLE `isg`.`branch_forum_detail` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `forum_category_id` INT(11) NOT NULL ,
  `read_write_group_id` INT(11) NOT NULL ,
  `read_only_group_id` INT(11) NOT NULL ,
  `branch_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_branch_id_idx` (`branch_id` ASC) ,
  CONSTRAINT `fk_branch_id`
    FOREIGN KEY (`branch_id` )
    REFERENCES `isg`.`branch` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

DROP TABLE IF EXISTS `isg`.`group_forum`;



CREATE TABLE `isg`.`group_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `forum_id` int(11) NOT NULL,
  `read_only` char(1) NOT NULL DEFAULT 'Y',
  `is_active` char(1) NOT NULL DEFAULT 'Y',
  `is_delete` char(1) NOT NULL DEFAULT 'N',
  `group_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_group_id_idx` (`group_id`),
  KEY `branch_branch_id_idx` (`branch_id`),
  CONSTRAINT `branch_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `group_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `isg`.`user_forum_profile`;

CREATE TABLE `isg`.`user_forum_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `forum_profile_id` int(11) NOT NULL,
  `is_active` char(1) DEFAULT 'Y',
  `is_delete` char(1) DEFAULT 'N',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `forum_profile_id_UNIQUE` (`forum_profile_id`),
  KEY `fk_user_id_forum_profile` (`user_id`),
  CONSTRAINT `fk_user_id_forum_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `isg`.`student_marks`;
DROP TABLE IF EXISTS `isg`.`student_result`;
DROP TABLE IF EXISTS `isg`.`subject`;

CREATE TABLE `isg`.`subject` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `branch_id` INTEGER NOT NULL,
  `standard_id` INTEGER NOT NULL,
  `semester` VARCHAR(45) NOT NULL,
  `name` VARCHAR(65) NOT NULL,
  `total_marks` VARCHAR(45) NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  `is_delete` CHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_subject_branch` FOREIGN KEY `FK_subject_branch` (`branch_id`)
    REFERENCES `branch` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_subject_standard` FOREIGN KEY `FK_subject_standard` (`standard_id`)
    REFERENCES `standard` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


CREATE TABLE `isg`.`student_result` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `student_id` INTEGER NOT NULL,
  `result` VARCHAR(45),
  `final_grade` VARCHAR(45),
  `position` VARCHAR(45),
  `general_remark` VARCHAR(100),
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_student_result_student` FOREIGN KEY `FK_student_result_student` (`student_id`)
    REFERENCES `student` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;


CREATE TABLE `isg`.`student_marks` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `student_id` INTEGER NOT NULL,
  `theory` VARCHAR(45),
  `practical` VARCHAR(45),
  `obtained` VARCHAR(45),
  `subject_id` INTEGER NOT NULL,
  `student_result_id` INTEGER NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_student_marks_student` FOREIGN KEY `FK_student_marks_student` (`student_id`)
    REFERENCES `student` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_student_marks_subject` FOREIGN KEY `FK_student_marks_subject` (`subject_id`)
    REFERENCES `subject` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `FK_student_marks_student_result` FOREIGN KEY `FK_student_marks_student_result` (`student_result_id`)
    REFERENCES `student_result` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
)
ENGINE = InnoDB;

ALTER TABLE `isg`.`student_result` ADD COLUMN `gross_total` VARCHAR(45) AFTER `general_remark`;
ALTER TABLE `isg`.`student_result` ADD COLUMN `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `gross_total`;

DROP TABLE IF EXISTS `isg`.`user_mall_profile`;
CREATE TABLE `isg`.`user_mall_profile` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `is_active` VARCHAR(1) NOT NULL DEFAULT 'Y',
  `is_delete` VARCHAR(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_user_mall_profile_user` FOREIGN KEY `FK_user_mall_profile_user` (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
);

ALTER TABLE `isg`.`passbook` ADD COLUMN `for_entity_user_id` INT(11) NOT NULL  AFTER `updated_date` ;

drop table `isg`.`institute_branch` 



DROP TABLE IF EXISTS `isg`.`ediary`;
CREATE TABLE `isg`.`ediary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `status` varchar(45) DEFAULT 'N',
  `branch_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ediary_branch_id_idx` (`branch_id`),
  KEY `fk_ediary_created_by_idx` (`created_by`),
  CONSTRAINT `fk_ediary_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ediary_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `isg`.`ediary_group`;
CREATE TABLE `isg`.`ediary_group` (
  `ediary_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`ediary_id`,`group_id`),
  KEY `fk_ediarygroup_ediary_idx` (`ediary_id`),
  KEY `fk_ediarygroup_group_idx` (`group_id`),
  CONSTRAINT `fk_ediarygroup_ediary` FOREIGN KEY (`ediary_id`) REFERENCES `ediary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ediarygroup_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `isg`.`ediary_individual`;

CREATE TABLE `isg`.`ediary_individual` (
  `ediary_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ediary_id`,`user_id`),
  KEY `fk_ediaryindividual_ediary_idx` (`ediary_id`),
  KEY `fk_ediaryindividual_user_idx` (`user_id`),
  CONSTRAINT `fk_ediaryindividual_ediary` FOREIGN KEY (`ediary_id`) REFERENCES `ediary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ediaryindividual_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--31-01-2016



--03022016
ALTER TABLE `isg`.`ediary` ADD COLUMN `created_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP  AFTER `created_by` , ADD COLUMN `updated_date` TIMESTAMP NULL  AFTER `created_date` ;
--03022016


--05022016
ALTER TABLE `isg`.`head` ADD COLUMN `branch_id` INTEGER AFTER `active`,
 ADD CONSTRAINT `FK_head_branchId` FOREIGN KEY `FK_head_branchId` (`branch_id`)
    REFERENCES `branch` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `isg`.`head` MODIFY COLUMN `branch_id` INTEGER,
 ADD COLUMN `is_delete` CHAR(1) NOT NULL AFTER `branch_id`;

ALTER TABLE `isg`.`head` MODIFY COLUMN `is_delete` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N';

ALTER TABLE `isg`.`head` DROP INDEX `name_UNIQUE`,
 ADD UNIQUE INDEX `name_UNIQUE` USING BTREE(`name`, `branch_id`);


ALTER TABLE `isg`.`fees` MODIFY COLUMN `head_id` INTEGER NOT NULL,
 ADD CONSTRAINT `FK_fees_head` FOREIGN KEY `FK_fees_head` (`head_id`)
    REFERENCES `head` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
    
ALTER TABLE `isg`.`notice` CHANGE COLUMN `priority` `priority` VARCHAR(45) NULL COMMENT 'High and Low'  ;
--05022016


--08022016
ALTER TABLE `isg`.`ediary_individual` RENAME TO  `isg`.`user_ediary` ;

ALTER TABLE `isg`.`group_forum` ADD COLUMN `created_by` INTEGER NOT NULL AFTER `forum_group_id`,
 ADD CONSTRAINT `FK_group_forum_user` FOREIGN KEY `FK_group_forum_user` (`created_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
    
--0802016

--15022016
DROP TABLE IF EXISTS `isg`.`payment_account_type`;
	CREATE TABLE `payment_account_type` (
	  `code` varchar(45) NOT NULL,
	  `description` varchar(45) DEFAULT NULL,
	  PRIMARY KEY (`code`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
DROP TABLE IF EXISTS `isg`.`user_payment_account`;	
	CREATE TABLE `user_payment_account` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `account_type` varchar(45) NOT NULL,
	  `user_id` int(11) NOT NULL,
	  `account_number` bigint(20) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `fk_payment_account_user_id_idx` (`user_id`),
	  CONSTRAINT `fk_payment_account_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
	) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
	
DROP TABLE IF EXISTS `isg`.`academic_year`;
CREATE TABLE  `isg`.`academic_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `is_current_active_year` char(1) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_academic_year_branchId` (`branch_id`),
  CONSTRAINT `FK_academic_year_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `isg`.`holidays`;
CREATE TABLE  `isg`.`holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_academic_year` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_holidays_currentYear` (`current_academic_year`),
  KEY `FK_holidays_branchId` (`branch_id`),
  CONSTRAINT `FK_holidays_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_holidays_currentYear` FOREIGN KEY (`current_academic_year`) REFERENCES `academic_year` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `isg`.`working_days`;
CREATE TABLE  `isg`.`working_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_academic_year` int(11) NOT NULL,
  `monday` char(1) DEFAULT NULL,
  `tuesday` char(1) DEFAULT NULL,
  `wednesday` char(1) DEFAULT NULL,
  `thursday` char(1) DEFAULT NULL,
  `friday` char(1) DEFAULT NULL,
  `saturday` char(1) DEFAULT NULL,
  `sunday` char(1) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_working_days_currentYear` (`current_academic_year`),
  KEY `FK_working_days_branchId` (`branch_id`),
  CONSTRAINT `FK_working_days_branchId` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FK_working_days_currentYear` FOREIGN KEY (`current_academic_year`) REFERENCES `academic_year` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


--15022016


--18022016
ALTER TABLE `isg`.`user_mall_profile` ADD COLUMN `shopping_profile_id` INT(11) NULL  AFTER `password` ;

DROP TABLE IF EXISTS `isg`.`authentication_type`;
CREATE  TABLE `isg`.`authentication_type` (
  `code` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`code`) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) )
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `isg`.`student_marks` ADD COLUMN `remarks` VARCHAR(100) AFTER `student_result_id`;

ALTER TABLE `isg`.`user` ADD COLUMN `authentication_type` VARCHAR(45) NOT NULL DEFAULT 'APPLICATION' AFTER `isDelete`;


ALTER TABLE `isg`.`user` 
  ADD CONSTRAINT `fk_authentication_type`
  FOREIGN KEY (`authentication_type` )
  REFERENCES `isg_sit`.`authentication_type` (`code` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_authentication_type_idx` (`authentication_type` ASC) ;



--18022016


--23022016
ALTER TABLE `isg`.`group` ADD COLUMN `created_by` INTEGER AFTER `isDelete`,
 ADD CONSTRAINT `FK_group_userId` FOREIGN KEY `FK_group_userId` (`created_by`)
    REFERENCES `user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
    
ALTER TABLE `isg`.`parent` CHANGE COLUMN `primaryContact` `primaryContact` VARCHAR(10) NULL  ;

ALTER TABLE `isg`.`institute_vendor` RENAME TO  `isg`.`branch_vendor` ;

drop table `isg`.`vendor`;

CREATE  TABLE `isg`.`vendor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `merchant_legal_name` VARCHAR(100) NOT NULL ,
  `marketing_name` VARCHAR(100) NOT NULL ,
  `address` VARCHAR(100) NOT NULL ,
  `area` VARCHAR(56) NOT NULL ,
  `city` VARCHAR(56) NOT NULL ,
  `pincode` VARCHAR(6) NOT NULL ,
  `phone` VARCHAR(12) NOT NULL ,
  `selling_category` VARCHAR(45) NOT NULL ,
  `contact_first_name` VARCHAR(45) NOT NULL ,
  `contact_last_name` VARCHAR(45) NOT NULL ,
  `mobile` VARCHAR(15) NOT NULL ,
  `email` VARCHAR(56) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `active` CHAR(1) NULL DEFAULT 'N' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_vendor_user_id_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_vendor_user_id`
    FOREIGN KEY (`user_id` )
    REFERENCES `isg`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `isg`.`email_config` ADD COLUMN `password` VARCHAR(45) NOT NULL AFTER `active`;

ALTER TABLE `isg`.`event` ADD COLUMN `forvendor` VARCHAR(1) AFTER `status`;

drop table IF EXISTS `isg`.`contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `photo` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `firstname` varchar(45) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `fk_profile_search_user_id_idx` (`user_id`),
  CONSTRAINT `fk_profile_search_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE `isg`.`parent` 
ADD UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) ;

ALTER TABLE `isg`.`student`
ADD UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) ;

ALTER TABLE `isg`.`vendor` 
ADD UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) ;

ALTER TABLE `isg`.`teacher` DROP FOREIGN KEY `FK_teacher_1` ;

ALTER TABLE `isg`.`teacher` CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL  , 
  ADD CONSTRAINT `FK_teacher_1`
  FOREIGN KEY (`user_id` )
  REFERENCES `isg`.`user` (`id` )
, ADD UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) ;


CREATE TABLE `user_contact` (
  `user_id` int(11) NOT NULL,
  `contact_user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`contact_user_id`),
  KEY `fk_user_contact_user_id_idx` (`user_id`),
  KEY `fk_user_contact_contact_user_id_idx` (`contact_user_id`),
  CONSTRAINT `fk_user_contact_contact_user_id` FOREIGN KEY (`contact_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_contact_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER $$
USE `isg`$$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `isg`.`parent_insert_trig`
AFTER INSERT ON `isg`.`parent`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
values(NEW.email, NEW.primaryContact, NEW.user_id, NEW.firstname, NEW.lastname, NEW.city, NEW.photo);

END$$

DELIMITER $$

USE `isg`$$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `isg`.`parent_update_trig`
AFTER UPDATE ON `isg`.`parent`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email, phone =NEW.primaryContact, photo = NEW.photo, 
firstname= NEW.firstname, lastname=NEW.lastname, city=NEW.city
where user_id = NEW.user_id;

END$$

DELIMITER $$
USE `isg`$$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `isg`.`student_insert_trig`
AFTER INSERT ON `isg`.`student`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
values(NEW.email_address, NEW.mobile, NEW.user_id, NEW.first_name, NEW.last_name, NEW.city, NEW.photo);


END$$

DELIMITER $$
USE `isg`$$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `isg`.`student_update_trig`
AFTER UPDATE ON `isg`.`student`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email_address, phone =NEW.mobile, photo = NEW.photo, 
firstname= NEW.first_name, lastname=NEW.last_name, city=NEW.city
where user_id = NEW.user_id;

END$$

DELIMITER $$
USE `isg`$$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `isg`.`teacher_insert_trig`
AFTER INSERT ON `isg`.`teacher`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, lastname, city, photo) 
values(NEW.emailAddress, NEW.primaryContact, NEW.user_id, NEW.firstName, NEW.lastName, NEW.city, NEW.photo);


END$$

DELIMITER $$
USE `isg`$$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `isg`.`teacher_update_trig`
AFTER UPDATE ON `isg`.`teacher`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.emailAddress, phone =NEW.primaryContact, photo = NEW.photo, 
firstname= NEW.firstName, lastname=NEW.lastName, city=NEW.city
where user_id = NEW.user_id;

END$$

DELIMITER $$
USE `isg`$$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `isg`.`vendor_insert_trig`
AFTER INSERT ON `isg`.`vendor`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
insert into contact(email,phone,user_id,firstname, city) 
values(NEW.email, NEW.mobile, NEW.user_id, NEW.merchant_legal_name, NEW.city);


END$$

DELIMITER $$
USE `isg`$$

CREATE
DEFINER=`root`@`localhost`
TRIGGER `isg`.`vendor_update_trig`
AFTER UPDATE ON `isg`.`vendor`
FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
update contact set email = NEW.email, phone =NEW.mobile,  
firstname= NEW.merchant_legal_name, city=NEW.city
where user_id = NEW.user_id;

END;


ALTER TABLE `isg`.`group` ADD COLUMN `is_public_group` CHAR(1) DEFAULT 'N' AFTER `created_by`;

ALTER TABLE `isg`.`vendor` MODIFY COLUMN `active` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'Y';
--23022016


--01032016

ALTER TABLE `isg`.`user` DROP FOREIGN KEY `FK_user_1` ;

ALTER TABLE `isg`.`user` DROP COLUMN `role_id` 

, DROP INDEX `FK_user_roll1` ;

drop table IF EXISTS `forgot_password_otp`;
CREATE TABLE `forgot_password_otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `otp` varchar(10) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expire_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `otp_UNIQUE` (`otp`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


ALTER TABLE `isg`.`token` ADD COLUMN `role_id` INT(10) UNSIGNED NOT NULL  AFTER `for_entity_id` ,
  ADD CONSTRAINT `fk_token_role_id`
  FOREIGN KEY (`role_id` )
  REFERENCES `isg`.`role` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_token_role_id_idx` (`role_id` ASC) ;


DELIMITER $$



DROP TRIGGER IF EXISTS isg.parent_insert_trig$$

USE `isg`$$





CREATE

DEFINER=`root`@`localhost`

TRIGGER `isg`.`parent_insert_trig`

AFTER INSERT ON `isg`.`parent`

FOR EACH ROW

-- Edit trigger body code below this line. Do not edit lines above this one

BEGIN



IF NOT EXISTS (SELECT id FROM contact WHERE user_id = NEW.user_id) THEN

insert into contact(email,phone,user_id,firstname, lastname, city, photo) 

values(NEW.email, NEW.primaryContact, NEW.user_id, NEW.firstname, NEW.lastname, NEW.city, NEW.photo);

END IF;



END$$

DELIMITER ;

ALTER TABLE `isg`.`fees` ADD COLUMN `academic_year_id` INTEGER NOT NULL,
 ADD CONSTRAINT `FK_fees_academicYear` FOREIGN KEY `FK_fees_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`standard` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `active`,
 ADD CONSTRAINT `FK_standard_academicYear` FOREIGN KEY `FK_standard_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`division` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `active`,
 ADD CONSTRAINT `FK_division_academicYear` FOREIGN KEY `FK_division_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `isg`.`head` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `is_delete`,
 ADD CONSTRAINT `FK_head_academicYear` FOREIGN KEY `FK_head_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `isg`.`notice` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `sent_by`,
 ADD CONSTRAINT `FK_notice_academicYear` FOREIGN KEY `FK_notice_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`group` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `is_public_group`,
 ADD CONSTRAINT `FK_group_academicYear` FOREIGN KEY `FK_group_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;

ALTER TABLE `isg`.`event` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `forvendor`,
 ADD CONSTRAINT `FK_event_academicYear` FOREIGN KEY `FK_event_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`group_forum` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `created_by`,
 ADD CONSTRAINT `FK_group_forum_academicYear` FOREIGN KEY `FK_group_forum_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;


ALTER TABLE `isg`.`subject` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `is_delete`,
 ADD CONSTRAINT `FK_subject_academicYear` FOREIGN KEY `FK_subject_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;



ALTER TABLE `isg`.`student_result` ADD COLUMN `academic_year_id` INTEGER NOT NULL AFTER `created_date`,
 ADD CONSTRAINT `FK_student_result_academicYear` FOREIGN KEY `FK_student_result_academicYear` (`academic_year_id`)
    REFERENCES `academic_year` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT;
    
 ALTER TABLE `isg`.`menu_items` ADD COLUMN `menu_position` INTEGER NOT NULL AFTER `state_url`;
 
 ALTER TABLE `isg`.`user` DROP FOREIGN KEY `FK_user_institute` ;

ALTER TABLE `isg`.`user` CHANGE COLUMN `authentication_type` `authentication_type` VARCHAR(45) NULL DEFAULT 'APPLICATION'  AFTER `id` , CHANGE COLUMN `role_id` `role_id` INT(10) UNSIGNED NOT NULL  AFTER `password` , CHANGE COLUMN `isDelete` `isDelete` CHAR(1) NOT NULL DEFAULT 'N'  AFTER `active` , CHANGE COLUMN `gcm_id` `gcm_id` VARCHAR(255) NULL  AFTER `isDelete` , CHANGE COLUMN `institute_id` `institute_id` INT(11) NULL  , ADD COLUMN `password_salt` VARCHAR(255) NOT NULL  AFTER `role_id` , ADD COLUMN `locked` CHAR(1) NOT NULL DEFAULT 'N'  AFTER `password_salt` , 
  ADD CONSTRAINT `FK_user_institute`
  FOREIGN KEY (`institute_id` )
  REFERENCES `isg_sit`.`institute` (`id` );
  
ALTER TABLE `isg_sit`.`user` CHANGE COLUMN `password` `password` VARCHAR(255) NOT NULL;
  
  
 
--01032016