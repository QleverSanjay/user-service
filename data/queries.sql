/*Added by ameya for OTP 16-OCT-17*/
create table otp_details(
`id` int(11) NOT NULL AUTO_INCREMENT,
`username` varchar(100)  NOT NULL,
`contact` varchar(10) NOT NULL,
`otp_number` int(6) NOT NULL,
`email_address` varchar(45) , 
`otp_creation_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`otp_end_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`active` varchar(1) NOT NULL,  
PRIMARY KEY (`id`));