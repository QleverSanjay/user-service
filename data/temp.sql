Drop table caste;

CREATE TABLE `caste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

Drop table religion;
CREATE TABLE `religion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

Drop table parent;
CREATE TABLE `parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `middlename` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `email` varchar(45) NOT NULL,
  `dob` date NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `taluka_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `primaryContact` varchar(10) NOT NULL,
  `secondaryContact` varchar(10) DEFAULT NULL,
  `caste_id` int(11) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `addressLine` varchar(45) NOT NULL,
  `area` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `pincode` varchar(45) NOT NULL,
  `active` char(1) DEFAULT 'Y',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `f-d-id_idx` (`district_id`),
  KEY `f-t-id_idx` (`taluka_id`),
  KEY `f-s-id_idx` (`state_id`),
  KEY `f-ca-id_idx` (`caste_id`),
  KEY `fk-religion-id_idx` (`religion_id`),
  KEY `fk-user-id_idx` (`user_id`),
  CONSTRAINT `fk-caste-id` FOREIGN KEY (`caste_id`) REFERENCES `caste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-district-id` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-religion-id` FOREIGN KEY (`religion_id`) REFERENCES `religion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-state-id` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-taluka-id` FOREIGN KEY (`taluka_id`) REFERENCES `taluka` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk-user-id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
