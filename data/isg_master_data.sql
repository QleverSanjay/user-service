INSERT INTO `caste` (`id`,`code`,`name`,`active`) VALUES
 (1,'NT','NT','Y'),
 (2,'Open','OPEN','Y'),
 (3,'SC','SC','Y'),
 (4,'OBC','OBC','Y');

 INSERT INTO `state` (`id`,`code`,`name`,`active`) VALUES
 (1,'NH','Maharashtra','Y');

 INSERT INTO `district` (`id`,`code`,`name`,`active`,`state_id`) VALUES
 (1,'Thane','Thane','Y',1),
 (2,'Mumbai','Mumbai','Y',1);
 
 INSERT INTO `taluka` (`id`,`code`,`name`,`active`,`district_id`) VALUES
 (1,'Kalyan','Kalyan','Y',1),
 (2,'Mumbai','Mumbai','Y',2);

  INSERT INTO `head` (`id`,`name`,`active`) VALUES
 (1,'Transportation fees','Y'),
 (2,'Primary','Y'),
 (3,'Term Fees','Y'),
 (4,'Annual Event Fees','Y'),
 (5,'School Development','Y');


  INSERT INTO `religion` (`id`,`code`,`name`,`active`) VALUES
 (1,'HN','Hindu','Y');

  INSERT INTO `role` (`id`,`name`) VALUES
 (1,'ADMIN'),
 (2,'STUDENT'),
 (3,'PARENT'),
 (4,'TEACHER'),
 (5,'VENDOR'),
 (6,'SCHOOL ADMIN');

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`,`username`,`password`,`active`,`role_id`,`institute_id`) VALUES
 (1,'admin','pass','Y',1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


INSERT INTO `isg`.`course` (`name`, `active`) VALUES ('English', 'Y');
