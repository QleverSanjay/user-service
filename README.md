# user-service
User related service API are stored under this service


Build instructions

Software requirement
> Latest maven installation, refer maven site for installation instructions
> JDK 1.7

How to build
> On command prompt, navigate till project root folder
> inside root folder, on prompt type mvn clean install


How to run stand alone
> on command prompt, execute following command
java -jar target/user-service-X.X.X.jar
Note:replace X.X.X with correct service version.
