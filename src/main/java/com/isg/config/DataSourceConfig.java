package com.isg.config;

import java.util.Arrays;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MulticastConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.isg.config.AppProperties.HazelCast;
import com.isg.config.AppProperties.Jdbc;
import com.jolbox.bonecp.BoneCPDataSource;

@Configuration
@EnableTransactionManagement
public class DataSourceConfig {

	@Autowired
	private AppProperties appProperties;

	@Bean
	public BoneCPDataSource dataSource() {

		Jdbc jdbc = appProperties.getJdbc();
		
		BoneCPDataSource boneCPDataSource = new BoneCPDataSource();
		boneCPDataSource.setDriverClass(jdbc.getDriverClassName());
		boneCPDataSource.setJdbcUrl(jdbc.getUrl());
		boneCPDataSource.setUsername(jdbc.getUsername());
		boneCPDataSource.setPassword(jdbc.getPassword());
		boneCPDataSource.setIdleConnectionTestPeriodInSeconds(30);
		boneCPDataSource.setIdleMaxAgeInSeconds(30);
		boneCPDataSource.setMaxConnectionsPerPartition(10);
		boneCPDataSource.setMinConnectionsPerPartition(3);
		boneCPDataSource.setPartitionCount(1);
		boneCPDataSource.setAcquireIncrement(5);
		
		return boneCPDataSource;
	}

	@Bean
	public DataSourceTransactionManager transaction(
			BoneCPDataSource dataSource) {
		DataSourceTransactionManager transaction = new DataSourceTransactionManager();
		transaction.setDataSource(dataSource);
		return transaction;
	}

	@Bean
	public Config hazelCastConfig(GroupConfig groupConfig,
			Properties hazelProperties, NetworkConfig networkConfig) {
		HazelCast hazelCast = appProperties.getHazelCast();
		
		
		return new Config().setInstanceName("instance")
				.setGroupConfig(groupConfig).setProperties(hazelProperties)
				.setNetworkConfig(networkConfig)
				.addMapConfig(new MapConfig(hazelCast.getMapConfigName()));
	}

	@Bean
	public ClientConfig hazelCastClientConfig(GroupConfig groupConfig,
			ClientNetworkConfig clientNetworkConfig) {
		return new ClientConfig().setGroupConfig(groupConfig).setNetworkConfig(
				clientNetworkConfig);
	}

	@Bean
	public GroupConfig groupConfig() {

		HazelCast hazelCast = appProperties.getHazelCast();
		GroupConfig config = new GroupConfig();
		config.setName(hazelCast.getName());
		config.setPassword(hazelCast.getPassword());

		return config;
	}

	@Bean
	public NetworkConfig networkConfig(JoinConfig joinConfig) {

		HazelCast hazelCast = appProperties.getHazelCast();

		NetworkConfig config = new NetworkConfig();
		config.setPort(hazelCast.getDefaultPort());
		config.setPortAutoIncrement(true);
		config.setJoin(joinConfig);
		return config;
	}

	@Bean
	public Properties hazelProperties() {
		Properties properties = new Properties();
		properties.put("hazelcast.merge.first.run.delay.seconds", 5);
		properties.put("hazelcast.merge.next.run.delay.seconds", 5);

		return properties;
	}

	@Bean
	public JoinConfig joinConfig() {

		MulticastConfig multicastConfig = new MulticastConfig();
		multicastConfig.setEnabled(false);

		TcpIpConfig tcpIpConfig = new TcpIpConfig();
		tcpIpConfig.setEnabled(false);

		JoinConfig join = new JoinConfig();
		join.setMulticastConfig(multicastConfig);
		join.setTcpIpConfig(tcpIpConfig);

		return join;
	}

	@Bean
	public ClientNetworkConfig clientNetworkConfig() {

		HazelCast hazelCast = appProperties.getHazelCast();

		ClientNetworkConfig config = new ClientNetworkConfig();
		config.setAddresses(Arrays.asList(hazelCast.getClientMemeber()));

		return config;
	}
}
