package com.isg.config;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

import com.isg.config.AppProperties;

@Data
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "props")
public class AppProperties {

	@NestedConfigurationProperty
	private Jdbc jdbc;

	@NestedConfigurationProperty
	private HazelCast hazelCast;

	@NestedConfigurationProperty
	private FileProps fileProps;

	@NestedConfigurationProperty
	private PaymentUrlConfig paymentUrlConfig;

	@Data
	@NoArgsConstructor
	public static class Jdbc {
		private String driverClassName;
		private String url;
		private String username;
		private String password;
	}

	@Data
	@NoArgsConstructor
	public static class HazelCast {

		private String name;
		private String password;
		private String value;
		private int defaultPort;
		private String clientMemeber;
		private String mapConfigName;
	}

	@Data
	@NoArgsConstructor
	public static class FileProps {
		private String baseSystemPath;
		private String externalDocumentsPath;
		private String internalDocumentsPath;
		private String uploadCsvTemplatesPath;
	}

	@Data
	@NoArgsConstructor
	public static class PaymentUrlConfig {
		private String createSession;
		private String savePaymentDetail;
		private String paymentDetail;
		private String acknowledgePayment;
		private String branchSpecificSchemeList;
		private String gatewayConfigurationsBySchemeList;
		private String saveRefundFeePayment;
	}

	@Data
	@NoArgsConstructor
	public static class Rabbit {
		private String host;
		private int port;
		private String username;
		private String password;
		private String routingKey;
		private String queue;
	}
}