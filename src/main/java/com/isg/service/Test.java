package com.isg.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

public class Test {

	public static void main(String args[]) throws Exception {

		try {
			getPayloadData(null, false);
		} catch (Exception e) {
			System.out.println(e.getClass().getName());
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			System.out.println(Arrays.asList(e.getStackTrace()) + "\n\n");

			e.printStackTrace();
		}
	}

	private static HashMap getPayloadData(String data, boolean isVerification) throws Exception {

		if (data == null) {
			throw new Exception("DDDDDD", new Throwable("AAS"));
		}
		HashMap vDataMap = new HashMap();
		if (isVerification) {
			data = data.substring(data.indexOf("<getTransactionTokenReturn>") + 27, data.indexOf("</getTransactionTokenReturn>"));
			System.out.println(data);
		}
		try {
			String SRCITC_Row = data;
			//			SRCITC_Row = SRCITC_Row.substring(1, SRCITC_Row.length() - 1);

			StringTokenizer strTokenFrst = new StringTokenizer(SRCITC_Row, "|");
			while (strTokenFrst.hasMoreElements()) {
				Object pTokenValue = strTokenFrst.nextElement();
				StringTokenizer strTokenSec = new StringTokenizer(
								pTokenValue.toString(), "=");
				while (strTokenSec.hasMoreElements()) {
					Object pKeyValue = strTokenSec.nextElement();
					Object pStrValue = null;
					if (strTokenSec.hasMoreElements())
						pStrValue = strTokenSec.nextElement();
					else
						pStrValue = "NA";
					vDataMap.put(pKeyValue, pStrValue);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return vDataMap;
	}
}
