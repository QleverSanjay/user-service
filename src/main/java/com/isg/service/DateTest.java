package com.isg.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DateTest {

	static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String arg[]) throws ParseException {

		// System.out.println(getStartDate(0, 2016));
		// System.out.println(getEndDate(0, 2016));
		// System.out.println(getWeeksOfMonth(0, 2016).toString());

		/*SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
		Date date = parseFormat.parse("12:00 PM");
		System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date birthdate = formatter.parse("2016-01-01");
		SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
		System.out.println(formatter1.format(birthdate));
		System.out.println("Calendar.SUNDAY" + Calendar.SUNDAY);*/
		boolean skipHolidays = true;

		Map<Integer, String> timeTableData = new LinkedHashMap<Integer, String>();
		timeTableData.put(new Integer(Calendar.MONDAY), "Monday lecture");
		timeTableData.put(new Integer(Calendar.TUESDAY), "Tuesday lecture");
		timeTableData.put(new Integer(Calendar.WEDNESDAY), "Wednesday lecture");
		timeTableData.put(new Integer(Calendar.THURSDAY), "Thursday lecture");
		timeTableData.put(new Integer(Calendar.FRIDAY), "Friday lecture");
		timeTableData.put(new Integer(Calendar.SATURDAY), "Saturday lecture");

		Set<Integer> workingDaysOff = new HashSet<Integer>();
		//workingDaysOff.add(Calendar.MONDAY);
		//workingDaysOff.add(Calendar.TUESDAY);
		//workingDaysOff.add(Calendar.WEDNESDAY);
		//workingDaysOff.add(Calendar.THURSDAY);
		//workingDaysOff.add(Calendar.FRIDAY);
		workingDaysOff.add(Calendar.SATURDAY);
		workingDaysOff.add(Calendar.SUNDAY);

		List<String> holiday = new ArrayList<String>();
		holiday.addAll(getDatesBetweenDates(parseDate("2016-08-15"), parseDate("2016-08-15")));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-10-02"), parseDate("2016-10-02")));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-09-05"), parseDate("2016-09-15")));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-09-26"), parseDate("2016-09-26")));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-07-26"), parseDate("2016-07-26")));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-08-15"), parseDate("2016-08-15")));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-08-15"), parseDate("2016-08-15")));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-08-15"), parseDate("2016-08-15")));
		holiday.addAll(getDatesBetweenDates(parseDate("2017-01-26"), parseDate("2017-01-26")));

		generateTimeTable("2016-09-01", "2016-09-30", skipHolidays, timeTableData, workingDaysOff, holiday);

	}

	public static void generateTimeTable(String timeTableStartDate, String timeTableEndDate, boolean skipTimeTableOnHolidays,
					Map<Integer, String> timeTableData, Set<Integer> workingDaysOff, List<String> holiday) {

		List<String> dates = getDatesBetweenDates(parseDate(timeTableStartDate), parseDate(timeTableEndDate));

		//System.out.println(dates);
		//System.out.println(holiday);

		Integer currentDateIndex = 0;

		for (String dateObj : dates) {
			System.out.print("Date to be scanned is ::" + dateObj);

			Date date = parseDate(dateObj);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);

			int currentDateIndexChk = calendar.get(Calendar.DAY_OF_WEEK);

			if (currentDateIndex == 0 && skipTimeTableOnHolidays) {
				currentDateIndex = new Integer(calendar.get(Calendar.DAY_OF_WEEK));
			}

			System.out.print(" currentDateIndex :: " + currentDateIndex);
			if (holiday.contains(dateObj) && !skipTimeTableOnHolidays) {
				//System.out.print(" This is holiday and skipHolidays is false , do not increase index");
				//currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, data, skipHolidays);
			} else if (holiday.contains(dateObj) && skipTimeTableOnHolidays) {
				//System.out.print(" This is holiday and skipHolidays is true hence increase index and move to next day");
				currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
			} else if (isDayOff(calendar, workingDaysOff)) {
				System.out.print(" This is week-off / no data eneterd for day by user, do not increase index");
				currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
			} else if (timeTableData.containsKey(currentDateIndex) && timeTableData.containsKey(currentDateIndexChk) && skipTimeTableOnHolidays) {
				System.out.print(" Insert Data in DB for Today lecture is ::" + timeTableData.get(currentDateIndex));
				currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
			} else if (skipTimeTableOnHolidays) {
				currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
			} else if (!skipTimeTableOnHolidays && timeTableData.containsKey(currentDateIndexChk)) {

				if (currentDateIndex == 0) {
					currentDateIndex = currentDateIndexChk;
				} else {
					currentDateIndex = getRoundRobinDateIndex(currentDateIndex, timeTableData.keySet());
				}
				System.out.print(" currentDateIndex rb :: " + currentDateIndex);
				System.out.print(" Insert Data in DB for Today lecture is rr pattern ::" + timeTableData.get(currentDateIndex));
				//currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, data, skipHolidays);
			}

			System.out.println("");
			//System.out.println("Today date index ::: " + currentDateIndex);

		}
	}

	private static Integer getRoundRobinDateIndex(Integer currentDateIndex, Set<Integer> keys) {
		// TODO Auto-generated method stub
		Integer nextDateIndex = null;
		int keySetSize = keys.size();
		Integer[] keyAry = new Integer[keySetSize];
		keys.toArray(keyAry);
		//System.out.println("currentDateIndex >>" + currentDateIndex);
		for (Integer i = 0; i < keySetSize; i++) {

			Integer key = keyAry[i];
			//System.out.println(key);
			if (key.intValue() == currentDateIndex.intValue() && (i == (keySetSize - 1))) {
				nextDateIndex = keyAry[0];
				//System.out.println("nextDateIndex >>" + nextDateIndex);
				return nextDateIndex;
			} else if (key.intValue() == currentDateIndex.intValue()) {
				nextDateIndex = keyAry[i + 1];
				//System.out.println("nextDateIndex >>" + nextDateIndex);
				return nextDateIndex;
			}

		}

		return nextDateIndex;
	}

	private static int getDateIndex(Integer currentIndex, Set<Integer> workingDaysOff, Map<Integer, String> data, boolean skipHolidays) {

		Integer newIndex = currentIndex;

		if (skipHolidays) {
			newIndex = currentIndex + 1;
		}

		if (newIndex > 7) {
			newIndex = 1; //rest to Sunday
		}

		while (true && !skipHolidays) {
			if (workingDaysOff.contains(newIndex)) {
				newIndex = newIndex + 1;
				if (newIndex > 7) {
					newIndex = 1; //rest to Sunday
				}
			} else if (!data.containsKey(newIndex)) {
				newIndex = newIndex + 1;
				if (newIndex > 7) {
					newIndex = 1; //rest to Sunday
				}
			} else {
				break;
			}

		}

		return newIndex;

	}

	private static boolean isDayOff(Calendar calendar, Set<Integer> workingDaysOff) {

		//System.out.println("Day of week::" + calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
		return workingDaysOff.contains(calendar.get(Calendar.DAY_OF_WEEK));
	}

	public static Date parseDate(String dateStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {
			return formatter.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<String> getDatesBetweenDates(Date startdate, Date enddate)
	{
		List<String> dates = new ArrayList<String>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().compareTo(enddate) <= 0)
		{

			Date result = calendar.getTime();
			dates.add(formatter.format(result));

			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	public static Integer[] getWeeksOfMonth(int month, int year)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 1);

		Set<Integer> weeks = new HashSet<Integer>();
		int ndays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		System.out.println(ndays);
		for (int i = 0; i < ndays; i++)
		{
			weeks.add(cal.get(Calendar.WEEK_OF_YEAR));
			cal.add(Calendar.DATE, 1);
		}
		System.out.println(weeks.toString());
		return weeks.toArray(new Integer[0]);
	}

	static String getStartDate(int month, int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 1);

		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
		return displayFormat.format(cal.getTime());

	}

	static String getEndDate(int month, int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		// Getting Maximum day for Given Month
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, maxDay);
		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
		return displayFormat.format(cal.getTime());

	}

}
