package com.isg.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileNIOTest {

	public static void main(String args[]) throws IOException {
		final Path tmp = Paths.get("c:/Temp/1/2");
		if (tmp != null) // null will be returned if the path has no parent
			Files.createDirectories(tmp);
	}

}
