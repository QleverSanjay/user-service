package com.isg.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import org.springframework.util.Base64Utils;

/**
 * @author JavaDigest
 * 
 */
public class EncyptDecrypt {

	static final String PUBLICKEY_PREFIX = "-----BEGIN PUBLIC KEY-----";
	static final String PUBLICKEY_POSTFIX = "-----END PUBLIC KEY-----";
	static final String PRIVATEKEY_PREFIX = "-----BEGIN RSA PRIVATE KEY-----";
	static final String PRIVATEKEY_POSTFIX = "-----END RSA PRIVATE KEY-----";

	/**
	 * String to hold name of the encryption algorithm.
	 */
	public static final String ALGORITHM = "RSA";

	/**
	 * String to hold the name of the private key file.
	 */
	public static final String PRIVATE_KEY_FILE = "C:/Temp/keys/private.key";

	/**
	 * String to hold name of the public key file.
	 */
	public static final String PUBLIC_KEY_FILE = "C:/Temp/keys/public.key";

	public static final String PUBLIC_KEY_FILE_PEM = "C:/Temp/keys/public.pem";

	/**
	 * Generate key which contains a pair of private and public key using 1024
	 * bytes. Store the set of keys in Prvate.key and Public.key files.
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void generateKey() {
		try {
			final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
			keyGen.initialize(6144);
			final KeyPair key = keyGen.generateKeyPair();
			PublicKey publicKey = key.getPublic();
			PrivateKey privateKey = key.getPrivate();

			File privateKeyFile = new File(PRIVATE_KEY_FILE);
			File publicKeyFile = new File(PUBLIC_KEY_FILE);

			// Create files to store public and private key
			if (privateKeyFile.getParentFile() != null) {
				privateKeyFile.getParentFile().mkdirs();
			}
			privateKeyFile.createNewFile();

			if (publicKeyFile.getParentFile() != null) {
				publicKeyFile.getParentFile().mkdirs();
			}
			publicKeyFile.createNewFile();

			// Saving the Public key in a file
			ObjectOutputStream publicKeyOS = new ObjectOutputStream(
							new FileOutputStream(publicKeyFile));
			publicKeyOS.writeObject(publicKey);
			publicKeyOS.close();

			// Saving the Private key in a file
			ObjectOutputStream privateKeyOS = new ObjectOutputStream(
							new FileOutputStream(privateKeyFile));
			privateKeyOS.writeObject(privateKey);
			privateKeyOS.close();

			String publicKeyPEM = PUBLICKEY_PREFIX + "\n" + DatatypeConverter.printBase64Binary(publicKey.getEncoded()).replaceAll("(.{64})", "$1\n") + "\n"
							+ PUBLICKEY_POSTFIX;

			System.out.println("Public PEM: " + "\n" + publicKeyPEM);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * The method checks if the pair of public and private key has been
	 * generated.
	 * 
	 * @return flag indicating if the pair of keys were generated.
	 */
	public static boolean areKeysPresent() {

		File privateKey = new File(PRIVATE_KEY_FILE);
		File publicKey = new File(PUBLIC_KEY_FILE);

		if (privateKey.exists() && publicKey.exists()) {
			return true;
		}
		return false;
	}

	/**
	 * Encrypt the plain text using public key.
	 * 
	 * @param text
	 *            : original plain text
	 * @param key
	 *            :The public key
	 * @return Encrypted text
	 * @throws java.lang.Exception
	 */
	public static byte[] encrypt(String text, PublicKey key) {
		byte[] cipherText = null;
		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(ALGORITHM);
			// encrypt the plain text using the public key
			cipher.init(Cipher.ENCRYPT_MODE, key);

			byte[] bytes = text.getBytes("UTF-8");

			cipherText = cipher.doFinal(text.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cipherText;
	}

	/**
	 * Decrypt text using private key.
	 * 
	 * @param text
	 *            :encrypted text
	 * @param key
	 *            :The private key
	 * @return plain text
	 * @throws java.lang.Exception
	 */
	public static String decrypt(byte[] text, PrivateKey key) {
		byte[] dectyptedText = null;
		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(ALGORITHM);

			// decrypt the text using the private key
			cipher.init(Cipher.DECRYPT_MODE, key);
			dectyptedText = cipher.doFinal(text);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return new String(dectyptedText);
	}

	public static void main1(String[] args) {

		//generateKey();
	}

	/**
	 * Test the EncryptionUtil
	 */
	public static void main(String[] args) {

		try {

			// generateKey();

			// Check if the pair of keys are present else generate those.
			// if (!areKeysPresent()) {
			// Method generates a pair of keys using the RSA algorithm and
			// stores it
			// in their respective files
			// generateKey();
			// }

			String originalText = "Text to be encrypted ";
			ObjectInputStream inputStream = null;

			/*AcknowledgePaymentProcessedRequest acknowledgePaymentProcessedRequest = new AcknowledgePaymentProcessedRequest();
			acknowledgePaymentProcessedRequest.setAmount(1000);
			// acknowledgePaymentProcessedRequest.setChannel(channel)
			acknowledgePaymentProcessedRequest.setCustomerEmail("vishal.samant@v2stech.com");
			acknowledgePaymentProcessedRequest.setCustomerName("Vishal Samant");
			acknowledgePaymentProcessedRequest.setInternetHandlingCharges(10);
			acknowledgePaymentProcessedRequest.setShippingCharges(10);
			acknowledgePaymentProcessedRequest.setPaymentGatewayConfigurationId(1);

			List<PaymentDetailTaxComponent> taxList = new ArrayList<PaymentDetailTaxComponent>();
			PaymentDetailTaxComponent paymentDetailTaxComponent = new PaymentDetailTaxComponent();
			paymentDetailTaxComponent.setCode("SERVICE_TAX");
			paymentDetailTaxComponent.setAmount(114.5f);
			taxList.add(paymentDetailTaxComponent);

			paymentDetailTaxComponent = new PaymentDetailTaxComponent();
			paymentDetailTaxComponent.setCode("SERVICE_TAX");
			paymentDetailTaxComponent.setAmount(114.5f);
			taxList.add(paymentDetailTaxComponent);

			paymentDetailTaxComponent = new PaymentDetailTaxComponent();
			paymentDetailTaxComponent.setCode("SERVICE_TAX");
			paymentDetailTaxComponent.setAmount(114.5f);
			taxList.add(paymentDetailTaxComponent);

			paymentDetailTaxComponent = new PaymentDetailTaxComponent();
			paymentDetailTaxComponent.setCode("SERVICE_TAX");
			paymentDetailTaxComponent.setAmount(114.5f);
			taxList.add(paymentDetailTaxComponent);
			acknowledgePaymentProcessedRequest.setPaymentDetailTaxComponent(taxList);

			try {
				ObjectMapper mapper = new ObjectMapper();
				originalText = mapper.writeValueAsString(acknowledgePaymentProcessedRequest);
				System.out.println(originalText);
			} catch (Exception e) {
				e.printStackTrace();
			}
			*/
			// Encrypt the string using the public key
			inputStream = new ObjectInputStream(new FileInputStream(PUBLIC_KEY_FILE));
			final PublicKey publicKey = (PublicKey) inputStream.readObject();
			final byte[] cipherText = encrypt(originalText, publicKey);

			String encoded = Base64Utils.encodeToString(cipherText);
			System.out.println("Encrypted:" + new String(encoded)); // Outputs
																	// "SGVsbG8="

			byte[] decoded = Base64Utils.decode(encoded.getBytes());
			// println(new String(decoded)) // Outputs "Hello"

			// Decrypt the cipher text using the private key.
			inputStream = new ObjectInputStream(new FileInputStream(PRIVATE_KEY_FILE));
			final PrivateKey privateKey = (PrivateKey) inputStream.readObject();
			final String plainText = decrypt(decoded, privateKey);

			// Printing the Original, Encrypted and Decrypted Text
			System.out.println("Original: " + originalText);
			System.out.println("Encrypted: " + cipherText.toString());

			System.out.println(Arrays.toString(cipherText));
			System.out.println("");
			System.out.println("Decrypted: " + plainText);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
