package com.isg.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageValidator {

	private final Pattern pattern;
	private Matcher matcher;

	private static final String IMAGE_PATTERN =
					"([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";

	public ImageValidator() {
		pattern = Pattern.compile(IMAGE_PATTERN);
	}

	/**
	 * Validate image with regular expression
	 * 
	 * @param image
	 *            image for validation
	 * @return true valid image, false invalid image
	 */
	public boolean validate(final String image) {

		matcher = pattern.matcher(image);
		return matcher.matches();

	}

	public String getFileExtension(final String fileNameWithExtension) {
		return fileNameWithExtension.substring(fileNameWithExtension.indexOf(".") + 1, fileNameWithExtension.length());
	}

	public static void main(String[] args) {
		ImageValidator imageValidator = new ImageValidator();
		System.out.println(imageValidator.validate("a.jpg"));
		System.out.println(imageValidator.getFileExtension("a.jpg"));

	}

}
