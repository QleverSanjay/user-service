package com.isg.service.user.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.util.Base64Utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.EncyptDecrypt;
import com.isg.service.user.client.payment.request.AcknowledgePaymentProcessedRequest;
import com.isg.service.user.model.PaymentDetailTaxComponent;

public class EncryptionDecryptionUtil {

	private static final String RSA_ALGORITHM = "RSA";

	private static final String AES_ALGORITHM = "AES";

	public static final String PRIVATE_KEY_FILE = "./private.key";

	// static Cipher cipher;

	public static void main1(String[] args) throws Exception {
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		SecretKey secretKey = keyGenerator.generateKey();

		// cipher = Cipher.getInstance("AES");

		String plainText = "";

		AcknowledgePaymentProcessedRequest acknowledgePaymentProcessedRequest =
						new AcknowledgePaymentProcessedRequest();
		acknowledgePaymentProcessedRequest.setAmount(1000); //
		acknowledgePaymentProcessedRequest.setChannel("");
		acknowledgePaymentProcessedRequest.setCustomerEmail("vikas@test.com");
		acknowledgePaymentProcessedRequest.setCustomerName("Vikas Kale");
		acknowledgePaymentProcessedRequest.setInternetHandlingCharges(10);
		acknowledgePaymentProcessedRequest.setShippingCharges(10);
		acknowledgePaymentProcessedRequest.setTotalAmount(1150);
		acknowledgePaymentProcessedRequest.setPaymentGatewayConfigurationId(1);

		List<PaymentDetailTaxComponent> taxList = new
						ArrayList<PaymentDetailTaxComponent>();
		PaymentDetailTaxComponent paymentDetailTaxComponent = new PaymentDetailTaxComponent();
		paymentDetailTaxComponent.setCode("SERVICE_TAX");
		paymentDetailTaxComponent.setAmount(114.5f);
		taxList.add(paymentDetailTaxComponent);

		paymentDetailTaxComponent = new PaymentDetailTaxComponent();
		paymentDetailTaxComponent.setCode("SERVICE_TAX");
		paymentDetailTaxComponent.setAmount(114.5f);
		taxList.add(paymentDetailTaxComponent);

		paymentDetailTaxComponent = new PaymentDetailTaxComponent();
		paymentDetailTaxComponent.setCode("SERVICE_TAX");
		paymentDetailTaxComponent.setAmount(114.5f);
		taxList.add(paymentDetailTaxComponent);

		paymentDetailTaxComponent = new PaymentDetailTaxComponent();
		paymentDetailTaxComponent.setCode("SERVICE_TAX");
		paymentDetailTaxComponent.setAmount(114.5f);
		taxList.add(paymentDetailTaxComponent);
		acknowledgePaymentProcessedRequest.setPaymentDetailTaxComponent(taxList);

		try {
			ObjectMapper mapper = new ObjectMapper();
			plainText = mapper.writeValueAsString(acknowledgePaymentProcessedRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Plain Text Before Encryption: " + plainText);

		String encryptedText = encrypt(plainText, secretKey);
		System.out.println("Encrypted Text After Encryption: " + encryptedText);

		ObjectInputStream inputStream = null; // Encrypt the string using the

		inputStream = new ObjectInputStream(new FileInputStream(EncyptDecrypt.PUBLIC_KEY_FILE));
		final PublicKey publicKey = (PublicKey) inputStream.readObject();
		final byte[] cipherText = encryptSecretKey(secretKey.getEncoded(), publicKey);
		String encoded = Base64Utils.encodeToString(cipherText);
		System.out.println("Encrypted secret key:" + new String(encoded));

		byte[] decoded = Base64Utils.decode(encoded.getBytes());
		//println(newtring(decoded)) // Outputs "Hello"

		// Decrypt the cipher text using the private key. 
		inputStream = new ObjectInputStream(new FileInputStream(EncyptDecrypt.PRIVATE_KEY_FILE));
		final PrivateKey privateKey = (PrivateKey) inputStream.readObject();
		secretKey = decryptSecretKey(decoded, privateKey);

		// Temp String decryptedText = decrypt(encryptedText, secretKey);
		//System.out.println("Decrypted Text After Decryption: " + decryptedText);

	}

	public static void main(String[] args) throws Exception {

		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(128);
		SecretKey secretKey = keyGenerator.generateKey();

		String encryptedPlainText = encrypt("Encrypit me", secretKey);
		// Decrypt the cipher text using the private key.
		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(EncyptDecrypt.PUBLIC_KEY_FILE));
		final PublicKey publicKey = (PublicKey) inputStream.readObject();

		//final PublicKey publicKey = loadPublicKey();

		byte[] encryptedSecretKeyBytes = encryptSecretKey(secretKey.getEncoded(), publicKey);
		String encodedSecretKey = Base64Utils.encodeToString(encryptedSecretKeyBytes);

		String encoded = "SlvqB/hWqE7m6FFETkiXY8IuUbJw4P9BJfteXODl8XY=";
		String encryptedText = encryptedPlainText;

		byte[] decoded = Base64Utils.decode(encoded.getBytes());

		// Decrypt the cipher text using the private key.
		inputStream = new ObjectInputStream(new FileInputStream(EncyptDecrypt.PRIVATE_KEY_FILE));
		final PrivateKey privateKey = (PrivateKey) inputStream.readObject();
		SecretKey secretKey1 = decryptSecretKey(decoded, privateKey);

		// Temp
		String decryptedText = decrypt(encryptedText, secretKey1);
		System.out.println("Decrypted Text : " + decryptedText);
	}

	public static String encrypt(String plainText, SecretKey secretKey)
					throws Exception {
		byte[] plainTextByte = plainText.getBytes();
		Cipher cipher = Cipher.getInstance(AES_ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] encryptedByte = cipher.doFinal(plainTextByte);
		BASE64Encoder encoder = new BASE64Encoder();
		String encryptedText = encoder.encode(encryptedByte);
		return encryptedText;
	}

	public static String decrypt(String encryptedText, SecretKey secretKey)
					throws Exception {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] encryptedTextByte = decoder.decodeBuffer(encryptedText);
		Cipher cipher = Cipher.getInstance(AES_ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		return decryptedText;
	}

	public static byte[] encryptSecretKey(byte[] secretKeyBytes, PublicKey key) {
		byte[] cipherText = null;
		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
			// encrypt the plain text using the public key
			cipher.init(Cipher.ENCRYPT_MODE, key);

			cipherText = cipher.doFinal(secretKeyBytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cipherText;
	}

	public static SecretKey decryptSecretKey(byte[] secretKeyBytes, PrivateKey key) {
		byte[] decodedKey = null;
		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);

			// decrypt the text using the private key
			cipher.init(Cipher.DECRYPT_MODE, key);
			decodedKey = cipher.doFinal(secretKeyBytes);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, AES_ALGORITHM);
		return originalKey;
	}

	public static PrivateKey loadPrivateKey(String fileName)
					throws IOException, GeneralSecurityException {
		PrivateKey key = null;
		InputStream is = null;
		try {
			is = fileName.getClass().getResourceAsStream("/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder builder = new StringBuilder();
			boolean inKey = false;
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				if (!inKey) {
					if (line.startsWith("-----BEGIN ") &&
									line.endsWith(" PRIVATE KEY-----")) {
						inKey = true;
					}
					continue;
				}
				else {
					if (line.startsWith("-----END ") &&
									line.endsWith(" PRIVATE KEY-----")) {
						inKey = false;
						break;
					}
					builder.append(line);
				}
			}
			//
			byte[] encoded = DatatypeConverter.parseBase64Binary(builder.toString());
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			key = kf.generatePrivate(keySpec);
		} finally {
			closeSilent(is);
		}
		return key;
	}

	public static PublicKey loadPublicKey()
					throws IOException, GeneralSecurityException {
		PublicKey key = null;
		InputStream is = null;
		try {
			//is = fileName.getClass().getResourceAsStream("/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(EncyptDecrypt.PUBLIC_KEY_FILE_PEM)));
			StringBuilder builder = new StringBuilder();
			boolean inKey = false;
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				if (!inKey) {
					if (line.startsWith("-----BEGIN ") &&
									line.endsWith(" PUBLIC KEY-----")) {
						inKey = true;
					}
					continue;
				}
				else {
					if (line.startsWith("-----END ") &&
									line.endsWith(" PUBLIC KEY-----")) {
						inKey = false;
						break;
					}
					builder.append(line);
				}
			}
			//
			byte[] encoded = DatatypeConverter.parseBase64Binary(builder.toString());
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			key = kf.generatePublic(keySpec);
		} finally {
			closeSilent(is);
		}
		return key;
	}

	public static void closeSilent(final InputStream is) {
		if (is == null)
			return;
		try {
			is.close();
		} catch (Exception ign) {
		}
	}
}
