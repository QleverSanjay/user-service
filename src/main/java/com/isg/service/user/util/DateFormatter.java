package com.isg.service.user.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateFormatter {

	public static final String DATE_FORMAT_DDMMYYYY_HYPHEN_SEPARATED = "dd-MM-yyyy";

	public static final String DATE_FORMAT_YYYYMMDD_HYPHEN_SEPARATED = "yyyy-MM-dd";

	public static final String DATE_FORMAT_YYYYMMDDHHMM_HYPHEN_SEPARATED = "yyyy-MM-dd HH:mm";

	public static final String DATE_FORMAT_DDMMYYYY_FORWARD_SLASH_SEPARATED = "dd/MM/yyyy";

	public static final String DATE_FORMAT_YYYYMMDDTHHMMSS_HYPHEN_SEPARATED = "yyyy-MM-dd'T'HH:mm:ss:SSS'Z'";

	public static String formatDate(String dateStr, String existingDateFormat, String desiredDateFormat) {

		SimpleDateFormat newformat = new SimpleDateFormat(desiredDateFormat);
		SimpleDateFormat oldformat = new SimpleDateFormat(existingDateFormat);
		String reformattedStr = "";
		try {
			reformattedStr = newformat.format(oldformat.parse(dateStr));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return reformattedStr;
	}

	public static String getSystemDateTime(String desiredDateFormat) {
		java.util.Date dateUtil = new java.util.Date();
		return new SimpleDateFormat(desiredDateFormat).format(dateUtil);
	}

	public static void main(String args[]) throws ParseException {
		System.out.println(formatDate("2005-05-20T18:30:00.000Z", DATE_FORMAT_YYYYMMDDTHHMMSS_HYPHEN_SEPARATED, DATE_FORMAT_DDMMYYYY_FORWARD_SLASH_SEPARATED));

		String string = "2005-05-20T18:30:00.000Z";
		String defaultTimezone = TimeZone.getDefault().getID();
		Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(string.replaceAll("Z$", "+0000"));

		System.out.println("string: " + string);
		System.out.println("defaultTimezone: " + defaultTimezone);
		System.out.println("date: " + (new SimpleDateFormat("dd/MM/yyyy")).format(date));
	}
}
