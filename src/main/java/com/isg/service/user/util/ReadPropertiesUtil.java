package com.isg.service.user.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadPropertiesUtil {

	public static Properties properties;

	static {
		properties = new Properties();
		InputStream inputStream = ReadPropertiesUtil.class.getClassLoader()
				.getResourceAsStream("/application.properties");
		try {
			properties.load(inputStream);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
