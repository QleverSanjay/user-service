package com.isg.service.user.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.StringUtils;

public class SmsConnexionUtil {

	public static String sendSMS(String msg, String phone) {

		List<NameValuePair> dataList = new ArrayList<>();

		if (!StringUtils.isEmpty(phone)) {
			phone = "91" + phone;
		}

		dataList.add(new BasicNameValuePair("action", "send"));
		dataList.add(new BasicNameValuePair("username", "Qfixinfo"));
		dataList.add(new BasicNameValuePair("passphrase", "123456"));
		dataList.add(new BasicNameValuePair("message", msg));
		dataList.add(new BasicNameValuePair("phone", phone));

		if (!StringUtils.isEmpty("EduQfix")) {
			dataList.add(new BasicNameValuePair("from", "EduQfix"));
		}

		StringBuffer sbuffer = new StringBuffer();

		HttpClient client = HttpClientBuilder.create().build();

		// Form SMS Request
		HttpPost httppost = new HttpPost("http://smsc.smsconnexion.com/api/gateway.aspx");
		try {
			httppost.setEntity(new UrlEncodedFormEntity(dataList, "UTF-8"));
			HttpResponse response = client.execute(httppost);

			int returnCode = response.getStatusLine().getStatusCode();

			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
				System.err.println("The Post method is not implemented by this URI");
			} else {
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String readLine;
				while (((readLine = br.readLine()) != null)) {
					sbuffer.append(readLine);
				}
			}
			System.out.println("************** SMS GWAY RESPONSE **************\n" + sbuffer.toString());
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {

		}

		return sbuffer.toString();
	}

	public String sendMultipleSMS(String msg, List<String> phoneList) {
		String phoneNumbers = "";
		if (phoneList != null) {
			for (int i = 0; i < phoneList.size(); i++) {
				String phone = phoneList.get(i);
				phoneNumbers += "91" + phone + (i < (phoneList.size() - 1) ? "," : "");
			}
		}
		return sendSMS(msg, phoneNumbers);
	}

}
