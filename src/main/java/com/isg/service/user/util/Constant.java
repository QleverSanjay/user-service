package com.isg.service.user.util;

import java.text.SimpleDateFormat;

import com.isg.service.user.exception.ApplicationException;

public final class Constant {

	public static String EDUQFIX_DOMAIN = "eduqfix.com";

	public static final String CHAT_PROFILE_APP_NAME = "Qfixinfo";

	public static String PASSWORD_NOT_APPLICABLE = "NOT_APPLICABLE";
	public static Integer ROLE_ID_STUDENT = 2;

	public static final SimpleDateFormat DATABASE_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");

	public static final SimpleDateFormat DATABASE_TIME_FORMAT = new SimpleDateFormat(
			"HH:mm:ss");
	
	
	
	public static enum PaymentStatus {
		UNPAID("UNPAID"), SUCCESS("SUCCESS"), PAID("PAID"), PARTIAL_PAID("PARTIAL_PAID");

		private final String code;

		private PaymentStatus(String code) {
			this.code = code;
		}

		public String get() {
			return code;
		}
	}

	public static final SimpleDateFormat USER_DATE_FORMAT = new SimpleDateFormat(
					"dd/MM/yyyy");

	public static enum PaymentAccountType {
		BANK("BANK"), QFIXPAY("QFIXPAY"), PAYPAL("PAYPAL");

		private final String code;

		private PaymentAccountType(String code) {
			this.code = code;
		}

		public String get() {
			return code;
		}
	}

	public static enum SOCIAL_PROFILE_TYPE {

		FACEBOOK("FACEBOOK"), GOOGLE("GOOGLE");

		private final String provider;

		private SOCIAL_PROFILE_TYPE(String provider) {
			this.provider = provider;
		}

		public String get() {
			return this.provider;
		}

		public String getTypeByProvider() {

			switch (get()) {
			case "FACEBOOK":
				return SOCIAL_PROFILE_TYPE.FACEBOOK.get();
			case "GOOGLE":
				return SOCIAL_PROFILE_TYPE.GOOGLE.get();
			default:
				throw new ApplicationException("Invalid provider specified.");
			}
		}

	}

	public static enum AUTHENTICATION_TYPE {

		SOCIAL("SOCIAL"), APPLICATION("APPLICATION");

		private final String type;

		private AUTHENTICATION_TYPE(String type) {
			this.type = type;
		}

		public String get() {
			return this.type;
		}

	}

	public static enum USER_STATUS {

		NEW("NEW_REGISTRATION"), EXISTING("EXISTING_USER");

		private final String status;

		private USER_STATUS(String status) {
			this.status = status;
		}

		public String get() {
			return this.status;
		}

	}

	public static enum PASSBOOK_TRANSACTION_TYPE {

		DEBIT("DEBIT"), CREDIT("CREDIT");

		private final String type;

		private PASSBOOK_TRANSACTION_TYPE(String type) {
			this.type = type;
		}

		public String get() {
			return this.type;
		}

	}

	public static enum NotificationTemplateCode {
		CREATE_EDIARY("CREATE_EDIARY"), CREATE_NOTICE("CREATE_NOTICE");

		private final String code;

		private NotificationTemplateCode(String code) {
			this.code = code;
		}

		public String get() {
			return code;
		}
	}

	public static enum EmailTemplate {
		PARENT_CREATE_EMAIL("USER_CREATE_EMAIL_PARENT"), REGISTRATION_OTP_EMAIL("REGISTRATION_OTP_EMAIL"), FORGOT_PASSWORD_EMAIL(
						"FORGOT_PASSWORD_EMAIL_PARENT"), INVITE_PARENT_EMAIL("INVITE_PARENT_EMAIL"),
		ADMISSION_SUCCESS_EMAIL("ADMISSION_SUCCESS_EMAIL"), ADMISSION_SUCCESS_SMS("ADMISSION_SUCCESS_SMS"),
		SERVICE_REQUEST_PARENT("SERVICE_REQUEST_PARENT"), SERVICE_REQUEST_STUDENT("SERVICE_REQUEST_STUDENT"),
		SERVICE_REQUEST_RESPONSE_STUDENT("SERVICE_REQUEST_RESPONSE_STUDENT"),
		SERVICE_REQUEST_RESPONSE_PARENT("SERVICE_REQUEST_RESPONSE_PARENT"), ADMISSION_FORM_RECEIPT_EMAIL("ADMISSION_FORM_RECEIPT_EMAIL"),
		PAYMENT_SUCCESS_EMAIL("PAYMENT_SUCCESS_EMAIL"), GENERATE_OTP_EMAIL("GENERATE_OTP_EMAIL");

		private final String code;

		private EmailTemplate(String code) {
			this.code = code;
		}

		public String get() {
			return code;
		}
	}

	public static enum SmsTemplate {
		PARENT_CREATE_SMS("USER_CREATE_SMS_PARENT"), REGISTRATION_OTP_SMS("REGISTRATION_OTP_SMS"), FORGOT_PASSWORD_SMS(
						"FORGOT_PASSWORD_SMS_PARENT"), INVITE_PARENT_SMS("INVITE_PARENT_SMS"), PAYMENT_SUCCESS_SMS("PAYMENT_SUCCESS_SMS"),
						GENERATE_OTP_SMS("GENERATE_OTP_SMS");

		private final String code;

		private SmsTemplate(String code) {
			this.code = code;
		}

		public String get() {
			return code;
		}
	}

	

	public static enum DeviceType {
		ANDROID("ANDROID"), IOS("IOS");

		private final String code;

		private DeviceType(String code) {
			this.code = code;
		}

		public String get() {
			return code;
		}
	}

	public static enum EventCategory {
		PERSONAL_TODO("PERSONAL_TODO"), SCHOOL_SENT("SCHOOL_SENT");

		private final String code;

		private EventCategory(String code) {
			this.code = code;
		}

		public String get() {
			return code;
		}
	}

	public static enum NotificationCategory {
		NEWS("NEWS"), ALERT("ALERT"), ASSIGNMENT("ASSIGNMENT");

		private final String code;

		private NotificationCategory(String code) {
			this.code = code;
		}

		public String get() {
			return code;
		}
	}

}
