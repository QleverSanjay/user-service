package com.isg.service.user.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.exception.InvalidLoginCredentialsException;
import com.isg.service.user.model.ErrorDetail;
import com.isg.service.user.model.ErrorResource;
import com.isg.service.user.model.FieldErrorResource;

/**
 * The exceptions below could be raised by any controller and they would be
 * handled here, if not handled in the controller already.
 * 
 */

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger log = Logger.getLogger(GlobalExceptionHandler.class);

	public GlobalExceptionHandler() {

	}

	@ExceptionHandler({ AuthenticationException.class })
	protected ResponseEntity<Object> handleInvalidRequest(AuthenticationException e, WebRequest request) {

		ErrorResource error = new ErrorResource("SERVICE_ERROR", "System can not process your request for security reasons.");
		log.error("", e);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return handleExceptionInternal(e, error, headers, HttpStatus.UNAUTHORIZED, request);
	}

	@ExceptionHandler({ InvalidLoginCredentialsException.class })
	protected ResponseEntity<Object> handleInvalidLoginCredentials(InvalidLoginCredentialsException e, WebRequest request) {

		System.out.println("Handling UnauthorizedAccessException::::::::::::::");
		ErrorResource error = new ErrorResource("SERVICE_ERROR", "Invalid username / password specified. Try again.");
		log.error("", e);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return handleExceptionInternal(e, error, headers, HttpStatus.UNAUTHORIZED, request);
	}

	@ExceptionHandler({ AuthorisationException.class })
	protected ResponseEntity<Object> handleInvalidRequest(AuthorisationException e, WebRequest request) {

		ErrorResource error = new ErrorResource("SERVICE_ERROR", "System can not process your request for security reasons.");
		log.error("", e);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return handleExceptionInternal(e, error, headers, HttpStatus.FORBIDDEN, request);
	}

	@ExceptionHandler({ ApplicationException.class })
	protected ResponseEntity<Object> handleInvalidRequest(RuntimeException e, WebRequest request) {
		ApplicationException ae = (ApplicationException) e;
		List<FieldErrorResource> fieldErrorResources = new ArrayList<FieldErrorResource>();
		log.error("", e);
		List<ErrorDetail> errorDetails = ae.getErrors();
		for (ErrorDetail errorDetail : errorDetails) {
			FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setCode(errorDetail.getCode());
			fieldErrorResource.setMessage(errorDetail.getDescription());
		}

		ErrorResource error = new ErrorResource("APPLICATION_ERROR", ae.getMessage());
		error.setFieldErrors(fieldErrorResources);

		if (ae.isThrowableSet()) {
			ae.printStackTrace();
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return handleExceptionInternal(e, error, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}

	@ExceptionHandler({ Exception.class })
	protected ResponseEntity<Object> handleInvalidRequest(Exception e, WebRequest request) {

		ErrorResource error = new ErrorResource("SERVICE_ERROR", "System can not process your request.");
		log.error("", e);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return handleExceptionInternal(e, error, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

}
