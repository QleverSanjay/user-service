package com.isg.service.user.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.RandomStringUtils;

public class FileStorePathAssembler {

	// TODO load from external properties.

	public static final String QFIX_FILE_FOLDER_BASE_PATH = "/var/www/html";
//	public static final String QFIX_FILE_FOLDER_BASE_PATH = "C:/temp/";
	private static final String QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH = "/images/Qfix/documents/External";
	public static final String PATH_SEPARATOR = "/";

	private static final String NOTICE_FOLDER_NAME = "notice";
	private static final String PROFILE_FOLDER_NAME = "profiles";
	private static final String EDIARY_FOLDER_NAME = "ediary";
	private static final String REGISTRATION_FOLDER_NAME = "r12r12gistra123ion";

	public static final String NO_PROFILE_IMAGE_RELATIVE_PATH = QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH + PATH_SEPARATOR + PROFILE_FOLDER_NAME
					+ PATH_SEPARATOR + "no-profile.png";

	public static final String QFIX_LOGO_IMAGE_RELATIVE_PATH = QFIX_FILE_FOLDER_BASE_PATH + PATH_SEPARATOR + "images"
					+ PATH_SEPARATOR + "qfix-logo.png";

	public static final String CHECKBOX_IMAGE_RELATIVE_PATH = QFIX_FILE_FOLDER_BASE_PATH + PATH_SEPARATOR + "images"
					+ PATH_SEPARATOR + "checkbox.png";

	public static final String QFIX_ONLINE_REGISTRATION_RELATIVE_PATH = QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH + PATH_SEPARATOR
					+ REGISTRATION_FOLDER_NAME;

	/*public static final String IOS_CERTIFICATE_RELATIVE_PATH = QFIX_FILE_FOLDER_BASE_PATH + PATH_SEPARATOR + QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH
					+ PATH_SEPARATOR + "qfix_production_push_notification.p12";*/

	public static enum PROFILE_FOLDER_TYPE {
		STUDENT_PROFILE("students"), PARENT_PROFILE("parents"), TEACHER_PROFILE("teachers"), VENDOR_PROFILE("vendors");

		private final String folderName;

		private PROFILE_FOLDER_TYPE(String folderName) {
			this.folderName = folderName;
		}

		public String getProfileFolderName() {
			return this.folderName;
		}
	}

	public static final String IMAGE_PATTERN = "((\\.(?i)(jpg|jpeg|png|gif|bmp))$)";

	public static final String ATTACHEMENT_PATTERN = "((\\.(?i)(zip|doc|docx|xls|xlsx|jpg|jpeg|png|gif|bmp|txt|pdf))$)";

	public static String getOnlineRegistrationFormDocBaseRelativePath(Integer branchId) {
		return QFIX_ONLINE_REGISTRATION_RELATIVE_PATH + getBranchPath(branchId) + PATH_SEPARATOR;
	}

	public static String getNoticeStoragePath(Integer instituteId, Integer branchId) {
		return QFIX_FILE_FOLDER_BASE_PATH + QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH + getInstitutePath(instituteId) + getBranchPath(branchId)
						+ PATH_SEPARATOR + NOTICE_FOLDER_NAME;
	}

	public static String getNoticeRelativePath(Integer instituteId, Integer branchId) {
		return QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH + getInstitutePath(instituteId) + getBranchPath(branchId)
						+ PATH_SEPARATOR + NOTICE_FOLDER_NAME;
	}

	public static String getEdiaryStoragePath(Integer instituteId, Integer branchId) {
		return QFIX_FILE_FOLDER_BASE_PATH + QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH + getInstitutePath(instituteId) + getBranchPath(branchId)
						+ PATH_SEPARATOR + EDIARY_FOLDER_NAME;
	}

	public static String getEdiaryRelativePath(Integer instituteId, Integer branchId) {
		return QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH + getInstitutePath(instituteId) + getBranchPath(branchId)
						+ PATH_SEPARATOR + EDIARY_FOLDER_NAME;
	}

	public static String getProfileStoragePath(Integer instituteId, Integer branchId, PROFILE_FOLDER_TYPE profileFolderType) {
		return QFIX_FILE_FOLDER_BASE_PATH + QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH + getInstitutePath(instituteId) + getBranchPath(branchId)
						+ PATH_SEPARATOR + PROFILE_FOLDER_NAME + PATH_SEPARATOR + profileFolderType.getProfileFolderName();
	}

	public static String getProfileStorageRelativePath(Integer instituteId, Integer branchId, PROFILE_FOLDER_TYPE profileFolderType) {
		return QFIX_EXTERNAL_STORE_FOLDER_RELATIVE_PATH + getInstitutePath(instituteId) + getBranchPath(branchId)
						+ PATH_SEPARATOR + PROFILE_FOLDER_NAME + PATH_SEPARATOR + profileFolderType.getProfileFolderName();
	}

	public static String getFileFolderBasePath() {
		return QFIX_FILE_FOLDER_BASE_PATH;
	}

	public static String getFileExtension(final String fileNameWithExtension) {
		return fileNameWithExtension.substring(fileNameWithExtension.indexOf(".") + 1, fileNameWithExtension.length());
	}

	public static boolean isValidImageExtension(final String pattern, final String fileNameWithExtension) {
		String fileNameWithExtensionFiltered = fileNameWithExtension.replaceAll(" ", "");
		Pattern p = Pattern.compile(pattern);
		Matcher matcher = p.matcher(fileNameWithExtensionFiltered);
		return matcher.matches();
	}

	public static String getRandomFileName(final String fileNameWithExtension) {
		return RandomStringUtils.random(20, true, true) + "." + getFileExtension(fileNameWithExtension);
	}

	private static String getInstitutePath(Integer instituteId) {
		return (instituteId == null) ? "" : (PATH_SEPARATOR + instituteId);
	}

	private static String getBranchPath(Integer branchId) {
		return (branchId == null) ? "" : (PATH_SEPARATOR + branchId);
	}

	public static void main(String args[]) {
		System.out.println(isValidImageExtension(ATTACHEMENT_PATTERN, "download (2).jpg"));

	}

}
