package com.isg.service.user.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.util.StringUtils;

import com.isg.service.user.exception.ApplicationException;

// References : https://www.javacodegeeks.com/2012/05/secure-password-storage-donts-dos-and.html
public class SecurityUtil {

	private static final int PASSWORD_HASH_ITERATION_COUNT = 10;

	public static byte[] hashPassword(final String password, final byte[] salt) {

		if (StringUtils.isEmpty(password) || salt == null) {
			throw new ApplicationException("Required parameters are missing.");
		}

		try {
			final int keyLength = 160;
			// TODO move to PBKDF2WithHmacSHA512 with JDK 1.8 as SHA1 is less
			// secure.
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, PASSWORD_HASH_ITERATION_COUNT, keyLength);
			SecretKey key = skf.generateSecret(spec);
			byte[] res = key.getEncoded();
			return res;
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] generateSalt() throws NoSuchAlgorithmException {
		// VERY important to use SecureRandom instead of just Random
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		// Generate a 8 byte (64 bit) salt as recommended by RSA PKCS5
		byte[] salt = new byte[8];
		random.nextBytes(salt);
		return salt;
	}

	public static boolean authenticate(String attemptedPassword, byte[] encryptedPassword, byte[] salt)
					throws NoSuchAlgorithmException, InvalidKeySpecException {
		// Encrypt the clear-text password using the same salt that was used to
		// encrypt the original password
		byte[] encryptedAttemptedPassword = hashPassword(attemptedPassword, salt);

		// Authentication succeeds if encrypted password that the user entered
		// is equal to the stored hash
		//		System.out.println("encryptedAttemptedPassword length" + encryptedAttemptedPassword.length);
		//		System.out.println("encryptedPassword length" + encryptedPassword.length);

		return Arrays.equals(encryptedPassword, encryptedAttemptedPassword);
	}
}
