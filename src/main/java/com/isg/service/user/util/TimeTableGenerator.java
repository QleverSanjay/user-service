package com.isg.service.user.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isg.service.user.model.AcademicYear;
import com.isg.service.user.model.BranchWorkingDay;
import com.isg.service.user.request.Holiday;
import com.isg.service.user.request.TimeTableEntry;

public class TimeTableGenerator {

	public static final SimpleDateFormat formatterYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");

	public static final SimpleDateFormat formatterDDMMYYYY = new SimpleDateFormat("dd-MM-yyyy");

	public static List<TimeTableEntry> getTimeTableEntry(BranchWorkingDay workingDay, List<Holiday> holidays, Map<String, List<TimeTableEntry>> payload,
					Date startDate, Date endDate) {

		System.out.println("startDate >>>>>>>>>" + startDate.toString());
		System.out.println("endDate >>>>>>>>>" + endDate.toString());
		Map<Integer, List<TimeTableEntry>> timeTableData = new LinkedHashMap<Integer, List<TimeTableEntry>>();

		Set<Integer> workingDaysOff = getWorkDayOff(workingDay);

		boolean skipTimeTableOnHolidays = (workingDay.getIsRoundRobinTimetable() == 'Y') ? false : true;

		System.out.println("skipTimeTableOnHolidays >>>>>>>>>" + skipTimeTableOnHolidays);

		List<String> expandedHolidayList = getHolidays(holidays);

		//System.out.println("expandedHolidayList >>>>>>>>>" + expandedHolidayList);

		//String academicYearStartDate = academicYear.getFromDate();
		//String academicYearEndDate = academicYear.getToDate();

		if (skipTimeTableOnHolidays) {

			if (payload.containsKey("D1")) {
				timeTableData.put(new Integer(Calendar.MONDAY), payload.get("D1"));
			}

			if (payload.containsKey("D2")) {
				timeTableData.put(new Integer(Calendar.TUESDAY), payload.get("D2"));
			}

			if (payload.containsKey("D3")) {
				timeTableData.put(new Integer(Calendar.WEDNESDAY), payload.get("D3"));
			}

			if (payload.containsKey("D4")) {
				timeTableData.put(new Integer(Calendar.THURSDAY), payload.get("D4"));
			}

			if (payload.containsKey("D5")) {
				timeTableData.put(new Integer(Calendar.FRIDAY), payload.get("D5"));
			}

			if (payload.containsKey("D6")) {
				timeTableData.put(new Integer(Calendar.SATURDAY), payload.get("D6"));
			}

			if (payload.containsKey("D7")) {
				timeTableData.put(new Integer(Calendar.SUNDAY), payload.get("D7"));
			}

			for (Integer workOffDay : workingDaysOff) {
				timeTableData.remove(workOffDay);
			}

		} else {

			if (payload.containsKey("D1")) {
				timeTableData.put(new Integer(1), payload.get("D1"));
			}

			if (payload.containsKey("D2")) {
				timeTableData.put(new Integer(2), payload.get("D2"));
			}

			if (payload.containsKey("D3")) {
				timeTableData.put(new Integer(3), payload.get("D3"));
			}

			if (payload.containsKey("D4")) {
				timeTableData.put(new Integer(4), payload.get("D4"));
			}

			if (payload.containsKey("D5")) {
				timeTableData.put(new Integer(5), payload.get("D5"));
			}

			if (payload.containsKey("D6")) {
				timeTableData.put(new Integer(6), payload.get("D6"));
			}

			if (payload.containsKey("D7")) {
				timeTableData.put(new Integer(7), payload.get("D7"));
			}

			//Date date = parseDate(academicYearStartDate, formatterDDMMYYYY);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);

			int indexOfStartDay = calendar.get(Calendar.DAY_OF_WEEK);
			//System.out.println("indexOfStartDay >>>" + indexOfStartDay);

			timeTableData = getReAlignedTimeTable(timeTableData, indexOfStartDay);

		}

		//System.out.println("Academic year start date:" + parseDate(academicYearStartDate, formatterDDMMYYYY));
		//System.out.println("Academic year end date:" + parseDate(academicYearEndDate, formatterDDMMYYYY));
		//System.out.println("skipTimeTableOnHolidays >>" + skipTimeTableOnHolidays);
		//System.out.println("timeTableData>>" + timeTableData.toString());
		//System.out.println("workingDaysOff>>" + workingDaysOff.toString());
		//System.out.println("expandedHolidayList>>" + expandedHolidayList.toString());

		List<TimeTableEntry> timeTableEntryList = generateTimeTable(startDate,
						endDate, skipTimeTableOnHolidays,
						timeTableData,
						workingDaysOff, expandedHolidayList);

		//System.out.println(timeTableEntryList.toString());

		return timeTableEntryList;

	}

	private static Set<Integer> getWorkDayOff(BranchWorkingDay workingDay) {

		Set<Integer> workingDaysOff = new HashSet<Integer>();

		if (workingDay.getIsMondayWorking() == 'N') {
			workingDaysOff.add(Calendar.MONDAY);
		}

		if (workingDay.getIsTuesdayWorking() == 'N') {
			workingDaysOff.add(Calendar.TUESDAY);
		}

		if (workingDay.getIsWednesdayWorking() == 'N') {
			workingDaysOff.add(Calendar.WEDNESDAY);
		}

		if (workingDay.getIsThursdayWorking() == 'N') {
			workingDaysOff.add(Calendar.THURSDAY);
		}

		if (workingDay.getIsFridayWorking() == 'N') {
			workingDaysOff.add(Calendar.FRIDAY);
		}

		if (workingDay.getIsSaturdayWorking() == 'N') {
			workingDaysOff.add(Calendar.SATURDAY);
		}

		if (workingDay.getIsSundayWorking() == 'N') {
			workingDaysOff.add(Calendar.SUNDAY);
		}

		return workingDaysOff;

	}

	private static List<String> getHolidays(List<Holiday> holidays) {

		List<String> expandedHolidayList = new ArrayList<String>();

		for (Holiday holiday : holidays) {
			expandedHolidayList.addAll(getDatesBetweenDates(parseDate(holiday.getFromDate(), formatterDDMMYYYY),
							parseDate(holiday.getToDate(), formatterDDMMYYYY)));
		}

		return expandedHolidayList;

	}

	private static Map<Integer, List<TimeTableEntry>> getReAlignedTimeTable(Map<Integer, List<TimeTableEntry>> timeTableData, int dayIndex) {
		Map<Integer, List<TimeTableEntry>> timeTableDataReAligned = new LinkedHashMap<Integer, List<TimeTableEntry>>();

		int loopIndex = 1;
		int keyIndex = 1;
		while (loopIndex <= 7) {

			if (timeTableData.containsKey(new Integer(loopIndex))) {

				timeTableDataReAligned.put(new Integer(keyIndex), timeTableData.get(new Integer(loopIndex)));
				keyIndex++;

			}

			dayIndex++;

			if (dayIndex > 7) {
				dayIndex = 1;
			}

			loopIndex++;

		}

		return timeTableDataReAligned;
	}

	/*private static int getWorkingDayIndex(int ){
		
	}*/

	public static List<TimeTableEntry> generateTimeTable(Date timeTableStartDate, Date timeTableEndDate, boolean skipTimeTableOnHolidays,
					Map<Integer, List<TimeTableEntry>> timeTableData, Set<Integer> workingDaysOff, List<String> holiday) {

		List<TimeTableEntry> timeTableEntryList = new ArrayList<TimeTableEntry>();

		List<String> dates = getDatesBetweenDates(timeTableStartDate, timeTableEndDate);

		//System.out.println(dates);
		//System.out.println(holiday);

		Integer currentDateIndex = 0;

		int roundRobinIndex = 1;
		int roundRobinMaxIndex = timeTableData.keySet().size();

		for (String dateObj : dates) {
			System.out.print("Date to be scanned is ::" + dateObj);

			Date date = parseDate(dateObj, formatterYYYYMMDD);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);

			int currentDateIndexChk = calendar.get(Calendar.DAY_OF_WEEK);

			if (currentDateIndex == 0 && skipTimeTableOnHolidays) {
				currentDateIndex = new Integer(calendar.get(Calendar.DAY_OF_WEEK));
			}

			//System.out.print(" currentDateIndex :: " + currentDateIndex);

			if (skipTimeTableOnHolidays) {

				if (holiday.contains(dateObj)) {
					//Its holiday and round daily pattern, increment current timetable day index
					currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
				} else if (isDayOff(calendar, workingDaysOff)) {
					//System.out.print(" This is week-off / no data eneterd for day by user, do not increase index");
					//This is week-off / no data enterd for day by user, increase index only if daily pattern
					currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
				} else if (timeTableData.containsKey(currentDateIndex) && timeTableData.containsKey(currentDateIndexChk) && skipTimeTableOnHolidays) {
					//System.out.print(" Insert Data in DB for Today lecture is ::" + timeTableData.get(currentDateIndex));
					timeTableEntryList.addAll(getUpdatedTimetableEntry(date, timeTableData.get(currentDateIndex)));
					currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
				} else {
					currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
				}

			} else {
				if (holiday.contains(dateObj)) {
					//Its holiday and round robin pattern, retain current timetable day index
					//System.out.print(" dateObj ::" + dateObj + " is a holiday");
				} else if (isDayOff(calendar, workingDaysOff)) {
					//System.out.print(" This is week-off / no data eneterd for day by user, do not increase index");
					//This is week-off / no data enterd for day by user, increase index only if daily pattern
					//currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
				} else {

					/*if (currentDateIndex == 0) {
						currentDateIndex = currentDateIndexChk;
					} else {
						currentDateIndex = getRoundRobinDateIndex(currentDateIndex, timeTableData.keySet());
					}*/
					//System.out.print(" currentDateIndex rb :: " + roundRobinIndex);
					//System.out.print(" Insert Data in DB for Today lecture is rr pattern ::" + timeTableData.get(new Integer(roundRobinIndex)));

					timeTableEntryList.addAll(getUpdatedTimetableEntry(date, timeTableData.get(new Integer(roundRobinIndex))));

					if (roundRobinIndex + 1 > roundRobinMaxIndex) {
						roundRobinIndex = 1;
					} else {
						roundRobinIndex = roundRobinIndex + 1;
					}
					//currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, data, skipHolidays);
				}
			}
			/*if (holiday.contains(dateObj) && !skipTimeTableOnHolidays) {
				//Its holiday and round robin pattern, retain current timetable day index
			} else if (holiday.contains(dateObj) && skipTimeTableOnHolidays) {
				//Its holiday and round daily pattern, increment current timetable day index
				currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
			} else if (isDayOff(calendar, workingDaysOff)) {
				//System.out.print(" This is week-off / no data eneterd for day by user, do not increase index");
				//This is week-off / no data enterd for day by user, increase index only if daily pattern
				currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
			} else if (timeTableData.containsKey(currentDateIndex) && timeTableData.containsKey(currentDateIndexChk) && skipTimeTableOnHolidays) {
				System.out.print(" Insert Data in DB for Today lecture is ::" + timeTableData.get(currentDateIndex));
				currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
			} else if (skipTimeTableOnHolidays) {
				currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, timeTableData, skipTimeTableOnHolidays);
			} else if (!skipTimeTableOnHolidays && timeTableData.containsKey(currentDateIndexChk)) {

				if (currentDateIndex == 0) {
					currentDateIndex = currentDateIndexChk;
				} else {
					currentDateIndex = getRoundRobinDateIndex(currentDateIndex, timeTableData.keySet());
				}
				System.out.print(" currentDateIndex rb :: " + currentDateIndex);
				System.out.print(" Insert Data in DB for Today lecture is rr pattern ::" + timeTableData.get(currentDateIndex));
				//currentDateIndex = getDateIndex(currentDateIndex, workingDaysOff, data, skipHolidays);
			}*/

			//System.out.println("");
			//System.out.println("Today date index ::: " + currentDateIndex);

		}

		return timeTableEntryList;
	}

	private static List<TimeTableEntry> getUpdatedTimetableEntry(Date date, List<TimeTableEntry> list) {
		List<TimeTableEntry> updatedTimeTableEntryList = new ArrayList<TimeTableEntry>();
		TimeTableEntry updatedTimeTableEntry = null;
		for (TimeTableEntry timetableEntry : list) {
			updatedTimeTableEntry = new TimeTableEntry(timetableEntry.getTitle(), timetableEntry.getStarttime(), timetableEntry.getEndtime(), date);
			updatedTimeTableEntryList.add(updatedTimeTableEntry);
		}

		return updatedTimeTableEntryList;
	}

	private static Integer getRoundRobinDateIndex(Integer currentDateIndex, Set<Integer> keys) {
		// TODO Auto-generated method stub
		Integer nextDateIndex = null;
		int keySetSize = keys.size();
		Integer[] keyAry = new Integer[keySetSize];
		keys.toArray(keyAry);
		//System.out.println("currentDateIndex >>" + currentDateIndex);
		for (Integer i = 0; i < keySetSize; i++) {

			Integer key = keyAry[i];
			//System.out.println(key);
			if (key.intValue() == currentDateIndex.intValue() && (i == (keySetSize - 1))) {
				nextDateIndex = keyAry[0];
				//System.out.println("nextDateIndex >>" + nextDateIndex);
				return nextDateIndex;
			} else if (key.intValue() == currentDateIndex.intValue()) {
				nextDateIndex = keyAry[i + 1];
				//System.out.println("nextDateIndex >>" + nextDateIndex);
				return nextDateIndex;
			}

		}

		return nextDateIndex;
	}

	private static int getDateIndex(Integer currentIndex, Set<Integer> workingDaysOff, Map<Integer, List<TimeTableEntry>> data, boolean skipHolidays) {

		Integer newIndex = currentIndex;

		if (skipHolidays) {
			newIndex = currentIndex + 1;
		}

		if (newIndex > 7) {
			newIndex = 1; //rest to Sunday
		}

		while (true && !skipHolidays) {
			if (workingDaysOff.contains(newIndex)) {
				newIndex = newIndex + 1;
				if (newIndex > 7) {
					newIndex = 1; //rest to Sunday
				}

			} else if (!data.containsKey(newIndex)) {
				newIndex = newIndex + 1;
				if (newIndex > 7) {
					newIndex = 1; //rest to Sunday
				}

			} else {
				break;
			}

		}

		return newIndex;

	}

	private static boolean isDayOff(Calendar calendar, Set<Integer> workingDaysOff) {

		//System.out.println("Day of week::" + calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
		return workingDaysOff.contains(calendar.get(Calendar.DAY_OF_WEEK));
	}

	public static Date parseDate(String dateStr, SimpleDateFormat formatter) {
		//SimpleDateFormat formatter = new SimpleDateFormat();

		try {
			return formatter.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<String> getDatesBetweenDates(Date startdate, Date enddate)
	{
		List<String> dates = new ArrayList<String>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().compareTo(enddate) <= 0)
		{

			Date result = calendar.getTime();
			dates.add(formatterYYYYMMDD.format(result));

			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	public static void main(String arg[]) throws ParseException {
		BranchWorkingDay workingDay = new BranchWorkingDay();
		workingDay.setIsMondayWorking('Y');
		workingDay.setIsTuesdayWorking('Y');
		workingDay.setIsWednesdayWorking('Y');
		workingDay.setIsThursdayWorking('Y');
		workingDay.setIsFridayWorking('Y');
		workingDay.setIsSaturdayWorking('N');
		workingDay.setIsSundayWorking('N');
		workingDay.setIsRoundRobinTimetable('N');

		List<Holiday> holidays = new ArrayList<Holiday>();
		Holiday holiday = new Holiday();
		holiday.setFromDate("05-09-2016");
		holiday.setToDate("15-09-2016");
		holidays.add(holiday);

		holiday = new Holiday();
		holiday.setFromDate("26-09-2016");
		holiday.setToDate("26-09-2016");
		holidays.add(holiday);

		AcademicYear academicYear = new AcademicYear();
		academicYear.setFromDate("01-09-2016");
		academicYear.setToDate("30-09-2016");

		Map<String, List<TimeTableEntry>> timeTableData = null;

		if (workingDay.getIsRoundRobinTimetable() == 'N') {

			timeTableData = new LinkedHashMap<String, List<TimeTableEntry>>();

			List<TimeTableEntry> timeTableEntry = null;

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Monday lecture", "10:00", "11:00"));
			timeTableData.put("D1", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Tuesday lecture", "10:00", "11:00"));
			timeTableData.put("D2", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Wednesday lecture", "10:00", "11:00"));
			//timeTableData.put("D3", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Thursday lecture", "10:00", "11:00"));
			timeTableData.put("D4", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Friday lecture", "10:00", "11:00"));
			timeTableData.put("D5", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Saturday lecture", "10:00", "11:00"));
			timeTableData.put("D6", timeTableEntry);

		} else {

			timeTableData = new LinkedHashMap<String, List<TimeTableEntry>>();

			List<TimeTableEntry> timeTableEntry = null;

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D1 lecture", "10:00", "11:00"));
			timeTableData.put("D1", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D2 lecture", "10:00", "11:00"));
			timeTableData.put("D2", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D3 lecture", "10:00", "11:00"));
			timeTableData.put("D3", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D4 lecture", "10:00", "11:00"));
			timeTableData.put("D4", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D5 lecture", "10:00", "11:00"));
			timeTableData.put("D5", timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D6 lecture", "10:00", "11:00"));
			timeTableData.put("D6", timeTableEntry);

		}

		Date startDate = parseDate(academicYear.getFromDate(), formatterDDMMYYYY);
		Date endDate = parseDate(academicYear.getToDate(), formatterDDMMYYYY);
		getTimeTableEntry(workingDay, holidays, timeTableData, startDate, endDate);

	}

	public static void main1(String arg[]) throws ParseException {
		Map<Integer, List<TimeTableEntry>> timeTableData = null;

		boolean skipHolidays = false;
		String startDate = "2016-09-01";
		String endDate = "2016-09-30";

		Set<Integer> workingDaysOff = new HashSet<Integer>();
		/*		workingDaysOff.add(Calendar.MONDAY);
				workingDaysOff.add(Calendar.TUESDAY);
				workingDaysOff.add(Calendar.WEDNESDAY);
				workingDaysOff.add(Calendar.THURSDAY);*/
		//workingDaysOff.add(Calendar.FRIDAY);
		workingDaysOff.add(Calendar.SATURDAY);
		workingDaysOff.add(Calendar.SUNDAY);

		if (skipHolidays) {

			timeTableData = new LinkedHashMap<Integer, List<TimeTableEntry>>();

			List<TimeTableEntry> timeTableEntry = null;

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Monday lecture", "10:00", "11:00"));
			timeTableData.put(new Integer(Calendar.MONDAY), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Tuesday lecture", "10:00", "11:00"));
			timeTableData.put(new Integer(Calendar.TUESDAY), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Wednesday lecture", "10:00", "11:00"));
			//timeTableData.put(new Integer(Calendar.WEDNESDAY), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Thursday lecture", "10:00", "11:00"));
			timeTableData.put(new Integer(Calendar.THURSDAY), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Friday lecture", "10:00", "11:00"));
			timeTableData.put(new Integer(Calendar.FRIDAY), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("Saturday lecture", "10:00", "11:00"));
			timeTableData.put(new Integer(Calendar.SATURDAY), timeTableEntry);

			//System.out.println("*******************" + workingDaysOff.size());

			for (Integer workOffDay : workingDaysOff) {
				timeTableData.remove(workOffDay);
			}

		} else {

			Map<Integer, List<TimeTableEntry>> timeTableDataRR = new LinkedHashMap<Integer, List<TimeTableEntry>>();

			List<TimeTableEntry> timeTableEntry = null;

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D1 lecture", "10:00", "11:00"));
			timeTableDataRR.put(new Integer("1"), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D2 lecture", "10:00", "11:00"));
			timeTableDataRR.put(new Integer("2"), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D3 lecture", "10:00", "11:00"));
			timeTableDataRR.put(new Integer("3"), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D4 lecture", "10:00", "11:00"));
			timeTableDataRR.put(new Integer("4"), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D5 lecture", "10:00", "11:00"));
			timeTableDataRR.put(new Integer("5"), timeTableEntry);

			timeTableEntry = new ArrayList<TimeTableEntry>();
			timeTableEntry.add(new TimeTableEntry("D6 lecture", "10:00", "11:00"));
			timeTableDataRR.put(new Integer("6"), timeTableEntry);

			Date date = parseDate(startDate, formatterYYYYMMDD);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);

			int indexOfStartDay = calendar.get(Calendar.DAY_OF_WEEK);
			//System.out.println("indexOfStartDay >>>" + indexOfStartDay);

			/*timeTableData = new LinkedHashMap<Integer, String>();
			timeTableData.put(new Integer(Calendar.THURSDAY), "D1 lecture");
			timeTableData.put(new Integer(Calendar.FRIDAY), "D2 lecture");
			timeTableData.put(new Integer(Calendar.SATURDAY), "D3 lecture");
			timeTableData.put(new Integer(Calendar.SUNDAY), "D4 lecture");
			timeTableData.put(new Integer(Calendar.MONDAY), "D5 lecture");
			timeTableData.put(new Integer(Calendar.TUESDAY), "D6 lecture");*/

			timeTableData = getReAlignedTimeTable(timeTableDataRR, indexOfStartDay);
			//workingDaysOff.add(Calendar.TUESDAY);
			//workingDaysOff.add(Calendar.THURSDAY);

		}

		List<String> holiday = new ArrayList<String>();
		holiday.addAll(getDatesBetweenDates(parseDate("2016-08-15", formatterYYYYMMDD), parseDate("2016-08-15", formatterYYYYMMDD)));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-10-02", formatterYYYYMMDD), parseDate("2016-10-02", formatterYYYYMMDD)));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-09-05", formatterYYYYMMDD), parseDate("2016-09-15", formatterYYYYMMDD)));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-09-26", formatterYYYYMMDD), parseDate("2016-09-26", formatterYYYYMMDD)));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-07-26", formatterYYYYMMDD), parseDate("2016-07-26", formatterYYYYMMDD)));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-08-15", formatterYYYYMMDD), parseDate("2016-08-15", formatterYYYYMMDD)));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-08-15", formatterYYYYMMDD), parseDate("2016-08-15", formatterYYYYMMDD)));
		holiday.addAll(getDatesBetweenDates(parseDate("2016-08-15", formatterYYYYMMDD), parseDate("2016-08-15", formatterYYYYMMDD)));
		holiday.addAll(getDatesBetweenDates(parseDate("2017-01-26", formatterYYYYMMDD), parseDate("2017-01-26", formatterYYYYMMDD)));

		generateTimeTable(parseDate(startDate, formatterYYYYMMDD), parseDate(endDate, formatterYYYYMMDD), skipHolidays, timeTableData, workingDaysOff, holiday);

	}
}
