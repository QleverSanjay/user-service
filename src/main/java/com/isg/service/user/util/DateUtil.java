package com.isg.service.user.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.util.StringUtils;

public class DateUtil {

	public static Integer[] getWeeksOfMonth(int month, int year)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 1);

		Set<Integer> weeks = new TreeSet<Integer>();
		int ndays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		for (int i = 0; i < ndays; i++)
		{
			weeks.add(cal.get(Calendar.WEEK_OF_YEAR));
			cal.add(Calendar.DATE, 1);
		}

		return weeks.toArray(new Integer[0]);
	}

	public static String getStartDate(int month, int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 1);

		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
		return displayFormat.format(cal.getTime());

	}

	public static String getEndDate(int month, int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		// Getting Maximum day for Given Month
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, maxDay);
		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
		return displayFormat.format(cal.getTime());

	}

	public static String getStartDateOfWeek(int month, int year, int week) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.WEEK_OF_YEAR, week);
		// Getting Maximum day for Given Month
		int minDay = cal.getActualMinimum(Calendar.DAY_OF_WEEK);
		cal.set(Calendar.DAY_OF_WEEK, minDay);
		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
		return displayFormat.format(cal.getTime());
	}

	public static String getEndDateOfWeek(int month, int year, int week) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.WEEK_OF_YEAR, week);
		// Getting Maximum day for Given Month
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_WEEK);
		cal.set(Calendar.DAY_OF_WEEK, maxDay);
		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
		return displayFormat.format(cal.getTime());
	}

	public static String getWeekDatesForMonth(int month) {
		Calendar cal = Calendar.getInstance();
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_WEEK);
		cal.set(Calendar.DAY_OF_WEEK, maxDay);
		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
		return displayFormat.format(cal.getTime());
	}

	public static Date parseDate(String dateStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {
			return formatter.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static java.sql.Date parseDateInDBFormat(String dateStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		java.sql.Date date = null;

		if (!StringUtils.isEmpty(dateStr)) {
			try {
				java.util.Date startDateParsed = formatter.parse(dateStr);
				date = new java.sql.Date(startDateParsed.getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	public static String convertDateFormat(String dateStr, String sourceFormat, String destinationFormat) {

		String date = null;

		if (!StringUtils.isEmpty(dateStr)) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat(sourceFormat);
				Date birthdate = formatter.parse("2016-01-01");
				SimpleDateFormat formatter1 = new SimpleDateFormat(destinationFormat);
				date = formatter1.format(birthdate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	public static List<Date> getDatesBetweenDates(Date startdate, Date enddate, int month)
	{
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().compareTo(enddate) <= 0)
		{
			if (calendar.get(Calendar.MONTH) == month) {
				Date result = calendar.getTime();
				dates.add(result);

			}
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

}
