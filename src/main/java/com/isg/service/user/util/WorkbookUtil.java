package com.isg.service.user.util;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.onlineadmission.AcademicDetail;
import com.isg.service.user.model.onlineadmission.Address;
import com.isg.service.user.model.onlineadmission.CandidateDetails;
import com.isg.service.user.model.onlineadmission.CasteDetails;
import com.isg.service.user.model.onlineadmission.ContactDetails;
import com.isg.service.user.model.onlineadmission.CoursePreference;
import com.isg.service.user.model.onlineadmission.EmploymentDetails;
import com.isg.service.user.model.onlineadmission.GuardianDetails;
import com.isg.service.user.model.onlineadmission.Location;
import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.model.onlineadmission.ParentDetail;
import com.isg.service.user.model.onlineadmission.QualifyingExamDetails;
import com.isg.service.user.model.onlineadmission.SiblingDetail;
import com.isg.service.user.model.onlineadmission.SubjectPreference;

public class WorkbookUtil {

	public byte[] getOnlineAdmissionReport(List<OnlineAdmission> onlineAdmissionList, String exportTemplateFactoryName) {

		if (exportTemplateFactoryName.equalsIgnoreCase("dalmia")) {
			return exportReportForDalmia(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("pathfinder")) {
			return exportReportForPathfinder(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("rosemine")) {
			return exportReportForRosemine(onlineAdmissionList);
		}
		else if (exportTemplateFactoryName.equalsIgnoreCase("hkimsr")) {
			return exportReportForHkimsr(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("vbu")) {
			return exportReportForVbu(onlineAdmissionList);

		} else if (exportTemplateFactoryName.equalsIgnoreCase("scp")) {
			return exportReportForScp(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("iitm")) {
			return exportReportForIITM(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("ascend")) {
			return exportReportForASCEND(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("vvs")) {
			return exportReportForVVS(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("lhs")) {
			return exportReportForLHS(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("lhe")) {
			return exportReportForLHE(onlineAdmissionList);
		}else if (exportTemplateFactoryName.equalsIgnoreCase("etoos")) {
			return exportReportForEtoos(onlineAdmissionList);
		}else if (exportTemplateFactoryName.equalsIgnoreCase("rtac")) {
			return exportReportForRtac(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("iihmr")) {
			return exportReportForIIHMR(onlineAdmissionList);
		} else if (exportTemplateFactoryName.equalsIgnoreCase("iitkR")) {
			return exportReportForIITKR(onlineAdmissionList);
		}
		else if (exportTemplateFactoryName.equalsIgnoreCase("iitkM")) {
			return exportReportForIITKM(onlineAdmissionList);
		}
		else if (exportTemplateFactoryName.equalsIgnoreCase("iitkD")) {
			return exportReportForIITKD(onlineAdmissionList);
		}
		else if (exportTemplateFactoryName.equalsIgnoreCase("vvsjunior")) {
			return exportReportForVVSJUNIOR(onlineAdmissionList);
		}
		
		else {
			throw new ApplicationException("Export format not specified.");
		}
	}

	private byte[] exportReportForVVS(List<OnlineAdmission> onlineAdmissionList) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");

			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Applicant is my own");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Category");

			data.add("Father First Name");
			data.add("Father Middle Name");
			data.add("Father Surname");
			data.add("Father office Phone");
			
			data.add("Father Occupation");
			data.add("Name of Company");
			data.add("Designation & Dept.");
			data.add("Personal NO.");
			data.add("Monthly Income");
			
			data.add("Father Phone No.");
			data.add("Father Email");
			
			data.add("Mother First Name");
			data.add("Mother Middle Name");
			data.add("Mother Surname");
			data.add("Mother Primary Phone ");
			data.add("Mother Secondary Phone");
			data.add("Mother Email");
			data.add("Mother Occupation ");
			data.add("Mother Organization Name");
			
			data.add("Designation & Dept");
			
			data.add("Personal NO");
			
			data.add("Monthly Income");
			
			data.add("Local Address");
			data.add("City");
			data.add("Postal Code");
			
			data.add("Address(Nativ Place)");
			data.add("District");
			data.add("State");
			
			data.add("Sibling First Name");
			data.add("Sibling Middle Name");
			data.add("Sibling Surname");
			data.add("Class/Section");
			data.add("Admission No.");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {
				try{

					if (onlineAdmission != null) {
	
						CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
	
						String gender = candidateDetails.getGender().getName();
						String dob = candidateDetails.getDateofbirth();
						String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());
						String category = candidateDetails.getCasteDetails().getCaste().getName();
						
						dataList = new LinkedList<String>();
	
						dataList.add(onlineAdmission.getApplicationId());
						dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());
	
	
						Name candidateName = candidateDetails.getName();
						dataList.add(candidateName.getFirstname());
						dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
						dataList.add(gender);
						dataList.add(dob);
					    dataList.add(category);
	
						ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
						if (fatherDetail != null) {
							Name fatherName = fatherDetail.getName();
							dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
							dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
							dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");
							dataList.add(!StringUtils.isEmpty(fatherDetail.getSecondaryPhone()) ? fatherDetail.getSecondaryPhone() : "");
							
							EmploymentDetails fatherEmploymentDetail = fatherDetail.getEmploymentDetails();
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getOccupation()) ? fatherEmploymentDetail.getOccupation() : "");
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getEmployerName()) ? fatherEmploymentDetail.getEmployerName() : "");
							
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getDesignation()) ? fatherEmploymentDetail.getDesignation() : "");
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getPincode()) ? fatherEmploymentDetail.getPincode() : "");
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getAnnualIncome()) ? fatherEmploymentDetail.getAnnualIncome() : "");
	
						} else {
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
						}
	
						ContactDetails cd = onlineAdmission.getContactDetails();
						dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
						dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");
	
						
						
						ParentDetail motherDetail = onlineAdmission.getMotherDetails();
						Name motherName = motherDetail.getName();
						dataList.add(!StringUtils.isEmpty(motherName.getFirstname()) ? motherName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(motherName.getMiddlename()) ? motherName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(motherName.getSurname()) ? motherName.getSurname() : "");
	
						dataList.add(!StringUtils.isEmpty(motherDetail.getPrimaryPhone()) ? motherDetail.getPrimaryPhone() : "");
						dataList.add(!StringUtils.isEmpty(motherDetail.getSecondaryPhone()) ? motherDetail.getSecondaryPhone() : "");
	
						dataList.add(!StringUtils.isEmpty(motherDetail.getEmail()) ? motherDetail.getEmail() : "");
	
						EmploymentDetails motherEmploymentDetail = motherDetail.getEmploymentDetails();
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getOccupation()) ? motherEmploymentDetail.getOccupation() : "");
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getEmployerName()) ? motherEmploymentDetail.getEmployerName() : "");
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getDesignation()) ? motherEmploymentDetail.getDesignation() : "");
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getPincode()) ? motherEmploymentDetail.getPincode() : "");
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getAnnualIncome()) ? motherEmploymentDetail.getAnnualIncome() : "");
						
						dataList.add(address);
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCity()) ? onlineAdmission.getAddressDetails()
										.getLocalAddress().getCity() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission.getAddressDetails()
										.getLocalAddress().getPincode().toString() : "");
						
						
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getArea()) ? onlineAdmission.getAddressDetails()
								.getLocalAddress().getArea() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()) ? onlineAdmission.getAddressDetails()
								.getLocalAddress().getDistrict().getName() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getState()) ? onlineAdmission.getAddressDetails()
						.getLocalAddress().getState().getName() : "");
		
	
	
						/*List<SiblingDetail> siblingDetails = onlineAdmission.getSiblingDetail();
						if (siblingDetails != null) {
							SiblingDetail siblingDetail = siblingDetails.get(0);
							Name siblingName = siblingDetail.getName();
							dataList.add(!StringUtils.isEmpty(siblingName.getFirstname()) ? siblingName.getFirstname() : "");
							dataList.add(!StringUtils.isEmpty(siblingName.getMiddlename()) ? siblingName.getMiddlename() : "");
							dataList.add(!StringUtils.isEmpty(siblingName.getSurname()) ? siblingName.getSurname() : "");
							dataList.add(!StringUtils.isEmpty(siblingDetail.getGrade()) ? siblingDetail.getGrade().toString() : "");
							dataList.add(!StringUtils.isEmpty(siblingDetail.getCurrentSchool()) ? siblingDetail.getCurrentSchool().toString() : "");
	
						} else {
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
						}*/
						List<SiblingDetail> siblingDetails = onlineAdmission.getSiblingDetail();
						boolean siblingAvailable = false;
						if (siblingDetails != null && siblingDetails.size() > 0) {
							SiblingDetail siblingDetail = siblingDetails.get(0);
							if(siblingDetail!= null ){
								Name siblingName = siblingDetail.getName();
								if(siblingName != null){
									siblingAvailable= true;
									dataList.add(!StringUtils.isEmpty(siblingName.getFirstname()) ? siblingName.getFirstname() : "");
									dataList.add(!StringUtils.isEmpty(siblingName.getMiddlename()) ? siblingName.getMiddlename() : "");
									dataList.add(!StringUtils.isEmpty(siblingName.getSurname()) ? siblingName.getSurname() : "");
									dataList.add(!StringUtils.isEmpty(siblingDetail.getGrade()) ? siblingDetail.getGrade().toString() : "");
									dataList.add(!StringUtils.isEmpty(siblingDetail.getCurrentSchool()) ? siblingDetail.getCurrentSchool().toString() : "");
	
								}
							}
						} 
						if(!siblingAvailable) {
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
						}
						
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");
	
						addRowInWorkBook(sheet, dataList, rowIndex);
	
						rowIndex++;
	
					}
				}catch(NullPointerException e){
					continue;
				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
	private byte[] exportReportForVVSJUNIOR(List<OnlineAdmission> onlineAdmissionList) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");

			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Applicant is my own");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Category");
			
			data.add("Subject");
			data.add("Name of X School");
			data.add("Name of X Board");
			data.add("Name of city in which school is located" );
			data.add("Session");
			data.add("Medium");
			data.add("Student Type");
			
			
			data.add("Father First Name");
			data.add("Father Middle Name");
			data.add("Father Surname");
			data.add("Father office Phone");
			
			data.add("Father Occupation");
			data.add("Name of Company");
			data.add("Designation & Dept.");
			data.add("Personal NO.");
			data.add("Monthly Income");
			
			data.add("Father Phone No.");
			data.add("Father Email");
			
			data.add("Mother First Name");
			data.add("Mother Middle Name");
			data.add("Mother Surname");
			data.add("Mother Primary Phone ");
			data.add("Mother Secondary Phone");
			data.add("Mother Email");
			data.add("Mother Occupation ");
			data.add("Mother Organization Name");
			
			data.add("Guardian First Name");
			data.add("Guardian Middle Name");
			data.add("Guardian Surname");
			data.add("Guardian Primary Phone ");
			data.add("Guardian Secondary Phone");
			data.add("Guardian Email");
			data.add("Guardian Occupation ");
			data.add("Guardian Organization Name");
			
			data.add("Designation & Dept");
			
			data.add("Personal NO");
			
			data.add("Monthly Income");
			
			data.add("Local Address");
			data.add("City");
			data.add("Postal Code");
			
			data.add("Address(Nativ Place)");
			data.add("District");
			data.add("State");
			
			/*data.add("Sibling First Name");
			data.add("Sibling Middle Name");
			data.add("Sibling Surname");
			data.add("Class/Section");
			data.add("Admission No.");*/

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {
				try{

					if (onlineAdmission != null) {
	
						CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
						QualifyingExamDetails qualifyingExamDetails=onlineAdmission.getQualifyingExamDetails();
	
						String gender = candidateDetails.getGender().getName();
						String dob = candidateDetails.getDateofbirth();
						String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());
						String category = candidateDetails.getCasteDetails().getCaste().getName();
						
						dataList = new LinkedList<String>();
	
						dataList.add(onlineAdmission.getApplicationId());
						dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());
	
	
						Name candidateName = candidateDetails.getName();
						dataList.add(candidateName.getFirstname());
						dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
						dataList.add(gender);
						dataList.add(dob);
					    dataList.add(category);
					    
	
						ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
						if (fatherDetail != null) {
							Name fatherName = fatherDetail.getName();
							dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
							dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
							dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");
							dataList.add(!StringUtils.isEmpty(fatherDetail.getSecondaryPhone()) ? fatherDetail.getSecondaryPhone() : "");
							
							EmploymentDetails fatherEmploymentDetail = fatherDetail.getEmploymentDetails();
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getOccupation()) ? fatherEmploymentDetail.getOccupation() : "");
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getEmployerName()) ? fatherEmploymentDetail.getEmployerName() : "");
							
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getDesignation()) ? fatherEmploymentDetail.getDesignation() : "");
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getPincode()) ? fatherEmploymentDetail.getPincode() : "");
							dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getAnnualIncome()) ? fatherEmploymentDetail.getAnnualIncome() : "");
	
						} else {
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
						}
	
						ContactDetails cd = onlineAdmission.getContactDetails();
						dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
						dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");
	
						
						
						ParentDetail motherDetail = onlineAdmission.getMotherDetails();
						Name motherName = motherDetail.getName();
						dataList.add(!StringUtils.isEmpty(motherName.getFirstname()) ? motherName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(motherName.getMiddlename()) ? motherName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(motherName.getSurname()) ? motherName.getSurname() : "");
	
						dataList.add(!StringUtils.isEmpty(motherDetail.getPrimaryPhone()) ? motherDetail.getPrimaryPhone() : "");
						dataList.add(!StringUtils.isEmpty(motherDetail.getSecondaryPhone()) ? motherDetail.getSecondaryPhone() : "");
	
						dataList.add(!StringUtils.isEmpty(motherDetail.getEmail()) ? motherDetail.getEmail() : "");
	
						EmploymentDetails motherEmploymentDetail = motherDetail.getEmploymentDetails();
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getOccupation()) ? motherEmploymentDetail.getOccupation() : "");
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getEmployerName()) ? motherEmploymentDetail.getEmployerName() : "");
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getDesignation()) ? motherEmploymentDetail.getDesignation() : "");
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getPincode()) ? motherEmploymentDetail.getPincode() : "");
						dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getAnnualIncome()) ? motherEmploymentDetail.getAnnualIncome() : "");
						
						dataList.add(address);
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCity()) ? onlineAdmission.getAddressDetails()
										.getLocalAddress().getCity() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission.getAddressDetails()
										.getLocalAddress().getPincode().toString() : "");
						
						
						
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getArea()) ? onlineAdmission.getAddressDetails()
								.getLocalAddress().getArea() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()) ? onlineAdmission.getAddressDetails()
								.getLocalAddress().getDistrict().getName() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getState()) ? onlineAdmission.getAddressDetails()
						.getLocalAddress().getState().getName() : "");
						
						dataList.add(qualifyingExamDetails.getSchoolCity());
						dataList.add(qualifyingExamDetails.getSchoolCollege());
						dataList.add(qualifyingExamDetails.getBoardUniversity().getName());
						dataList.add(qualifyingExamDetails.getPassingYear());
						dataList.add(qualifyingExamDetails.getStudentType());
						dataList.add(qualifyingExamDetails.getPassingYear());
						dataList.add(qualifyingExamDetails.getPreviousStream().getName());
						
						
		
	
	
						/*List<SiblingDetail> siblingDetails = onlineAdmission.getSiblingDetail();
						if (siblingDetails != null) {
							SiblingDetail siblingDetail = siblingDetails.get(0);
							Name siblingName = siblingDetail.getName();
							dataList.add(!StringUtils.isEmpty(siblingName.getFirstname()) ? siblingName.getFirstname() : "");
							dataList.add(!StringUtils.isEmpty(siblingName.getMiddlename()) ? siblingName.getMiddlename() : "");
							dataList.add(!StringUtils.isEmpty(siblingName.getSurname()) ? siblingName.getSurname() : "");
							dataList.add(!StringUtils.isEmpty(siblingDetail.getGrade()) ? siblingDetail.getGrade().toString() : "");
							dataList.add(!StringUtils.isEmpty(siblingDetail.getCurrentSchool()) ? siblingDetail.getCurrentSchool().toString() : "");
	
						} else {
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
						}
						List<SiblingDetail> siblingDetails = onlineAdmission.getSiblingDetail();
						boolean siblingAvailable = false;
						if (siblingDetails != null && siblingDetails.size() > 0) {
							SiblingDetail siblingDetail = siblingDetails.get(0);
							if(siblingDetail!= null ){
								Name siblingName = siblingDetail.getName();
								if(siblingName != null){
									siblingAvailable= true;
									dataList.add(!StringUtils.isEmpty(siblingName.getFirstname()) ? siblingName.getFirstname() : "");
									dataList.add(!StringUtils.isEmpty(siblingName.getMiddlename()) ? siblingName.getMiddlename() : "");
									dataList.add(!StringUtils.isEmpty(siblingName.getSurname()) ? siblingName.getSurname() : "");
									dataList.add(!StringUtils.isEmpty(siblingDetail.getGrade()) ? siblingDetail.getGrade().toString() : "");
									dataList.add(!StringUtils.isEmpty(siblingDetail.getCurrentSchool()) ? siblingDetail.getCurrentSchool().toString() : "");
	
								}
							}
						} 
						if(!siblingAvailable) {
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
						}*/
						
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");
	
						addRowInWorkBook(sheet, dataList, rowIndex);
	
						rowIndex++;
	
					}
				}catch(NullPointerException e){
					continue;
				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

	private byte[] exportReportForDalmia(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");
			data.add("Title");
			data.add("STUDENT NAME");
			data.add("Gender");
			data.add("Married / Unmarried");
			data.add("Date Of Birth");
			data.add("Place Of Birth");
			data.add("Caste Category");
			data.add("Sub Caste");
			data.add("Religion ");
			data.add("Physical Handicapped");
			data.add("Application Form No");
			data.add("Physcial Form No");
			data.add("Mother Tongue");
			data.add("Aadhar Card No");
			data.add("Bank Account No");
			data.add("Bank Name");
			//			data.add("Last School");
			data.add("Social Reservation");
			data.add("LOCAL ADDRESS ");
			data.add("Native Place Address");
			data.add("Mobile No");
			data.add("Email Id.");
			
			data.add("Father Name");
			data.add("Father Occupation");
			data.add("Father's Mobile no.");
			data.add("Father Email");
			data.add("Mother Name");
			data.add("Mother Occupation");
			data.add("Mother Email");
			
			data.add("SSC Year Of Passing");
			data.add("SSC School Attended");
			data.add("SSC Board/University");
			data.add("SSC Total Marks Obtained");
			data.add("SSC Total Marks Out Of");
			data.add("SSC Percentage");
			//		data.add("HSC Marks");

			data.add("Qualifying Exam");
			data.add("Qualifying Exam Board");
			data.add("Qualifying Exam College");
			data.add("Qualifying Exam Month/Year");
			data.add("Qualifying Exam Marks Obtained");
			data.add("Qualifying Exam Marks Total");
			data.add("Qualifying Exam Marks Percentage");
			data.add("Gap In Education/Repeater");
			data.add("Qualifying Exam Grade");
			data.add("Qualifying Exam Seat Number");
			data.add("Previous Stream (Applicable Only For BMS)");
			data.add("In-House or Outsider");
			data.add("School Verified");
			data.add("Created At");
			data.add("Verified At");
			data.add("Games & Sports Participation");
			data.add("Games & Sports Level");

			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				try{
				
				if (onlineAdmission != null) {
					
					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
					boolean isMarried = (candidateDetails.getMaritalStatus() != null &&
									candidateDetails.getMaritalStatus().getName().equalsIgnoreCase("MARRIED")) ? true : false;
					boolean isMale = candidateDetails.getGender() != null ? ("Male".equalsIgnoreCase(candidateDetails.getGender().getName()))
									: true;

					String applicationId = onlineAdmission.getApplicationId();
					//					System.out.println("ApplicationID :::>>>" + applicationId);
					String courseName = onlineAdmission.getCourseDetails().getCourse().getName();					
					String title = getTitle(isMarried, isMale);
					String studentName = getStudentName(onlineAdmission.getCandidateDetails(), onlineAdmission.getMotherDetails());
					String gender = candidateDetails.getGender() != null ? candidateDetails.getGender().getName() : "";

					String maritalStatus = isMarried ? "MARRIED" : "UNMARRIED";
					String dob = candidateDetails.getDateofbirth();
					String placeOfBirth = !StringUtils.isEmpty(candidateDetails.getPlaceofbirth()) ? candidateDetails.getPlaceofbirth() : "";

					String casteCategory = candidateDetails.getCasteDetails().getCaste() != null ? candidateDetails.getCasteDetails().getCaste().getName() : "";

					String subCaste = candidateDetails.getCasteDetails().getSubcaste() != null ?
									candidateDetails.getCasteDetails().getSubcaste().getName() : "";
					String religion = candidateDetails.getCasteDetails().getReligion() != null
									? candidateDetails.getCasteDetails().getReligion().getName() : "";

					String physicallyHandicapped = candidateDetails.getPhysicallyHandicapped() != null ?
									candidateDetails.getPhysicallyHandicapped().getName() : "";

					String applicationFormNumber = "";
					String mkclRegistrationNo = "";

					String physicalFormNumber = "";

					if (candidateDetails.getFormDetails() != null) {
						applicationFormNumber = candidateDetails.getFormDetails().getApplicationFormNo();
						mkclRegistrationNo = candidateDetails.getFormDetails().getMkclRegistrationNo();
						physicalFormNumber = candidateDetails.getFormDetails().getPhysicalFormNo();
					}

					String motherToungue = !StringUtils.isEmpty(candidateDetails.getMotherTounge()) ? candidateDetails.getMotherTounge() : "";
					String aadhaarCardNumber = (candidateDetails.getAadhaaarCard() == null ? "" : candidateDetails.getAadhaaarCard() + "");
					String bankAccountNumber = candidateDetails.getBankDetails() != null ? "" + candidateDetails.getBankDetails().getBankAccount() : "";
					String bankName = candidateDetails.getBankDetails() != null ? candidateDetails.getBankDetails().getBankName() : "";
					//					String lasetSchoolCollege = candidateDetails.getLastSchoolName();
					String socialReservation = candidateDetails.getSocialReservation() != null ? candidateDetails.getSocialReservation().getName() : "";

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());
					String nativePlaceAddress = !StringUtils.isEmpty(candidateDetails.getNativePlaceAddress()) ? candidateDetails.getNativePlaceAddress() : "";
					String mobileNumber = onlineAdmission.getContactDetails().getPersonalMobileNumber();
					String email = onlineAdmission.getContactDetails().getPersonalEmail();
					
					//parent details added on 26/10/17 by praveen
					String parentMobile = !StringUtils.isEmpty(onlineAdmission.getContactDetails().getParentContactNumber()) ? onlineAdmission
									.getContactDetails().getParentContactNumber() : "";
					String fatherEmail = !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmail()) ? onlineAdmission
							.getFatherDetails().getEmail() : "";
							
					String motherEmail = !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmail()) ? onlineAdmission
							.getMotherDetails().getEmail() : "";
									
					String fatherName =	(!StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName().getFirstname()) 
												? onlineAdmission.getFatherDetails()
							.getName().getFirstname(): "")
									+ " "
									+ (!StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName().getMiddlename())? onlineAdmission.getFatherDetails()
													.getName().getMiddlename() :  "")
									+ " "
									+ (!StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName().getSurname()) ?
											onlineAdmission.getFatherDetails().getName().getSurname() : "");
	
					String fatherOccupation = !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails().getOccupation()) ? onlineAdmission
							.getFatherDetails().getEmploymentDetails().getOccupation() : "";
						
					String motherName =	(!StringUtils.isEmpty(onlineAdmission.getMotherDetails().getName().getFirstname()) ? onlineAdmission.getMotherDetails().getName()
												.getFirstname() : "")
									+ " "
									+ (!StringUtils.isEmpty(onlineAdmission.getMotherDetails().getName().getMiddlename())? onlineAdmission.getMotherDetails()
													.getName().getMiddlename() :  "")
									+ " "
									+ (!StringUtils.isEmpty(onlineAdmission.getMotherDetails().getName().getSurname()) ? 
											onlineAdmission.getMotherDetails().getName().getSurname() : "");
													
					String motherOccupation = !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getOccupation()) ? onlineAdmission
							.getMotherDetails().getEmploymentDetails().getOccupation() : "";
						
							
							
					AcademicDetail ssc = onlineAdmission.getAcademicDetails().get(0);
					
					String qualifyingExam = null;
					String qualifyingExamBoard = null;
					String qualifyingExamCollegeAttended = null;
					String qualifyingExamYearOfPassing = null;
					String qualifyingExamMarksObtained = null;
					String qualifyingExamTotalMarks = null;
					String qualifyingExamPercentage = null;
					
					String sscYearOfPassing = ssc.getPassingYear();
					String sscSchoolAttended = ssc.getSchool();
					String sscBoard = ssc.getBoardUniversity();
					String sscObtainedMarks = ssc.getObtainedMarks() + "";
					String sscTotalMarks = ssc.getTotalMarks() + "";
					String sscPercentage = ssc.getPercentage() + "";
					String qualifyingExamGapInEduaction = null;
					String qualifyingExamGrade = null;
					String qualifyingExamSeatNumber = null;
					String qualifyingExamInhouseOrOutsider = null;
					String previousStream = null;
					String schoolVerified = null;
					String createdAt = null;
					String verifiedAt = null;
					String isPartOfSport = null;
					String sportDetails = null;

					QualifyingExamDetails quExamDetail = onlineAdmission.getQualifyingExamDetails();
					if(quExamDetail != null){
						
						qualifyingExam = quExamDetail.getExam().getName();
						qualifyingExamBoard = quExamDetail.getBoardUniversity() != null ? quExamDetail.getBoardUniversity().getName() : "";
						qualifyingExamCollegeAttended = quExamDetail.getSchoolCollege();
						qualifyingExamYearOfPassing = quExamDetail.getPassingMonth() + "/" + quExamDetail.getPassingYear();
						qualifyingExamMarksObtained = quExamDetail.getObtainedMarks() + "";
						qualifyingExamTotalMarks = quExamDetail.getTotalMarks() + "";
						qualifyingExamPercentage = "0";
	
						try {
							qualifyingExamPercentage = ((quExamDetail.getObtainedMarks() * 100) / quExamDetail.getTotalMarks()) + "";
						}catch(Exception e){
							e.printStackTrace();
						}
	
						qualifyingExamGapInEduaction = quExamDetail.getEducationGap();
						qualifyingExamGrade = quExamDetail.getGrade();
						qualifyingExamSeatNumber = quExamDetail.getExamSeatNo();
						qualifyingExamInhouseOrOutsider = quExamDetail.getApplicantfrom();
						previousStream = (quExamDetail.getPreviousStream() != null) ? quExamDetail.getPreviousStream().getName() : "";
						schoolVerified = onlineAdmission.getSchoolVerified();
						schoolVerified = "Y".equals(schoolVerified) ? "Verified" : "Pending";
						createdAt = onlineAdmission.getCreatedAt();
						verifiedAt = onlineAdmission.getVerifiedAt();
						isPartOfSport = onlineAdmission.getIspartofsports() == 1 ? "Y" : "N";
						sportDetails = onlineAdmission.getCandidateDetails().getSportsDetails();
					}

					dataList = new LinkedList<String>();

					dataList.add(applicationId);
					dataList.add(courseName);
					dataList.add(title);
					dataList.add(studentName);
					dataList.add(gender);
					dataList.add(maritalStatus);
					dataList.add(dob);
					dataList.add(placeOfBirth);
					dataList.add(casteCategory);
					dataList.add(subCaste);
					dataList.add(religion);
					dataList.add(physicallyHandicapped);
					dataList.add(StringUtils.isEmpty(applicationFormNumber) ? mkclRegistrationNo : applicationFormNumber);
					dataList.add(physicalFormNumber);
					dataList.add(motherToungue);
					dataList.add(StringUtils.isEmpty(aadhaarCardNumber) ? "" : aadhaarCardNumber);
					dataList.add(bankAccountNumber);
					dataList.add(bankName);
					//					dataList.add(lasetSchoolCollege);
					dataList.add(socialReservation);
					dataList.add(address);
					dataList.add(nativePlaceAddress);
					dataList.add(mobileNumber);
					dataList.add(email);

					dataList.add(fatherName);
					dataList.add(fatherOccupation);
					dataList.add(parentMobile);
					dataList.add(fatherEmail);

					dataList.add(motherName);
					dataList.add(motherOccupation);
					dataList.add(motherEmail);
					
					dataList.add(sscYearOfPassing);
					dataList.add(sscSchoolAttended);
					dataList.add(sscBoard);
					dataList.add(sscObtainedMarks);
					dataList.add(sscTotalMarks);
					dataList.add(sscPercentage);

					dataList.add(qualifyingExam);
					dataList.add(qualifyingExamBoard);
					dataList.add(qualifyingExamCollegeAttended);
					dataList.add(qualifyingExamYearOfPassing);
					dataList.add(qualifyingExamMarksObtained);
					dataList.add(qualifyingExamTotalMarks);
					dataList.add(qualifyingExamPercentage);
					dataList.add(qualifyingExamGapInEduaction);
					dataList.add(qualifyingExamGrade);
					dataList.add(qualifyingExamSeatNumber);
					dataList.add(previousStream);
					dataList.add(qualifyingExamInhouseOrOutsider);
					dataList.add(schoolVerified);
					dataList.add(createdAt);
					dataList.add(verifiedAt);
					dataList.add(isPartOfSport);
					dataList.add(StringUtils.isEmpty(sportDetails) || "N".equals(isPartOfSport) ? "" : sportDetails);
					addRowInWorkBook(sheet, dataList, rowIndex);
					rowIndex++;
				}
				}catch(NullPointerException e){
					e.printStackTrace();
					continue;
				}
			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

	private byte[] exportReportForPathfinder(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");
			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Student Gender");
			data.add("Address Line 1");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Any preschool schools attended prior");
			data.add("Languages spoken at home");
			data.add("Mother First Name");
			data.add("Mother Middle Name");
			data.add("Mother Surname");
			data.add("Mother Age");
			data.add("Mother Citizenship");
			data.add("Mother School");
			data.add("Mother College /Post Graduate");
			data.add("Mother Occupation ");
			data.add("Mother Organization Name ");
			data.add("Mother Primary Phone ");
			data.add("Mother Secondary Phone");
			data.add("Mother Email");
			data.add("Mother Office Address");
			data.add("Mother Hobbies");
			data.add("Father First Name");
			data.add("Father Middle Name");
			data.add("Father Surname");
			data.add("Father Age");
			data.add("Father Citizenship");
			data.add("Father School");
			data.add("Father College /Post Graduate");
			data.add("Father Occupation ");
			data.add("Father Organization Name ");
			data.add("Father Primary Phone ");
			data.add("Father Secondary Phone");
			data.add("Father Email");
			data.add("Father Office Address");
			data.add("Father Hobbies");
			data.add("Sibling First Name");
			data.add("Sibling Middle Name");
			data.add("Sibling Surname");
			data.add("Sibling Date of Birth (DD/MM/YYYY)");
			data.add("Sibling Current School ");
			data.add("Sibling Other Pre-schools / Schools Attended");
			data.add("How did you hear about us ");
			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
					boolean isMarried = (candidateDetails.getMaritalStatus() != null &&
									candidateDetails.getMaritalStatus().getName().equalsIgnoreCase("MARRIED")) ? true : false;
					boolean isMale = candidateDetails.getGender().getName().equalsIgnoreCase("Male") ? true : false;

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());

					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(address);
					dataList.add(dob);
					dataList.add(!StringUtils.isEmpty(candidateDetails.getLastSchoolName()) ? candidateDetails.getLastSchoolName() : "");
					dataList.add(!StringUtils.isEmpty(candidateDetails.getLanguagesKnown()) ? candidateDetails.getLanguagesKnown() : "");

					ParentDetail motherDetail = onlineAdmission.getMotherDetails();
					Name motherName = motherDetail.getName();
					dataList.add(!StringUtils.isEmpty(motherName.getFirstname()) ? motherName.getFirstname() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getMiddlename()) ? motherName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getSurname()) ? motherName.getSurname() : "");
					dataList.add((motherDetail.getAge() != 0) ? "" + motherDetail.getAge() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getCitizenship()) ? motherDetail.getCitizenship() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getSchoolDetail()) ? motherDetail.getSchoolDetail() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getCollegDetail()) ? motherDetail.getCollegDetail() : "");

					EmploymentDetails motherEmploymentDetail = motherDetail.getEmploymentDetails();
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getOccupation()) ? motherEmploymentDetail.getOccupation() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getEmployerName()) ? motherEmploymentDetail.getEmployerName() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getPrimaryPhone()) ? motherDetail.getPrimaryPhone() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getSecondaryPhone()) ? motherDetail.getSecondaryPhone() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getEmail()) ? motherDetail.getEmail() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getAddress()) ? motherEmploymentDetail.getAddress() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getHobbies()) ? motherDetail.getHobbies() : "");

					ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
					if (fatherDetail != null) {
						Name fatherName = fatherDetail.getName();
						dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");
						dataList.add((fatherDetail.getAge() != 0) ? "" + motherDetail.getAge() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getCitizenship()) ? fatherDetail.getCitizenship() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getSchoolDetail()) ? fatherDetail.getSchoolDetail() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getCollegDetail()) ? fatherDetail.getCollegDetail() : "");

						EmploymentDetails fatherEmploymentDetail = fatherDetail.getEmploymentDetails();
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getOccupation()) ? fatherEmploymentDetail.getOccupation() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getEmployerName()) ? fatherEmploymentDetail.getEmployerName() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getPrimaryPhone()) ? fatherDetail.getPrimaryPhone() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getSecondaryPhone()) ? fatherDetail.getSecondaryPhone() : "");

						dataList.add(!StringUtils.isEmpty(fatherDetail.getEmail()) ? fatherDetail.getEmail() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getAddress()) ? fatherEmploymentDetail.getAddress() : "");

						dataList.add(!StringUtils.isEmpty(fatherDetail.getHobbies()) ? fatherDetail.getHobbies() : "");
					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					List<SiblingDetail> siblingDetails = onlineAdmission.getSiblingDetail();
					if (siblingDetails != null) {
						SiblingDetail siblingDetail = siblingDetails.get(0);
						Name siblingName = siblingDetail.getName();
						dataList.add(!StringUtils.isEmpty(siblingName.getFirstname()) ? siblingName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(siblingName.getMiddlename()) ? siblingName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(siblingName.getSurname()) ? siblingName.getSurname() : "");
						dataList.add(!StringUtils.isEmpty(siblingDetail.getDateofbirth()) ? siblingDetail.getDateofbirth() : "");
						dataList.add(!StringUtils.isEmpty(siblingDetail.getCurrentSchool()) ? siblingDetail.getCurrentSchool() : "");
						dataList.add(!StringUtils.isEmpty(siblingDetail.getOtherschoolAttended()) ? siblingDetail.getOtherschoolAttended() : "");

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getGeneralComment()) ? onlineAdmission.getGeneralComment() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

	private byte[] exportReportForASCEND(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");
			data.add("Course Preference");

			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Student Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Student Age Year");
			data.add("Student Age Month");

			data.add("Guardian First Name");
			data.add("Guardian Middle Name");
			data.add("Guardian Surname");
			data.add("Guardian Occupation ");
			data.add("Guardian Organization Name ");
			data.add("Guardian Secondary Phone");
			data.add("Guardian PAN Number");

			data.add("Relationship to Child");
			data.add("Guardian Primary Phone ");
			data.add("Guardian Email");
			data.add("Guardian Address");
			data.add("Guardian City");
			data.add("Postal Code");

			data.add("Other Guardian First Name");
			data.add("Other Guardian Middle Name");
			data.add("Other Guardian Surname");
			data.add("Other Guardian Primary Phone ");
			data.add("Other Guardian Secondary Phone");
			data.add("Other Guardian Email");
			data.add("Other Guardian Address");
			data.add("Other Guardian City");
			data.add("Other Guardian Postal Code");
			data.add("Other Guardian Occupation ");
			data.add("Other Guardian Organization Name ");
			data.add("Other Guardian PAN Number");
			data.add("Other Guardian Relationship to Child");

			data.add("Sibling First Name");
			data.add("Sibling Middle Name");
			data.add("Sibling Surname");
			data.add("Sibling Date of Birth (DD/MM/YYYY)");
			data.add("Sex M/F");
			data.add("Grade Apply");

			data.add("Previous School");
			data.add("Dates attended");
			data.add("Grade(s)");
			data.add("Previous School Address");
			data.add("Language od Instruction");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
					boolean isMarried = (candidateDetails.getMaritalStatus() != null &&
									candidateDetails.getMaritalStatus().getName().equalsIgnoreCase("MARRIED")) ? true : false;
					boolean isMale = candidateDetails.getGender().getName().equalsIgnoreCase("Male") ? true : false;

					String gender = candidateDetails.getGender().getName();
					String dob = candidateDetails.getDateofbirth();
					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());
					String ageYear = candidateDetails.getAgeYear();
					String ageMonth = candidateDetails.getAgeMonth();

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());

					String couresePreferenceStr = "";
					List<CoursePreference> couresePreferenceList = onlineAdmission.getCourseDetails().getCoursePreferenceList();
					if (!CollectionUtils.isEmpty(couresePreferenceList)) {

						for (CoursePreference coursePreference : couresePreferenceList) {
							couresePreferenceStr += coursePreference.getName();
						}

					}
					dataList.add(couresePreferenceStr);

					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(dob);
					dataList.add(ageYear);
					dataList.add(ageMonth);

					ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
					if (fatherDetail != null) {
						Name fatherName = fatherDetail.getName();
						dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");

						EmploymentDetails fatherEmploymentDetail = fatherDetail.getEmploymentDetails();
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getOccupation()) ? fatherEmploymentDetail.getOccupation() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getEmployerName()) ? fatherEmploymentDetail.getEmployerName() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getSecondaryPhone()) ? fatherDetail.getSecondaryPhone() : "");

						dataList.add(!StringUtils.isEmpty(fatherDetail.getPanNumber()) ? fatherDetail.getPanNumber() : "");

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					GuardianDetails relation = onlineAdmission.getGuardianDetails();
					if (relation != null) {
						dataList.add(!StringUtils.isEmpty(relation.getCandidaterelation()) ? relation.getCandidaterelation() : "");
					} else {
						dataList.add("");
					}

					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");

					dataList.add(address);
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCity()) ? onlineAdmission.getAddressDetails()
									.getLocalAddress().getCity() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission.getAddressDetails()
									.getLocalAddress().getPincode().toString() : "");

					ParentDetail motherDetail = onlineAdmission.getMotherDetails();
					Name motherName = motherDetail.getName();
					dataList.add(!StringUtils.isEmpty(motherName.getFirstname()) ? motherName.getFirstname() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getMiddlename()) ? motherName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getSurname()) ? motherName.getSurname() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getPrimaryPhone()) ? motherDetail.getPrimaryPhone() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getSecondaryPhone()) ? motherDetail.getSecondaryPhone() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getEmail()) ? motherDetail.getEmail() : "");

					EmploymentDetails motherEmploymentDetail = motherDetail.getEmploymentDetails();
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getAddress()) ? motherEmploymentDetail.getAddress() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getCity()) ? motherEmploymentDetail.getCity() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getPincode()) ? motherEmploymentDetail.getPincode() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getOccupation()) ? motherEmploymentDetail.getOccupation() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getEmployerName()) ? motherEmploymentDetail.getEmployerName() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getPanNumber()) ? motherDetail.getPanNumber() : "");

					if (relation != null) {
						dataList.add(!StringUtils.isEmpty(relation.getRelation()) ? relation.getRelation() : "");
					} else {
						dataList.add("");
					}

					List<SiblingDetail> siblingDetails = onlineAdmission.getSiblingDetail();
					if (siblingDetails != null) {
						SiblingDetail siblingDetail = siblingDetails.get(0);
						Name siblingName = siblingDetail.getName();
						dataList.add(!StringUtils.isEmpty(siblingName.getFirstname()) ? siblingName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(siblingName.getMiddlename()) ? siblingName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(siblingName.getSurname()) ? siblingName.getSurname() : "");
						dataList.add(!StringUtils.isEmpty(siblingDetail.getDateofbirth()) ? siblingDetail.getDateofbirth() : "");
						dataList.add(!StringUtils.isEmpty(siblingDetail.getGender()) ? siblingDetail.getGender().getName().toUpperCase() : "");
						dataList.add(!StringUtils.isEmpty(siblingDetail.getGrade()) ? siblingDetail.getGrade().toString() : "");

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					QualifyingExamDetails academic = onlineAdmission.getQualifyingExamDetails();
					if (academic != null) {
						dataList.add(!StringUtils.isEmpty(academic.getSchoolCollege()) ? academic.getSchoolCollege() : "");
						dataList.add(!StringUtils.isEmpty(academic.getDatesAttend()) ? academic.getDatesAttend() : "");
						dataList.add(!StringUtils.isEmpty(academic.getGrade()) ? academic.getGrade() : "");
						dataList.add(!StringUtils.isEmpty(academic.getAddress()) ? academic.getAddress() : "");
						dataList.add(!StringUtils.isEmpty(academic.getLanguageInstruction()) ? academic.getLanguageInstruction() : "");

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

	private byte[] exportReportForRosemine(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");
			data.add("Form Number");
			data.add("Course Preference");

			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Student Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Student Educational Qualification");
			data.add("Student Category");
			data.add("Location");

			data.add("Mother First Name");
			data.add("Mother Middle Name");
			data.add("Mother Surname");
			data.add("Mother Primary Phone ");
			data.add("Mother Email");
			data.add("Father First Name");
			data.add("Father Middle Name");
			data.add("Father Surname");
			data.add("Father Occupation ");
			data.add("Father Annual Income ");
			data.add("Father Primary Phone ");
			data.add("Father Email");
			data.add("Parent Address");
			data.add("Mailing Address");
			data.add("How did you hear about us ");
			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");

			data.add("Location");
			data.add("Course Preference");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
					boolean isMarried = (candidateDetails.getMaritalStatus() != null &&
									candidateDetails.getMaritalStatus().getName().equalsIgnoreCase("MARRIED")) ? true : false;
					boolean isMale = candidateDetails.getGender().getName().equalsIgnoreCase("Male") ? true : false;

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());
					dataList.add("" + onlineAdmission.getFormNumber());

					String couresePreferenceStr = "";
					List<CoursePreference> couresePreferenceList = onlineAdmission.getCourseDetails().getCoursePreferenceList();
					if (!CollectionUtils.isEmpty(couresePreferenceList)) {

						for (CoursePreference coursePreference : couresePreferenceList) {
							couresePreferenceStr += coursePreference.getName() + ",";
						}

					}
					dataList.add(couresePreferenceStr);

					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(dob);
					dataList.add((candidateDetails.getEducationalqualifications() != null) ? candidateDetails.getEducationalqualifications().getName() : "");
					dataList.add((candidateDetails.getCategory() != null) ? candidateDetails.getCategory().getName() : "");

					String locationStr = "";
					List<Location> locationList = onlineAdmission.getCandidateDetails().getLocation();
					if (!CollectionUtils.isEmpty(locationList)) {

						for (Location locaton : locationList) {
							locationStr += locaton.getName() + ",";
						}

					}
					dataList.add(locationStr);

					ParentDetail motherDetail = onlineAdmission.getMotherDetails();
					Name motherName = motherDetail.getName();
					dataList.add(!StringUtils.isEmpty(motherName.getFirstname()) ? motherName.getFirstname() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getMiddlename()) ? motherName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getSurname()) ? motherName.getSurname() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getPrimaryPhone()) ? motherDetail.getPrimaryPhone() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getEmail()) ? motherDetail.getEmail() : "");

					ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
					if (fatherDetail != null) {
						Name fatherName = fatherDetail.getName();
						dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");

						EmploymentDetails fatherEmploymentDetail = fatherDetail.getEmploymentDetails();
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getOccupation()) ? fatherEmploymentDetail.getOccupation() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getAnnualIncome()) ? fatherEmploymentDetail.getAnnualIncome() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getPrimaryPhone()) ? fatherDetail.getPrimaryPhone() : "");

						dataList.add(!StringUtils.isEmpty(fatherDetail.getEmail()) ? fatherDetail.getEmail() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getAddress()) ? fatherEmploymentDetail.getAddress() : "");

						dataList.add(onlineAdmission.getAddressDetails().getLocalAddress().getAddressLine1());

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getGeneralComment()) ? onlineAdmission.getGeneralComment() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

	private byte[] exportReportForHkimsr(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");

			data.add("Course Preference");
			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Student Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Address");
			data.add("Student Educational Qualification");
			data.add("Student Category");
			data.add("Mobile Number");
			data.add("Email Id");
			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");

			data.add("Course Preference");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());

					String couresePreferenceStr = "";
					List<CoursePreference> couresePreferenceList = onlineAdmission.getCourseDetails().getCoursePreferenceList();
					if (!CollectionUtils.isEmpty(couresePreferenceList)) {

						for (CoursePreference coursePreference : couresePreferenceList) {
							couresePreferenceStr += coursePreference.getName() + ",";
						}

					}
					dataList.add(couresePreferenceStr);

					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(dob);
					dataList.add(address);
					dataList.add((candidateDetails.getEducationalqualifications() != null) ? candidateDetails.getEducationalqualifications().getName() : "");
					dataList.add((candidateDetails.getCategory() != null) ? candidateDetails.getCategory().getName() : "");

					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
	
	private byte[] exportReportForIIHMR(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");
			data.add("Course Detail");
			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Student Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Address");
			data.add("Mobile Number");
			data.add("Email Id");
			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");

			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());

					String couresePreferenceStr = "";
					List<CoursePreference> couresePreferenceList = onlineAdmission.getCourseDetails().getCoursePreferenceList();
					if (!CollectionUtils.isEmpty(couresePreferenceList)) {

						for (CoursePreference coursePreference : couresePreferenceList) {
							couresePreferenceStr += coursePreference.getName() + ",";
						}

					}
					dataList.add(couresePreferenceStr);

					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(dob);
					dataList.add(address);
					
					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
	
	private byte[] exportReportForEtoos(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			
			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Student Gender");
			data.add("Date of Birth");
			
			data.add("Mobile Number");
			data.add("Email Id");
			data.add("Address");
			data.add("Country");
			data.add("State");
			data.add("City");
			data.add("Postel Code");
			
			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");

			data.add("Course Preference");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					
					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(dob);
					
					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");
					
					dataList.add(address);
					
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCountry()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getCountry().getName() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getState()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getState().getName() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getDistrict().getName() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getPincode().toString() : "");
			
			
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
	
	private byte[] exportReportForRtac(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			
			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Student Gender");
			data.add("Date of Birth");
			
			data.add("Mobile Number");
			data.add("Email Id");
			data.add("Address");
			data.add("Country");
			data.add("State");
			data.add("City");
			data.add("Postel Code");
			
			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");

			data.add("Course Preference");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					
					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(dob);
					
					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");
					
					dataList.add(address);
					
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCountry()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getCountry().getName() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getState()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getState().getName() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getDistrict().getName() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getPincode().toString() : "");
			
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
	
	
	private byte[] exportReportForVbu(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");

			//data.add("Course Preference");
			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");

			data.add("Mother First Name");
			data.add("Mother Middle Name");
			data.add("Mother Surname");

			data.add("Father First Name");
			data.add("Father Middle Name");
			data.add("Father Surname");

			data.add("Student Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			//data.add("Address");
			//data.add("Student Educational Qualification");
			//data.add("Student Category");
			data.add("Mobile Number");
			data.add("Email Id");
			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");

			//data.add("Course Preference");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					//String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());

					/*String couresePreferenceStr = "";
					List<CoursePreference> couresePreferenceList = onlineAdmission.getCourseDetails().getCoursePreferenceList();
					if (!CollectionUtils.isEmpty(couresePreferenceList)) {
						for (CoursePreference coursePreference : couresePreferenceList) {
							couresePreferenceStr += coursePreference.getName() + ",";
						}
					}
					dataList.add(couresePreferenceStr);
					*/
					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");

					ParentDetail motherDetail = onlineAdmission.getMotherDetails();
					Name motherName = motherDetail.getName();
					dataList.add(!StringUtils.isEmpty(motherName.getFirstname()) ? motherName.getFirstname() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getMiddlename()) ? motherName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getSurname()) ? motherName.getSurname() : "");

					ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
					Name fatherName = fatherDetail.getName();
					dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
					dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");

					dataList.add(gender);
					dataList.add(dob);
					//dataList.add(address);
					//dataList.add((candidateDetails.getEducationalqualifications() != null) ? candidateDetails.getEducationalqualifications().getName() : "");
					//dataList.add((candidateDetails.getCategory() != null) ? candidateDetails.getCategory().getName() : "");

					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

	private byte[] exportReportForScp(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");

			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");

			data.add("Father First Name");
			data.add("Father Middle Name");
			data.add("Father Surname");
			data.add("Student Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Residential Address");
			data.add("Caste Category");
			data.add("Religion ");

			data.add("SSC Year Of Passing");
			data.add("SSC Board/University");
			data.add("SSC Marks Obtained in Physical Sc");
			data.add("SSC MArks Obtained in Math");
			data.add("SSC Percentage");

			data.add("Mobile Number");
			data.add("Email Id");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");

			data.add("Category");

			//data.add("Course Preference");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					String qexam = "";

					if (onlineAdmission.getQualifyingExamDetails() != null && onlineAdmission.getQualifyingExamDetails().getExam() != null) {
						qexam = onlineAdmission.getQualifyingExamDetails().getExam().getName();
					}
					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					//dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());

					String couresePreferenceStr = "";
					List<CoursePreference> couresePreferenceList = onlineAdmission.getCourseDetails().getCoursePreferenceList();
					if (!CollectionUtils.isEmpty(couresePreferenceList)) {

						for (CoursePreference coursePreference : couresePreferenceList) {
							couresePreferenceStr += coursePreference.getName() + ",";
						}

					}
					dataList.add(couresePreferenceStr);

					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");

					ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
					Name fatherName = fatherDetail.getName();
					dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
					dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");

					dataList.add(gender);
					dataList.add(dob);
					dataList.add(address);

					CasteDetails casteDetail = candidateDetails.getCasteDetails();
					String casteCategory = casteDetail == null ? "" : (casteDetail.getCaste() == null ? "" : casteDetail.getCaste().getName());
					String religion = casteDetail == null ? "" : casteDetail.getReligion().getName();

					dataList.add(casteCategory);
					dataList.add(religion);

					AcademicDetail ssc = onlineAdmission.getAcademicDetails().get(0);
					String sscYearOfPassing = ssc.getPassingYear();
					String sscBoard = ssc.getBoardUniversity();
					String sscObtainedMarksinPhySc = (ssc.getSubjectMarks() != null ? ssc.getSubjectMarks().get("physicalSc") + "" : "");
					String sscObtainedMarksinMeth = (ssc.getSubjectMarks() != null ? ssc.getSubjectMarks().get("maths") + "" : "");
					String sscPercentage = ssc.getPercentage() + "";

					dataList.add(sscYearOfPassing);
					dataList.add(sscBoard);
					dataList.add(sscObtainedMarksinPhySc);
					dataList.add(sscObtainedMarksinMeth);
					dataList.add(sscPercentage);

					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					dataList.add(qexam);

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

	private byte[] exportReportForIITM(List<OnlineAdmission> onlineAdmissionList) {

		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");

			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");

			data.add("Student Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Residential Address");
			data.add("Mobile Number");
			data.add("Email Address");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");

			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();

					String dob = candidateDetails.getDateofbirth();

					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());

					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");

					dataList.add(gender);
					dataList.add(dob);
					dataList.add(address);

					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

	private void addRowInWorkBook(XSSFSheet sheet, List<String> dataList, int rowIndex) {
		XSSFRow row = sheet.createRow(rowIndex);
		int index = 0;
		for (String str : dataList)
		{
			Cell cell = row.createCell(index);
			cell.setCellValue(str);
			index++;
		}

	}

	private String getAddress(Address localAddress) {

		if (localAddress != null) {
			List<String> addressDetails = new LinkedList<String>();

			if (!StringUtils.isEmpty(localAddress.getHouseNumber())) {
				addressDetails.add(localAddress.getHouseNumber());
			}

			if (!StringUtils.isEmpty(localAddress.getAddressLine1())) {
				addressDetails.add(localAddress.getAddressLine1());
			}

			if (!StringUtils.isEmpty(localAddress.getArea())) {
				addressDetails.add(localAddress.getArea());
			}

			if (localAddress.getTaluka() != null && !StringUtils.isEmpty(localAddress.getTaluka())) {
				addressDetails.add(localAddress.getTaluka().getName());
			}

			if (localAddress.getDistrict() != null && !StringUtils.isEmpty(localAddress.getDistrict())) {
				addressDetails.add(localAddress.getDistrict().getName());
			}

			if (localAddress.getState() != null && !StringUtils.isEmpty(localAddress.getState())) {
				addressDetails.add(localAddress.getState().getName());
			}

			if (localAddress.getCountry() != null && !StringUtils.isEmpty(localAddress.getCountry())) {
				addressDetails.add(localAddress.getCountry().getName());
			}

			return StringUtils.collectionToCommaDelimitedString(addressDetails);

		} else {
			return "";
		}

	}

	private String getStudentName(CandidateDetails candidateDetails, ParentDetail motherDetail) {
		Name name = candidateDetails.getName();
		return name.getSurname() + " " + name.getFirstname() + " " + (!StringUtils.isEmpty(name.getMiddlename()) ? name.getMiddlename() + " " : "")
						+ (motherDetail.getName() != null ? motherDetail.getName().getFirstname() : "");
	}

	private String getTitle(boolean isMarried, boolean isMale) {
		if (isMale) {
			return "Mr";
		} else if (isMarried) {
			return "Mrs";
		} else {
			return "Ms";
		}
	}

	public static void main(String args[]) throws Exception {
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet("new sheet");
		Row row = sheet.createRow((short) 2);

		Set<String> data = new LinkedHashSet<String>();
		data.add("Title");
		data.add("STUDENT NAME");
		data.add("Gender");
		data.add("Married / Unmarried");
		data.add("Date Of Birth");
		data.add("Place Of Birth");
		data.add("Caste Category");
		data.add("Sub Caste");
		data.add("Religion ");
		data.add("Physical Handicapped");
		data.add("Application Form No");
		data.add("Physcial Form No");
		data.add("Mother Tongue");
		data.add("Aadhar Card No");
		data.add("Bank Account No");
		data.add("Bank Name");
		data.add("Last School");
		data.add("Social Reservation");
		data.add("LOCAL ADDRESS ");
		data.add("Native Place Address");
		data.add("Mobile No");
		data.add("Parents Ph. No.");
		data.add("Email Id.");

		int index = 0;
		for (String str : data)
		{
			System.out.println("Index ::" + index + " str::" + str);
			row.createCell(index).setCellValue(str);
			index++;
		}

		/*row.createCell(0).setCellValue(1.1);
		row.createCell(1).setCellValue("");
		row.createCell(2).setCellValue(Calendar.getInstance());
		row.createCell(3).setCellValue("a string");
		row.createCell(4).setCellValue(true);
		row.createCell(5).setCellValue("a string");
		row.createCell(6).setCellValue("a string");
		row.createCell(7).setCellValue("a string");
		row.createCell(8).setCellValue("a string");
		row.createCell(9).setCellValue("a string");
		row.createCell(10).setCellValue("a string");
		row.createCell(11).setCellValue("a string");
		row.createCell(12).setCellValue("a string");
		row.createCell(13).setCellValue("a string");
		row.createCell(14).setCellValue("a string");
		row.createCell(15).setCellValue("a string");
		row.createCell(16).setCellValue("a string");
		row.createCell(17).setCellValue("a string");
		row.createCell(18).setCellValue("a string");
		row.createCell(19).setCellValue("a string");
		row.createCell(20).setCellValue("a string");
		row.createCell(21).setCellValue("a string");
		row.createCell(22).setCellValue("a string");*/

		// Write the output to a file
		FileOutputStream fileOut = new FileOutputStream("c:\\temp\\workbook.xls");
		wb.write(fileOut);
		fileOut.close();
	}
	
	private byte[] exportReportForLHS(List<OnlineAdmission> onlineAdmissionList) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");

			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Blood Group Of Child");
			data.add("Category");
			data.add("Religion");

			data.add("Father First Name");
			data.add("Father Middle Name");
			data.add("Father Surname");
			data.add("Father office Phone");
			data.add("Father Occupation");
			data.add("Name of Company");
			data.add("Designation & Dept.");
			data.add("Parent status");
			data.add("Office Address");
			
			data.add("Father Phone No.");
			data.add("Father Email");
			
			data.add("Distance from School");
			data.add("Address");
			data.add("City");
			data.add("Postal Code");
			data.add("District");
			data.add("State");
			
			data.add("Mother First Name");
			data.add("Mother Middle Name");
			data.add("Mother Surname");
			data.add("Mother Primary Phone ");
			data.add("Mother Office Phone");
			data.add("Mother Email");
			
			data.add("Mother Occupation ");
			data.add("Mother Organization Name");
			data.add("Designation & Dept.");
			data.add("Parent status");
			data.add("Office Address");
			
			data.add("Sibling First Name");
			data.add("Sibling Middle Name");
			data.add("Sibling Surname");
			data.add("Class/Section");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();
					String dob = candidateDetails.getDateofbirth();
					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());
					String category = onlineAdmission.getCandidateDetails().getCasteDetails().getCaste() != null ? onlineAdmission
							.getCandidateDetails().getCasteDetails().getCaste().getName() : "";

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());


					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(dob);
					dataList.add(!StringUtils.isEmpty(candidateDetails.getBloodGroup())? candidateDetails.getBloodGroup() : "");
				    dataList.add(category);
				    dataList.add(!StringUtils.isEmpty(candidateDetails.getCasteDetails().getReligion().getName())? candidateDetails.getCasteDetails().getReligion().getName() : "");

					ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
					if (fatherDetail != null) {
						Name fatherName = fatherDetail.getName();
						dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getSecondaryPhone()) ? fatherDetail.getSecondaryPhone() : "");
						
						EmploymentDetails fatherEmploymentDetail = fatherDetail.getEmploymentDetails();
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getOccupation()) ? fatherEmploymentDetail.getOccupation() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getEmployerName()) ? fatherEmploymentDetail.getEmployerName() : "");
						
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getDesignation()) ? fatherEmploymentDetail.getDesignation() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getStatus()) ? fatherDetail.getStatus() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getAddress()) ? fatherEmploymentDetail.getAddress() : "");

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getDistanceFromSchool())? onlineAdmission.getAddressDetails().getDistanceFromSchool() : "");
					dataList.add(address);
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCity()) ? onlineAdmission.getAddressDetails()
									.getLocalAddress().getCity() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission.getAddressDetails()
									.getLocalAddress().getPincode().toString() : "");
					
					
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getDistrict().getName() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getState()) ? onlineAdmission.getAddressDetails()
					.getLocalAddress().getState().getName() : "");
	

					ParentDetail motherDetail = onlineAdmission.getMotherDetails();
					Name motherName = motherDetail.getName();
					dataList.add(!StringUtils.isEmpty(motherName.getFirstname()) ? motherName.getFirstname() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getMiddlename()) ? motherName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getSurname()) ? motherName.getSurname() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getPrimaryPhone()) ? motherDetail.getPrimaryPhone() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getSecondaryPhone()) ? motherDetail.getSecondaryPhone() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getEmail()) ? motherDetail.getEmail() : "");

					EmploymentDetails motherEmploymentDetail = motherDetail.getEmploymentDetails();
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getOccupation()) ? motherEmploymentDetail.getOccupation() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getEmployerName()) ? motherEmploymentDetail.getEmployerName() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getDesignation()) ? motherEmploymentDetail.getDesignation() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getStatus()) ? motherDetail.getStatus() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getAddress()) ? motherEmploymentDetail.getAddress() : "");


					List<SiblingDetail> siblingDetails = onlineAdmission.getSiblingDetail();
					boolean siblingAvailable = false;
					if (siblingDetails != null && siblingDetails.size() > 0) {
						SiblingDetail siblingDetail = siblingDetails.get(0);
						if(siblingDetail!= null ){
							Name siblingName = siblingDetail.getName();
							if(siblingName != null){
								siblingAvailable= true;
								dataList.add(!StringUtils.isEmpty(siblingName.getFirstname()) ? siblingName.getFirstname() : "");
								dataList.add(!StringUtils.isEmpty(siblingName.getMiddlename()) ? siblingName.getMiddlename() : "");
								dataList.add(!StringUtils.isEmpty(siblingName.getSurname()) ? siblingName.getSurname() : "");
								dataList.add(!StringUtils.isEmpty(siblingDetail.getGrade()) ? siblingDetail.getGrade().toString() : "");
							}
						}
					} 
					if(!siblingAvailable) {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
	
	
	private byte[] exportReportForLHE(List<OnlineAdmission> onlineAdmissionList) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Course Name");

			data.add("Student First Name");
			data.add("Student Middle Name");
			data.add("Student Surname");
			data.add("Gender");
			data.add("Date of Birth (DD/MM/YYYY)");
			data.add("Blood Group Of Child");
			data.add("Category");
			data.add("Religion");

			data.add("Father First Name");
			data.add("Father Middle Name");
			data.add("Father Surname");
			data.add("Father office Phone");
			data.add("Father Occupation");
			data.add("Name of Company");
			data.add("Designation & Dept.");
			data.add("Parent status");
			data.add("Office Address");
			
			data.add("Father Phone No.");
			
			data.add("Father Email");
			data.add("Distance from School");
			data.add("Address");
			data.add("City");
			data.add("Postal Code");
			data.add("District");
			data.add("State");
			
			data.add("Mother First Name");
			data.add("Mother Middle Name");
			data.add("Mother Surname");
			data.add("Mother Primary Phone ");
			data.add("Mother Office Phone");
			data.add("Mother Email");
			
			data.add("Mother Occupation ");
			data.add("Mother Organization Name");
			data.add("Designation & Dept.");
			data.add("Parent status");
			data.add("Office Address");
			
			data.add("Sibling First Name");
			data.add("Sibling Middle Name");
			data.add("Sibling Surname");
			data.add("Class/Section");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();

					String gender = candidateDetails.getGender().getName();
					String dob = candidateDetails.getDateofbirth();
					String address = getAddress(onlineAdmission.getAddressDetails().getLocalAddress());
					String category = onlineAdmission.getCandidateDetails().getCasteDetails().getCaste() != null ? onlineAdmission
							.getCandidateDetails().getCasteDetails().getCaste().getName() : "";;

					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					dataList.add(onlineAdmission.getCourseDetails().getCourse().getName());


					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(gender);
					dataList.add(dob);
					dataList.add(!StringUtils.isEmpty(candidateDetails.getBloodGroup())? candidateDetails.getBloodGroup() : "");
				    dataList.add(category);
				    dataList.add(!StringUtils.isEmpty(candidateDetails.getCasteDetails().getReligion().getName())? candidateDetails.getCasteDetails().getReligion().getName() : "");

					ParentDetail fatherDetail = onlineAdmission.getFatherDetails();
					if (fatherDetail != null) {
						Name fatherName = fatherDetail.getName();
						dataList.add(!StringUtils.isEmpty(fatherName.getFirstname()) ? fatherName.getFirstname() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getMiddlename()) ? fatherName.getMiddlename() : "");
						dataList.add(!StringUtils.isEmpty(fatherName.getSurname()) ? fatherName.getSurname() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getSecondaryPhone()) ? fatherDetail.getSecondaryPhone() : "");
						
						EmploymentDetails fatherEmploymentDetail = fatherDetail.getEmploymentDetails();
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getOccupation()) ? fatherEmploymentDetail.getOccupation() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getEmployerName()) ? fatherEmploymentDetail.getEmployerName() : "");
						
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getDesignation()) ? fatherEmploymentDetail.getDesignation() : "");
						dataList.add(!StringUtils.isEmpty(fatherDetail.getStatus()) ? fatherDetail.getStatus() : "");
						dataList.add(!StringUtils.isEmpty(fatherEmploymentDetail.getAddress()) ? fatherEmploymentDetail.getAddress() : "");

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					ContactDetails cd = onlineAdmission.getContactDetails();
					dataList.add(!StringUtils.isEmpty(cd.getPersonalMobileNumber()) ? cd.getPersonalMobileNumber() : "");
					dataList.add(!StringUtils.isEmpty(cd.getPersonalEmail()) ? cd.getPersonalEmail() : "");

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getDistanceFromSchool())? onlineAdmission.getAddressDetails().getDistanceFromSchool() : "");
					dataList.add(address);
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCity()) ? onlineAdmission.getAddressDetails()
									.getLocalAddress().getCity() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission.getAddressDetails()
									.getLocalAddress().getPincode().toString() : "");
					
					
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()) ? onlineAdmission.getAddressDetails()
							.getLocalAddress().getDistrict().getName() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getState()) ? onlineAdmission.getAddressDetails()
					.getLocalAddress().getState().getName() : "");
	

					ParentDetail motherDetail = onlineAdmission.getMotherDetails();
					Name motherName = motherDetail.getName();
					dataList.add(!StringUtils.isEmpty(motherName.getFirstname()) ? motherName.getFirstname() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getMiddlename()) ? motherName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(motherName.getSurname()) ? motherName.getSurname() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getPrimaryPhone()) ? motherDetail.getPrimaryPhone() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getSecondaryPhone()) ? motherDetail.getSecondaryPhone() : "");

					dataList.add(!StringUtils.isEmpty(motherDetail.getEmail()) ? motherDetail.getEmail() : "");

					EmploymentDetails motherEmploymentDetail = motherDetail.getEmploymentDetails();
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getOccupation()) ? motherEmploymentDetail.getOccupation() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getEmployerName()) ? motherEmploymentDetail.getEmployerName() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getDesignation()) ? motherEmploymentDetail.getDesignation() : "");
					dataList.add(!StringUtils.isEmpty(motherDetail.getStatus()) ? motherDetail.getStatus() : "");
					dataList.add(!StringUtils.isEmpty(motherEmploymentDetail.getAddress()) ? motherEmploymentDetail.getAddress() : "");


					List<SiblingDetail> siblingDetails = onlineAdmission.getSiblingDetail();
					boolean siblingAvailable = false;
					if (siblingDetails != null && siblingDetails.size() > 0) {
						SiblingDetail siblingDetail = siblingDetails.get(0);
						if(siblingDetail!= null ){
							Name siblingName = siblingDetail.getName();
							if(siblingName != null){
								siblingAvailable= true;
								dataList.add(!StringUtils.isEmpty(siblingName.getFirstname()) ? siblingName.getFirstname() : "");
								dataList.add(!StringUtils.isEmpty(siblingName.getMiddlename()) ? siblingName.getMiddlename() : "");
								dataList.add(!StringUtils.isEmpty(siblingName.getSurname()) ? siblingName.getSurname() : "");
								dataList.add(!StringUtils.isEmpty(siblingDetail.getGrade()) ? siblingDetail.getGrade().toString() : "");
							}
						}
					} 
					if(!siblingAvailable) {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}

					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			}

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
	
	private byte[] exportReportForIITKR(List<OnlineAdmission> onlineAdmissionList) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Student First Name");
			data.add("Student Surname");
			data.add("Department");
			data.add("year of Graduation");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
				
					dataList.add(onlineAdmission.getAcademicDetails().get(0).getDepartment());
					
					try{
						dataList.add(onlineAdmission.getAcademicDetails().get(0).getYear());
					}catch(NullPointerException e){
						
					}finally{
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

						addRowInWorkBook(sheet, dataList, rowIndex);

						rowIndex++;

					}

					} else {
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							dataList.add("");
							
							dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
							dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
							dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
							dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
							dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

							addRowInWorkBook(sheet, dataList, rowIndex);

							rowIndex++;

					}
				}

			

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
	
	private byte[] exportReportForIITKM(List<OnlineAdmission> onlineAdmissionList) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Student First Name");
			data.add("Student Surname");
			data.add("Department");
			data.add("year of Graduation");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					
					try{
						
					dataList.add(onlineAdmission.getAcademicDetails().get(0).getDepartment());
					dataList.add(onlineAdmission.getAcademicDetails().get(0).getYear());
					}catch(NullPointerException e){
						
					}finally{
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

						addRowInWorkBook(sheet, dataList, rowIndex);

						rowIndex++;
					}
					

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
						dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

						addRowInWorkBook(sheet, dataList, rowIndex);

						rowIndex++;
					}	
					

				}

			

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}
private byte[] exportReportForIITKD(List<OnlineAdmission> onlineAdmissionList) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream outputStream = null;
		byte[] dataBytes = null;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			outputStream = new ByteArrayOutputStream();
			XSSFSheet sheet = workbook.createSheet("Admission Report");
			Set<String> data = new LinkedHashSet<String>();
			data.add("Application ID");
			data.add("Student First Name");
			data.add("Student Surname");
			data.add("Department");
			data.add("year of Graduation");

			data.add("Payment Status");
			data.add("Payment Date");
			data.add("Payment Qfix Reference Number");
			data.add("Payment Amount");
			data.add("Payment Mode");
			XSSFRow row = sheet.createRow((short) 0);
			int index = 0;
			for (String str : data)
			{
				Cell cell = row.createCell(index);
				cell.setCellValue(str);
				index++;
			}

			int rowIndex = 1;
			List<String> dataList = null;

			for (OnlineAdmission onlineAdmission : onlineAdmissionList) {

				if (onlineAdmission != null) {

					CandidateDetails candidateDetails = onlineAdmission.getCandidateDetails();
					dataList = new LinkedList<String>();

					dataList.add(onlineAdmission.getApplicationId());
					Name candidateName = candidateDetails.getName();
					dataList.add(candidateName.getFirstname());
					dataList.add(!StringUtils.isEmpty(candidateName.getMiddlename()) ? candidateName.getMiddlename() : "");
					dataList.add(!StringUtils.isEmpty(candidateName.getSurname()) ? candidateName.getSurname() : "");
					dataList.add(onlineAdmission.getAcademicDetails().get(0).getDepartment());
					dataList.add(onlineAdmission.getAcademicDetails().get(0).getYear());
					

					} else {
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
						dataList.add("");
					}
					
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentStatus()) ? onlineAdmission.getPaymentStatus() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getPaymentDate()) ? onlineAdmission.getPaymentDate() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getQfixReferenceNumber()) ? onlineAdmission.getQfixReferenceNumber() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getAmount()) ? onlineAdmission.getAmount() : "");
					dataList.add(!StringUtils.isEmpty(onlineAdmission.getModeOfPayment()) ? onlineAdmission.getModeOfPayment() : "");

					addRowInWorkBook(sheet, dataList, rowIndex);

					rowIndex++;

				}

			

			workbook.write(outputStream);
			dataBytes = outputStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return dataBytes;
	}

}