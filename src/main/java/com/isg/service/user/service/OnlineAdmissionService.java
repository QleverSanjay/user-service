package com.isg.service.user.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.isg.config.AppProperties;
import com.isg.service.user.dao.OnlineAdmissionDao;
import com.isg.service.user.model.onlineadmission.OnlineAdmissionUserDocument;
import com.isg.service.user.request.OnlineAdmissionForm;

@Service
public class OnlineAdmissionService {

	@Autowired
	private AppProperties appProperties;

	@Autowired
	private OnlineAdmissionDao onlineAdmissionDao;

	public void setOnlineAdmissionDao(OnlineAdmissionDao onlineAdmissionDao) {
		this.onlineAdmissionDao = onlineAdmissionDao;
	}

	@Transactional
	public byte[] downloadStudentAdmissionFormAttachments(
			Integer onlineAdmissionId) throws IOException {

		OnlineAdmissionForm admissionForm = onlineAdmissionDao
				.getOnlineAdmissionForm(onlineAdmissionId);

		if (admissionForm == null) {
			return null;
		}

		List<OnlineAdmissionUserDocument> documents = onlineAdmissionDao
				.listOnlineAdmissionUserDocuments(onlineAdmissionId);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);

		if (admissionForm.getPrintPdfPath() != null) {
			/*
			 * String admissionForPdfPath = "/var/www/html".concat(admissionForm
			 * .getPrintPdfPath());
			 */

			String admissionForPdfPath = appProperties.getFileProps()
					.getBaseSystemPath()
					.concat(admissionForm.getPrintPdfPath());

			makeZipEntry(admissionForPdfPath, zipOutputStream);
		}

		if (documents != null && documents.size() > 0) {

			for (OnlineAdmissionUserDocument userDocument : documents) {

				String path = userDocument.getDocumentPath();
				makeZipEntry(path, zipOutputStream);
			}

		}
		zipOutputStream.close();
		return outputStream.toByteArray();
	}

	private ZipEntry makeZipEntry(String path, ZipOutputStream zipOutputStream)
			throws IOException {
		File document = new File(path);
		byte[] bytes = Files.readAllBytes(document.toPath());

		ZipEntry zipEntry = new ZipEntry(document.getName());
		zipEntry.setSize(bytes.length);

		zipOutputStream.putNextEntry(zipEntry);
		zipOutputStream.write(bytes);
		zipOutputStream.closeEntry();

		return zipEntry;
	}

}
