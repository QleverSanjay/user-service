package com.isg.service.user.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.model.StudentRegistration;
import com.isg.service.user.request.StudentDirect;

@Service
public class StudentService {

	@Autowired
	private StudentDao studentDao;

	@Transactional
	public StudentDirect getStudentDetails(Integer branchId, String uniqueId,
			String uniqueIdName) throws SQLException {
		StudentDirect studentList = studentDao.searchStudent(branchId,
				uniqueId, uniqueIdName);
		return studentList;
	}

	@Transactional
	public Integer saveStudent(StudentRegistration student) throws Exception {

		Integer userId = studentDao.insertStudent(student);
		return userId;
	}
}
