package com.isg.service.user.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.user.dao.FeeDetailsDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.FeesInfo;
import com.isg.service.user.request.FeeDetails;
import com.isg.service.user.request.FeesCode;
import com.isg.service.user.request.PaymentTransaction;

@RestController
@RequestMapping("/parent")
public class FeeServiceController extends BaseController {

	@Autowired
	FeeDetailsDao feeDetailsDao;

	@RequestMapping(value = "/fees/dues", produces = { "application/json" })
	public List<PaymentTransaction> getFeeDues(
			@NotNull @RequestHeader(value = "token") String accessTokenStr)
			throws AuthenticationException, SQLException,
			AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);
		if (!ROLES.PARENT.get().equalsIgnoreCase(identity.getDefaultRole())
				&& !ROLES.STUDENT.get().equalsIgnoreCase(
						identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		}
		List<PaymentTransaction> feeDuesList = null;
		try {
			feeDuesList = feeDetailsDao.getFeeDues(identity.getEntityId(),
					identity.getDefaultRole());
			System.out
					.println("feeDuesList >=======>>>>>" + feeDuesList.size());
			List<PaymentTransaction> feePaidList = feeDetailsDao
					.getPaidPaymentTransactions(identity.getEntityId(),
							identity.getUserId(), identity.getDefaultRole());
			System.out
					.println("feePaidList >=======>>>>>" + feePaidList.size());
			feeDuesList.addAll(feePaidList);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return feeDuesList;
	}

	@RequestMapping(value = "/fees/feesInfo", produces = { "application/json" })
	public FeesInfo getFeeInfo(
			@NotNull @RequestHeader(value = "token") String accessTokenStr,
			@RequestParam(value = "feeScheduleId", required = true) Long feeScheduleId,
			@RequestParam(value = "paymentId", required = false) Long paymentId)
			throws AuthenticationException, SQLException,
			AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);
		/*
		 * if (!ROLES.PARENT.get().equalsIgnoreCase(identity.getDefaultRole())
		 * && !ROLES.STUDENT.get().equalsIgnoreCase(identity.getDefaultRole()))
		 * { throw new AuthorisationException("Function not allowed."); }
		 */
		FeesInfo feesInfo = null;
		try {
			feesInfo = feeDetailsDao.getFeeInfo(null, identity.getDefaultRole(), feeScheduleId, paymentId, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feesInfo;
	}

	@RequestMapping(value = "/fees/mark", method = RequestMethod.POST, produces = { "application/json" })
	public void markFeesStatus(
			@NotNull @RequestHeader(value = "token") String accessTokenStr,
			@RequestBody List<PaymentTransaction> feeDues)
			throws AuthenticationException, SQLException,
			AuthorisationException, ParseException {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!ROLES.PARENT.get().equalsIgnoreCase(identity.getDefaultRole())
				&& !ROLES.STUDENT.get().equalsIgnoreCase(
						identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		}

		for (PaymentTransaction FeeDuesObj : feeDues) {
			FeeDetails feeDetails = feeDetailsDao.getFeeDetails(FeeDuesObj
					.getFeesId());

			float amountPaid = StringUtils.isEmpty(FeeDuesObj.getAmount()) ? 0
					: Float.parseFloat(FeeDuesObj.getAmount());
			float feesAmount = StringUtils.isEmpty(feeDetails.getAmount()) ? 0
					: Float.parseFloat(feeDetails.getAmount());

			feeDetailsDao.markPaymentStatus(
					identity.getUserId(),
					identity.getEntityId(),
					FeeDuesObj.getIsPaidFlag(),
					FeeDuesObj.getFeesId(),
					FeeDuesObj.getStudentId(),
					null,
					(amountPaid == 0 || (feesAmount < amountPaid)) ? feeDetails
							.getAmount() : FeeDuesObj.getAmount(), FeeDuesObj
							.getPaymentId(), feeDetails.getDescription(),
					identity.getDefaultRole(), null, null, 0.0, "MANUAL");

		}
	}

	@RequestMapping(value = "/feesCode", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<FeesCode>> getFeeCodes(@RequestParam(value="standardId",required=false) Integer standardId,@RequestParam(value="divisionId",required=false) Integer 
			divisionId,@RequestParam(value="categoryId",required=false) Integer casteId,@RequestParam Integer branchId)
			throws AuthenticationException, SQLException,
			AuthorisationException {
		System.out.println("FeesCode Hit*******************");
		List<FeesCode> feesCodeList = null;
		try{
		
		 feesCodeList=feeDetailsDao.getApplicableFees( standardId,divisionId,casteId,branchId);
		
		}catch(Exception e){
			e.printStackTrace();
			ResponseEntity.badRequest().body(
					"Something bad happend, try again after sometime");
		}

		/*ResponseEntity.badRequest().body(
				"Something bad happend, try again after sometime");*/
		return ResponseEntity.ok(feesCodeList);

	}
}