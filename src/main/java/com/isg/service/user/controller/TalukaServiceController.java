package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.TalukaDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.Taluka;

@RestController
public class TalukaServiceController extends BaseController {

	@Autowired
	TalukaDao talukaDao;
	
	@RequestMapping(value = "/talukas", produces = { "application/json" })
	public List<Taluka> getTalukas(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException {

		authenitcateRequest(accessTokenStr);
		return talukaDao.getTalukas();
	}

	@RequestMapping(value = "/district/{id}/talukas", produces = { "application/json" })
	public List<Taluka> getTalukasByDistrictId(@NotNull @RequestHeader(value = "token") String accessTokenStr, @PathVariable Integer id)
					throws AuthenticationException, SQLException {

		return talukaDao.getTalukasOrCitiesByDistrictId(id);
	}
}
