package com.isg.service.user.controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.identity.model.Identity.ROLES_ID;
import com.isg.service.identity.model.Token;
import com.isg.service.user.client.notification.NotificationClient;
import com.isg.service.user.client.social.response.UserInfoResponse;
import com.isg.service.user.dao.ChatDao;
import com.isg.service.user.dao.EventDao;
import com.isg.service.user.dao.NotificationDao;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.ShoppingDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.exception.InvalidLoginCredentialsException;
import com.isg.service.user.model.Group;
import com.isg.service.user.model.OTPDetails;
import com.isg.service.user.model.PaymentAccount;
import com.isg.service.user.model.ShoppingProfile;
import com.isg.service.user.model.UserChatProfile;
import com.isg.service.user.model.UserForumProfile;
import com.isg.service.user.request.ChangePassword;
import com.isg.service.user.request.ChatProfile;
import com.isg.service.user.request.Ediary;
import com.isg.service.user.request.Event;
import com.isg.service.user.request.Otp;
import com.isg.service.user.request.Passbook;
import com.isg.service.user.request.RememberMeLogin;
import com.isg.service.user.request.ResetPassword;
import com.isg.service.user.request.User;
import com.isg.service.user.response.AuthenticationResponse;
import com.isg.service.user.response.OtpResponse;
import com.isg.service.user.response.UserDetail;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.Constant.EmailTemplate;
import com.isg.service.user.util.Constant.SOCIAL_PROFILE_TYPE;
import com.isg.service.user.util.Constant.SmsTemplate;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserServiceController extends BaseController {

	@Autowired
	UserDao userDao;

	@Autowired
	ParentDao parentDao;

	@Autowired
	ShoppingDao shoppingDao;

	@Autowired
	ChatDao chatDao;

	@Autowired
	NotificationDao notificationDao;

	@Autowired
	EventDao eventDao;

	@Autowired
	StudentDao studentDao;

	@Autowired
	private NotificationClient notificationClient;

//	private static final Logger log = Logger.getLogger(UserServiceController.class);

	/**
	 * Login Service
	 * 
	 * Sample Payload for this API request:
	 * {"login":"account-login-username","password":"some-password"}
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = { "application/json" })
	public AuthenticationResponse login( @RequestBody User user) throws Exception {
		System.out.println("Username received ".concat(user.getUsername()));
		AuthenticationResponse authenticationResponse = null;

		try {
			User userObj = userDao.getUserDetails(user);

			log.debug("USER OBJECT" + userObj);
			if (userObj == null) {
				throw new ApplicationException("User login failed.");
			}

			authenticationResponse = loadUserDetails(userObj);

			if (!StringUtils.isEmpty(user.getDeviceType()) && !StringUtils.isEmpty(user.getGcmId()) && !"undefined".equalsIgnoreCase(user.getGcmId())) {
				userDao.updateUserGCM(userObj.getId(), user.getGcmId(), user.getDeviceType());
			}
			String count = userDao.getLoginCount(user);
			authenticationResponse.setLoginCount(count);
		} catch (Exception e) {
			log.error("Login failed.", e);
			userDao.logInvalidLoginAttempt(user);
			throw new InvalidLoginCredentialsException("Invalid Username/ Password specified.");
		}

		return authenticationResponse;
	}
	
	@RequestMapping(value="/otpLogin", method=RequestMethod.POST)
	public AuthenticationResponse otpLogin(@RequestParam("username") String username,
										   @RequestParam("otp") Integer otp) throws InvalidLoginCredentialsException, AuthenticationException, SQLException{
		log.info("Login via OTP for username " + username);
		AuthenticationResponse authenticationResponse = null;
		User user = null;
		try{
			if(!userDao.verifyOTP(username, otp)){
				authenticationResponse = new AuthenticationResponse();
				authenticationResponse.setStatus("Invalid OTP");
				return authenticationResponse;
			}
			user = userDao.getUserByUsername(username);
			log.info("Getting user details for username " + username);
			if(user == null){
				authenticationResponse = new AuthenticationResponse();
				authenticationResponse.setStatus("Invalid username");
				return authenticationResponse;
			}
			
			authenticationResponse = loadUserDetails(user);
			
			if (!StringUtils.isEmpty(user.getDeviceType()) && !StringUtils.isEmpty(user.getGcmId()) && !"undefined".equalsIgnoreCase(user.getGcmId())) {
				userDao.updateUserGCM(user.getId(), user.getGcmId(), user.getDeviceType());
			}
			String count = userDao.getLoginCount(user);
			authenticationResponse.setLoginCount(count);
		}catch(Exception e) {
			log.error("Login failed.", e);
			userDao.logInvalidLoginAttempt(user);
			throw new InvalidLoginCredentialsException("Invalid Username/ Password specified.");
		}
		
		return authenticationResponse;
	}
	
	@RequestMapping(value = "/generateLoginOTP", method = RequestMethod.POST)
	public ResponseEntity<Map> generateOTP( @RequestParam("username") String username) throws Exception {
		//ResponseEntity<String> responseEntity = new ResponseEntity<>("my response body", HttpStatus.OK);
		String otpResponse = "";
		Map response = new HashMap<String, Object>(); 
		log.info("Generating OTP for username " + username);
		
		if(!userDao.checkUserExist(username)){
				//throw new InvalidLoginCredentialsException("Invalid Username");
				response.put("invalidUsername", "Invalid Username");
				log.info(username + " Username does not exist");
				return ResponseEntity.badRequest().body(response);
		}
		Integer id = userDao.generateLoginOTP(username);
	
		if(id == null){
				//throw new InvalidLoginCredentialsException("Mobile Number Doesnt exist");
				log.info("Mobile Number or email should exist for " + username);
				response.put("contactError", "Mobile Number or Email should exist");
				return ResponseEntity.badRequest().body(response);
		}else {
			OTPDetails otpDetails = userDao.getOTPDetails(id, username); 
			if(otpDetails != null){
				otpResponse = userDao.getMaskedContact(otpDetails);
			}
			log.info(username + " OTP success ");
			response.put("success", otpResponse);
			return ResponseEntity.ok(response);
		}
	}
	
	/*@RequestMapping(value="/generateLoginOTP", method=RequestMethod.POST)
	public String generateOP(@RequestParam("username") String username){
		if(studentDao.generateOTP(username) > 0){
			
			return "has contact";
		}
		else 
			return "no contact";
		return username;
	}*/


	/**
	 * Login Service
	 * 
	 * Sample Payload for this API request:
	 * {"login":"account-login-username","password":"some-password"}
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login_with_token", method = RequestMethod.POST, produces = { "application/json" })
	public AuthenticationResponse loginWithToken(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody RememberMeLogin rememberMeLogin) throws Exception {

		AuthenticationResponse authenticationResponse = null;

		Identity identity = authenitcateRequest(accessTokenStr);
		String defaultRole = identity.getDefaultRole();
		try {

			invalidateExistingToken(accessTokenStr);
			User userObj = userDao.getUserByIdAndRole(identity.getUserId(), defaultRole);
			if (userObj == null) {
				throw new ApplicationException("User login failed.");
			}

			authenticationResponse = loadUserDetails(userObj);

			if (!StringUtils.isEmpty(rememberMeLogin.getDeviceType()) && !StringUtils.isEmpty(rememberMeLogin.getGcmId())
							&& !"undefined".equalsIgnoreCase(rememberMeLogin.getGcmId())) {
				userDao.updateUserGCM(userObj.getId(), rememberMeLogin.getGcmId(), rememberMeLogin.getDeviceType());
			}
		} catch (Exception e) {
			log.error("Login failed.", e);
			throw new InvalidLoginCredentialsException("Invalid Username/ Password specified.");
		}

		return authenticationResponse;
	}

	/**
	 * Login Service
	 * 
	 * Sample Payload for this API request:
	 * {"login":"account-login-username","password":"some-password"}
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login/social", method = RequestMethod.POST, produces = { "application/json" })
	public AuthenticationResponse loginWithSocialProfile(@Valid @RequestBody Map<String, String> payload) throws Exception {

		AuthenticationResponse authenticationResponse = null;
		try {
			String accessToken = payload.get("access_token");
			String socialProfileProvider = payload.get("provider");

			if (StringUtils.isEmpty(accessToken) || !isValidProviderSpecified(socialProfileProvider)) {
				throw new ApplicationException("Required parameter is missing.");
			}

			UserInfoResponse userInfoResponse = userDao.getUserSocialProfileDetails(socialProfileProvider, accessToken);

			if (userInfoResponse == null) {
				throw new ApplicationException("User login failed.");
			}

			User userObj = userDao.getUserDetailsBySocialProfile(userInfoResponse);

			if (userObj != null && userObj.getId() != null) {
				authenticationResponse = loadUserDetails(userObj);
			} else {
				// New registration flow.....register as a parent....
				/*
				Parent parent = new Parent();
				parent.setEmail(userInfoResponse.getEmail());
				parent.setFirstname(userInfoResponse.getName());
				parent.setPassword(Constant.PASSWORD_NOT_APPLICABLE);

				RegistrationResponse registrationResponse = parentDao.registerParent(parent, Constant.AUTHENTICATION_TYPE.SOCIAL.get());
				authenticationResponse = new AuthenticationResponse();
				authenticationResponse.setStatus(Constant.USER_STATUS.NEW.get());
				// Remove after release.
				authenticationResponse.setRoles(registrationResponse.getRoles());
				authenticationResponse.setChatProfile(registrationResponse.getChatProfile());
				authenticationResponse.setShoppingProfile(registrationResponse.getShoppingProfile());

				Token token = identityDao.generateToken(registrationResponse.getUserId(), getDefaultRoleId(registrationResponse.getRoles()));
				String tokenStr = token.getToken();
				authenticationResponse.setToken(tokenStr);
				// Save identity in the cache
				storeIdentityInCache(tokenStr, token.getIdentity());
				*/
				throw new ApplicationException("Invalid Username/ Password speicified.");
			}

		} catch (Exception e) {
			log.error("Login failed.", e);
			throw new AuthenticationException("Invalid social login credentials specified.");
		}

		return authenticationResponse;
	}

	private AuthenticationResponse loadUserDetails(User user) throws AuthenticationException, SQLException {

		AuthenticationResponse response = new AuthenticationResponse();

		Integer userId = user.getId();

		if (userId != null && userId != 0) {
			Token token = identityDao.generateToken(userId, getDefaultRoleId(user.getRoles()));
			String tokenStr = token.getToken();
			response.setToken(tokenStr);

			ShoppingProfile shoppingProfileDetailsInDb = shoppingDao.getShoppingProfileDetails(userId);
			if (shoppingProfileDetailsInDb != null) {
				ShoppingProfile shoppingProfile = shoppingDao.getShoppingSessionDetails(shoppingProfileDetailsInDb.getUsername(),
								shoppingProfileDetailsInDb.getPassword());
				response.setShoppingProfile(shoppingProfile);
			}

			UserChatProfile userChatProfile = userDao.getUserChatProfileDetails(userId);
			if (userChatProfile != null) {
				ChatProfile chatProfile = chatDao.getChatSessionDetails(userChatProfile.getLogin(), userChatProfile.getPassword(),
								userChatProfile.getChatProfileId());
				response.setChatProfile(chatProfile);
			}

			UserForumProfile userForumProfile = userDao.getUserForumProfileDetails(userId);

			if (userForumProfile != null) {
				response.setForumProfile(userForumProfile);
			}

			response.setRoles(user.getRoles());

			response.setBranchConfiguration(token.getIdentity().getBranchConfigurationDetails());

			// Save identity in the cache
			storeIdentityInCache(tokenStr, token.getIdentity());

		} else {
			throw new ApplicationException("User login failed.");
		}

		return response;
	}

	@RequestMapping(value = "/otp/generate", method = RequestMethod.POST, produces = { "application/json" })
	public void generateOTP(@RequestBody Otp otp) {
		OtpResponse otpResponse = userDao.generateOTP(otp.getEmail(), otp.getPrimaryContact(), true);

		Map<String, Object> content = new HashMap<String, Object>();
		content.put("OTP", otpResponse.getMessage());

		if ("Success".equalsIgnoreCase(otpResponse.getCode())) {
			notificationClient.sendEmail(otp.getEmail(), EmailTemplate.REGISTRATION_OTP_EMAIL.get(), content);
			notificationClient.sendSMS(otp.getPrimaryContact(), SmsTemplate.REGISTRATION_OTP_SMS.get(), content);
		}

	}

	@RequestMapping(value = "/gcm", method = RequestMethod.POST, produces = { "application/json" })
	public boolean updateUserGCM(@NotNull @RequestHeader(value = "token") String accessTokenStr, @RequestBody Map<String, String> payload)
					throws Exception {

		String gcmId = payload.get("gcm_id");
		String deviceType = payload.get("device_type");
		if (StringUtils.isEmpty(gcmId) || StringUtils.isEmpty(deviceType)) {
			throw new ApplicationException("Required parameter is missing.");
		}

		Identity identity = authenitcateRequest(accessTokenStr);
		return userDao.updateUserGCM(identity.getUserId(), gcmId, deviceType);
	}

	@RequestMapping(value = "/task", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public void createPersonalTask(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody Event event)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		log.debug("createPersonalTask request :: " + event.toString());
		eventDao.createPersonalTask(identity.getDefaultRoleId(), event, identity.getUserId(), identity.getEntityId(), identity.getDefaultRole());
	}

	@RequestMapping(value = "/task", method = RequestMethod.PUT, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.OK)
	public void updatePersonalTask(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody Event event)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		log.debug("updatePersonalTask request :: " + event.toString());
		eventDao.updatePersonalTask(event, identity.getUserId(), identity.getEntityId(), identity.getDefaultRole());
	}

	@RequestMapping(value = "/task", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deletePersonalTask(@NotNull @RequestHeader(value = "token") String accessTokenStr, @RequestParam("event_id") Integer eventId)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		eventDao.deletePersonalTask(eventId, identity.getUserId(), identity.getEntityId());
	}

	@RequestMapping(value = "/passbook", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public void addEntryToPassbook(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody Passbook passbook)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);

		boolean outcome = true;

		if ("PARENT".equalsIgnoreCase(identity.getDefaultRole()) && passbook.getOtherDetails() != null && passbook.getOtherDetails().get("id") != null) {
			outcome = studentDao.canAccessDetails(identity.getEntityId(), passbook.getReceivingEntityId());
		} else if ("STUDENT".equalsIgnoreCase(identity.getDefaultRole())) {
			passbook.setOtherDetails(null);
		}

		if (outcome == false) {
			throw new AuthorisationException("Function not allowed, invalid receiving passbook entity user id specified.");
		}

		userDao.addEntryToPassbook(identity.getUserId(), passbook);
	}

	@RequestMapping(value = "/passbook", method = RequestMethod.GET, produces = { "application/json" })
	public Map<String, Object> getPassbookDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "last_sync_timestamp", required = false) Long lastSyncTimeStamp)
					throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("last_sync_timestamp", System.currentTimeMillis());

		PaymentAccount paymentAccount = userDao.getPaymentAccount(identity.getUserId());
		response.put("qfixpay_account_number", paymentAccount.getAccountNumber());

		List<Passbook> passbookList = userDao.getPassbookDetails(identity.getUserId(), (lastSyncTimeStamp == null) ? 0 : lastSyncTimeStamp.longValue());
		response.put("data", passbookList);

		return response;
	}

	@RequestMapping(value = "/groups", method = RequestMethod.GET, produces = { "application/json" })
	public List<Group> getAssignedGroups(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "search_text", required = false) String searchText,
					@RequestParam(value = "exclude_group_id", required = false) String excludeIds,
					@RequestParam(value = "branch_id", required = false) Integer branchId) throws AuthenticationException, SQLException,
					AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);
		String role = identity.getDefaultRole();

		if (!Identity.ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(role) && !Identity.ROLES.TEACHER.get().equalsIgnoreCase(role)
						&& !Identity.ROLES.ADMIN.get().equalsIgnoreCase(role)) {
			throw new AuthorisationException("Function not allowed.");
		}

		return userDao.getAssignedGroups(identity.getUserId(), searchText, excludeIds, identity.getDefaultRole(), branchId);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, produces = { "application/json" })
	public List<UserDetail> searchIndividualUsers(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "search_text", required = false) String searchText,
					@RequestParam(value = "exclude_user_id", required = false) String excludeIds,
					@RequestParam(value = "branch_id", required = false) Integer branchId) throws AuthenticationException, SQLException,
					AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);
		String role = identity.getDefaultRole();

		if (!Identity.ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(role) && !Identity.ROLES.TEACHER.get().equalsIgnoreCase(role)
						&& !Identity.ROLES.ADMIN.get().equalsIgnoreCase(role)) {
			throw new AuthorisationException("Function not allowed.");
		}

		Integer entityId = 0;

		if (Identity.ROLES.TEACHER.get().equalsIgnoreCase(role)) {
			entityId = identity.getEntityId();
			branchId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.BRANCH_ID);
			System.out.println("entityId for teacher>>>>>>" + entityId);
		}
		return userDao.searchIndividualUsers(identity.getUserId(), searchText, excludeIds, identity.getDefaultRole(), branchId, entityId);
	}

	@RequestMapping(value = "/ediary", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public void addEdiaryEntry(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody Ediary ediary)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		log.debug("addEdiaryEntry request :: " + ediary.toString());

		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole()) && !"SCHOOL ADMIN".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowd for current user profile");
		} else if (ediary.getGroupIds() == null && ediary.getUserIds() == null) {
			throw new IllegalArgumentException("Mandatory parameter (Group Id / User Id) for sending notification not supplied ");
		}

		Integer branchId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.BRANCH_ID);
		Integer instituteId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.INSTITUTE_ID);

		eventDao.addEdiaryEntry(ediary, identity.getUserId(), instituteId, branchId, identity.getEntityId());
	}

	@RequestMapping(value = "/password", method = RequestMethod.PUT, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.OK)
	public void changePassword(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody ChangePassword changePassword)
					throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);
		log.debug("changePassword request :: " + changePassword.toString());
		boolean issuccess = userDao.changePassword(identity.getUserId(), changePassword);
		if (!issuccess) {
			throw new ApplicationException("Password cannot be changed.");
		}
	}

	@RequestMapping(value = "/password", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.OK)
	public void forgotPassword(@RequestParam(required = false, value = "email") String email,
					@RequestParam(required = false, value = "mobile") String mobile)
					throws Exception {

		if (StringUtils.isEmpty(email) && StringUtils.isEmpty(mobile)) {
			throw new Exception("Email or mobile is required.");
		}

		log.debug("ForgotPassword request for EMAIL :: " + email);
		log.debug("ForgotPassword request for MOBILE :: " + mobile);

		User user = null;
		if (!StringUtils.isEmpty(email)) {
			user = userDao.getUserByEmail(email);
		}
		else if (!StringUtils.isEmpty(mobile)) {
			user = userDao.getUserByMobile(mobile);
		}
		if (user != null) {
			userDao.saveForgotPasswordOtp(user.getUsername(), email);
		} else {
			log.error("User email or mobile do not exists, hence forgot password request is ignored silently without informing user.");
			throw new ApplicationException("Email or mobile do not exists");
		}
	}

	@RequestMapping(value = "/password/reset", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.OK)
	public void resetPassword(@Valid @RequestBody ResetPassword resetPassword)
					throws Exception {
		boolean issuccess = userDao.resetPassword(resetPassword.getOtp(), resetPassword.getNewPassword());
		if (!issuccess) {
			throw new ApplicationException("Reset password failed, please check otp specified / try with new otp again.");
		}
	}

	@RequestMapping(value = "/token", method = RequestMethod.POST, produces = { "application/json" })
	public String getToken(@Valid @RequestBody User user)
					throws AuthenticationException, InvalidLoginCredentialsException, SQLException, NoSuchAlgorithmException, InvalidKeySpecException {

		User userObj = userDao.getUserDetails(user);

		// Special case added to retrieve token from shopping portal for vendor
		// registration process. Hence allowed for super admin profile only.
		if (userObj == null || !userObj.getRoles().contains(ROLES.ADMIN.get())) {
			throw new AuthenticationException("User login failed.");
		}

		Token token = identityDao.generateToken(userObj.getId(), ROLES_ID.ADMIN.get());

		// Save identity in the cache
		storeIdentityInCache(token.getToken(), token.getIdentity());

		return "{\"token\":\"" + token.getToken() + "\"}";
	}

	private boolean isValidProviderSpecified(String provider) {
		boolean isValidProviderSpecified = false;
		try {
			SOCIAL_PROFILE_TYPE profileType = Constant.SOCIAL_PROFILE_TYPE.valueOf(provider);

			if (profileType != null) {
				isValidProviderSpecified = true;
			}
		} catch (Exception e) {
			// Gracefully ignore

		}

		return isValidProviderSpecified;
	}

	/**
	 * Change identity of the current user, used for dual role login.
	 * 
	 * @param accessTokenStr
	 * @param role
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/identity", method = RequestMethod.PUT, produces = { "application/json" })
	public void changeIdentity(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "role") String role) throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!identity.getRoleDetails().contains(role.toUpperCase())) {
			throw new ApplicationException("Requested role based switch is not allowed.");
		}

		List<String> roleDetails = new ArrayList<String>();
		roleDetails.add(role.toUpperCase());

		int roleId = getDefaultRoleId(roleDetails);

		Identity switchedIdentity = identityDao.changeIdentity(identity.getUserId(), roleId, accessTokenStr);

		// Save identity in the cache
		storeIdentityInCache(accessTokenStr, switchedIdentity);
	}

	/**
	 * Logout from system.
	 * 
	 * @param accessTokenStr
	 * @param role
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.PUT, produces = { "application/json" })
	public void logout(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws Exception {
		authenitcateRequest(accessTokenStr);
		invalidateExistingToken(accessTokenStr);
	}
	
	
}