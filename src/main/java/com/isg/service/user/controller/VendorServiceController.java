package com.isg.service.user.controller;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.VendorDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.request.Vendor;
import com.isg.service.user.request.VendorMerchantDetail;

@RestController
@RequestMapping("/vendor")
public class VendorServiceController extends BaseController {

	@Autowired
	VendorDao vendorDao;

	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = { "application/json" })
	public Vendor register(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody Vendor vendor)
					throws AuthenticationException, SQLException, NoSuchAlgorithmException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		}

		return vendorDao.register(vendor);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = { "application/json" })
	public Vendor register(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody VendorMerchantDetail vendorMerchantDetail)
					throws AuthenticationException, SQLException, NoSuchAlgorithmException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		}
		return vendorDao.updateMerchantDetails(vendorMerchantDetail);
	}

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public Vendor register(@NotNull @RequestHeader(value = "token") String accessTokenStr)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		}
		return vendorDao.getById(identity.getEntityId());
	}

}
