package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.GuardianTypeDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.GuardianType;

@RestController
public class GuardianTypeServiceController extends BaseController {

	@Autowired
	GuardianTypeDao guardianTypeDao;

	@RequestMapping(value = "/guardianType", produces = { "application/json" })
	public List<GuardianType> getAll(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return guardianTypeDao.getAll();
	}
}
