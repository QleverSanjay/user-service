package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.CityDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.City;

@RestController
public class CitiesServiceController extends BaseController {

	@Autowired
	CityDao cityDao;

	@RequestMapping(value = "/city/all", produces = { "application/json" })
	public List<City> getCities(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return cityDao.getCities();
	}

	@RequestMapping(value = "/cities/{id}", produces = { "application/json" })
	public List<City> getCitiesByTalukaId(@NotNull @RequestHeader(value = "token") String accessTokenStr, @PathVariable Integer id)
					throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return cityDao.getCitiesByTalukaId(id);
	}

	@RequestMapping(value = "/cities/name/{name}", produces = { "application/json" })
	public List<City> getCitiesByName(@NotNull @RequestHeader(value = "token") String accessTokenStr, @PathVariable String name)
					throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return cityDao.getCitiesByName(name);
	}

}
