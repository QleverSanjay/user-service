package com.isg.service.user.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.ChatDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.Contact;
import com.isg.service.user.model.Group;
import com.isg.service.user.model.UserChatProfile;
import com.isg.service.user.request.InitiateChat;
import com.isg.service.user.request.SearchContact;

@RestController
@RequestMapping("/chat")
public class ChatServiceController extends BaseController {

	@Autowired
	ChatDao chatDao;

	@Autowired
	@Lazy
	UserDao userDao;

	/**
	 * Search contact to initiate a private chat.
	 * 
	 * @param accessTokenStr
	 * @param searchText
	 * @throws Exception
	 */
	@RequestMapping(value = "/contact/search", method = RequestMethod.POST, produces = { "application/json" })
	public List<Contact> searchContact(@NotNull @RequestHeader(value = "token") String accessTokenStr, @NotNull @RequestBody SearchContact searchContact)
					throws Exception {

		if (StringUtils.isEmpty(searchContact.getEmail()) && StringUtils.isEmpty(searchContact.getPhone())) {
			throw new ApplicationException("Required parameters missing.");
		}

		Identity identity = authenitcateRequest(accessTokenStr);
		return chatDao.searchContact(searchContact, identity.getUserId());
	}

	/**
	 * Initiate a chat with contact
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/initiate", method = RequestMethod.POST, produces = { "application/json" })
	public void initiatChat(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody InitiateChat initiateChat)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);

		UserChatProfile userChatProfile = userDao.getUserChatProfileDetails(identity.getUserId());

		if (userChatProfile == null) {
			throw new ApplicationException("User request failed, no matching chat profile found.");
		}

		if ("Y".equalsIgnoreCase(initiateChat.getIsPrivateChatFlag())) {
			chatDao.createPrivateGroup(identity.getUserId(), userChatProfile.getLogin(), userChatProfile.getPassword(), initiateChat.getChatContactIds());
		} else {
			chatDao.createPublicGroup(identity.getUserId(), userChatProfile.getLogin(), userChatProfile.getPassword(), initiateChat.getName(),
							initiateChat.getChatContactIds());
		}

	}

	/**
	 * Get list of suggested groups for user.
	 * 
	 * @param accessTokenStr
	 * @throws Exception
	 */
	@RequestMapping(value = "/group/list/suggest", method = RequestMethod.GET, produces = { "application/json" })
	public List<Group> retrieveSuggestedGroup(@NotNull @RequestHeader(value = "token") String accessTokenStr)
					throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);
		return chatDao.retrieveSuggestedGroup(identity.getDefaultRole(), identity.getEntityId(), identity.getUserId());
	}

	/**
	 * Join a chat group.
	 * 
	 * @param accessTokenStr
	 * @param groupId
	 * @throws Exception
	 */
	@RequestMapping(value = "/group/join", method = RequestMethod.POST, produces = { "application/json" })
	public void join(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody Map<String, Integer> payload)
					throws Exception {
		Integer groupId = payload.get("group_id");

		if (groupId == null) {
			throw new ApplicationException("Required parameter group_id is missing from payload.");
		}

		Identity identity = authenitcateRequest(accessTokenStr);

		UserChatProfile userChatProfile = userDao.getUserChatProfileDetails(identity.getUserId());

		if (userChatProfile == null) {
			throw new ApplicationException("User request failed, no matching chat profile found.");
		}

		chatDao.joinGroup(groupId, identity.getUserId(), userChatProfile.getChatProfileId(), identity.getDefaultRoleId());

	}

	/**
	 * Delete a chat group.
	 * 
	 * If user is not owner of the group, user will be removed from group, in
	 * case group is owned by the logged in user, group will be hard deleted.
	 * 
	 * @param accessTokenStr
	 * @param dialogId
	 * @throws Exception
	 */
	@RequestMapping(value = "/group/{chat_group_id}", method = RequestMethod.DELETE, produces = { "application/json" })
	public void deleteChatGroup(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @PathVariable("chat_group_id") String dialogId)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);

		UserChatProfile userChatProfile = userDao.getUserChatProfileDetails(identity.getUserId());

		if (userChatProfile != null) {
			chatDao.deleteChatGroup(userChatProfile.getLogin(), userChatProfile.getPassword(), dialogId);
		} else {
			throw new ApplicationException("User request failed, no matching chat profile found.");

		}

	}
}
