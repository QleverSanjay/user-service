package com.isg.service.user.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.FeeDetailsDao;
import com.isg.service.user.dao.ShoppingDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.StandardDivision;
import com.isg.service.user.model.StudentDetail;
import com.isg.service.user.model.StudentRegistration;
import com.isg.service.user.model.onlineadmission.Category;
import com.isg.service.user.request.FeesCode;
import com.isg.service.user.request.Goal;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.StudentBasicDetails;
import com.isg.service.user.request.StudentDirect;
import com.isg.service.user.request.UpdateGoal;
import com.isg.service.user.response.FindStudentProfileMatchesForAParent;
import com.isg.service.user.service.StudentService;
import com.isg.service.user.validator.StudentValidator;
import com.isg.service.user.validator.ValidationError;
import com.isg.service.user.validator.ValidatorUtil;

@RestController
public class StudentServiceController extends BaseController {

	@Autowired
	StudentDao studentDao;
	@Autowired
	StudentValidator studentValidator;

	@Autowired
	private ShoppingDao shoppingDao;
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	FeeDetailsDao feeDetailsDao;
	
	@RequestMapping(value = "/student/link", method = RequestMethod.GET, produces = { "application/json" })
	public List<FindStudentProfileMatchesForAParent> getUnlinkedStudentByParentId(@NotNull @RequestHeader(value = "token") String accessTokenStr)
					throws AuthenticationException, SQLException, AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);

		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}

		return studentDao.getUnlinkedStudentByParentId(identity.getEntityId());
	}

	@RequestMapping(value = "/student", method = RequestMethod.POST, produces = { "application/json" })
	public void saveStudent(@NotNull @RequestHeader(value = "token") String accessTokenStr, @RequestBody Student student) throws AuthenticationException,
					SQLException, AuthorisationException, IOException, NoSuchAlgorithmException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		} else {
			student.setParentId(identity.getEntityId());
			studentDao.saveStudent(student);
		}

	}

	@RequestMapping(value = "/student", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateStudent(@NotNull @RequestHeader(value = "token") String accessTokenStr, @RequestBody Student student) throws AuthenticationException,
					SQLException, AuthorisationException, IOException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if ("PARENT".equalsIgnoreCase(identity.getDefaultRole()) && studentDao.canAccessDetails(identity.getEntityId(), student.getPersonalDetails().getId())) {
			Student existingRecord = studentDao.getStudentProfileDetailsbyId(student.getPersonalDetails().getId());
			student.setParentId(identity.getEntityId());
			student.setEducationDetails(existingRecord.getEducationDetails());
		} else if ("STUDENT".equalsIgnoreCase(identity.getDefaultRole())) {
			Student existingRecord = studentDao.getStudentProfileDetailsbyId(identity.getEntityId());
			student.setParentId(existingRecord.getParentId());
			student.getPersonalDetails().setId(identity.getEntityId());
			student.setEducationDetails(existingRecord.getEducationDetails());
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		studentDao.updateStudent(student, identity.getDefaultRole(), identity.getEntityId(), identity.getUserId());

		/*
		if ("PARENT".equalsIgnoreCase(identity.getDefaultRole())) {
			Student existingStudentRecord = studentDao.getStudentProfileDetailsbyId(identity.getEntityId());
			shoppingDao.linkCustomerAccounts(identity.getUserId(), student.getPersonalDetails().getId());
		} */
	}

	@RequestMapping(value = "/student", method = RequestMethod.GET, produces = { "application/json" })
	public Student getStudentDetailsById(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "student_id", required = false) Integer studentId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if ("PARENT".equalsIgnoreCase(identity.getDefaultRole()) && !StringUtils.isEmpty(studentId)
						&& studentDao.canAccessDetails(identity.getEntityId(), studentId)) {
			// Ignore gracefully : its a linked student or already added as
			// child.
		} else if ("STUDENT".equalsIgnoreCase(identity.getDefaultRole())) {
			studentId = identity.getEntityId();
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		return studentDao.getStudentProfileDetailsbyId(studentId);
	}

	@RequestMapping(value = "/search/student", method = RequestMethod.GET, produces = { "application/json" })
	public List<StudentBasicDetails> getAssociatedStudentList(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "search_text") String searchText,
					@RequestParam(value = "exclude_user_id", required = false) String excludeIds) throws AuthenticationException, SQLException,
					AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);
		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		}

		Integer branchId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.BRANCH_ID);
		return studentDao.getAssociatedStudentList(branchId, searchText, excludeIds);

	}

	@RequestMapping(value = "/goals", method = RequestMethod.GET, produces = { "application/json" })
	public Map<String, Object> getGoalDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "student_id", required = false) Integer studentId,
					@RequestParam(value = "last_sync_timestamp", required = false) Long lastSyncTimeStamp) throws AuthenticationException,
					SQLException,
					AuthorisationException, ParseException {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole())) && !("STUDENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("last_sync_timestamp", System.currentTimeMillis());

		List<Goal> goalList = studentDao.getGoalDetails(identity.getDefaultRole(), identity.getEntityId(), studentId, (lastSyncTimeStamp == null) ? 0
						: lastSyncTimeStamp.longValue());
		response.put("data", goalList);
		return response;

	}

	@RequestMapping(value = "/goals", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public void saveGoal(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody Goal goal) throws AuthenticationException,
					SQLException,
					AuthorisationException, ParseException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		} else if (!studentDao.canAccessDetails(identity.getEntityId(), goal.getStudentId())) {
			throw new AuthorisationException("Function not allowed.");
		}

		studentDao.saveGoal(identity.getEntityId(), goal);

	}

	@RequestMapping(value = "/goals", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateGoalStatus(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody UpdateGoal updateGoal) throws AuthenticationException, SQLException, AuthorisationException, ParseException {

		Identity identity = authenitcateRequest(accessTokenStr);
		String defaultRole = identity.getDefaultRole();

		if (!("PARENT".equalsIgnoreCase(defaultRole)) && !("STUDENT".equalsIgnoreCase(defaultRole))) {
			throw new AuthorisationException("Function not allowed.");
		} else if ("STUDENT".equalsIgnoreCase(defaultRole)
						&& !("COMPLETE".equalsIgnoreCase(updateGoal.getStatus()) || "RESUBMIT".equalsIgnoreCase(updateGoal.getStatus()))) {
			throw new AuthorisationException("Function not allowed, invlid status specified in request.");
		} else if ("PARENT".equalsIgnoreCase(defaultRole)
						&& !("ACCEPTED".equalsIgnoreCase(updateGoal.getStatus()) || "REJECTED".equalsIgnoreCase(updateGoal.getStatus()))) {
			throw new AuthorisationException("Function not allowed, invlid status specified in request.");
		}

		studentDao.updateGoal(defaultRole, identity.getEntityId(), identity.getUserId(), updateGoal.getId(), updateGoal.getFeedback(), updateGoal.getStatus());
	}

	@RequestMapping(value = "/goals", method = RequestMethod.DELETE, produces = { "application/json" })
	public void deleteGoal(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestParam(value = "goal_id") Integer goalId) throws AuthenticationException,
					SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);
		String defaultRole = identity.getDefaultRole();

		if (!("PARENT".equalsIgnoreCase(defaultRole))) {
			throw new AuthorisationException("Function not allowed.");
		}

		boolean outcome = studentDao.deleteGoal(defaultRole, identity.getEntityId(), goalId);

		if (!outcome) {
			throw new ApplicationException("Can not delete goal, check status of the goal.");
		}
	}

	@RequestMapping(value = "/student/list", method = RequestMethod.GET, produces = { "application/json" })
	public List<StudentDetail> getStudentsByIds(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "student_ids", required = false) String studentId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!"ADMIN".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		}

		return studentDao.getStudentNamebyIds(studentId);
	}
	
	@RequestMapping(value = "/student/search", method = RequestMethod.GET, produces = {"application/json"})
	public StudentDirect searchStudent(@RequestParam("uniqueId") String uniqueId,@RequestParam("uniqueIdName") String uniqueIdName,@RequestParam("branchId")Integer branchId) throws SQLException {
		System.out.println("service hit the controller for student details");
		StudentDirect studentDirect=new StudentDirect();
		try{
			studentDirect=studentService.getStudentDetails(branchId, uniqueId, uniqueIdName);
			
		}catch(Exception e){
		
			System.out.println(e);
			
	
		 studentDirect.setStatus("student not found");
			return studentDirect;
			
		}
		
		return studentDirect;
		
	}
	
	
	@RequestMapping(value = "standard/search",method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<HashMap<String,List>> getStandardDivisionCategoryList(@RequestParam(value = "branchId") Integer branchId){
		
		List<StandardDivision> division=null;
		List<Category> category=null;
		List<FeesCode> feesCodeList=null;
		try{
			division = studentDao.getStandardList(branchId);
			category=studentDao.getCategoryList();
			feesCodeList=feeDetailsDao.getAdditionalFeeCode(branchId);
			
			
		}catch(Exception e){
			e.printStackTrace();
			ResponseEntity.badRequest().body("contact system admin");
		}
		
		HashMap<String, List> response = new HashMap<String, List>();
		response.put("standardDivision",division);
		response.put("category",category );
		response.put("additionalFeeCodeList", feesCodeList);
		
		return ResponseEntity.ok(response);
	}
	
	
	
	@RequestMapping(value = "student/save", method = RequestMethod.POST, produces = {"application/json"})
	public ResponseEntity<? extends Object> saveStudent(@RequestBody String student) throws SQLException {
		
		System.out.println("service hit the controller for student save");
		StudentRegistration studentreg=new StudentRegistration();
		String result = null;
		StudentDirect studentDirect=new StudentDirect();
		ObjectMapper mapper = new ObjectMapper();
		try {
			
			
			studentreg = mapper.readValue(student, StudentRegistration.class);
			
			
			ValidationError error = ValidatorUtil.validateForAllErrors(studentreg, studentValidator);
			if(error != null){
				String title = "Invalid student details.";

				error.setTitle(title);
				result = "{\"status\":\"fail\", \"isValidationError\" : \"true\", " +
					"\"message\":\"Invalida Data provided\", " +
					"\"records\" : "+mapper.writeValueAsString(error)+"}";
				studentDirect.setStatus("failed");
				return ResponseEntity.badRequest().body(result);
			}
		} catch (JsonParseException e1) {
			
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			
			e1.printStackTrace();
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
		
		
		
		try{
			
			
			
			Integer  userId=studentService.saveStudent(studentreg);
			
			if(userId!=null){
				
				studentDirect=studentService.getStudentDetails(studentreg.getBranchId(), userId.toString(),"userId");
			}
			/*if(isSaved){
				
				result="success";
			}
			*/
		}catch(Exception e){
		
			System.out.println(e);
			
			ResponseEntity.status(500);
		
			
			
		}
		studentDirect.setStatus("success");
		return ResponseEntity.ok(studentDirect);
		
	}
	
	
	
}
