package com.isg.service.user.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.BranchDao;
import com.isg.service.user.dao.ManageProfileDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.FileAttachment;
import com.isg.service.user.request.Student;
import com.isg.service.user.util.FileStorePathAssembler;

@RestController
@RequestMapping("/upload")
public class FileStorageServiceController extends BaseController {

	@Autowired
	ManageProfileDao manageProfileDao;

	@Autowired
	BranchDao branchDao;

	@Autowired
	StudentDao studentDao;

	@RequestMapping(value = "/avatar", method = RequestMethod.POST, produces = { "application/json" })
	public String uploadProfilePic(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody FileAttachment fileAttachment)
					throws AuthenticationException, SQLException, IOException {

		System.out.println("GOt REQUEST TO UPLOAD AVTAR.......");
		if (!FileStorePathAssembler.isValidImageExtension(FileStorePathAssembler.IMAGE_PATTERN, fileAttachment.getFileNameWithExtension())) {
			System.out.println("Throwing EXception....");
			throw new ApplicationException("Unsupported media type, please upload a valid photo file with extension as jpg|png|gif|bmp");
		}

		Identity identity = authenitcateRequest(accessTokenStr);

		Integer branchId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.BRANCH_ID);
		Integer instituteId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.INSTITUTE_ID);

		return manageProfileDao.updateProfilePicture(identity.getEntityId(), instituteId, branchId, identity.getDefaultRole(), fileAttachment);

	}

	@RequestMapping(value = "/avatar/for_student", method = RequestMethod.POST, produces = { "application/json" })
	public String uploadProfilePicForStudent(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody FileAttachment fileAttachment,
					@NotNull @RequestParam(value = "student_id") Integer studentId
					)
									throws AuthenticationException, SQLException, IOException, AuthorisationException {

		if (!FileStorePathAssembler.isValidImageExtension(FileStorePathAssembler.IMAGE_PATTERN, fileAttachment.getFileNameWithExtension())) {
			throw new ApplicationException("Unsupported media type, please upload a valid photo file with extension as jpg|png|gif|bmp");
		} else if (studentId == null) {
			throw new ApplicationException("Invalid student id specified.");
		}

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		} else if (!studentDao.canAccessDetails(identity.getEntityId(), studentId)) {
			throw new AuthorisationException("Function not allowed.");
		}

		Student student = studentDao.getStudentProfileDetailsbyId(studentId);
		Integer branchId = student.getEducationDetails().getBranchId();
		Integer instituteId = null;
		if (branchId != null) {
			instituteId = branchDao.getBranchDetails(branchId).getInstituteId();
		}

		return manageProfileDao.updateProfilePicture(studentId, instituteId, branchId, "STUDENT", fileAttachment);

	}

}
