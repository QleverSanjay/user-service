package com.isg.service.user.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.NotificationDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.FileAttachment;
import com.isg.service.user.model.NotificationCount;
import com.isg.service.user.model.NotificationRecipientDetail;
import com.isg.service.user.request.Notification;
import com.isg.service.user.request.SendNotfication;

@RestController
// Max uploaded file size (here it is 5 MB)
@MultipartConfig(fileSizeThreshold = 5120)
public class NotificationServiceController extends BaseController {

	@Autowired
	NotificationDao notificationDao;

	static Logger log = Logger.getLogger(NotificationServiceController.class.getName());
	
	@RequestMapping(value = "/notifications", method = RequestMethod.GET, produces = { "application/json" })
	public List<Notification> getNotification(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "last_notification_received_id", required = false) Integer lastNotificationReceivedId,
					@RequestParam(value = "role", required = false) String role) throws AuthenticationException, SQLException, AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);

		String defaultRole = identity.getDefaultRole();

		if (!StringUtils.isEmpty(role) && identity.isRoleExists(role)) {
			defaultRole = role;
		}

		Integer entityId = identity.getEntityId(defaultRole);

		return notificationDao.getNotification(defaultRole, identity.getUserId(), entityId, lastNotificationReceivedId);
	}

	@RequestMapping(value = "/assignments", method = RequestMethod.GET, produces = { "application/json" })
	public List<Notification> getAssignments(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "last_assignment_received_id", required = false) Integer lastAssignmentReceivedId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		String defaultRole = identity.getDefaultRole();

		Integer entityId = identity.getEntityId(defaultRole);

		return notificationDao.getAssignment(defaultRole, identity.getUserId(), entityId, lastAssignmentReceivedId);
	}

	@RequestMapping(value = "/notification/{notification_id}", produces = { "application/json" })
	public Notification getNotificationDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable("notification_id") Integer notificationId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		return notificationDao.getNotificationDetails(identity.getDefaultRole(), identity.getUserId(), identity.getEntityId(), notificationId);
	}

	@RequestMapping(value = "/notification/unread", produces = { "application/json" })
	public NotificationCount getUnreadMessageCount(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws IOException, SQLException,
					AuthenticationException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		return notificationDao.getUnreadMessageCount(identity.getDefaultRole(), identity.getDefaultRoleId(), identity.getUserId(), identity.getEntityId());
	}

	@RequestMapping(value = "/notification/send_with_attachment", method = RequestMethod.POST, consumes = { "multipart/form-data" }, produces = { "application/json" })
	public void sendNotificationWithAttachment(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestPart(value = "data", required = false) String sendNotficationStr,
					@RequestPart(value = "file", required = false) MultipartFile file)
					throws IOException,
					SQLException,
					AuthenticationException, AuthorisationException, ParseException {

		SendNotfication sendNotfication = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			sendNotfication = mapper.readValue(sendNotficationStr, SendNotfication.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		FileAttachment fileAttachment = null;

		if (!file.isEmpty()) {
			fileAttachment = new FileAttachment();
			String fileName = file.getOriginalFilename();
			fileAttachment.setFileNameWithExtension(fileName);
			fileAttachment.setBytes(file.getBytes());
		}

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowd for current user profile");
		} else if (sendNotfication.getGroupIds() == null && sendNotfication.getUserIds() == null) {
			throw new IllegalArgumentException("Mandatory parameter (Group Id / User Id) for sending notification not supplied ");
		}

		Integer branchId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.BRANCH_ID);
		Integer instituteId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.INSTITUTE_ID);

		sendNotfication.setFileAttachment(fileAttachment);
		notificationDao.sendNotification(identity.getUserId(), branchId, instituteId, identity.getDefaultRole(),
						sendNotfication);
	}

	@RequestMapping(value = "/notification/send", method = RequestMethod.POST, produces = { "application/json" })
	public void sendNotification(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestBody SendNotfication sendNotfication)
					throws IOException,
					SQLException,
					AuthenticationException, AuthorisationException, ParseException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowd for current user profile");
		} else if (sendNotfication.getGroupIds() == null && sendNotfication.getUserIds() == null) {
			throw new IllegalArgumentException("Mandatory parameter (Group Id / User Id) for sending notification not supplied ");
		}

		Integer branchId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.BRANCH_ID);
		Integer instituteId = identity.getEntityDetails().get(Identity.ENTITY_KEYS.INSTITUTE_ID);

		notificationDao.sendNotification(identity.getUserId(), branchId, instituteId, identity.getDefaultRole(),
						sendNotfication);
	}

	@RequestMapping(value = "/notification/{notification_id}/recipients", produces = { "application/json" })
	public NotificationRecipientDetail getNotificationRecipientDetail(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable("notification_id") Integer notificationId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole()) && !notificationDao.canAccessNoticeDetail(identity.getUserId(), notificationId)) {
			throw new AuthorisationException("Function not allowd for current user profile");
		}

		return notificationDao.getNotificationRecipientDetail(notificationId);
	}

	@RequestMapping(value = "/notification/mark/read", method = RequestMethod.POST, produces = { "application/json" })
	public Map<String, Integer> markAsRead(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody Map<String, Integer> payload)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		Integer noticeId = payload.get("notification_id");
		Integer forUserId = payload.get("for_user_id");

		if (noticeId == null || noticeId.intValue() == 0) {
			throw new ApplicationException("Required parameter notice id not specified.");
		}

		if (!"PARENT".equalsIgnoreCase(identity.getDefaultRole())) {
			forUserId = identity.getUserId();
		} else if (forUserId == null || forUserId.intValue() == 0) {
			throw new ApplicationException("Required parameter for user id not specified.");
		}

		return notificationDao.markAsRead(noticeId, identity.getUserId(), forUserId);
	}
}
