package com.isg.service.user.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.user.client.payment.request.RefundDetail;
import com.isg.service.user.dao.FeeDetailsDao;
import com.isg.service.user.dao.OnlineAdmissionDao;
import com.isg.service.user.dao.PaymentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.FeesInfo;
import com.isg.service.user.model.InitiateFeesPaymentResponse;
import com.isg.service.user.model.OnlineAdmissionFormDetail;
import com.isg.service.user.model.PaymentDetail;
import com.isg.service.user.model.PaymentDetailResponse;
import com.isg.service.user.model.PaymentMoreInfo;
import com.isg.service.user.model.ShoppingPayment;
import com.isg.service.user.model.pdf.PaymentChallanGenerator;
import com.isg.service.user.model.pdf.PaymentRecipt;
import com.isg.service.user.request.BranchConfiguration;
import com.isg.service.user.request.OnlineAdmissionFee;
import com.isg.service.user.request.PaymentBranchScheme;
import com.isg.service.user.request.PaymentTransaction;
import com.isg.service.user.response.OnlineAdmissionConfiguration;

@RestController
@RequestMapping("/payment")
public class PaymentServiceController extends BaseController {

	private static final Logger log = Logger.getLogger(PaymentServiceController.class);

	@Autowired
	PaymentDao paymentDao;

	@Autowired
	private FeeDetailsDao feeDetailsDAO;

	@Autowired
	OnlineAdmissionDao onlineAdmissionDao;

	@Autowired
	private UserDao userDao;

	@RequestMapping(value = "/fees/initiate", method = RequestMethod.POST, produces = { "application/json" })
	public InitiateFeesPaymentResponse initiateFeesPayment(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestBody List<PaymentTransaction> feeDues, @NotNull @RequestParam(value = "channel") String channel)
					throws AuthenticationException, SQLException, JsonProcessingException {
		Identity identity = authenitcateRequest(accessTokenStr);
		try {
			String accessCode = paymentDao.initiateFeesPayment(feeDues, identity.getEntityId(), identity.getUserId(), identity.getDefaultRole(), channel);
			InitiateFeesPaymentResponse initiateFeesPaymentResponse = new InitiateFeesPaymentResponse();
			initiateFeesPaymentResponse.setAccessCode(accessCode);
			return initiateFeesPaymentResponse;
		} catch (Exception ex) {
			log.error("Exception>>>>>>>>>>>>>>>>>>>s", ex);
			ex.printStackTrace();
			throw ex;
		}
	}

	@RequestMapping(value = "/shopping/initiate", method = RequestMethod.POST, produces = { "application/json" })
	public InitiateFeesPaymentResponse initiateShoppingPayment(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestBody ShoppingPayment shoppingPayment, @NotNull @RequestParam(value = "channel") String channel)
					throws AuthenticationException, SQLException, IOException {
		Identity identity = authenitcateRequest(accessTokenStr);

		try {
			String accessCode = paymentDao.initiateShoppingPayment(shoppingPayment, identity.getEntityId(), identity.getUserId(), identity.getDefaultRole(),
							channel);
			InitiateFeesPaymentResponse initiateFeesPaymentResponse = new InitiateFeesPaymentResponse();
			initiateFeesPaymentResponse.setAccessCode(accessCode);
			return initiateFeesPaymentResponse;
		} catch (Exception ex) {
			log.error("Exception>>>>>>>>>>>>>>>>>>>s", ex);
			ex.printStackTrace();
			throw ex;
		}

	}

//	@ResponseBody
	@RequestMapping(value = "/admission/fees/initiate", method = RequestMethod.POST/**, produces = { "application/json" }**/)
	public InitiateFeesPaymentResponse initiateOnlineAdmissionFeesPayment(
					@Valid @RequestBody OnlineAdmissionFee onlineAdmissionFee)
					throws AuthorisationException, JsonParseException, JsonMappingException, IOException {
			System.out.println("Entering admissin");
		try {
			log.debug("onlineAdmissionFee is::" + onlineAdmissionFee);
			OnlineAdmissionFormDetail onlineAdmissionFormDetails = onlineAdmissionDao.retrieveOnlineFormDetail(onlineAdmissionFee.getToken());
			OnlineAdmissionConfiguration configuration = onlineAdmissionDao.getAdmissionFormConfiguration(onlineAdmissionFormDetails.getBranch().getId());

			if (!"SUCCESS".equalsIgnoreCase(onlineAdmissionFormDetails.getPaymentStatus())) {
				String accessCode = paymentDao.initiateOnlineAdmissionFeesPayment(onlineAdmissionFee, onlineAdmissionFormDetails, "ADMISSION-WEB",
								configuration);
				InitiateFeesPaymentResponse initiateFeesPaymentResponse = new InitiateFeesPaymentResponse();
				initiateFeesPaymentResponse.setAccessCode(accessCode);
				return initiateFeesPaymentResponse;
			} else {
				throw new ApplicationException("This application is already paid.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@RequestMapping(value = "/acknowledge", method = RequestMethod.GET, produces = { "application/json" })
	public PaymentDetailResponse acknowledgePayment(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "code") String accessCode)
					throws AuthenticationException, SQLException, IOException, AuthorisationException, ParseException {
		Identity identity = authenitcateRequest(accessTokenStr);

		Integer entityId = null;
		String role = identity.getDefaultRole();

		if (!Identity.ROLES.ADMIN.get().equals(role)) {
			entityId = identity.getEntityId();
		}

		return paymentDao.acknowledgePayment(accessCode, entityId, identity.getDefaultRole());
	}


	@RequestMapping(value = "/mark-unpaid", method = RequestMethod.GET, produces = { "application/json" })
	public PaymentDetailResponse markPaymentUnpaid(@NotNull @RequestHeader(value = "token") String accessTokenStr,
			@NotNull @RequestParam(value = "code") String accessCode)
					throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);
		
		Integer entityId = null;
		String role = identity.getDefaultRole();

		if (!Identity.ROLES.ADMIN.get().equals(role)) {
			entityId = identity.getEntityId();
		}

		return paymentDao.markPaymentUnpaid(accessCode, entityId, identity.getDefaultRole());
	}
	

	@RequestMapping(value = "/refund", method = RequestMethod.GET, produces = { "application/json" })
	public void refundPayment(@NotNull @RequestHeader(value = "token") String accessTokenStr,
			@NotNull @RequestParam(value = "paymentId") Integer paymentId, @NotNull @RequestParam(value = "amount") Double amount ) throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);

		Integer entityId = null;
		String role = identity.getDefaultRole();

		if (!Identity.ROLES.ADMIN.get().equals(role) && !Identity.ROLES.SCHOOL_ADMIN.get().equals(role)) {
			throw new ApplicationException("Only admin or school admin can do this.");
		}

		paymentDao.refundFeePayment(paymentId, amount, identity.getUserId());
	}


	@RequestMapping(value = "/acknowledge/refund", method = RequestMethod.PUT, produces = { "application/json" })
	public void refundAcknowledge(@NotNull @RequestHeader(value = "token") String accessTokenStr, @RequestBody RefundDetail refundDetail) throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);

		Integer entityId = null;
		String role = identity.getDefaultRole();

		if (!Identity.ROLES.ADMIN.get().equals(role)) {
			throw new ApplicationException("Only admin or school admin can do this.");
		}

		paymentDao.acknowledgeRefund(refundDetail);
	}


	@RequestMapping(value = "/acknowledge", method = RequestMethod.POST, produces = { "application/json" })
	public void acknowledgePaymentProcessedByShoppingSite(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "key") String key, @NotNull @RequestParam(value = "payload") String payload)
					throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);
		if (!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed for this profile, only admin users allowed to perform this task.");
		}
		paymentDao.acknowledgePayment(key, payload, identity.getUserId());
	}

	@RequestMapping( method = RequestMethod.GET, produces = { "application/json" })
	public PaymentDetailResponse getPayment(
					@NotNull @RequestParam(value = "code") String accessCode)
					throws AuthenticationException, SQLException, IOException, AuthorisationException, ParseException {
		//authenitcateRequest(accessTokenStr);
		return paymentDao.getPayment(accessCode);
	}

	@RequestMapping(value = "/mode", method = RequestMethod.PUT, produces = { "application/json" })
	public PaymentDetailResponse updatePaymentAudit(@NotNull @RequestHeader("token") String token,
					@NotNull @RequestParam(value = "paymentMode") String paymentMode,
					@NotNull @RequestParam(value = "qfixReferenceNumber") String qfixReferenceNumber)
					throws AuthenticationException, SQLException, IOException, AuthorisationException, ParseException {
		//authenitcateRequest(accessTokenStr);
		Identity identity = authenitcateRequest(token);
		if (!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed for this profile, only admin users allowed to perform this task.");
		}
		return paymentDao.updatePaymentMode(paymentMode, qfixReferenceNumber);
	}

	@RequestMapping(value = "/{payment_id}/receipt", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ResponseEntity<byte[]> getPaymentReceipt(@NotNull @PathVariable(value = "payment_id") Long feeScheduleId,
					@RequestParam(value = "token") String accessTokenStr, 
					@RequestParam(value = "payment_id",required=false) Long paymentId, 
					@RequestParam(value = "pg_refund_id",required=false) Long pgRefundId)
					throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);

		BranchConfiguration branchConfiguration = null;
		System.out.println("CUR_ROLE ::: " + identity.getDefaultRole());

		Integer entityId = null;
		try {
			entityId = identity.getEntityId();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			System.out.println("Going to gET::::::=============RECIEPT====================");
			FeesInfo feeInfo = feeDetailsDAO.getFeeInfo(entityId, identity.getDefaultRole(), feeScheduleId, paymentId, pgRefundId);
			System.out.println("Fees info ::: " + feeInfo);
			branchConfiguration = paymentDao.getBranchConfiguration(feeInfo.getBranchId());

			if (feeInfo != null) {
				ResponseEntity<byte[]> response = null;
				ByteArrayOutputStream bao = null;
				try {

					String filename = "QFIX-PAYMENT-RECEIPT-" 
							+ (!StringUtils.isEmpty(feeInfo.getDescription()) ? feeInfo.getDescription() : feeInfo.getPaymentStatus());
					log.debug("filename ::" + filename);
					//					PDFReceiptGenerator pdfReceiptGenerator = new PDFReceiptGenerator();
					bao = new PaymentRecipt().createPdf(feeInfo, branchConfiguration);
					// System.out.println("bao is null :::" + (bao == null));
					if (bao != null) {
						HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.parseMediaType("application/pdf"));
						headers.setContentDispositionFormData(filename, filename + ".pdf");
						headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
						byte[] bytes = bao.toByteArray();
						response = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
					}

					return response;
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				} finally {
					if (bao != null) {
						bao.close();
					}
				}
			} else {
				throw new AuthorisationException("Invalid payment id specified.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String a[]) throws JsonParseException, JsonMappingException, IOException {
		String data = "{    \"id\": 7374,    \"qfix_reference_number\": \"19192640961414267889\",    \"amount\": 100,    \"option_name\": null,    \"internet_handling_charges\": 0,    \"payment_gateway_configuration_id\": 9,    \"customer_name\": \"Tushar D`Souja\",    \"customer_email\": \"diliphajare10@gmail.com\",    \"access_code\": \"c203083f-553b-4864-b3a3-cf3c76df8196\",    \"payment_status_code\": \"NEW\",    \"total_amount\": \"100.0\",    \"additional_properties\": {},    \"channel\": \"PARENT-WEB\",    \"payment_tax_type\": \"T1\",    \"payment_tax_details\": [],    \"shipping_charges\": \"0.0\",    \"payment_for_description\": \"DAILY SATURDAY (Amount:100.0)\",    \"mode_of_payment\": null,    \"customer_phone\": \"8097137625\",    \"customer_address\": \"\",    \"customer_city\": \"\",    \"customer_state\": \"\",    \"customer_pincode\": \"\",    \"customer_country\": \"IND\",    \"split_payment_detail_text\": \"test_100.0_0.0\",    \"offline_payments_enabled\": true,    \"payment_mode\": \"H\",    \"customer_unique_no\": \"Dummy First Standard - Dummy A - \"}";
		ObjectMapper mapper = new ObjectMapper();
		PaymentDetail p = mapper.readValue(data, PaymentDetail.class);
		System.out.println(p);
	}

	@RequestMapping(value = "/challan", method = RequestMethod.POST)
	public ResponseEntity<byte[]> getPaymentChallan(@RequestBody PaymentDetail paymentDetail,
					@RequestHeader(value = "token") String accessTokenStr)
					throws Exception {
		System.out.println("here............" + paymentDetail);
		Identity identity = authenitcateRequest(accessTokenStr);

		try {

			if (paymentDetail != null) {
				ResponseEntity<byte[]> response = null;
				ByteArrayOutputStream bao = null;
				try {

					String filename = "QFIX-PAYMENT-CHALLAN-" + paymentDetail.getQfixReferenceNumber();
					log.debug("filename ::" + filename);
					//					PDFReceiptGenerator pdfReceiptGenerator = new PDFReceiptGenerator();
					bao = new PaymentChallanGenerator().createPdf(paymentDetail);
					// System.out.println("bao is null :::" + (bao == null));
					if (bao != null) {
						HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.parseMediaType("application/pdf"));
						headers.setContentDispositionFormData(filename, filename + ".pdf");
						headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
						byte[] bytes = bao.toByteArray();
						response = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
						System.out.println("returning responseentity.......................");
						return response;
					}
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				} finally {
					if (bao != null) {
						bao.close();
					}
				}
			} else {
				throw new AuthorisationException("Invalid payment id specified.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/due/count", produces = { "application/json" })
	public String getUnreadPaymentCount(@NotNull @RequestHeader(value = "token") String accessTokenStr)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		// Get fees dues count
		if (ROLES.PARENT.get().equalsIgnoreCase(identity.getDefaultRole()) || ROLES.STUDENT.get().equalsIgnoreCase(identity.getDefaultRole())) {
			int feeDuesCount = feeDetailsDAO.getFeesDuesCount(identity.getEntityId(), identity.getDefaultRole());
			return "{\"payment\":\"" + feeDuesCount + "\"}";
		}

		return "{\"payment\":\"0\"}";
	}

	@RequestMapping(value = "/admission/fees/acknowledge", method = RequestMethod.GET, produces = { "application/json" })
	public PaymentDetailResponse acknowledgeOnlineAdmissionPayment(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "code") String accessCode)
					throws AuthenticationException, SQLException, IOException, AuthorisationException, ParseException {
		Identity identity = authenitcateRequest(accessTokenStr);
		return paymentDao.acknowledgeOnlineAdmissionPayment(accessCode);
	}

	@RequestMapping(value = "/scheme", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public List<PaymentBranchScheme> getBranchPaymentSchemeList(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "branch_id") Integer branchId)
					throws AuthenticationException, SQLException, IOException, AuthorisationException, ParseException {

		//System.out.println("token:: " + accessTokenStr);
		System.out.println("branchId:: " + branchId);

		Identity identity = authenitcateRequest(accessTokenStr);
		System.out.println("identity:: " + identity.toString());
		if (!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole()) && !Identity.ROLES.SCHOOL_ADMIN.get().equals(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed for this profile, only admin users allowed to perform this task.");
		} else if (Identity.ROLES.SCHOOL_ADMIN.get().equals(identity.getDefaultRole())
						&& !userDao.canUserAccessSpecifiedBranch(identity.getUserId(), branchId)) {
			throw new AuthorisationException("Function not allowed for this profile, invalid branch id specified.");
		}
		return paymentDao.getBranchPaymentSchemeList(branchId);
	}

	@RequestMapping(value = "/paymentMoreInfo", method = RequestMethod.GET, produces = { "application/json" })
	public PaymentMoreInfo getPaymentMoreInfo(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam("qfixReferenceNumber") String qfixReferenceNumber) throws Exception {
		log.debug("Inside processPaymentGatewayResponse");
		authenitcateRequest(accessTokenStr);
		return paymentDao.getPaymentMoreInfo(qfixReferenceNumber);
	}

}
