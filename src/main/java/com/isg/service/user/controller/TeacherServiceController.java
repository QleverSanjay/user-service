package com.isg.service.user.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.TeacherDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.Division;
import com.isg.service.user.request.Standard;
import com.isg.service.user.request.Teacher;

@RestController
@RequestMapping("/teacher")
public class TeacherServiceController extends BaseController {

	@Autowired
	TeacherDao teacherDao;

	@RequestMapping(value = "", method = RequestMethod.GET, produces = { "application/json" })
	public Teacher getTeacherDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthenticationException("Function not allowed.");
		}

		return teacherDao.getDetails(identity.getEntityId());
	}

	@RequestMapping(value = "", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateTeacherProfile(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody Teacher teacher) throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthenticationException("Function not allowed.");
		}

		teacher.setId(identity.getEntityId());
		boolean status = teacherDao.update(teacher);

		if (!status) {
			throw new ApplicationException("Error occured in teacher profile update process.");
		}
	}

	@RequestMapping(value = "/standard/all", method = RequestMethod.GET, produces = { "application/json" })
	public List<Standard> getAllStandard(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthenticationException("Function not allowed.");
		}

		return teacherDao.getStandard(identity.getEntityId());

	}

	@RequestMapping(value = "/standard/{standard_id}/division/all", method = RequestMethod.GET, produces = { "application/json" })
	public List<Division> getAllDivisions(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable(value = "standard_id") Integer standardId) throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!"TEACHER".equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthenticationException("Function not allowed.");
		}

		return teacherDao.getDivision(identity.getEntityId(), standardId);

	}

}
