package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.InstituteDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.Institute;

@RestController
public class InstituteServiceController extends BaseController {

	@Autowired
	InstituteDao instituteDao;

	@RequestMapping(value = "/institute", produces = { "application/json" })
	public List<Institute> getAll(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return instituteDao.getAll();
	}

	@RequestMapping(value = "/institute/{id}/branch/all", produces = { "application/json" })
	public List<Branch> getBranchesByInstituteId(@RequestHeader(value = "token",required=false) String accessTokenStr, @PathVariable Integer id)
					throws AuthenticationException, SQLException {
//		authenitcateRequest(accessTokenStr);
		return instituteDao.getBranchesByInstituteId(id);
	}

	@RequestMapping(value = "/branch/{id}", produces = { "application/json" })
	public Branch getBranchDetails(@RequestHeader(value = "token",required=false) String accessTokenStr, @PathVariable Integer id)
					throws AuthenticationException, SQLException {
//		authenitcateRequest(accessTokenStr);
		return instituteDao.getBranchDetails(id);
	}
	
	@RequestMapping(value="/institute/search",method=RequestMethod.GET)
	public List<Institute> searchInstitute(@RequestParam("name")String instituteName){
		return instituteDao.searchInstitute(instituteName);
	}

}
