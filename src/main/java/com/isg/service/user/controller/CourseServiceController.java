package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.CourseDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.request.Courses;

@RestController
public class CourseServiceController extends BaseController {

	@Autowired
	CourseDao courseDao;

	@RequestMapping(value = "/courses", produces = { "application/json" })
	public List<Courses> getAll(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException,
					AuthorisationException {
		authenitcateRequest(accessTokenStr);
		return courseDao.getAll();
	}

	@RequestMapping(value = "/institute/{instituteId}/branch/{branchId}/course/all", produces = { "application/json" })
	public List<Courses> getCourseNamesForInstitutes(@NotNull @RequestHeader(value = "token") String accessTokenStr, @PathVariable Integer instituteId,
					@PathVariable Integer branchId) throws AuthenticationException, SQLException, AuthorisationException {
		authenitcateRequest(accessTokenStr);
		return courseDao.getCourseNamesForInstitutes(instituteId, branchId);
	}

}
