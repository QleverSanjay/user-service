package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.MasterDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.request.Division;
import com.isg.service.user.request.Standard;

@RestController
public class MasterServiceController extends BaseController {

	@Autowired
	MasterDao masterDao;

	@RequestMapping(value = "/standards", produces = { "application/json" })
	public List<Standard> getStandards(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "branch_id") Integer branchId)
					throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return masterDao.getStandards(branchId);
	}

	@RequestMapping(value = "/divisions", produces = { "application/json" })
	public List<Division> getDivisions(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "standard_id") Integer standardId)
					throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return masterDao.getDivisions(standardId);
	}

	@RequestMapping(value = "/app/version", method = RequestMethod.POST, produces = { "application/json" })
	public void saveAppVersion(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestBody Map<String, String> payload)
					throws AuthenticationException, SQLException, AuthorisationException {

		String appCode = payload.get("app_code");
		String versionInfo = payload.get("version_information");

		if (StringUtils.isEmpty(appCode) || StringUtils.isEmpty(versionInfo)) {
			throw new ApplicationException("Required parameters are missing.");
		}

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed for this profile, only admin users allowed to perform this task.");
		}
		masterDao.saveAppVersion(appCode, versionInfo);
	}

	@RequestMapping(value = "/app/version", method = RequestMethod.GET, produces = { "application/json" })
	public Map<String, String> getAppVersion() throws AuthenticationException, SQLException {
		Map<String, String> versionInfo = masterDao.getAppVersion();
		if (versionInfo == null || versionInfo.isEmpty()) {
			throw new ApplicationException("System cannot find required app version information.");
		}

		return versionInfo;
	}

	@RequestMapping(value = "/call_db_function", method = RequestMethod.POST, produces = { "application/json" })
	public int callDatabaseFunction(@RequestBody Map<String, String> payload) throws Exception {

		Integer acId = Integer.parseInt(payload.get("academic_year_id"));
		Integer branchId = Integer.parseInt(payload.get("branch_id"));
		String mode = payload.get("mode");

		return masterDao.generateSerialNumber(acId, branchId, mode);
	}

}
