package com.isg.service.user.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.user.dao.EventDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.EventResponse;
import com.isg.service.user.model.EventSyncResponse;
import com.isg.service.user.request.Ediary;
import com.isg.service.user.request.Event;
import com.isg.service.user.request.Timetable;

@RestController
public class EventServiceController extends BaseController {

	@Autowired
	EventDao eventDao;

	@Autowired
	private StudentDao studentDao;

	@Autowired
	private UserDao userDao;

	@RequestMapping(value = "/events/sync", produces = { "application/json" })
	public EventSyncResponse syncEventsForAEntity(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "last_sync_timestamp", required = false) Long lastSyncTimeStamp,
					@RequestParam(value = "role", required = false) String role)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);

		String defaultRole = identity.getDefaultRole();

		if (!StringUtils.isEmpty(role) && identity.isRoleExists(role)) {
			defaultRole = role;
		}

		EventSyncResponse eventResponse = new EventSyncResponse();
		eventResponse.setLastSyncTimeStamp(System.currentTimeMillis());

		Collection<EventResponse> events = eventDao.syncEventsForAEntity(defaultRole, identity.getUserId(), identity.getEntityId(),
						(lastSyncTimeStamp == null || lastSyncTimeStamp == 0) ? System.currentTimeMillis() : lastSyncTimeStamp.longValue());
		eventResponse.setEventResponse(events);
		return eventResponse;

	}

	@RequestMapping(value = "/events", produces = { "application/json" })
	public Collection<EventResponse> getEventsForAEntity(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "start_date") String startDate, @NotNull @RequestParam(value = "end_date") String endDate,
					@RequestParam(value = "role", required = false) String role)
					throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);

		String defaultRole = identity.getDefaultRole();

		if (!StringUtils.isEmpty(role) && identity.isRoleExists(role)) {
			defaultRole = role;
		}

		return eventDao.getEventsForAEntity(defaultRole, identity.getUserId(), identity.getEntityId(), startDate, endDate);
	}

	@RequestMapping(value = "/ediary", method = RequestMethod.GET, produces = { "application/json" })
	public Collection<EventResponse> getEdiaryDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "start_date") String startDate,
					@NotNull @RequestParam(value = "end_date") String endDate) throws AuthenticationException,
					SQLException,
					AuthorisationException, ParseException {
		Identity identity = authenitcateRequest(accessTokenStr);

		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole())) && !("TEACHER".equalsIgnoreCase(identity.getDefaultRole()))
						&& !("STUDENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}

		return eventDao.getEdiaryDetails(identity.getDefaultRole(), identity.getUserId(), identity.getEntityId(), startDate, endDate);
	}

	@RequestMapping(value = "/ediary/sync", produces = { "application/json" })
	public EventSyncResponse syncEdiaryForAEntity(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "last_sync_timestamp", required = false) Long lastSyncTimeStamp,
					@RequestParam(value = "role", required = false) String role)
					throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);

		String defaultRole = identity.getDefaultRole();

		if (!StringUtils.isEmpty(role) && identity.isRoleExists(role)) {
			defaultRole = role;
		}

		if (!("PARENT".equalsIgnoreCase(defaultRole)) && !("TEACHER".equalsIgnoreCase(defaultRole))
						&& !("STUDENT".equalsIgnoreCase(defaultRole))) {
			throw new AuthorisationException("Function not allowed.");
		}

		EventSyncResponse eventResponse = new EventSyncResponse();
		eventResponse.setLastSyncTimeStamp(System.currentTimeMillis());

		Collection<EventResponse> events = eventDao.syncEdiaryForAEntity(defaultRole, identity.getUserId(), identity.getEntityId(),
						(lastSyncTimeStamp == null || lastSyncTimeStamp == 0) ? System.currentTimeMillis() : lastSyncTimeStamp.longValue());
		eventResponse.setEventResponse(events);
		return eventResponse;

	}

	@RequestMapping(value = "/ediary/list", method = RequestMethod.GET, produces = { "application/json" })
	public Collection<Ediary> getEdiaryDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException,
					SQLException,
					AuthorisationException, ParseException {
		Identity identity = authenitcateRequest(accessTokenStr);
		System.out.println("identity ::::::::" + identity.toString());
		if (!identity.getRoleDetails().contains(ROLES.TEACHER.get())) {
			throw new AuthorisationException("Function not allowed.");
		}

		return eventDao.getEdiaryDetails(identity.getUserId());
	}

	@RequestMapping(value = "/event/{event_id}", produces = { "application/json" })
	public Event getEventDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable("event_id") Integer eventId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		return eventDao.getEventDetails(identity.getUserId(), identity.getEntityId(), eventId);
	}

	@RequestMapping(value = "/ediary/{ediary_id}", produces = { "application/json" })
	public Ediary getEdiaryDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable("ediary_id") Integer ediaryId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		return eventDao.getEdiaryDetail(identity.getUserId(), identity.getEntityId(), ediaryId, identity.getDefaultRole());
	}

	@RequestMapping(value = "/branch/{branch_id}/timetable", method = RequestMethod.POST, produces = { "application/json" })
	public void saveTimeTable(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable("branch_id") Integer branchId,
					@Valid @RequestBody Timetable timetable)
					throws AuthenticationException, SQLException, AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);

		if (timetable.getDetail().isEmpty()) {
			throw new ApplicationException("Timetable data is missing.");
		}

		if (!(ROLES.ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())) && !(ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		} else if (ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())
						&& !userDao.canUserAccessSpecifiedBranch(identity.getUserId(), branchId)) {
			throw new AuthorisationException("Function not allowed.");
		}

		eventDao.saveTimeTable(timetable, branchId, identity.getUserId(), identity.getDefaultRole());

	}

	@RequestMapping(value = "/branch/{branch_id}/timetable/{timetable_id}", method = RequestMethod.PUT, produces = { "application/json" })
	public void updateTimeTable(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable("branch_id") Integer branchId,
					@NotNull @PathVariable("timetable_id") Integer timetableId,
					@Valid @RequestBody Timetable timetable)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (timetable.getDetail().isEmpty()) {
			throw new ApplicationException("Timetable data is missing.");
		}

		if (!ROLES.ADMIN.get().equalsIgnoreCase(identity.getDefaultRole()) && !ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		} else if (ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())
						&& !userDao.canUserAccessSpecifiedBranch(identity.getUserId(), branchId)) {
			throw new AuthorisationException("Function not allowed.");
		} else if (ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())
						&& !eventDao.canUserUpdateSpecifiedTimetable(branchId, timetableId)) {
			throw new AuthorisationException("Function not allowed.");
		}

		eventDao.updateTimeTable(timetable, timetableId, branchId, identity.getUserId(), identity.getDefaultRole());

	}

	@RequestMapping(value = "/branch/{branch_id}/timetable/{timetable_id}", method = RequestMethod.DELETE, produces = { "application/json" })
	public void deleteTimeTable(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable("branch_id") Integer branchId,
					@NotNull @PathVariable("timetable_id") Integer timetableId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!ROLES.ADMIN.get().equalsIgnoreCase(identity.getDefaultRole()) && !ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		} else if (ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())
						&& !userDao.canUserAccessSpecifiedBranch(identity.getUserId(), branchId)) {
			throw new AuthorisationException("Function not allowed.");
		} else if (ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())
						&& !eventDao.canUserUpdateSpecifiedTimetable(branchId, timetableId)) {
			throw new AuthorisationException("Function not allowed.");
		}

		eventDao.deletTimetable(timetableId);

	}

	@RequestMapping(value = "/timetable", method = RequestMethod.GET, produces = { "application/json" })
	public Timetable getTimeTable(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "standard_id") Integer standardId,
					@RequestParam(value = "division_id", required = false) Integer divisionId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if (!ROLES.ADMIN.get().equalsIgnoreCase(identity.getDefaultRole()) && !ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(identity.getDefaultRole())
						&& !ROLES.TEACHER.get().equalsIgnoreCase(identity.getDefaultRole())) {
			throw new AuthorisationException("Function not allowed.");
		} else {
			//TODO to check if branch is allowed and standards specified are available under branch.
		}

		return eventDao.getTimeTableForSchool(standardId, divisionId);

	}

	@RequestMapping(value = "/branch/{branch_id}/timetable", method = RequestMethod.GET, produces = { "application/json" })
	public Timetable getTimeTableForStudent(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable("branch_id") Integer branchId,
					@NotNull @RequestParam(value = "student_id") Integer studentId)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		if ("PARENT".equalsIgnoreCase(identity.getDefaultRole()) && studentDao.canAccessDetails(identity.getEntityId(), studentId)) {
			//Ignore
		} else if ("STUDENT".equalsIgnoreCase(identity.getDefaultRole())) {
			studentId = identity.getEntityId();
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		return eventDao.getTimeTableForStudent(branchId, studentId);

	}

	@RequestMapping(value = "/ediary/mark/read", method = RequestMethod.POST, produces = { "application/json" })
	public Map<String, Integer> markEdiaryAsRead(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody Map<String, Integer> payload)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		Integer ediaryId = payload.get("ediary_id");
		Integer forUserId = payload.get("for_user_id");

		if (ediaryId == null || ediaryId.intValue() == 0) {
			throw new ApplicationException("Required parameter ediaryId id not specified.");
		}

		if (!"PARENT".equalsIgnoreCase(identity.getDefaultRole())) {
			forUserId = identity.getUserId();
		} else if (forUserId == null || forUserId.intValue() == 0) {
			throw new ApplicationException("Required parameter for user id not specified.");
		}

		return eventDao.markEdiaryAsRead(ediaryId, identity.getUserId(), forUserId);
	}

	@RequestMapping(value = "/event/mark/read", method = RequestMethod.POST, produces = { "application/json" })
	public Map<String, Integer> markEventAsRead(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody Map<String, Integer> payload)
					throws AuthenticationException, SQLException, AuthorisationException {

		Identity identity = authenitcateRequest(accessTokenStr);

		Integer eventId = payload.get("event_id");
		Integer forUserId = payload.get("for_user_id");

		if (eventId == null || eventId.intValue() == 0) {
			throw new ApplicationException("Required parameter eventId id not specified.");
		}

		if (!"PARENT".equalsIgnoreCase(identity.getDefaultRole())) {
			forUserId = identity.getUserId();
		} else if (forUserId == null || forUserId.intValue() == 0) {
			throw new ApplicationException("Required parameter for user id not specified.");
		}

		return eventDao.markEventAsRead(eventId, identity.getUserId(), forUserId, identity.getEntityId(), identity.getDefaultRoleId());
	}

}
