package com.isg.service.user.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isg.service.identity.dao.IdentityDao;
import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.identity.model.Identity.ROLES_ID;
import com.isg.service.identity.model.Token;
import com.isg.service.user.client.cache.CacheManager;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.util.JsonUtil;

@RestController
public class BaseController {

	@Autowired
	IdentityDao identityDao;

	@Autowired
	CacheManager cacheManager;

	private static final Logger log = Logger.getLogger(BaseController.class);

	Identity authenitcateRequest(String accessTokenStr) throws AuthenticationException, SQLException {

		Identity identity;

		String cachedIdentity = cacheManager.get(accessTokenStr);

		if (StringUtils.isEmpty(cachedIdentity)) {
			Token token = identityDao.validateToken(accessTokenStr);
			identity = token.getIdentity();
			storeIdentityInCache(accessTokenStr, identity);

		} else {
			try {
				identity = JsonUtil.convertJsonToObject(cachedIdentity, Identity.class);
				log.debug("Identity details ::" + identity);
			} catch (IOException e) {
				throw new AuthenticationException("Error occured while authenticating token.");
			}
		}

		return identity;

	}

	void invalidateExistingToken(String accessTokenStr) throws AuthenticationException, SQLException {
		cacheManager.remove(accessTokenStr);
		identityDao.invalidateToken(accessTokenStr);

	}

	void storeIdentityInCache(String accessTokenStr, Identity identity) throws AuthenticationException {
		String json;
		try {
			json = JsonUtil.convertObjectToJson(identity);
			cacheManager.store(accessTokenStr, json);
			log.debug("Stored value in cache manager." + accessTokenStr);
		} catch (JsonProcessingException e) {
			log.error(e);
			throw new AuthenticationException("Error occured while storing token in cache.");
		}
	}

	int getDefaultRoleId(List<String> roleDetails) {

		if (roleDetails.contains(ROLES.PARENT.get())) {
			return ROLES_ID.PARENT.get();
		} else if (roleDetails.contains(ROLES.STUDENT.get())) {
			return ROLES_ID.STUDENT.get();
		} else if (roleDetails.contains(ROLES.TEACHER.get())) {
			return ROLES_ID.TEACHER.get();
		} else if (roleDetails.contains(ROLES.VENDOR.get())) {
			return ROLES_ID.VENDOR.get();
		} else if (roleDetails.contains(ROLES.ADMIN.get())) {
			return ROLES_ID.ADMIN.get();
		} else if (roleDetails.contains(ROLES.SCHOOL_ADMIN.get())) {
			return ROLES_ID.SCHOOL_ADMIN.get();
		} else if (roleDetails.contains(ROLES.BANK_PARTNER.get())) {
			return ROLES_ID.BANK_PARTNER.get();
		}else {
			throw new IllegalArgumentException("Role not available.");
		}
	}
}
