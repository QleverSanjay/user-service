package com.isg.service.user.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
	
	@RequestMapping(value="/health-check",method=RequestMethod.GET)
	public ResponseEntity healthCheck(){
		return ResponseEntity.ok("System is healthy");
	}
}