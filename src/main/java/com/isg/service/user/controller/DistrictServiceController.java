package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.DistrictDao;
import com.isg.service.user.dao.TalukaDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.District;
import com.isg.service.user.request.Taluka;

@RestController
//@RequestMapping("/districts")
public class DistrictServiceController extends BaseController {

	@Autowired
	DistrictDao districtDao;

	@Autowired
	TalukaDao talukaDAO;

	@RequestMapping(produces = { "application/json" })
	public List<District> getDistricts(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return districtDao.getDistricts();
	}

	@RequestMapping(value = "/district/{id}/taluka_and_cities", produces = { "application/json" })
	public List<Taluka> getCitiesByDistrictId(@PathVariable Integer id)
					throws AuthenticationException, SQLException {
		return talukaDAO.getTalukasOrCitiesByDistrictId(id);
	}

}
