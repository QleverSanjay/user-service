package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.ReligionDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.request.Religion;

@RestController
@RequestMapping("/religions")
public class ReligionServiceController extends BaseController {

	@Autowired
	ReligionDao religionDao;

	@RequestMapping(value = "", produces = { "application/json" })
	public List<Religion> getReligions(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException,
					AuthorisationException {
		return religionDao.getReligions();
	}

	@RequestMapping(value = "/{id}", produces = { "application/json" })
	public List<Religion> getReligionsById(@NotNull @RequestHeader(value = "token") String accessTokenStr, @PathVariable Integer id)
					throws AuthenticationException, SQLException, AuthorisationException {
		authenitcateRequest(accessTokenStr);
		return religionDao.getReligionsById(id);
	}

	@RequestMapping(value = "/name/{name}", produces = { "application/json" })
	public List<Religion> getReligionsByName(@NotNull @RequestHeader(value = "token") String accessTokenStr, @PathVariable String name)
					throws AuthenticationException, SQLException, AuthorisationException {
		authenitcateRequest(accessTokenStr);
		return religionDao.getReligionsByName(name);
	}
}
