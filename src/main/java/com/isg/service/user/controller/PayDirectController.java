package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.dao.IdentityDao;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.identity.model.Token;
import com.isg.service.user.dao.PayDirectDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.Institute;


@RestController
@RequestMapping(value = "/payDirect")
public class PayDirectController extends BaseController {

	@Autowired
	IdentityDao identityDao;
	
	@Autowired
	PayDirectDao payDirectDao;

	@RequestMapping(value = "/payment/token", method = RequestMethod.POST, produces = { "application/json" })
	public String generatePayDirectToken(
			@RequestParam("id") Integer userId) throws Exception {
		
		Integer roleId = getDefaultRoleId(Collections
				.singletonList(ROLES.STUDENT.get()));

		Token token = identityDao.generateToken(userId, roleId);
		System.out.println("role id for student>> " + roleId);
		/*if (token == null) {
			return ResponseEntity
					.badRequest()
					.body("Cannot procced for payment right now. Try again after sometime.");
		}*/

		String tokenStr = token.getToken();

		// Save identity in the cache
		storeIdentityInCache(tokenStr, token.getIdentity());
		System.out.println("token generated>>>" + tokenStr);
		return "{\"token\":\"" + token.getToken() + "\"}";
		//return ResponseEntity.ok(tokenStr);
	}
	
	
	@RequestMapping(value = "/institute/{id}/branch/all", produces = { "application/json" })
	public List<Branch> getBranchesByInstituteId(@RequestHeader(value = "token",required=false) String accessTokenStr, @PathVariable Integer id)
					throws AuthenticationException, SQLException {
//		authenitcateRequest(accessTokenStr);
		return payDirectDao.getBranchesByInstituteId(id);
	}

	
	@RequestMapping(value="/institute/search",method=RequestMethod.GET)
	public List<Institute> searchInstitute(@RequestParam("name")String instituteName){
		return payDirectDao.searchInstitute(instituteName);
	}

}
