package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Token;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.request.Parent;
import com.isg.service.user.request.ParentRegistrationByAdmin;
import com.isg.service.user.request.Student;
import com.isg.service.user.response.InviteResponse;
import com.isg.service.user.response.ParentRegistrationByAdminResponse;
import com.isg.service.user.response.RegistrationResponse;
import com.isg.service.user.response.StudentBasicProfile;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.Constant.AUTHENTICATION_TYPE;

@RestController
@RequestMapping("/parent")
public class ParentServiceController extends BaseController {

	private static final Logger log = Logger.getLogger(ParentServiceController.class);

	@Autowired
	ParentDao parentDao;

	@Autowired
	StudentDao studentDao;

	/**
	 * Registers a new parent on system.
	 * 
	 * @param parent
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public RegistrationResponse register(@Valid @RequestBody Parent parent) throws Exception {

		// AuthenticationResponse response = parentDao.registerParent(parent,
		// AUTHENTICATION_TYPE.APPLICATION.get());
		RegistrationResponse registrationResponse = parentDao.registerParent(parent, AUTHENTICATION_TYPE.APPLICATION.get());
		registrationResponse.setStatus(Constant.USER_STATUS.NEW.get());

		Token token = identityDao.generateToken(registrationResponse.getUserId(), getDefaultRoleId(registrationResponse.getRoles()));
		String tokenStr = token.getToken();
		registrationResponse.setToken(tokenStr);
		// Save identity in the cache
		storeIdentityInCache(tokenStr, token.getIdentity());

		return registrationResponse;
	}

	@RequestMapping(value = "/register_by_admin", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public List<ParentRegistrationByAdminResponse> registerParentByAdmin(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@Valid @RequestBody ParentRegistrationByAdmin parent) throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!(Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole())) && !(Identity.ROLES.SCHOOL_ADMIN.get().equals(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}

		return parentDao.registerParentByAdmin(parent);

	}

	@RequestMapping(value = "/registerMobile", method = RequestMethod.POST, produces = { "application/json" })
	public boolean registerMobile(@NotNull @RequestHeader(value = "token") String accessTokenStr, @NotNull @RequestParam(value = "gcmId") String gcmId)
					throws SQLException, AuthenticationException, AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);
		return parentDao.registerMobile(identity.getEntityId(), gcmId);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = { "application/json" })
	public boolean updateParent(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody Parent parent) throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);
		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}
		parent.setId(identity.getEntityId());
		boolean response = parentDao.updateParent(parent);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public Parent getParent(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);
		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}

		return parentDao.getParent(identity.getEntityId());
	}

	@RequestMapping(value = "/student/profile", produces = { "application/json" })
	public List<StudentBasicProfile> getStudentBasicProfileDetails(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}
		return studentDao.getStudentBasicProfileDetails(identity.getEntityId());
	}

	@RequestMapping(value = "/student/list", method = RequestMethod.GET, produces = { "application/json" })
	public List<Student> list(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException,
					AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);
		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}
		return studentDao.listByParentId(identity.getEntityId());
	}

	/**
	 * Invite a new parent on system.
	 * 
	 * @param parent
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/invite", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public void sendInvite(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody Parent parent) throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);

		if (!("PARENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}

		parentDao.sendInvite(parent.getFirstname(), parent.getEmail(), parent.getPrimaryContact(), identity.getUserId());

	}

	/**
	 * Verify invitation code.
	 * 
	 * @param parent
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/verify/invite_code", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public InviteResponse verifyInvite(@RequestBody Map<String, String> payload) throws Exception {
		String otp = payload.get("invite_code");

		if (StringUtils.isEmpty(otp)) {
			throw new ApplicationException("Required parameter is missing.");
		}

		return parentDao.validateInvite(otp);

	}

}
