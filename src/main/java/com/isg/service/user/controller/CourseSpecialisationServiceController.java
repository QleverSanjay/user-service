package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.CourseSpecialisationDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.CourseSpecialisation;

@RestController
public class CourseSpecialisationServiceController extends BaseController {

	@Autowired
	CourseSpecialisationDao coursespecialisationDao;

	@RequestMapping(value = "/course/{id}/specialisation/all", produces = { "application/json" })
	public List<CourseSpecialisation> getAllByCourseId(@NotNull @RequestHeader(value = "token") String accessTokenStr, @PathVariable Integer id)
					throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return coursespecialisationDao.getAllByCourseId(id);
	}

	@RequestMapping(value = "/institute/{instituteId}/branch/{branchId}/course/{courseId}/specialisation/all", produces = { "application/json" })
	public List<CourseSpecialisation> getCourseSpecialisationForInstitutes(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@PathVariable Integer instituteId,
					@PathVariable Integer branchId, @PathVariable Integer courseId) throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);
		return coursespecialisationDao.getCourseSpecialisationForInstitutes(instituteId, branchId, courseId);
	}

}
