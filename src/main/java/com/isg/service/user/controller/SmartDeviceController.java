package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.ReportDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.model.HealthChart;
import com.isg.service.user.util.DateUtil;

@RestController
@RequestMapping("/device")
public class SmartDeviceController extends BaseController {

	@Autowired
	ReportDao reportDao;
	
	static Map<Date, Integer> stepsMap = new HashMap<>();
	static {
		stepsMap.put(new Date("05/01/2016"), 1310);
		stepsMap.put(new Date("05/02/2016"), 6550);
		stepsMap.put(new Date("05/03/2016"), 1310);
		stepsMap.put(new Date("05/04/2016"), 5240);
		stepsMap.put(new Date("05/05/2016"), 1310);
		stepsMap.put(new Date("05/06/2016"), 1310);
		stepsMap.put(new Date("05/07/2016"), 1310);
		stepsMap.put(new Date("05/08/2016"), 2620);
		stepsMap.put(new Date("05/09/2016"), 2620);
		stepsMap.put(new Date("05/10/2016"), 2620);
		stepsMap.put(new Date("05/11/2016"), 3930);
		stepsMap.put(new Date("05/12/2016"), 5240);
		stepsMap.put(new Date("05/13/2016"), 6550);
		stepsMap.put(new Date("05/14/2016"), 7860);
		stepsMap.put(new Date("05/15/2016"), 1310);
		stepsMap.put(new Date("05/16/2016"), 1310);
		stepsMap.put(new Date("05/17/2016"), 1310);
		stepsMap.put(new Date("05/18/2016"), 1310);
		stepsMap.put(new Date("05/19/2016"), 1310);
		stepsMap.put(new Date("05/20/2016"), 1310);
		stepsMap.put(new Date("05/21/2016"), 1310);
		stepsMap.put(new Date("05/22/2016"), 1310);
		stepsMap.put(new Date("05/23/2016"), 7860);
		stepsMap.put(new Date("05/24/2016"), 1310);
		stepsMap.put(new Date("05/25/2016"), 1310);
		stepsMap.put(new Date("05/26/2016"), 1310);
		stepsMap.put(new Date("05/27/2016"), 2620);
		stepsMap.put(new Date("05/28/2016"), 2620);
		stepsMap.put(new Date("05/29/2016"), 3930);
		stepsMap.put(new Date("05/30/2016"), 3930);
		stepsMap.put(new Date("05/31/2016"), 3930);
		stepsMap.put(new Date("06/01/2016"), 1310);
		stepsMap.put(new Date("06/02/2016"), 1310);
		stepsMap.put(new Date("06/03/2016"), 1310);
		stepsMap.put(new Date("06/04/2016"), 5240);
		stepsMap.put(new Date("06/05/2016"), 3930);
		stepsMap.put(new Date("06/06/2016"), 1310);
		stepsMap.put(new Date("06/07/2016"), 2620);
		stepsMap.put(new Date("06/08/2016"), 2620);
		stepsMap.put(new Date("06/09/2016"), 1310);
		stepsMap.put(new Date("06/10/2016"), 1310);
		stepsMap.put(new Date("06/11/2016"), 2620);
		stepsMap.put(new Date("06/12/2016"), 2620);
		stepsMap.put(new Date("06/13/2016"), 2620);
		stepsMap.put(new Date("06/14/2016"), 1310);
		/*stepsMap.put(new Date("02/15/2016"), 1310);
		stepsMap.put(new Date("02/16/2016"), 3930);
		stepsMap.put(new Date("02/17/2016"), 3930);
		stepsMap.put(new Date("02/18/2016"), 3930);
		stepsMap.put(new Date("02/19/2016"), 3930);
		stepsMap.put(new Date("02/20/2016"), 3930);
		stepsMap.put(new Date("02/21/2016"), 1310);
		stepsMap.put(new Date("02/22/2016"), 1310);
		stepsMap.put(new Date("02/23/2016"), 1310);
		stepsMap.put(new Date("02/24/2016"), 2620);
		stepsMap.put(new Date("02/25/2016"), 2620);
		stepsMap.put(new Date("02/26/2016"), 5240);
		stepsMap.put(new Date("02/27/2016"), 5240);
		stepsMap.put(new Date("02/28/2016"), 6550);*/
	}

	static Map<Date, Integer> distanceMap = new HashMap<>();
	static {
		distanceMap.put(new Date("05/01/2016"), 1000);
		distanceMap.put(new Date("05/02/2016"), 5000);
		distanceMap.put(new Date("05/03/2016"), 1000);
		distanceMap.put(new Date("05/04/2016"), 4000);
		distanceMap.put(new Date("05/05/2016"), 1000);
		distanceMap.put(new Date("05/06/2016"), 1000);
		distanceMap.put(new Date("05/07/2016"), 1000);
		distanceMap.put(new Date("05/08/2016"), 2000);
		distanceMap.put(new Date("05/09/2016"), 2000);
		distanceMap.put(new Date("05/10/2016"), 2000);
		distanceMap.put(new Date("05/11/2016"), 3000);
		distanceMap.put(new Date("05/12/2016"), 4000);
		distanceMap.put(new Date("05/13/2016"), 5000);
		distanceMap.put(new Date("05/14/2016"), 6000);
		distanceMap.put(new Date("05/15/2016"), 1000);
		distanceMap.put(new Date("05/16/2016"), 1000);
		distanceMap.put(new Date("05/17/2016"), 1000);
		distanceMap.put(new Date("05/18/2016"), 1000);
		distanceMap.put(new Date("05/19/2016"), 1000);
		distanceMap.put(new Date("05/20/2016"), 1000);
		distanceMap.put(new Date("05/21/2016"), 1000);
		distanceMap.put(new Date("05/22/2016"), 1000);
		distanceMap.put(new Date("05/23/2016"), 6000);
		distanceMap.put(new Date("05/24/2016"), 1000);
		distanceMap.put(new Date("05/25/2016"), 1000);
		distanceMap.put(new Date("05/26/2016"), 1000);
		distanceMap.put(new Date("05/27/2016"), 2000);
		distanceMap.put(new Date("05/28/2016"), 2000);
		distanceMap.put(new Date("05/29/2016"), 3000);
		distanceMap.put(new Date("05/30/2016"), 3000);
		distanceMap.put(new Date("05/31/2016"), 3000);
		distanceMap.put(new Date("06/01/2016"), 1000);
		distanceMap.put(new Date("06/02/2016"), 1000);
		distanceMap.put(new Date("06/03/2016"), 1000);
		distanceMap.put(new Date("06/04/2016"), 4000);
		distanceMap.put(new Date("06/05/2016"), 3000);
		distanceMap.put(new Date("06/06/2016"), 1000);
		distanceMap.put(new Date("06/07/2016"), 1000);
		distanceMap.put(new Date("06/08/2016"), 2000);
		distanceMap.put(new Date("06/09/2016"), 1000);
		distanceMap.put(new Date("06/10/2016"), 1000);
		distanceMap.put(new Date("06/11/2016"), 2000);
		distanceMap.put(new Date("06/12/2016"), 2000);
		distanceMap.put(new Date("06/13/2016"), 2000);
		distanceMap.put(new Date("06/14/2016"), 1000);
		/*distanceMap.put(new Date("02/15/2016"), 1000);
		distanceMap.put(new Date("02/16/2016"), 3000);
		distanceMap.put(new Date("02/17/2016"), 3000);
		distanceMap.put(new Date("02/18/2016"), 3000);
		distanceMap.put(new Date("02/19/2016"), 3000);
		distanceMap.put(new Date("02/20/2016"), 3000);
		distanceMap.put(new Date("02/21/2016"), 1000);
		distanceMap.put(new Date("02/22/2016"), 1000);
		distanceMap.put(new Date("02/23/2016"), 1000);
		distanceMap.put(new Date("02/24/2016"), 2000);
		distanceMap.put(new Date("02/25/2016"), 2000);
		distanceMap.put(new Date("02/26/2016"), 4000);
		distanceMap.put(new Date("02/27/2016"), 4000);
		distanceMap.put(new Date("02/28/2016"), 5000);*/
	}

	// @RequestMapping(value = "/getLocation")
	/*
	 * public Location getLocations() { String confFile =
	 * "applicationContext.xml"; ConfigurableApplicationContext context = new
	 * ClassPathXmlApplicationContext(confFile); LocationDao locationDao =
	 * (LocationDao) context.getBean("locationDAO"); return
	 * locationDao.getLocation(); }
	 */
	@RequestMapping(value = "/health/report", produces = { "application/json" })
	public HealthChart getChartData(@NotNull @RequestHeader(value = "token") String accessTokenStr, @NotNull @RequestParam(value = "month") int month,
					@NotNull @RequestParam(value = "year") int year, @RequestParam(value = "for_user_id", required = false) Integer forEntityUserId)
					throws AuthenticationException, SQLException {
		authenitcateRequest(accessTokenStr);

		return getHealthData(month, year);
	}

	private HealthChart getHealthData(int month, int year) {
		HealthChart healthChart = new HealthChart();
		List<String> seriesList = new ArrayList<>();
		List<String> weekList = new ArrayList<>();
		List<Integer> stepsList = new ArrayList<>();
		List<Integer> distanceList = new ArrayList<>();
		Integer[] weeks = DateUtil.getWeeksOfMonth(month, year);
		Integer weekStart = weeks[0];
		for (Integer week : weeks) {
			weekList.add("W" + week);
			String startDate = DateUtil.getStartDateOfWeek(month, year, week);
			String endDate = DateUtil.getEndDateOfWeek(month, year, week);
			List<Date> dates = DateUtil.getDatesBetweenDates(DateUtil.parseDate(startDate), DateUtil.parseDate(endDate), month);
			Integer steps = 0;
			Integer distance = 0;
			for (Date date : dates) {
				steps = steps + (stepsMap.get(date) == null ? 0 : stepsMap.get(date));
				distance = distance + (distanceMap.get(date) == null ? 0 : distanceMap.get(date));
			}
			stepsList.add(steps);
			distanceList.add(distance);
			System.out.println("Week : " + (week - (weekStart - 1)) + " Steps : " + steps + " Distance : " + distance);
		}
		seriesList.add("Steps");
		seriesList.add("Distance");
		healthChart.setSeries(seriesList.toArray(new String[0]));
		healthChart.setWeeks(weekList.toArray(new String[0]));
		healthChart.getData().add(stepsList.toArray());
		healthChart.getData().add(distanceList.toArray());

		return healthChart;
	}

	/*
	 * public static void main(String args[]) { SmartDeviceController
	 * smartDeviceController = new SmartDeviceController();
	 * System.out.println(smartDeviceController.getChartData()); }
	 */

}
