package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.user.dao.BranchDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.AcademicYear;
import com.isg.service.user.model.HolidayResponse;
import com.isg.service.user.model.StandardDivision;
import com.isg.service.user.model.onlineadmission.Category;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.FeesCode;

@RestController
public class BranchServiceController extends BaseController {

	private static final Logger log = Logger.getLogger(BranchServiceController.class);

	@Autowired
	BranchDao branchDao;

	@RequestMapping(value = "/branches", produces = { "application/json" })
	public List<Branch> getAllBranches(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException, SQLException,
					AuthorisationException {
		authenitcateRequest(accessTokenStr);
		try {
			return branchDao.getAll();
		} catch (Exception e) {
			log.error("Error occured in getAllBranches() service call", e);
		}

		return new ArrayList<Branch>();
	}

	@RequestMapping(value = "/holidays", produces = { "application/json" })
	public Collection<HolidayResponse> getHolidays(@NotNull @RequestHeader(value = "token") String accessTokenStr) throws AuthenticationException,
					SQLException,
					AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);

		if (!(ROLES.PARENT.get().equalsIgnoreCase(identity.getDefaultRole())) && !(ROLES.TEACHER.get().equalsIgnoreCase(identity.getDefaultRole()))
						&& !("STUDENT".equalsIgnoreCase(identity.getDefaultRole()))) {
			throw new AuthorisationException("Function not allowed.");
		}

		return branchDao.getHolidays(identity.getUserId(), identity.getEntityId(), identity.getDefaultRole());
	}

	@RequestMapping(value = "/branch/{branch_id}/academic-year/list", produces = { "application/json" })
	public List<AcademicYear> getAcademicYearList(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable(value = "branch_id") Integer branchId) throws AuthenticationException,
					SQLException,
					AuthorisationException {

		return branchDao.getAcademicYearList(branchId);
	}
	
	
	/*@RequestMapping(value = "standard/search",method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<HashMap<String,List>> getStandardDivisionCategoryList(@RequestParam(value = "branchId") Integer branchId){
		
		List<StandardDivision> division=null;
		List<Category> category=null;
		List<FeesCode> feesCodeList=null;
		try{
			division = branchDao.getStandardList(branchId);
			category=branchDao.getCategoryList();
			
			
		}catch(Exception e){
			e.printStackTrace();
			ResponseEntity.badRequest().body("contact system admin");
		}
		
		HashMap<String, List> response = new HashMap<String, List>();
		response.put("standardDivision",division);
		response.put("category",category );
		
		return ResponseEntity.ok(response);
	}*/
	
	
	
}
