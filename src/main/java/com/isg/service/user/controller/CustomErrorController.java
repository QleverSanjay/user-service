package com.isg.service.user.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomErrorController implements ErrorController {
	private static final String ERROR_PATH = "/error";

	@RequestMapping(value = ERROR_PATH)
	public String handleError() {
		System.out.println("Unexepcted error...please contact system administrator");
		return "Unexepcted error...please contact system administrator.";
	}

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}
}
