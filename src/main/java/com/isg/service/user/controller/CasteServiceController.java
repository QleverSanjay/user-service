package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.user.dao.CasteDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.Caste;

@RestController
public class CasteServiceController extends BaseController {

	@Autowired
	CasteDao casteDao;

	@RequestMapping(value = "/castes", produces = { "application/json" })
	public List<Caste> getCastes() throws AuthenticationException, SQLException {
		return casteDao.getCastes();
	}

	@RequestMapping(value = "/caste/{id}", produces = { "application/json" })
	public Caste getCasteById(@NotNull @RequestHeader(value = "token") String accessTokenStr, @NotNull @PathVariable Integer id)
					throws AuthenticationException,
					SQLException {
		authenitcateRequest(accessTokenStr);
		return casteDao.getCasteById(id);
	}

	@RequestMapping(value = "/castes/name/{name}", produces = { "application/json" })
	public List<Caste> getCastesByName(@NotNull @RequestHeader(value = "token") String accessTokenStr, @NotNull @PathVariable String name)
					throws AuthenticationException,
					SQLException {
		authenitcateRequest(accessTokenStr);
		return casteDao.getCastesByName(name);
	}

	@RequestMapping(value = "/castes/{id}/subcaste/all", produces = { "application/json" })
	public List<Caste> getSubCastesByCasteId(@PathVariable Integer id)
					throws AuthenticationException {
		return casteDao.getSubCastesByCasteId(id);
	}

}
