package com.isg.service.user.controller;

import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.ServiceRequestDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.request.ServiceRequest;
import com.isg.service.user.request.ServiceRequestType;

@RestController
public class ServiceRequestServiceController extends BaseController {

	private static final Logger log = Logger.getLogger(ServiceRequestServiceController.class);

	@Autowired
	ServiceRequestDao serviceRequestDao;
	@Autowired
	StudentDao studentDAO;

	@RequestMapping(value = "/branch/{id}/service-request-type", produces = { "application/json" })
	public List<ServiceRequestType> getServiceRequestTypeByBranchId(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @PathVariable Integer id)
					throws AuthenticationException,
					SQLException {
		authenitcateRequest(accessTokenStr);
		return serviceRequestDao.getServiceRequestTypeByBranchId(id);
	}

	@RequestMapping(value = "/user/service-request", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseStatus(value = HttpStatus.CREATED)
	public void sendServiceRequest(@NotNull @RequestHeader(value = "token") String accessTokenStr, @Valid @RequestBody ServiceRequest serviceRequest)
					throws Exception {

		Identity identity = authenitcateRequest(accessTokenStr);
		log.debug("ServiceRequest request :: " + serviceRequest.toString());

		if ("PARENT".equalsIgnoreCase(identity.getDefaultRole())
						&& serviceRequest.getStudentId() != null
						&& studentDAO.canAccessDetails(identity.getEntityId(), serviceRequest.getStudentId())) {

			serviceRequest.setParentId(identity.getEntityId());
			serviceRequestDao.sendServiceRequestNotification(serviceRequest);

		} else if ("STUDENT".equalsIgnoreCase(identity.getDefaultRole())
						&& serviceRequest.getStudentId() != null
						&& identity.getEntityId().equals(serviceRequest.getStudentId())) {
			serviceRequestDao.sendServiceRequestNotification(serviceRequest);
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

	}

}
