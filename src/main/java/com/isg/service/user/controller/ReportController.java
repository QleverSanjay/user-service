package com.isg.service.user.controller;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.ReportDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.ExpenseChart;
import com.isg.service.user.model.FeeHeadChart;
import com.isg.service.user.model.StudentResult;
import com.isg.service.user.request.Student;

@RestController
@RequestMapping("/report")
public class ReportController extends BaseController {

	@Autowired
	ReportDao reportDao;

	private static final Logger log = Logger.getLogger(ReportController.class);

	@Autowired
	StudentDao studentDao;

	@RequestMapping(value = "/expense", method = RequestMethod.GET, produces = { "application/json" })
	public ExpenseChart getExpenseReport(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "month") int month, @NotNull @RequestParam(value = "year") int year,
					@RequestParam(value = "forUserId", required = false) Integer forEntityUserId)
					throws AuthenticationException, SQLException, AuthorisationException, ParseException {
		Identity identity = authenitcateRequest(accessTokenStr);

		return reportDao.getExpenseChart(identity.getUserId(), month, year, (forEntityUserId != null) ? forEntityUserId : 0);
	}

	@RequestMapping(value = "/result", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public List<StudentResult> getResults(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam Integer studentId) throws AuthenticationException, SQLException, AuthorisationException {
		Identity identity = authenitcateRequest(accessTokenStr);
		Integer userId = null;
		if ("PARENT".equalsIgnoreCase(identity.getDefaultRole()) && studentDao.canAccessDetails(identity.getEntityId(), studentId)) {
			// Do any processing if required.
		} else if ("STUDENT".equalsIgnoreCase(identity.getDefaultRole())) {
			studentId = identity.getEntityId();
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		List<StudentResult> studentResult = reportDao.getStudentResults(studentId);
		return studentResult;
	}

	@RequestMapping(value = "/download", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ResponseEntity<byte[]> downloadReport(@RequestParam Integer resultId) throws Exception {

		// Identity identity = authenitcateRequest(accessTokenStr);
		log.debug("resultId ::" + resultId);
		HashMap reportDetails = reportDao.getReportById(resultId);

		if (reportDetails != null) {
			String filename = reportDetails.keySet().iterator().next().toString();
			log.debug("filename ::" + filename);

			ByteArrayOutputStream bao = null;
			try {
				bao = (ByteArrayOutputStream) reportDetails.get(filename);

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setContentDispositionFormData(filename, filename + ".pdf");
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(bao.toByteArray(), headers, HttpStatus.OK);
				return response;
			} finally {
				if (bao != null) {
					bao.close();
				}
			}
		}
		return null;
	}

	@RequestMapping(value = "/fees/dues/student/list", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public List<Student> getStudentsDueForFeePayment(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@NotNull @RequestParam(value = "forFeeHeadId") Integer feeHeadId,
					@RequestParam(value = "forStandardId", required = false) Integer standardId) throws Exception {

		authenitcateRequest(accessTokenStr);

		List<Student> studentList = reportDao.getStudentsDueForFeePayment(feeHeadId, standardId);
		return studentList;
	}

	@RequestMapping(value = "/fees/dues/stats", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public FeeHeadChart getFeeHeadStats(@NotNull @RequestHeader(value = "token") String accessTokenStr,
					@RequestParam(value = "forFeeHeadId", required = false) Integer feeHeadId,
					@RequestParam(value = "forStandardId", required = false) Integer standardId) throws Exception {
		authenitcateRequest(accessTokenStr);

		FeeHeadChart feeHeadChart = reportDao.getFeeHeadStats(feeHeadId, standardId);
		return feeHeadChart;
	}
}
