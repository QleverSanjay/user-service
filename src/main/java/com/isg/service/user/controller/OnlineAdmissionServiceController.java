package com.isg.service.user.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.config.AppProperties;
import com.isg.service.identity.model.Identity;
import com.isg.service.user.dao.InstituteDao;
import com.isg.service.user.dao.OnlineAdmissionDao;
import com.isg.service.user.dao.PaymentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.factory.pdf.onlineadmission.PdfBuilder;
import com.isg.service.user.factory.pdf.onlineadmission.PdfFactory;
import com.isg.service.user.model.FileAttachment;
import com.isg.service.user.model.OnlineAdmissionCourse;
import com.isg.service.user.model.OnlineAdmissionCourseSubjectPreference;
import com.isg.service.user.model.OnlineAdmissionDocument;
import com.isg.service.user.model.OnlineAdmissionFormDetail;
import com.isg.service.user.model.OnlineAdmissionSeedProgramme;
import com.isg.service.user.model.OnlineAdmissionUrlConfiguration;
import com.isg.service.user.model.Payment;
import com.isg.service.user.model.QualificationExam;
import com.isg.service.user.model.University;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.model.pdf.PDFReceiptGenerator;
import com.isg.service.user.request.BranchConfiguration;
import com.isg.service.user.request.Institute;
import com.isg.service.user.request.OnlineAdmissionBasicProfileDetail;
import com.isg.service.user.request.OnlineAdmissionForm;
import com.isg.service.user.request.OnlineAdmissionLoginRequest;
import com.isg.service.user.response.OnlineAdmissionLoginResponse;
import com.isg.service.user.response.SaveOnlineAdmissionBasicProfileDetailResponse;
import com.isg.service.user.response.SaveOnlineAdmissionFormResponse;
import com.isg.service.user.response.UpdateOnlineAdmissionFormResponse;
import com.isg.service.user.service.OnlineAdmissionService;
import com.isg.service.user.util.WorkbookUtil;

@RestController
@MultipartConfig(fileSizeThreshold = 5120)
public class OnlineAdmissionServiceController extends BaseController {
	private static final Logger log = Logger
			.getLogger(OnlineAdmissionServiceController.class);

	@Autowired
	OnlineAdmissionDao onlineAdmissionDao;

	@Autowired
	OnlineAdmissionService onlineAdmissionService;

	@Autowired
	private AppProperties appProperties;

	@Autowired
	@Lazy
	private UserDao userDao;

	@Autowired
	InstituteDao instituteDao;

	@Autowired
	@Lazy
	PaymentDao paymentDao;

	@RequestMapping(value = { "/branch/{branch_id}/online/admission/course/list" }, produces = { "application/json" })
	public List<OnlineAdmissionCourse> getOnlineAdmissionCourses(
			@NotNull @RequestParam("category") String category,
			@NotNull @PathVariable("branch_id") Integer branchId) {
		return onlineAdmissionDao.getOnlineAdmissionCourses(branchId, category);
	}

	@RequestMapping(value = { "/online/admission/course/{course_id}/seed-programme/list" }, produces = { "application/json" })
	public List<OnlineAdmissionSeedProgramme> getOnlineAdmissionSeedProgramme(
			@NotNull @PathVariable("course_id") Integer courseId,
			@NotNull @RequestParam("category") String category) {
		return onlineAdmissionDao.getOnlineAdmissionSeedProgramme(courseId,
				category.equals("optional"));
	}

	@RequestMapping(value = { "/online/admission/course/{course_id}/subject-preference/list" }, produces = { "application/json" })
	public Map<String, List<OnlineAdmissionCourseSubjectPreference>> getOnlineAdmissionSubjectPreference(
			@NotNull @PathVariable("course_id") Integer courseId) {
		return onlineAdmissionDao.getOnlineAdmissionSubjectPreference(courseId);
	}

	@RequestMapping(value = { "/branch/{branch_id}/online/admission/qualifying-exam/list" }, produces = { "application/json" })
	public List<QualificationExam> getOnlineAdmissionQualifyingExam(
			@NotNull @PathVariable("branch_id") Integer branchId) {
		return onlineAdmissionDao.getOnlineAdmissionQualifyingExam(branchId);
	}

	@RequestMapping(value = { "/online/admission/verifyUnverifyAdmissionForm" }, produces = { "application/json" }, method = { org.springframework.web.bind.annotation.RequestMethod.PUT })
	public void verifyUnverifyAdmissionForm(
			@NotNull @RequestHeader("token") String token,
			@RequestBody Map<String, Object> dataMap) throws Exception {
		String applicationId = (String) dataMap.get("application_id");
		Boolean isMarkVerify = (Boolean) dataMap.get("is_mark_verify");
		if ((isMarkVerify == null) || (applicationId == null)) {
			throw new ApplicationException(
					"applicationId and isMarkVerify is required.");
		}

		Identity identity = authenitcateRequest(token);
		if ((!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole()))
				&& (!Identity.ROLES.SCHOOL_ADMIN.get().equals(
						identity.getDefaultRole()))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, only admin users allowed to perform this task.");
		}

		onlineAdmissionDao.verifyUnverifyAdmissionForm(applicationId,
						isMarkVerify.booleanValue(), (Integer) identity .getEntityDetails()
						.get(com.isg.service.identity.model.Identity.ENTITY_KEYS.BRANCH_ID));
	}

	@RequestMapping(value = { "/online/admission/university/list" }, produces = { "application/json" })
	public List<University> getOnlineAdmissionUniversity() {
		return onlineAdmissionDao.getOnlineAdmissionUniversity();
	}

	@RequestMapping(value = { "/online/admission/course/{course_id}/document/list" }, produces = { "application/json" })
	public List<OnlineAdmissionDocument> getOnlineAdmissionDocument(
			@NotNull @PathVariable("course_id") Integer courseId) {
		return onlineAdmissionDao.getOnlineAdmissionDocument(courseId);
	}

	@RequestMapping(value = { "/online/admission/url-configuration" }, produces = { "application/json" })
	public OnlineAdmissionUrlConfiguration getOnlineAdmissionDocumentUrlConfiguration(
			@NotNull @RequestParam("for_url_identifier") String urlIdentifier,
			@NotNull @RequestParam("category") String category) {
		return onlineAdmissionDao.getOnlineAdmissionDocumentUrlConfiguration(
				urlIdentifier, category);
	}

	@RequestMapping(value = { "/online/admission/single-step" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST }, 
			consumes = { "multipart/form-data" }, produces = { "application/json" })
	public SaveOnlineAdmissionFormResponse saveOnlineAdmissionSingleStep(
			HttpServletRequest request,
			@NotNull @RequestPart(value = "data", required = true) String onlineAdmissionFormStr,
			@RequestPart(value = "photo", required = false) MultipartFile photo,
			@RequestPart(value = "signature", required = false) MultipartFile signature,
			@RequestPart(value = "documents[]", required = false) List<MultipartFile> documents)
			throws IOException, AuthorisationException {
		try {
			FileAttachment photoFileAttachment = null;

			System.out.println("documents.......>>>>>>>>>>>" + documents.size());

			if ((photo != null) && (!photo.isEmpty())) {
				log.debug("photo.getName() >>" + photo.getName());
			}

			if ((photo != null) && (!photo.isEmpty())) {
				photoFileAttachment = new FileAttachment();
				String fileName = photo.getOriginalFilename();
				photoFileAttachment.setFileNameWithExtension(fileName);
				photoFileAttachment.setBytes(photo.getBytes());
				log.debug("photoFileAttachment >>"
						+ photoFileAttachment.getFileNameWithExtension());
			}

			FileAttachment signatureFileAttachment = null;

			if ((signature != null) && (!signature.isEmpty())) {
				log.debug("signature.getName() >>" + signature.getName());
			}

			if ((signature != null) && (!signature.isEmpty())) {
				signatureFileAttachment = new FileAttachment();
				String fileName = signature.getOriginalFilename();
				signatureFileAttachment.setFileNameWithExtension(fileName);
				signatureFileAttachment.setBytes(signature.getBytes());
			}

			return onlineAdmissionDao.saveOnlineAdmissionSingleStep(
					onlineAdmissionFormStr, photoFileAttachment,
					signatureFileAttachment, documents);

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
			throw e;
		}
	}

	@RequestMapping(value = { "/online/admission/basic/profile" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST }, produces = { "application/json" })
	public SaveOnlineAdmissionBasicProfileDetailResponse saveOnlineAdmissionBasicProfileDetail(
			@Valid @RequestBody OnlineAdmissionBasicProfileDetail onlineAdmissionBasicProfileDetail) {
		return onlineAdmissionDao
				.saveOnlineAdmissionBasicProfileDetail(onlineAdmissionBasicProfileDetail);
	}

	@RequestMapping(value = { "/online/admission/form" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST }, produces = { "application/json" })
	public SaveOnlineAdmissionFormResponse saveOnlineAdmissionForm(
			@Valid @RequestBody OnlineAdmissionForm onlineAdmissionForm)
			throws AuthorisationException, IOException {
		return onlineAdmissionDao.saveOnlineAdmissionForm(onlineAdmissionForm);
	}

	@RequestMapping(value = { "/online/admission/form-with-attachment" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST }, consumes = { "multipart/form-data" }, produces = { "application/json" })
	public SaveOnlineAdmissionFormResponse saveOnlineAdmissionFormWithAttachment(
			@NotNull @RequestPart(value = "data", required = true) String onlineAdmissionFormStr,
			@RequestPart(value = "photo", required = false) MultipartFile photo,
			@RequestPart(value = "signature", required = false) MultipartFile signature)
			throws AuthorisationException, IOException {
		log.debug("onlineAdmissionFormStr >>" + onlineAdmissionFormStr);
		log.debug(Boolean.valueOf("photo >>" + photo == null));
		log.debug(Boolean.valueOf("signature >>" + signature == null));

		if ((photo != null) && (!photo.isEmpty())) {
			log.debug("photo.getName() >>" + photo.getName());
		}

		if ((signature != null) && (!signature.isEmpty())) {
			log.debug("signature.getName() >>" + signature.getName());
			System.out.println("signature.getName() >>" + signature.getName());
		}

		OnlineAdmissionForm onlineAdmissionForm = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			onlineAdmissionForm = (OnlineAdmissionForm) mapper.readValue(
					onlineAdmissionFormStr, OnlineAdmissionForm.class);

			log.debug("onlineAdmissionForm :::" + onlineAdmissionForm);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ApplicationException(
					"Invalid request submitted. Please check request JSON.");
		}

		FileAttachment photoFileAttachment = null;

		if ((photo != null) && (!photo.isEmpty())) {
			photoFileAttachment = new FileAttachment();
			String fileName = photo.getOriginalFilename();
			photoFileAttachment.setFileNameWithExtension(fileName);
			photoFileAttachment.setBytes(photo.getBytes());
		}

		FileAttachment signatureFileAttachment = null;

		if ((signature != null) && (!signature.isEmpty())) {
			signatureFileAttachment = new FileAttachment();
			String fileName = signature.getOriginalFilename();
			signatureFileAttachment.setFileNameWithExtension(fileName);
			signatureFileAttachment.setBytes(signature.getBytes());
		}

		if ((photoFileAttachment == null) || (signatureFileAttachment == null)) {
			throw new ApplicationException(
					"Invalid request submitted. Photo / signature file is missing.");
		}

		return onlineAdmissionDao.saveOnlineAdmissionFormWithAttachment(
				onlineAdmissionForm, photoFileAttachment,
				signatureFileAttachment, true, null);
	}

	@RequestMapping(value = { "/online/admission/login" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST }, produces = { "application/json" })
	public OnlineAdmissionLoginResponse getOnlineAdmissionForm(
			@Valid @RequestBody OnlineAdmissionLoginRequest onlineAdmissionLoginRequest)
			throws AuthenticationException {
		return onlineAdmissionDao.login(onlineAdmissionLoginRequest);
	}

	@RequestMapping(value = { "/online/admission/getFormToEditByAdmin" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET }, produces = { "application/json" })
	public OnlineAdmissionLoginResponse getFormToEditByAdmin(
			@NotNull @RequestHeader("token") String accessTokenStr,
			@NotNull @RequestParam("applicationId") String applicationId)
			throws Exception {
		Identity identity = authenitcateRequest(accessTokenStr);
		if ((!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole()))
				&& (!Identity.ROLES.SCHOOL_ADMIN.get().equals(
						identity.getDefaultRole()))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, only admin users allowed to perform this task.");
		}

		OnlineAdmissionLoginResponse response = onlineAdmissionDao
				.getFormDetailsForAdmin(applicationId);

		if ((Identity.ROLES.SCHOOL_ADMIN.get()
				.equals(identity.getDefaultRole()))
				&& (!userDao.canUserAccessSpecifiedBranch(identity.getUserId(),
						response.getOnlineAdmissionFormDetail().getBranch()
								.getId()))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, invalid branch.");
		}

		return response;
	}

	@RequestMapping(value = { "/online/admission/admin-update-form-with-attachment" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST }, consumes = { "multipart/form-data" }, produces = { "application/json" })
	public UpdateOnlineAdmissionFormResponse updateOnlineAdmissionFormWithAttachmentByAdmin(
			@NotNull @RequestHeader("token") String token,
			@NotNull @RequestPart(value = "data", required = false) String onlineAdmissionFormStr,
			@RequestPart(value = "photo", required = false) MultipartFile photo,
			@RequestPart(value = "signature", required = false) MultipartFile signature)
			throws AuthorisationException, Exception {
		Identity identity = authenitcateRequest(token);
		if ((!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole()))
				&& (!Identity.ROLES.SCHOOL_ADMIN.get().equals(
						identity.getDefaultRole()))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, only admin users allowed to perform this task.");
		}

		log.debug("onlineAdmissionFormStr >>" + onlineAdmissionFormStr);
		log.debug(Boolean.valueOf("photo is null >>" + photo == null));
		log.debug(Boolean
				.valueOf("signature image is null>>" + signature == null));

		if ((photo != null) && (!photo.isEmpty())) {
			log.debug("photo.getName() >>" + photo.getName());
		}

		if ((signature != null) && (!signature.isEmpty())) {
			log.debug("signature.getName() >>" + signature.getName());
		}

		OnlineAdmissionForm onlineAdmissionForm = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			onlineAdmissionForm = (OnlineAdmissionForm) mapper.readValue(
					onlineAdmissionFormStr, OnlineAdmissionForm.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ApplicationException(
					"Invalid request submitted. Please check request JSON.");
		}

		OnlineAdmissionLoginResponse response = onlineAdmissionDao
				.getFormDetailsForAdmin(onlineAdmissionForm.getApplicantId());

		if ((Identity.ROLES.SCHOOL_ADMIN.get()
				.equals(identity.getDefaultRole()))
				&& (!userDao.canUserAccessSpecifiedBranch(identity.getUserId(),
						response.getOnlineAdmissionFormDetail().getBranch()
								.getId()))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, invalid branch.");
		}

		FileAttachment photoFileAttachment = null;

		if ((photo != null) && (!photo.isEmpty())) {
			photoFileAttachment = new FileAttachment();
			String fileName = photo.getOriginalFilename();
			photoFileAttachment.setFileNameWithExtension(fileName);
			photoFileAttachment.setBytes(photo.getBytes());
		}

		FileAttachment signatureFileAttachment = null;

		if ((signature != null) && (!signature.isEmpty())) {
			signatureFileAttachment = new FileAttachment();
			String fileName = signature.getOriginalFilename();
			signatureFileAttachment.setFileNameWithExtension(fileName);
			signatureFileAttachment.setBytes(signature.getBytes());
		}
		onlineAdmissionForm.setTempAccessId(response.getToken());
		return onlineAdmissionDao.updateOnlineAdmissionForm(
				onlineAdmissionForm, photoFileAttachment,
				signatureFileAttachment);
	}

	@RequestMapping(value = { "/online/admission/update-form-with-attachment" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST }, consumes = { "multipart/form-data" }, produces = { "application/json" })
	public UpdateOnlineAdmissionFormResponse updateOnlineAdmissionFormWithAttachment(
			@NotNull @RequestPart(value = "data", required = false) String onlineAdmissionFormStr,
			@RequestPart(value = "photo", required = false) MultipartFile photo,
			@RequestPart(value = "signature", required = false) MultipartFile signature)
			throws AuthorisationException, IOException {
		String name = photo.getName();

		if (!Pattern.matches("^.+[.]pdf", name)) {
		}

		log.debug("onlineAdmissionFormStr >>" + onlineAdmissionFormStr);
		log.debug(Boolean.valueOf("photo is null >>" + photo == null));
		log.debug(Boolean
				.valueOf("signature image is null>>" + signature == null));

		if ((photo != null) && (!photo.isEmpty())) {
			log.debug("photo.getName() >>" + photo.getName());
		}

		if ((signature != null) && (!signature.isEmpty())) {
			log.debug("signature.getName() >>" + signature.getName());
		}

		OnlineAdmissionForm onlineAdmissionForm = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			onlineAdmissionForm = (OnlineAdmissionForm) mapper.readValue(
					onlineAdmissionFormStr, OnlineAdmissionForm.class);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ApplicationException(
					"Invalid request submitted. Please check request JSON.");
		}

		FileAttachment photoFileAttachment = null;

		if ((photo != null) && (!photo.isEmpty())) {
			photoFileAttachment = new FileAttachment();
			String fileName = photo.getOriginalFilename();
			photoFileAttachment.setFileNameWithExtension(fileName);
			photoFileAttachment.setBytes(photo.getBytes());
		}

		FileAttachment signatureFileAttachment = null;

		if ((signature != null) && (!signature.isEmpty())) {
			signatureFileAttachment = new FileAttachment();
			String fileName = signature.getOriginalFilename();
			signatureFileAttachment.setFileNameWithExtension(fileName);
			signatureFileAttachment.setBytes(signature.getBytes());
		}

		return onlineAdmissionDao.updateOnlineAdmissionForm(
				onlineAdmissionForm, photoFileAttachment,
				signatureFileAttachment);
	}

	@RequestMapping(value = { "/online/admission/form/print" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET }, produces = { "application/json" })
	public ResponseEntity<byte[]> printOnlineAdmissionForm(
			@NotNull @RequestParam("token") String token)
			throws AuthorisationException {
		ResponseEntity<byte[]> response = null;
		try {
			OnlineAdmissionFormDetail onlineAdmissionFormDetail = onlineAdmissionDao
					.retrieveOnlineFormDetail(token);

			byte[] data = Files.readAllBytes(new File("/var/www/html"
					+ onlineAdmissionFormDetail.getPdfFilePath()).toPath());

			String filename = "QFIX-ONLINE-REGISTRATION-FORM";
			log.debug("filename ::" + filename);

			if (data != null) {
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType
						.parseMediaType("application/pdf"));

				headers.setContentDispositionFormData(filename, filename
						+ ".pdf");

				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				response = new ResponseEntity(data, headers, HttpStatus.OK);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}

		return response;
	}

	@RequestMapping(value = { "/online/admission/report/export" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET }, produces = { "application/json" })
	public ResponseEntity<byte[]> exportAdmissionReport(
			@NotNull @RequestParam("token") String accessTokenStr,
			@NotNull @RequestParam("branch_id") Integer branchId,
			@NotNull @RequestParam("course_id") Integer courseId,
			@NotNull @RequestParam("academic_year_id") Integer academicYearId)
			throws AuthorisationException, AuthenticationException,
			SQLException {
		Identity identity = authenitcateRequest(accessTokenStr);
		if ((!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole()))
				&& (!Identity.ROLES.SCHOOL_ADMIN.get().equals(
						identity.getDefaultRole()))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, only admin users allowed to perform this task.");
		}
		if ((Identity.ROLES.SCHOOL_ADMIN.get()
				.equals(identity.getDefaultRole()))
				&& (!userDao.canUserAccessSpecifiedBranch(identity.getUserId(),
						branchId))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, invalid branch.");
		}

		ResponseEntity<byte[]> response = null;
		try {
			List<OnlineAdmission> onlineAdmissionList = onlineAdmissionDao
					.exportAdmissionReport(branchId, courseId, academicYearId,
							true);

			String exportTemplateFactoryName = onlineAdmissionDao
					.getExportTemplateFactoryName(branchId, courseId);

			WorkbookUtil workbookUtil = new WorkbookUtil();
			byte[] data = workbookUtil.getOnlineAdmissionReport(
					onlineAdmissionList, exportTemplateFactoryName);

			String filename = "ONLINE-REGISTRATION-EXPORT";
			log.debug("filename ::" + filename);

			if (data != null) {
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(new MediaType("application",
						"vnd.openxmlformats-officedocument.spreadsheetml.sheet"));

				headers.setContentDispositionFormData("Content-Disposition",
						filename + ".xlsx");

				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				headers.setContentLength(data.length);
				response = new ResponseEntity(data, headers, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}

		return response;
	}

	@RequestMapping(value = { "/online/admission/attachments/{onlineAdmissionId}" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public ResponseEntity downloadStudentAdmissionFormAttachments(
			@PathVariable("onlineAdmissionId") Integer onlineAdmissionId,
			@RequestParam("applicantName") String applicantName,
			HttpServletResponse servletResponse) throws IOException {
		byte[] bytes;
		try {
			bytes = onlineAdmissionService
					.downloadStudentAdmissionFormAttachments(onlineAdmissionId);
		} catch (IOException e) {
			return ResponseEntity.badRequest().body(
					"Cannot procced with download right now");
		}

		if ((bytes != null) && (bytes.length > 0)) {
			String fileName = applicantName.concat("-attachments.zip");

			servletResponse.setContentLength(bytes.length);
			servletResponse.setContentType("application/zip");
			servletResponse.setHeader("Content-Disposition",
					"attachment; filename=".concat(fileName));

			FileCopyUtils.copy(bytes, servletResponse.getOutputStream());
			servletResponse.flushBuffer();
		}
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = { "/online/admission/report/search" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET }, produces = { "application/json" })
	public List<OnlineAdmission> searchAdmissionReport(
			@NotNull @RequestParam("token") String accessTokenStr,
			@NotNull @RequestParam("branch_id") Integer branchId,
			@NotNull @RequestParam("course_id") Integer courseId,
			@NotNull @RequestParam("academic_year_id") Integer academicYearId)
			throws AuthorisationException, AuthenticationException,
			SQLException {
		Identity identity = authenitcateRequest(accessTokenStr);
		if ((!Identity.ROLES.ADMIN.get().equals(identity.getDefaultRole()))
				&& (!Identity.ROLES.SCHOOL_ADMIN.get().equals(
						identity.getDefaultRole()))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, only admin users allowed to perform this task.");
		}
		if ((Identity.ROLES.SCHOOL_ADMIN.get()
				.equals(identity.getDefaultRole()))
				&& (!userDao.canUserAccessSpecifiedBranch(identity.getUserId(),
						branchId))) {

			throw new AuthorisationException(
					"Function not allowed for this profile, invalid branch.");
		}

		try {
			List<OnlineAdmission> onlineAdmissionList = onlineAdmissionDao
					.exportAdmissionReport(branchId, courseId, academicYearId,
							false);

			List<OnlineAdmission> localList1 = onlineAdmissionList;
			return localList1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}

		return null;
	}

	@RequestMapping(value = { "/online/admission/payment/receipt" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET }, produces = { "application/json" })
	@ResponseBody
	public ResponseEntity<byte[]> getPaymentReceipt(
			@NotNull @RequestHeader("token") String accessTokenStr)
			throws AuthenticationException, SQLException, IOException,
			AuthorisationException {
		OnlineAdmissionFormDetail onlineAdmissionFormDetail = onlineAdmissionDao
				.retrieveOnlineFormDetail(accessTokenStr);

		Payment paymentObj = onlineAdmissionDao
				.getPaymentDetailsWithAuditInformation(onlineAdmissionFormDetail
						.getApplicantId());

		paymentObj
				.setForPerson(onlineAdmissionFormDetail.getFirstName()
						+ " "
						+ (!StringUtils.isEmpty(onlineAdmissionFormDetail
								.getSurname()) ? onlineAdmissionFormDetail
								.getSurname() : ""));

		if (paymentObj != null) {
			ResponseEntity<byte[]> response = null;
			ByteArrayOutputStream bao = null;
			try {
				BranchConfiguration branchConfiguration = paymentDao
						.getBranchConfiguration(paymentObj.getBranchId());
				String filename = "QFIX-PAYMENT-RECEIPT";
				log.debug("filename ::" + filename);
				PDFReceiptGenerator pdfReceiptGenerator = new PDFReceiptGenerator();

				String logoUrl = null;

				if (onlineAdmissionFormDetail.getBranch() != null) {
					Institute institue = instituteDao
							.getById(onlineAdmissionFormDetail.getBranch()
									.getInstituteId());

					logoUrl = institue.getLogo_url();
				}

				log.debug("logoUrl ::" + logoUrl);
				bao = pdfReceiptGenerator.generatePaymentReceipt(paymentObj,
						onlineAdmissionFormDetail.getBranch(), logoUrl, null);

				if (bao != null) {
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType
							.parseMediaType("application/pdf"));

					headers.setContentDispositionFormData(filename, filename
							+ ".pdf");

					headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
					byte[] bytes = bao.toByteArray();
					response = new ResponseEntity(bytes, headers, HttpStatus.OK);

					return response;
				}
			} finally {
				if (bao != null) {
					bao.close();
				}
			}
		}
		return null;
	}

	@RequestMapping(value = { "/online/admission/form" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	@ResponseBody
	public ResponseEntity getSubmittedForm(
			@NotNull @RequestParam("token") String accessTokenStr,
			HttpServletResponse servletResponse) throws AuthorisationException,
			IOException {
		OnlineAdmissionFormDetail onlineAdmissionFormDetail = onlineAdmissionDao
				.retrieveOnlineFormDetail(accessTokenStr);

		ObjectMapper mapper = new ObjectMapper();

		OnlineAdmission onlineAdmission = (OnlineAdmission) mapper.readValue(
				onlineAdmissionFormDetail.getFormPayload().replaceAll("\\\\",
						""), OnlineAdmission.class);

		String templateFactoryName = onlineAdmissionDao
				.getExportTemplateFactoryName(onlineAdmissionFormDetail
						.getBranch().getId(), Integer.valueOf(onlineAdmission
						.getCourseDetails().getCourse().getId().intValue()));

		PdfBuilder pdfBuilder = PdfFactory
				.getPdfBuilderInstance(templateFactoryName);
		Institute institue = instituteDao.getById(onlineAdmissionFormDetail
				.getBranch().getInstituteId());

		System.out.println("onlineAdmissionFormDetail.getPdfFilePath() :::::::>>> "
						+ onlineAdmissionFormDetail.getPdfFilePath());
		String admissionForPdfPath = null;
		if (StringUtils.isEmpty(onlineAdmissionFormDetail.getPdfFilePath())) {
			new RandomStringUtils();
			admissionForPdfPath = "/var/www/html/images/Qfix/documents/External/r12r12gistra123ion/"
					+ onlineAdmissionFormDetail.getBranch().getId()
					+ "/"
					+ RandomStringUtils.random(20, true, true) + ".pdf";
		} else {
			 admissionForPdfPath = appProperties.getFileProps().getBaseSystemPath().concat(onlineAdmissionFormDetail.getPdfFilePath());
			//admissionForPdfPath = "/var/www/html"+ onlineAdmissionFormDetail.getPdfFilePath();
			String tempAdmissionForPdfPath = admissionForPdfPath.replace("/var/www/html", "");
			onlineAdmissionDao.updateAdmissionFormPath(
					onlineAdmissionFormDetail.getApplicantId(),
					tempAdmissionForPdfPath);
		}
		File document = new File(admissionForPdfPath);

		if (!document.exists()) {
			System.out.println("File Not Exist Creating New.....>>>>>>>>>>>>>>>");
			pdfBuilder.createPdf(onlineAdmission, admissionForPdfPath, null, null, "/var/www/html" + institue.getLogo_url());

			document = new File(admissionForPdfPath);
		}

		byte[] bytes = Files.readAllBytes(document.toPath());

		servletResponse.setContentLength(bytes.length);
		servletResponse.setContentType("application/pdf");
		servletResponse.setHeader("Content-Disposition",
				"attachment; filename=".concat("admission-form.pdf"));

		FileCopyUtils.copy(bytes, servletResponse.getOutputStream());
		servletResponse.flushBuffer();

		return ResponseEntity.ok().build();
	}

	public static void main(String a[]) throws AuthorisationException,
			IOException {
		new OnlineAdmissionServiceController().getSubmittedForm("", null);
	}
}
