package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.util.StringUtils;

import com.isg.service.user.model.onlineadmission.OnlineAdmission;

public class ParentsAndGardianDetails {

	/**
	 * @param args
	 */
	PDDocument doc = null;
	PDPage page = null;
	public int RECTANGLE_WIDTH = 535;
	public int RECTANGLE_HEIGHT = 12;
	public float X0 = 40f;
	public float marginX = 5f;
	public float CONTENT_FONT_SIZE = 9;
	float previousContentY;
	public float ROW_WIDTH = 535;

	/************************ Drawing the data for Parents And Details **********/

	public Object[] addParentsAndGuardianDetails(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission)
					throws IOException {

		ApplicationContent applicationContent = new ApplicationContent();

		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];

		previousContentY = (float) previousContenyYAndContentStream[0];

		contentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		//System.out.println("Inside of the Parents and Guardian Details");

		/******************* to draw Parents and Guardian details text ******************/
		contentStream.fillRect(X0 + marginX, previousContentY -= 18, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY += 3);
		contentStream.drawString("Parents And Guardian Details :");
		contentStream.endText();
		contentStream.drawLine(X0 + marginX, previousContentY -= 3, X0 + marginX + ROW_WIDTH, previousContentY);

		/****************** to draw content of Parents and Guardian Details ************/

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("1.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Father's Occupation");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getFatherDetails() == null) ? ":  "
						: (onlineAdmission.getFatherDetails().getEmploymentDetails() == null) ? ":  " : (onlineAdmission.getFatherDetails()
										.getEmploymentDetails().getOccupation() == null) ? ":  " : ":  "
										+ onlineAdmission.getFatherDetails().getEmploymentDetails().getOccupation().toUpperCase());
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("2.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Father's Orgnization");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getFatherDetails() == null) ? ":  "
						: (onlineAdmission.getFatherDetails().getEmploymentDetails() == null) ? ":  " : (onlineAdmission.getFatherDetails()
										.getEmploymentDetails().getEmployerName() == null) ? ":  " : ":  "
										+ onlineAdmission.getFatherDetails().getEmploymentDetails().getEmployerName().toUpperCase());
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("3.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Father's Designation");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getFatherDetails() == null) ? ":  "
						: (onlineAdmission.getFatherDetails().getEmploymentDetails() == null) ? ":  " : (onlineAdmission.getFatherDetails()
										.getEmploymentDetails().getDesignation() == null) ? ":  " : ":  "
										+ onlineAdmission.getFatherDetails().getEmploymentDetails().getDesignation().toUpperCase());
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("4.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Mother's Occupation");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getMotherDetails() == null) ? ":  "
						: (onlineAdmission.getMotherDetails().getEmploymentDetails() == null) ? ":  " : (onlineAdmission.getMotherDetails()
										.getEmploymentDetails().getOccupation() == null) ? ":  " : ":  "
										+ onlineAdmission.getMotherDetails().getEmploymentDetails().getOccupation().toUpperCase());
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("5.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Mother's Orgnization");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getMotherDetails() == null) ? ":  "
						: (onlineAdmission.getMotherDetails().getEmploymentDetails() == null) ? ":  " : (onlineAdmission.getMotherDetails()
										.getEmploymentDetails().getEmployerName() == null) ? ":  " : ":  "
										+ onlineAdmission.getMotherDetails().getEmploymentDetails().getEmployerName().toUpperCase());
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("6.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Mother's Designation");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getMotherDetails() == null) ? ":  "
						: (onlineAdmission.getMotherDetails().getEmploymentDetails() == null) ? ":  " : (onlineAdmission.getMotherDetails()
										.getEmploymentDetails().getDesignation() == null) ? ":  " : ":  "
										+ onlineAdmission.getMotherDetails().getEmploymentDetails().getDesignation().toUpperCase());
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("7.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Parent Anual Income");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getFatherDetails() == null) ? ":  "
						: (onlineAdmission.getFatherDetails().getEmploymentDetails() == null) ? ":  " : (onlineAdmission.getFatherDetails()
										.getEmploymentDetails().getAnnualIncome() == null) ? ":  " : ":  "
										+ onlineAdmission.getFatherDetails().getEmploymentDetails().getAnnualIncome().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("8.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Parent Email Address");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		if (onlineAdmission.getGuardianDetails() != null) {
			if (StringUtils.isEmpty(onlineAdmission.getGuardianDetails().getEmail())) {
				if (onlineAdmission.getFatherDetails() != null) {
					contentStream.drawString(":  " + onlineAdmission.getFatherDetails().getEmail());
				}
				else if (onlineAdmission.getMotherDetails() != null) {
					contentStream.drawString(":  " + onlineAdmission.getMotherDetails().getEmail());
				}
				else {
					contentStream.drawString(":  " + onlineAdmission.getGuardianDetails().getEmail());
				}
			}
			else {
				contentStream.drawString(":  " + onlineAdmission.getGuardianDetails().getEmail());
			}
		}
		else {
			contentStream.drawString(":  ");
		}
		//contentStream.drawString((onlineAdmission.getGuardianDetails() != null ) ? ( StringUtils.isEmpty( (onlineAdmission.getGuardianDetails().getEmail())) ? (onlineAdmission.getFatherDetails() == null) ) ? ((onlineAdmission.getMotherDetails() == null ) ? ":  " : ":  " + (onlineAdmission.getMotherDetails().getEmail())) : ":  " + (onlineAdmission.getFatherDetails().getEmail()) : ":  " );
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("9.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Staying With");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		if (onlineAdmission.getGuardianDetails() != null) {
			if (StringUtils.isEmpty(onlineAdmission.getGuardianDetails().getCandidaterelation())) {
				if (onlineAdmission.getFatherDetails() != null) {
					contentStream.drawString(":  " + onlineAdmission.getFatherDetails().getName().getFirstname().toUpperCase());
				}
				else if (onlineAdmission.getMotherDetails() != null) {
					contentStream.drawString(":  " + onlineAdmission.getMotherDetails().getName().getFirstname().toUpperCase());
				}
				else {
					contentStream.drawString(":  " + onlineAdmission.getGuardianDetails().getCandidaterelation().toUpperCase());
				}
			}
			else {
				contentStream.drawString(":  " + onlineAdmission.getGuardianDetails().getCandidaterelation().toUpperCase());
			}
		}
		else {
			contentStream.drawString(":  ");
		}
		//contentStream.drawString( (String) ((onlineAdmission.getGuardianDetails() == null) ? ((onlineAdmission.getFatherDetails() == null) ? ( (onlineAdmission.getMotherDetails() == null ) ? ":  " : onlineAdmission.getMotherDetails().getName().getFirstname().toUpperCase()) :(onlineAdmission.getFatherDetails().getName().getFirstname().toUpperCase())) : ":  " +onlineAdmission.getGuardianDetails().getCandidaterelation().toUpperCase()));
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("10.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Guardians Name.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		if (onlineAdmission.getGuardianDetails() != null) {
			contentStream.drawString((onlineAdmission.getGuardianDetails().getName() == null) ? ":  " : (onlineAdmission.getGuardianDetails().getName()
							.getFirstname() == null) ? ":  " : ":  "
							+ onlineAdmission.getGuardianDetails().getName().getFirstname().toUpperCase());
		} else {
			contentStream.drawString(":  ");
		}

		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("11.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Relation with Applicant");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		if (onlineAdmission.getGuardianDetails() != null) {
			contentStream.drawString((onlineAdmission.getGuardianDetails().getCandidaterelation() == null) ? ":  " : ":  "
							+ onlineAdmission.getGuardianDetails().getCandidaterelation().toUpperCase());
		} else {
			contentStream.drawString(":  ");
		}

		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("12.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Native Address");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? ":  "
						: (onlineAdmission.getCandidateDetails().getNativePlaceAddress() == null) ? ":  " : ":  "
										+ onlineAdmission.getCandidateDetails().getNativePlaceAddress().toUpperCase());
		contentStream.endText();

		/************** Code here to check which method to call depend on dc or jc ********************/

		previousContenyYAndContentStream[0] = previousContentY;
		previousContenyYAndContentStream[1] = contentStream;
		if (onlineAdmission.getAcademicDetails() != null) {
			if (onlineAdmission.getCourseDetails().getCourse().getId() == 7) {
				previousContenyYAndContentStream = addSubjectPrefrences(doc, previousContenyYAndContentStream, onlineAdmission);
			}
		}
		previousContenyYAndContentStream = addSeedProgramme(doc, previousContenyYAndContentStream, onlineAdmission);
		return previousContenyYAndContentStream;
	}

	/********************* To draw Subject Preferences (Only if admission is of FYJC) ***************/
	public Object[] addSubjectPrefrences(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission)
					throws IOException {

		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];

		previousContentY = (float) previousContenyYAndContentStream[0];
		ApplicationContent applicationContent = new ApplicationContent();
		contentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		//System.out.println("Inside of the Subject Preferences Details");

		//******************* to draw Subject Prefrences details text******************//*
		contentStream.fillRect(X0 + marginX, previousContentY -= 20, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY += 3);
		contentStream.drawString("Subject Preferences :");
		contentStream.endText();
		contentStream.drawLine(X0 + marginX, previousContentY -= 3, X0 + marginX + ROW_WIDTH, previousContentY);

		//******************to draw content of Subject Prefrences Details only if application is of the fyjc************//*

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("1.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Preference 1");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCourseDetails() == null) ? ":  "
						: (onlineAdmission.getCourseDetails().getSubjectPreference() == null) ? ":  " : (onlineAdmission.getCourseDetails()
										.getSubjectPreference().getPreference1() == null) ? ":  " : (onlineAdmission.getCourseDetails().getSubjectPreference()
										.getPreference1().getName() == null) ? ":  " : ":  "
										+ (onlineAdmission.getCourseDetails().getSubjectPreference().getPreference1().getName().toUpperCase()));
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("2.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Preference 2");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCourseDetails() == null) ? ":  "
						: (onlineAdmission.getCourseDetails().getSubjectPreference() == null) ? ":  " : (onlineAdmission.getCourseDetails()
										.getSubjectPreference().getPreference2() == null) ? ":  " : (onlineAdmission.getCourseDetails().getSubjectPreference()
										.getPreference2().getName() == null) ? ":  " : ":  "
										+ (onlineAdmission.getCourseDetails().getSubjectPreference().getPreference2().getName().toUpperCase()));
		contentStream.endText();

		previousContenyYAndContentStream[0] = previousContentY;
		previousContenyYAndContentStream[1] = contentStream;
		return previousContenyYAndContentStream;
	}

	/********************* To draw Seed Programme (Only if admission is of DC) ***************/
	public Object[] addSeedProgramme(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission) throws IOException {

		ApplicationContent applicationContent = new ApplicationContent();

		//System.out.println("Inside of the Seed Programme Details");

		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];

		float previousContentY = (float) previousContenyYAndContentStream[0];
		contentStream.setNonStrokingColor(Color.LIGHT_GRAY);

		//******************* to draw Seed Programme details text******************
		/*contentStream.fillRect(X0 + marginX, previousContentY -= 20, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY += 3);
		contentStream.drawString("Seed Programme :");
		contentStream.endText();
		contentStream.drawLine(X0 + marginX, previousContentY -= 3, X0 + marginX + ROW_WIDTH, previousContentY);

		//******************to draw content of Seed Programme Details only if application is of the fyjc************

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("1.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Compulsory Subject");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCourseDetails() == null) ? ":  " : (onlineAdmission.getCourseDetails().getSeedProgramme() == null) ? ":  "
						: (onlineAdmission.getCourseDetails().getSeedProgramme().getCompulsorySubject() == null) ? ":  " : (onlineAdmission.getCourseDetails()
										.getSeedProgramme().getCompulsorySubject().getName() == null) ? ":  " : ":  "
										+ (onlineAdmission.getCourseDetails().getSeedProgramme().getCompulsorySubject().getName().toUpperCase()));
		contentStream.endText();

		contentStream = applicationContent.checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("2.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Optional Subject");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCourseDetails() == null) ? ":  " : (onlineAdmission.getCourseDetails().getSeedProgramme() == null) ? ":  "
						: (onlineAdmission.getCourseDetails().getSeedProgramme().getOptionalSubject() == null) ? ":  " : (onlineAdmission.getCourseDetails()
										.getSeedProgramme().getOptionalSubject().getName() == null) ? ":  " : ":  "
										+ (onlineAdmission.getCourseDetails().getSeedProgramme().getOptionalSubject().getName().toUpperCase()));
		contentStream.endText();*/

		previousContenyYAndContentStream[0] = previousContentY;
		previousContenyYAndContentStream[1] = contentStream;
		return previousContenyYAndContentStream;

	}

}
