package com.isg.service.user.factory.validator.onlineadmission;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.onlineadmission.Course;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.response.OnlineAdmissionConfiguration;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class DalmiaValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public void validate(OnlineAdmission onlineAdmission, OnlineAdmissionConfiguration admissionConfiguration) throws ApplicationException {
		String seatNo = onlineAdmission.getQualifyingExamDetails().getExamSeatNo();
		Course course = onlineAdmission.getCourseDetails().getCourse();
		String uniqueIdentifier = "course-id=" + course.getId() + ", academicyear="
						+ onlineAdmission.getAcademicYear() + ", seat-no="
						+ seatNo;

		onlineAdmission.setUniqueIdentifier(uniqueIdentifier);

		String sql = "select count(*) from online_admission where unique_identifier = '" + uniqueIdentifier + "'";
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			throw new ApplicationException("Dear applicant You have already submitted form for this course("
							+ course.getName() + ") and seat number(" + seatNo + ") please " +
							"login with your credentials to view the earlier submitted form.");
		}
	}
}