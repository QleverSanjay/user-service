package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.request.Branch;

public class TAndCFinalPage {

	private final int HEADER_SPLIT_POSITION = 45;
	float Y0 = 0;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 45;
	private final float HEADER_FONT_SIZE = 14;
	float previousContentY = 650;
	private final int X0 = 40;
	private final float CONTENT_FONT_SIZE = 12;
	Map<String, Float> headerCenterXPosition = new HashMap<String, Float>();
	public void termsAndConditionFinalPage(PDDocument doc, OnlineAdmission onlineAdmission, String branchLogo) throws COSVisitorException, IOException {
		PDPage page = new PDPage();
		doc.addPage(page);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Branch branch = onlineAdmission.getBranch();
		Date date = new Date();
		Map<String, Float> splitedTextListWithXPosition = new HashMap<String, Float>();

		String headerTextCollegeName = "PRAHLADRAI DALMIA LIONS COLLEGE OF COMMERCE & ECONOMICS  ASDJLASJDL ASDJLASDJSAJ ";
		String headerTextAddress = "Sunder Nagar, S. V. Raod, Malad (W) Mumbai - 400 064";
		String collegeName = "LIONS CLUB OF MALAD-BORIVALI COLLEGE CHARITY TRUST "
						+ "Prahladrai Dalmia Lions College Campus"
						+ "Student Enrichment And Employment Development Programme"
						+ "(SEED)";

		String paragraph1 = "I hereby agree, if admitted to abide by the rules and regulations at present in force, or that"
						+ " may hereafter be made for the administration of the college and I undertake that so long as";
		String paragraph2 = "I am the student of the college, I will do nothing either inside or outside the college, that"
						+ " will interfere with its orderly working, discipline and anti-ragging policy.";
		String paragraph3 = "If my admission is cancelled as per the rules and regulations of the college, I shall not have"
						+ " any grievance whatsoever. I hereby solemnly over and undertake that I will not indulge in"
						+ " any behaviour or act may be constituted as misconduct which includes;";
		String paragraph4 = "1) Criminal activity with support of outsiders with criminals and anti-social elements of"
						+ " society.";
		String paragraph5 = "2) Violation of Information Technology Act, cyber offences like creating /participating in"
						+ " websites/ blogs condemning institute. ";
		String paragraph6 = "3) Defamation or scandalous statements against the fellow students institute and institute"
						+ " authorities or staff.";
		String paragraph7 = "I will not participate in or abet or propagate through any act of commission or omission"
						+ " that may constituted as misconduct. The above activities are only illustrative and not exhaustive or inclusive of all acts of"
						+ " misconduct.";
		String paragraph8 = "I hereby undertake to attend minimum 50% of lectures in the SEED Programme. ";
		String paragraph9 = "I am aware that if I failed to do so, my certificate will not be granted. I am joining certificate"
						+ " course of seed programme on my own accord which is a value added autonomous course"
						+ " with one Common and one optional subject as stated above for Three/Five Consecutive"
						+ " years of my undergraduation at Prahladrai Dalmia Lions College of Comm &amp; Eco.";

		//StringBuffer termsAndConditionBuffer = new StringBuffer(termsAndCondition);

		List<String> splitedCollegeName = new ArrayList<String>();
		TAndCFinalPage paragraphFormating = new TAndCFinalPage();
		splitedCollegeName = paragraphFormating.stringSplit((onlineAdmission.getBranch() == null) ? " " : (onlineAdmission.getBranch().getName() == null) ? " " : onlineAdmission.getBranch().getName().toUpperCase(), HEADER_SPLIT_POSITION + 12, 0);

		PDPageContentStream headerContentStream = new PDPageContentStream(doc, page, true, true, true);
		headerContentStream.setNonStrokingColor(Color.BLACK);

		/************************************** To draw image ***************************************/
		PDXObjectImage xImage = null;
		try {
			String imagePath = branchLogo;
			if (imagePath.toLowerCase().endsWith(".jpg")) {
				xImage = new PDJpeg(doc, new FileInputStream(imagePath));
			}
			/*else if (imagePath.toLowerCase().endsWith(".tif") || imagePath.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (RandomAccess) new RandomAccessFile(new File(image),"r"));
			}*/
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(imagePath));
				xImage = new PDPixelMap(doc, awtImage);
			}
			headerContentStream.drawXObject(xImage, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 10,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 80, 60, 50);
		} catch (FileNotFoundException fe) {
			//System.out.println("Image not found");
			fe.printStackTrace();
		}

		/************************************** To draw college name ***************************************/
		int fontSize = 16;
		PDFont font = PDType1Font.HELVETICA;
		float yPosition = PDPage.PAGE_SIZE_A4.getUpperRightY();
		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, HEADER_FONT_SIZE);
		headerCenterXPosition = makeCenterText(splitedCollegeName, font,(int) page.getMediaBox().getUpperRightY());
		headerContentStream.moveTextPositionByAmount(headerCenterXPosition.get(splitedCollegeName.get(0)), yPosition -= 150);
		for (int i = 0; i < splitedCollegeName.size(); i++) {
			headerContentStream.drawString(splitedCollegeName.get(i));
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -19);
		}
		headerContentStream.appendRawCommands("T*\n");
		headerContentStream.setFont(font, HEADER_FONT_SIZE - 6);
		headerContentStream.drawString((branch.getAddressLine() == null) ? " " : branch.getAddressLine().toUpperCase() + " " + "," + " "
				+ (branch.getCity() != null ? branch.getCity().toUpperCase() : "") + " " + "-" + " " + branch.getPincode());
		headerContentStream.endText();

		/************************************* To draw Terms Ad Condition ********************************/

		TAndCFinalPage paragraphFormating1 = new TAndCFinalPage();
		TAndCFinalPage paragraphFormating2 = new TAndCFinalPage();
		TAndCFinalPage paragraphFormating3 = new TAndCFinalPage();
		TAndCFinalPage paragraphFormating4 = new TAndCFinalPage();
		TAndCFinalPage paragraphFormating5 = new TAndCFinalPage();
		TAndCFinalPage paragraphFormating6 = new TAndCFinalPage();
		TAndCFinalPage paragraphFormating7 = new TAndCFinalPage();
		TAndCFinalPage paragraphFormating8 = new TAndCFinalPage();
		TAndCFinalPage paragraphFormating9 = new TAndCFinalPage();

		List<String> splitedpParagraph1 = paragraphFormating1.stringSplit(paragraph1, 80, 0);
		splitedpParagraph1 = paragraphFormating1.formatingText(splitedpParagraph1, 80, font, fontSize, page);

		List<String> splitedpParagraph2 = paragraphFormating2.stringSplit(paragraph2, 80, 0);
		splitedpParagraph2 = paragraphFormating2.formatingText(splitedpParagraph2, 80, font, fontSize, page);

		List<String> splitedpParagraph3 = paragraphFormating3.stringSplit(paragraph3, 80, 0);
		splitedpParagraph3 = paragraphFormating3.formatingText(splitedpParagraph3, 80, font, fontSize, page);

		List<String> splitedpParagraph4 = paragraphFormating4.stringSplit(paragraph4, 80, 0);
		splitedpParagraph4 = paragraphFormating4.formatingText(splitedpParagraph4, 80, font, fontSize, page);

		List<String> splitedpParagraph5 = paragraphFormating5.stringSplit(paragraph5, 80, 0);
		splitedpParagraph5 = paragraphFormating5.formatingText(splitedpParagraph5, 80, font, fontSize, page);

		List<String> splitedpParagraph6 = paragraphFormating6.stringSplit(paragraph6, 80, 0);
		splitedpParagraph6 = paragraphFormating6.formatingText(splitedpParagraph6, 80, font, fontSize, page);

		List<String> splitedpParagraph7 = paragraphFormating7.stringSplit(paragraph7, 80, 0);
		splitedpParagraph7 = paragraphFormating7.formatingText(splitedpParagraph7, 80, font, fontSize, page);

		List<String> splitedpParagraph8 = paragraphFormating8.stringSplit(paragraph8, 80, 0);
		splitedpParagraph8 = paragraphFormating8.formatingText(splitedpParagraph8, 80, font, fontSize, page);

		List<String> splitedpParagraph9 = paragraphFormating9.stringSplit(paragraph9, 80, 0);
		splitedpParagraph9 = paragraphFormating9.formatingText(splitedpParagraph9, 80, font, fontSize, page);

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 60);
		for (String oneLine : splitedpParagraph1) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 55);
		for (String oneLine : splitedpParagraph2) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 55);
		for (String oneLine : splitedpParagraph3) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 70); // for 2) point
		for (String oneLine : splitedpParagraph4) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 35); // for 3) point
		for (String oneLine : splitedpParagraph5) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 35);
		for (String oneLine : splitedpParagraph6) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 35);
		for (String oneLine : splitedpParagraph7) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 50);
		for (String oneLine : splitedpParagraph8) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0, previousContentY -= 20);
		for (String oneLine : splitedpParagraph9) {
			headerContentStream.drawString(oneLine);
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -18);
		}
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(30, 100);
		headerContentStream.drawString("Date  :  " + dateFormat.format(date));
		headerContentStream.endText();

		headerContentStream.drawLine(25, 75, 145, 75);

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(30, 60);
		headerContentStream.drawString("Signature Of Parents");
		headerContentStream.endText();

		headerContentStream.drawLine(415, 75, 560, 75);

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(430, 60);
		headerContentStream.drawString("Signature Of Student");
		headerContentStream.endText();

		headerContentStream.close();
		/*previousContentY = 700;
		Y0 = previousContentY;*/

	}

	int flag;
	public List<String> list = new ArrayList<String>();

	public List<String> stringSplit(String text, int limit, int flag) // method to split text or wrap text
	{

		if (flag == 0) {
			list.clear();
			flag = 1;
		}
		//List<String> innerStringSplit(String text1, int limit1){
		int thislimit = limit;
		int spacePosition = 0, z = 0;
		if (text.length() > thislimit) {
			if (text.substring(0, thislimit).contains(" ")) {
				for (int i = z; i < thislimit; i++) {
					if (text.charAt(i) == ' ') {
						spacePosition = i;
					}
				}
				if (text.charAt(spacePosition) == ' ') {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition + 1, text.length()), thislimit, 1);
				} else {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition, text.length()), thislimit, 1);
				}
			} else {
				list.add(text.substring(0, thislimit));
				stringSplit(text.substring(thislimit, text.length()), thislimit, 1);
			}
		} else {
			list.add(text);
		}
		return list;
		//}

	}


	private Map<String, Float> makeCenterText(List<String> splitedTextList, PDFont font,  int totalWidth) throws IOException {
		return makeCenterText(splitedTextList, null, (float) totalWidth);
	}

	private Map<String, Float> makeCenterText(String text, PDFont font, int totalWidth) throws IOException {
		return makeCenterText(null, text, (float) totalWidth);
	}

	private Map<String, Float> makeCenterText(List<String> splitedTextList, String text, Float totalWidth) throws IOException {
		Map<String, Float> splitedTextListWithXPosition = new HashMap<String, Float>();
		float xPosition;
		if (splitedTextList != null) {
			for (String nextText : splitedTextList) {
				PDFont font = PDType1Font.HELVETICA;
				int fontSize = 16; // Or whatever font size you want.
				float titleWidth = font.getStringWidth(nextText) / 1000 * fontSize;
				//float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
				float midOfText;
				float midOfWidth;
				midOfText =(titleWidth / 2);
				midOfWidth = (totalWidth / 2);
				xPosition = (midOfWidth - midOfText);
				splitedTextListWithXPosition.put(nextText, midOfText);
			}
		}
		else {
			PDFont font = PDType1Font.HELVETICA;
			int fontSize = 16; // Or whatever font size you want.
			float titleWidth = (font.getStringWidth(text) / 1000) * fontSize;
			//float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
			float midOfText;
			float midOfWidth;
			midOfText =(titleWidth / 2);
			midOfWidth = (totalWidth / 2);
			xPosition = (midOfWidth - midOfText);
			splitedTextListWithXPosition.put(text, midOfWidth);
		}
		return splitedTextListWithXPosition;
	}
	
	public List<String> formatingText(List<String> splitedpParagraph, float limit, PDFont font, int fontSize, PDPage page) throws IOException {
		/********** logic to include space in string **********/

		for (int i = 0; i < splitedpParagraph.size(); i++) {
			if (splitedpParagraph.get(i).contains(" ")) {
				String tempText1 = splitedpParagraph.get(i);
				tempText1 = tempText1.replace(" ", "   ");
				splitedpParagraph.remove(i);
				splitedpParagraph.add(i, tempText1);
			}
		}

		for (int i = 0; i < splitedpParagraph.size(); i++) { //logic for justify alignment add extra space  in line 

			String tempText1 = splitedpParagraph.get(i);
			float titleWidth = font.getStringWidth(tempText1) / 1000 * fontSize;
			float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
			float limit1 = -((page.getMediaBox().getWidth() - titleWidth) / 2);
			float remainingPixels = -(limit - limit1);
			//System.out.println("Remaining pixels are ::"+remainingPixels);
			String pixcelString = " ";
			/*for(int j = 1; j < remainingPixels; j++){
				pixcelString += " ";
			}*/
			if (limit1 < limit) {
				if (remainingPixels > 0) {
					StringBuffer temp2 = new StringBuffer(tempText1);
					;
					for (int k = 0; k < temp2.length(); k++) {
						if (remainingPixels > 0) {
							if (temp2.charAt(k) == ' ') {
								temp2.replace(k, k + 1, "  ");
								remainingPixels--;
							}
						}
					}

					splitedpParagraph.remove(i);
					splitedpParagraph.add(i, temp2.toString());
				}
			}
		}

		return splitedpParagraph;
	}

}
