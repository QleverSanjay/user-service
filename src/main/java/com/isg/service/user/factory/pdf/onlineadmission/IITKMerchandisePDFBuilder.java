package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.util.StringUtils;

import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.util.FileStorePathAssembler;

public class IITKMerchandisePDFBuilder implements PdfBuilder{
	
	public float marginX = 60;
	public float previousContentOfY;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 70f;
	//PDXObjectImage image = null;
	int academicYearId, branchId;
	String identifier;
	private static final Logger log = Logger.getLogger(RoseminePdfBuilder.class);

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException {
		createPdf(onlineAdmission, pdfFilePath, null, branchLogo);
	}

	public PDPageContentStream drawHeader(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission,
					String branchLogo) throws IOException {
		Date date = new Date();

		//Drawing logo of Branch 
		if (!StringUtils.isEmpty(branchLogo))
		{
			drawImage(doc, branchLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}
		else {
			drawImage(doc, FileStorePathAssembler.QFIX_LOGO_IMAGE_RELATIVE_PATH, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}

		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, 200, 730, PDType1Font.HELVETICA, 16, "Indian Institute of Technology Kharagpur ");
		drawString.drawString(contentStream, 200, 710, PDType1Font.HELVETICA, 12, "");

		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, 220, 650, PDType1Font.HELVETICA, 16, "Merchandise Form");
		contentStream.setNonStrokingColor(Color.BLACK);

		//Application Id  
		drawString.drawString(contentStream, 60, 575, PDType1Font.HELVETICA, 10, "Application Id :");
		drawString.drawString(
						contentStream,
						145,
						575,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getApplicationId()) ? !StringUtils.isEmpty(onlineAdmission.getApplicationId()) ? onlineAdmission
										.getApplicationId() : "" : ""));

		//contentStream.drawLine(60, 565, 260, 565);

		drawString.drawString(contentStream, 420, 575, PDType1Font.HELVETICA, 10, "Date :");
		drawString.drawString(contentStream, 455, 575, PDType1Font.HELVETICA, 10, new SimpleDateFormat("dd-MMM-yyyy").format(date));
		//contentStream.drawLine(415, 565, 575, 565);

		previousContentOfY = 560;
		return contentStream;
	}

	public PDPageContentStream childContent(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Personal Details");
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Student Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getName()) ? !StringUtils
										.isEmpty(onlineAdmission.getCandidateDetails().getName().getFirstname()) ? onlineAdmission.getCandidateDetails()
										.getName().getFirstname().toUpperCase() : ""
										: ""
										: "")
										+ " "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getMiddlename()) ? onlineAdmission.getCandidateDetails().getName().getMiddlename()
														.toUpperCase() : "" : "" : "")
										+ " "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getSurname()) ? onlineAdmission.getCandidateDetails().getName().getSurname().toUpperCase()
														: "" : "" : ""));

		
		
		try{
			
			if(!StringUtils.isEmpty(onlineAdmission.getAcademicDetails().get(0).getYear())){
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Year of Graduation");
			drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
			drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10,
							(!StringUtils.isEmpty(onlineAdmission.getAcademicDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAcademicDetails().get(0).getYear()) 
									?onlineAdmission.getAcademicDetails().get(0).getYear(): "":""));
			}
			if(!StringUtils.isEmpty(onlineAdmission.getAcademicDetails().get(0).getDepartment())){
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Department");
			drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
			drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10,
							(!StringUtils.isEmpty(onlineAdmission.getAcademicDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAcademicDetails().get(0).getDepartment()) 
									?onlineAdmission.getAcademicDetails().get(0).getDepartment(): "":""));
			}
			if(!StringUtils.isEmpty(onlineAdmission.getGeneralComment())){
			
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Comment");
			drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
			drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10,
							(!StringUtils.isEmpty(onlineAdmission.getGeneralComment())?onlineAdmission.getGeneralComment():""));
			
			}
		
		}catch(NullPointerException e){
			
		}
		
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Payment Description");
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Merchandise Price");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (onlineAdmission
						.getCourseDetails() != null ? onlineAdmission.getCourseDetails().getPaymentAmount() + "" : ""));

		return contentStream;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc, boolean addNewPage) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentOfY < 56 || addNewPage) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentOfY = 780;
		}
		return contentStream1;
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}

	public void drawImage(PDDocument doc, String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {

		PDXObjectImage xImage = null;
		InputStream in = null;
		try {
			//System.out.println("Image Path to draw>>>>:::" + path);
			log.debug("Image Path to draw>>>>:::" + path);
			String image = path;
			if (image.toLowerCase().endsWith(".jpg"))
			{
				in = new FileInputStream(image);
				xImage = new PDJpeg(doc, in);
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Image not Found at ::" + path);
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException {
		PDDocument doc = new PDDocument();
		PDPageContentStream contentStream = null;
		PDPage page;
		try {
			System.out.println(pdfFilePath);
			page = new PDPage();
			doc.addPage(page);
			contentStream = new PDPageContentStream(doc, page);
			DrawString drawString = new DrawString();
			contentStream = drawHeader(doc, contentStream, drawString, onlineAdmission, branchLogo);
			contentStream = childContent(doc, contentStream, drawString, onlineAdmission);
			contentStream.close();
			doc.save(pdfFilePath);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}

			if (contentStream != null) {
				contentStream.close();
			}

		}
	}
	

}
