package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.isg.service.user.model.onlineadmission.OnlineAdmission;

public class LastExamDetails1 {

	/**
	 * @param args
	 */
	PDDocument doc = null;
	PDPage page = null;
	public int RECTANGLE_WIDTH = 535;
	public int RECTANGLE_HEIGHT = 12;
	public float X0 = 40f;
	public float marginX = 5f;
	public float previousContentY;
	public float CONTENT_FONT_SIZE = 8;
	public float ROW_WIDTH = 535;
	private final float ROW_HEIGHT = 20;
	int remarksFlag = 0;
	private final PDRectangle PAGE_SIZE = PDPage.PAGE_SIZE_A4;
	private final float MARGIN = 50;
	private final boolean IS_LANDSCAPE = false;
	float LASTHEADERLINEYPOSITION = 0;
	float LASTROWLINEYPOSITION = 0;
	public int header = 0;
	// Font configuration
	private final PDFont TEXT_FONT = PDType1Font.HELVETICA;
	private final float FONT_SIZE = 8;

	// Table configuration
	private final float CELL_MARGIN = 2;
	int rowCounter = 0;
	int maxRowCounter = 0;
	public float UPPER_CONTENT_HEIGHT;

	/************************ Drawing the data for Last Exam Details **********/

	public Object[] addLastExamDetails(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission) throws IOException {

		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];

		previousContentY = ((float) previousContenyYAndContentStream[0]) - 40;

		contentStream = checkEndOfPage(contentStream, doc, 0);
		contentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		//System.out.println("Inside of the Last Exam Details Details ");

		/******************* to draw Last Exam Details text ******************/
		contentStream.fillRect(X0 + marginX, previousContentY -= 18, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY += 5);
		contentStream.drawString("Last Exam Details :");
		contentStream.endText();
		contentStream.drawLine(X0 + marginX, previousContentY -= 3, X0 + marginX + ROW_WIDTH, previousContentY);

		com.isg.service.user.model.onlineadmission.QualifyingExamDetails qaulifyingExamDetailsList = new com.isg.service.user.model.onlineadmission.QualifyingExamDetails();
		qaulifyingExamDetailsList = onlineAdmission.getQualifyingExamDetails();

		contentStream = drawTable(contentStream, qaulifyingExamDetailsList);
		previousContenyYAndContentStream[0] = previousContentY;
		previousContenyYAndContentStream[1] = contentStream;
		return previousContenyYAndContentStream;

		/****************** to draw content of Last Exam Details ************/
	}

	private PDPageContentStream generateContentStream(PDPageContentStream contentStream, Table table) throws IOException {
		// User transformation matrix to change the reference when drawing.
		// This is necessary for the landscape position to draw correctly
		if (table.isLandscape()) {
			contentStream.concatenate2CTM(0, 1, -1, 0, table.getPageSize().getWidth(), 0);
		}
		contentStream.setFont(table.getTextFont(), table.getFontSize());
		return contentStream;
	}

	private PDPageContentStream makeContentStreamLandscapePosition(PDPageContentStream contentStream, Table table) throws IOException {

		// User transformation matrix to change the reference when drawing.
		// This is necessary for the landscape position to draw correctly
		if (table.isLandscape()) {
			contentStream.concatenate2CTM(0, 1, -1, 0, table.getPageSize().getWidth(), 0);
		}
		contentStream.setFont(table.getTextFont(), table.getFontSize());
		return contentStream;
	}

	private Table createSubjectContent(com.isg.service.user.model.onlineadmission.QualifyingExamDetails qaulifyingExamDetailsList) {

		// Total size of columns must not be greater than table width.
		UPPER_CONTENT_HEIGHT = PDPage.PAGE_SIZE_A4.getUpperRightY() - 200;
		String[][] content;
		List<Column> columns = new ArrayList<Column>();
		// Start print logic
		content = new String[1][9];
		int i = 0;
		int j = 0;
		int theorycounter = 0, practicalcounter = 0, totalcounter = 0, remarkscounter = 0;
		int qualifyingExamDetails = 1;
		int srNo = 0; //adding data to content condition - any on row of that column have data

		content[i][j] = ((qaulifyingExamDetailsList.getExam() != null) ? qaulifyingExamDetailsList.getExam().getName().toUpperCase() : " ");
		//if(theorycounter != lengthOfQaulifyingDetails)
		content[i][++j] = ((qaulifyingExamDetailsList.getPassingYear() != null) ? qaulifyingExamDetailsList.getPassingYear().toUpperCase() : " ");

		//if(practicalcounter != lengthOfQaulifyingDetails)
		content[i][++j] = ((qaulifyingExamDetailsList.getSchoolCollege() != null) ? qaulifyingExamDetailsList.getSchoolCollege().toUpperCase() : " ");

		//if(totalcounter != lengthOfQaulifyingDetails)
		content[i][++j] = ((qaulifyingExamDetailsList.getBoardUniversity().getName() != null) ? qaulifyingExamDetailsList.getBoardUniversity().getName()
						.toUpperCase() : " ");

		//if(remarkscounter != lengthOfQaulifyingDetails)
		content[i][++j] = ((qaulifyingExamDetailsList.getExamSeatNo() != null) ? qaulifyingExamDetailsList.getExamSeatNo().toUpperCase() : " ");

		content[i][++j] = ((qaulifyingExamDetailsList.getObtainedMarks() != null) ? qaulifyingExamDetailsList.getObtainedMarks() + "" : " ");

		content[i][++j] = ((qaulifyingExamDetailsList.getTotalMarks() != null) ? qaulifyingExamDetailsList.getTotalMarks().toString() : " ");

		content[i][++j] = ((qaulifyingExamDetailsList.getPercentage() != null) ? qaulifyingExamDetailsList.getPercentage().toString() : " ");

		content[i][++j] = ((qaulifyingExamDetailsList.getGrade() != null) ? qaulifyingExamDetailsList.getGrade().toUpperCase() : " ");
		//Adjust array counter

		List<String> lastExamSplitedList = TextFormatter.stringSplit("Last Exam", 10, 0);

		List<String> yearInWhichPassSplitedList = TextFormatter.stringSplit("Year in which pass", 50, 0);

		List<String> collegeAttendedSplitedList = TextFormatter.stringSplit("College Attended", 50, 0);

		List<String> boardOrUniversitySplitedList = TextFormatter.stringSplit(" Board / University", 50, 0);

		List<String> marksObtainSplitedList = TextFormatter.stringSplit("Marks Obtained", 50, 0);

		List<String> totalmarksSplitedList = TextFormatter.stringSplit("Total Marks ", 50, 0);

		List<String> percentageSplitedList = TextFormatter.stringSplit("Percentage", 10, 0);

		List<String> gradeSplitedList = TextFormatter.stringSplit("Grade ", 10, 0);
		int columnsLength;
		/*if(academicDetailsList.size() != 0)									//add column if it have value , if it contains NA then remove
		{*/
		columns.add(new Column(lastExamSplitedList, 50));
		/*}
		if (theorycounter == academicDetailsList.size()) {		
		} else {*/
		columns.add(new Column(yearInWhichPassSplitedList, 90));
		/*}
		if (practicalcounter == academicDetailsList.size()) {
		}
		else{*/columns.add(new Column(collegeAttendedSplitedList, 100));
		/*}
		if (totalcounter == academicDetailsList.size()) {
		} else {*/
		columns.add(new Column(boardOrUniversitySplitedList, 100));
		//		}
		//		if (remarkscounter == academicDetailsList.size()) {
		//		} else {
		//remarksFlag = 1;										//flag to indicate right alignment is only for remarks
		//}
		columns.add(new Column(marksObtainSplitedList, 85));
		columns.add(new Column(totalmarksSplitedList, 65));
		columns.add(new Column(percentageSplitedList, 50));
		//System.out.println("The list of columns "+columns);
		//Code for make length of all column equal
		columnsLength = (columns.size() == 0) ? 1 : columns.size();
		int equalWidth = (500 / columnsLength); //adjust value according to text 
		//columns.remove(columnsLength-1);
		/*if(columns.size() != 0)
		{
			for(Column c : columns)
			{
				//c.setWidth(equalWidth);
			}
		}*/

		float tableHeight = ROW_HEIGHT * (2);
		Table table = new TableBuilder().setCellMargin(CELL_MARGIN)
						.setColumns(columns).setContent(content).setHeight(tableHeight)
						.setNumberOfRows(content.length).setRowHeight(ROW_HEIGHT)
						.setMargin(MARGIN).setPageSize(PAGE_SIZE)
						.setEqualWidth(equalWidth)
						.setLandscape(IS_LANDSCAPE).setTextFont(TEXT_FONT)
						.setFontSize(FONT_SIZE).build();
		List<Column> columns1 = table.getColumns();
		for (Column column : columns1) {
			header = (column.getName().size() > header) ? column.getName().size() : header;
		}
		return table;
	}

	public PDPageContentStream drawTable(PDPageContentStream contentStream,
					com.isg.service.user.model.onlineadmission.QualifyingExamDetails qaulifyingExamDetailsList) throws IOException {
		Table table = createSubjectContent(qaulifyingExamDetailsList);

		// Calculate pagination
		Integer rowsPerPage = table.getContent().length + 1; // subtract

		/* PDPage page = null;
		 page = generatePage(doc, table);*/
		contentStream = generateContentStream(contentStream, table);
		String[][] currentPageContent = getContentForCurrentPage(table, rowsPerPage, 0);
		drawCurrentPage(table, currentPageContent, contentStream);

		//    table = createCategoryContent(studentResult);
		//    // Calculate pagination
		//    rowsPerPage = table.getContent().length + 1; // subtract
		//
		//    contentStream = generateContentStream(doc, page, table);
		//    currentPageContent = getContentForCurrentPage(table, rowsPerPage, 0);
		//    drawCurrentPage(table, currentPageContent, contentStream);

		return contentStream;
	}

	private String[][] getContentForCurrentPage(Table table, Integer rowsPerPage, int pageCount) {
		int startRange = pageCount * rowsPerPage;
		int endRange = (pageCount * rowsPerPage) + rowsPerPage;
		if (endRange > table.getNumberOfRows()) {
			endRange = table.getNumberOfRows();
		}
		return Arrays.copyOfRange(table.getContent(), startRange, endRange);
	}

	private void drawCurrentPage(Table table, String[][] currentPageContent, PDPageContentStream contentStream)
					throws IOException {
		//float tableTopY = table.isLandscape() ? table.getPageSize().getWidth() - table.getMargin() : table.getPageSize().getHeight() - table.getMargin();

		float tableTopY = previousContentY - 20;
		// Draws grid and borders

		drawTableGrid(table, currentPageContent, contentStream, tableTopY);

		// Position cursor to start drawing content
		float nextTextX = table.getMargin() + table.getCellMargin();
		// Calculate center alignment for text in cell considering font height
		float nextTextY = tableTopY - (table.getRowHeight() / 2)
						- ((table.getTextFont().getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * table.getFontSize()) / 4);

		// Write column headers
		writeHeaderContentLine(table.getColumns(), contentStream, nextTextX, nextTextY, table, PDType1Font.HELVETICA_BOLD, 8f, 1);

		List<Column> columns = table.getColumns();
		for (Column column : columns) {
			header = (column.getName().size() > header) ? column.getName().size() : header;
		}
		nextTextY -= table.getRowHeight() * (header + 1 + maxRowCounter);
		nextTextY += 20;
		nextTextX = table.getMargin() + table.getCellMargin();

		// Write content
		//nextTextY -= table.getRowHeight() ;
		for (int i = 0; i < currentPageContent.length; i++) {

			writeContentLine(currentPageContent[i], contentStream, nextTextX, nextTextY, table, PDType1Font.HELVETICA, 8f, 0);
			nextTextY -= table.getRowHeight();
			nextTextX = table.getMargin() + table.getCellMargin();
		}

		//contentStream.close();
	}

	private void writeHeaderContentLine(List<Column> columns, PDPageContentStream contentStream, float nextTextX, float nextTextY,
					Table table, PDType1Font fontType, float fontSize, int columnIdentifies) throws IOException {
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			//System.out.println("Column list is ::"+columns);
			List<String> splitedHeader = columns.get(i).getName();

			contentStream.setFont(fontType, fontSize);
			int middleColumnIdentifierFlag = 0;
			/*if(i == (table.getNumberOfColumns()-1))      //drawing for last column at right alignment
			{
				float textWidth = TEXT_FONT.getStringWidth(text) / 1000 * fontSize;
				float titleHeight = TEXT_FONT.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
				stream.moveTextPositionByAmount(, page.getMediaBox().getHeight - marginTop - titleheight);*/
			//nextTextX += ( (table.getEqualWidth() - textWidth ) - (table.getCellMargin()+3));//10 is just for adjusting pixel which reduce the onborder overlaping

			contentStream.beginText();
			middleColumnIdentifierFlag = 1;
			contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
			/*}
			else if(i != 0 && middleColumnIdentifierFlag == 0)				//Drawing for All middle Column for Center Alignment
			{
				int minus ;
				minus = (columnIdentifies == 1) ? 20 : 10;					//for Header Adjustment
				nextTextX += ( (table.getEqualWidth() / 2 ) - minus );
				contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
				nextTextX -= ( (table.getEqualWidth() / 2 ) - minus );
			}
			else															//Drawing for First column for Left Alignment
			{*/
			//contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
			//}

			for (String text : splitedHeader) {
				contentStream.drawString(text != null ? text : "NA");
				contentStream.appendRawCommands("T*\n");
				contentStream.moveTextPositionByAmount(0, -12);
			}

			nextTextY -= (header * 12);
			LASTHEADERLINEYPOSITION = nextTextY;
			nextTextY += (header * 12);
			nextTextX += table.getColumns().get(i).getWidth();
			contentStream.endText();
		}
	}

	private void writeContentLine(String[] lineContent, PDPageContentStream contentStream, float nextTextX, float nextTextY,
					Table table, PDType1Font fontType, float fontSize, int columnIdentifies) throws IOException {

		//lastHeaderLineYPosition = nextTextY - lastHeaderLineYPosition;
		//nextTextY += lastHeaderLineYPosition;  
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			String text = lineContent[i];

			List<String> splitedText = TextFormatter.stringSplit(text, 10, 0);
			contentStream.beginText();
			contentStream.setFont(fontType, fontSize);
			//int middleColumnIdentifierFlag = 0;

			/* if(i == (table.getNumberOfColumns()-1))      //drawing for last column at right alignment
			{
			 	float textWidth = TEXT_FONT.getStringWidth(text) / 1000 * fontSize;
			 	float titleHeight = TEXT_FONT.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
			 	stream.moveTextPositionByAmount(, page.getMediaBox().getHeight - marginTop - titleheight);
			 	nextTextX += ( (table.getEqualWidth() - textWidth ) - (table.getCellMargin()+3));//10 is just for adjusting pixel which reduce the onborder overlaping
			 	
			 	contentStream.moveTextPositionByAmount(nextTextX, nextTextY );
			 	middleColumnIdentifierFlag = 1;
			}
			 else if(i != 0 && middleColumnIdentifierFlag == 0)				//Drawing for All middle Column for Center Alignment
			 {
			 	int minus ;
			 	minus = (columnIdentifies == 1) ? 20 : 10;					//for Header Adjustment
			 	nextTextX += ( (table.getEqualWidth() / 2 ) - minus );
			 	contentStream.moveTextPositionByAmount(nextTextX, nextTextY );
			 	nextTextX -= ( (table.getEqualWidth() / 2 ) - minus );
			 }
			 else															//Drawing for First column for Left Alignment
			 {*/
			contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
			/*}*/
			//System.out.println("Row content for drawing   ::   "+splitedText);

			//for(String lineBylineText : splitedText){
			contentStream.drawString(text != null ? text : "NA");
			//contentStream.appendRawCommands("T*\n");	
			//contentStream.moveTextPositionByAmount(0, -12);
			rowCounter++;
			// }
			maxRowCounter = (rowCounter > maxRowCounter) ? rowCounter : maxRowCounter;
			nextTextY -= (maxRowCounter * 12);
			LASTROWLINEYPOSITION = nextTextY;
			nextTextY += (maxRowCounter * 12);
			contentStream.endText();
			nextTextX += table.getColumns().get(i).getWidth();

		}
		previousContentY = nextTextY;
	}

	private void drawTableGrid(Table table, String[][] currentPageContent, PDPageContentStream contentStream, float tableTopY)
					throws IOException {
		// Draw row lines
		float nextY = tableTopY;

		int header = 0;
		List<Column> columns = table.getColumns();
		for (Column column : columns) {
			header = (column.getName().size() > header) ? column.getName().size() : header;
		}
		int currentPageLength = currentPageContent.length + header + maxRowCounter + 1;
		for (int i = 0; i <= currentPageLength; i++) {
			if (i == 0) {
				contentStream.drawLine(table.getMargin(), nextY, table.getMargin() + table.getWidth(), nextY);
			}
			if (i > (header - 1)) {
				nextY += table.getRowHeight();
				contentStream.drawLine(table.getMargin(), nextY, table.getMargin() + table.getWidth(), nextY);
				nextY -= table.getRowHeight();
			}
			nextY -= table.getRowHeight();
		}

		// Draw column lines
		final float tableYLength = table.getRowHeight() + (table.getRowHeight() * (currentPageLength - 1));
		final float tableBottomY = (tableTopY - tableYLength) + table.getRowHeight();
		float nextX = table.getMargin();
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			contentStream.drawLine(nextX, tableTopY, nextX, tableBottomY);
			nextX += table.getColumns().get(i).getWidth();
		}

		if (currentPageLength != 0)
		{
			contentStream.drawLine(nextX, tableTopY, nextX, tableBottomY);
		}
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		//PDPageContentStream contentStream = new PDPageContentStream(doc, page,true,true);
		doc.addPage(page1);
		return page1;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc, int beginTextFlag) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentY < 70) {
			if (beginTextFlag == 1) {
				contentStream.endText();
			}
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentY = 780;
			if (beginTextFlag == 1) {
				contentStream1.beginText();
				beginTextFlag = 1;
			}
		}
		return contentStream1;
	}

}