package com.isg.service.user.factory.pdf.onlineadmission;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

public class DrawString {

	/**
	 * @param args
	 * @throws IOException 
	 * This method is use to draw string on contentStream
	 */
	public void drawString(PDPageContentStream contentStream, float x, float y, PDFont font, float size, Object object) throws IOException{
		contentStream.beginText();
		contentStream.moveTextPositionByAmount(x, y);
		contentStream.setFont(font, size);
		contentStream.drawString((String) object);
		contentStream.endText();
	}

	public void drawImage(PDPageContentStream contentStream, int i, int j) {
		// TODO Auto-generated method stub
		
	}
}
