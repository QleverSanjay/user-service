package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.util.FileStorePathAssembler;

public class VVSPdfBuilder implements PdfBuilder {

	public float marginX = 60;
	public float previousContentOfY;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 70f;
	private static final Logger log = Logger.getLogger(VVSPdfBuilder.class);

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException {
		createPdf(onlineAdmission, pdfFilePath, studentPhotoPath, null, branchLogo);
	}

	public PDPageContentStream drawHeader(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission,
					String studentPhotoPath, String branchLogo) throws IOException {
		
		if (!StringUtils.isEmpty(branchLogo))
		{
			drawImage(doc, branchLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}
		else {
			drawImage(doc, FileStorePathAssembler.QFIX_LOGO_IMAGE_RELATIVE_PATH, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}
		
		contentStream.addRect((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 195, (PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT-50, 100,65);
		drawString.drawString(contentStream, 500, 770, PDType1Font.HELVETICA, 12, "For office use");
		drawString.drawString(contentStream, 500, 755, PDType1Font.HELVETICA, 10, "Adm No.");
		drawString.drawString(contentStream, 543, 755, PDType1Font.HELVETICA, 10, ".............");
		drawString.drawString(contentStream, 500, 742, PDType1Font.HELVETICA, 10, "Date:");
		drawString.drawString(contentStream, 540, 742, PDType1Font.HELVETICA, 10, ".............");
		drawString.drawString(contentStream, 500, 730, PDType1Font.HELVETICA, 10, "Sec: ");
		drawString.drawString(contentStream, 540, 730, PDType1Font.HELVETICA, 10, ".............");
		
		String address ="/var/www/html/images/Qfix/documents/External/vvs-name.jpg";
		drawImage(doc, address, (PDPage.PAGE_SIZE_A4.getUpperRightX() ) - 452,
				(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
								- 45, 350, 60, contentStream);
		/*drawString.drawString(contentStream, 230, 760, PDType1Font.TIMES_BOLD_ITALIC, 16, "Valley View School");

		drawString.drawString(contentStream, 170, 740, PDType1Font.HELVETICA, 8, "Senior Secondary (Plus Two) School, Affiliated to CBSE, Delhi (Affiliation No. 3430110)");
		drawString.drawString(contentStream, 200, 730, PDType1Font.TIMES_BOLD_ITALIC, 10, "Conferred the Title 'CBSE New Generation School' by the Board");
		drawString.drawString(contentStream, 170, 720, PDType1Font.HELVETICA, 10, "Ashok Road, TRF Nagar, P.O.-TELCO, Jamshedpur-831004, Jharkhand");*/
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, 190, 700, PDType1Font.HELVETICA, 14, "Registration Form for - NURSERY Session: 2018-19");

		contentStream.setNonStrokingColor(Color.BLACK);
		String applicantId = onlineAdmission.getAdmissionId().toString();
		
		//Application Id  
		drawString.drawString(contentStream, 60, 650, PDType1Font.HELVETICA, 10, "Form No :");
		drawString.drawString(
						contentStream,
						110,
						650,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(applicantId) ? applicantId : ""));

		//drawString.drawString(contentStream, 420, 650, PDType1Font.HELVETICA, 10, "Date :");
		
		contentStream.addRect((PDPage.PAGE_SIZE_A4.getUpperRightX()/2-130), (PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 290, 350,160);
		drawString.drawString(contentStream, 180, 570, PDType1Font.HELVETICA, 10, "Please affix (do not staple) a recent Post Card Size (3.5'X5') Photograph");
		drawString.drawString(contentStream, 190, 550, PDType1Font.HELVETICA, 10, " of both Parents with the Applicant (Child) in between the Parents");
		previousContentOfY = 470;
		return contentStream;
	}

	public PDPageContentStream childContent(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {

		contentStream.drawLine(marginX, previousContentOfY, marginX + 515, previousContentOfY);
		/*contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Child Information");
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
*/
		previousContentOfY -= 10;
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10, "Child Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getName()) ? !StringUtils
										.isEmpty(onlineAdmission.getCandidateDetails().getName().getFirstname()) ? onlineAdmission.getCandidateDetails()
										.getName().getFirstname().toUpperCase() : ""
										: ""
										: "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getMiddlename()) ? onlineAdmission.getCandidateDetails().getName().getMiddlename()
														.toUpperCase() : "" : "" : "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getSurname()) ? onlineAdmission.getCandidateDetails().getName().getSurname().toUpperCase()
														: "" : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Applicant is my own ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getGender()) ? onlineAdmission
						.getCandidateDetails().getGender().getName().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Date of Birth");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getDateofbirth()) ? onlineAdmission
						.getCandidateDetails().getDateofbirth() : "" : ""));

		
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Category");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getCasteDetails().getCaste()) ? onlineAdmission
						.getCandidateDetails().getCasteDetails().getCaste().getName().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Aadhar Number ");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getAadhaaarCard()) ? onlineAdmission
						.getCandidateDetails().getAadhaaarCard().toString() : "" : ""));

		/*contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 12, "This application is for");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Programme Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCourseDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCourseDetails().getCourse()) ? onlineAdmission
						.getCourseDetails().getCourse().getName() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Pay Amount (Rs.)");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, ("200"));
*/
		return contentStream;

	}

	public PDPageContentStream parentDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 20;
		/*contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Parent/Guardian Information");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);
*/
		/*contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 11, "Father Details");
		contentStream.setNonStrokingColor(Color.BLACK);
*/
		//fathers details
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Father Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");

		String fatherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) && !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName())) {
			Name nameObj = onlineAdmission.getFatherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			fatherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						fatherName);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Mobile Phone");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission
										.getContactDetails()) ? !StringUtils.isEmpty(onlineAdmission.getContactDetails()
										.getPersonalMobileNumber()) ? onlineAdmission.getContactDetails().getPersonalMobileNumber() : "" : ""));

		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Office Phone");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getSecondaryPhone()) ? onlineAdmission
						.getFatherDetails().getSecondaryPhone() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getContactDetails()) ? !StringUtils.isEmpty(onlineAdmission.getContactDetails().getPersonalEmail()) ? onlineAdmission
						.getContactDetails()
						.getPersonalEmail() : "" : ""));
		
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Resi. Land line");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getResilandline()) ? onlineAdmission
						.getFatherDetails().getResilandline() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -=20, PDType1Font.HELVETICA, 10, "Occupation");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Company");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getEmployerName()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getEmployerName().toUpperCase() : ""
										: "" : ""));

		drawString.drawString(contentStream, marginX+310, previousContentOfY , PDType1Font.HELVETICA, 10, "Monthly Income");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails().getAnnualIncome()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getAnnualIncome()
						 : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Designation & Dept.");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails().getDesignation()) ? onlineAdmission
						.getFatherDetails().getEmploymentDetails().getDesignation().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX+310, previousContentOfY, PDType1Font.HELVETICA, 10, "Personal No.");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails().getPincode()) ? onlineAdmission
						.getFatherDetails().getEmploymentDetails().getPincode() : "" : ""));

		previousContentOfY -= 10;
		contentStream.drawLine(marginX, previousContentOfY-=20 , marginX + 150, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Sign with official seal of Establishment Head/HR Dept (for Perma Empl. Only)");

		
		//mother guardian
		previousContentOfY -= 20;
		contentStream = checkEndOfPage(contentStream, doc);
		/*contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 11, "Mother Details");
		contentStream.setNonStrokingColor(Color.BLACK);
*/
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Mother Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		String motherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) && !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getName())) {
			Name nameObj = onlineAdmission.getMotherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			motherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						motherName);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Primary Phone");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getPrimaryPhone()) ? onlineAdmission.getMotherDetails()
						.getPrimaryPhone() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Secondary Phone");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getSecondaryPhone()) ? onlineAdmission
						.getMotherDetails().getSecondaryPhone() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmail()) ? onlineAdmission.getMotherDetails()
						.getEmail() : "" : ""));
		
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Resi Land line");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getResilandline()) ? onlineAdmission
						.getMotherDetails().getResilandline() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Occupation");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getMotherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Organization Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails()
										.getEmployerName()) ? onlineAdmission.getMotherDetails().getEmploymentDetails().getEmployerName().toUpperCase() : ""
										: "" : ""));
		
		contentStream = checkEndOfPage(contentStream, doc);
		
		drawString.drawString(contentStream, marginX+310, previousContentOfY, PDType1Font.HELVETICA, 10, "Designation & Dept.");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getDesignation()) ? onlineAdmission
						.getMotherDetails().getEmploymentDetails().getDesignation() : "" : ""));
		
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Monthly Income");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getAnnualIncome()) ? onlineAdmission
						.getMotherDetails().getEmploymentDetails().getAnnualIncome() : "" : ""));
		
		drawString.drawString(contentStream, marginX+310, previousContentOfY, PDType1Font.HELVETICA, 10, "Personal No.");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getPincode()) ? onlineAdmission
						.getMotherDetails().getEmploymentDetails().getPincode() : "" : ""));
		
		previousContentOfY -= 10;
		contentStream.drawLine(marginX, previousContentOfY-=20 , marginX + 150, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Sign with official seal of Establishment Head/HR Dept (for Perma Empl. Only)");

		return contentStream;
	}

	public PDPageContentStream addressDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {

		previousContentOfY -= 70;
		/*contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Address Information");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);
		*/
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Local Address");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils
										.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress()) ? !StringUtils.isEmpty(onlineAdmission
										.getAddressDetails().getLocalAddress().getAddressLine1()) ? onlineAdmission.getAddressDetails()
										.getLocalAddress()
										.getAddressLine1().toUpperCase() : "" : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "City");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress()
										) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCity()) ? onlineAdmission
										.getAddressDetails().getLocalAddress().getCity()
										: ""
										: ""
										: ""));
		
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX+310, previousContentOfY, PDType1Font.HELVETICA, 10, "Postal Code");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 420,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress()
										) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission
										.getAddressDetails().getLocalAddress().getPincode().toString()
										: ""
										: ""
										: ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Address(Nativ Place)");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress()) ? !StringUtils.isEmpty(onlineAdmission
								.getAddressDetails().getLocalAddress().getArea()) ? onlineAdmission
								.getAddressDetails().getLocalAddress().getArea()
										: ""
										: ""
										: ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "District");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission
								.getAddressDetails().getLocalAddress()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()) ? onlineAdmission
										.getAddressDetails().getLocalAddress().getDistrict().getName().toString()
										: ""
										: ""
										: ""));
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX+ 310, previousContentOfY, PDType1Font.HELVETICA, 10, "State");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 420,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission
								.getAddressDetails().getLocalAddress().getState()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
								.getLocalAddress().getState().getName()) ? onlineAdmission
								.getAddressDetails().getLocalAddress().getState().getName().toString() : "" : "" : ""));

		
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 10, "Is Father/Mother Alumni of Valley View School"
				+ " (Please attach Board Admit Card/Pass certificate)");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Alumni Yes/No ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getAlumniDetail()) ? onlineAdmission.getFatherDetails()
						.getAlumniDetail().toString() : "" : ""));
		
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "AISSE(X) Year");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getSchoolDetail()) ? onlineAdmission.getFatherDetails()
						.getSchoolDetail().toString() : "" : ""));

		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "AISSE(XII) Year");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getCollegDetail()) ? onlineAdmission.getFatherDetails()
						.getCollegDetail().toString() : "" : ""));
		
		return contentStream;
	}

	public PDPageContentStream siblingsDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 11, "Own Sister/Brother already studying at Valley View School");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name :");

		String siblingName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getSiblingDetail()) && !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getName())) {
			Name nameObj = onlineAdmission.getSiblingDetail().get(0).getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			siblingName = firstName + middleName + surName;
		}
		drawString.drawString(
						contentStream,
						marginX + 45,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						siblingName);

		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Class/Sec ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getSiblingDetail()) ? !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getGrade()) ? onlineAdmission.getSiblingDetail().get(0)
						.getGrade().toString() : "" : ""));

		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Admission No.");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getSiblingDetail()) ? !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getCurrentSchool()) ? onlineAdmission.getSiblingDetail().get(0)
						.getCurrentSchool() : "" : ""));
		return contentStream;
	}

	public PDPageContentStream otherDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 11, "Other Details");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Blood Group of Child :");
		drawString.drawString(contentStream, marginX + 120, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getBloodGroup()) ? onlineAdmission.getCandidateDetails()
						.getBloodGroup().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"Has the Child been administered immunisation vaccines after birth?");
		drawString.drawString(contentStream, marginX + 20, previousContentOfY -= 15, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getImmunisation()) ? onlineAdmission.getCandidateDetails()
						.getImmunisation().toString() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"Medical History: Has the child suffered from any Chronic ailment or undergone any prolonged medical treatment?");
		drawString.drawString(contentStream, marginX + 20, previousContentOfY -= 15, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getMedicalHistory()) ? onlineAdmission.getCandidateDetails()
						.getMedicalHistory().toString() : "" : ""));
		
		drawString.drawString(contentStream, marginX + 60, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
				.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getPsychoTesting()) ? onlineAdmission.getCandidateDetails()
				.getPsychoTesting().toString() : "" : ""));
		
		return contentStream;

	}

	public PDPageContentStream signatureDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		//contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 11, "Parent Signatures");
		//contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);
		Date date = new Date();

		drawString.drawString(contentStream, marginX, previousContentOfY-= 40, PDType1Font.HELVETICA, 10, "Date :");
		drawString.drawString(contentStream, marginX+30, previousContentOfY, PDType1Font.HELVETICA, 10, new SimpleDateFormat("dd-MMM-yyyy").format(date));
		contentStream.drawLine(marginX+25, previousContentOfY , marginX + 100, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Jamshedpur");

		contentStream.drawLine(marginX + 200, previousContentOfY += 20, marginX + 300, previousContentOfY);
		drawString.drawString(contentStream, marginX + 200, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Father Signature ");

		contentStream.drawLine(marginX + 400, previousContentOfY += 20, marginX + 500, previousContentOfY);
		drawString.drawString(contentStream, marginX + 400, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Mother Signature ");

		return contentStream;
	}

	public PDPageContentStream extraDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
			throws IOException {
			previousContentOfY -= 10;
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Note: Please take back to back print of Registration Form, affix photograph and submit Xerox copies of");
			drawString.drawString(contentStream, marginX, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, " following documents with original for verification on the Dates of Submission in School office.");
			
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Dates of Submission in School office");
			
			drawString.drawString(contentStream, marginX+10, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, "07.11.2017 (Tuesday) Permanent employees of TRF & all other Tata Companies.");
			drawString.drawString(contentStream, marginX+10, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, "08.11.2017 (Wednesday) Siblings (own bro/sis in VVS) & Govt. empl.(incl. Defence & Govt. Teachers).");
			drawString.drawString(contentStream, marginX+10, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, "09.11.2017 (Thursday) All others.");
			drawString.drawString(contentStream, marginX+10, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, "10.11.2017 (Friday) All others.");
			
			contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
			
			drawString.drawString(contentStream, marginX+150, previousContentOfY -= 20, PDType1Font.HELVETICA, 11, "For School office only");
			
			previousContentOfY -= 10;
			
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,"1. Birth Certificate of Child for all");
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,"2. Hospital Birth record of Child for all");
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,"3. Service proof of parent (Med. Book/Gate pass/ID Card for all perma. Empl. of any org.)");
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,"4. Proof of Residence for all (Electricity bill/Gas bill only)");
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,"5. One Gate pass size photograph of Child for all");
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,"6. Caste Certificate of SC/ST (if applicable)");

			previousContentOfY -= 10;
			contentStream.drawLine(marginX, previousContentOfY-=20 , marginX + 100, previousContentOfY);
			drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Receiver's Sign");
			drawString.drawString(contentStream, marginX, previousContentOfY-= 20, PDType1Font.HELVETICA, 10, "Date ");
			
			
			return contentStream;

}
	
	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentOfY < 56) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentOfY = 780;
		}
		return contentStream1;
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}

	public void drawImage(PDDocument doc, String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {

		PDXObjectImage xImage = null;
		InputStream in = null;
		try {
			//System.out.println("Image Path to draw>>>>:::" + path);
			log.debug("Image Path to draw>>>>:::" + path);
			String image = path;
			if (image.toLowerCase().endsWith(".jpg"))
			{
				xImage = new PDJpeg(doc, new FileInputStream(image));
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (org.apache.pdfbox.io.RandomAccess) new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Image not Found at ::" + path);
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public static void main(String a[]) throws JsonParseException, JsonMappingException, IOException {
		String data = "{\"course_details\":{\"course\":{\"id\":34,\"code\":\"20\",\"name\":\"Nursery\",\"active\":false,\"category\":\"PS\"}},\"candidate_details\":{\"name\":{\"firstname\":\"ram\",\"middlename\":\"g\",\"surname\":\"banaras wale\"},\"caste_details\":{\"caste\":{\"id\":\"1\",\"name\":\"General\"}},\"gender\":{\"name\":\"Son\",\"id\":\"2\"},\"dateofbirth\":\"05/01/2015\",\"blood_group\":\"A-\",\"immunisation\":\"Yes\",\"medical_history\":\"Yes\",\"psycho_testing\":\"hngfbdvs\"},\"father_details\":{\"employment_details\":{\"occupation\":\"rdsv\",\"annual_income\":\"567\"},\"name\":{\"firstname\":\"gbfcv\",\"surname\":\"fbdc\"},\"alumni_detail\":\"Yes\",\"school_detail\":\"2014\",\"college_detail\":\"2016\"},\"mother_details\":{\"employment_details\":{},\"name\":{\"firstname\":\"regdfv\",\"surname\":\"rfdbxc\"}},\"sibling_details\":[{}],\"address_details\":{\"local_address\":{\"address_line1\":\"fdgfhg grsh dfghj\",\"city\":\"adcbv\",\"pincode\":\"765478\",\"state\":{\"id\":2,\"code\":\"2\",\"name\":\"HIMACHAL PRADESH\",\"active\":true},\"district\":{\"id\":17,\"code\":\"17\",\"name\":\"Chamba\",\"stateId\":2,\"active\":true},\"area\":\"gfbcxv erbfdcx\"}},\"contact_details\":{\"personal_mobile_number\":\"8796543678\",\"personal_email\":\"fdcbx@regf.gdf\"},\"url_configuration_id\":10}";
		OnlineAdmission asdmission = new ObjectMapper().readValue(data, OnlineAdmission.class);
		new VVSPdfBuilder().createPdf(asdmission, "c:/temp/images/vvs.pdf",
						null,null,
						"C:/Temp/images/Qfix/documents/External/vvs.jpg");
	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException {
		PDDocument doc = new PDDocument();
		PDPage page;
		try {
			page = new PDPage();
			doc.addPage(page);
			PDPageContentStream contentStream = new PDPageContentStream(doc, page);
			DrawString drawString = new DrawString();
			contentStream = drawHeader(doc, contentStream, drawString, onlineAdmission, studentPhotoPath, branchLogo);
			contentStream = childContent(doc, contentStream, drawString, onlineAdmission);
			contentStream = parentDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = addressDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = siblingsDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = otherDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = signatureDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = extraDetails(doc, contentStream, drawString, onlineAdmission);

			contentStream.close();
			doc.save(pdfFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}
		}
	}
}