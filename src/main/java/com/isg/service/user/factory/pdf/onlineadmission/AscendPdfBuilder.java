package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.model.onlineadmission.CourseDetails;
import com.isg.service.user.model.onlineadmission.CoursePreference;
import com.isg.service.user.model.onlineadmission.Document;
import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.util.FileStorePathAssembler;

public class AscendPdfBuilder implements PdfBuilder {

	public float marginX = 60;
	public float previousContentOfY;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 70f;
	private static final Logger log = Logger.getLogger(AscendPdfBuilder.class);

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException {
		createPdf(onlineAdmission, pdfFilePath, studentPhotoPath, null, branchLogo);
	}

	public PDPageContentStream drawHeader(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission,
					String studentPhotoPath, String branchLogo) throws IOException {
		Date date = new Date();

		if (!StringUtils.isEmpty(branchLogo))
		{
			drawImage(doc, branchLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}
		else {
			drawImage(doc, FileStorePathAssembler.QFIX_LOGO_IMAGE_RELATIVE_PATH, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}

		drawString.drawString(contentStream, 200, 740, PDType1Font.HELVETICA, 16, "ASCEND INTERNATIONAL SCHOOL");

		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, 220, 700, PDType1Font.HELVETICA, 16, "Admission Application");
		drawString.drawString(contentStream, 220, 680, PDType1Font.HELVETICA, 12, "For the 2017-18 School Year");

		contentStream.setNonStrokingColor(Color.BLACK);

		//Application Id  
		drawString.drawString(contentStream, 60, 650, PDType1Font.HELVETICA, 10, "Application Id :");
		drawString.drawString(
						contentStream,
						145,
						650,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getApplicationId()) ? !StringUtils.isEmpty(onlineAdmission.getApplicationId()) ? onlineAdmission
										.getApplicationId() : "" : ""));

		drawString.drawString(contentStream, 420, 650, PDType1Font.HELVETICA, 10, "Date :");
		drawString.drawString(contentStream, 455, 650, PDType1Font.HELVETICA, 10, new SimpleDateFormat("dd-MMM-yyyy").format(date));
		previousContentOfY = 620;
		return contentStream;
	}

	public PDPageContentStream childContent(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {

		contentStream.drawLine(marginX, previousContentOfY, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Child Information");
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 10, "Child Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getName()) ? !StringUtils
										.isEmpty(onlineAdmission.getCandidateDetails().getName().getFirstname()) ? onlineAdmission.getCandidateDetails()
										.getName().getFirstname().toUpperCase() : ""
										: ""
										: "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getMiddlename()) ? onlineAdmission.getCandidateDetails().getName().getMiddlename()
														.toUpperCase() : "" : "" : "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getSurname()) ? onlineAdmission.getCandidateDetails().getName().getSurname().toUpperCase()
														: "" : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Gender");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getGender()) ? onlineAdmission
						.getCandidateDetails().getGender().getName().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX + 320, previousContentOfY, PDType1Font.HELVETICA, 10, "Date of Birth");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getDateofbirth()) ? onlineAdmission
						.getCandidateDetails().getDateofbirth() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 10, "My child will be ");
		drawString.drawString(contentStream, marginX + 80, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getAgeYear()) ? onlineAdmission
						.getCandidateDetails().getAgeYear() : "" : ""));

		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, "years and ");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getAgeMonth()) ? onlineAdmission
						.getCandidateDetails().getAgeMonth() : "" : ""));

		drawString.drawString(contentStream, marginX + 180, previousContentOfY, PDType1Font.HELVETICA, 10, "months by August 31st, 2017.");

		drawString.drawString(contentStream, marginX, previousContentOfY -= 10, PDType1Font.COURIER, 10,
						"Children must be three years old by August 31st,2017 to begin Half Day. To apply for");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 10, PDType1Font.COURIER, 10,
						"kindergarden I (Jr KG), Children must be at least four years old by August 31st,2017; ");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 10, PDType1Font.COURIER, 10,
						"Kindergarden II (Sr KG), children must be at least five years old.");

		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 12, "This application is for");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Programme Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCourseDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCourseDetails().getCourse()) ? onlineAdmission
						.getCourseDetails().getCourse().getName() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Grade Preference");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		String cources = getCourcepreferences(onlineAdmission);
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, cources);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Pay Amount (Rs.)");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, ("6500"));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Payment Pay Choice");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getGeneralComment()) ? !StringUtils.isEmpty(onlineAdmission.getGeneralComment()) ? onlineAdmission.getGeneralComment()
						.toString() : "" : ""));

		return contentStream;

	}

	public String getCourcepreferences(OnlineAdmission onlineAdmission) {
		String cname = "";
		CourseDetails courseDetail = onlineAdmission.getCourseDetails();
		if (courseDetail != null) {
			int i = 0;
			List<CoursePreference> cources = courseDetail.getCoursePreferenceList();
			for (CoursePreference cource : cources) {
				cname += cource.getName();
				cname += (i != cources.size() - 1 ? ", " : "");
				i++;
				if (!StringUtils.isEmpty(onlineAdmission.getCourseDetails().getOtherCourseDetail())) {
					cname += (StringUtils.isEmpty(cname) ? "" : ", ") + onlineAdmission.getCourseDetails().getOtherCourseDetail();
				}
			}
		}
		return cname;
	}

	public PDPageContentStream parentDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Parent/Guardian Information");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 11, "Parent or Guardian");
		contentStream.setNonStrokingColor(Color.BLACK);

		//fathers details
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");

		String fatherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) && !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName())) {
			Name nameObj = onlineAdmission.getFatherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			fatherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						fatherName);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Address");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils
										.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress()) ? !StringUtils.isEmpty(onlineAdmission
										.getAddressDetails().getLocalAddress().getAddressLine1()) ? onlineAdmission.getAddressDetails()
										.getLocalAddress()
										.getAddressLine1().toUpperCase() : "" : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "City");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress()
										) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCity()) ? onlineAdmission
										.getAddressDetails().getLocalAddress().getCity()
										: ""
										: ""
										: ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Postal Code");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress()
										) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission
										.getAddressDetails().getLocalAddress().getPincode().toString()
										: ""
										: ""
										: ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Primary Phone");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission
										.getContactDetails()) ? !StringUtils.isEmpty(onlineAdmission.getContactDetails()
										.getPersonalMobileNumber()) ? onlineAdmission.getContactDetails().getPersonalMobileNumber() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Secondary Phone");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getSecondaryPhone()) ? onlineAdmission
						.getFatherDetails().getSecondaryPhone() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getContactDetails()) ? !StringUtils.isEmpty(onlineAdmission.getContactDetails().getPersonalEmail()) ? onlineAdmission
						.getContactDetails()
						.getPersonalEmail() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "PAN No.");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getPanNumber()) ? onlineAdmission.getFatherDetails()
						.getPanNumber().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Occupation");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Company");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getEmployerName()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getEmployerName().toUpperCase() : ""
										: "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Relationship to child");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getGuardianDetails().getCandidaterelation()) ? onlineAdmission
						.getGuardianDetails().getCandidaterelation().toUpperCase() : "" : ""));

		//mother guardian
		previousContentOfY -= 60;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 11, "Parent or Guardian");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		String motherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) && !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getName())) {
			Name nameObj = onlineAdmission.getMotherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			motherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						motherName);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Address");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getAddress()) ? onlineAdmission
										.getMotherDetails().getEmploymentDetails().getAddress().toUpperCase()
										: ""
										: ""
										: ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "City");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getCity()) ? onlineAdmission
										.getMotherDetails().getEmploymentDetails().getCity().toUpperCase()
										: ""
										: ""
										: ""));
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Postal Code");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getPincode()) ? onlineAdmission
										.getMotherDetails().getEmploymentDetails().getPincode()
										: ""
										: ""
										: ""));
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Primary Phone");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getPrimaryPhone()) ? onlineAdmission.getMotherDetails()
						.getPrimaryPhone() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Secondary Phone");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getSecondaryPhone()) ? onlineAdmission
						.getMotherDetails().getSecondaryPhone() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmail()) ? onlineAdmission.getMotherDetails()
						.getEmail() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "PAN Number");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getPanNumber()) ? onlineAdmission.getMotherDetails()
						.getPanNumber().toString() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Occupation");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getMotherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Organization Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails()
										.getEmployerName()) ? onlineAdmission.getMotherDetails().getEmploymentDetails().getEmployerName().toUpperCase() : ""
										: "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Relationship to child");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getGuardianDetails().getRelation()) ? onlineAdmission
						.getGuardianDetails().getRelation().toUpperCase() : "" : ""));
		//contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		return contentStream;
	}

	public PDPageContentStream academicDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Academic History of Student");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Previous School ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 150, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getQualifyingExamDetails()) ? !StringUtils.isEmpty(onlineAdmission.getQualifyingExamDetails().getSchoolCollege()) ? onlineAdmission
						.getQualifyingExamDetails().getSchoolCollege().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Dates attended");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 150,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission
										.getQualifyingExamDetails().getDatesAttend()) ? !StringUtils.isEmpty(onlineAdmission.getQualifyingExamDetails()
										.getDatesAttend()) ? onlineAdmission
										.getQualifyingExamDetails().getDatesAttend().toString() : "" : ""));

		drawString.drawString(contentStream, marginX + 300, previousContentOfY, PDType1Font.HELVETICA, 10, "Grade(s)");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 450, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getQualifyingExamDetails().getGrade()) ? !StringUtils.isEmpty(onlineAdmission.getQualifyingExamDetails().getGrade()) ? onlineAdmission
						.getQualifyingExamDetails().getGrade().toString() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Address");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 150,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission
										.getQualifyingExamDetails().getAddress()) ? !StringUtils.isEmpty(onlineAdmission.getQualifyingExamDetails()
										.getAddress()) ? onlineAdmission
										.getQualifyingExamDetails().getAddress().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX + 300, previousContentOfY, PDType1Font.HELVETICA, 10, "Language of Instruction");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 450,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission
										.getQualifyingExamDetails().getLanguageInstruction()) ? !StringUtils.isEmpty(onlineAdmission.getQualifyingExamDetails()
										.getLanguageInstruction()) ? onlineAdmission.getQualifyingExamDetails().getLanguageInstruction().toUpperCase() : ""
										: ""));
		return contentStream;
	}

	public PDPageContentStream siblingsDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Siblings Applying to Ascend");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name :");

		String siblingName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getSiblingDetail()) && !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getName())) {
			Name nameObj = onlineAdmission.getSiblingDetail().get(0).getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			siblingName = firstName + middleName + surName;
		}
		drawString.drawString(
						contentStream,
						marginX + 45,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						siblingName);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX + 200, previousContentOfY, PDType1Font.HELVETICA, 10, "Sex M/F :");
		drawString.drawString(
						contentStream,
						marginX + 250,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission
										.getSiblingDetail()) ? !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getGender()) ? !StringUtils
										.isEmpty(onlineAdmission.getSiblingDetail().get(0).getGender().getName()) ? onlineAdmission
										.getSiblingDetail()
										.get(0).getGender().getName().toUpperCase() : "" : "" : ""));

		drawString.drawString(contentStream, marginX + 300, previousContentOfY, PDType1Font.HELVETICA, 10, "Age :");
		drawString.drawString(contentStream, marginX + 340, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getSiblingDetail()) ? !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getDateofbirth()) ? onlineAdmission.getSiblingDetail()
								.get(0).getDateofbirth().toString() : "" : ""));

		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, "Grade Apply :");
		drawString.drawString(contentStream, marginX + 480, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getSiblingDetail()) ? !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getGrade()) ? onlineAdmission.getSiblingDetail()
								.get(0).getGrade().toString() : "" : ""));

		return contentStream;
	}

	public PDPageContentStream languageSpoken(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Language Spoken");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "1. Mother toung(first language)");
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 360, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getMotherTounge()) ? onlineAdmission
						.getCandidateDetails().getMotherTounge().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "2. Primary languages spoken at home");
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 360, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getPrimaryLanguage()) ? onlineAdmission
						.getCandidateDetails().getPrimaryLanguage().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "3. Additional Language spoken");
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 360, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getLanguagesKnown()) ? onlineAdmission
						.getCandidateDetails().getLanguagesKnown().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"4. Has your child studied English? Y/N if yes, How many years");
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 360, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getEngYear()) ? onlineAdmission
						.getCandidateDetails().getEngYear().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"5. Has your child received ELL support(English Language support)? ");
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 360, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getEllSupport()) ? onlineAdmission
						.getCandidateDetails().getEllSupport().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"6. Has your child ever been referred for and/or received ");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10,
						"    psychological, educational or cognitive testing?");
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 360, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getPsychoTesting()) ? onlineAdmission
						.getCandidateDetails().getPsychoTesting().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"7. Has your child been diagnosed with a specific learning disability?");
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 360, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getLearningDisability()) ? onlineAdmission
						.getCandidateDetails().getLearningDisability().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "8. Has your child received learning support?");
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 360, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getLerningSupport()) ? onlineAdmission
						.getCandidateDetails().getLerningSupport().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX -= 10, previousContentOfY -= 20, PDType1Font.COURIER, 8,
						"* If yes to 6,7,8, please include reports of test therapy.");
		return contentStream;
	}

	public PDPageContentStream documentsSubmit(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 60;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Documents to submit with Application");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 10, "");

		int serialNumber = 0;
		int nextLineCounter = 0;
		int nextLine = (-12);

		Map<Integer, String> documentsList = new HashMap<Integer, String>();
		List<Document> documentList = new ArrayList<Document>();
		documentList = onlineAdmission.getDocuments();
		/************* Documents data from model is from here in if loop *****************/
		for (int i = 0; i < documentList.size(); i++) {
			documentsList.put(serialNumber = (serialNumber == 0) ? 1 : serialNumber + 1, documentList.get(i).getName());
		}

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.moveTextPositionByAmount(marginX, previousContentOfY);
		for (Integer key : documentsList.keySet()) {
			contentStream.drawString(key + "   " + documentsList.get(key));
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, nextLine = -12);
			nextLineCounter++;
		}
		previousContentOfY += (nextLine * nextLineCounter);
		contentStream.endText();
		contentStream = checkEndOfPage(contentStream, doc);

		return contentStream;
	}

	public PDPageContentStream signatureDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 20;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Parent Signatures");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream.drawLine(marginX, previousContentOfY -= 60, marginX + 200, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Parent/Guardian Signature ");

		contentStream.drawLine(marginX + 300, previousContentOfY += 20, marginX + 450, previousContentOfY);
		drawString.drawString(contentStream, marginX + 300, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Date");

		contentStream.drawLine(marginX, previousContentOfY -= 60, marginX + 200, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Parent/Guardian Signature ");

		contentStream.drawLine(marginX + 300, previousContentOfY += 20, marginX + 450, previousContentOfY);
		drawString.drawString(contentStream, marginX + 300, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Date");

		return contentStream;
	}

	public PDPageContentStream footerDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 60;
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.COURIER, 11,
						"Thank You. We wish your child the best educational experience possible.");
		drawString.drawString(contentStream, marginX + 200, previousContentOfY -= 30, PDType1Font.HELVETICA, 10, "Ascend International School");
		drawString.drawString(contentStream, marginX + 200, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, "5 F Block, Opp Govt Colony");
		drawString.drawString(contentStream, marginX + 210, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, "Bandra Kurla Complex");
		drawString.drawString(contentStream, marginX + 210, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, "Mumbai, India 400051");
		drawString.drawString(contentStream, marginX + 220, previousContentOfY -= 10, PDType1Font.HELVETICA, 10, "022 7122 2000");
		return contentStream;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentOfY < 56) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentOfY = 780;
		}
		return contentStream1;
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}

	public void drawImage(PDDocument doc, String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {

		PDXObjectImage xImage = null;
		InputStream in = null;
		try {
			//System.out.println("Image Path to draw>>>>:::" + path);
			log.debug("Image Path to draw>>>>:::" + path);
			String image = path;
			if (image.toLowerCase().endsWith(".jpg"))
			{
				xImage = new PDJpeg(doc, new FileInputStream(image));
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (org.apache.pdfbox.io.RandomAccess) new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Image not Found at ::" + path);
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public static void main(String a[]) throws JsonParseException, JsonMappingException, IOException {
		//String data = "{\"course_details\":{\"course\":{\"id\":32,\"code\":\"18\",\"name\":\"Primary Programme\",\"active\":false,\"category\":\"PS\"},\"course_preferences\":[{\"id\":5,\"name\":\"Grade5\",\"checked\":true}]},\"candidate_details\":{\"name\":{\"firstname\":\"Ram\",\"middlename\":\"lal\",\"surname\":\"JAt\"},\"mother_tounge\":\"q\",\"primary_language\":\"w\",\"languages_known\":\"e\",\"english_year\":\"r\",\"dateofbirth\":\"01/02/2001\",\"gender\":{\"name\":\"Male\",\"id\":\"1\"}},\"father_details\":{\"employment_details\":{\"occupation\":\"gbfedc\",\"employer_name\":\"kjmhgnf\"},\"name\":{\"firstname\":\"ram\",\"middlename\":\"singh\",\"surname\":\"lala\"},\"secondary_phone\":\"7984651326\",\"pan_number\":\"BHGJK7868J\"},\"mother_details\":{\"employment_details\":{\"city\":\"Goregaon\",\"address\":\"Goregaon Mumbai\",\"occupation\":\"vrecw\",\"employer_name\":\"brfvds\"},\"name\":{\"firstname\":\"gbfvdsc\",\"middlename\":\"mt\",\"surname\":\"tbre\"},\"pan_number\":\"trfd\",\"primary_phone\":\"9776435878\",\"secondary_phone\":\"8675463524\",\"email\":\"gbfdv@tgrf.gg\"},\"qualifying_exam__details\":{\"school_college\":\"trge\",\"dates_attend\":\"2014-17\",\"grade\":\"4\",\"address\":\"trgfvcx\",\"language_instruction\":\"rtefds\"},\"sibling_details\":{\"name\":{\"firstname\":\"refdc\",\"middlename\":\"refdc\",\"surname\":\"fredcs\"},\"grade\":\"5\",\"gender\":{\"name\":\"Female\",\"id\":\"2\"},\"dateofbirth\":\"01/05/1996\"},\"address_details\":{\"local_address\":{\"address_line1\":\"Goregaon Mumbai\",\"city\":\"hkgj\"}},\"documents\":[{\"id\":1,\"name\":\"Birth Certificate of Student\"},{\"id\":3,\"name\":\"1 Photograph - each parent/guardian\"},{\"id\":4,\"name\":\"Passport Copy - both parents(if held)\"},{\"id\":5,\"name\":\"Application Fee\"},{\"id\":6,\"name\":\"Teacher's recommendation form\"},{\"id\":7,\"name\":\"Parent Questionnaire\"},{\"id\":8,\"name\":\"Transcripts/prograss from current or previous school\"},{\"id\":9,\"name\":\"Student Records Request form(submitted to current or previous school)\"},{\"id\":20,\"name\":\"Passport Copy & work visa copy of both Parents\"},{\"id\":30,\"name\":\"Birth Certificate of Student\"},{\"id\":40,\"name\":\"2 Photographs - Student\"},{\"id\":50,\"name\":\"1 Photograph - each parent/guardian\"},{\"id\":100,\"name\":\"Student Records Request form(submitted to current or previous school)\"},{\"id\":90,\"name\":\"Transcripts/prograss from current or previous school\"},{\"id\":80,\"name\":\"Parent Questionnaire\"}],\"contact_details\":{\"personal_email\":\"praveeen@ggmd.com\",\"personal_mobile_number\":\"8765432456\"},\"url_configuration_id\":9,\"guardian_details\":{\"candidaterelation\":\"Father\",\"relation\":\"mother\"}}";
		String data = "{\"course_details\":{\"course\":{\"id\":31,\"code\":\"17\",\"name\":\"Pre-Primary Programme\",\"active\":false,\"category\":\"PS\"},\"course_preferences\":[{\"id\":102,\"name\":\"KG2\",\"checked\":true}]},\"candidate_details\":{\"name\":{\"firstname\":\"ythf\",\"surname\":\"gredfs\"},\"dateofbirth\":\"10/06/1999\",\"gender\":{\"name\":\"Male\",\"id\":\"1\"},\"age_year\":\"4\",\"age_month\":\"2\",\"mother_tounge\":\"English\",\"primary_language\":\"HIndi\",\"languages_known\":\"Hindi\",\"english_year\":\"Yes, 2\",\"ell_support\":\"Yes\",\"psycho_testing\":\"No\",\"learning_disability\":\"No\",\"leraning_support\":\"No\"},\"father_details\":{\"employment_details\":{\"occupation\":\"Doctor\",\"employer_name\":\"Govt.\"},\"name\":{\"firstname\":\"trtfds\",\"surname\":\"gbfdvs\"},\"pan_number\":\"JHHGFJ5786L\"},\"mother_details\":{\"employment_details\":{\"occupation\":\"NA\",\"employer_name\":\"nbgvfd\",\"pincode\":\"400076\",\"city\":\"Powai\",\"address\":\"Mumbai, Hiranandani\"},\"name\":{\"firstname\":\"gbfdv\",\"middlename\":\"gbffds\",\"surname\":\"gfvd\"},\"email\":\"fvd@rgdf.dgf\",\"secondary_phone\":\"9876543256\",\"primary_phone\":\"8976543765\",\"pan_number\":\"jhmgn465k\"},\"qualifying_exam__details\":{\"school_college\":\"bgfdvc\",\"dates_attend\":\"2017-2018\",\"grade\":\"4\",\"address\":\"Powai\",\"language_instruction\":\"English\"},\"sibling_details\":{\"name\":{\"firstname\":\"yjtg\",\"surname\":\"fdgnh\"},\"dateofbirth\":\"05/07/2004\",\"grade\":\"7\",\"gender\":{\"name\":\"Female\",\"id\":\"2\"}},\"address_details\":{\"local_address\":{\"address_line1\":\"Mumbai, Hiranandani\",\"city\":\"Powai\",\"pincode\":\"400076\"}},\"documents\":[{\"id\":1,\"name\":\"Birth Certificate of Student\"},{\"id\":2,\"name\":\"2 Photographs - Student\"}],\"contact_details\":{\"personal_email\":\"iojk@kj.ll\",\"personal_mobile_number\":\"9895632568\"},\"url_configuration_id\":9,\"guardian_details\":{\"candidaterelation\":\"Mother\",\"relation\":\"Father\"},\"general_comment\":\"Instalment\"}";
		OnlineAdmission asdmission = new ObjectMapper().readValue(data, OnlineAdmission.class);
		new AscendPdfBuilder().createPdf(asdmission, "c:/temp/images/ascend.pdf",
						null, null,
						"C:/Temp/images/Qfix/documents/External/ascend.jpg");
	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException {
		PDDocument doc = new PDDocument();
		PDPage page;
		try {
			page = new PDPage();
			doc.addPage(page);
			PDPageContentStream contentStream = new PDPageContentStream(doc, page);
			DrawString drawString = new DrawString();
			contentStream = drawHeader(doc, contentStream, drawString, onlineAdmission, studentPhotoPath, branchLogo);
			contentStream = childContent(doc, contentStream, drawString, onlineAdmission);
			contentStream = parentDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = siblingsDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = academicDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = languageSpoken(doc, contentStream, drawString, onlineAdmission);
			contentStream = documentsSubmit(doc, contentStream, drawString, onlineAdmission);
			contentStream = signatureDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = footerDetails(doc, contentStream, drawString, onlineAdmission);

			contentStream.close();
			doc.save(pdfFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}
		}
	}

}