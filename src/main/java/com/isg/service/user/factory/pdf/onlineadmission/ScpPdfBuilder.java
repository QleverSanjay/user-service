package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.model.onlineadmission.CourseDetails;
import com.isg.service.user.model.onlineadmission.CoursePreference;
import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.util.FileStorePathAssembler;

public class ScpPdfBuilder implements PdfBuilder {
	public float marginX = 60;
	public float previousContentOfY;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 70f;
	int academicYearId, branchId;
	String identifier;
	private static final Logger log = Logger.getLogger(RoseminePdfBuilder.class);

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException {
		// TODO Auto-generated method stub
		createPdf(onlineAdmission, pdfFilePath, null, branchLogo);
	}

	public PDPageContentStream drawHeader(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission,
					String branchLogo) throws IOException {
		Date date = new Date();

		//Drawing logo of Branch 

		if (!StringUtils.isEmpty(branchLogo))
		{
			drawImage(doc, branchLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}
		else {
			drawImage(doc, FileStorePathAssembler.QFIX_LOGO_IMAGE_RELATIVE_PATH, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}

		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, 200, 730, PDType1Font.HELVETICA, 16, "South Calcutta Polytechnic");
		drawString.drawString(contentStream, 200, 710, PDType1Font.HELVETICA, 12, "Atghara, Baruipur, Kolkata-144");

		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, 220, 650, PDType1Font.HELVETICA, 16, "Admission Form");
		contentStream.setNonStrokingColor(Color.BLACK);

		//Application Id  
		drawString.drawString(contentStream, 60, 575, PDType1Font.HELVETICA, 10, "Application Id :");
		drawString.drawString(
						contentStream,
						145,
						575,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getApplicationId()) ? onlineAdmission
										.getApplicationId() : ""));

		drawString.drawString(contentStream, 420, 575, PDType1Font.HELVETICA, 10, "Date :");
		drawString.drawString(contentStream, 455, 575, PDType1Font.HELVETICA, 10, new SimpleDateFormat("dd-MMM-yyyy").format(date));

		previousContentOfY = 560;
		return contentStream;
	}

	public PDPageContentStream childContent(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Personal Details");
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Category Choose");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(onlineAdmission.getQualifyingExamDetails() == null ? ""
										: (onlineAdmission.getQualifyingExamDetails().getExam() != null ?
														onlineAdmission.getQualifyingExamDetails().getExam().getName() : "")));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Student Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getName()) ? !StringUtils
										.isEmpty(onlineAdmission.getCandidateDetails().getName().getFirstname()) ? onlineAdmission.getCandidateDetails()
										.getName().getFirstname().toUpperCase() : ""
										: ""
										: "")
										+ " "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getMiddlename()) ? onlineAdmission.getCandidateDetails().getName().getMiddlename()
														.toUpperCase() : "" : "" : "")
										+ " "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getSurname()) ? onlineAdmission.getCandidateDetails().getName().getSurname().toUpperCase()
														: "" : "" : ""));

		//fathers details
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Father Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");

		String fatherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) && !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName())) {
			Name nameObj = onlineAdmission.getFatherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			fatherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 160,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						fatherName);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Residential Address ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress().getAddressLine1()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress().getAddressLine1()) ? onlineAdmission
										.getAddressDetails()
										.getLocalAddress().getAddressLine1().toUpperCase()
										: ""
										: ""
										: ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Date of Birth ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getDateofbirth()) ? onlineAdmission
						.getCandidateDetails().getDateofbirth() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Mobile Number");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getContactDetails()) ? !StringUtils.isEmpty(onlineAdmission.getContactDetails().getPersonalMobileNumber()) ? onlineAdmission
						.getContactDetails()
						.getPersonalMobileNumber() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email Id ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getContactDetails()) ? !StringUtils.isEmpty(onlineAdmission.getContactDetails().getPersonalEmail()) ? onlineAdmission
						.getContactDetails()
						.getPersonalEmail() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Caste ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getCasteDetails().getCaste()) ? onlineAdmission
						.getCandidateDetails().getCasteDetails().getCaste().getName().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Religion ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getCasteDetails().getReligion()) ? onlineAdmission
						.getCandidateDetails().getCasteDetails().getReligion().getName().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Gender ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getGender()) ? onlineAdmission
						.getCandidateDetails().getGender().getName().toUpperCase() : "" : ""));

		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Course applied for");
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Course Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		String cources = getCourcepreferences(onlineAdmission);
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, cources);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Booking Amount");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (onlineAdmission
						.getCourseDetails() != null ? onlineAdmission.getCourseDetails().getPaymentAmount() + "" : ""));

		//table for ssc & hsc exam details

		return contentStream;
	}

	public String getCourcepreferences(OnlineAdmission onlineAdmission) {
		String cname = "";
		CourseDetails courseDetail = onlineAdmission.getCourseDetails();
		if (courseDetail != null) {
			int i = 0;
			List<CoursePreference> cources = courseDetail.getCoursePreferenceList();
			for (CoursePreference cource : cources) {
				cname += cource.getName();
				cname += (i != cources.size() - 1 ? ", " : "");
				i++;
				if (!StringUtils.isEmpty(onlineAdmission.getCourseDetails().getOtherCourseDetail())) {
					cname += (StringUtils.isEmpty(cname) ? "" : ", ") + onlineAdmission.getCourseDetails().getOtherCourseDetail();
				}
			}
		}
		return cname;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc, boolean addNewPage) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentOfY < 56 || addNewPage) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentOfY = 780;
		}
		return contentStream1;
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}

	public void drawImage(PDDocument doc, String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {

		PDXObjectImage xImage = null;
		InputStream in = null;
		try {
			//System.out.println("Image Path to draw>>>>:::" + path);
			log.debug("Image Path to draw>>>>:::" + path);
			String image = path;
			if (image.toLowerCase().endsWith(".jpg"))
			{
				in = new FileInputStream(image);
				xImage = new PDJpeg(doc, in);
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Image not Found at ::" + path);
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public static void main(String a[]) throws JsonParseException, JsonMappingException, IOException {
		String data = "{\"course_details\":{\"course\":{\"id\":22,\"code\":\"12\",\"name\":\"Cources\",\"active\":false,\"category\":\"DC\"},\"payment_amount\":\"21000\",\"course_preferences\":[{\"id\":5,\"name\":\"Computer Science & Technology\",\"checked\":true}]},\"candidate_details\":{\"name\":{\"firstname\":\"yjhgbfd\",\"surname\":\"hngbfd\"},\"caste_details\":{\"caste\":{\"id\":8,\"identifier\":\"8\",\"code\":\"NT-D\",\"name\":\"NOMADIC TRIBES-D\",\"active\":true},\"religion\":{\"id\":4,\"name\":\"Chistan\"}},\"dateofbirth\":\"01/01/1998\",\"gender\":{\"name\":\"Female\",\"id\":\"2\"}},\"contact_details\":{\"personal_mobile_number\":\"8976543467\",\"personal_email\":\"jhg@trhfg.hgf\"},\"father_details\":{\"name\":{\"firstname\":\"hngfbdv\",\"surname\":\"rtfdv\"}},\"qualifying_exam__details\":{\"exam\":{\"name\":\"Jexpo\",\"id\":\"1\"}},\"address_details\":{\"local_address\":{\"address_line1\":\"jtrctbhym n\"}},\"url_configuration_id\":6,\"academic_details\":[{\"marksobtaine\":{},\"board_university\":\"cbse\",\"passing_year\":\"2006\",\"percentage\":\"78\",\"subject_marks\":{\"physicalSc\":\"78\",\"maths\":\"78\"},\"exam\":\"MADHYAMIK\"},{\"board_university\":\"cbse\",\"passing_year\":\"2009\",\"percentage\":\"78\",\"obtained_marks\":\"96\",\"exam\":\"HIGHER SECONDARY\"},{\"marksobtaine\":{},\"board_university\":\"cbse\",\"passing_year\":\"2006\",\"percentage\":\"78\",\"subject_marks\":{\"physicalSc\":\"78\",\"maths\":\"78\"},\"exam\":\"MADHYAMIK\"},{\"board_university\":\"cbse\",\"passing_year\":\"2009\",\"percentage\":\"78\",\"obtained_marks\":\"96\",\"exam\":\"HIGHER SECONDARY\"}],\"sscexam\":{\"marksobtaine\":{},\"board_university\":\"cbse\",\"passing_year\":\"2006\",\"percentage\":\"78\",\"subject_marks\":{\"physicalSc\":\"78\",\"maths\":\"78\"},\"exam\":\"MADHYAMIK\"},\"hscexam\":{\"board_university\":\"cbse\",\"passing_year\":\"2009\",\"percentage\":\"78\",\"obtained_marks\":\"96\",\"exam\":\"HIGHER SECONDARY\"}}";
		OnlineAdmission asdmission = new ObjectMapper().readValue(data, OnlineAdmission.class);
		new ScpPdfBuilder().createPdf(asdmission, "c:/temp/images/scp.pdf",
						null, null,
						"C:/Temp/images/Qfix/documents/External/r12r12gistra123ion/1/jI7KUGBMPs5w40lsU0eg.jpg");
	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException {
		// TODO Auto-generated method stub
		PDDocument doc = new PDDocument();
		PDPageContentStream contentStream = null;
		PDPage page;
		try {
			System.out.println(pdfFilePath);
			page = new PDPage();
			doc.addPage(page);
			contentStream = new PDPageContentStream(doc, page);
			DrawString drawString = new DrawString();
			contentStream = drawHeader(doc, contentStream, drawString, onlineAdmission, branchLogo);
			contentStream = childContent(doc, contentStream, drawString, onlineAdmission);
			AcademicDetails academicDetails = new AcademicDetails();

			Object[] previousContentYAndContentStream = new Object[2];

			previousContentYAndContentStream[0] = previousContentOfY;
			previousContentYAndContentStream[1] = contentStream;
			academicDetails.setFormFor("scp");
			academicDetails.addAcademicDetails(doc, previousContentYAndContentStream, onlineAdmission);

			List<Float> columnWidth = new ArrayList<Float>();

			columnWidth.add(50f);
			columnWidth.add(50f);
			/*List<List<String>> tableData = new ArrayList<List<String>>();
			List<String> data = new ArrayList<String>();

			data.add("Physical Sc.");
			data.add("Math");
			tableData.add(data);

			data = new ArrayList<String>();

			data.add("80");
			data.add("60");
			tableData.add(data);
			float obtainedMarksXPosition = PDPage.PAGE_SIZE_A4.getUpperRightX() - 145;
			contentStream.drawLine(obtainedMarksXPosition, previousContentOfY - 58, obtainedMarksXPosition + 100, previousContentOfY - 58);
			drawString.drawString(contentStream, obtainedMarksXPosition, previousContentOfY - 68, PDType1Font.HELVETICA, 8, "Obtained Marks");
			contentStream.drawLine(obtainedMarksXPosition + 100, previousContentOfY - 58, obtainedMarksXPosition + 100, previousContentOfY - 70);
			new ChallanTableGenerator().generateTable(contentStream, obtainedMarksXPosition, previousContentOfY - 30, tableData,
							columnWidth);*/
			contentStream.close();
			doc.save(pdfFilePath);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}

			if (contentStream != null) {
				contentStream.close();
			}

		}

	}
}
