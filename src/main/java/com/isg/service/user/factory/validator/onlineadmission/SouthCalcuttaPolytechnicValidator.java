package com.isg.service.user.factory.validator.onlineadmission;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.response.OnlineAdmissionConfiguration;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class SouthCalcuttaPolytechnicValidator extends JdbcDaoSupport implements Validator {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}


	@Override
	public void validate(OnlineAdmission onlineAdmission, OnlineAdmissionConfiguration admissionConfiguration) throws ApplicationException {
		final double paymentAmount = onlineAdmission.getCourseDetails().getPaymentAmount();

		String sql = "select min_amount, max_amount from online_admission_course where branch_id = " + admissionConfiguration.getBranchId() + " AND id = "
						+ onlineAdmission.getCourseDetails().getCourse().getId();

		Map<String, Object> data = getJdbcTemplate().queryForMap(sql);
		double minAmount = 0.0;
		double maxAmount = 0.0;

		if (data.get("min_amount") != null) {
			minAmount = (double) data.get("min_amount");
		}
		if (data.get("max_amount") != null) {
			maxAmount = (double) data.get("max_amount");
		}

		if (minAmount > 0 && paymentAmount < minAmount) {
			throw new ApplicationException("Payment amount must be greater than " + minAmount);
		}
		else if (maxAmount > 0 && paymentAmount > maxAmount) {
			throw new ApplicationException("Payment amount must be less than " + maxAmount);
		}
	}
}