package com.isg.service.user.factory.validator.onlineadmission;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ValidatorFactory {

	private static final Logger log = Logger.getLogger(ValidatorFactory.class);

	@Autowired
	DalmiaValidator dalmiaValidator;

	@Autowired
	SouthCalcuttaPolytechnicValidator southCalcuttaPolytechnicValidator;

	public Validator getPdfBuilderInstance(String templateName) {
		if (templateName.equalsIgnoreCase("dalmia")) {
			return dalmiaValidator;
		} else if (templateName.equalsIgnoreCase("scp")) {
			return southCalcuttaPolytechnicValidator;
		} else {
			log.error("Validator Factory is not configured for template: "
					+ templateName);
		}

		return null;
	}

}