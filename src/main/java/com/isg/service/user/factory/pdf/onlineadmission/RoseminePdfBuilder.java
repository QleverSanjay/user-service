package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.util.StringUtils;

import com.isg.service.user.model.onlineadmission.CourseDetails;
import com.isg.service.user.model.onlineadmission.CoursePreference;
import com.isg.service.user.model.onlineadmission.Location;
import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.util.FileStorePathAssembler;

public class RoseminePdfBuilder implements PdfBuilder {

	public float marginX = 60;
	public float previousContentOfY;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 70f;
	//PDXObjectImage image = null;
	int academicYearId, branchId;
	String identifier;
	private static final Logger log = Logger.getLogger(RoseminePdfBuilder.class);

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException {
		createPdf(onlineAdmission, pdfFilePath, studentPhotoPath, null, branchLogo);
	}

	public PDPageContentStream termsandCondition(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		try {
			contentStream = checkEndOfPage(contentStream, doc, true);
			contentStream.setNonStrokingColor(new Color(100, 125, 250));
			drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Terms and Condition");
			contentStream.setNonStrokingColor(Color.BLACK);
			/*PDXObjectImage image = null;
			image = new PDJpeg(doc, new FileInputStream("D:\\image\\rm.jpg"));
			contentStream.drawXObject(image, 60, 20, 500, 700);
			*/
			//draw rectangle for terms and conditions page
			String tncImgPath = FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH
							+ FileStorePathAssembler.QFIX_ONLINE_REGISTRATION_RELATIVE_PATH + FileStorePathAssembler.PATH_SEPARATOR
							+ onlineAdmission.getBranch().getId() + FileStorePathAssembler.PATH_SEPARATOR
							+ "roseminetnc.jpg";

			System.out.println("Terms and conditions image path >>>>>" + tncImgPath);
			log.debug("Terms and conditions image path >>>>>" + tncImgPath);

			drawImage(doc, tncImgPath, 60, 20, 500, 700, contentStream);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return contentStream;

	}

	public static void main(String a[]) {
		System.out.println(FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH
						+ FileStorePathAssembler.QFIX_ONLINE_REGISTRATION_RELATIVE_PATH + FileStorePathAssembler.PATH_SEPARATOR
						+ "1" + FileStorePathAssembler.PATH_SEPARATOR
						+ "roseminetnc.png");
	}

	public PDPageContentStream drawHeader(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission,
					String studentPhotoPath, String signaturePhotoPath, String branchLogo) throws IOException {
		Date date = new Date();

		//Drawing logo of Branch 

		if (!StringUtils.isEmpty(branchLogo))
		{
			drawImage(doc, branchLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 50,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 150, 100, contentStream);
		}
		else {
			drawImage(doc, FileStorePathAssembler.QFIX_LOGO_IMAGE_RELATIVE_PATH, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 50,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 150, 100, contentStream);
		}


		//draw rectangle
		//contentStream.addRect((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 153, (PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 171, 124,123);

		/*	if (!StringUtils.isEmpty(studentPhotoPath)) {
				drawImage(doc, studentPhotoPath, (PDPage.PAGE_SIZE_A4.getUpperRightX() - 50), previousContentOfY, 100, 100, contentStream);
			}*/
		if (studentPhotoPath != null && !StringUtils.isEmpty(studentPhotoPath))
		{
			//draw rectangle
			contentStream.addRect((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 153, (PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 171,
							124,
							123);
			drawImage(doc, studentPhotoPath, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 153, (PDPage.PAGE_SIZE_A4.getUpperRightY())
							- PADDING_BOTTOM_OF_DOCUMENT - 171, 124, 123, contentStream);
		}

		if (signaturePhotoPath != null && !StringUtils.isEmpty(signaturePhotoPath))
		{
			drawImage(doc, signaturePhotoPath, 55, (PDPage.PAGE_SIZE_A4.getUpperRightY())
							- PADDING_BOTTOM_OF_DOCUMENT - 204, 124, 123, contentStream);
		}
		//drawString.drawString(contentStream, 480, 650, PDType1Font.HELVETICA, 10, "Child Photo Here");

		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, 220, 650, PDType1Font.HELVETICA, 16, "Registration Form");
		contentStream.setNonStrokingColor(Color.BLACK);
		//drawString.drawString(contentStream, 60, 575, PDType1Font.HELVETICA, 10, "Form No :-");

		//drawString.drawString(contentStream, 120, 575, PDType1Font.HELVETICA, 10, onlineAdmission.getFormNumber());
		contentStream.drawLine(60, 565, 180, 565);

		drawString.drawString(contentStream, 420, 575, PDType1Font.HELVETICA, 10, "Date :-");
		drawString.drawString(contentStream, 455, 575, PDType1Font.HELVETICA, 10, new SimpleDateFormat("dd-MMM-yyyy").format(date));
		contentStream.drawLine(415, 565, 575, 565);

		previousContentOfY = 560;
		return contentStream;
	}

	public PDPageContentStream childContent(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Student Details");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Student Name :-");
		drawString.drawString(
						contentStream,
						marginX + 75,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getName()) ? !StringUtils
										.isEmpty(onlineAdmission.getCandidateDetails().getName().getFirstname()) ? onlineAdmission.getCandidateDetails()
										.getName().getFirstname().toUpperCase() : ""
										: ""
										: "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getMiddlename()) ? onlineAdmission.getCandidateDetails().getName().getMiddlename()
														.toUpperCase() : "" : "" : "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getSurname()) ? onlineAdmission.getCandidateDetails().getName().getSurname().toUpperCase()
														: "" : "" : ""));
		drawString.drawString(contentStream, marginX + 340, previousContentOfY, PDType1Font.HELVETICA, 10, "Date of Birth :-");
		drawString.drawString(contentStream, marginX + 410, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getDateofbirth()) ? onlineAdmission
						.getCandidateDetails().getDateofbirth() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Gender :-");
		drawString.drawString(contentStream, marginX + 50, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getGender()) ? onlineAdmission
						.getCandidateDetails().getGender().getName().toUpperCase() : "" : ""));
		drawString.drawString(contentStream, marginX + 340, previousContentOfY, PDType1Font.HELVETICA, 10, "Nationality :-");
		drawString.drawString(contentStream, marginX + 410, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getNationality()) ? onlineAdmission
						.getCandidateDetails().getNationality() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Educational Qualifications :-");
		drawString.drawString(contentStream, marginX + 130, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getEducationalqualifications()) ? onlineAdmission
						.getCandidateDetails().getEducationalqualifications().getName() : "" : ""));

		drawString.drawString(contentStream, marginX + 340, previousContentOfY, PDType1Font.HELVETICA, 10, "Category :-");
		drawString.drawString(contentStream, marginX + 410, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getCategory()) ? onlineAdmission
						.getCandidateDetails().getCategory().getName() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email Id :-");
		drawString.drawString(contentStream, marginX + 60, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmail()) ? onlineAdmission.getFatherDetails()
						.getEmail() : "" : ""));

		return contentStream;
	}

	public String getLocations(OnlineAdmission onlineAdmission) {
		String name = "";
		if (onlineAdmission.getCandidateDetails() != null) {
			int i = 0;
			List<Location> locations = onlineAdmission.getCandidateDetails().getLocation();
			if (locations != null && !locations.isEmpty()) {
				for (Location location : locations) {
					name += location.getName() + (i != locations.size() - 1 ? ", " : "");
					i++;
				}
			}
			if (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getOtherLocation())) {
				name += (StringUtils.isEmpty(name) ? "" : ", ") + onlineAdmission.getCandidateDetails().getOtherLocation();
			}
		}
		System.out.println("Locations ::: " + name);
		return name;
	}

	public String getCourcepreferences(OnlineAdmission onlineAdmission) {
		String cname = "";
		CourseDetails courseDetail = onlineAdmission.getCourseDetails();
		if (courseDetail != null) {
			int i = 0;
			List<CoursePreference> cources = courseDetail.getCoursePreferenceList();
			if (cources != null && !cources.isEmpty()) {
				Map<String, String> otherCourses = courseDetail.getOtherCoursePreference();
				for (CoursePreference cource : cources) {
					cname += cource.getName();
					if (otherCourses != null && !otherCourses.isEmpty()
									&& courseDetail.getCourse().getName().equalsIgnoreCase("Educational")) {

						boolean flag = false;
						String str1 = null;
						String str2 = null;
						if (cource.getName().equalsIgnoreCase("b.tech")) {
							str1 = otherCourses.get("btech1");
							str2 = otherCourses.get("btech2");
							flag = true;
						}
						if (cource.getName().equalsIgnoreCase("m.tech")) {
							str1 = otherCourses.get("mtech1");
							str2 = otherCourses.get("mtech2");
							flag = true;
						}
						if (cource.getName().equalsIgnoreCase("Polytechnic")) {
							str1 = otherCourses.get("poly1");
							str2 = otherCourses.get("poly2");
							flag = true;
						}
						if (flag) {
							str1 = str1 == null ? "" : str1;
							str2 = str2 == null ? "" : (str1 == null ? "" : ", ") + str2;
							cname += (StringUtils.isEmpty(cname) ? "" : "[" + str1 + str2 + "]");
						}
					}
					cname += (i != cources.size() - 1 ? ", " : "");
					i++;
				}
			}
			if (!StringUtils.isEmpty(onlineAdmission.getCourseDetails().getOtherCourseDetail())) {
				cname += (StringUtils.isEmpty(cname) ? "" : ", ") + onlineAdmission.getCourseDetails().getOtherCourseDetail();
			}
		}

		System.out.println("Cources ::: " + cname);
		return cname;
	}

	public PDPageContentStream locationDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 20;
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Choice of location for your higher education");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "City:-");
		String locations = getLocations(onlineAdmission);
		drawString.drawString(contentStream, marginX + 40, previousContentOfY, PDType1Font.HELVETICA, 10, locations);

		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Choice of course for your higher education");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Course:-");
		drawString.drawString(contentStream, marginX + 50, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCourseDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCourseDetails().getCourse()) ? onlineAdmission
						.getCourseDetails().getCourse().getName() : "" : ""));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Course Preference:-");
		String cources = getCourcepreferences(onlineAdmission);
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, cources);

		previousContentOfY -= 5;
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "How did you hear about us :-");
		drawString.drawString(contentStream, marginX + 135, previousContentOfY, PDType1Font.HELVETICA, 10,
						(!StringUtils.isEmpty(onlineAdmission.getGeneralComment()) ? onlineAdmission.getGeneralComment().toUpperCase() : ""));
		return contentStream;
	}

	public PDPageContentStream parentsDetail(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {

		previousContentOfY -= 10;
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Parent Details");
		contentStream.setNonStrokingColor(Color.BLACK);

		//fathers details
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Father Name :-");

		String fatherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) && !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName())) {
			Name nameObj = onlineAdmission.getFatherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			fatherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 90,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						fatherName);

		//mother details
		drawString.drawString(contentStream, marginX + 300, previousContentOfY, PDType1Font.HELVETICA, 10, "Mother Name :-");

		String motherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) && !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getName())) {
			Name nameObj = onlineAdmission.getMotherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			motherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 380,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						motherName);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Fathers Phone :-");
		drawString.drawString(contentStream, marginX + 90, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getPrimaryPhone()) ? onlineAdmission.getFatherDetails()
						.getPrimaryPhone() : "" : ""));

		//mothers no.
		drawString.drawString(contentStream, marginX + 300, previousContentOfY, PDType1Font.HELVETICA, 10, "Mothers Phone :-");
		drawString.drawString(contentStream, marginX + 380, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getPrimaryPhone()) ? onlineAdmission.getMotherDetails()
						.getPrimaryPhone() : "" : ""));

		//contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		/*//mothers email
		drawString.drawString(contentStream, marginX + 300, previousContentOfY, PDType1Font.HELVETICA, 10, "Mother Email  :-");
		drawString.drawString(contentStream, marginX + 380, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmail()) ? onlineAdmission.getMotherDetails()
						.getEmail() : "" : ""));
		*/
		//contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Parents Occupation :-");
		drawString.drawString(
						contentStream,
						marginX + 105,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));

		drawString.drawString(contentStream, marginX + 300, previousContentOfY, PDType1Font.HELVETICA, 10, "Parents Annual Income :-");
		drawString.drawString(
						contentStream,
						marginX + 420,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getAnnualIncome()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getAnnualIncome().toUpperCase() : ""
										: "" : ""));

		/*drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Office Address :-");
		drawString.drawString(
						contentStream,
						marginX + 90,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails().getAddress()) ? onlineAdmission
										.getFatherDetails().getEmploymentDetails().getAddress().toUpperCase()
										: ""
										: ""
										: ""));
		*/
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Address :-");
		drawString.drawString(
						contentStream,
						marginX + 70,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress().getAddressLine1()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress().getAddressLine1()) ? onlineAdmission
										.getAddressDetails()
										.getLocalAddress().getAddressLine1().toUpperCase()
										: ""
										: ""
										: ""));

		return contentStream;
	}

	public PDPageContentStream admissionProcess(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		contentStream = checkEndOfPage(contentStream, doc, true);
		previousContentOfY -= 10;
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Admission Process");
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc, false);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"a). Candidates should fill the form and submit to RMECT office.");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"b). Candidates should select atleast 1 state as a preference for higher studies.");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"c). State wise and branch wise application forms will be prepared by a committee after proper scanning and matching");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10, "the bar code.");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"d). Students selected for scholarships will receive a welcome kit,which denotes the confirmation of acceptance of form.");
		drawString.drawString(
						contentStream,
						marginX,
						previousContentOfY -= 20,
						PDType1Font.HELVETICA,
						10,
						"e). After the confirmation of choice of branch, state, institution and final consent, the admission process will start");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10, "and students will be informed about the same.");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"f). Students will be advised to collect their admission Letter and relevent documents received by RoseMine office.");
		drawString.drawString(
						contentStream,
						marginX,
						previousContentOfY -= 20,
						PDType1Font.HELVETICA,
						10,
						"g). Students will have to submit the cost of their first semester hostel,fooding,exam fee and other miscellaneous");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10,
						"expenses and required documents in time before receiving the admission letter. Otherwise this letter will be treated");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10,
						"as incomplete and invalid.");

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"h). Student has to report to the institution and must SMS RoseMine when they report to the college campus within");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10,
						"time frame.");

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"i). Once the process is completed, the student must report their results semester wise time to time to the scholarship");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, " provider-RMECT.");
		contentStream = checkEndOfPage(contentStream, doc, false);
		return contentStream;
	}

	public PDPageContentStream listofenclosures(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "List of Enclosures");
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc, false);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"a). High School Marksheet High School Certificate");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"b). Intermediate Marksheet Intermediate Certificate");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"c). Graduation Marksheet Graduation Certificate");

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"d). Cast Certificate Domicile/Residence Certificate");
		drawString.drawString(
						contentStream,
						marginX,
						previousContentOfY -= 20,
						PDType1Font.HELVETICA,
						10,
						"e). TC/MigrationCertificate Photos (10 Latest Passport sizephotos)");
		return contentStream;
	}

	public PDPageContentStream instructions(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Instructions");
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc, false);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"a). Enclose 3 sets of self attested photocopies of all certificates/marksheets for proper verification by the institution.");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"b). Enclose Certificates of College/University for any extra curricular activities/sports/hobbies.");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"c). Students eligible for scholarships should submit original caste certificate,Income certificate and Domicile/Residence ");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "certificate provider-RMECT.");
		drawString.drawString(
						contentStream,
						marginX + 20,
						previousContentOfY -= 40,
						PDType1Font.HELVETICA,
						10,
						"I hereby declare that I have read and understood the terms and conditions,and the information provided in this form ");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10,
						"is true to the best of my knowledge and belief.I have filled up the form properly with the consent of my parents and ");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10,
						"submitting it through post to the RoseMine office,Patna-Bihar.Incase the details provided by me are found incorrect,");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10,
						"misleading or not in accordance with the requirement of the course and institution,my admission/scholarship process");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10,
						"may be cancelled and no further claim will be made by me.");
		return contentStream;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc, boolean addNewPage) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentOfY < 56 || addNewPage) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentOfY = 780;
		}
		return contentStream1;
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}

	public void drawImage(PDDocument doc, String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {

		PDXObjectImage xImage = null;
		InputStream in = null;
		try {
			//System.out.println("Image Path to draw>>>>:::" + path);
			log.debug("Image Path to draw>>>>:::" + path);
			String image = path;
			if (image.toLowerCase().endsWith(".jpg"))
			{
				in = new FileInputStream(image);
				xImage = new PDJpeg(doc, in);
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Image not Found at ::" + path);
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException {
		PDDocument doc = new PDDocument();
		PDPageContentStream contentStream = null;
		PDPage page;
		try {
			System.out.println(pdfFilePath);
			page = new PDPage();
			doc.addPage(page);
			contentStream = new PDPageContentStream(doc, page);
			DrawString drawString = new DrawString();
			contentStream = drawHeader(doc, contentStream, drawString, onlineAdmission, studentPhotoPath, signaturePhotoPath, branchLogo);
			contentStream = childContent(doc, contentStream, drawString, onlineAdmission);
			contentStream = parentsDetail(doc, contentStream, drawString, onlineAdmission);
			contentStream = locationDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = admissionProcess(doc, contentStream, drawString, onlineAdmission);
			contentStream = listofenclosures(doc, contentStream, drawString, onlineAdmission);
			contentStream = instructions(doc, contentStream, drawString, onlineAdmission);
			contentStream = termsandCondition(doc, contentStream, drawString, onlineAdmission);
			contentStream.close();
			doc.save(pdfFilePath);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}

			if (contentStream != null) {
				contentStream.close();
			}

		}
	}

}
