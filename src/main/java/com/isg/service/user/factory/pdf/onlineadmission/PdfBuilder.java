package com.isg.service.user.factory.pdf.onlineadmission;

import java.io.IOException;

import com.isg.service.user.model.onlineadmission.OnlineAdmission;

public interface PdfBuilder {
	void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException;

	void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException;

}
