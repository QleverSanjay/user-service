package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.util.StringUtils;

import com.isg.service.user.model.onlineadmission.OnlineAdmission;

public class ApplicationContent {

	/**
	 * @param args
	 * @throws IOException
	 */
	public float previousContentY = 515; //here value of previousContentY is reduced because of the rectangle draw in upper direction
	private final float X0 = 40f;
	private final float RECTANGLE_WIDTH = 535;
	private final float ROW_WIDTH = 535;
	private final float RECTANGLE_HEIGHT = 12;
	private final float marginX = 5;
	private final float CONTENT_FONT_SIZE = 9f;

	public PDDocument addApllicationDetails(PDDocument doc, OnlineAdmission onlineAdmission, String branchLogo) throws Exception {

		PDPage page = addNewPage(doc);

		PDPageContentStream contentStream = new PDPageContentStream(doc, page, true, true);

		Object[] previousContentYAndContentStream = new Object[2];

		previousContentYAndContentStream[0] = previousContentY;
		previousContentYAndContentStream[1] = contentStream;

		ParentsAndGardianDetails parentsAndGardianDetails = new ParentsAndGardianDetails();
		AcademicDetails academicDetails = new AcademicDetails();
		QualifyingExamDetail qualifyingExamDetail = new QualifyingExamDetail();
		DocumentsSubmittedForAdmission documentsSubmittedForAdmission = new DocumentsSubmittedForAdmission();
		SubjectsOffered subjectsOffered = new SubjectsOffered();

		previousContentYAndContentStream = addPersonalDetails(doc, previousContentYAndContentStream, onlineAdmission);
		previousContentYAndContentStream = addContactDetails(doc, previousContentYAndContentStream, onlineAdmission);
		previousContentYAndContentStream = parentsAndGardianDetails.addParentsAndGuardianDetails(doc, previousContentYAndContentStream, onlineAdmission);

		/*		if (onlineAdmission.getCourseDetails().getCourse().getId() != 7) {
					page = new PDPage();
					doc.addPage(page);
				}
		*/
		previousContentYAndContentStream = (onlineAdmission.getAcademicDetails() == null) ? previousContentYAndContentStream : academicDetails
						.addAcademicDetails(doc, previousContentYAndContentStream, onlineAdmission);
		previousContentYAndContentStream = (onlineAdmission.getQualifyingExamDetails() == null) ? previousContentYAndContentStream : qualifyingExamDetail
						.addQaulifyingExamDetails(doc, previousContentYAndContentStream, onlineAdmission);
		previousContentYAndContentStream = (onlineAdmission.getDocuments() == null) ? previousContentYAndContentStream : documentsSubmittedForAdmission
						.addDocumentsSubmittedForAdmission(doc, previousContentYAndContentStream,
										onlineAdmission);
		System.out.println("Value for courseDetails courseID :: " + onlineAdmission.getCourseDetails().getCourse().getId());
		if (onlineAdmission.getAcademicDetails() != null) {
			if (onlineAdmission.getCourseDetails().getCourse().getId() == 7) {
				previousContentYAndContentStream = subjectsOffered.addSubjectsOffered(doc, previousContentYAndContentStream, onlineAdmission);
			}
		}

		TermsAndCondition termsAndCondition = new TermsAndCondition();
		termsAndCondition.termsAndConditionFirstPage(doc, onlineAdmission);

		//		TAndCFinalPage tAndCFinalPage = new TAndCFinalPage();
		//		tAndCFinalPage.termsAndConditionFinalPage(doc, onlineAdmission, branchLogo);
		return doc;
	}

	public Object[] addPersonalDetails(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission) throws Exception {

		/*boolean addressDetailsflag; 
		boolean contactDetailsFlag;
		boolean candidateDetailsFlag;
		boolean academicDetailsFlag;
		boolean branchDetailsFlag;
		boolean fatherDetailsFlag;
		boolean motherDetailsFlag;
		boolean qualifyingExamDetailsFlag;
		if(onlineAdmission.getAddressDetails() == null){
			addressDetailsflag = false;
		}
		if(onlineAdmission.getContactDetails() == null){
			contactDetailsFlag = false;
		}
		if(onlineAdmission.getCandidateDetails() == null){
			candidateDetailsFlag = false;
		}
		if(onlineAdmission.getAcademicDetails() == null){
			academicDetailsFlag = false;
		}
		if(onlineAdmission.getDocuments() == null){
			branchDetailsFlag = false;
		}
		if(onlineAdmission.getFatherDetails() == null){
			fatherDetailsFlag = false;
		}
		if(onlineAdmission.getMotherDetails() == null){
			motherDetailsFlag = false;
		}
		if(onlineAdmission.getQualifyingExamDetails() == null){
			qualifyingExamDetailsFlag = false;
		}*/
		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];
		previousContentY = (float) previousContenyYAndContentStream[0];
		contentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		//System.out.println("Inside of the personal details");

		/******************* to draw personal details text ******************/
		contentStream.fillRect(X0 + marginX, previousContentY, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY += 3);
		contentStream.drawString("Personal Details");
		contentStream.endText();
		contentStream.drawLine(X0 + marginX, previousContentY -= 3, X0 + marginX + ROW_WIDTH, previousContentY);

		/******************* to draw personal details header ******************/
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		/*List<String> personalDetailHeaderList = getPersonalHeaderList();					//This logic is for dynamic name header creation
		for(String personalHeader : personalDetailHeaderList){
			*/
		contentStream.moveTextPositionByAmount(X0 + 163, previousContentY -= 12);
		contentStream.drawString("SURNAME");
		contentStream.endText();
		/*}*/

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 300, previousContentY);
		contentStream.drawString("FIRST NAME");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 430, previousContentY);
		contentStream.drawString("MIDDLE NAME");
		contentStream.endText();

		/****************** to draw content of Personal Details ************/

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("1.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Candidate Name");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getName() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getName().getSurname() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getName().getSurname().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 300, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getName() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getName().getFirstname() == null) ? " " : onlineAdmission.getCandidateDetails()
										.getName().getFirstname().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 430, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getName() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getName().getMiddlename() == null) ? " " : onlineAdmission.getCandidateDetails()
										.getName().getMiddlename().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("2.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Father's Name");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getFatherDetails() == null) ? " " : (onlineAdmission.getFatherDetails().getName() == null) ? " "
						: (onlineAdmission.getFatherDetails().getName().getSurname() == null) ? " " : ":  "
										+ onlineAdmission.getFatherDetails().getName().getSurname().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 300, previousContentY);
		contentStream.drawString((onlineAdmission.getFatherDetails() == null) ? " " : (onlineAdmission.getFatherDetails().getName() == null) ? " "
						: (onlineAdmission.getFatherDetails().getName().getFirstname() == null) ? " " : onlineAdmission.getFatherDetails().getName()
										.getFirstname().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 430, previousContentY);
		contentStream.drawString((onlineAdmission.getFatherDetails() == null) ? " " : (onlineAdmission.getFatherDetails().getName() == null) ? " "
						: (onlineAdmission.getFatherDetails().getName().getMiddlename() == null) ? " " : onlineAdmission.getFatherDetails().getName()
										.getMiddlename().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("3.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Mother's Name");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getMotherDetails() == null) ? " " : (onlineAdmission.getMotherDetails().getName() == null) ? " "
						: (onlineAdmission.getMotherDetails().getName().getFirstname() == null) ? " " : ":  "
										+ onlineAdmission.getMotherDetails().getName().getFirstname().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("4.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Name as on MarkSheet");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getPrintedNameOnMarksheet() == null) ? " " : (onlineAdmission.getCandidateDetails()
										.getPrintedNameOnMarksheet().getSurname() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getPrintedNameOnMarksheet().getSurname().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 300, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getPrintedNameOnMarksheet() == null) ? " " : (onlineAdmission.getCandidateDetails()
										.getPrintedNameOnMarksheet().getFirstname() == null) ? " " : onlineAdmission
										.getCandidateDetails().getPrintedNameOnMarksheet().getFirstname().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 430, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getPrintedNameOnMarksheet() == null) ? " " : onlineAdmission.getCandidateDetails()
										.getPrintedNameOnMarksheet().getMiddlename() == null ? " " : onlineAdmission
										.getCandidateDetails().getPrintedNameOnMarksheet().getMiddlename().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("5.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Married / Unmarried");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getMaritalStatus() == null) ? " " : (onlineAdmission.getCandidateDetails().getMaritalStatus()
										.getName() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getMaritalStatus().getName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("6.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Male / Female");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getGender() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getGender().getName() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getGender().getName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("7.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Foreign Student");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getForeignStudent() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getForeignStudent().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("8.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Mother Toungue");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getMotherTounge() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getMotherTounge().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("9.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Aadhar Card No");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getAadhaaarCard() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getAadhaaarCard().toString());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("10.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Bank Name");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getBankDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getBankDetails().getBankName() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getBankDetails().getBankName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("11.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Bank Account No.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getBankDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getBankDetails().getBankAccount() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getBankDetails().getBankAccount().toString());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("12.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Transaction Type");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getBankDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getBankDetails().getTransactionType() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getBankDetails().getTransactionType().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("13.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Local Address");
		contentStream.endText();

		int addressLineCounter = 0;
		String addressLine = (onlineAdmission.getAddressDetails() == null) ? " " : (onlineAdmission.getAddressDetails().getLocalAddress() == null) ? " "
						: (onlineAdmission.getAddressDetails().getLocalAddress().getAddressLine1() == null) ? " " : ":  "
										+ onlineAdmission.getAddressDetails().getLocalAddress().getAddressLine1().toUpperCase();

		List<String> addressLineTextList = TextFormatter.stringSplit(addressLine, 75, 0);

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		for (String addressLineText : addressLineTextList) {
			contentStream.drawString(addressLineText);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -16);
			addressLineCounter++;
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= (16 * (addressLineCounter = (addressLineCounter != 0) ? addressLineCounter : 1))); //// reduce - 16 if any changes is done
		contentStream.drawString("   HOUSE NO    ");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 205, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " " : (onlineAdmission.getAddressDetails().getLocalAddress() == null) ? " "
						: (onlineAdmission.getAddressDetails().getLocalAddress().getHouseNumber() == null) ? " " : "  :   "
										+ onlineAdmission.getAddressDetails().getLocalAddress().getHouseNumber().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= 16);
		contentStream.drawString("   AREA/LOCATION   :");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 240, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " " : (onlineAdmission.getAddressDetails().getLocalAddress() == null) ? " "
						: (onlineAdmission.getAddressDetails().getLocalAddress().getArea() == null) ? " " : onlineAdmission.getAddressDetails()
										.getLocalAddress().getArea().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= 16);
		contentStream.drawString("   CITY/DISTRICT   :");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 230, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " "
						: (onlineAdmission.getAddressDetails().getLocalAddress() == null) ? " "
										: (onlineAdmission.getAddressDetails().getLocalAddress().getDistrict() == null) ? " "
														: (onlineAdmission.getAddressDetails().getLocalAddress().getDistrict().getName() == null) ? (onlineAdmission
																		.getAddressDetails().getLocalAddress().getDistrict().getOther() == null) ? " "
																		: onlineAdmission.getAddressDetails().getLocalAddress().getDistrict().getOther()
																						.toUpperCase()
																		: onlineAdmission.getAddressDetails().getLocalAddress().getDistrict().getName()
																						.equals("OTHER") ? (onlineAdmission.getAddressDetails()
																						.getLocalAddress().getDistrict().getOther() == null) ? " "
																						: onlineAdmission.getAddressDetails().getPermanentAddress()
																										.getDistrict().getOther().toUpperCase()
																						: onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()
																										.getName().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= 16);
		contentStream.drawString("   STATE   :");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 200, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " "
						: (onlineAdmission.getAddressDetails().getLocalAddress() == null) ? " "
										: (onlineAdmission.getAddressDetails().getLocalAddress().getState() == null) ? " "
														: (onlineAdmission.getAddressDetails().getLocalAddress().getState().getName().equals("OTHER")) ? (onlineAdmission
																		.getAddressDetails().getLocalAddress().getState().getOther().toUpperCase())
																		: onlineAdmission.getAddressDetails().getLocalAddress().getState().getName()
																						.toUpperCase()
						);
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= 16);
		contentStream.drawString("   COUNTRY   :");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 212, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " "
						: (onlineAdmission.getAddressDetails().getLocalAddress() == null) ? " "
										: (onlineAdmission.getAddressDetails().getLocalAddress().getCountry() == null) ? " "
														: (onlineAdmission.getAddressDetails().getLocalAddress().getCountry().getName().equals("OTHER")) ? " "
																		+ onlineAdmission.getAddressDetails().getLocalAddress().getCountry().getOther()
																						.toUpperCase()
																		: " "
																						+ onlineAdmission.getAddressDetails().getLocalAddress().getCountry()
																										.getName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("14.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Permanent Address");
		contentStream.endText();

		String addressLine1 = (onlineAdmission.getAddressDetails() == null) ? " " : (onlineAdmission.getAddressDetails().getPermanentAddress() == null) ? " "
						: (onlineAdmission.getAddressDetails().getPermanentAddress().getAddressLine1() == null) ? " " : ":  "
										+ onlineAdmission.getAddressDetails().getPermanentAddress().getAddressLine1().toUpperCase();

		List<String> addressLineTextList1 = TextFormatter.stringSplit(addressLine1, 75, 0);

		addressLineCounter = 0;
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		for (String addressLineText : addressLineTextList1) {
			contentStream.drawString(addressLineText);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -16);
			addressLineCounter++;
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= (16 * (addressLineCounter = (addressLineCounter != 0) ? addressLineCounter : 1))); // reduce - 16 if any changes is done
		contentStream.drawString("   HOUSE NO    ");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 205, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " "
						: (onlineAdmission.getAddressDetails().getPermanentAddress() == null) ? " " : (onlineAdmission.getAddressDetails()
										.getPermanentAddress().getHouseNumber() == null) ? " " : "  :   "
										+ onlineAdmission.getAddressDetails().getPermanentAddress().getHouseNumber().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= 16);
		contentStream.drawString("   AREA/LOCATION   :");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 240, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " "
						: (onlineAdmission.getAddressDetails().getPermanentAddress() == null) ? " " : (onlineAdmission.getAddressDetails()
										.getPermanentAddress().getArea() == null) ? " " : onlineAdmission.getAddressDetails()
										.getPermanentAddress().getArea().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= 16);
		contentStream.drawString("   CITY/DISTRICT   :");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 230, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " "
						: (onlineAdmission.getAddressDetails().getPermanentAddress() == null) ? " "
										: (onlineAdmission.getAddressDetails().getPermanentAddress().getDistrict() == null) ? " "
														: (onlineAdmission.getAddressDetails().getPermanentAddress().getDistrict().getName() == null) ? (onlineAdmission
																		.getAddressDetails().getPermanentAddress().getDistrict().getOther() == null) ? " "
																		: onlineAdmission.getAddressDetails().getPermanentAddress().getDistrict().getOther()
																						.toUpperCase()
																		: onlineAdmission.getAddressDetails().getPermanentAddress().getDistrict().getName()
																						.equals("OTHER") ? (onlineAdmission.getAddressDetails()
																						.getPermanentAddress().getDistrict().getOther() == null) ? " "
																						: onlineAdmission.getAddressDetails().getPermanentAddress()
																										.getDistrict().getOther().toUpperCase()
																						: onlineAdmission.getAddressDetails().getPermanentAddress()
																										.getDistrict().getName().toUpperCase());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= 16);
		contentStream.drawString("   STATE   :");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 200, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " "
						: (onlineAdmission.getAddressDetails().getPermanentAddress() == null) ? " "
										: (onlineAdmission.getAddressDetails().getPermanentAddress().getState() == null) ? " "
														: (onlineAdmission.getAddressDetails().getPermanentAddress().getState().getName().equals("OTHER")) ? (onlineAdmission
																		.getAddressDetails().getPermanentAddress().getState().getOther().toUpperCase())
																		: onlineAdmission.getAddressDetails().getPermanentAddress().getState().getName()
																						.toUpperCase()
						);
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY -= 16);
		contentStream.drawString("   COUNTRY   :");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 212, previousContentY);
		contentStream.drawString((onlineAdmission.getAddressDetails() == null) ? " "
						: (onlineAdmission.getAddressDetails().getPermanentAddress() == null) ? " "
										: (onlineAdmission.getAddressDetails().getPermanentAddress().getCountry() == null) ? " "
														: (onlineAdmission.getAddressDetails().getPermanentAddress().getCountry().getName().equals("OTHER")) ? " "
																		+ onlineAdmission.getAddressDetails().getPermanentAddress().getCountry().getOther()
																						.toUpperCase()
																		: " "
																						+ onlineAdmission.getAddressDetails().getPermanentAddress()
																										.getCountry().getName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("15.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Place of Birth");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getPlaceofbirth() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getPlaceofbirth().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("16.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Date of Birth");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		String dateOfBirth = (onlineAdmission.getCandidateDetails() == null) ? "" : (onlineAdmission.getCandidateDetails().getDateofbirth() == null) ? " " :
						onlineAdmission.getCandidateDetails().getDateofbirth();
		//System.out.println("Date Format of Date of Birth ::"+dateOfBirth);
		//SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		//Date date = dateFormat.parse(dateOfBirth);
		if (!StringUtils.isEmpty(dateOfBirth)) {
			try {
				Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(dateOfBirth.replaceAll("Z$", "+0000"));
				contentStream.drawString(":  " + new SimpleDateFormat("dd/MM/yyyy").format(date));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("17.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Category");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getCasteDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getCasteDetails()
										.getCaste() == null) ? " "
										: (onlineAdmission.getCandidateDetails().getCasteDetails().getCaste().getName() == null) ? " " : ":  "
														+ onlineAdmission.getCandidateDetails().getCasteDetails().getCaste().getName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("18.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Caste");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getCasteDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getCasteDetails()
										.getSubcaste() == null) ? " "
										: (onlineAdmission.getCandidateDetails().getCasteDetails().getSubcaste().getName() == null) ? " " : ":  "
														+ onlineAdmission.getCandidateDetails().getCasteDetails().getSubcaste().getName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("19.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Religion");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getCasteDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getCasteDetails()
										.getReligion() == null) ? " "
										: (onlineAdmission.getCandidateDetails().getCasteDetails().getReligion().getName() == null) ? " " : ":  "
														+ onlineAdmission.getCandidateDetails().getCasteDetails().getReligion().getName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("20.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Minority");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getCasteDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getCasteDetails()
										.getMinority() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getCasteDetails().getMinority().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("21.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Phy. Handicapped");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getPhysicallyHandicapped() == null) ? " " : (onlineAdmission.getCandidateDetails()
										.getPhysicallyHandicapped().getName() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getPhysicallyHandicapped().getName().toUpperCase());
		contentStream.endText();

		/*contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("22.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Last School/College");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getLastSchoolName() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getLastSchoolName().toUpperCase());
		contentStream.endText();*/

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("22.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Social Reservation");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getSocialReservation() == null) ? " " : (onlineAdmission.getCandidateDetails()
										.getSocialReservation().getName() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getSocialReservation().getName().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("23.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Identification Mark");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getIdentificationMark() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getIdentificationMark().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("24.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Blood Group");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getBloodGroup() == null) ? " "
						: ":  "
										+ onlineAdmission.getCandidateDetails().getBloodGroup().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("25.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Hobbies");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getHobbies() == null) ? " "
						: ":  "
										+ onlineAdmission.getCandidateDetails().getHobbies().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("26.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Employed Status");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		if (onlineAdmission.getCandidateDetails().getEmploymentDetails() != null) {
			contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
							: (onlineAdmission.getCandidateDetails().getEmploymentDetails() == null) ? " " : (onlineAdmission.getCandidateDetails()
											.getEmploymentDetails().getDesignation() == null) ? "NO" : ":  YES");
		} else {
			contentStream.drawString(": NO");
		}

		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("27.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Designation");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);

		if ((onlineAdmission.getCandidateDetails() != null)) {
			if (onlineAdmission.getCandidateDetails().getEmploymentDetails() != null) {
				contentStream.drawString((onlineAdmission.getCandidateDetails().getEmploymentDetails().getDesignation() == null) ? " " : ":  "
								+ onlineAdmission.getCandidateDetails().getEmploymentDetails().getDesignation().toUpperCase());
			} else {
				contentStream.drawString("");
			}
		}

		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("28.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Wish to join NCC/NSS");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " " : (onlineAdmission.getCandidateDetails().getNccPref() == null) ? "NA"
						: ":  "
										+ onlineAdmission.getCandidateDetails().getNccPref().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("29.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Sports Participation");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getCandidateDetails() == null) ? " "
						: (onlineAdmission.getCandidateDetails().getSportsDetails() == null) ? " " : ":  "
										+ onlineAdmission.getCandidateDetails().getSportsDetails().toUpperCase());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("30.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       In House/Outsider");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getQualifyingExamDetails() == null) ? " "
						: (onlineAdmission.getQualifyingExamDetails().getApplicantfrom() == null) ? " " : ":  "
										+ onlineAdmission.getQualifyingExamDetails().getApplicantfrom().toUpperCase());
		contentStream.endText();

		previousContenyYAndContentStream[0] = previousContentY;
		previousContenyYAndContentStream[1] = contentStream;
		return previousContenyYAndContentStream;
	}

	public Object[] addContactDetails(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission) throws IOException {

		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];
		previousContentY = (float) previousContenyYAndContentStream[0];
		contentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		//System.out.println("Inside of the Contact details");
		previousContentY -= 16;
		/******************* to draw personal details text ******************/
		contentStream.fillRect(X0 + marginX, previousContentY -= 6, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY += 3);
		contentStream.drawString("Contact Details");
		contentStream.endText();
		contentStream.drawLine(X0 + marginX, previousContentY -= 3, X0 + marginX + ROW_WIDTH, previousContentY);

		/****************** drawing content **********************/

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("1.");
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Mob. No.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getContactDetails() == null) ? " "
						: (onlineAdmission.getContactDetails().getPersonalMobileNumber() == null) ? " " : ":  "
										+ onlineAdmission.getContactDetails().getPersonalMobileNumber());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("2.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Parents Ph. No.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getContactDetails() == null) ? " "
						: (onlineAdmission.getContactDetails().getParentContactNumber() == null) ? " " : ":  "
										+ onlineAdmission.getContactDetails().getParentContactNumber());
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY -= 16);
		contentStream.drawString("3.");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY);
		contentStream.drawString("       Email Id");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString((onlineAdmission.getContactDetails() == null) ? " " : (onlineAdmission.getContactDetails().getPersonalEmail() == null) ? " "
						: ":  "
										+ onlineAdmission.getContactDetails().getPersonalEmail().toLowerCase());
		contentStream.endText();

		previousContenyYAndContentStream[0] = previousContentY;
		previousContenyYAndContentStream[1] = contentStream;
		return previousContenyYAndContentStream;

	}

	/*private List<String> getPersonalHeaderList(){
		List<String> personalDetailHeaderList = new ArrayList<String>();
		personalDetailHeaderList.add("SURNAME");
		personalDetailHeaderList.add("FIRST NAME");
		personalDetailHeaderList.add("MIDDLE NAME");
		return personalDetailHeaderList;
	}*/

	/********************************
	 * method to add new page
	 * 
	 * @return
	 * @throws IOException
	 ****************************/
	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		//PDPageContentStream contentStream = new PDPageContentStream(doc, page,true,true);
		doc.addPage(page1);
		return page1;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentY < 56) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentY = 780;
		}
		return contentStream1;
	}
}
