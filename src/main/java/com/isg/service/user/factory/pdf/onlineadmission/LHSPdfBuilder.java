package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.model.onlineadmission.SiblingDetail;
import com.isg.service.user.util.FileStorePathAssembler;

public class LHSPdfBuilder implements PdfBuilder {

	public float marginX = 60;
	public float previousContentOfY;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 70f;
	private static final Logger log = Logger.getLogger(LHSPdfBuilder.class);

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException {
		createPdf(onlineAdmission, pdfFilePath, studentPhotoPath, null, branchLogo);
	}

	public PDPageContentStream drawHeader(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission,
					String studentPhotoPath, String branchLogo) throws IOException {
		
		if (!StringUtils.isEmpty(branchLogo))
		{
			drawImage(doc, branchLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}
		else {
			drawImage(doc, FileStorePathAssembler.QFIX_LOGO_IMAGE_RELATIVE_PATH, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
											- 91, 100, 100, contentStream);
		}
		
		contentStream.addRect((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 195, (PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT-50, 100,65);
		drawString.drawString(contentStream, 500, 770, PDType1Font.HELVETICA, 12, "For office use");
		drawString.drawString(contentStream, 500, 755, PDType1Font.HELVETICA, 10, "Adm No.");
		drawString.drawString(contentStream, 543, 755, PDType1Font.HELVETICA, 10, ".............");
		drawString.drawString(contentStream, 500, 742, PDType1Font.HELVETICA, 10, "Date:");
		drawString.drawString(contentStream, 540, 742, PDType1Font.HELVETICA, 10, ".............");
		drawString.drawString(contentStream, 500, 730, PDType1Font.HELVETICA, 10, "Sec: ");
		drawString.drawString(contentStream, 540, 730, PDType1Font.HELVETICA, 10, ".............");

		drawString.drawString(contentStream, 230, 760, PDType1Font.TIMES_BOLD_ITALIC, 16, "LORETO HOUSE");

		drawString.drawString(contentStream, 220, 740, PDType1Font.HELVETICA, 10, "7, Middleton Row, Kolkata - 700071");
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, 190, 700, PDType1Font.HELVETICA, 14, "Registration Form for - NURSERY Session: 2018-19");

		contentStream.setNonStrokingColor(Color.BLACK);

		String applicantId = onlineAdmission.getApplicationId();
		
		System.out.println(applicantId+"  "+ onlineAdmission);
		
		//Application Id  
		drawString.drawString(contentStream, 60, 650, PDType1Font.HELVETICA, 10, "Form No:");
		drawString.drawString(
						contentStream,
						135,
						650,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(applicantId) ? applicantId : ""));
		
		contentStream.addRect((PDPage.PAGE_SIZE_A4.getUpperRightX()/2-130), (PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 290, 350,160);
		drawString.drawString(contentStream, 180, 570, PDType1Font.HELVETICA, 10, "Photograph of Family (Mother, Father & child)");
		previousContentOfY = 450;
		return contentStream;
	}

	public PDPageContentStream childContent(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {

		contentStream.drawLine(marginX, previousContentOfY, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Child Information");
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 10, "Child Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getName()) ? !StringUtils
										.isEmpty(onlineAdmission.getCandidateDetails().getName().getFirstname()) ? onlineAdmission.getCandidateDetails()
										.getName().getFirstname().toUpperCase() : ""
										: ""
										: "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getMiddlename()) ? onlineAdmission.getCandidateDetails().getName().getMiddlename()
														.toUpperCase() : "" : "" : "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getSurname()) ? onlineAdmission.getCandidateDetails().getName().getSurname().toUpperCase()
														: "" : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Gender");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getGender()) ? onlineAdmission
						.getCandidateDetails().getGender().getName().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Date of Birth");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getDateofbirth()) ? onlineAdmission
						.getCandidateDetails().getDateofbirth() : "" : ""));
		
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Blood Group");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getBloodGroup()) ? onlineAdmission.getCandidateDetails()
						.getBloodGroup().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Religion");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getCasteDetails().getReligion()) ? onlineAdmission
						.getCandidateDetails().getCasteDetails().getReligion().getName() : "" : ""));
		
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Nationality ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getNationality()) ? onlineAdmission
						.getCandidateDetails().getNationality().toUpperCase() : "" : ""));
		
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Mother Tongue");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getMotherTounge()) ? onlineAdmission
						.getCandidateDetails().getMotherTounge().toUpperCase() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Category");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getCasteDetails().getCaste()) ? onlineAdmission
						.getCandidateDetails().getCasteDetails().getCaste().getName().toUpperCase() : "" : ""));
		
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Distance From School");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getDistanceFromSchool()) ? onlineAdmission
				.getAddressDetails().getDistanceFromSchool().toUpperCase() : "" : ""));
		
		return contentStream;

	}

	public PDPageContentStream parentDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Parent/Guardian Information");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 11, "Father Details");
		contentStream.setNonStrokingColor(Color.BLACK);

		//fathers details
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");

		String fatherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) && !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName())) {
			Name nameObj = onlineAdmission.getFatherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			fatherName = firstName + middleName + surName;
		}
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						fatherName);
		
		
		/*18_08_2017_PB*/
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Status");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getStatus()) ? onlineAdmission
						.getFatherDetails().getStatus(): "" : ""));
		
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Mobile Phone");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission
										.getContactDetails()) ? !StringUtils.isEmpty(onlineAdmission.getContactDetails()
										.getPersonalMobileNumber()) ? onlineAdmission.getContactDetails().getPersonalMobileNumber() : "" : ""));
		
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Office Phone");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getSecondaryPhone()) ? onlineAdmission
						.getFatherDetails().getSecondaryPhone() : "" : ""));
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getContactDetails()) ? !StringUtils.isEmpty(onlineAdmission.getContactDetails().getPersonalEmail()) ? onlineAdmission
						.getContactDetails()
						.getPersonalEmail() : "" : ""));
		
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Occupation");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));
		
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Company");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getEmployerName()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getEmployerName().toUpperCase() : ""
										: "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Designation & Dept.");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails().getDesignation()) ? onlineAdmission
						.getFatherDetails().getEmploymentDetails().getDesignation().toUpperCase() : "" : ""));
		
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Office Address");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails().getAddress()) ? onlineAdmission
						.getFatherDetails().getEmploymentDetails().getAddress() : "" : ""));
		
	
		
		//mother guardian
		previousContentOfY -= 10;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 11, "Mother Details");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		String motherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) && !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getName())) {
			Name nameObj = onlineAdmission.getMotherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			motherName = firstName + middleName + surName;
		}
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						motherName);
		
		/*18_08_2017_PB*/
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Status");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getStatus()) ? onlineAdmission
						.getMotherDetails().getStatus(): "" : ""));
		

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Mobile Phone");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getPrimaryPhone()) ? onlineAdmission.getMotherDetails()
						.getPrimaryPhone() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX + 310, previousContentOfY, PDType1Font.HELVETICA, 10, "Office Phone");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getSecondaryPhone()) ? onlineAdmission
						.getMotherDetails().getSecondaryPhone() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmail()) ? onlineAdmission.getMotherDetails()
						.getEmail() : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Occupation");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getMotherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Organization Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails()
										.getEmployerName()) ? onlineAdmission.getMotherDetails().getEmploymentDetails().getEmployerName().toUpperCase() : ""
										: "" : ""));

		drawString.drawString(contentStream, marginX+310, previousContentOfY, PDType1Font.HELVETICA, 10, "Designation & Dept.");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 420, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getDesignation()) ? onlineAdmission
						.getMotherDetails().getEmploymentDetails().getDesignation() : "" : ""));
		
		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Office Address");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 160, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getAddress()) ? onlineAdmission
						.getMotherDetails().getEmploymentDetails().getAddress() : "" : ""));

		return contentStream;
	}

	public PDPageContentStream addressDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 12, "Address Details");
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Area");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils
										.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress()) ? !StringUtils.isEmpty(onlineAdmission
										.getAddressDetails().getLocalAddress().getAddressLine1()) ? onlineAdmission.getAddressDetails()
										.getLocalAddress()
										.getAddressLine1().toUpperCase() : "" : "" : ""));

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "City");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress()
										) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getCity()) ? onlineAdmission
										.getAddressDetails().getLocalAddress().getCity()
										: ""
										: ""
										: ""));
		
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX+310, previousContentOfY, PDType1Font.HELVETICA, 10, "Postal Code");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 420,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
										.getLocalAddress()
										) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getPincode()) ? onlineAdmission
										.getAddressDetails().getLocalAddress().getPincode().toString()
										: ""
										: ""
										: ""));


		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "District");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 140,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission
								.getAddressDetails().getLocalAddress()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress().getDistrict()) ? onlineAdmission
										.getAddressDetails().getLocalAddress().getDistrict().getName().toString()
										: ""
										: ""
										: ""));
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX+ 310, previousContentOfY, PDType1Font.HELVETICA, 10, "State");
		drawString.drawString(contentStream, marginX + 400, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(
						contentStream,
						marginX + 420,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils.isEmpty(onlineAdmission
								.getAddressDetails().getLocalAddress().getState()) ? !StringUtils.isEmpty(onlineAdmission.getAddressDetails()
								.getLocalAddress().getState().getName()) ? onlineAdmission
								.getAddressDetails().getLocalAddress().getState().getName().toString() : "" : "" : ""));

		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 12, "This application is for");
		contentStream.setNonStrokingColor(Color.BLACK);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Programme Name");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCourseDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCourseDetails().getCourse()) ? onlineAdmission
						.getCourseDetails().getCourse().getName() : "" : ""));

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Pay Amount (Rs.)");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, ("200"));

		return contentStream;
	}

	public PDPageContentStream siblingsDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 10;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Sibling Details (Own sister)");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);

		for(SiblingDetail siblingDetails:onlineAdmission.getSiblingDetail()){
			
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name :");

		String siblingName = "";
		if (!StringUtils.isEmpty(siblingDetails) && !StringUtils.isEmpty(siblingDetails.getName())) {
			Name nameObj =siblingDetails.getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			siblingName = firstName + middleName + surName;
		}
		drawString.drawString(
						contentStream,
						marginX + 45,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						siblingName);

		drawString.drawString(contentStream, marginX, previousContentOfY-=20, PDType1Font.HELVETICA, 10, "Class/Sec ");
		drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, ":");
		drawString.drawString(contentStream, marginX + 140, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(siblingDetails) ? !StringUtils.isEmpty(siblingDetails.getGrade()) ?siblingDetails.getGrade().toString() : "" : ""));
		}

		return contentStream;
	}


	public PDPageContentStream signatureDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		previousContentOfY -= 20;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream.drawLine(marginX, previousContentOfY -= 20, marginX + 515, previousContentOfY);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 14, "Parent Signatures");
		contentStream.drawLine(marginX, previousContentOfY -= 5, marginX + 515, previousContentOfY);
		contentStream.setNonStrokingColor(Color.BLACK);
		Date date = new Date();

		drawString.drawString(contentStream, marginX, previousContentOfY-= 60, PDType1Font.HELVETICA, 10, "Date :");
		drawString.drawString(contentStream, marginX+30, previousContentOfY, PDType1Font.HELVETICA, 10, new SimpleDateFormat("dd-MMM-yyyy").format(date));
		contentStream.drawLine(marginX+25, previousContentOfY , marginX + 100, previousContentOfY);

		contentStream.drawLine(marginX + 200, previousContentOfY += 20, marginX + 300, previousContentOfY);
		drawString.drawString(contentStream, marginX + 200, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Father Signature ");

		contentStream.drawLine(marginX + 400, previousContentOfY += 20, marginX + 500, previousContentOfY);
		drawString.drawString(contentStream, marginX + 400, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Mother Signature ");

		return contentStream;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentOfY < 56) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentOfY = 780;
		}
		return contentStream1;
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}

	public void drawImage(PDDocument doc, String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {

		PDXObjectImage xImage = null;
		InputStream in = null;
		try {
			//System.out.println("Image Path to draw>>>>:::" + path);
			log.debug("Image Path to draw>>>>:::" + path);
			String image = path;
			if (image.toLowerCase().endsWith(".jpg"))
			{
				xImage = new PDJpeg(doc, new FileInputStream(image));
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (org.apache.pdfbox.io.RandomAccess) new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Image not Found at ::" + path);
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public static void main(String a[]) throws JsonParseException, JsonMappingException, IOException {
		String data = "{\"course_details\":{\"course\":{\"id\":1,\"code\":\"1\",\"name\":\"CourseETOOS\",\"active\":false,\"category\":\"PS\"},\"admitted\":\"543\",\"payment_amount\":\"5432\"},\"candidate_details\":{\"name\":{\"firstname\":\"rgdf\",\"surname\":\"rfvd\"},\"gender\":{\"name\":\"Male\",\"id\":\"1\"},\"dateofbirth\":\"27/08/2017\"},\"address_details\":{\"local_address\":{\"country\":{\"name\":\"INDIA\",\"id\":\"1\"},\"state\":{\"id\":13,\"code\":\"13\",\"name\":\"NAGALAND\",\"active\":true},\"district\":{\"id\":261,\"code\":\"261\",\"name\":\"Kohima\",\"stateId\":13,\"active\":true},\"pincode\":\"465987\",\"address_line1\":\"trgfd\"}},\"contact_details\":{\"personal_mobile_number\":\"4587961258\",\"personal_email\":\"gfh@edgf.gf\"},\"url_configuration_id\":3,\"general_comment\":\"trbfvd\"}";
		OnlineAdmission asdmission = new ObjectMapper().readValue(data, OnlineAdmission.class);
		new LHSPdfBuilder().createPdf(asdmission, "c:/temp/images/lhs.pdf",
						null, null,
						"C:/Temp/images/Qfix/documents/External/vvs.jpg");
	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException {
		PDDocument doc = new PDDocument();
		PDPage page;
		try {
			page = new PDPage();
			doc.addPage(page);
			PDPageContentStream contentStream = new PDPageContentStream(doc, page);
			DrawString drawString = new DrawString();
			contentStream = drawHeader(doc, contentStream, drawString, onlineAdmission, studentPhotoPath, branchLogo);
			contentStream = childContent(doc, contentStream, drawString, onlineAdmission);
			contentStream = addressDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = parentDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = siblingsDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = signatureDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream.close();
			doc.save(pdfFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}
		}
	}

}