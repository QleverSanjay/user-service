package com.isg.service.user.factory.pdf.onlineadmission;

public class AcademicDetailsTempModel {

	/**
	 * @param args
	 */
	private  String SrNo;
	private  String LastExam;
	private  String PassingYear;
	private  String BoardUniversity;
	private  String SchoolAttended;
	private  String MarksObtained;
	private  String OutOf;
	private String percentage;
	public String getSrNo() {
		return SrNo;
	}
	public void setSrNo(String srNo) {
		SrNo = srNo;
	}
	public String getLastExam() {
		return LastExam;
	}
	public void setLastExam(String lastExam) {
		LastExam = lastExam;
	}
	public String getPassingYear() {
		return PassingYear;
	}
	public void setPassingYear(String passingYear) {
		PassingYear = passingYear;
	}
	public String getBoardUniversity() {
		return BoardUniversity;
	}
	public void setBoardUniversity(String boardUniversity) {
		BoardUniversity = boardUniversity;
	}
	public String getSchoolAttended() {
		return SchoolAttended;
	}
	public void setSchoolAttended(String schoolAttended) {
		SchoolAttended = schoolAttended;
	}
	public String getMarksObtained() {
		return MarksObtained;
	}
	public void setMarksObtained(String marksObtained) {
		MarksObtained = marksObtained;
	}
	public String getOutOf() {
		return OutOf;
	}
	public void setOutOf(String outOf) {
		OutOf = outOf;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	
	
}
