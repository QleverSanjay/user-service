package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.util.StringUtils;

import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.util.FileStorePathAssembler;

public class PathfinderPdfBuilder implements PdfBuilder {

	public float marginX = 60;
	public float previousContentOfY;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 70f;
	private static final Logger log = Logger.getLogger(PathfinderPdfBuilder.class);

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException {
		createPdf(onlineAdmission, pdfFilePath, studentPhotoPath, null, branchLogo);
	}

	public PDPageContentStream drawHeader(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission,
					String studentPhotoPath, String branchLogo) throws IOException {
		Date date = new Date();
		//Drawing logo of Pathfindersps
		if (!StringUtils.isEmpty(branchLogo))
		{
			drawImage(doc, branchLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 50, (PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT
							- 91, 150, 100, contentStream);
		}
		else {
			drawString.drawString(contentStream, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 50, (PDPage.PAGE_SIZE_A4.getUpperRightY())
							- PADDING_BOTTOM_OF_DOCUMENT - 91, PDType1Font.HELVETICA, 10, "Branch logo not found");
		}

		//draw rectangle
		contentStream.addRect((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 153, (PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 171, 124,
						123);

		drawString.drawString(contentStream, 480, 660, PDType1Font.HELVETICA, 10, "Child Photo Here");

		if (!StringUtils.isEmpty(studentPhotoPath))
		{
			drawImage(doc, studentPhotoPath, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 153, (PDPage.PAGE_SIZE_A4.getUpperRightY())
							- PADDING_BOTTOM_OF_DOCUMENT - 171, 124, 123, contentStream);
		}

		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, 250, 650, PDType1Font.HELVETICA, 14, "Registration Form");
		drawString.drawString(contentStream, 210, 630, PDType1Font.HELVETICA, 10, "Pre-nursery/ Nursery/ Lower Kindergarten");
		contentStream.setNonStrokingColor(Color.BLACK);
		drawString.drawString(contentStream, 420, 575, PDType1Font.HELVETICA, 10, "Date :-");
		drawString.drawString(contentStream, 455, 575, PDType1Font.HELVETICA, 10, new SimpleDateFormat("dd-MMM-yyyy").format(date));
		contentStream.drawLine(415, 565, 575, 565);

		previousContentOfY = 560;
		return contentStream;
	}

	public PDPageContentStream childContent(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Child Name :-");
		drawString.drawString(
						contentStream,
						marginX + 65,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getName()) ? !StringUtils
										.isEmpty(onlineAdmission.getCandidateDetails().getName().getFirstname()) ? onlineAdmission.getCandidateDetails()
										.getName().getFirstname().toUpperCase() : ""
										: ""
										: "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getMiddlename()) ? onlineAdmission.getCandidateDetails().getName().getMiddlename()
														.toUpperCase() : "" : "" : "")
										+ "  "
										+ (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission
														.getCandidateDetails().getName()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
														.getName().getSurname()) ? onlineAdmission.getCandidateDetails().getName().getSurname().toUpperCase()
														: "" : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Gender :-");
		drawString.drawString(contentStream, marginX + 50, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getGender()) ? onlineAdmission
						.getCandidateDetails().getGender().getName().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Date of Birth :-");
		drawString.drawString(contentStream, marginX + 70, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getDateofbirth()) ? onlineAdmission
						.getCandidateDetails().getDateofbirth() : "" : ""));
		drawString.drawString(contentStream, marginX + 260, previousContentOfY, PDType1Font.HELVETICA, 10, "Place of Birth :-");
		drawString.drawString(contentStream, marginX + 335, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getPlaceofbirth()) ? onlineAdmission
						.getCandidateDetails().getPlaceofbirth() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Any preschools attended prior :-");
		drawString.drawString(contentStream, marginX + 150, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getLastSchoolName()) ? onlineAdmission
						.getCandidateDetails().getLastSchoolName().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Languages spoken at home :-");
		drawString.drawString(contentStream, marginX + 145, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails().getLanguagesKnown()) ? onlineAdmission
						.getCandidateDetails().getLanguagesKnown().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);
		return contentStream;

	}

	public PDPageContentStream intakeCriteria(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {

		if (!StringUtils.isEmpty(onlineAdmission.getCandidateDetails())) {
			if (onlineAdmission.getCandidateDetails().getAcademicTerm().equals("1") || onlineAdmission.getCandidateDetails().getAcademicTerm().equals("2")
							|| onlineAdmission.getCandidateDetails().getAcademicTerm().equals("3")
							|| onlineAdmission.getCandidateDetails().getAcademicTerm().equals("4")) {
				contentStream = checkEndOfPage(contentStream, doc);
				drawImage(doc, FileStorePathAssembler.CHECKBOX_IMAGE_RELATIVE_PATH, marginX, previousContentOfY -= 30, 10, 10, contentStream);
				drawString.drawString(contentStream, marginX + 20, previousContentOfY += 2, PDType1Font.HELVETICA_BOLD, 10, "Pre-nursery :");
				drawString.drawString(contentStream, marginX + 100, previousContentOfY, PDType1Font.HELVETICA, 10, "(22 months to 29 months)");
				drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA_BOLD, 10, "Intake Criteria for Pre-nursery :");
				drawString.drawString(contentStream, marginX + 148, previousContentOfY, PDType1Font.HELVETICA, 10, "Twice each term");

				if (onlineAdmission.getCandidateDetails().getAcademicTerm().equals("1") || onlineAdmission.getCandidateDetails().getAcademicTerm().equals("2")) {
					contentStream = checkEndOfPage(contentStream, doc);
					drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA_BOLD, 10, "Term 1");

					if (onlineAdmission.getCandidateDetails().getAcademicTerm().equals("1")) {
						drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
										"Start July - child(ren) should turn 2 years between June to August of the academic year");
					}
					else {
						drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
										"Start September - child(ren) should turn 2 years between September to November of the academic year");
					}
				}

				else {
					contentStream = checkEndOfPage(contentStream, doc);
					drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA_BOLD, 10, "Term 2");

					if (onlineAdmission.getCandidateDetails().getAcademicTerm().equals("3")) {
						drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
										"Start January - child(ren) should turn 2 years between December to February of the academic year");
					}
					else {
						drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
										"Start March - child(ren) should turn 2 years between March to May of the academic year");
					}
				}
			}
			else if (onlineAdmission.getCandidateDetails().getAcademicTerm().equals("5")) {

				contentStream = checkEndOfPage(contentStream, doc);
				drawImage(doc, FileStorePathAssembler.CHECKBOX_IMAGE_RELATIVE_PATH, marginX, previousContentOfY -= 30, 10, 10, contentStream);
				drawString.drawString(contentStream, marginX + 20, previousContentOfY += 2, PDType1Font.HELVETICA_BOLD, 10, "Nursery :");
				drawString.drawString(contentStream, marginX + 70, previousContentOfY, PDType1Font.HELVETICA, 10, "(2.6 years to 3.5 years) ");
				contentStream = checkEndOfPage(contentStream, doc);
				drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 8,
								"* Please note that our admissions to Nursery & Kindergarten are based on availabilities as our Pre-nursery children move to Nursery & Nursery");
				drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 8, "children move to LKG.");

			}
			else {
				contentStream = checkEndOfPage(contentStream, doc);
				drawImage(doc, FileStorePathAssembler.CHECKBOX_IMAGE_RELATIVE_PATH, marginX, previousContentOfY -= 30, 10, 10, contentStream);
				drawString.drawString(contentStream, marginX + 20, previousContentOfY += 2, PDType1Font.HELVETICA_BOLD, 10, "Lower Kindergarten:");
				drawString.drawString(contentStream, marginX + 120, previousContentOfY, PDType1Font.HELVETICA, 10, "(3.6 years to 4.5 years)");
				contentStream = checkEndOfPage(contentStream, doc);
				drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 8,
								"* Please note that our admissions to Nursery & Kindergarten are based on availabilities as our Pre-nursery children move to Nursery & Nursery");
				drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 8, "children move to LKG.");
			}
		}

		//TODO TAKE DATA FROM getAcademicdetails 
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA_BOLD, 10, "Start Term Month / Year :-");
		drawString.drawString(
						contentStream,
						marginX + 130,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getCandidateDetails()) ? !StringUtils.isEmpty(onlineAdmission.getCandidateDetails()
										.getAcademicYear().getName()) ? onlineAdmission.getCandidateDetails().getAcademicYear().getName() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		return contentStream;
	}

	public PDPageContentStream motherDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {

		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Mother");
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name :-");

		String motherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) && !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getName())) {
			Name nameObj = onlineAdmission.getMotherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			motherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 45,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						motherName);
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Age :-");
		drawString.drawString(contentStream, marginX + 35, previousContentOfY, PDType1Font.HELVETICA, 10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? onlineAdmission.getMotherDetails().getAge() != 0 ? onlineAdmission
										.getMotherDetails().getAge() + "" : "" : ""));
		drawString.drawString(contentStream, marginX + 260, previousContentOfY, PDType1Font.HELVETICA, 10, "Citizenship :-");
		drawString.drawString(contentStream, marginX + 325, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getCitizenship()) ? onlineAdmission.getMotherDetails()
						.getCitizenship().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "School :-");
		drawString.drawString(contentStream, marginX + 45, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getSchoolDetail()) ? onlineAdmission.getMotherDetails()
						.getSchoolDetail().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "College /Post Graduate :-");
		drawString.drawString(contentStream, marginX + 120, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getCollegDetail()) ? onlineAdmission.getMotherDetails()
						.getCollegDetail().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Occupation :-");
		drawString.drawString(
						contentStream,
						marginX + 65,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getMotherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Organization Name :-");
		drawString.drawString(
						contentStream,
						marginX + 100,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails()
										.getEmployerName()) ? onlineAdmission.getMotherDetails().getEmploymentDetails().getEmployerName().toUpperCase() : ""
										: "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Primary Phone :-");
		drawString.drawString(contentStream, marginX + 80, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getPrimaryPhone()) ? onlineAdmission.getMotherDetails()
						.getPrimaryPhone() : "" : ""));
		drawString.drawString(contentStream, marginX + 260, previousContentOfY, PDType1Font.HELVETICA, 10, "Secondary Phone :-");
		drawString.drawString(contentStream, marginX + 355, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getSecondaryPhone()) ? onlineAdmission
						.getMotherDetails().getSecondaryPhone() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email  :-");
		drawString.drawString(contentStream, marginX + 45, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmail()) ? onlineAdmission.getMotherDetails()
						.getEmail() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Office Address :-");
		drawString.drawString(
						contentStream,
						marginX + 80,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getEmploymentDetails().getAddress()) ? onlineAdmission
										.getMotherDetails().getEmploymentDetails().getAddress().toUpperCase()
										: ""
										: ""
										: ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Hobbies :-");
		drawString.drawString(contentStream, marginX + 50, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getMotherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getMotherDetails().getHobbies()) ? onlineAdmission.getMotherDetails()
						.getHobbies().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		return contentStream;
	}

	public PDPageContentStream fatherDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {

		previousContentOfY -= 40;
		contentStream = checkEndOfPage(contentStream, doc);
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Father");
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name :-");

		String fatherName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) && !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getName())) {
			Name nameObj = onlineAdmission.getFatherDetails().getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			fatherName = firstName + middleName + surName;
		}

		drawString.drawString(
						contentStream,
						marginX + 45,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						fatherName);

		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Age :-");
		drawString.drawString(contentStream, marginX + 35, previousContentOfY, PDType1Font.HELVETICA, 10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? onlineAdmission.getFatherDetails().getAge() != 0 ? onlineAdmission
										.getFatherDetails().getAge() + "" : "" : ""));
		drawString.drawString(contentStream, marginX + 260, previousContentOfY, PDType1Font.HELVETICA, 10, "Citizenship :-");
		drawString.drawString(contentStream, marginX + 325, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getCitizenship()) ? onlineAdmission.getFatherDetails()
						.getCitizenship().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "School :-");
		drawString.drawString(contentStream, marginX + 45, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getSchoolDetail()) ? onlineAdmission.getFatherDetails()
						.getSchoolDetail().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "College /Post Graduate :-");
		drawString.drawString(contentStream, marginX + 120, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getCollegDetail()) ? onlineAdmission.getFatherDetails()
						.getCollegDetail().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Occupation :-");
		drawString.drawString(
						contentStream,
						marginX + 65,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getOccupation()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getOccupation().toUpperCase() : "" : ""
										: ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Organization Name :-");
		drawString.drawString(
						contentStream,
						marginX + 100,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails()
										.getEmployerName()) ? onlineAdmission.getFatherDetails().getEmploymentDetails().getEmployerName().toUpperCase() : ""
										: "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Primary Phone :-");
		drawString.drawString(contentStream, marginX + 80, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getPrimaryPhone()) ? onlineAdmission.getFatherDetails()
						.getPrimaryPhone() : "" : ""));
		drawString.drawString(contentStream, marginX + 260, previousContentOfY, PDType1Font.HELVETICA, 10, "Secondary Phone :-");
		drawString.drawString(contentStream, marginX + 355, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getSecondaryPhone()) ? onlineAdmission
						.getFatherDetails().getSecondaryPhone() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Email  :-");
		drawString.drawString(contentStream, marginX + 45, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmail()) ? onlineAdmission.getFatherDetails()
						.getEmail() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Office Address :-");
		drawString.drawString(
						contentStream,
						marginX + 80,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails()
										.getEmploymentDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getEmploymentDetails().getAddress()) ? onlineAdmission
										.getFatherDetails().getEmploymentDetails().getAddress().toUpperCase()
										: ""
										: ""
										: ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Hobbies :-");
		drawString.drawString(contentStream, marginX + 50, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getFatherDetails()) ? !StringUtils.isEmpty(onlineAdmission.getFatherDetails().getHobbies()) ? onlineAdmission.getFatherDetails()
						.getHobbies().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		return contentStream;
	}

	public PDPageContentStream siblingsDetails(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		contentStream.setNonStrokingColor(new Color(100, 125, 250));
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 30, PDType1Font.HELVETICA, 14, "Siblings");
		contentStream.setNonStrokingColor(Color.BLACK);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Name :-");
		String siblingName = "";
		if (!StringUtils.isEmpty(onlineAdmission.getSiblingDetail()) && !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getName())) {
			Name nameObj = onlineAdmission.getSiblingDetail().get(0).getName();

			String firstName = !StringUtils.isEmpty(nameObj.getFirstname()) ? nameObj.getFirstname().toUpperCase() + " " : "";
			String middleName = !StringUtils.isEmpty(nameObj.getMiddlename()) ? nameObj.getMiddlename().toUpperCase() + " " : "";
			String surName = !StringUtils.isEmpty(nameObj.getSurname()) ? nameObj.getSurname().toUpperCase() + " " : "";
			siblingName = firstName + middleName + surName;
		}
		drawString.drawString(
						contentStream,
						marginX + 45,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						siblingName);
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Birth Date :-");
		drawString.drawString(contentStream, marginX + 60, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getSiblingDetail()) ? !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getDateofbirth()) ? onlineAdmission.getSiblingDetail()
						.get(0).getDateofbirth() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Current School :-");
		drawString.drawString(contentStream, marginX + 80, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getSiblingDetail()) ? !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getCurrentSchool()) ? onlineAdmission.getSiblingDetail()
								.get(0).getCurrentSchool().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Other Pre-schools / Schools Attended :-");
		drawString.drawString(contentStream, marginX + 180, previousContentOfY, PDType1Font.HELVETICA, 10, (!StringUtils.isEmpty(onlineAdmission
						.getSiblingDetail()) ? !StringUtils.isEmpty(onlineAdmission.getSiblingDetail().get(0).getOtherschoolAttended()) ? onlineAdmission
						.getSiblingDetail().get(0).getOtherschoolAttended().toUpperCase() : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Home Address :-");
		drawString.drawString(
						contentStream,
						marginX + 80,
						previousContentOfY,
						PDType1Font.HELVETICA,
						10,
						(!StringUtils.isEmpty(onlineAdmission.getAddressDetails()) ? !StringUtils
										.isEmpty(onlineAdmission.getAddressDetails().getLocalAddress()) ? !StringUtils.isEmpty(onlineAdmission
										.getAddressDetails().getLocalAddress().getAddressLine1()) ? onlineAdmission.getAddressDetails().getLocalAddress()
										.getAddressLine1().toUpperCase() : "" : "" : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);
		contentStream.drawLine(marginX, previousContentOfY -= 23, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "How did you hear about us :-");
		drawString.drawString(contentStream, marginX + 135, previousContentOfY, PDType1Font.HELVETICA, 10,
						(!StringUtils.isEmpty(onlineAdmission.getGeneralComment()) ? onlineAdmission.getGeneralComment().toUpperCase() : ""));
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10, "Any Parent Signature :-");
		drawString.drawString(contentStream, marginX + 260, previousContentOfY, PDType1Font.HELVETICA, 10, "Date :-");
		contentStream.drawLine(marginX, previousContentOfY -= 3, marginX + 515, previousContentOfY);

		return contentStream;
	}

	public PDPageContentStream termsAndCondition(PDDocument doc, PDPageContentStream contentStream, DrawString drawString, OnlineAdmission onlineAdmission)
					throws IOException {
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"(The parent will sign and date on the day of the interaction)");
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA_BOLD, 10,
						"Kindly download this form. After filling in all the information please email it to us at: admin@pathfindersps.com");
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"You will need to pay a registration fee of INR 500/-which is not applicable to tuition charges if your child enrolls");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10, "at Pathfinders.");
		contentStream = checkEndOfPage(contentStream, doc);
		drawString.drawString(contentStream, marginX, previousContentOfY -= 20, PDType1Font.HELVETICA, 10,
						"Pay cash at the school location at 61, Rose Cottage, Dr. SS Rao Road, Opp Citi Tower, Parel 400012, Mumbai");
		drawString.drawString(contentStream, marginX, previousContentOfY -= 15, PDType1Font.HELVETICA, 10, "on the day of the interaction.");
		return contentStream;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentOfY < 56) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentOfY = 780;
		}
		return contentStream1;
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}

	public void drawImage(PDDocument doc, String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {

		PDXObjectImage xImage = null;
		InputStream in = null;
		try {
			//System.out.println("Image Path to draw>>>>:::" + path);
			log.debug("Image Path to draw>>>>:::" + path);
			String image = path;
			if (image.toLowerCase().endsWith(".jpg"))
			{
				xImage = new PDJpeg(doc, new FileInputStream(image));
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (RandomAccess) new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Image not Found at ::" + path);
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}

	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException {
		PDDocument doc = new PDDocument();
		PDPage page;
		try {
			page = new PDPage();
			doc.addPage(page);
			PDPageContentStream contentStream = new PDPageContentStream(doc, page);
			DrawString drawString = new DrawString();
			contentStream = drawHeader(doc, contentStream, drawString, onlineAdmission, studentPhotoPath, branchLogo);
			contentStream = childContent(doc, contentStream, drawString, onlineAdmission);
			contentStream = intakeCriteria(doc, contentStream, drawString, onlineAdmission);
			contentStream = motherDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = fatherDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = siblingsDetails(doc, contentStream, drawString, onlineAdmission);
			contentStream = termsAndCondition(doc, contentStream, drawString, onlineAdmission);
			contentStream.close();
			doc.save(pdfFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}
		}
	}

}
