package com.isg.service.user.factory.pdf.onlineadmission;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.isg.service.user.model.onlineadmission.Document;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;

public class DocumentsSubmittedForAdmission {

	/**
	 * @param args
	 */
	PDDocument doc = null;
	PDPage page = null;
	public int RECTANGLE_WIDTH = 535;
	public int RECTANGLE_HEIGHT = 12;
	public float X0 = 40f;
	public float marginX = 5f;
	public float previousContentY;
	public float CONTENT_FONT_SIZE = 9;
	public float ROW_WIDTH = 535;
	int remarksFlag = 0;
	float lastHeaderLineYPosition = 0;
	public int header = 0;
	// Font configuration
	int serialNumber = 0;
	// Table configuration
	public float UPPER_CONTENT_HEIGHT;
	public int beginTextFlag;
	public int nextLine = (-12);
	int nextLineCounter = 0;

	public Object[] addDocumentsSubmittedForAdmission(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission)
					throws Exception {

		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];

		previousContentY = ((float) previousContenyYAndContentStream[0]);

		/************ Adding comtent of the document with serial number ****************/
		Map<Integer, String> documentsList = new HashMap<Integer, String>();
		List<Document> documentList = new ArrayList<Document>();
		documentList = onlineAdmission.getDocuments();
		/************* Documents data from model is from here in if loop *****************/
		for (int i = 0; i < documentList.size(); i++) {
			documentsList.put(serialNumber = (serialNumber == 0) ? 1 : serialNumber + 1, documentList.get(i).getName());
		}

		contentStream = checkEndOfPage(contentStream, doc, 0);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 10, previousContentY -= 13);
		contentStream.drawString("Gap In Education");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 152, previousContentY);
		contentStream.drawString(":  NO");
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc, 0);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + 10, previousContentY -= 16);
		contentStream.drawString("Documents Submitted at time of Admission");
		contentStream.endText();

		contentStream = checkEndOfPage(contentStream, doc, 0);
		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 16);
		for (Integer key : documentsList.keySet()) {
			contentStream.drawString(key + "   " + documentsList.get(key));
			contentStream.appendRawCommands("T*\n");
			/*contentStream =  checkEndOfPage(contentStream, doc, beginTextFlag = (previousContentY < 56) ? 1 : 0);
			if(beginTextFlag == 1){
			contentStream.moveTextPositionByAmount(X0, previousContentY = 500);
			}*/

			contentStream.moveTextPositionByAmount(0, nextLine = -12);
			/*previousContentY += nextLine;*/
			nextLineCounter++;
		}
		previousContentY += (nextLine * nextLineCounter);
		contentStream.endText();

		if (onlineAdmission.getAcademicDetails() != null) {
			if (onlineAdmission.getCourseDetails().getCourse().getId() != 7) {
				contentStream.close();
			}
		}
		previousContenyYAndContentStream[0] = previousContentY;
		previousContenyYAndContentStream[1] = contentStream;
		return previousContenyYAndContentStream;
	}

	/*private List<String> getPersonalHeaderList(){
		List<String> personalDetailHeaderList = new ArrayList<String>();
		personalDetailHeaderList.add("SURNAME");
		personalDetailHeaderList.add("FIRST NAME");
		personalDetailHeaderList.add("MIDDLE NAME");
		return personalDetailHeaderList;
	}*/

	/********************************
	 * method to add new page
	 * 
	 * @return
	 * @throws IOException
	 ****************************/
	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		//PDPageContentStream contentStream = new PDPageContentStream(doc, page,true,true);
		doc.addPage(page1);
		return page1;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc, int beginTextFlag) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentY < 60) {
			if (beginTextFlag == 1) {
				contentStream.endText();
			}
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentY = 780;
			if (beginTextFlag == 1) {
				contentStream1.beginText();
				beginTextFlag = 1;
			}
		}
		return contentStream1;
	}

}
