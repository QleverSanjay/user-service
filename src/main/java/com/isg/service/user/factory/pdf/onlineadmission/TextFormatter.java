package com.isg.service.user.factory.pdf.onlineadmission;

import java.util.ArrayList;
import java.util.List;

public class TextFormatter {

	public static List<String> stringSplit(String text, int limit, int flag) {
		List<String> list = new ArrayList<String>();
		return stringSplit(text, limit, flag, list);
	}

	static List<String> stringSplit(String text, int limit, int flag, List<String> list) // method to split text or wrap text
	{

		if (flag == 0) {
			list.clear();
			flag = 1;
		}
		int thislimit = limit;
		int spacePosition = 0, z = 0;
		if (text != null && text.length() > thislimit) {
			if (text.substring(0, thislimit).contains(" ")) {
				for (int i = z; i < thislimit; i++) {
					if (text.charAt(i) == ' ') {
						spacePosition = i;
					}
				}
				if (text.charAt(spacePosition) == ' ') {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition + 1, text.length()), thislimit, 1, list);
				} else {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition, text.length()), thislimit, 1, list);
				}
			} else {
				list.add(text.substring(0, thislimit));
				stringSplit(text.substring(thislimit, text.length()), thislimit, 1, list);
			}
		} else {
			list.add(text);
		}
		return list;
		//}

	}

}
