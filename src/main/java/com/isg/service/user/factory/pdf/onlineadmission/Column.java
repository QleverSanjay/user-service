package com.isg.service.user.factory.pdf.onlineadmission;

import java.util.List;

public class Column {

    private List<String> name;
    private float width;

    public Column(List<String> name, float width) {
        this.name = name;
        this.width = width;
    }

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }
}
