package com.isg.service.user.factory.validator.onlineadmission;

import org.springframework.stereotype.Repository;

import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.response.OnlineAdmissionConfiguration;

@Repository
public interface Validator {

	public void validate(OnlineAdmission onlineAdmission, OnlineAdmissionConfiguration admissionConfiguration) throws ApplicationException;
}
