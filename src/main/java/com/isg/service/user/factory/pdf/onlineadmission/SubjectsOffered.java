package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.isg.service.user.model.onlineadmission.OnlineAdmission;

public class SubjectsOffered {

	PDDocument doc = null;
	PDPage page = new PDPage();
	public int RECTANGLE_WIDTH = 535;
	public int RECTANGLE_HEIGHT = 12;
	public float X0 = 40f;
	public float marginX = 5f;
	public float previousContentY;
	public float CONTENT_FONT_SIZE = 9;
	public float ROW_WIDTH = 535;
	private final float ROW_HEIGHT = 20;
	int remarksFlag = 0;
	private final PDRectangle PAGE_SIZE = PDPage.PAGE_SIZE_A4;
	private final float MARGIN = 50;
	private final boolean IS_LANDSCAPE = false;
	float lastHeaderLineYPosition = 0;
	public int header = 0;
	// Font configuration
	private final PDFont TEXT_FONT = PDType1Font.HELVETICA;
	private final float FONT_SIZE = 9;

	// Table configuration
	private final float CELL_MARGIN = 2;
	public float UPPER_CONTENT_HEIGHT;

	/************************ Drawing the data for Academic Details **********/

	public Object[] addSubjectsOffered(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission) throws IOException {

		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];//new PDPageContentStream(doc,page);
		previousContentY = ((float) previousContenyYAndContentStream[0]);

		//System.out.println("Inside of the Subjects Offered ");

		contentStream = checkEndOfPage(contentStream, doc);
		/******************* to draw Academic Detailss text ******************/
		contentStream.setNonStrokingColor(Color.GRAY);
		contentStream.fillRect(X0 + marginX, previousContentY -= 18, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.black);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY += 3);
		contentStream.drawString("Subjects Offered :");
		contentStream.endText();
		contentStream.drawLine(X0 + marginX, previousContentY -= 3, X0 + marginX + ROW_WIDTH, previousContentY);

		List<String> subjectOfferedSrNoList = new ArrayList<String>();
		subjectOfferedSrNoList.add("1.");
		subjectOfferedSrNoList.add("2.");
		subjectOfferedSrNoList.add("3.");
		subjectOfferedSrNoList.add("5.");
		subjectOfferedSrNoList.add("6.");
		subjectOfferedSrNoList.add("7.");
		subjectOfferedSrNoList.add("8.");

		contentStream = drawTable(contentStream, subjectOfferedSrNoList);
		contentStream.close();

		previousContenyYAndContentStream[0] = previousContentY;
		previousContenyYAndContentStream[1] = previousContenyYAndContentStream;

		return previousContenyYAndContentStream;

		/****************** to draw content of Parents and Guardian Details ************/
	}

	private PDPageContentStream generateContentStream(PDPageContentStream contentStream, Table table) throws IOException {
		// User transformation matrix to change the reference when drawing.
		// This is necessary for the landscape position to draw correctly
		if (table.isLandscape()) {
			contentStream.concatenate2CTM(0, 1, -1, 0, table.getPageSize().getWidth(), 0);
		}
		contentStream.setFont(table.getTextFont(), table.getFontSize());
		return contentStream;
	}

	private Table createSubjectContent(List<String> subjectOfferedSrNoList) {

		// Total size of columns must not be greater than table width.
		UPPER_CONTENT_HEIGHT = PDPage.PAGE_SIZE_A4.getUpperRightY() - 200;
		String[][] content;
		List<Column> columns = new ArrayList<Column>();
		// Start print logic
		content = new String[subjectOfferedSrNoList.size()][9];
		int i = 0;
		int j = 0;
		int lengthOfsubjectOffered = subjectOfferedSrNoList.size();
		for (int k = 0; k < subjectOfferedSrNoList.size(); k++) { //adding data to content condition - any on row of that column have data

			content[i][j] = (subjectOfferedSrNoList.get(k));
			content[i][++j] = "";
			i++;
			j = 0;
		}

		List<String> srNo = new ArrayList<String>();
		srNo = TextFormatter.stringSplit("Sr No.", 10, 0);

		List<String> subjectOfferedList = new ArrayList<String>();
		subjectOfferedList = TextFormatter.stringSplit("Subjects Offered (S.S.C.)", 50, 0);
		columns.add(new Column(srNo, 35));
		columns.add(new Column(subjectOfferedList, 480));
		//Code for make length of all column equal
		int columnsLength = (columns.size() == 0) ? 1 : columns.size();
		int equalWidth = (500 / columnsLength); //adjust value according to text 
		//columns.remove(columnsLength-1);
		/*if(columns.size() != 0)
		{
			for(Column c : columns)
			{
				//c.setWidth(equalWidth);
			}
		}*/

		float tableHeight = ROW_HEIGHT * (subjectOfferedSrNoList.size() + 1);
		Table table = new TableBuilder().setCellMargin(CELL_MARGIN)
						.setColumns(columns).setContent(content).setHeight(tableHeight)
						.setNumberOfRows(content.length).setRowHeight(ROW_HEIGHT)
						.setMargin(MARGIN).setPageSize(PAGE_SIZE)
						.setEqualWidth(equalWidth)
						.setLandscape(IS_LANDSCAPE).setTextFont(TEXT_FONT)
						.setFontSize(FONT_SIZE).build();
		List<Column> columns1 = table.getColumns();
		for (Column column : columns1) {
			header = (column.getName().size() > header) ? column.getName().size() : header;
		}
		return table;
	}

	public PDPageContentStream drawTable(PDPageContentStream contentStream, List<String> subjectOfferedSrNoList) throws IOException {
		Table table = createSubjectContent(subjectOfferedSrNoList);

		// Calculate pagination
		Integer rowsPerPage = table.getContent().length + 2; // subtract
		contentStream = generateContentStream(contentStream, table);
		String[][] currentPageContent = getContentForCurrentPage(table, rowsPerPage, 0);
		drawCurrentPage(table, currentPageContent, contentStream);

		return contentStream;
	}

	private String[][] getContentForCurrentPage(Table table, Integer rowsPerPage, int pageCount) {
		int startRange = pageCount * rowsPerPage;
		int endRange = (pageCount * rowsPerPage) + rowsPerPage;
		if (endRange > table.getNumberOfRows()) {
			endRange = table.getNumberOfRows();
		}
		return Arrays.copyOfRange(table.getContent(), startRange, endRange);
	}

	private void drawCurrentPage(Table table, String[][] currentPageContent, PDPageContentStream contentStream)
					throws IOException {
		//float tableTopY = table.isLandscape() ? table.getPageSize().getWidth() - table.getMargin() : table.getPageSize().getHeight() - table.getMargin();

		float tableTopY = previousContentY - 20;
		// Draws grid and borders

		drawTableGrid(table, currentPageContent, contentStream, tableTopY);

		// Position cursor to start drawing content
		float nextTextX = table.getMargin() + table.getCellMargin();
		// Calculate center alignment for text in cell considering font height
		float nextTextY = tableTopY - (table.getRowHeight() / 2)
						- ((table.getTextFont().getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * table.getFontSize()) / 4);

		// Write column headers
		writeHeaderContentLine(table.getColumns(), contentStream, nextTextX, nextTextY, table, PDType1Font.HELVETICA_BOLD, 8f, 1);

		List<Column> columns = table.getColumns();
		for (Column column : columns) {
			header = (column.getName().size() > header) ? column.getName().size() : header;
		}
		nextTextY -= table.getRowHeight() * (header + 2); //move rowY down or up here
		nextTextY += 20;
		nextTextX = table.getMargin() + table.getCellMargin();

		// Write content
		nextTextY += table.getRowHeight();
		for (int i = 0; i < currentPageContent.length; i++) {

			writeContentLine(currentPageContent[i], contentStream, nextTextX, nextTextY, table, PDType1Font.HELVETICA, 8f, 0);
			nextTextY -= table.getRowHeight();
			nextTextX = table.getMargin() + table.getCellMargin();
		}

		//contentStream.close();
	}

	private void writeHeaderContentLine(List<Column> columns, PDPageContentStream contentStream, float nextTextX, float nextTextY,
					Table table, PDType1Font fontType, float fontSize, int columnIdentifies) throws IOException {
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			List<String> splitedHeader = columns.get(i).getName();

			contentStream.setFont(fontType, fontSize);
			int middleColumnIdentifierFlag = 0;
			contentStream.beginText();
			middleColumnIdentifierFlag = 1;
			contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
			for (String text : splitedHeader) {
				contentStream.drawString(text != null ? text : "NA");
				contentStream.appendRawCommands("T*\n");
				contentStream.moveTextPositionByAmount(0, -12);
			}

			nextTextY -= (header * 12);
			lastHeaderLineYPosition = nextTextY;
			nextTextY += (header * 12);
			nextTextX += table.getColumns().get(i).getWidth();
			contentStream.endText();
		}
	}

	private void writeContentLine(String[] lineContent, PDPageContentStream contentStream, float nextTextX, float nextTextY,
					Table table, PDType1Font fontType, float fontSize, int columnIdentifies) throws IOException {
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			String text = lineContent[i];
			List<String> splitedText = TextFormatter.stringSplit(text, 10, 0);
			contentStream.beginText();
			contentStream.setFont(fontType, fontSize);
			contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
			contentStream.drawString(text != null ? text : "NA");
			contentStream.endText();
			nextTextX += table.getColumns().get(i).getWidth();

		}
		previousContentY = nextTextY;
	}

	private void drawTableGrid(Table table, String[][] currentPageContent, PDPageContentStream contentStream, float tableTopY)
					throws IOException {
		// Draw row lines
		float nextY = tableTopY;

		int header = 0;
		List<Column> columns = table.getColumns();
		for (Column column : columns) {
			header = (column.getName().size() > header) ? column.getName().size() : header;
		}
		int currentPageLength = currentPageContent.length + header + 1; // to add extra row here

		for (int i = 0; i <= currentPageLength; i++) {
			if (i == 0) {
				contentStream.drawLine(table.getMargin(), nextY, table.getMargin() + table.getWidth(), nextY);
			}
			if (i > (header - 1)) {
				nextY += table.getRowHeight();
				contentStream.drawLine(table.getMargin(), nextY, table.getMargin() + table.getWidth(), nextY);
				nextY -= table.getRowHeight();
			}
			nextY -= table.getRowHeight();
		}

		// Draw column lines
		final float tableYLength = table.getRowHeight() + (table.getRowHeight() * (currentPageLength - 1));
		final float tableBottomY = (tableTopY - tableYLength) + table.getRowHeight();
		float nextX = table.getMargin();
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			contentStream.drawLine(nextX, tableTopY, nextX, tableBottomY);
			nextX += table.getColumns().get(i).getWidth();
		}

		if (currentPageLength != 0)
		{
			contentStream.drawLine(nextX, tableTopY, nextX, tableBottomY);
		}

	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		//PDPageContentStream contentStream = new PDPageContentStream(doc, page,true,true);
		doc.addPage(page1);
		return page1;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentY < 275) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentY = 780;
		}
		return contentStream1;
	}

}
