package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.isg.service.user.model.onlineadmission.AcademicDetail;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;

public class AcademicDetails {

	/**
	 * @param args
	 */
	PDDocument doc = null;
	PDPage page = null;
	public int RECTANGLE_WIDTH = 535;
	public int RECTANGLE_HEIGHT = 12;
	public float X0 = 40f;
	public float marginX = 5f;
	public float CONTENT_FONT_SIZE = 9;
	public float ROW_WIDTH = 535;
	private final float ROW_HEIGHT = 20;
	int remarksFlag = 0;
	private final PDRectangle PAGE_SIZE = PDPage.PAGE_SIZE_A4;
	private final float MARGIN = 50;
	private final boolean IS_LANDSCAPE = false;
	float LASTHEADERLINEYPOSITION = 0;
	float LASTROWLINEYPOSITION = 0;
	public int headerMaxSize = 0;
	float previousContentY;
	float nextTextY;
	// Font configuration
	private final PDFont TEXT_FONT = PDType1Font.HELVETICA;
	private final float FONT_SIZE = 9;

	// Table configuration
	private final float CELL_MARGIN = 2;
	int rowCounter = 0;
	int maxRowCounter = 0;
	public float UPPER_CONTENT_HEIGHT;
	float titleHeight;
	List<Float> columnSplitPosition = new ArrayList<Float>();
	List<Float> rowYPosition = new ArrayList<Float>();

	/************************ Drawing the data for Qualifying Exam Details **********/

	String formFor;

	public void setFormFor(String formFor) {
		this.formFor = formFor;
	}

	public Object[] addAcademicDetails(PDDocument doc, Object[] previousContenyYAndContentStream, OnlineAdmission onlineAdmission) throws IOException {

		PDPageContentStream contentStream = (PDPageContentStream) previousContenyYAndContentStream[1];

		previousContentY = ((float) previousContenyYAndContentStream[0]) - 20;

		contentStream = checkEndOfPage(contentStream, doc, 0);
		contentStream.setNonStrokingColor(Color.LIGHT_GRAY);

		/******************* to draw Qualifying Exam Details text ******************/
		contentStream.fillRect(X0 + marginX, previousContentY -= 18, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0 + marginX, previousContentY += 3);
		contentStream.drawString("Academic Exam Details :");
		contentStream.endText();
		contentStream.drawLine(X0 + marginX, previousContentY -= 3, X0 + marginX + ROW_WIDTH, previousContentY);

		List<AcademicDetail> academicExamDetails = new ArrayList<AcademicDetail>();
		academicExamDetails = onlineAdmission.getAcademicDetails();

		contentStream = drawTable(contentStream, academicExamDetails);

		previousContenyYAndContentStream[0] = previousContentY - 40;
		previousContenyYAndContentStream[1] = contentStream;

		return previousContenyYAndContentStream;

		/****************** to draw content of Academic Exam Details ************/
	}

	private PDPageContentStream generateContentStream(PDPageContentStream contentStream, Table table) throws IOException {
		// User transformation matrix to change the reference when drawing.
		// This is necessary for the landscape position to draw correctly
		if (table.isLandscape()) {
			contentStream.concatenate2CTM(0, 1, -1, 0, table.getPageSize().getWidth(), 0);
		}
		contentStream.setFont(table.getTextFont(), table.getFontSize());
		return contentStream;
	}

	private Table createSubjectContent(List<AcademicDetail> acdemicExamDetailsList) {

		// Total size of columns must not be greater than table width.
		UPPER_CONTENT_HEIGHT = PDPage.PAGE_SIZE_A4.getUpperRightY() - 200;
		String[][] content;
		List<Column> columns = new ArrayList<Column>();
		// Start print logic
		content = new String[acdemicExamDetailsList.size()][9]; //set here academicExamDetails.size() if Academic details are increase in row and take the object of Academic exam details
		int i = 0;
		int j = 0;
		int srNo = 1;
		for (int k = 0; k < acdemicExamDetailsList.size(); k++) { //adding data to content condition - any on row of that column have data
			AcademicDetail academicDetails = acdemicExamDetailsList.get(k);
			if (academicDetails.getExam() != null) {
				content[i][j] = srNo + "";
				content[i][++j] = academicDetails.getExam();
				srNo++;
			}
			if (academicDetails.getPassingYear() != null) {
				content[i][++j] = academicDetails.getPassingYear();
			}
			if (academicDetails.getSchool() != null) {
				content[i][++j] = academicDetails.getSchool();
			}
			if (academicDetails.getBoardUniversity() != null) {
				content[i][++j] = academicDetails.getBoardUniversity();
			}
			if (academicDetails.getObtainedMarks() != null) {
				content[i][++j] = academicDetails.getObtainedMarks().toString();
			}
			if (academicDetails.getTotalMarks() != null) {
				content[i][++j] = academicDetails.getTotalMarks().toString();
			}
			if (academicDetails.getExam() != null) {
				content[i][++j] = academicDetails.getPercentage().toString();
			}

			//Adjust array counter
			i++;
			j = 0;
		}
		columnSplitPosition.add(5f); //Setting of Header content and Row Content split position
		columnSplitPosition.add(10f);
		columnSplitPosition.add(10f);
		columnSplitPosition.add(20f);
		columnSplitPosition.add(20f);
		columnSplitPosition.add(10f);
		columnSplitPosition.add(10f);
		columnSplitPosition.add(10f);
		columnSplitPosition.add(10f);

		List<String> srNoSplitedList = TextFormatter.stringSplit("Sr No.", 10, 0); //setting Column Name with the split position
		List<String> lastExamSplitedList = TextFormatter.stringSplit("Last Exam", 10, 0);
		List<String> passingYearSplitedList = TextFormatter.stringSplit("Passing Year", 10, 0);
		//List<String> schoolAttendedSplitedList = TextFormatter.stringSplit("School Attended", 10, 0);

		//List<String> boardAndUniversitySplitedList = TextFormatter.stringSplit("Board / University", 10, 0);

		List<String> marksObtainSplitedList = TextFormatter.stringSplit("Marks Obtained", 10, 0);

		List<String> totalmarksSplitedList = TextFormatter.stringSplit("Out Of", 10, 0);

		List<String> percentageSplitedList = TextFormatter.stringSplit("Percentage", 10, 0);

		int columnsLength;
		columns.add(new Column(srNoSplitedList, 40)); // Adding of ColumnList with Column Position
		columns.add(new Column(lastExamSplitedList, 70));
		columns.add(new Column(passingYearSplitedList, 40));
	//columns.add(new Column(schoolAttendedSplitedList, 120));
		//columns.add(new Column(boardAndUniversitySplitedList, 120));
		columns.add(new Column(marksObtainSplitedList, 45));
		columns.add(new Column(totalmarksSplitedList, 35));
		columns.add(new Column(percentageSplitedList, 50));

		//Code for make length of all column equal
		columnsLength = (columns.size() == 0) ? 1 : columns.size();
		int equalWidth = (500 / columnsLength); //adjust value according to text 
		//columns.remove(columnsLength-1);
		/*if(columns.size() != 0)
		{
			for(Column c : columns)
			{
				//c.setWidth(equalWidth);
			}
		}*/

		float tableHeight = ROW_HEIGHT * (2);
		Table table = new TableBuilder().setCellMargin(CELL_MARGIN)
						.setColumns(columns).setContent(content).setHeight(tableHeight)
						.setNumberOfRows(content.length).setRowHeight(ROW_HEIGHT)
						.setMargin(MARGIN).setPageSize(PAGE_SIZE)
						.setEqualWidth(equalWidth)
						.setLandscape(IS_LANDSCAPE).setTextFont(TEXT_FONT)
						.setFontSize(FONT_SIZE).build();
		List<Column> columns1 = table.getColumns();
		for (Column column : columns1) {
			headerMaxSize = (column.getName().size() > headerMaxSize) ? column.getName().size() : headerMaxSize;
		}
		return table;
	}

	//table for South Calcatta Ploytechnic 
	private Table createScpSubjectContent(List<AcademicDetail> acdemicExamDetailsList) {

		// Total size of columns must not be greater than table width.
		UPPER_CONTENT_HEIGHT = PDPage.PAGE_SIZE_A4.getUpperRightY() - 200;
		String[][] content;
		List<Column> columns = new ArrayList<Column>();
		// Start print logic
		content = new String[acdemicExamDetailsList.size()][5]; //set here academicExamDetails.size() if Academic details are increase in row and take the object of Academic exam details
		int i = 0;
		int j = 0;
		for (int k = 0; k < acdemicExamDetailsList.size(); k++) { //adding data to content condition - any on row of that column have data
			AcademicDetail academicDetails = acdemicExamDetailsList.get(k);
			if (academicDetails.getExam() != null) {
				content[i][j] = academicDetails.getExam();
			}
			if (academicDetails.getPassingYear() != null) {
				content[i][++j] = academicDetails.getPassingYear();
			}
			if (academicDetails.getBoardUniversity() != null) {
				content[i][++j] = academicDetails.getBoardUniversity();
			}

			if (academicDetails.getPercentage() != null) {
				content[i][++j] = academicDetails.getPercentage().toString();
			}

			String obtainedMarks = "";
			Map<String, Double> subjectMarks = academicDetails.getSubjectMarks();
			if (subjectMarks != null && !subjectMarks.isEmpty()) {
				obtainedMarks = "Physical Sc= " + subjectMarks.get("physicalSc") + ", Maths= " + subjectMarks.get("maths");
			}
			else {
				obtainedMarks = academicDetails.getObtainedMarks() + "";
			}
			content[i][++j] = obtainedMarks;

			//Adjust array counter
			i++;
			j = 0;
		}
		columnSplitPosition.add(20f); //Setting of Header content and Row Content split position
		columnSplitPosition.add(20f);
		columnSplitPosition.add(20f);
		columnSplitPosition.add(20f);
		columnSplitPosition.add(50f);

		List<String> examinationSplitedList = TextFormatter.stringSplit("Examination", 20, 0); //setting Column Name with the split position
		List<String> passingYearSplitedList = TextFormatter.stringSplit("Passing Year", 20, 0);
		List<String> boardAndUniversitySplitedList = TextFormatter.stringSplit("Board / University", 20, 0);

		List<String> percentageSplitedList = TextFormatter.stringSplit("Percentage", 20, 0);
		List<String> marksObtainSplitedList = TextFormatter.stringSplit("Marks Obtained", 20, 0);
		int columnsLength;
		columns.add(new Column(examinationSplitedList, 100)); // Adding of ColumnList with Column Position
		columns.add(new Column(passingYearSplitedList, 100));
		columns.add(new Column(boardAndUniversitySplitedList, 120));
		columns.add(new Column(percentageSplitedList, 80));
		columns.add(new Column(marksObtainSplitedList, 120));

		//Code for make length of all column equal
		columnsLength = (columns.size() == 0) ? 1 : columns.size();
		int equalWidth = (500 / columnsLength); //adjust value according to text 

		float tableHeight = ROW_HEIGHT * (2);
		Table table = new TableBuilder().setCellMargin(CELL_MARGIN)
						.setColumns(columns).setContent(content).setHeight(tableHeight)
						.setNumberOfRows(content.length).setRowHeight(ROW_HEIGHT)
						.setMargin(MARGIN).setPageSize(PAGE_SIZE)
						.setEqualWidth(equalWidth)
						.setLandscape(IS_LANDSCAPE).setTextFont(TEXT_FONT)
						.setFontSize(FONT_SIZE).build();
		List<Column> columns1 = table.getColumns();
		for (Column column : columns1) {
			headerMaxSize = (column.getName().size() > headerMaxSize) ? column.getName().size() : headerMaxSize;
		}
		return table;
	}

	public PDPageContentStream drawTable(PDPageContentStream contentStream, List<AcademicDetail> academicExamDetailsList) throws IOException {
		// for dalmia pdf table
		Table table = null;
		//for SCP pdf view table
		if (formFor != null && formFor.equals("scp")) {
			table = createScpSubjectContent(academicExamDetailsList);
		}
		else {
			table = createSubjectContent(academicExamDetailsList);
		}

		// Calculate pagination
		Integer rowsPerPage = table.getContent().length; // subtract
		contentStream = generateContentStream(contentStream, table);
		String[][] currentPageContent = getContentForCurrentPage(table, rowsPerPage, 0);
		drawCurrentPage(table, currentPageContent, contentStream, previousContentY);

		return contentStream;
	}

	private String[][] getContentForCurrentPage(Table table, Integer rowsPerPage, int pageCount) {
		int startRange = pageCount * rowsPerPage;
		int endRange = (pageCount * rowsPerPage) + rowsPerPage;
		if (endRange > table.getNumberOfRows()) {
			endRange = table.getNumberOfRows();
		}
		return Arrays.copyOfRange(table.getContent(), startRange, endRange);
	}

	private void drawCurrentPage(Table table, String[][] currentPageContent, PDPageContentStream contentStream, float previousContentY)
					throws IOException {
		float nextTextX = MARGIN + marginX;
		nextTextY = previousContentY - 30;
		float tableTopY = previousContentY - 20;
		writeHeaderContentLine(table.getColumns(), contentStream, nextTextX, nextTextY, table, PDType1Font.HELVETICA, 8, 0);

		for (int i = 0; i < currentPageContent.length; i++) {

			writeContentLine(currentPageContent[i], contentStream, nextTextX, LASTHEADERLINEYPOSITION, table, PDType1Font.HELVETICA, 8f, 0);
			//nextTextY += table.getRowHeight();
			nextTextX = MARGIN + marginX;
		}

		drawTableGrid(table, currentPageContent, contentStream, tableTopY);
	}

	private void writeHeaderContentLine(List<Column> columns, PDPageContentStream contentStream, float nextTextX, float nextTextY1,
					Table table, PDType1Font fontType, float fontSize, int columnIdentifies) throws IOException {
		titleHeight = TEXT_FONT.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * 8;
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			List<String> splitedHeader = columns.get(i).getName();
			contentStream.setFont(fontType, fontSize);
			contentStream.beginText();
			contentStream.moveTextPositionByAmount(nextTextX, nextTextY1);
			for (String text : splitedHeader) {
				contentStream.drawString(text != null ? text : "NA");
				contentStream.appendRawCommands("T*\n");
				contentStream.moveTextPositionByAmount(0, -12);
			}
			nextTextX += table.getColumns().get(i).getWidth();
			contentStream.endText();
		}
		nextTextY -= (headerMaxSize * 12);//+ (titleHeight * headerMaxSize);
		LASTHEADERLINEYPOSITION = nextTextY;
		rowYPosition.add(LASTHEADERLINEYPOSITION + 10);
	}

	private void writeContentLine(String[] lineContent, PDPageContentStream contentStream, float nextTextX, float nextTextY1,
					Table table, PDType1Font fontType, float fontSize, int columnIdentifies) throws IOException {
		int rowMaxSize = 0;
		maxRowCounter = 0;
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			rowCounter = 0;
			String text = lineContent[i];

			List<String> splitedText = TextFormatter.stringSplit(text, columnSplitPosition.get(i).intValue(), 0);
			contentStream.beginText();
			contentStream.setFont(fontType, fontSize);
			contentStream.moveTextPositionByAmount(nextTextX, nextTextY1);
			rowMaxSize = (splitedText.size() > rowMaxSize) ? splitedText.size() : rowMaxSize;
			for (String lineBylineText : splitedText) {
				contentStream.drawString(lineBylineText != null ? lineBylineText : " ");
				contentStream.appendRawCommands("T*\n");
				contentStream.moveTextPositionByAmount(0, -12);
				rowCounter++;
			}
			maxRowCounter = (rowCounter > maxRowCounter) ? rowCounter : maxRowCounter;
			nextTextX += table.getColumns().get(i).getWidth();
			contentStream.endText();
		}
		nextTextY -= (maxRowCounter * 12);//+ (titleHeight * rowMaxSize);
		LASTHEADERLINEYPOSITION = nextTextY;
		rowYPosition.add(LASTHEADERLINEYPOSITION + 10);
		previousContentY = LASTHEADERLINEYPOSITION + 10;
		LASTROWLINEYPOSITION = nextTextY + 10;
	}

	private void drawTableGrid(Table table, String[][] currentPageContent, PDPageContentStream contentStream, float tableTopY)
					throws IOException {
		// Draw row lines
		float nextY = tableTopY;

		int header = 0;
		List<Column> columns = table.getColumns();
		for (Column column : columns) {
			header = (column.getName().size() > header) ? column.getName().size() : header;
		}
		int currentPageLength = currentPageContent.length;
		for (int i = 0; i <= currentPageLength; i++) {
			contentStream.drawLine(table.getMargin(), rowYPosition.get(0), table.getMargin() + table.getWidth(), rowYPosition.get(0));
			if (i == 0) {
				contentStream.drawLine(table.getMargin(), nextY, table.getMargin() + table.getWidth(), nextY);
				nextY -= (table.getRowHeight());
			}
			else {
				contentStream.drawLine(table.getMargin(), rowYPosition.get(i), table.getMargin() + table.getWidth(), rowYPosition.get(i));
			}
		}

		// Draw column lines
		final float tableYLength = table.getRowHeight() + (table.getRowHeight() * (currentPageLength - 1));
		final float tableBottomY = (tableTopY - tableYLength) + table.getRowHeight();
		float nextX = table.getMargin();
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			contentStream.drawLine(nextX, tableTopY, nextX, LASTROWLINEYPOSITION);
			nextX += table.getColumns().get(i).getWidth();
		}

		if (currentPageLength != 0)
		{
			contentStream.drawLine(nextX, tableTopY, nextX, LASTROWLINEYPOSITION);
		}
	}

	private PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		//PDPageContentStream contentStream = new PDPageContentStream(doc, page,true,true);
		doc.addPage(page1);
		return page1;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc, int beginTextFlag) throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentY < 200) {
			if (beginTextFlag == 1) {
				contentStream.endText();
			}
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentY = 780;
			if (beginTextFlag == 1) {
				contentStream1.beginText();
				beginTextFlag = 1;
			}
		}
		return contentStream1;
	}

}
