package com.isg.service.user.factory.pdf.onlineadmission;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.isg.service.user.model.onlineadmission.OnlineAdmission;

public class TermsAndCondition {

	/**
	 * @param args
	 */
	PDDocument doc;
	private final float CONTENT_FONT_SIZE = 10;
	private final float X0 = 40;
	PDPage page;

	public void termsAndConditionFirstPage(PDDocument doc, OnlineAdmission onlineAdmission) throws IOException {

		DrawString drawString = new DrawString();

		page = new PDPage();
		doc.addPage(page);
		PDFont font = PDType1Font.HELVETICA_BOLD;
		PDPageContentStream contentStream = new PDPageContentStream(doc, page, true, true);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String test = "ppppppppppppppaaaaawwwqqwwwwwaaqqqwwaaaaaaaaaaaaaaaaaaaawwwwwwwwwwwwwwwwwwwwwwwwwwwwwwqqqqqqqqqqqqqqqqqqqqqqqqqqq";

		int fontSize = 16; // Or whatever font size you want.
		float titleWidth = font.getStringWidth(test) / 1000 * fontSize;
		float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
		float limit = (-((page.getMediaBox().getWidth() - titleWidth) / 2)) - 175;

		int previousContentY = 780;

		String paragraph = "I hereby agree, if admitted to abide by the rules and regulations at present in force, or that may hereafter be made for the administration of the college and I undertake that so long as I am the student of the college, I will do nothing either inside or outside the college, that will interfere with its orderly working, discipline and anti-ragging policy.";
		String paragraph1 = "Passing and marks certificates of university degree examination / board examination passed must be producedat the time of admission. Transfer certificate from the previous college / school leaving certificate / finaleligibility certificate of the Mumbai University, as the case may be must be produced at the time of admission. Otherwise, the admission shall be provisional. If transfer /eligibility certificate is not submitted to the college, the admission is liable to be cancelled. If my admission is cancelled as per the rules and regulations of the  college, I shall not have any grievance whatsoever.";
		String paragraph2 = "I hereby solemnly over and undertake that I will not indulge in any behaviour or act may be constituted as misconduct which includes;";
		String paragraph3 = "1) Criminal activity with support of outsiders with criminals and anti-social elements of society";
		String paragraph4 = "2) Violation of Information Technology Act, cyber offences like creating /participating in websites/ blogs condemning institute";
		String paragraph5 = "3) Defamation or scandalous statements against the fellow students, institute and institute authorities or staff";
		String paragraph6 = "I will not participate in or abet or propagate through any act of commission or omission that may constituted as misconduct.";
		String paragraph7 = "The above activities are only illustrative and not exhaustive or inclusive of all acts of misconduct.";
		String paragraph8 = "I hereby undertake to attend minimum 75% of lectures in the college. I am aware that if I failed to do so, my term will not be granted.";
		String paragraph9 = "";

		TermsAndCondition paragraphFormating = new TermsAndCondition();
		TermsAndCondition paragraphFormating1 = new TermsAndCondition();
		TermsAndCondition paragraphFormating2 = new TermsAndCondition();
		TermsAndCondition paragraphFormating3 = new TermsAndCondition();
		TermsAndCondition paragraphFormating4 = new TermsAndCondition();
		TermsAndCondition paragraphFormating5 = new TermsAndCondition();
		TermsAndCondition paragraphFormating6 = new TermsAndCondition();
		TermsAndCondition paragraphFormating7 = new TermsAndCondition();
		TermsAndCondition paragraphFormating8 = new TermsAndCondition();
		TermsAndCondition paragraphFormating9 = new TermsAndCondition();

		int textWrapLimit = 125;
		List<String> splitedpParagraph = paragraphFormating.stringSplit(paragraph, textWrapLimit, 0);
		splitedpParagraph = paragraphFormating.formatingText(splitedpParagraph, textWrapLimit, font, fontSize, page);

		List<String> splitedpParagraph1 = paragraphFormating1.stringSplit(paragraph1, textWrapLimit, 0);
		splitedpParagraph1 = paragraphFormating1.formatingText(splitedpParagraph1, textWrapLimit, font, fontSize, page);

		List<String> splitedpParagraph2 = paragraphFormating2.stringSplit(paragraph2, textWrapLimit, 0);
		splitedpParagraph2 = paragraphFormating2.formatingText(splitedpParagraph2, textWrapLimit, font, fontSize, page);

		List<String> splitedpParagraph3 = paragraphFormating3.stringSplit(paragraph3, textWrapLimit, 0);
		splitedpParagraph3 = paragraphFormating3.formatingText(splitedpParagraph3, textWrapLimit, font, fontSize, page);

		List<String> splitedpParagraph4 = paragraphFormating4.stringSplit(paragraph4, textWrapLimit, 0);
		splitedpParagraph4 = paragraphFormating4.formatingText(splitedpParagraph4, textWrapLimit, font, fontSize, page);

		List<String> splitedpParagraph5 = paragraphFormating5.stringSplit(paragraph5, textWrapLimit, 0);
		splitedpParagraph5 = paragraphFormating5.formatingText(splitedpParagraph5, textWrapLimit, font, fontSize, page);

		List<String> splitedpParagraph6 = paragraphFormating6.stringSplit(paragraph6, textWrapLimit, 0);
		splitedpParagraph6 = paragraphFormating6.formatingText(splitedpParagraph6, textWrapLimit, font, fontSize, page);

		List<String> splitedpParagraph7 = paragraphFormating7.stringSplit(paragraph7, textWrapLimit, 0);
		splitedpParagraph7 = paragraphFormating7.formatingText(splitedpParagraph7, textWrapLimit, font, fontSize, page);

		List<String> splitedpParagraph8 = paragraphFormating8.stringSplit(paragraph8, textWrapLimit, 0);
		splitedpParagraph8 = paragraphFormating8.formatingText(splitedpParagraph8, textWrapLimit, font, fontSize, page);

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 20);
		for (String oneLine : splitedpParagraph) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 60);
		for (String oneLine : splitedpParagraph1) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 110);
		for (String oneLine : splitedpParagraph2) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 30);
		for (String oneLine : splitedpParagraph3) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 20); // for 2) point
		for (String oneLine : splitedpParagraph4) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 30); // for 3) point
		for (String oneLine : splitedpParagraph5) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 20);
		for (String oneLine : splitedpParagraph6) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 18);
		for (String oneLine : splitedpParagraph7) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(X0, previousContentY -= 18);
		for (String oneLine : splitedpParagraph8) {
			contentStream.drawString(oneLine);
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -14);
		}
		contentStream.endText();

		drawString.drawString(contentStream, 30, 220, PDType1Font.HELVETICA, CONTENT_FONT_SIZE, "Date  :  " + dateFormat.format(date));
		contentStream.drawLine(25, 175, 125, 175);
		drawString.drawString(contentStream, 30, 160, PDType1Font.HELVETICA, CONTENT_FONT_SIZE, "Signature Of Student");
		contentStream.drawLine(415, 175, 560, 175);
		drawString.drawString(contentStream, 415, 160, PDType1Font.HELVETICA, CONTENT_FONT_SIZE, "Signature Of Parents/Guardians");
		drawString.drawString(contentStream, 30, 130, PDType1Font.HELVETICA, CONTENT_FONT_SIZE, "Verified By :");
		contentStream.drawLine(90, 130, 270, 130);
		contentStream.close();
	}

	int flag;
	public List<String> list = new ArrayList<String>();

	public List<String> stringSplit(String text, float limit, int flag) // method to split text or wrap text
	{

		if (flag == 0) {
			list.clear();
			flag = 1;
		}
		//List<String> innerStringSplit(String text1, int limit1){
		int thislimit = (int) limit;
		int spacePosition = 0, z = 0;
		if (text.length() > thislimit) {
			if (text.substring(0, thislimit).contains(" ")) {
				for (int i = z; i < thislimit; i++) {
					if (text.charAt(i) == ' ') {
						spacePosition = i;
					}
				}
				if (text.charAt(spacePosition) == ' ') {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition + 1, text.length()), thislimit, 1);
				} else {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition, text.length()), thislimit, 1);
				}
			} else {
				list.add(text.substring(0, thislimit));
				stringSplit(text.substring(thislimit, text.length()), thislimit, 1);
			}
		} else {
			list.add(text);
		}
		return list;
		//}
	}

	public List<String> formatingText(List<String> splitedpParagraph, float limit, PDFont font, int fontSize, PDPage page) throws IOException {
		/********** logic to include space in string **********/

		/*for (int i = 0; i < splitedpParagraph.size(); i++) {
			if (splitedpParagraph.get(i).contains(" ")) {
				String tempText1 = splitedpParagraph.get(i);
				tempText1 = tempText1.replace(" ", "     ");
				splitedpParagraph.remove(i);
				splitedpParagraph.add(i, tempText1);
			}
		}*/

		for (int i = 0; i < splitedpParagraph.size(); i++) {
			/*Matrix textPositionSt;
			Matrix textPositionEnd;
			float maxFontH;
			float[] individualWidths;
			float spaceWidth;
			String string;
			PDFont currentFont;
			float fontSizeValue;
			int fontSizeInPt;
			TextPosition textPosition =  new TextPosition(page, textPositionSt, textPositionEnd, maxFontH, individualWidths, spaceWidth, string, currentFont, fontSizeValue, fontSizeInPt, ws);
			*/String tempText1 = splitedpParagraph.get(i);
			float titleWidth = font.getStringWidth(tempText1) / 1000 * fontSize;
			float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
			float limit1 = -((page.getMediaBox().getWidth() - titleWidth) / 2);
			float remainingPixels = -(limit - limit1);
			//System.out.println("Remaining pixels are ::"+remainingPixels);
			String pixcelString = " ";
			/*for(int j = 1; j < remainingPixels; j++){
				pixcelString += " ";
			}*/
			if (limit1 < limit) {
				if (remainingPixels > 0) {
					StringBuffer temp2 = new StringBuffer(tempText1);
					;
					for (int k = 0; k < temp2.length(); k++) {
						if (remainingPixels > 0) {
							if (temp2.charAt(k) == ' ') {
								temp2.replace(k, k + 1, "  ");
								remainingPixels--;
							}
						}
					}

					splitedpParagraph.remove(i);
					splitedpParagraph.add(i, temp2.toString());
				}
			}
		}

		return splitedpParagraph;
	}

}
