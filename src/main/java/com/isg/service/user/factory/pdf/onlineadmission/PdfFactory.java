package com.isg.service.user.factory.pdf.onlineadmission;

import org.apache.log4j.Logger;

public class PdfFactory {

	private static final Logger log = Logger.getLogger(PdfFactory.class);

	public static PdfBuilder getPdfBuilderInstance(String templateName) {
		if (templateName.equalsIgnoreCase("dalmia")) {
			return new DalmiaPdfBuilder();
		} else if (templateName.equalsIgnoreCase("pathfinder")) {
			return new PathfinderPdfBuilder();
		} else if (templateName.equalsIgnoreCase("rosemine")) {
			return new RoseminePdfBuilder();
		} else if (templateName.equalsIgnoreCase("hkimsr")) {
			return new HkimsrPdfBuilder();
		}
		else if (templateName.equalsIgnoreCase("vbu")) {
			return new VbuPdfBuilder();
		}
		else if (templateName.equalsIgnoreCase("scp")) {
			return new ScpPdfBuilder();
		} else if (templateName.equalsIgnoreCase("iitm")) {
			return new IITMPdfBuilder();
		} else if (templateName.equalsIgnoreCase("gcekarad")) {
			return new GCEKaradPdfBuilder();
		} else if (templateName.equalsIgnoreCase("ascend")) {
			return new AscendPdfBuilder();
		} else if (templateName.equalsIgnoreCase("vvs")) {
			return new VVSPdfBuilder();
		}else if (templateName.equalsIgnoreCase("lhs")) {
			return new LHSPdfBuilder();
		}
		else if(templateName.equalsIgnoreCase("lhe")){
			
			return new LHEPdfBuilder();
		} else if (templateName.equalsIgnoreCase("etoos")) {
			return new ETOSPdfBuilder();
		} else if (templateName.equalsIgnoreCase("rtac")) {
			return new RTACPdfBuilder();
		} else if (templateName.equalsIgnoreCase("iihmr")) {
			return new IIHMRPdfBuilder();
		}else if (templateName.equalsIgnoreCase("iitkD")) {
			return new IITKDPdfBuilder();
		}
		else if (templateName.equalsIgnoreCase("iitkR")) {
			return new IITKRegistrationPDFBuilder();
		}
		else if (templateName.equalsIgnoreCase("iitkM")) {
			return new IITKMerchandisePDFBuilder();
		}
		else if (templateName.equalsIgnoreCase("vvsjunior")) {
			return new VVSJUNIORPdfBulider();
		}
		else {
			log.error("PDF Factory is not configured for template: " + templateName);
		}

		return null;
	}

}
