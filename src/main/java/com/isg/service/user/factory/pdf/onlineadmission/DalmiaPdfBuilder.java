package com.isg.service.user.factory.pdf.onlineadmission;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.request.Branch;

public class DalmiaPdfBuilder implements PdfBuilder {

	/**
	 * @param args
	 */
	private final int HEADER_SPLIT_POSITION = 55;
	private final float HEADER_FONT_SIZE = 14f;
	private final float CONTENT_FONT_SIZE = 9f;
	private final float X0 = 40f;
	private float Y0 = 0f;
	private final int marginX = 5;
	private final float PADDING_BOTTOM_OF_DOCUMENT = 40f;
	private float previousContentY = 0f;
	Map<String, Float> textX = new HashMap<String, Float>();
	Map<String, Float> headerCenterXPosition = new HashMap<String, Float>();
	private final String FONT_NAME = "MS Reference Sans Serif";
	PDPage pageForWidth = new PDPage();

	public static void main(String a[]) throws JsonParseException, JsonMappingException, IOException {
		String data = "{\"formNumber\":null,\"paymentStatus\":null,\"qfixReferenceNumber\":null,\"paymentDate\":null,\"modeOfPayment\":null,\"amount\":null,\"url_configuration_id\":null,\"application_id\":\"AY16-17/101458821027138\",\"applicatant_id\":\"AY16-17/101458821027138\",\"course_details\":{\"course\":{\"id\":7,\"name\":\"FYJC ( COMMERCE )\"},\"seed_programme\":{\"compulsory_subject\":{\"id\":27,\"name\":\"Development English\"},\"optional_subject\":{\"id\":30,\"name\":\"Basic Computers\"}},\"subject_preference\":{\"preference1\":null,\"preference2\":null},\"subject_preferences\":null,\"applied\":null,\"admitted\":null,\"otherCoureDetail\":null,\"course_preferences\":null,\"otherCoursePreference\":null},\"academic_details\":[{\"exam\":\"SSC\",\"passing_year\":\"2015\",\"school\":\"dvdv\",\"board_university\":\"vdv\",\"obtained_marks\":1222,\"total_marks\":2222,\"percentage\":55.0}],\"qualifying_exam__details\":{\"exam\":{\"id\":null,\"code\":\"7006\",\"name\":\"MCOM\",\"other\":null},\"passing_year\":\"2011\",\"passing_month\":\"July\",\"school_college\":\"feff\",\"board_university\":{\"id\":6,\"name\":\"Maharashtra State Board of Secondary and Higher Secondary Education\",\"other\":null},\"obtained_marks\":232,\"total_marks\":333,\"percentage\":69.67,\"education_gap\":\"NO\",\"grade\":null,\"exam_seat_no\":\"232232\",\"previous_stream\":null,\"applicantfrom\":\"\"},\"candidate_details\":{\"name\":{\"surname\":\"dfvdfvgb\",\"firstname\":\"fv\",\"middlename\":\"fvdfvf\"},\"printed_name_on_marksheet\":{\"surname\":\"fvfvfv\",\"firstname\":\"vfvfvfvf\",\"middlename\":\"vfvfvff\"},\"gender\":{\"id\":1,\"name\":\"MALE\"},\"marital_status\":{\"id\":2,\"name\":\"UNMARRIED\"},\"dateofbirth\":\"2017-05-27T18:30:00.000Z\",\"placeofbirth\":null,\"caste_details\":{\"caste\":{\"id\":7,\"name\":\"NOMADIC TRIBES-C\"},\"subcaste\":{\"id\":302,\"name\":\"HINDU DHANGAR\",\"other\":null},\"religion\":{\"id\":7,\"name\":\"MARATHA\"},\"minority\":\"NO\"},\"physically_handicapped\":null,\"form_details\":{\"application_form_no\":\"2222222222\","
						+
						"\"mkcl_registration_no\":null,\"physical_form_no\":\"222222222222\"},\"mother_tounge\":\"ggg\",\"foreign_student\":\"NO\",\"aadhaaar_card\":232323232323,\"bank_details\":{\"bank_account\":13322434343434,\"bank_name\":\"ffrfgb\",\"transaction_type\":\"OFFLINE\"},\"employment_details\":null,\"ncc_pref\":\"NOT_INTERESTED\",\"last_school_name\":\"gcbncg\",\"sports_details\":null,\"social_reservation\":null,\"identification_mark\":null,\"hobbies\":\"fbggbggb\",\"blood_group\":null,\"native_place_address\":\"bvgbgg\",\"languages_known\":null,\"nationality\":null,\"educationalqualification\":null,\"category\":null,\"locations\":null,\"otherLocation\":null,\"course\":null,\"academic_year\":null,\"academic_term\":null},\"contact_details\":{\"personal_mobile_number\":\"3434444343\",\"personal_email\":\"fvg@uud.dk\",\"parent_contact_number\":null},\"mother_details\":{\"name\":{\"surname\":null,\"firstname\":\"fvfvfvfv\",\"middlename\":null},\"employment_details\":{\"employer_name\":\"bgbgb\",\"designation\":\"gbgb\",\"occupation\":\"gbgbg\",\"annual_income\":null,\"address\":null},\"email\":null,\"citizenship\":null,\"age\":0,\"school_detail\":null,\"college_detail\":null,\"primary_phone\":null,\"secondary_phone\":null,\"hobbies\":null,\"nationality\":null},\"father_details\":{\"name\":{\"surname\":\"vfvfvf\",\"firstname\":\"vfvfv\",\"middlename\":\"ffvfvf\"},\"employment_details\":{\"employer_name\":\"bbggbbg\",\"designation\":\"gbgbg\",\"occupation\":\"gbggbbb\",\"annual_income\":\"454454\",\"address\":null},\"email\":\"nhn@hy.hh\",\"citizenship\":null,\"age\":0,\"school_detail\":null,\"college_detail\":null,\"primary_phone\":null,\"secondary_phone\":null,\"hobbies\":null,\"nationality\":null},\"gaurdian_details\":{\"name\":null,\"candidaterelation\":\"eff\",\"email\":null},\"guardian_details\":{\"name\":null,\"candidaterelation\":\"eff\",\"email\":null},\"address_details\":{\"local_address\":{\"address_line1\":\"rfg\",\"house_number\":null,\"area\":null,\"pincode\":232232,\"country\":{\"id\":1,\"name\":\"INDIA\",\"other\":null},\"state\":{\"id\":8,\"name\":\"RAJASTHAN\",\"other\":null},\"district\":{\"id\":99,\"name\":\"Alwar\"},\"taluka\":{\"id\":524,\"name\":\"Kishangarh Bas\",\"other\":null}},\"permanent_address\":{\"address_line1\":\"rfg\",\"house_number\":null,\"area\":null,\"pincode\":232232,\"country\":{\"id\":1,\"name\":\"INDIA\",\"other\":null},\"state\":{\"id\":8,\"name\":\"RAJASTHAN\",\"other\":null},\"district\":{\"id\":99,\"name\":\"Alwar\"},\"taluka\":{\"id\":524,\"name\":\"Kishangarh Bas\",\"other\":null}}},\"documents\":[{\"id\":132,\"name\":\"Original School Leaving Certificate (10th Std)\"},{\"id\":133,\"name\":\"Original Mark sheet(10th Std)\"},{\"id\":134,\"name\":\"Xerox Copy of School Leaving Certificate.\"},{\"id\":135,\"name\":\"Xerox Copy Of Mark sheet.\"}],\"branch_detail\":{\"id\":101,\"name\":\"PRAHLADRAI DALMIA LIONS COLLEGE OF COMMERCE & ECONOMICS\",\"addressLine\":\"SV ROAD, SUNDAR NGR\",\"city\":\"MALAD WEST, MUMBAI\",\"area\":\"Malad West,Mumbai, Maharashtra 400064\",\"pincode\":\"400064\",\"contactnumber\":\"9999999999\",\"contactemail\":\"dalmia@eduqfix.com\",\"websiteurl\":\"www.dalmia.com\",\"districtId\":null,\"talukaId\":null,\"instituteId\":30,\"stateId\":null,\"active\":null,\"branchConfiguration\":null},\"general_comment\":null,\"sibling_details\":null}";
		OnlineAdmission asdmission = new ObjectMapper().readValue(data, OnlineAdmission.class);
		new DalmiaPdfBuilder().createPdf(asdmission, "c:/temp/images/L6d0yA5zTKeE5gwCaZN4.pdf",
						"c:/temp/images/Qfix/documents/External/r12r12gistra123ion/39/vvs.jpg", null,
						"c:/temp/images/Qfix/documents/External/r12r12gistra123ion/39/vvs.jpg");
	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String branchLogo) throws IOException {
		createPdf(onlineAdmission, pdfFilePath, studentPhotoPath, null, branchLogo);
	}

	public void createHeader(OnlineAdmission onlineAdmission, String studentPhotoPath, String branchLogo, PDDocument doc) throws IOException {

		Branch branch = onlineAdmission.getBranch();

		List<String> splitedHeaderTextCollegeName = new ArrayList<String>();
		splitedHeaderTextCollegeName = stringSplit((branch.getName() == null) ? " " : branch.getName().toUpperCase(), HEADER_SPLIT_POSITION, 0);

		PDPageContentStream headerContentStream = new PDPageContentStream(doc, (PDPage) doc.getDocumentCatalog().getAllPages().get(0), true, true, true);
		headerContentStream.setNonStrokingColor(Color.BLACK);
		PDFont font = PDType1Font.HELVETICA;

		/************************************** To draw image ***************************************/
		PDXObjectImage xImage = null;
		try {
			String imagePath = branchLogo;
			System.out.println("***********************************888"+imagePath);
			if (imagePath.toLowerCase().endsWith(".jpg")) {
				xImage = new PDJpeg(doc, new FileInputStream(imagePath));
			}
			/*else if (imagePath.toLowerCase().endsWith(".tif") || imagePath.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (RandomAccess) new RandomAccessFile(new File(image),"r"));
			}*/
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(imagePath));
				xImage = new PDPixelMap(doc, awtImage);
			}
			headerContentStream.drawXObject(xImage, (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 260,
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 80, 60, 60);
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}

		try {
			String imagePath = studentPhotoPath;
			if (imagePath.toLowerCase().endsWith(".jpg")) {
				xImage = new PDJpeg(doc, new FileInputStream(imagePath));
			}
			/*else if (imagePath.toLowerCase().endsWith(".tif") || imagePath.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (RandomAccess) new RandomAccessFile(new File(image),"r"));
			}*/
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(imagePath));
				xImage = new PDPixelMap(doc, awtImage);
			}
			headerContentStream.addRect((PDPage.PAGE_SIZE_A4.getUpperRightX() - 95),
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - 285, 80, 80);
			headerContentStream.drawXObject(xImage, (PDPage.PAGE_SIZE_A4.getUpperRightX() - 95),
							(PDPage.PAGE_SIZE_A4.getUpperRightY()) - 285, 80, 80);
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}

		/************************************** To draw college name ***************************************/
		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, HEADER_FONT_SIZE - 2);
		headerCenterXPosition = makeCenterText(splitedHeaderTextCollegeName, font, (int) pageForWidth.getMediaBox().getWidth());
		headerContentStream.moveTextPositionByAmount(headerCenterXPosition.get(splitedHeaderTextCollegeName.get(0)) - 160,
						PDPage.PAGE_SIZE_A4.getUpperRightY() - 90);
		for (int i = 0; i < splitedHeaderTextCollegeName.size(); i++) {
			headerContentStream.drawString((splitedHeaderTextCollegeName.get(i) == null) ? " " : (splitedHeaderTextCollegeName.get(i)));
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(70, -19);
		}
		headerContentStream.appendRawCommands("T*\n");
		//headerContentStream.endText();

		//headerContentStream.beginText();
		String address = (branch.getAddressLine() == null) ? " " : branch.getAddressLine().toUpperCase() + " " + "," + " "
						+ (branch.getCity() != null ? branch.getCity().toUpperCase() : "") + " " + "-" + " " + branch.getPincode();
		//Map<String, Float> addressXPosition = makeCenterText(address, font,(int) pageForWidth.getMediaBox().getWidth());
		//headerContentStream.moveTextPositionByAmount(addressXPosition.get(address), PDPage.PAGE_SIZE_A4.getUpperRightY() - 120);
		headerContentStream.setFont(font, HEADER_FONT_SIZE - 6);
		headerContentStream.drawString(address);
		headerContentStream.endText();
		/************************************* To Draw line ************************************************/

		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD_OBLIQUE, HEADER_FONT_SIZE);
		headerContentStream.drawLine(X0, 700, X0 + 545, 700);
		previousContentY = 700;
		Y0 = previousContentY;
		/************************************** To draw official use only ***********************************/

		/*MAY USE THIS : PDType1Font.getStandardFont(FONT_NAME)*/
		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA, CONTENT_FONT_SIZE);
		String tempText = "For Office Use Only";
		textX = makeCenterText(tempText, font, 545);
		headerContentStream.moveTextPositionByAmount(textX.get(tempText), Y0 - 15);
		headerContentStream.drawString(tempText); //set the Sample here
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + marginX, Y0 - 35);
		headerContentStream.drawString("REGISTRATION ID. :"); //set the Sample here
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + marginX + 80, Y0 - 35);
		headerContentStream.drawString((onlineAdmission.getApplicationId() == null) ? " " : " " + onlineAdmission.getApplicationId());
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount((545 / 2), Y0 - 35);
		headerContentStream.drawString((onlineAdmission.getCandidateDetails().getFormDetails().getMkclRegistrationNo() == null) ? "APPLICATION FORM NO. :"
						: "MKCL No. :");
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount((545 / 2)
						+ ((onlineAdmission.getCandidateDetails().getFormDetails().getMkclRegistrationNo() == null) ? 100 : 50), Y0 - 35);
		headerContentStream.drawString((onlineAdmission.getCandidateDetails().getFormDetails().getMkclRegistrationNo() == null) ? " "
						+ onlineAdmission.getCandidateDetails().getFormDetails().getApplicationFormNo() : " "
						+ onlineAdmission.getCandidateDetails().getFormDetails().getMkclRegistrationNo());
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + marginX, Y0 - 50);
		headerContentStream
						.drawString("RECEIPT NO. :................         ID NO. .....................           DATE : ......................             CONSESSION :...................");
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.getStandardFont(FONT_NAME), CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + marginX, Y0 - 75);
		headerContentStream.drawString("To,");
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.getStandardFont(FONT_NAME), CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + marginX, Y0 - 90);
		headerContentStream.drawString("The Principal");
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.getStandardFont(FONT_NAME), CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + marginX, Y0 - 105);
		for (int i = 0; i < splitedHeaderTextCollegeName.size(); i++) {
			//headerContentStream.moveTextPositionByAmount(getCenterTextX(splitedHeaderText.get(i), font, page.getMediaBox().getWidth()), PDPage.PAGE_SIZE_A4.getUpperRightY() - 100);
			headerContentStream.drawString((splitedHeaderTextCollegeName.get(i) == null) ? " " : splitedHeaderTextCollegeName.get(i));
			headerContentStream.appendRawCommands("T*\n");
			headerContentStream.moveTextPositionByAmount(0, -15);
		}
		headerContentStream.appendRawCommands("T*\n");
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + marginX, Y0 - 150);
		headerContentStream.drawString("Form No                                                   :");
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.setNonStrokingColor(Color.red);
		headerContentStream.moveTextPositionByAmount(X0 + 160, Y0 - 150);
		headerContentStream.drawString((onlineAdmission.getCandidateDetails().getFormDetails().getPhysicalFormNo() == null) ? " " : " "
						+ onlineAdmission.getCandidateDetails().getFormDetails().getPhysicalFormNo());
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setNonStrokingColor(Color.black);
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + marginX, Y0 - 165);
		headerContentStream.drawString("Applied to Course                                   :");
		headerContentStream.endText();

		headerContentStream.beginText();
		headerContentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		headerContentStream.moveTextPositionByAmount(X0 + 160, Y0 - 165);
		headerContentStream.drawString((onlineAdmission.getCourseDetails().getCourse().getClass().getName() == null) ? " " : " "
						+ onlineAdmission.getCourseDetails().getCourse().getName());
		headerContentStream.endText();

		headerContentStream.drawLine(X0, Y0 -= 5, X0 + 545, Y0);
		headerContentStream.drawLine(X0, Y0, X0, Y0 - 55);
		headerContentStream.drawLine(X0 + 545, Y0, X0 + 545, Y0 - 55);
		headerContentStream.drawLine(X0, Y0 -= 55, X0 + 545, Y0);
		Y0 -= 105;
		previousContentY = Y0;
		headerContentStream.close();
	}

	int flag;
	public List<String> list = new ArrayList<String>();

	public List<String> stringSplit(String text, int limit, int flag) // method to split text or wrap text
	{

		if (flag == 0) {
			list.clear();
			flag = 1;
		}
		int thislimit = limit;
		int spacePosition = 0, z = 0;
		if (text != null && text.length() > thislimit) {
			if (text.substring(0, thislimit).contains(" ")) {
				for (int i = z; i < thislimit; i++) {
					if (text.charAt(i) == ' ') {
						spacePosition = i;
					}
				}
				if (text.charAt(spacePosition) == ' ') {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition + 1, text.length()), thislimit, 1);
				} else {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition, text.length()), thislimit, 1);
				}
			} else {
				list.add(text.substring(0, thislimit));
				stringSplit(text.substring(thislimit, text.length()), thislimit, 1);
			}
		} else {
			list.add(text);
		}
		return list;
		//}

	}

	private Map<String, Float> makeCenterText(List<String> splitedTextList, PDFont font, int totalWidth) throws IOException {
		return makeCenterText(splitedTextList, null, (float) totalWidth);
	}

	private Map<String, Float> makeCenterText(String text, PDFont font, int totalWidth) throws IOException {
		return makeCenterText(null, text, (float) totalWidth);
	}

	private Map<String, Float> makeCenterText(List<String> splitedTextList, String text, Float totalWidth) throws IOException {
		Map<String, Float> splitedTextListWithXPosition = new HashMap<String, Float>();
		float xPosition = 0;
		if (splitedTextList != null) {
			for (String nextText : splitedTextList) {
				PDFont font = PDType1Font.HELVETICA;
				int fontSize = 16; // Or whatever font size you want.
				float titleWidth = font.getStringWidth(nextText) / 1000 * fontSize;
				//float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
				float midOfText;
				float midOfWidth;
				midOfText = (titleWidth / 2);
				midOfWidth = (totalWidth / 2);
				xPosition = (midOfWidth - midOfText);
				splitedTextListWithXPosition.put(nextText, midOfText);
			}
		}
		else {
			PDFont font = PDType1Font.HELVETICA;
			int fontSize = 16; // Or whatever font size you want.
			float titleWidth = (font.getStringWidth(text) / 1000) * fontSize;
			//float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
			float midOfText;
			float midOfWidth;
			midOfText = (titleWidth / 2);
			midOfWidth = (totalWidth / 2);
			xPosition = (totalWidth - titleWidth);
			if (text.equals("For Office Use Only")) {
				splitedTextListWithXPosition.put(text, midOfWidth);
			}
			else {
				splitedTextListWithXPosition.put(text, xPosition);
			}
		}
		return splitedTextListWithXPosition;
	}

	/*private Map<String, Float> addressXPosition(List<String> splitedTextHeaderList,  List<String> splitedTextaddressList) throws IOException {
		return addressXPosition(splitedTextHeaderList, splitedTextaddressList, null , null);
	}

	private Map<String, Float> addressXPosition(String textHeader, PDFont font,  String textaddress) throws IOException {
		return addressXPosition(null,  null, textHeader , textaddress);
	}

	private Map<String, Float> addressXPosition(List<String> splitedTextHeaderList,  List<String> splitedTextaddressList, String textHeader , String textaddress) throws IOException {
		Map<String, Float> splitedTextListHeaderWithWidth = new HashMap<String, Float>();
		Map<String, Float> splitedTextListAddressWithWidth = new HashMap<String, Float>();
		float xPosition;
		if (splitedTextHeaderList != null) {
			for (String nextText : splitedTextHeaderList) {
				PDFont font = PDType1Font.HELVETICA;
				int fontSize = 16; // Or whatever font size you want.
				float titleWidth = font.getStringWidth(nextText) / 1000 * fontSize;
				//float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
				splitedTextListHeaderWithWidth.put(nextText, titleWidth);
			}
		}
		if (splitedTextaddressList != null) {
			for (String nextText : splitedTextaddressList) {
				PDFont font = PDType1Font.HELVETICA;
				int fontSize = 16; // Or whatever font size you want.
				float titleWidth = font.getStringWidth(nextText) / 1000 * fontSize;
				//float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
				splitedTextListAddressWithWidth.put(nextText, titleWidth);
			}
		}
		else {
			PDFont font = PDType1Font.HELVETICA;
			int fontSize = 16; // Or whatever font size you want.
			float titleWidth = (font.getStringWidth(text) / 1000) * fontSize;
			//float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
			float midOfText;
			float midOfWidth;
			midOfText =(titleWidth / 2);
			midOfWidth = (totalWidth / 2);
			xPosition = (midOfWidth - midOfText);
			splitedTextListHeaderWithWidth.put(text, midOfWidth);
		}
		return splitedTextListHeaderWithWidth;
	}
	*/

	private float getTextWidth(String text, PDFont font) throws IOException {
		int fontSize = 16; // Or whatever font size you want.
		float textWidth = font.getStringWidth(text) / 1000 * fontSize;
		//float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
		return textWidth;
	}

	@Override
	public void createPdf(OnlineAdmission onlineAdmission, String pdfFilePath, String studentPhotoPath, String signaturePhotoPath, String branchLogo)
					throws IOException {
		PDDocument doc = new PDDocument();
		try {

			ApplicationContent createContent = new ApplicationContent();
			doc = createContent.addApllicationDetails(doc, onlineAdmission, branchLogo);
			//ApplicationReport applicationReport = new ApplicationReport();
			createHeader(onlineAdmission, studentPhotoPath, branchLogo, doc);

			doc.save(pdfFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				doc.close();
			}
		}
	}
}
