package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeleteChatGroup {

	@JsonProperty(value = "user_id")
	@Digits(integer = 11, fraction = 0)
	private Integer userId;

	@JsonProperty(value = "chat_dialog_id")
	@Size(max = 45)
	private String chatDialogId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getChatDialogId() {
		return chatDialogId;
	}

	public void setChatDialogId(String chatDialogId) {
		this.chatDialogId = chatDialogId;
	}

	@Override
	public String toString() {
		return "DeleteChatGroup [userId=" + userId + ", chatDialogId=" + chatDialogId + "]";
	}

}
