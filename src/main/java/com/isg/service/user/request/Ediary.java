package com.isg.service.user.request;

import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.model.FileAttachment;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Ediary {

	@JsonProperty("id")
	@Digits(integer = 11, fraction = 0)
	private Integer id;

	private String identifier;

	@NotNull
	private String description;

	@NotNull
	@Size(max = 20)
	private String date;

	@JsonProperty("image_url")
	@Size(max = 255)
	private String imageUrl;

	List<Integer> groupIds;

	List<Integer> userIds;

	FileAttachment fileAttachment;

	private String sentBy;

	public Ediary() {

	}

	public Ediary(Integer id, String description, String date, String imageUrl, String sentBy) {
		super();
		this.id = id;
		this.description = description;
		this.date = date;
		this.imageUrl = imageUrl;
		this.sentBy = sentBy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("_id")
	public String getIdentifier() {
		return identifier;
	}

	@JsonIgnore
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@JsonIgnore
	public List<Integer> getGroupIds() {
		return groupIds;
	}

	@JsonProperty("group_id")
	public void setGroupIds(List<Integer> groupIds) {
		this.groupIds = groupIds;
	}

	@JsonIgnore
	public List<Integer> getUserIds() {
		return userIds;
	}

	@JsonProperty("individual_user_id")
	public void setUserIds(List<Integer> userIds) {
		this.userIds = userIds;
	}

	@JsonIgnore
	public FileAttachment getFileAttachment() {
		return fileAttachment;
	}

	@JsonProperty("attachment")
	public void setFileAttachment(FileAttachment fileAttachment) {
		this.fileAttachment = fileAttachment;
	}

	@JsonProperty("sent_by")
	public String getSentBy() {
		return sentBy;
	}

	@JsonIgnoreProperties
	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}

	@Override
	public String toString() {
		return "Ediary [id=" + id + ", identifier=" + identifier + ", description=" + description + ", date=" + date + ", imageUrl=" + imageUrl + "]";
	}

}
