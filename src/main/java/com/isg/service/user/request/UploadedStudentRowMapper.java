package com.isg.service.user.request;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UploadedStudentRowMapper implements RowMapper<Student> {
	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {

		StudentPersonDetails personalDetails = new StudentPersonDetails();
		StudentAddress address = new StudentAddress();
		StudentEducationDetails educationalDetails = new StudentEducationDetails();
		StudentMiscellaneousDetails miscellaneousDetails = new StudentMiscellaneousDetails();
		Student student = new Student();
		personalDetails.setName(rs.getString("first_name") + ((rs.getString("last_name") == null) ? "" : " " + rs.getString("last_name")));

		String dob = rs.getDate("dob").toString();
		/*
		 * if(!StringUtils.isEmpty(dob)) {
		 * personalDetails.setDob(DateFormatter.formatDate(dob,
		 * DateFormatter.DATE_FORMAT_YYYYMMDD_HYPHEN_SEPARATED,
		 * DateFormatter.DATE_FORMAT_DDMMYYYY_HYPHEN_SEPARATED)); }
		 */

		personalDetails.setDob(dob);
		personalDetails.setRelationWithParent(rs.getString("relation_with_parent"));
		personalDetails.setEmailAddress(rs.getString("email_address"));
		personalDetails.setMobile(rs.getString("mobile"));
		personalDetails.setRegistrationCode(rs.getString("registration_code"));
		personalDetails.setGender(rs.getString("gender"));
		address.setLine1(rs.getString("line1"));
		address.setArea(rs.getString("area"));
		address.setCity(rs.getString("city"));
		address.setPincode(rs.getString("pincode"));
		personalDetails.setAddress(address);
		personalDetails.setReligion(rs.getString("religion"));
		personalDetails.setCasteId(rs.getInt("caste_id") == 0 ? null : rs.getInt("caste_id"));
		personalDetails.setPhoto(rs.getString("photo"));
		educationalDetails.setBranchId(rs.getInt("branch_id"));
		educationalDetails.setStandard(rs.getInt("standard"));
		educationalDetails.setDivision(rs.getInt("division"));
		educationalDetails.setRollNumber(rs.getString("roll_number"));
		miscellaneousDetails.setGoalChild(rs.getString("goal_child"));
		miscellaneousDetails.setGoalParent(rs.getString("goal_parent"));
		student.setEducationDetails(educationalDetails);
		student.setPersonalDetails(personalDetails);
		student.setOtherDetails(miscellaneousDetails);
		student.setStudentUploadId(rs.getLong("id"));
		student.setUserId(rs.getLong("user_id"));
		return student;
	}

}
