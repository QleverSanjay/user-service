package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentAddress {
	@JsonProperty(value = "line_1")
	@Size(max = 100)
	private String line1;
	@JsonProperty(value = "area")
	@Size(max = 45)
	private String area;
	@JsonProperty(value = "city")
	@Size(max = 45)
	private String city;
	@JsonProperty(value = "pincode")
	@Size(max = 45)
	private String pincode;

	@JsonProperty(value = "districtId")
	@Digits(integer = 11, fraction = 0)
	Integer districtId;

	@JsonProperty(value = "talukaId")
	@Digits(integer = 11, fraction = 0)
	Integer talukaId;

	@JsonProperty(value = "stateId")
	@Digits(integer = 11, fraction = 0)
	Integer stateId;
	
	private String stateName;
	private String districtName;

	public String getLine1() {
		return (line1 != null) ? line1 : "";
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getArea() {
		return (area != null) ? area : "";
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return (city != null) ? city : "";
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return (pincode != null) ? pincode : "";
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTalukaId() {
		return talukaId;
	}

	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	
}
