package com.isg.service.user.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.model.FileAttachment;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SendNotfication extends Notification {

	@JsonProperty("group_id")
	List<Integer> groupIds;

	@JsonProperty("individual_user_id")
	List<Integer> userIds;

	FileAttachment fileAttachment;

	@JsonIgnore
	public List<Integer> getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(List<Integer> groupIds) {
		this.groupIds = groupIds;
	}

	@JsonIgnore
	public List<Integer> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Integer> userIds) {
		this.userIds = userIds;
	}

	@JsonIgnore
	public FileAttachment getFileAttachment() {
		return fileAttachment;
	}

	@JsonProperty("attachment")
	public void setFileAttachment(FileAttachment fileAttachment) {
		this.fileAttachment = fileAttachment;
	}

	@Override
	public String toString() {
		return "SendNotfication [groupIds=" + groupIds + ", userIds=" + userIds + ", getId()=" + getId() + ", getTitle()=" + getTitle() + ", getDescription()="
						+ getDescription() + ", getPriority()=" + getPriority() + ", getBranch_id()=" + getBranch_id() + ", getCategory()=" + getCategory()
						+ ", getDate()=" + getDate() + ", getImageUrl()=" + getImageUrl() + ", getDetailDescription()="
						+ getDetailDescription() + ", getIdentifier()=" + getIdentifier() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
						+ ", hashCode()=" + hashCode() + "]";
	}

}
