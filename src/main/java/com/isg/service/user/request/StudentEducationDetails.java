package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentEducationDetails {

	@JsonProperty(value = "branch_id")
	@Digits(integer = 11, fraction = 0)
	private Integer branchId;

	@JsonProperty(value = "branch_name")
	private String branchName;

	@JsonProperty(value = "standard")
	@Digits(integer = 11, fraction = 0)
	private Integer standard;

	@JsonProperty(value = "standard_name")
	private String standardName;

	@JsonProperty(value = "division")
	@Digits(integer = 11, fraction = 0)
	private Integer division;

	@JsonProperty(value = "division_name")
	private String divisionName;

	@JsonProperty(value = "roll_number")
	@Size(max = 45)
	private String rollNumber;

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getStandard() {
		return standard;
	}

	public void setStandard(Integer standard) {
		this.standard = standard;
	}

	public Integer getDivision() {
		return (division != null) ? division : null;
	}

	public void setDivision(Integer division) {
		this.division = division;
	}

	public String getRollNumber() {
		return (rollNumber != null) ? rollNumber : "";
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	@Override
	public String toString() {
		return "StudentEducationDetails [branchId=" + branchId + ", standard=" + standard + ", division=" + division
						+ ", rollNumber=" + rollNumber + "]";
	}

}
