package com.isg.service.user.request;

public class ServiceRequestType {
	
	private Integer id;
	private String requestType;
	private String email;
	private String emailResponse;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailResponse() {
		return emailResponse;
	}
	public void setEmailResponse(String emailResponse) {
		this.emailResponse = emailResponse;
	}

}
