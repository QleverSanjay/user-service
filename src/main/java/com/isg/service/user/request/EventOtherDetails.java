package com.isg.service.user.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventOtherDetails {

	@JsonProperty(value = "for_student")
	boolean forStudent;

	@JsonProperty(value = "student_id")
	Integer studentId;

	@JsonProperty(value = "student_gender")
	String studentGender;

	public boolean isForStudent() {
		return forStudent;
	}

	public void setForStudent(boolean forStudent) {
		this.forStudent = forStudent;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	@Override
	public String toString() {
		return "EventOtherDetails [forStudent=" + forStudent + ", studentId=" + studentId + ", studentGender=" + studentGender + "]";
	}

}
