package com.isg.service.user.request;

public class Branch {
	private Integer id;
	private String name;
	private String addressLine;
	private String city;
	private String area;
	private String pincode;
	private String contactnumber;
	private String contactemail;
	private String websiteurl;
	private Integer districtId;
	private Integer talukaId;
	private Integer instituteId;
	private Integer stateId;
	private String active;
	private String studentDataAvailable;
	private String uniqueIdentifierLable;
	private String uniqueIdentifierName;

	public String getStudentDataAvailable() {
		return studentDataAvailable;
	}

	public void setStudentDataAvailable(String studentDataAvailable) {
		this.studentDataAvailable = studentDataAvailable;
	}

	public String getUniqueIdentifierLable() {
		return uniqueIdentifierLable;
	}

	public void setUniqueIdentifierLable(String uniqueIdentifierLable) {
		this.uniqueIdentifierLable = uniqueIdentifierLable;
	}

	public String getUniqueIdentifierName() {
		return uniqueIdentifierName;
	}

	public void setUniqueIdentifierName(String uniqueIdentifierName) {
		this.uniqueIdentifierName = uniqueIdentifierName;
	}

	private BranchConfiguration branchConfiguration;

	public BranchConfiguration getBranchConfiguration() {
		return branchConfiguration;
	}

	public void setBranchConfiguration(BranchConfiguration branchConfiguration) {
		this.branchConfiguration = branchConfiguration;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getContactnumber() {
		return contactnumber;
	}

	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}

	public String getContactemail() {
		return contactemail;
	}

	public void setContactemail(String contactemail) {
		this.contactemail = contactemail;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public void setWebsiteurl(String websiteurl) {
		this.websiteurl = websiteurl;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTalukaId() {
		return talukaId;
	}

	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Branch [id=" + id + ", name=" + name + ", addressLine="
				+ addressLine + ", city=" + city + ", area=" + area
				+ ", pincode=" + pincode + ", contactnumber=" + contactnumber
				+ ", contactemail=" + contactemail + ", websiteurl="
				+ websiteurl + ", districtId=" + districtId + ", talukaId="
				+ talukaId + ", instituteId=" + instituteId + ", stateId="
				+ stateId + ", active=" + active + ", studentDataAvailable="
				+ studentDataAvailable + ", uniqueIdentifierLable="
				+ uniqueIdentifierLable + ", uniqueIdentifierName="
				+ uniqueIdentifierName + ", branchConfiguration="
				+ branchConfiguration + "]";
	}
	
	

}
