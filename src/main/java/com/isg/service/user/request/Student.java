package com.isg.service.user.request;

import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class Student {
	@JsonProperty(value = "parent_id")
	@Digits(integer = 11, fraction = 0)
	private Integer parentId;
	@JsonProperty(value = "otp")
	@Size(max = 10)
	private String otp;
	@JsonProperty(value = "personal_details")
	private StudentPersonDetails personalDetails = new StudentPersonDetails();
	@JsonProperty(value = "education_details")
	private StudentEducationDetails educationDetails = new StudentEducationDetails();
	@JsonProperty(value = "other_details")
	private StudentMiscellaneousDetails otherDetails = new StudentMiscellaneousDetails();
	
	@JsonProperty(value = "student_upload_id")
	private Long studentUploadId;
	@JsonProperty(value = "user_id")
	@Digits(integer = 11, fraction = 0)
	private Long userId;
	@JsonProperty(value = "username")
	@Size(max = 100)
	private String username;
	private List<PaymentTransaction> feesInfo;
	
	private String showBranchLogo;
	
	private String branchLogoUrl;


	@Override
	public String toString() {
		return "Student [parentId=" + parentId + ", otp=" + otp + ", personalDetails=" + personalDetails + ", educationDetails=" + educationDetails
						+ ", otherDetails=" + otherDetails + ",username=" + username + ", studentUploadId=" + studentUploadId + ", userId=" + userId + "]";
	}
}
