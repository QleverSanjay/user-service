package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Notification {
	@JsonProperty("id")
	@Digits(integer = 11, fraction = 0)
	private Integer id;

	@JsonProperty("_id")
	private String identifier;

	@NotNull
	@Size(max = 100)
	private String title;

	@Size(max = 255)
	private String description;

	@Size(max = 45)
	private String priority;

	@JsonProperty("branch_id")
	@Digits(integer = 11, fraction = 0)
	private Integer branch_id;

	@Size(max = 45)
	private String category;

	@Size(max = 20)
	private String date;

	@JsonProperty("image_url")
	@Size(max = 255)
	private String imageUrl;

	@JsonProperty("detail_description")
	private String detailDescription;

	private String sentBy;

	private String forUser;

	private Integer forUserId;

	private char readStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return (description == null) ? "" : description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Integer getBranch_id() {
		return (branch_id == null) ? null : branch_id;
	}

	public void setBranch_id(Integer branch_id) {
		this.branch_id = branch_id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getImageUrl() {
		return (imageUrl == null) ? "" : imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDetailDescription() {
		return (detailDescription == null) ? "" : detailDescription;
	}

	public void setDetailDescription(String detailDescription) {
		this.detailDescription = detailDescription;
	}

	public String getIdentifier() {
		return (this.id != null) ? this.id.toString() : "";
	}

	@JsonProperty("sent_by")
	public String getSentBy() {
		return sentBy;
	}

	@JsonIgnoreProperties
	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}

	@JsonProperty("for_person")
	public String getForUser() {
		return forUser;
	}

	@JsonIgnoreProperties
	public void setForUser(String forUser) {
		this.forUser = forUser;
	}

	@JsonProperty("for_user_id")
	public Integer getForUserId() {
		return forUserId;
	}

	@JsonIgnoreProperties
	public void setForUserId(Integer forUserId) {
		this.forUserId = forUserId;
	}

	@JsonProperty("read_status")
	public char getReadStatus() {
		return readStatus;
	}

	@JsonIgnoreProperties
	public void setReadStatus(char readStatus) {
		this.readStatus = readStatus;
	}

	@Override
	public String toString() {
		return "Notification [id=" + id + ", title=" + title + ", description=" + description + ", priority=" + priority + ", branch_id=" + branch_id
						+ ", category=" + category + ", date=" + date + ", imageUrl=" + imageUrl + ", detailDescription="
						+ detailDescription + "]";
	}

}
