package com.isg.service.user.request;

public class BranchConfiguration {
	private Integer branchId;
	private Integer InstituteId;
	private String useSchoolLogAsWatermarkImage;
	private String hideLogo;
	private String trailerRecord;
	private String address;
	private String branchName;
	private String instituteLogoUrl;

	public String getInstituteLogoUrl() {
		return instituteLogoUrl;
	}

	public void setInstituteLogoUrl(String instituteLogoUrl) {
		this.instituteLogoUrl = instituteLogoUrl;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUseSchoolLogAsWatermarkImage() {
		return useSchoolLogAsWatermarkImage;
	}

	public void setUseSchoolLogAsWatermarkImage(String useSchoolLogAsWatermarkImage) {
		this.useSchoolLogAsWatermarkImage = useSchoolLogAsWatermarkImage;
	}

	public String getHideLogo() {
		return hideLogo;
	}

	public void setHideLogo(String hideLogo) {
		this.hideLogo = hideLogo;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getInstituteId() {
		return InstituteId;
	}

	public void setInstituteId(Integer instituteId) {
		InstituteId = instituteId;
	}

	public String getTrailerRecord() {
		return trailerRecord;
	}

	public void setTrailerRecord(String trailerRecord) {
		this.trailerRecord = trailerRecord;
	}

}
