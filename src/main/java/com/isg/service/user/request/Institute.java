package com.isg.service.user.request;

public class Institute {
	private Integer instituteId;
	private String instituteCode;
	private String instituteName;
	private Integer parentInstituteId;
	private boolean active;
	private String logo_url;
	private String name;
	private Long id;
//	private Integer id;

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public String getInstituteCode() {
		return instituteCode;
	}

	public void setInstituteCode(String instituteCode) {
		this.instituteCode = instituteCode;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public Integer getParentInstituteId() {
		return parentInstituteId;
	}

	public void setParentInstituteId(Integer parentInstituteId) {
		this.parentInstituteId = parentInstituteId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLogo_url() {
		return logo_url;
	}

	public void setLogo_url(String logo_url) {
		this.logo_url = logo_url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	

}
