package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExportOnlineAdmissionReport {

	@JsonProperty("branch_id")
	@NotNull
	@Digits(integer = 11, fraction = 0)
	Integer branchId;

	@JsonProperty("course_id")
	@NotNull
	@Digits(integer = 11, fraction = 0)
	Integer courseId;

	@JsonProperty("academic_year_id")
	@NotNull
	@Digits(integer = 11, fraction = 0)
	Integer academicYearId;

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

}
