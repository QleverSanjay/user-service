package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Parent {

	@JsonProperty(value = "id")
	@Digits(integer = 11, fraction = 0)
	private Integer id;

	@JsonProperty(value = "firstname")
	@NotNull
	@Size(max = 45)
	String firstname;

	@JsonProperty(value = "middlename")
	@Size(max = 45)
	String middlename;

	@JsonProperty(value = "lastname")
	// @NotNull
	@Size(max = 45)
	String lastname;

	@JsonProperty(value = "gender")
	@Size(max = 1)
	String gender;

	@JsonProperty(value = "dob")
	@Size(max = 10)
	String dob;

	@JsonProperty(value = "email")
	@Size(max = 45)
	String email;

	@Size(max = 40)
	String password;

	@JsonProperty(value = "primaryContact")
	@Size(max = 10)
	String primaryContact;

	@JsonProperty(value = "secondaryContact")
	@Size(max = 10)
	String secondaryContact;

	@JsonProperty(value = "addressLine")
	@Size(max = 100)
	String addressLine;

	@JsonProperty(value = "area")
	@Size(max = 45)
	String area;

	@JsonProperty(value = "city")
	@Size(max = 45)
	String city;

	@JsonProperty(value = "pincode")
	@Size(max = 45)
	String pincode;

	@JsonProperty(value = "districtId")
	@Digits(integer = 11, fraction = 0)
	Integer districtId;

	@JsonProperty(value = "talukaId")
	@Digits(integer = 11, fraction = 0)
	Integer talukaId;

	@JsonProperty(value = "stateId")
	@Digits(integer = 11, fraction = 0)
	Integer stateId;

	@Digits(integer = 11, fraction = 0)
	Integer religionId;

	@Digits(integer = 11, fraction = 0)
	Integer casteId;

	@Size(max = 6)
	String otp;

	@Size(max = 255)
	String photo;

	@JsonIgnore
	@Digits(integer = 11, fraction = 0)
	Integer user_id;

	@JsonProperty(value = "username")
	@Size(max = 100)
	private String username;

	private String branchLogoUrl;
	
	private String showBranchLogo;

	@Override
	public String toString() {
		return "Parent [id=" + id + ", firstname=" + firstname + ", middlename=" + middlename + ", lastname=" + lastname + ", gender=" + gender + ", dob="
						+ dob + ", email=" + email + ", primaryContact=" + primaryContact + ", secondaryContact=" + secondaryContact + ", addressLine="
						+ addressLine + ", area=" + area + ", city=" + city + ", pincode=" + pincode + ", districtId=" + districtId + ", talukaId=" + talukaId
						+ ", stateId=" + stateId + ",username=" + username + ", religionId=" + religionId + ", casteId=" + casteId + "]";
	}

}
