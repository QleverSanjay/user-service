package com.isg.service.user.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VendorMerchantDetail {

	@JsonProperty(value = "email")
	@NotNull
	@Size(max = 56)
	private String email;

	@JsonProperty(value = "merchant_legal_name")
	@NotNull
	@Size(max = 100)
	private String merchantLegalName;

	@JsonProperty(value = "marketing_name")
	@NotNull
	@Size(max = 100)
	private String marketingName;

	@JsonProperty(value = "address")
	@NotNull
	@Size(max = 100)
	private String addressLine;

	@JsonProperty(value = "area")
	@NotNull
	@Size(max = 56)
	private String area;

	@JsonProperty(value = "city")
	@NotNull
	@Size(max = 56)
	private String city;

	@JsonProperty(value = "pincode")
	@NotNull
	@Size(max = 6)
	private String pincode;

	@JsonProperty(value = "phone")
	@NotNull
	@Size(max = 12)
	private String phone;

	@JsonProperty(value = "selling_category")
	@NotNull
	@Size(max = 45)
	private String sellingCategory;

	@JsonProperty(value = "mobile")
	@NotNull
	@Size(max = 15)
	private String mobile;

	@NotNull
	@Size(max = 11)
	private String shopping_profile_id;

	@JsonProperty(value = "active")
	@NotNull
	private char status;

	@JsonProperty(value = "is_registration_approved")
	@NotNull
	private char isRegistrationApproved;

	public String getMerchantLegalName() {
		return merchantLegalName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMerchantLegalName(String merchantLegalName) {
		this.merchantLegalName = merchantLegalName;
	}

	public String getMarketingName() {
		return marketingName;
	}

	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSellingCategory() {
		return sellingCategory;
	}

	public void setSellingCategory(String sellingCategory) {
		this.sellingCategory = sellingCategory;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@JsonIgnore
	public String getShopping_profile_id() {
		return shopping_profile_id;
	}

	@JsonProperty(value = "shopping_profile_id")
	public void setShopping_profile_id(String shopping_profile_id) {
		this.shopping_profile_id = shopping_profile_id;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public char getIsRegistrationApproved() {
		return isRegistrationApproved;
	}

	public void setIsRegistrationApproved(char isRegistrationApproved) {
		this.isRegistrationApproved = isRegistrationApproved;
	}

	@Override
	public String toString() {
		return "VendorMerchantDetail [email=" + email + ", merchantLegalName=" + merchantLegalName + ", marketingName=" + marketingName + ", addressLine="
						+ addressLine + ", area=" + area + ", city=" + city + ", pincode=" + pincode + ", phone=" + phone + ", sellingCategory="
						+ sellingCategory + ", mobile=" + mobile + ", shopping_profile_id=" + shopping_profile_id + ", status=" + status
						+ ", isRegistrationApproved=" + isRegistrationApproved + "]";
	}

}
