package com.isg.service.user.request;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

	Integer id;

	@JsonProperty(value = "login")
	@NotNull
	@Size(max = 100)
	String username;

	@JsonProperty(value = "password")
	@NotNull
	@Size(max = 40)
	String password;

	@Size(max = 255)
	String gcmId;

	@Size(max = 10)
	String deviceType;

	@JsonIgnore
	byte[] passwordSalt;

	@JsonIgnore
	byte[] encryptedPassword;
	@JsonProperty(value = "loginCount")
	String loginCount;

	@JsonIgnore
	String plainPassword;

	public String getPlainPassword() {
		return plainPassword;
	}

	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}

	public String getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(String loginCount) {
		this.loginCount = loginCount;
	}

	List<String> roles = new ArrayList<String>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public byte[] getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(byte[] passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	public byte[] getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(byte[] encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	@JsonIgnoreProperties
	public String getGcmId() {
		return gcmId;
	}

	@JsonProperty(value = "gcm_id")
	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	@JsonIgnoreProperties
	public String getDeviceType() {
		return deviceType;
	}

	@JsonProperty(value = "device_type")
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", deviceType=" + deviceType + ", loginCount=" + loginCount + "]";
	}

}
