package com.isg.service.user.request;

import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.model.EventRecurrencePattern;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Event {

	@JsonProperty(value = "id")
	@Size(max = 30)
	String id;

	@JsonProperty("_id")
	private String identifier;

	@JsonProperty(value = "title")
	@NotNull
	@Size(max = 45)
	String title;

	@JsonProperty(value = "summary")
	@Size(max = 45)
	String summary;

	@JsonProperty(value = "description")
	@Size(max = 255)
	String description;

	@JsonProperty(value = "location")
	@Size(max = 100)
	String venue;

	@JsonProperty(value = "start_date")
	@NotNull
	@Size(max = 20)
	String startDate;

	@JsonProperty(value = "start_time")
	@NotNull
	@Size(max = 20)
	String starttime;

	@JsonProperty(value = "end_date")
	@Size(max = 20)
	String endDate;

	@JsonProperty(value = "end_time")
	@NotNull
	@Size(max = 20)
	String endtime;

	@JsonProperty(value = "all_day")
	boolean alldayevent;

	@JsonProperty(value = "remind_me")
	boolean remindMe;

	@JsonProperty(value = "remind_before")
	int remindMeBefore;

	@JsonProperty(value = "branchId")
	@Digits(integer = 11, fraction = 0)
	int branchId;

	@JsonProperty(value = "attach_image_url")
	@Size(max = 255)
	String image;

	@Size(max = 1)
	String status;

	String category;

	String eventScheduleUpdated;

	@JsonProperty(value = "rrule")
	List<EventRecurrencePattern> eventRecurrencePattern;

	@JsonProperty(value = "other_details")
	EventOtherDetails otherDetails;

	private String sentBy;

	@JsonIgnoreProperties
	int parentEventId;

	private Integer forUserId;

	private char readStatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public boolean isAlldayevent() {
		return alldayevent;
	}

	public void setAlldayevent(boolean alldayevent) {
		this.alldayevent = alldayevent;
	}

	public boolean isRemindMe() {
		return remindMe;
	}

	public void setRemindMe(boolean remindMe) {
		this.remindMe = remindMe;
	}

	public int getRemindMeBefore() {
		return remindMeBefore;
	}

	public void setRemindMeBefore(int remindMeBefore) {
		this.remindMeBefore = remindMeBefore;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<EventRecurrencePattern> getEventRecurrencePattern() {
		return eventRecurrencePattern;
	}

	public void setEventRecurrencePattern(List<EventRecurrencePattern> eventRecurrencePattern) {
		this.eventRecurrencePattern = eventRecurrencePattern;
	}

	public EventOtherDetails getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(EventOtherDetails otherDetails) {
		this.otherDetails = otherDetails;
	}

	public String getIdentifier() {
		return "" + this.id;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonIgnore
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("is_schdule_updated")
	public String getScheduleUpdated() {
		return eventScheduleUpdated;
	}

	@JsonIgnore
	public void setScheduleUpdated(String eventScheduleUpdated) {
		this.eventScheduleUpdated = eventScheduleUpdated;
	}

	@JsonProperty("sent_by")
	public String getSentBy() {
		return sentBy;
	}

	@JsonIgnoreProperties
	public void setSentBy(String sentBy) {
		this.sentBy = sentBy;
	}

	@JsonProperty("category")
	public String getCategory() {
		return category;
	}

	@JsonIgnoreProperties
	public void setCategory(String category) {
		this.category = category;
	}

	public int getParentEventId() {
		return parentEventId;
	}

	public void setParentEventId(int parentEventId) {
		this.parentEventId = parentEventId;
	}

	@JsonProperty("for_user_id")
	public Integer getForUserId() {
		return forUserId;
	}

	@JsonIgnoreProperties
	public void setForUserId(Integer forUserId) {
		this.forUserId = forUserId;
	}

	@JsonProperty("read_status")
	public char getReadStatus() {
		return readStatus;
	}

	@JsonIgnoreProperties
	public void setReadStatus(char readStatus) {
		this.readStatus = readStatus;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", identifier=" + identifier + ", title=" + title + ", summary=" + summary + ", description=" + description + ", venue="
						+ venue + ", startDate=" + startDate + ", starttime=" + starttime + ", endDate=" + endDate + ", endtime=" + endtime + ", alldayevent="
						+ alldayevent + ", remindMe=" + remindMe + ", remindMeBefore=" + remindMeBefore + ", branchId=" + branchId + ", image=" + image
						+ ", status=" + status + ", category=" + category + ", eventScheduleUpdated=" + eventScheduleUpdated + ", eventRecurrencePattern="
						+ eventRecurrencePattern + ", otherDetails=" + otherDetails + ", sentBy=" + sentBy + ", parentEventId=" + parentEventId
						+ ", forUserId=" + forUserId + ", readStatus=" + readStatus + "]";
	}

}
