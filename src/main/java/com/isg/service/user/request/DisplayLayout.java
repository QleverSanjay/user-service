package com.isg.service.user.request;

public class DisplayLayout {

	private Integer id;

	private String feeDescription;
	private String otherDescription;

	private double feeAmount;

	private Integer branchId;
	private Integer instituteId;

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getFeeDescription() {
		return feeDescription;
	}

	public void setFeeDescription(String feeDescription) {
		this.feeDescription = feeDescription;
	}

	public String getOtherDescription() {
		return otherDescription;
	}

	public void setOtherDescription(String otherDescription) {
		this.otherDescription = otherDescription;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	@Override
	public String toString() {
		return "DisplayLyout [id=" + id + ", feeDescription=" + feeDescription
						+ ", otherDescription=" + otherDescription + ", feeAmount="
						+ feeAmount + ", branchId=" + branchId + "]";
	}

}
