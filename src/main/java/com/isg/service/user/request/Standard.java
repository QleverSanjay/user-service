package com.isg.service.user.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Standard {

	Integer id;

	String displayName;
	
	@JsonProperty(value = "id")
	public Integer getId() {
		return id;
	}

	@JsonIgnore
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty(value = "name")
	public String getDisplayName() {
		return displayName;
	}

	@JsonIgnore
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Override
	public String toString() {
		return "Standard [id=" + id + ", displayName=" + displayName + "]";
	}

}
