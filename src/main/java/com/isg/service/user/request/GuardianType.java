package com.isg.service.user.request;

public class GuardianType {
	private int guardianTypeId;
	private String guardianTypeName;
	private boolean active;
	public int getGuardianTypeId() {
		return guardianTypeId;
	}
	public void setGuardianTypeId(int guardianTypeId) {
		this.guardianTypeId = guardianTypeId;
	}
	public String getGuardianTypeName() {
		return guardianTypeName;
	}
	public void setGuardianTypeName(String guardianTypeName) {
		this.guardianTypeName = guardianTypeName;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
