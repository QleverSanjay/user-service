package com.isg.service.user.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FeesCode {
	
	private String dueDate;
	private Integer amount;
	private Integer feesId;
	private Integer feeCodeConfigId;
	private String FeeType;
	private Integer headId;
	private String description;
	
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public Integer getFeesId() {
		return feesId;
	}
	public void setFeesId(Integer feesId) {
		this.feesId = feesId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getFeeCodeConfigId() {
		return feeCodeConfigId;
	}
	public void setFeeCodeConfigId(Integer feeCodeConfigId) {
		this.feeCodeConfigId = feeCodeConfigId;
	}
	public String getFeeType() {
		return FeeType;
	}
	public void setFeeType(String feeType) {
		FeeType = feeType;
	}
	public Integer getHeadId() {
		return headId;
	}
	public void setHeadId(Integer headId) {
		this.headId = headId;
	}
	@Override
	public String toString() {
		return "FeesCode [dueDate=" + dueDate + ", amount=" + amount
				+ ", feesId=" + feesId + ", feeCodeConfigId=" + feeCodeConfigId
				+ ", FeeType=" + FeeType + ", headId=" + headId
				+ ", description=" + description + "]";
	}
	
	
	

}
