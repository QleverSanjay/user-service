package com.isg.service.user.request;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TeacherRowMapper implements RowMapper<Teacher> {
	@Override
	public Teacher mapRow(ResultSet rs, int rowNum) throws SQLException {

		Teacher teacher = new Teacher();
		teacher.setId(rs.getInt("id"));
		teacher.setFirstName(rs.getString("firstName"));
		teacher.setLastName(rs.getString("lastName"));
		teacher.setDob(rs.getDate("dateOfBirth").toString());
		teacher.setEmail(rs.getString("emailAddress"));
		teacher.setGender(rs.getString("gender"));
		teacher.setPrimaryContactNumber(rs.getString("primaryContact"));
		teacher.setSecondContactNumber(rs.getString("secondaryContact"));
		teacher.setAddressLine(rs.getString("addressLine"));
		teacher.setArea(rs.getString("area"));
		teacher.setCity(rs.getString("city"));
		teacher.setPincode(rs.getString("pinCode"));
		teacher.setPhoto(rs.getString("photo"));
		teacher.setBranchId(rs.getInt("branch_id"));
		teacher.setStateId(rs.getInt("state_id"));
		teacher.setDistrictId(rs.getInt("district_id"));
		teacher.setTalukaId(rs.getInt("taluka_id"));

		return teacher;
	}
}
