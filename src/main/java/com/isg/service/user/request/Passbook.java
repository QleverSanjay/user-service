package com.isg.service.user.request;

import java.util.Map;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Passbook {

	@JsonProperty(value = "id")
	@Digits(integer = 11, fraction = 0)
	int id;

	@JsonProperty("_id")
	@Size(max = 11)
	private String identifier;

	@JsonProperty(value = "title")
	@NotNull
	@Size(max = 255)
	String title;

	@JsonProperty(value = "description")
	@Size(max = 1000)
	String description;

	@JsonProperty(value = "transaction_date")
	@NotNull
	@Size(max = 20)
	String date;

	@JsonProperty(value = "transaction_type")
	@NotNull
	@Size(max = 6)
	String transactionType;

	@JsonProperty(value = "amount")
	@NotNull
	@Digits(integer = 11, fraction = 2)
	float amount;

	@Size(max = 3)
	String selfExpenseFlag;

	@JsonProperty(value = "applicable_for")
	Map<String, Object> otherDetails;

	@JsonProperty(value = "for_entity_name")
	@Size(max = 100)
	String forEntityName;

	@JsonIgnoreProperties
	Integer forEntityUserId;

	@JsonIgnoreProperties
	Integer parentTransactionId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Map<String, Object> getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(Map<String, Object> otherDetails) {
		this.otherDetails = otherDetails;
	}

	@JsonIgnore
	public Integer getReceivingEntityId() {
		return Integer.parseInt(this.getOtherDetails().get("id").toString());
	}

	@JsonIgnore
	public String getSelfExpenseFlag() {
		return selfExpenseFlag;
	}

	@JsonProperty(value = "mark_as_self_expense")
	public void setSelfExpenseFlag(String selfExpenseFlag) {
		this.selfExpenseFlag = selfExpenseFlag;
	}

	public String getForEntityName() {
		return forEntityName;
	}

	public void setForEntityName(String forEntityName) {
		this.forEntityName = forEntityName;
	}

	public Integer getForEntityUserId() {
		return forEntityUserId;
	}

	public void setForEntityUserId(Integer forEntityUserId) {
		this.forEntityUserId = forEntityUserId;
	}

	public Integer getParentTransactionId() {
		return parentTransactionId;
	}

	public void setParentTransactionId(Integer parentTransactionId) {
		this.parentTransactionId = parentTransactionId;
	}

	@Override
	public String toString() {
		return "Passbook [id=" + id + ", identifier=" + identifier + ", title=" + title + ", description=" + description + ", date=" + date
						+ ", transactionType=" + transactionType + ", amount=" + amount + ", selfExpenseFlag=" + selfExpenseFlag + ", otherDetails="
						+ otherDetails + ", forEntityName=" + forEntityName + "]";
	}

}
