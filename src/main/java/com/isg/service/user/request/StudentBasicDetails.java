package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentBasicDetails {
	@JsonProperty(value = "id")
	@Digits(integer = 11, fraction = 0)
	private Integer id;
	@JsonProperty(value = "name")
	@Size(max = 45)
	private String name;
	@JsonProperty(value = "institute_name")
	private String institute_name;
	@JsonProperty(value = "standard_name")
	private String standard_name;
	@JsonProperty(value = "division")
	private String division;
	@JsonProperty(value = "roll_number")
	@Size(max = 45)
	private String rollNumber;
	
	@JsonIgnoreProperties
	private String registrationCode;
	
	@JsonIgnoreProperties
	private String mobile;
	
	@JsonIgnoreProperties
	private String email;
	
	@JsonIgnoreProperties
	private String instituteLogoUrl;
	
	@JsonIgnoreProperties
	private String branchName;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstitute_name() {
		return institute_name;
	}

	public void setInstitute_name(String institute_name) {
		this.institute_name = institute_name;
	}

	public String getStandard_name() {
		return standard_name;
	}

	public void setStandard_name(String standard_name) {
		this.standard_name = standard_name;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInstituteLogoUrl() {
		return instituteLogoUrl;
	}

	public void setInstituteLogoUrl(String instituteLogoUrl) {
		this.instituteLogoUrl = instituteLogoUrl;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	

}
