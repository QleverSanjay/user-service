package com.isg.service.user.request;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineAdmissionFee {

	@JsonProperty("caste_id")
	//@NotNull
	Integer casteId;

	@JsonProperty("subcaste_id")
	Integer subcasteId;

	@JsonProperty("amount")
	//@NotNull
	float amount;

	@JsonProperty("course_id")
	@NotNull
	Integer courseId;

	@JsonProperty("token")
	@NotNull
	String token;

	public Integer getCasteId() {
		return casteId;
	}

	public void setCasteId(Integer casteId) {
		this.casteId = casteId;
	}

	public Integer getSubcasteId() {
		return subcasteId;
	}

	public void setSubcasteId(Integer subcasteId) {
		this.subcasteId = subcasteId;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "OnlineAdmissionFee [casteId=" + casteId + ", subcasteId=" + subcasteId + ", amount=" + amount + ", courseId=" + courseId + "]";
	}

}
