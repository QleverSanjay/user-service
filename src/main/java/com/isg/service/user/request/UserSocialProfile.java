package com.isg.service.user.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSocialProfile {

	@JsonProperty(value = "username")
	@NotNull
	@Size(max = 100)
	String username;

	String profileId;

	@JsonProperty(value = "provider")
	@NotNull
	@Size(max = 20)
	String provider;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public String getProfileId() {
		return profileId;
	}

	@JsonProperty(value = "profile_id")
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

}
