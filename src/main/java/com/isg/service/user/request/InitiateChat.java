package com.isg.service.user.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InitiateChat {

	@JsonProperty(value = "is_private_chat_flag")
	@NotNull
	@Size(max = 1)
	private String isPrivateChatFlag;

	@JsonProperty(value = "chat_with_contact_user_ids")
	@NotNull
	@Size(max = 1000)
	private String chatContactIds;

	@JsonProperty(value = "group_name")
	@Size(max = 56)
	String name;

	public String getIsPrivateChatFlag() {
		return isPrivateChatFlag;
	}

	public void setIsPrivateChatFlag(String isPrivateChatFlag) {
		this.isPrivateChatFlag = isPrivateChatFlag;
	}

	public String getChatContactIds() {
		return chatContactIds;
	}

	public void setChatContactIds(String chatContactIds) {
		this.chatContactIds = chatContactIds;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "InitiateChat [isPrivateChatFlag=" + isPrivateChatFlag + ", chatContactIds=" + chatContactIds + ", name=" + name + "]";
	}

}
