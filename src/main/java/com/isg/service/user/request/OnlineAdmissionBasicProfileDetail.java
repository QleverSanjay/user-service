package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineAdmissionBasicProfileDetail {

	@JsonProperty("first_name")
	@NotNull
	@Size(max = 45)
	String firstName;

	@JsonProperty("middle_name")
	@Size(max = 45)
	String middleName;

	@JsonProperty("surname")
	@NotNull
	@Size(max = 45)
	String surname;

	@JsonProperty("gender")
	@NotNull
	char gender;

	@JsonProperty("dob")
	@NotNull
	@Size(min = 10, max = 10)
	String dob;

	@JsonProperty("mobile")
	@NotNull
	@Size(max = 12)
	String mobile;

	@JsonProperty("email")
	@NotNull
	@Size(max = 100)
	String email;

	@JsonProperty("url_configuration_id")
	@NotNull
	@Digits(integer = 11, fraction = 0)
	Integer urlConfigurationId;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getUrlConfigurationId() {
		return urlConfigurationId;
	}

	public void setUrlConfigurationId(Integer urlConfigurationId) {
		this.urlConfigurationId = urlConfigurationId;
	}

	@Override
	public String toString() {
		return "OnlineAdmissionBasicProfileDetail [firstName=" + firstName + ", middleName=" + middleName + ", surname=" + surname + ", gender=" + gender
						+ ", dob=" + dob + ", mobile=" + mobile + ", email=" + email + ", urlConfigurationId=" + urlConfigurationId
						+ "]";
	}

}
