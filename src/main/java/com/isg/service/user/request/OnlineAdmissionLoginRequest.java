package com.isg.service.user.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineAdmissionLoginRequest {

	@JsonProperty("applicant_Id")
	@NotNull
	@Size(max = 45)
	String applicantId;

	@JsonProperty("password")
	@NotNull
	@Size(max = 45)
	String password;

	public String getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
