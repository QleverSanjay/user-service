package com.isg.service.user.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RememberMeLogin {

	@Size(max = 255)
	@NotNull
	String gcmId;

	@Size(max = 10)
	@NotNull
	String deviceType;

	@JsonIgnoreProperties
	public String getGcmId() {
		return gcmId;
	}

	@JsonProperty(value = "gcm_id")
	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}

	@JsonIgnoreProperties
	public String getDeviceType() {
		return deviceType;
	}

	@JsonProperty(value = "device_type")
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

}
