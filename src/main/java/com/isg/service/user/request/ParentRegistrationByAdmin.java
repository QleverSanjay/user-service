package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ParentRegistrationByAdmin {

	@JsonProperty(value = "firstname")
	@NotNull
	@Size(max = 45)
	String firstname;

	@JsonProperty(value = "middlename")
	@Size(max = 45)
	String middlename;

	@JsonProperty(value = "lastname")
	// @NotNull
	@Size(max = 45)
	String lastname;

	@JsonProperty(value = "gender")
	@Size(max = 1)
	String gender;

	@JsonProperty(value = "dob")
	@Size(max = 10)
	String dob;

	@JsonProperty(value = "email")
	@Size(max = 45)
	String email;

	@Size(max = 40)
	String password;

	@JsonProperty(value = "primaryContact")
	@Size(max = 10)
	String primaryContact;

	@JsonProperty(value = "secondaryContact")
	@Size(max = 10)
	String secondaryContact;

	@JsonProperty(value = "addressLine")
	@Size(max = 45)
	String addressLine;

	@JsonProperty(value = "area")
	@Size(max = 45)
	String area;

	@JsonProperty(value = "city")
	@Size(max = 45)
	String city;

	@JsonProperty(value = "pincode")
	@Size(max = 45)
	String pincode;

	@Digits(integer = 11, fraction = 0)
	Integer districtId;

	@Digits(integer = 11, fraction = 0)
	Integer talukaId;

	@Digits(integer = 11, fraction = 0)
	Integer stateId;

	@Digits(integer = 11, fraction = 0)
	Integer religionId;

	@Digits(integer = 11, fraction = 0)
	Integer casteId;

	@Digits(integer = 11, fraction = 0)
	Integer incomeId;

	@Digits(integer = 11, fraction = 0)
	Integer professionId;

	@JsonProperty(value = "parentUserId")
	@Size(max = 45)
	String parentUserId;

	public String getParentUserId() {
		return parentUserId;
	}

	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty(value = "password")
	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getSecondaryContact() {
		return secondaryContact;
	}

	public void setSecondaryContact(String secondaryContact) {
		this.secondaryContact = secondaryContact;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	@JsonIgnore
	public Integer getDistrictId() {
		return districtId;
	}

	@JsonProperty(value = "districtId")
	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	@JsonIgnore
	public Integer getTalukaId() {
		return talukaId;
	}

	@JsonProperty(value = "talukaId")
	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}

	@JsonIgnore
	public Integer getStateId() {
		return stateId;
	}

	@JsonProperty(value = "stateId")
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	@JsonIgnore
	public Integer getReligionId() {
		return religionId;
	}

	@JsonProperty(value = "religionId")
	public void setReligionId(Integer religionId) {
		this.religionId = religionId;
	}

	@JsonIgnore
	public Integer getCasteId() {
		return casteId;
	}

	@JsonProperty(value = "casteId")
	public void setCasteId(Integer casteId) {
		this.casteId = casteId;
	}

	@JsonIgnore
	public Integer getIncomeId() {
		return incomeId;
	}

	@JsonProperty(value = "income_id")
	public void setIncomeId(Integer incomeId) {
		this.incomeId = incomeId;
	}

	@JsonIgnore
	public Integer getProfessionId() {
		return professionId;
	}

	@JsonProperty(value = "profession_id")
	public void setProfessionId(Integer professionId) {
		this.professionId = professionId;
	}

	@Override
	public String toString() {
		return "ParentRegistrationByAdmin [firstname=" + firstname + ", middlename=" + middlename + ", lastname=" + lastname + ", gender=" + gender + ", dob="
						+ dob + ", email=" + email + ", password=" + password + ", primaryContact=" + primaryContact + ", secondaryContact=" + secondaryContact
						+ ", addressLine=" + addressLine + ", area=" + area + ", city=" + city + ", pincode=" + pincode + ", districtId=" + districtId
						+ ", talukaId=" + talukaId + ", stateId=" + stateId + ", religionId=" + religionId + ", casteId=" + casteId + ", incomeId=" + incomeId
						+ ",parentUserId=" + parentUserId + ", professionId=" + professionId + "]";
	}

}
