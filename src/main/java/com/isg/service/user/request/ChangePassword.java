package com.isg.service.user.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePassword {

	@JsonProperty(value = "current_password")
	@NotNull
	@Size(max = 40)
	String currentPassword;

	@JsonProperty(value = "new_password")
	@NotNull
	@Size(max = 40)
	String newPassword;

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Override
	public String toString() {
		return "ChangePassword [currentPassword=" + currentPassword
						+ ", newPassword=" + newPassword + "]";
	}

}
