package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Goal {
	@JsonProperty(value = "id")
	@Digits(integer = 11, fraction = 0)
	Integer id;

	private String identifier;

	@JsonProperty(value = "title")
	@NotNull
	@Size(max = 45)
	String title;

	@JsonProperty(value = "description")
	@Size(max = 100)
	String description;

	@JsonProperty(value = "start_date")
	@NotNull
	@Size(max = 20)
	String startDate;

	@JsonProperty(value = "end_date")
	@NotNull
	@Size(max = 20)
	String endDate;

	@NotNull
	@Size(max = 15)
	String rewardType;

	@JsonProperty(value = "amount")
	@Digits(integer = 11, fraction = 2)
	float amount;

	@JsonProperty(value = "reward_description")
	@Size(max = 45)
	String rewardDescription;

	@JsonProperty(value = "feedback")
	@Size(max = 45)
	String feedback;

	@Size(max = 15)
	String status;

	@JsonProperty(value = "student_id")
	@NotNull
	@Digits(integer = 11, fraction = 0)
	Integer studentId;

	String createdDate;

	String studentName;

	String studentGender;

	Integer studentUserId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getRewardType() {
		return rewardType;
	}

	public void setRewardType(String rewardType) {
		this.rewardType = rewardType;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getRewardDescription() {
		return rewardDescription;
	}

	public void setRewardDescription(String rewardDescription) {
		this.rewardDescription = rewardDescription;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	@JsonProperty(value = "status")
	public String getStatus() {
		return status;
	}

	@JsonIgnore
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty(value = "created_date")
	public String getCreatedDate() {
		return createdDate;
	}

	@JsonIgnore
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	@JsonProperty(value = "_id")
	public String getIdentifier() {
		return "" + this.id;
	}

	@JsonProperty(value = "student_name")
	public String getStudentName() {
		return studentName;
	}

	@JsonIgnore
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	@JsonProperty(value = "student_gender")
	public String getStudentGender() {
		return studentGender;
	}

	@JsonIgnore
	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	@JsonIgnore
	public Integer getStudentUserId() {
		return studentUserId;
	}

	@JsonIgnore
	public void setStudentUserId(Integer studentUserId) {
		this.studentUserId = studentUserId;
	}

	@Override
	public String toString() {
		return "Goal [id=" + id + ",  title=" + title + ", description=" + description + ", startDate=" + startDate
						+ ", endDate=" + endDate + ", rewardType=" + rewardType + ", amount=" + amount + ", rewardDescription=" + rewardDescription
						+ ", feedback=" + feedback + ", status=" + status + ", studentId=" + studentId + ", createdDate=" + createdDate + ", studentName="
						+ studentName + "]";
	}

}
