package com.isg.service.user.request;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentDirect {
	
	@JsonProperty(value = "parent_id")
	@Digits(integer = 11, fraction = 0)
	private Integer parentId;
	@JsonProperty(value = "otp")
	@Size(max = 10)
	private String otp;
	@JsonProperty(value = "personal_details")
	private StudentPersonDetails personalDetails = new StudentPersonDetails();
	@JsonProperty(value = "education_details")
	private StudentEducationDetails educationDetails = new StudentEducationDetails();
	@JsonProperty(value = "other_details")
	private StudentMiscellaneousDetails otherDetails = new StudentMiscellaneousDetails();
	@JsonProperty(value = "student_upload_id")
	private Long studentUploadId;
	@JsonProperty(value = "user_id")
	@Digits(integer = 11, fraction = 0)
	private int userId;
	@JsonProperty(value = "username")
	@Size(max = 100)
	private String username;
	private String status;
	private String token;
	private String email;
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	List<String> roles = new ArrayList<String>();
	
	private List<PaymentTransaction> feesInfo;
	public List<PaymentTransaction> getFeesInfo() {
		return feesInfo;
	}

	public void setFeesInfo(List<PaymentTransaction> feesInfo) {
		this.feesInfo = feesInfo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public StudentPersonDetails getPersonalDetails() {
		return personalDetails;
	}

	public void setPersonalDetails(StudentPersonDetails personalDetails) {
		this.personalDetails = personalDetails;
	}

	public StudentEducationDetails getEducationDetails() {
		return educationDetails;
	}

	public void setEducationDetails(StudentEducationDetails educationDetails) {
		this.educationDetails = educationDetails;
	}

	public StudentMiscellaneousDetails getOtherDetails() {
		return otherDetails;
	}

	public void setOtherDetails(StudentMiscellaneousDetails otherDetails) {
		this.otherDetails = otherDetails;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Long getStudentUploadId() {
		return studentUploadId;
	}

	public void setStudentUploadId(Long uploadStudentId) {
		this.studentUploadId = uploadStudentId;
	}

	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}
