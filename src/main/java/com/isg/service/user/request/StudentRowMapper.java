package com.isg.service.user.request;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.StringUtils;

public class StudentRowMapper implements RowMapper<Student> {

	boolean getWithUserDetail;

	public boolean isGetWithUserDetail() {
		return getWithUserDetail;
	}

	public StudentRowMapper(boolean getWithUserDetail) {
		this.getWithUserDetail = getWithUserDetail;
	}

	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {

		StudentPersonDetails personalDetails = new StudentPersonDetails();
		StudentAddress address = new StudentAddress();
		StudentEducationDetails educationalDetails = new StudentEducationDetails();
		StudentMiscellaneousDetails miscellaneousDetails = new StudentMiscellaneousDetails();

    Student student = new Student();
		personalDetails.setId(rs.getInt("id"));
		personalDetails.setName(rs.getString("first_name") + " " + (StringUtils.isEmpty(rs.getString("last_name")) ? "" : rs.getString("last_name")));
		personalDetails.setFirstName(rs.getString("first_name"));
		personalDetails.setLastName(rs.getString("last_name"));
		String dob = rs.getDate("dob") != null ? rs.getDate("dob").toString() : "";
		personalDetails.setDob(dob);
		personalDetails.setRelationWithParent(rs.getString("relation_with_parent"));
		personalDetails.setEmailAddress(rs.getString("email_address"));
		personalDetails.setMobile(rs.getString("mobile"));
		personalDetails.setRegistrationCode(rs.getString("registration_code"));
		personalDetails.setGender(rs.getString("gender"));
		address.setLine1(rs.getString("line1"));
		address.setArea(rs.getString("area"));
		address.setCity(rs.getString("city"));
		address.setPincode(rs.getString("pincode"));
		address.setDistrictId(rs.getInt("district_id") == 0 ? null : rs.getInt("district_id"));
		address.setTalukaId(rs.getInt("taluka_id") == 0 ? null : rs.getInt("taluka_id"));
		address.setStateId(rs.getInt("state_id") == 0 ? null : rs.getInt("state_id"));
		personalDetails.setAddress(address);
		personalDetails.setReligion(rs.getString("religion"));
		personalDetails.setCasteId(rs.getInt("caste_id") == 0 ? null : rs.getInt("caste_id"));
		personalDetails.setPhoto(rs.getString("photo"));
		student.setParentId(rs.getInt("parent_id"));
		educationalDetails.setBranchId(rs.getInt("branch_id") == 0 ? null : rs.getInt("branch_id"));
		educationalDetails.setStandard(rs.getInt("standard_id") == 0 ? null : rs.getInt("standard_id"));
		educationalDetails.setDivision(rs.getInt("division_id") == 0 ? null : rs.getInt("division_id"));
		educationalDetails.setRollNumber(rs.getString("roll_number"));
		educationalDetails.setBranchName(rs.getString("branch_name"));
		educationalDetails.setStandardName(rs.getString("standard_name"));
		educationalDetails.setDivisionName(rs.getString("division_name"));
		miscellaneousDetails.setGoalChild(rs.getString("goal_child"));
		miscellaneousDetails.setGoalParent(rs.getString("goal_parent"));
		miscellaneousDetails.setHobbies(rs.getString("hobbies"));
		student.setEducationDetails(educationalDetails);
		student.setPersonalDetails(personalDetails);
		student.setOtherDetails(miscellaneousDetails);
		student.setUserId(rs.getLong("user_id"));

		student.setShowBranchLogo(rs.getString("showBranchLogo"));
		student.setBranchLogoUrl(rs.getString("branchLogoUrl"));

		if (isGetWithUserDetail()) {
			student.setUsername(rs.getString("username"));
		}
		return student;
	}
}
