package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Teacher {

	private Integer id;

	@JsonProperty(value = "firstname")
	@NotNull
	@Size(max = 45)
	private String firstName;

	@JsonProperty(value = "lastname")
	@NotNull
	@Size(max = 45)
	private String lastName;

	@Size(max = 10)
	private String dob;

	@NotNull
	@Size(max = 45)
	private String email;

	@NotNull
	@Size(max = 1)
	private String gender;

	@JsonProperty(value = "primaryContact")
	@NotNull
	@Size(max = 15)
	private String primaryContactNumber;

	@Size(max = 15)
	@JsonProperty(value = "secondaryContact")
	private String secondContactNumber;

	@JsonProperty(value = "addressLine")
	@Size(max = 45)
	private String addressLine;

	@Size(max = 45)
	private String area;

	@NotNull
	@Size(max = 45)
	private String city;

	@NotNull
	@Size(max = 45)
	private String pincode;

	@Size(max = 255)
	private String photo;

	@JsonProperty(value = "branch_id")
	@Digits(integer = 11, fraction = 0)
	private Integer branchId;

	@JsonProperty(value = "districtId")
	@Digits(integer = 11, fraction = 0)
	Integer districtId;

	@JsonProperty(value = "talukaId")
	@Digits(integer = 11, fraction = 0)
	Integer talukaId;

	@JsonProperty(value = "stateId")
	@Digits(integer = 11, fraction = 0)
	Integer stateId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPrimaryContactNumber() {
		return primaryContactNumber;
	}

	public void setPrimaryContactNumber(String primaryContactNumber) {
		this.primaryContactNumber = primaryContactNumber;
	}

	public String getSecondContactNumber() {
		return secondContactNumber;
	}

	public void setSecondContactNumber(String secondContactNumber) {
		this.secondContactNumber = secondContactNumber;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getTalukaId() {
		return talukaId;
	}

	public void setTalukaId(Integer talukaId) {
		this.talukaId = talukaId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	@Override
	public String toString() {
		return "Teacher [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", dob=" + dob + ", email=" + email + ", gender=" + gender
						+ ", primaryContactNumber=" + primaryContactNumber + ", secondContactNumber=" + secondContactNumber + ", addressLine=" + addressLine
						+ ", area=" + area + ", city=" + city + ", pincode=" + pincode + ", photo=" + photo + ", branchId=" + branchId + "]";
	}

}
