package com.isg.service.user.request;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentMiscellaneousDetails {
	@JsonProperty(value = "goal_child")
	@Size(max = 45)
	private String goalChild;
	@JsonProperty(value = "goal_parent")
	@Size(max = 45)
	private String goalParent;
	@JsonProperty(value = "hobbies")
	@Size(max = 100)
	private String hobbies;

	public String getGoalChild() {
		return (goalChild != null) ? goalChild : "";
	}

	public void setGoalChild(String goalChild) {
		this.goalChild = goalChild;
	}

	public String getGoalParent() {
		return (goalParent != null) ? goalParent : "";
	}

	public void setGoalParent(String goalParent) {
		this.goalParent = goalParent;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	@Override
	public String toString() {
		return "StudentMiscellaneousDetails [goalChild=" + goalChild + ", goalParent=" + goalParent + ", hobbies=" + hobbies + "]";
	}

}
