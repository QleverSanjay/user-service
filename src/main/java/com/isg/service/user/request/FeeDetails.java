package com.isg.service.user.request;

import java.sql.Date;

import lombok.Data;

@Data
public class FeeDetails {
	private Integer id;
	private Integer head_id;
	private String description;
	private Date duedate;
	private Date fromdate;
	private Date todate;
	private Integer caste_id;
	private String standard;
	private String amount;
	private Integer branch_id;

}
