package com.isg.service.user.request;

public class BranchForumDetail {

	Integer branchId;

	Integer categoryId;

	Integer readWriteGroupId;

	Integer readOnlyGroupId;

	public BranchForumDetail(Integer branchId, Integer categoryId, Integer readWriteGroupId, Integer readOnlyGroupId) {
		super();
		this.branchId = branchId;
		this.categoryId = categoryId;
		this.readWriteGroupId = readWriteGroupId;
		this.readOnlyGroupId = readOnlyGroupId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getReadWriteGroupId() {
		return readWriteGroupId;
	}

	public void setReadWriteGroupId(Integer readWriteGroupId) {
		this.readWriteGroupId = readWriteGroupId;
	}

	public Integer getReadOnlyGroupId() {
		return readOnlyGroupId;
	}

	public void setReadOnlyGroupId(Integer readOnlyGroupId) {
		this.readOnlyGroupId = readOnlyGroupId;
	}

}
