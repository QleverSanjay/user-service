package com.isg.service.user.request;

public class InstituteBranch {

	private Integer instituteBranchId;
	private Integer branchId;
	private Integer instituteId;
	private boolean active;
	public Integer getInstituteBranchId() {
		return instituteBranchId;
	}
	public void setInstituteBranchId(Integer instituteBranchId) {
		this.instituteBranchId = instituteBranchId;
	}
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public Integer getInstituteId() {
		return instituteId;
	}
	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
