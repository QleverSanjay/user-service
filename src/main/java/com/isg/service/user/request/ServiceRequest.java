package com.isg.service.user.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ServiceRequest {
	
	@JsonProperty(value = "studentId")
	private Integer studentId;
	
	@JsonProperty(value = "requestTypeId")
	private Integer serviceRequestId;
	
	@JsonIgnoreProperties
	private Integer parentId;
	
	@JsonProperty(value = "body")
	private String emailBody;
	
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public Integer getServiceRequestId() {
		return serviceRequestId;
	}
	public void setServiceRequestId(Integer serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}
	public String getEmailBody() {
		return emailBody;
	}
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
	@Override
	public String toString() {
		return "ServiceRequest [studentId=" + studentId + ", serviceRequestId=" + serviceRequestId + ", parentId=" + parentId + ", emailBody=" + emailBody
						+ "]";
	}

}

