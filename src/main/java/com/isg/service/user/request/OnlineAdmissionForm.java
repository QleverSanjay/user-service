package com.isg.service.user.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineAdmissionForm {

	@JsonProperty("applicant_Id")
	@NotNull
	@Size(max = 45)
	String applicantId;

	@JsonProperty("token")
	@NotNull
	@Size(max = 20)
	String tempAccessId;

	@JsonProperty("form_payload")
	@NotNull
	String formPayload;
	
	String printPdfPath;
	
	Integer id;
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrintPdfPath() {
		return printPdfPath;
	}

	public void setPrintPdfPath(String printPdfPath) {
		this.printPdfPath = printPdfPath;
	}

	public String getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}

	public String getTempAccessId() {
		return tempAccessId;
	}

	public void setTempAccessId(String tempAccessId) {
		this.tempAccessId = tempAccessId;
	}

	public String getFormPayload() {
		return formPayload;
	}

	public void setFormPayload(String formPayload) {
		this.formPayload = formPayload;
	}

	@Override
	public String toString() {
		return "OnlineAdmissionForm [applicantId=" + applicantId + ", tempAccessId=" + tempAccessId + ", formPayload=" + formPayload + "]";
	}

}
