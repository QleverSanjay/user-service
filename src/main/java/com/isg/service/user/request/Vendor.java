package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Vendor {

	@Digits(integer = 11, fraction = 0)
	Integer id;

	@JsonProperty(value = "contact_first_name")
	@NotNull
	@Size(max = 45)
	private String contactFirstName;

	@JsonProperty(value = "contact_last_name")
	@NotNull
	@Size(max = 45)
	private String contactLastName;

	@JsonProperty(value = "email")
	@NotNull
	@Size(max = 56)
	private String email;

	@NotNull
	@Size(max = 40)
	private String password;

	@Size(max = 11)
	private String shopping_profile_id;

	@Digits(integer = 11, fraction = 0)
	private Integer userId;

	@JsonProperty(value = "photo")
	@Size(max = 255)
	private String photo;

	@JsonProperty(value = "active")
	@NotNull
	private char status;

	@JsonProperty(value = "is_registration_approved")
	@NotNull
	private char isRegistrationApproved;

	@JsonProperty(value = "id")
	public Integer getId() {
		return id;
	}

	@JsonIgnore
	public void setId(Integer id) {
		this.id = id;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty(value = "password")
	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	public String getShopping_profile_id() {
		return shopping_profile_id;
	}

	@JsonProperty(value = "shopping_profile_id")
	public void setShopping_profile_id(String shopping_profile_id) {
		this.shopping_profile_id = shopping_profile_id;
	}

	@JsonProperty(value = "user_id")
	public Integer getUserId() {
		return userId;
	}

	@JsonIgnore
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public char getIsRegistrationApproved() {
		return isRegistrationApproved;
	}

	public void setIsRegistrationApproved(char isRegistrationApproved) {
		this.isRegistrationApproved = isRegistrationApproved;
	}

	@Override
	public String toString() {
		return "Vendor [id=" + id + ", contactFirstName=" + contactFirstName + ", contactLastName=" + contactLastName + ", email=" + email
						+ ", shopping_profile_id=" + shopping_profile_id + ", userId=" + userId + ", photo=" + photo + ", status=" + status
						+ ", isRegistrationApproved=" + isRegistrationApproved + "]";
	}

}
