package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentPersonDetails {
	@JsonProperty(value = "id")
	@Digits(integer = 11, fraction = 0)
	private Integer id;
	@JsonProperty(value = "name")
	@NotNull
	@Size(max = 45)
	private String name;
	@JsonProperty(value = "first_name")
	@NotNull
	@Size(max = 45)
	private String firstName;
	@JsonProperty(value = "last_name")
	@NotNull
	@Size(max = 45)
	private String lastName;
	@JsonProperty(value = "dob")
	@NotNull
	@Size(max = 10)
	private String dob;
	@JsonProperty(value = "relation_with_parent")
	@Size(max = 45)
	private String relationWithParent;
	@JsonProperty(value = "email_address")
	@Size(max = 45)
	private String emailAddress;
	@JsonProperty(value = "mobile")
	@Size(max = 10)
	private String mobile;
	@JsonProperty(value = "registration_code")
	@Size(max = 10)
	private String registrationCode;
	@JsonProperty(value = "religion")
	@Size(max = 10)
	private String religion;
	@JsonProperty(value = "caste_id")
	@Digits(integer = 11, fraction = 0)
	private Integer casteId;
	@JsonProperty(value = "photo")
	@Size(max = 255)
	private String photo;
	@JsonProperty(value = "address")
	private StudentAddress address;
	@JsonProperty(value = "gender")
	@NotNull
	@Size(max = 10)
	private String gender;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGender() {
		return (gender != null) ? gender : "";
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getRelationWithParent() {
		return (relationWithParent != null) ? relationWithParent : "";
	}

	public void setRelationWithParent(String relationWithParent) {
		this.relationWithParent = relationWithParent;
	}

	public String getEmailAddress() {
		return (emailAddress != null) ? emailAddress : "";
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMobile() {
		return (mobile != null) ? mobile : "";
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRegistrationCode() {
		return (registrationCode != null) ? registrationCode : "";
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public String getReligion() {
		return (religion != null) ? religion : "";
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public Integer getCasteId() {
		return casteId;
	}

	public void setCasteId(Integer casteId) {
		this.casteId = casteId;
	}

	public String getPhoto() {
		return (photo != null) ? photo : "";
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public StudentAddress getAddress() {
		return address;
	}

	public void setAddress(StudentAddress address) {
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "StudentPersonDetails [id=" + id + ", name=" + name + ", dob=" + dob + ", relationWithParent=" + relationWithParent + ", emailAddress="
						+ emailAddress + ", mobile=" + mobile + ", registrationCode=" + registrationCode + ", religion=" + religion + ", casteId=" + casteId
						+ ", photo=" + photo + ", address=" + address + ", gender=" + gender + "]";
	}

}
