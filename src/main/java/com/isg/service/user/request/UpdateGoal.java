package com.isg.service.user.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateGoal {
	@JsonProperty(value = "goal_id")
	@NotNull
	@Digits(integer = 11, fraction = 0)
	Integer id;

	@JsonProperty(value = "feedback")
	@Size(max = 255)
	String feedback;

	@JsonProperty(value = "status")
	@NotNull
	@Size(max = 15)
	String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
