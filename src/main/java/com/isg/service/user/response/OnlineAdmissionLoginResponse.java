package com.isg.service.user.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.model.OnlineAdmissionFormDetail;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineAdmissionLoginResponse {

	@JsonProperty("form_details")
	OnlineAdmissionFormDetail onlineAdmissionFormDetail;

	@JsonProperty("token")
	String token;

	@JsonProperty("payment_status")
	String paymentStatus;

	@JsonProperty("online_admission_configuration")
	OnlineAdmissionConfiguration onlineAdmissionConfiguration;

	public OnlineAdmissionConfiguration getOnlineAdmissionConfiguration() {
		return onlineAdmissionConfiguration;
	}

	public void setOnlineAdmissionConfiguration(OnlineAdmissionConfiguration onlineAdmissionConfiguration) {
		this.onlineAdmissionConfiguration = onlineAdmissionConfiguration;
	}

	public OnlineAdmissionFormDetail getOnlineAdmissionFormDetail() {
		return onlineAdmissionFormDetail;
	}

	public void setOnlineAdmissionFormDetail(OnlineAdmissionFormDetail onlineAdmissionFormDetail) {
		this.onlineAdmissionFormDetail = onlineAdmissionFormDetail;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

}
