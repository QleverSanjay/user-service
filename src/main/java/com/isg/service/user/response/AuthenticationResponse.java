package com.isg.service.user.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.isg.service.identity.model.Identity.BRANCH_CONFIGURATION_FEATURE;
import com.isg.service.user.model.ShoppingProfile;
import com.isg.service.user.model.UserForumProfile;
import com.isg.service.user.request.ChatProfile;
import com.isg.service.user.util.Constant;

@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = Id.NAME)
@JsonTypeName(value = "authentication")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationResponse {

	@JsonProperty("status")
	String status = Constant.USER_STATUS.EXISTING.get();

	@JsonProperty("token")
	String token;

	@JsonProperty("roles")
	List<String> roles = new ArrayList<String>();

	@JsonProperty("chatProfile")
	ChatProfile chatProfile;

	@JsonProperty("forumProfile")
	UserForumProfile forumProfile;

	@JsonProperty("shoppingProfile")
	ShoppingProfile shoppingProfile;

	@JsonProperty("branch_configuration_detail")
	Map<BRANCH_CONFIGURATION_FEATURE, String> branchConfiguration;
	@JsonProperty("loginCount")
	private String loginCount;

	public String getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(String loginCount) {
		this.loginCount = loginCount;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public ChatProfile getChatProfile() {
		return chatProfile;
	}

	public void setChatProfile(ChatProfile chatProfile) {
		this.chatProfile = chatProfile;
	}

	public UserForumProfile getForumProfile() {
		return forumProfile;
	}

	public void setForumProfile(UserForumProfile forumProfile) {
		this.forumProfile = forumProfile;
	}

	public ShoppingProfile getShoppingProfile() {
		return shoppingProfile;
	}

	public void setShoppingProfile(ShoppingProfile shoppingProfile) {
		this.shoppingProfile = shoppingProfile;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Map<BRANCH_CONFIGURATION_FEATURE, String> getBranchConfiguration() {
		return branchConfiguration;
	}

	public void setBranchConfiguration(Map<BRANCH_CONFIGURATION_FEATURE, String> branchConfiguration) {
		this.branchConfiguration = branchConfiguration;
	}

	@Override
	public String toString() {
		return "";
	}

}
