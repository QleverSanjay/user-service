package com.isg.service.user.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveOnlineAdmissionBasicProfileDetailResponse {

	String applicantId;

	String tempAccessId;

	@JsonProperty("applicant_Id")
	public String getApplicantId() {
		return applicantId;
	}

	@JsonIgnore
	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}

	@JsonProperty("temp_access_Id")
	public String getTempAccessId() {
		return tempAccessId;
	}

	@JsonIgnore
	public void setTempAccessId(String tempAccessId) {
		this.tempAccessId = tempAccessId;
	}

}
