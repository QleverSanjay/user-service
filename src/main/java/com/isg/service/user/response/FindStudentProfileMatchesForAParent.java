package com.isg.service.user.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FindStudentProfileMatchesForAParent {

	@JsonProperty("id")
	private String id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("institute_name")
	private String instituteName;

	@JsonProperty("standard_name")
	private String standardName;

	@JsonProperty("division")
	private String division;

	@JsonProperty("roll_number")
	private String rollNumber;

	@JsonProperty("mobile_match_found")
	private int isParentMobileMatch;

	@JsonProperty("email_match_found")
	private int isParentEmailMatch;

	public FindStudentProfileMatchesForAParent(String id, String name, String instituteName, String standardName, String division, String rollNumber,
					int isParentMobileMatch, int isParentEmailMatch) {
		this.id = id;
		this.name = name;
		this.instituteName = instituteName;
		this.standardName = standardName;
		this.division = division;
		this.rollNumber = rollNumber;
		this.isParentMobileMatch = isParentMobileMatch;
		this.isParentEmailMatch = isParentEmailMatch;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public int getIsParentMobileMatch() {
		return isParentMobileMatch;
	}

	public void setIsParentMobileMatch(int isParentMobileMatch) {
		this.isParentMobileMatch = isParentMobileMatch;
	}

	public int getIsParentEmailMatch() {
		return isParentEmailMatch;
	}

	public void setIsParentEmailMatch(int isParentEmailMatch) {
		this.isParentEmailMatch = isParentEmailMatch;
	}

	@Override
	public String toString() {
		return "FindStudentProfileMatchesForAParent [id=" + id + ", name=" + name + ", instituteName=" + instituteName + ", standardName=" + standardName
						+ ", division=" + division + ", rollNumber=" + rollNumber + ", isParentMobileMatch=" + isParentMobileMatch + ", isParentEmailMatch="
						+ isParentEmailMatch + "]";
	}

}
