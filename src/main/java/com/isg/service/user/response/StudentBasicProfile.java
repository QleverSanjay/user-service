package com.isg.service.user.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = Id.NAME)
//@JsonTypeName(value = "student_profile")
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentBasicProfile {

	@JsonProperty("id")
	long id;

	@JsonProperty("student_name")
	String name;

	@JsonProperty("photo_url")
	String photo;
	
	@JsonProperty("gender")
	String gender;
	
	public StudentBasicProfile(long id, String name, String photo, String gender) {
		this.id = id;
		this.name = name;
		this.photo = photo;
		this.gender = gender;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "StudentBasicProfile [id=" + id + ", name=" + name + ", photo="
				+ photo + ", gender=" + gender + "]";
	}

}
