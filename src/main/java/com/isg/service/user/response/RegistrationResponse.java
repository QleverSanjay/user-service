package com.isg.service.user.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.isg.service.user.model.ShoppingProfile;
import com.isg.service.user.request.ChatProfile;

@Deprecated
@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = Id.NAME)
@JsonTypeName(value = "authentication")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistrationResponse {

	@JsonProperty("status")
	String status;

	@JsonProperty("message")
	String message;

	@JsonProperty("parentId")
	int parentId;

	@JsonProperty("userId")
	int userId;

	@JsonProperty("chatProfile")
	ChatProfile chatProfile;

	@JsonProperty("shoppingProfile")
	ShoppingProfile shoppingProfile;

	@JsonProperty("token")
	String token;

	List<String> roles = new ArrayList<String>();

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public ChatProfile getChatProfile() {
		return chatProfile;
	}

	public void setChatProfile(ChatProfile chatProfile) {
		this.chatProfile = chatProfile;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@JsonProperty("roles")
	public List<String> getRoles() {
		return roles;
	}

	@JsonIgnore
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public ShoppingProfile getShoppingProfile() {
		return shoppingProfile;
	}

	public void setShoppingProfile(ShoppingProfile shoppingProfile) {
		this.shoppingProfile = shoppingProfile;
	}

	@Override
	public String toString() {
		return "RegistrationResponse [status=" + status + ", message=" + message + ", parentId=" + parentId + ", userId=" + userId + ", chatProfile="
						+ chatProfile + ", shoppingProfile=" + shoppingProfile + ", token=" + token + ", roles=" + roles + "]";
	}

}
