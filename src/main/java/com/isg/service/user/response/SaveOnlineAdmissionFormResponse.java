package com.isg.service.user.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveOnlineAdmissionFormResponse {

	String applicantId;

	String password;

	@JsonProperty("applicant_Id")
	public String getApplicantId() {
		return applicantId;
	}

	@JsonIgnore
	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}

	@JsonProperty("password")
	public String getPassword() {
		return password;
	}

	@JsonIgnore
	public void setPassword(String password) {
		this.password = password;
	}

}
