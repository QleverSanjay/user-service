package com.isg.service.user.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ParentRegistrationByAdminResponse {

	@JsonProperty(value = "parent_id")
	Integer parentId;

	@JsonProperty(value = "user_id")
	Integer userId;

	@JsonProperty(value = "chat_profile_id")
	Integer chatProfileId;

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getChatProfileId() {
		return chatProfileId;
	}

	public void setChatProfileId(Integer chatProfileId) {
		this.chatProfileId = chatProfileId;
	}

	public void setChatProfileId(int chatProfileId) {
		this.chatProfileId = chatProfileId;

	}

}
