package com.isg.service.user.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OnlineAdmissionConfiguration {

	@JsonProperty("admission_form_search_enabled")
	private String admissionFormSearchEnabled;

	@JsonProperty("admission_form_edit_enabled_for_student")
	private String admissionFormEditEnabledForStudent;

	@JsonProperty("admission_form_payment_enabled")
	private String admissionFormPaymentEnabled;

	@JsonProperty("admission_form_for")
	private String admissionFormFor;

	@JsonProperty("allow_to_specify_custom_admission_fees_amount")
	private String allowToSpecifyCustomAdmissionFeesAmount;

	@JsonProperty("admission_form_redirect_to_payment")
	private String admissionFormRedirectToPayment;

	private Integer branchId;

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getAllowToSpecifyCustomAdmissionFeesAmount() {
		return allowToSpecifyCustomAdmissionFeesAmount;
	}

	public void setAllowToSpecifyCustomAdmissionFeesAmount(String allowToSpecifyCustomAdmissionFeesAmount) {
		this.allowToSpecifyCustomAdmissionFeesAmount = allowToSpecifyCustomAdmissionFeesAmount;
	}

	public String getAdmissionFormRedirectToPayment() {
		return admissionFormRedirectToPayment;
	}

	public void setAdmissionFormRedirectToPayment(String admissionFormRedirectToPayment) {
		this.admissionFormRedirectToPayment = admissionFormRedirectToPayment;
	}

	public String getAdmissionFormFor() {
		return admissionFormFor;
	}

	public void setAdmissionFormFor(String admissionFormFor) {
		this.admissionFormFor = admissionFormFor;
	}

	public String getAdmissionFormSearchEnabled() {
		return admissionFormSearchEnabled;
	}

	public void setAdmissionFormSearchEnabled(String admissionFormSearchEnabled) {
		this.admissionFormSearchEnabled = admissionFormSearchEnabled;
	}

	public String getAdmissionFormEditEnabledForStudent() {
		return admissionFormEditEnabledForStudent;
	}

	public void setAdmissionFormEditEnabledForStudent(String admissionFormEditEnabledForStudent) {
		this.admissionFormEditEnabledForStudent = admissionFormEditEnabledForStudent;
	}

	public String getAdmissionFormPaymentEnabled() {
		return admissionFormPaymentEnabled;
	}

	public void setAdmissionFormPaymentEnabled(String admissionFormPaymentEnabled) {
		this.admissionFormPaymentEnabled = admissionFormPaymentEnabled;
	}

}
