package com.isg.service.user.validator;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

//import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.isg.service.user.model.StudentRegistration;
import com.isg.service.user.request.Branch;
import com.isg.service.user.util.DateUtil;
import com.jolbox.bonecp.BoneCPDataSource;

@Scope(value="prototype")
@Repository
public class StudentValidator extends JdbcDaoSupport implements Validator {

	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	/*@Autowired
	MasterValidator masterValidator;*/

	
	private List<StudentRegistration> studentList;
	private DateUtil dateUtil;
	private int index;

	/*private Map<String, Integer> STADARD_MAP, DIVISION_MAP, CASTE_MAP, FEES_CODE_MAP, INCOME_RANGE_MAP, PROFESSION_MAP;

	private Map<String, Integer> STATE_MAP;

	private Map<String, List<StudentRegistration>> studentMap;

	private Map<Integer, Map<String, Integer>> DISTRICT_MAP_BY_STATE;

	private Map<Integer, Map<String, Integer>> TALUKA_MAP_BY_DISTRICT;*/

	public void setIndex(int index) {
		this.index = index;
	}

	public StudentValidator() {
		dateUtil = new DateUtil();
	}

	public boolean supports(Class<?> clazz) {
		return StudentRegistration.class.isAssignableFrom(clazz);
	}

	public void setStudentRegistrationList(List<StudentRegistration> studentList) {
		this.studentList = studentList;
	}




	public void validate(Object target, Errors errors) {
		StudentRegistration student = (StudentRegistration) target;
		try {
				validateAndSetUserName(student, errors);		
			checkStudentRegistrationIsUnique(student, errors);
		} catch (Exception e) {
			e.printStackTrace();
			errors.rejectValue("", "errors.exception", "Problem while validating student.");
		}
	}




	private void validateAndSetUserName(StudentRegistration student, Errors errors) {
		Integer branchId=student.getBranchId();
		String query="select b.*,bc.unique_identifier_lable as uniqueIdentifierLable,bc.unique_identifier_name as uniqueIdentifierName from branch as b inner join branch_configuration as bc on bc.branch_id=b.id where b.id="+student.getBranchId();
		
		@SuppressWarnings("unchecked")
		Branch branch= getJdbcTemplate().queryForObject(query,new BeanPropertyRowMapper<Branch>(Branch.class));
		System.out.println(branch);
		
		System.out.println("branchId******************"+branchId);
		System.out.println("branch name**********"+student.getBranchName());
		String userId = null;
		if(branch.getUniqueIdentifierName().equalsIgnoreCase("Email")){
			 userId=student.getEmailAddress();
			
		}
		else if(branch.getUniqueIdentifierName().equalsIgnoreCase("Mobile")){
			
			userId=student.getMobile();
		}
		else if(branch.getUniqueIdentifierName().equalsIgnoreCase("Registration Number")){
			
			userId=student.getBranchId()+"/"+!StringUtils.isEmpty(student.getRegistrationCode());
		}
		
	
		try {
			
				String sql = "select count(*) from user where username='"
						+ userId + "'";
				Integer count = getJdbcTemplate().queryForObject(sql,
						Integer.class);
				if (count > 0) {
					/*userId = getRandomUserName(student.getFirstname(),
							student.getSurname());*/
					
					errors.rejectValue("studentUserId",
							"error.studentUserId",
							"invalid student User Id occured due to duplicate email or mobile or registration code");
				}
				student.setStudentUserId(userId);
			
		} catch (Exception e) {
			errors.rejectValue("Unique Identity",
					"error.userName",
					"invalid unique identity field");
			e.printStackTrace();
		}
	}




	private void checkStudentRegistrationIsUnique(StudentRegistration student, final Errors errors) {
		// Check name email and mobile is unique.
		String sql = null;
		/* String userName = student.getEmailAddress(); */

		sql = "select count(*) as count from student_upload where hash_code = "
				+ student.hashCode()
				+ (student.getId() != null ? " AND id != (select student_upload_id from student where id = "
						+ student.getId() + ")"
						: "") + " AND isDelete = 'N'";

		System.out.println("Check student with db SQL :::: \n" + sql);
		Integer count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			errors.rejectValue("UserId", "error.userId",
					"StudentRegistration with this username and contact details already exist.");
		}

		if (count.intValue() == 0) {
		
			if (!StringUtils.isEmpty(student.getRollNumber())) {
				// Check rollNumber is unique.
				if (student.getStandardId() == null
						|| student.getDivisionId() == null) {
					errors.rejectValue("", "error.stdDivmandatoryWhenRollNum",
							"Standard and division is mandatory when roll number is specified.");
				} else {
					sql = "select count(*) as count from student where roll_number = '"
							+ student.getRollNumber()
							+ "' AND branch_id = "
							+ student.getBranchId()
							+ " AND standard_id = "
							+ student.getStandardId()
							+ " AND division_id = "
							+ student.getDivisionId()
							+ " AND active = 'Y' AND isDelete = 'N' "
							+ (student.getId() != null ? " AND id != "
									+ student.getId() + "" : "");

					getJdbcTemplate().query(sql, new RowMapper<Integer>() {
						@Override
						public Integer mapRow(ResultSet resultSet, int arg1)
								throws SQLException {
							if (resultSet.getInt("count") > 0) {
								errors.rejectValue("rollNumber",
										"error.rollNumber",
										"StudentRegistration with this roll number already exist.");
							}
							return null;
						}
					});
				}
			}

			if (!StringUtils.isEmpty(student.getRegistrationCode())) {
				// Check registration code is unique.
				sql = "select count(*) as count from student where registration_code = '"
						+ student.getRegistrationCode()
						+ "' AND branch_id = "
						+ student.getBranchId()
						+ " AND active = 'Y' AND isDelete = 'N' "
						+ (student.getId() != null ? " AND id != "
								+ student.getId() + "" : "");

				getJdbcTemplate().query(sql, new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						if (resultSet.getInt("count") > 0) {
							errors.rejectValue("studentRegistrationId",
									"error.registrationCode",
									"StudentRegistration with this registration Code already exist.");
						}
						return null;
					}
				});
			}
			if(!StringUtils.isEmpty(student.getMobile())){
				/*check mobile is unique*/
				
				sql = "select count(*) as count from student where mobile = '"
						+ student.getMobile()+ "'  AND branch_id = "
						+ student.getBranchId()
						+ " AND active = 'Y' AND isDelete = 'N' ";
				
				System.out.println(sql);

				getJdbcTemplate().query(sql, new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						if (resultSet.getInt("count") > 0) {
							errors.rejectValue("mobile",
									"error.mobile",
									"StudentRegistration with this mobile is exist.");
						}
						return null;
					}
				});
				
				
			}
			
			if(!StringUtils.isEmpty(student.getEmailAddress())){
				/*check email is unique*/
				
				sql = "select count(*) as count from student where email_address = '"
						+ student.getEmailAddress()+ "'  AND branch_id = "
								+ student.getBranchId()
								+ " AND active = 'Y' AND isDelete = 'N' ";

				getJdbcTemplate().query(sql, new RowMapper<Integer>() {
					@Override
					public Integer mapRow(ResultSet resultSet, int arg1)
							throws SQLException {
						if (resultSet.getInt("count") > 0) {
							errors.rejectValue("emailAddress",
									"error.emailAddress",
									"StudentRegistration with this Email is exist.");
						}
						return null;
					}
				});
				
				
			}
		}

	}
}