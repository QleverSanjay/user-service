package com.isg.service.user.validator;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ValidationError {

	
	private List<String> errors;
	private String title;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "ValidationError [errors=" + errors + "]";
	}

	

}
