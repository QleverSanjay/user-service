package com.isg.service.user.client.notification;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gcm.server.Sender;
import com.notnoop.apns.ApnsService;

public class NotificationImpl implements NotificationClient {

	private static final Logger log = Logger.getLogger(NotificationImpl.class);
	static final String MESSAGE_KEY = "message";

	AmqpTemplate template;

	ApnsService service;

	Sender sender;

	public NotificationImpl() {

	}

	public void setTemplate(AmqpTemplate template) {
		this.template = template;
	}

	@Override
	public void sendEmail(String receiverEmail, String templateCode, Map<String, Object> emailConent) {

		Notification email = new Notification();
		email.setType("EMAIL");
		email.setTo(receiverEmail);
		email.setTemplateCode(templateCode);
		email.setContent(emailConent);
		log.debug("SENDING EMAIL-------------");
		try {
			String jsonSend = new ObjectMapper().writeValueAsString(email);
			log.debug(jsonSend);
			sendMessage(jsonSend);
		} catch (Exception e) {
			log.error("", e);
		}
	}

	@Override
	public void sendSMS(String primaryContact, String templateCode, Map<String, Object> smsContent) {

		Sms sms = new Sms();
		sms.setType("SMS");
		sms.setContactNumber(primaryContact);
		sms.setTemplateCode(templateCode);
		sms.setSmsContent(smsContent);
		log.debug("SENDING SMS-------------");
		try {
			String jsonSend = new ObjectMapper().writeValueAsString(sms);
			sendMessage(jsonSend);
		} catch (Exception e) {
			log.error("", e);
		}
	}

	@Override
	public void sendMessage(String jsonMessage) {
		log.debug("JSOn-------------" + jsonMessage);
		template.convertAndSend(jsonMessage);
	}

}