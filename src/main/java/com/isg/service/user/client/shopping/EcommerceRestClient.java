package com.isg.service.user.client.shopping;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isg.service.user.client.RestClient;
import com.isg.service.user.client.ServiceConfiguration;

@SpringBootApplication
public class EcommerceRestClient extends RestClient {

	private static final Logger log = Logger.getLogger(EcommerceRestClient.class);

	private static final String ADD_ON_COOKIES = " app_code=defand1; screen_size=1600x381;";

	private static final String COOKIE_DEFAULT_PATH = "path=/shopping;";

	// private Date cookieExpiresOn;

	public ResponseEntity<String> executeRequest(
					MultiValueMap<String, String> valueMap,
					ServiceConfiguration serviceConfiguration, boolean loginRequired)
					throws JsonProcessingException {
		return executeRequest(valueMap, null, serviceConfiguration, null,
						loginRequired);
	}

	public ResponseEntity<String> executeRequest(
					MultiValueMap<String, String> valueMap,
					String cookieText,
					ServiceConfiguration serviceConfiguration,
					Object[] urlParameters, boolean loginRequired)
					throws JsonProcessingException {

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = null;
		headers = new HttpHeaders();

		if (cookieText == null) {
			headers.add("Cookie", ADD_ON_COOKIES);
		} else {
			headers.add("Cookie", ADD_ON_COOKIES + cookieText);
		}

		/*
		 * if (loginRequired) { headers = new HttpHeaders();
		 * headers.add("Cookie", ADD_ON_COOKIES); } else { headers =
		 * getHeader(valueMap); }
		 */

		if (valueMap == null) {
			valueMap = new LinkedMultiValueMap<String, String>();
		}
		log.debug("Shopping rest client request - parameters ----->>>> " + valueMap);
		HttpEntity<?> entity = new HttpEntity<>(valueMap, headers);

		String url = (urlParameters != null) ? getUrl(getThreadSafeUrl(serviceConfiguration.getUrl()), urlParameters)
						: serviceConfiguration.getUrl();
		System.out.println("url:" + url);
		ResponseEntity<String> response = restTemplate.exchange(url,
						serviceConfiguration.getMethod(), entity, String.class,
						valueMap);
		log.debug("Shoppping response-Body----->>>>> " + response.getBody());
		return response;

	}

	public ResponseEntity<String> executeRequest(
					ServiceConfiguration serviceConfiguration)
					throws JsonProcessingException {
		return executeRequest(null, null, serviceConfiguration, null, false);

	}

	private String getUrl(String url, Object[] urlParameters) {
		MessageFormat mf = new MessageFormat(url);
		return mf.format(urlParameters);
	}

	private String getThreadSafeUrl(String nonThreadSafeUrl) {
		return new String(nonThreadSafeUrl);
	}

	public String getCookies(MultiValueMap<String, String> valueMap)
					throws JsonProcessingException {
		ResponseEntity<String> response = executeRequest(valueMap, ShoppingUrlConfiguration.CREATE_SESSION_CONFIG, true);
		return getFrontendCookieText(response.getHeaders().toString());
	}

	public String getCookies(ResponseEntity<String> response) throws JsonProcessingException {
		return getFrontendCookieText(response.getHeaders().toString());
	}

	public String getFrontendCookieText(String cookieText) {
		String firstStr = cookieText.substring(cookieText
						.lastIndexOf("frontend"));
		String frontendText = firstStr.substring(0, firstStr.indexOf(";") + 1);
		return frontendText;
	}

	public Date getCookieExpireTime(String cookieText) {
		Date date = null;
		try {
			if (cookieText.lastIndexOf("expires") > 0) {
				String firstStr = cookieText.substring(cookieText
								.lastIndexOf("expires"));
				String frontendText = firstStr.substring(firstStr.indexOf("=") + 1,
								firstStr.indexOf(";"));

				String string = frontendText;
				DateFormat format = new SimpleDateFormat(
								"EEE, dd-MMM-yyyy HH:mm:ss z", Locale.ENGLISH);

				date = format.parse(string);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		EcommerceRestClient client = new EcommerceRestClient();

		MultiValueMap<String, String> valueMap = new
						LinkedMultiValueMap<String, String>();

		valueMap.add("username", "vikas@test.com");
		valueMap.add("password", "Vikas@123");

		String cookieText = client.getCookies(valueMap);
		/*
		 * ResponseEntity<String> response = client.executeRequest(valueMap,
		 * ShoppingUrlConfiguration.CREATE_SESSION_CONFIG, true);
		 * SessionResponse sessionResponse = new SessionResponse();
		 * sessionResponse.setCookies(client.getCookies(response));
		 */

		// System.out.println(cookieText);

		Object[] obj = new Object[] { "74" };
		ResponseEntity<String> response = client.executeRequest(null, cookieText, ShoppingUrlConfiguration.GET_ORDER_DETAIL_CONFIG, obj, true);
		// System.out.println(response.getBody());

		//
		// MultiValueMap<String, String> valueMap = new
		// LinkedMultiValueMap<String, String>();
		//
		// valueMap.add("birthdate", "10/10/2001");
		// valueMap.add("firstname", "vishal1");
		// valueMap.add("lastname", ".");
		// valueMap.add("email", "vishal5@v2stech.com");
		// valueMap.add("password", "vishal");
		// valueMap.add("confirmation", "vishal");
		// valueMap.add("is_subscribed", "1");
		// valueMap.add("mobile", "10000000000");
		//
		// ResponseEntity<String> response = client.executeRequest(valueMap,
		// EcommerceUrlConfiguration.CREATE_CUSTOMER_CONFIG, true);
		//
		// XmlMapper xmlMapper = new XmlMapper();
		// CreateCustomerResponse createCustomerResponse =
		// xmlMapper.readValue(response.getBody(),
		// CreateCustomerResponse.class);
		// System.out.println("customerId :::" +
		// createCustomerResponse.getCustomer_id());

	}
}