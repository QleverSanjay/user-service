package com.isg.service.user.client.chat.response.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UnreadMessageCountResponse implements Response {

	@JsonProperty("total")
	int total;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "UnreadMessageCountResponse [total=" + total + "]";
	}

}
