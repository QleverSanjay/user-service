package com.isg.service.user.client.shopping.response.session;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.isg.service.user.client.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionResponse implements Response {
	String cookies;

	// Date cookieExpiresOn;

	public String getCookies() {
		return cookies;
	}

	public void setCookies(String cookies) {
		this.cookies = cookies;
	}
}
