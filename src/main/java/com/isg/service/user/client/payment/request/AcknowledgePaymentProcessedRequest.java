package com.isg.service.user.client.payment.request;

import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;
import com.isg.service.user.model.PaymentDetailTaxComponent;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AcknowledgePaymentProcessedRequest implements Request {

	@JsonProperty(value = "qfix_reference_number")
	@Size(max = 20)
	String qfixReferenceNumber;

	@JsonProperty(value = "amount")
	@NotNull
	@Digits(integer = 10, fraction = 2)
	float amount;

	@JsonProperty(value = "internet_handling_charges")
	@Digits(integer = 10, fraction = 2)
	@NotNull
	float internetHandlingCharges;

	@JsonProperty(value = "shipping_charges")
	@Digits(integer = 10, fraction = 2)
	@NotNull
	float shippingCharges;

	@JsonProperty(value = "total_amount")
	@NotNull
	@Digits(integer = 20, fraction = 2)
	float totalAmount;

	@JsonProperty(value = "payment_gateway_configuration_id")
	@Digits(integer = 11, fraction = 0)
	@NotNull
	Integer paymentGatewayConfigurationId;

	@JsonProperty(value = "customer_name")
	@Size(max = 100)
	@NotNull
	String customerName;

	@JsonProperty(value = "customer_email")
	@Size(max = 100)
	@NotNull
	String customerEmail;

	@JsonProperty(value = "channel")
	@Size(max = 15)
	String channel;

	@JsonProperty(value = "payment_tax_type")
	@Size(max = 15)
	String paymentTaxType;

	@JsonProperty(value = "payment_option_code")
	@Digits(integer = 11, fraction = 0)
	@NotNull
	Integer paymentOptionCode;

	@JsonProperty(value = "payment_tax_details")
	List<PaymentDetailTaxComponent> paymentDetailTaxComponent;

	public String getQfixReferenceNumber() {
		return qfixReferenceNumber;
	}

	public void setQfixReferenceNumber(String qfixReferenceNumber) {
		this.qfixReferenceNumber = qfixReferenceNumber;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public float getInternetHandlingCharges() {
		return internetHandlingCharges;
	}

	public void setInternetHandlingCharges(float internetHandlingCharges) {
		this.internetHandlingCharges = internetHandlingCharges;
	}

	public float getShippingCharges() {
		return shippingCharges;
	}

	public void setShippingCharges(float shippingCharges) {
		this.shippingCharges = shippingCharges;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getPaymentGatewayConfigurationId() {
		return paymentGatewayConfigurationId;
	}

	public void setPaymentGatewayConfigurationId(Integer paymentGatewayConfigurationId) {
		this.paymentGatewayConfigurationId = paymentGatewayConfigurationId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getPaymentTaxType() {
		return paymentTaxType;
	}

	public void setPaymentTaxType(String paymentTaxType) {
		this.paymentTaxType = paymentTaxType;
	}

	public Integer getPaymentOptionCode() {
		return paymentOptionCode;
	}

	public void setPaymentOptionCode(Integer paymentOptionCode) {
		this.paymentOptionCode = paymentOptionCode;
	}

	public List<PaymentDetailTaxComponent> getPaymentDetailTaxComponent() {
		return paymentDetailTaxComponent;
	}

	public void setPaymentDetailTaxComponent(List<PaymentDetailTaxComponent> paymentDetailTaxComponent) {
		this.paymentDetailTaxComponent = paymentDetailTaxComponent;
	}

	@Override
	public String toString() {
		return "AcknowledgePaymentProcessedRequest [qfixReferenceNumber=" + qfixReferenceNumber + ", amount=" + amount + ", internetHandlingCharges="
						+ internetHandlingCharges + ", shippingCharges=" + shippingCharges + ", paymentGatewayConfigurationId=" + paymentGatewayConfigurationId
						+ ", customerName=" + customerName + ", customerEmail=" + customerEmail + ", channel=" + channel + ", paymentTaxType=" + paymentTaxType
						+ ", paymentDetailTaxComponent=" + paymentDetailTaxComponent + "]";
	}

}
