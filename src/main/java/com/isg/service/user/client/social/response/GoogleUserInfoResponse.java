package com.isg.service.user.client.social.response;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleUserInfoResponse extends UserInfoResponse {

	String firstName;

	String surname;

	@Override
	@JsonIgnore
	public String getFirstName() {
		return firstName;
	}

	@Override
	@NotNull
	@JsonProperty(value = "given_name")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	@JsonIgnore
	public String getSurname() {
		return surname;
	}

	@Override
	@NotNull
	@JsonProperty(value = "family_name")
	public void setSurname(String surname) {
		this.surname = surname;
	}

}
