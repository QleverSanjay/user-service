package com.isg.service.user.client.forum;

import org.springframework.http.HttpMethod;

import com.isg.service.user.client.ServiceConfiguration;

// TODO : Read from a XML configuration file.
public class ForumUrlConfiguration {

	// live url www.eduqfix.com
	public static final ServiceConfiguration CREATE_USER_CONFIG = new ServiceConfiguration("http://localhost/collab/api/users.php?action=add",
					HttpMethod.POST);

	public static final ServiceConfiguration LINK_USER_TO_FORUM_CONFIG = new ServiceConfiguration(
					"http://localhost/collab/api/users.php?action=linkuser",
					HttpMethod.POST);

}
