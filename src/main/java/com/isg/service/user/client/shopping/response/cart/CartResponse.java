package com.isg.service.user.client.shopping.response.cart;

import com.isg.service.user.client.Response;

public class CartResponse implements Response {

	String status;
	Integer customer_id;
	float amount;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "CartResponse [status=" + status + ", customer_id=" + customer_id + ", amount=" + amount + "]";
	}

}
