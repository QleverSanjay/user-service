package com.isg.service.user.client.social;

import org.springframework.http.HttpMethod;

import com.isg.service.user.client.ServiceConfiguration;

// TODO : Read from a XML configuration file.
public class FacebookUrlConfiguration {

	public static final ServiceConfiguration TOKEN_INFO = new ServiceConfiguration(
					"https://graph.facebook.com/me?fields=email,name,id,gender,first_name,last_name&access_token={access_token}", HttpMethod.GET);
	// TODO :: Couldn't figure out the revoke api url so far from facebook api
	// documentation
	/*
	 * public static final ServiceConfiguration REVOKE_TOKEN = new
	 * ServiceConfiguration(
	 * "https://accounts.google.com/o/oauth2/revoke?token={access_token}",
	 * HttpMethod.GET);
	 */

}
