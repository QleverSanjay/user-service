package com.isg.service.user.client.chat.request.group;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreatePrivateGroup implements Request {

	@JsonProperty(value = "owner_username")
	@NotNull
	String groupOwnerUsername;

	@JsonProperty(value = "owner_password")
	@NotNull
	String groupOwnerPassword;

	@JsonProperty(value = "occupants_ids")
	@NotNull
	String occupantsUserProfileIds;

	public String getGroupOwnerUsername() {
		return groupOwnerUsername;
	}

	public void setGroupOwnerUsername(String groupOwnerUsername) {
		this.groupOwnerUsername = groupOwnerUsername;
	}

	public String getGroupOwnerPassword() {
		return groupOwnerPassword;
	}

	public void setGroupOwnerPassword(String groupOwnerPassword) {
		this.groupOwnerPassword = groupOwnerPassword;
	}

	public String getOccupantsUserProfileIds() {
		return occupantsUserProfileIds;
	}

	public void setOccupantsUserProfileIds(String occupantsUserProfileIds) {
		this.occupantsUserProfileIds = occupantsUserProfileIds;
	}

	@Override
	public String toString() {
		return "CreatePrivateGroup [groupOwnerUsername=" + groupOwnerUsername + ", occupantsUserProfileIds=" + occupantsUserProfileIds + "]";
	}

}
