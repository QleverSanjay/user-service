package com.isg.service.user.client.forum.request.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateUserRequest implements Request {

	@JsonProperty(value = "username")
	String username;

	@JsonProperty(value = "user_password")
	String password;

	@JsonProperty(value = "user_email")
	String email;

	@JsonProperty(value = "school_id")
	int schoolId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

}
