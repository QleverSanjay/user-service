package com.isg.service.user.client.chat;

import org.springframework.http.HttpMethod;

import com.isg.service.user.client.ServiceConfiguration;

// TODO : Read from a XML configuration file.
public class ChatUrlConfiguration {

	// live url 10.80.76.13:8080
	public static final ServiceConfiguration CREATE_SESSION_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/chat/login",
					HttpMethod.POST);

	public static final ServiceConfiguration CREATE_USER_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/chat/users", HttpMethod.POST);

	public static final ServiceConfiguration CREATE_GROUP_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/groups/public",
					HttpMethod.POST);

	public static final ServiceConfiguration UPDATE_GROUP_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/groups/update",
					HttpMethod.PUT);

	public static final ServiceConfiguration CHAT_UNREAD_COUNT_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/chat/message/unread",
					HttpMethod.POST);

	public static final ServiceConfiguration CREATE_PRIVATE_GROUP_CONFIG = new ServiceConfiguration("http://localhost:8080/chat-service/groups/private",
					HttpMethod.POST);

	public static final ServiceConfiguration DELETE_DIALOG_CONFIG = new ServiceConfiguration(
					"http://localhost:8080/chat-service/groups?dialog_id={dialog_id}&owner_username={owner_username}&owner_password={owner_password}",
					HttpMethod.DELETE);
}
