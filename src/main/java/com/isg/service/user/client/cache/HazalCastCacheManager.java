package com.isg.service.user.client.cache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

/**
 * This class shows the way you must declare the attributes to retrieve the
 * hazelcast objects from spring context.
 * <p/>
 * Note: for collections, you must use the @Resource annotation instead of the
 * 
 * @Autowired.
 */
@Repository
public class HazalCastCacheManager implements CacheManager {

	private static final Logger log = Logger.getLogger(HazalCastCacheManager.class);

//	@Resource(name = "map")
//	private IMap<Object, Object> hzMap;

	@Autowired
	private HazelcastInstance instance;

	@Override
	public String get(String key) {
		IMap<String, String> map = instance.getMap("map");
		log.debug("cache map size :::" + map.size());
		log.debug("cache map keys :::" + map.keySet());
		return map.get(key);
	}

	@Override
	public void store(String key, String value) {
		IMap<String, String> map = instance.getMap("map");
		try {
			Future future = map.putAsync(key, value, 30, TimeUnit.DAYS);
			Object oldValue = future.get(40, TimeUnit.MILLISECONDS);
		} catch (TimeoutException | InterruptedException | ExecutionException t) { // time
																					// wasn't
																					// enough
		}
		// map.put(key, value);
	}

	@Override
	public void remove(String key) {
		IMap<String, String> map = instance.getMap("map");

		map.evict(key);

	}

//	@PostConstruct
//	public void theProof() {
//		Assert.notNull(this.hzMap);
//		/*Assert.notNull(this.hzMultiMap);
//		Assert.notNull(this.hzQueue);
//		Assert.notNull(this.hzTopic);
//		Assert.notNull(this.hzSet);
//		Assert.notNull(this.hzList);
//		Assert.notNull(this.hzExecutorService);
//		Assert.notNull(this.hzIdGenerator);
//		Assert.notNull(this.hzAtomicLong);
//		Assert.notNull(this.hzAtomicReference);
//		Assert.notNull(this.hzCountDownLatch);
//		Assert.notNull(this.hzSemaphore);
//		Assert.notNull(this.hzLock);
//		Assert.notNull(this.hzMap);*/
//
//		System.out.println("hzMap = " + this.hzMap.getClass());
//		/*System.out.println("hzMultiMap = " + this.hzMultiMap.getClass());
//		System.out.println("hzQueue = " + this.hzQueue.getClass());
//		System.out.println("hzTopic = " + this.hzTopic.getClass());
//		System.out.println("hzSet = " + this.hzSet.getClass());
//		System.out.println("hzList = " + this.hzList.getClass());
//		System.out.println("hzExecutorService = " + this.hzExecutorService.getClass());
//		System.out.println("hzIdGenerator = " + this.hzIdGenerator.getClass());
//		System.out.println("hzAtomicLong = " + this.hzAtomicLong.getClass());
//		System.out.println("hzAtomicReference = " + this.hzAtomicReference.getClass());
//		System.out.println("hzCountDownLatch = " + this.hzCountDownLatch.getClass());
//		System.out.println("hzSemaphore = " + this.hzSemaphore.getClass());
//		System.out.println("hzLock = " + this.hzLock.getClass());*/
//		System.out.println("hzMap = " + this.hzMap.getClass());
//	}
}
