package com.isg.service.user.client.cache;

import org.springframework.stereotype.Repository;

@Repository
public interface CacheManager {

	public void store(String token, String value);

	public String get(String key);

	void remove(String key);
}