package com.isg.service.user.client.shopping;

import org.springframework.http.HttpMethod;

import com.isg.service.user.client.ServiceConfiguration;

// TODO : Read from a XML configuration file.
public class ShoppingUrlConfiguration {
	// live url www.eduqfix.com

	public static final ServiceConfiguration GET_SERVER_CONFIG = new ServiceConfiguration(
					"http://localhost/shopping/xmlconnect/configuration/index/app_code/defand1/screen_size/1600x381/", HttpMethod.GET);

	public static final ServiceConfiguration CREATE_SESSION_CONFIG = new ServiceConfiguration("http://localhost/shopping/xmlconnect/Customer/login",
					HttpMethod.POST);

	public static final ServiceConfiguration CREATE_CUSTOMER_CONFIG = new ServiceConfiguration("http://localhost/shopping/xmlconnect/Customer/save",
					HttpMethod.POST);

	public static final ServiceConfiguration ACKNOWLEDGE_PAYMENT_CONFIG = new ServiceConfiguration(
					"http://localhost/shopping/xmlconnect/order/updateStatus",
					HttpMethod.POST);

	public static final ServiceConfiguration GET_ORDER_DETAIL_CONFIG = new ServiceConfiguration(
					"http://localhost/shopping/xmlconnect/Customer/orderDetails/?order_id={0}",
					HttpMethod.POST);

	public static final ServiceConfiguration LINK_ACCOUNTS_CONFIG = new ServiceConfiguration(
					"http://localhost/shopping/xmlconnect/Customer/link",
					HttpMethod.POST);

}
