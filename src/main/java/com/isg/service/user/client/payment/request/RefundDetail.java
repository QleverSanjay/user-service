package com.isg.service.user.client.payment.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

public class RefundDetail implements Request{

	@JsonProperty("refund_details")
	List<RefundStatus> refundDetails;
	@JsonProperty("qfix_reference_number")
	String qfixReferenceNumber;

	public List<RefundStatus> getRefundDetails() {
		return refundDetails;
	}

	public void setRefundDetails(List<RefundStatus> refundDetails) {
		this.refundDetails = refundDetails;
	}

	public String getQfixReferenceNumber() {
		return qfixReferenceNumber;
	}

	public void setQfixReferenceNumber(String qfixReferenceNumber) {
		this.qfixReferenceNumber = qfixReferenceNumber;
	}

}
