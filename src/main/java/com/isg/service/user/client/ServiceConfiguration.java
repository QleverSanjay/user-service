package com.isg.service.user.client;

import java.text.MessageFormat;

import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;

public class ServiceConfiguration {

	private static final Logger log = Logger.getLogger(ServiceConfiguration.class);

	private String url;

	private Object[] urlParameters;

	private boolean replaceParameters = false;

	private final HttpMethod method;

	public ServiceConfiguration(String url, HttpMethod method) {
		this.url = url;
		this.method = method;
	}

	public ServiceConfiguration(String url, HttpMethod method, boolean replaceParameters) {
		this(url, method);
		this.replaceParameters = replaceParameters;
	}

	public String getUrl() {
		if (replaceParameters) {
			MessageFormat mf = new MessageFormat(url);
			url = mf.format(urlParameters);
		}

		log.info("service configuration url========" + url);
		return url;
	}

	public HttpMethod getMethod() {
		return method;
	}

	public void setUrlParameters(Object[] urlParameters) {
		this.urlParameters = urlParameters;
	}

}
