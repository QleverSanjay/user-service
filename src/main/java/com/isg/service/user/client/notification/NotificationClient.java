package com.isg.service.user.client.notification;

import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository
public interface NotificationClient {

	//void sendNotication(String title, String titleMessage, String detailJson, Map<String, List<String>> gcmIdsMap);

	void sendEmail(String receiverEmail, String templateCode, Map<String, Object> emailConent);

	void sendSMS(String primaryContact, String templateCode, Map<String, Object> smsContent);

	void sendMessage(String jsonMessage);

}