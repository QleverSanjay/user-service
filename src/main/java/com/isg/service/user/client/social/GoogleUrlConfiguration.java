package com.isg.service.user.client.social;

import org.springframework.http.HttpMethod;

import com.isg.service.user.client.ServiceConfiguration;

// TODO : Read from a XML configuration file.
public class GoogleUrlConfiguration {

	public static final ServiceConfiguration TOKEN_INFO = new ServiceConfiguration(
					"https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token={access_token}", HttpMethod.GET);

	public static final ServiceConfiguration REVOKE_TOKEN = new ServiceConfiguration(
					"https://accounts.google.com/o/oauth2/revoke?token={access_token}", HttpMethod.GET);

}
