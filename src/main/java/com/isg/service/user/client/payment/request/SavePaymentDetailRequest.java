package com.isg.service.user.client.payment.request;

import java.util.Map;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SavePaymentDetailRequest implements Request {

	@JsonProperty(value = "qfix_reference_number")
	@Size(max = 20)
	String qfixReferenceNumber;

	@JsonProperty(value = "amount")
	@Digits(integer = 10, fraction = 2)
	float amount;

	@JsonProperty(value = "internet_handling_charges")
	@Digits(integer = 10, fraction = 2)
	float internetHandlingCharges;

	@JsonProperty(value = "shipping_charges")
	@Digits(integer = 10, fraction = 2)
	float shippingCharges;

	@JsonProperty(value = "payment_gateway_configuration_id")
	@Digits(integer = 11, fraction = 0)
	Integer paymentGatewayConfigurationId;

	@JsonProperty(value = "customer_name")
	@Size(max = 100)
	String customerName;

	@JsonProperty(value = "customer_email")
	@Size(max = 100)
	String customerEmail;

	@JsonProperty(value = "customer_phone")
	@Size(max = 15)
	String customerPhone;

	@JsonProperty(value = "customer_address")
	@Size(max = 200)
	String customerAddress;

	@JsonProperty(value = "customer_city")
	@Size(max = 50)
	String customerCity;

	@JsonProperty(value = "customer_state")
	@Size(max = 50)
	String customerState;

	@JsonProperty(value = "customer_pincode")
	@Size(max = 10)
	String customerPincode;

	@JsonProperty(value = "customer_country")
	@Size(max = 3)
	String customerCountry;

	@JsonProperty(value = "access_code")
	@Size(max = 36)
	String accessCode;

	@JsonProperty(value = "channel")
	@Size(max = 15)
	String channel;

	@JsonProperty(value = "payment_tax_type")
	@Size(max = 15)
	String paymentTaxType;

	@JsonProperty(value = "payment_for_description")
	@Size(max = 1000)
	String paymentForDescription;

	@JsonProperty(value = "payment_for_name")
	@Size(max = 1000)
	String paymentForName;

	@JsonProperty(value = "split_payment_detail")
	Map<Integer, String> splitPaymentDetail;

	@JsonProperty(value = "offline_payments_enabled")
	boolean offlinePaymentsEnabled;

	@JsonProperty("currency_icon_class")
	String currencyIconClass;

	@JsonProperty("currency_code")
	String currencyCode;

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyIconClass() {
		return currencyIconClass;
	}

	public void setCurrencyIconClass(String currencyIconClass) {
		this.currencyIconClass = currencyIconClass;
	}

	public String getPaymentForName() {
		return paymentForName;
	}

	public void setPaymentForName(String paymentForName) {
		this.paymentForName = paymentForName;
	}

	public boolean isOfflinePaymentsEnabled() {
		return offlinePaymentsEnabled;
	}

	public void setOfflinePaymentsEnabled(boolean offlinePaymentsEnabled) {
		this.offlinePaymentsEnabled = offlinePaymentsEnabled;
	}

	public String getQfixReferenceNumber() {
		return qfixReferenceNumber;
	}

	public void setQfixReferenceNumber(String qfixReferenceNumber) {
		this.qfixReferenceNumber = qfixReferenceNumber;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public float getInternetHandlingCharges() {
		return internetHandlingCharges;
	}

	public void setInternetHandlingCharges(float internetHandlingCharges) {
		this.internetHandlingCharges = internetHandlingCharges;
	}

	public float getShippingCharges() {
		return shippingCharges;
	}

	public void setShippingCharges(float shippingCharges) {
		this.shippingCharges = shippingCharges;
	}

	public Integer getPaymentGatewayConfigurationId() {
		return paymentGatewayConfigurationId;
	}

	public void setPaymentGatewayConfigurationId(Integer paymentGatewayConfigurationId) {
		this.paymentGatewayConfigurationId = paymentGatewayConfigurationId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getPaymentTaxType() {
		return paymentTaxType;
	}

	public void setPaymentTaxType(String paymentTaxType) {
		this.paymentTaxType = paymentTaxType;
	}

	public String getPaymentForDescription() {
		return paymentForDescription;
	}

	public void setPaymentForDescription(String paymentForDescription) {
		this.paymentForDescription = paymentForDescription;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getCustomerPincode() {
		return customerPincode;
	}

	public void setCustomerPincode(String customerPincode) {
		this.customerPincode = customerPincode;
	}

	public String getCustomerCountry() {
		return customerCountry;
	}

	public void setCustomerCountry(String customerCountry) {
		this.customerCountry = customerCountry;
	}

	public Map<Integer, String> getSplitPaymentDetail() {
		return splitPaymentDetail;
	}

	public void setSplitPaymentDetail(Map<Integer, String> splitPaymentDetail) {
		this.splitPaymentDetail = splitPaymentDetail;
	}

	@Override
	public String toString() {
		return "";
	}

}
