package com.isg.service.user.client.social.response;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Response;
import com.isg.service.user.util.Constant.SOCIAL_PROFILE_TYPE;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfoResponse implements Response {
	@NotNull
	String id;
	@NotNull
	String name;
	@NotNull
	String email;

	String firstName;

	String surname;

	String gender;

	SOCIAL_PROFILE_TYPE provider;

	@JsonIgnore
	public String getId() {
		return id;
	}

	@JsonProperty(value = "id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonIgnore
	public String getName() {
		return name;
	}

	@JsonProperty(value = "name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public String getEmail() {
		return email;
	}

	@JsonProperty(value = "email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonIgnore
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty(value = "given_name")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonIgnore
	public String getSurname() {
		return surname;
	}

	@JsonProperty(value = "family_name")
	public void setSurname(String surname) {
		this.surname = surname;
	}

	@JsonIgnore
	public String getGender() {
		return gender;
	}

	@JsonProperty(value = "gender")
	public void setGender(String gender) {
		this.gender = gender;
	}

	public SOCIAL_PROFILE_TYPE getProvider() {
		return provider;
	}

	public void setProvider(SOCIAL_PROFILE_TYPE provider) {
		this.provider = provider;
	}

}
