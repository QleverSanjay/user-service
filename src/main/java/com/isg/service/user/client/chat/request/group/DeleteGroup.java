package com.isg.service.user.client.chat.request.group;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeleteGroup implements Request {

	@JsonProperty(value = "owner_username")
	@NotNull
	String groupOwnerUsername;

	@JsonProperty(value = "owner_password")
	@NotNull
	String groupOwnerPassword;

	@JsonProperty(value = "dialog_id")
	@NotNull
	String dialogId;

	public String getGroupOwnerUsername() {
		return groupOwnerUsername;
	}

	public void setGroupOwnerUsername(String groupOwnerUsername) {
		this.groupOwnerUsername = groupOwnerUsername;
	}

	public String getGroupOwnerPassword() {
		return groupOwnerPassword;
	}

	public void setGroupOwnerPassword(String groupOwnerPassword) {
		this.groupOwnerPassword = groupOwnerPassword;
	}

	public String getDialogId() {
		return dialogId;
	}

	public void setDialogId(String dialogId) {
		this.dialogId = dialogId;
	}

}
