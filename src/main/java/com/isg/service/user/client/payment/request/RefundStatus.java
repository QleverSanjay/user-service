package com.isg.service.user.client.payment.request;

public class RefundStatus {

	String paymentGatewayRefundId;
	String refundAmount;
	String refundStatus;
	String refundDate;

	public String getPaymentGatewayRefundId() {
		return paymentGatewayRefundId;
	}

	public void setPaymentGatewayRefundId(String paymentGatewayRefundId) {
		this.paymentGatewayRefundId = paymentGatewayRefundId;
	}

	public String getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(String refundDate) {
		this.refundDate = refundDate;
	}

}
