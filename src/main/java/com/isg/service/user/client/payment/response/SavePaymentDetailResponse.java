package com.isg.service.user.client.payment.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SavePaymentDetailResponse implements Response {

	@JsonProperty("payment_detail_id")
	int id;

	@JsonProperty("transaction_refund_id")
	String transactionRefundId;

	@JsonProperty("access_code")
	String accessCode;

	@JsonProperty("pg_transaction_id")
	String pgTransactionId;

	public String getPgTransactionId() {
		return pgTransactionId;
	}

	public void setPgTransactionId(String pgTransactionId) {
		this.pgTransactionId = pgTransactionId;
	}

	public String getTransactionRefundId() {
		return transactionRefundId;
	}

	public void setTransactionRefundId(String transactionRefundId) {
		this.transactionRefundId = transactionRefundId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	@Override
	public String toString() {
		return "";
	}

}
