package com.isg.service.user.client.shopping.response.session;

import com.isg.service.user.client.Response;

public class CreateCustomerResponse implements Response {

	String status;
	Integer customer_id;
	String text;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Integer customer_id) {
		this.customer_id = customer_id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "CreateCustomerResponse [status=" + status + ", customer_id=" + customer_id + "]";
	}

}
