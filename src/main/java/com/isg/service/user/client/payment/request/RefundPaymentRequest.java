package com.isg.service.user.client.payment.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RefundPaymentRequest implements Request {

	@JsonProperty("qfix_reference_number")
	String qfixReferenceNumber;

	@JsonProperty("amount")
	double amount;

	@JsonProperty("currency")
	String currency;

	public String getQfixReferenceNumber() {
		return qfixReferenceNumber;
	}

	public void setQfixReferenceNumber(String qfixReferenceNumber) {
		this.qfixReferenceNumber = qfixReferenceNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}