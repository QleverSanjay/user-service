package com.isg.service.user.client.notification;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Notification {
	private Integer id;
	private String to;
	private String type;
	private Map<String, Object> content;
	private Map<String, Integer> userIds;
	private List<String> attachments;
	Map<String, Object> subjectContent;

	@JsonProperty("sendEmail")
	private boolean sendEmail;

	@JsonProperty("sendSMS")
	private boolean sendSMS;

	@JsonProperty("sendPushMessage")
	private boolean sendPushMessage;

	private String templateCode;
	private boolean isBatchEnabled;

	private Map<String, Object> additionalData;

	public Map<String, Object> getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(Map<String, Object> additionalData) {
		this.additionalData = additionalData;
	}

	public boolean isBatchEnabled() {
		return isBatchEnabled;
	}

	public void setBatchEnabled(boolean isBatchEnabled) {
		this.isBatchEnabled = isBatchEnabled;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, Object> getContent() {
		return content;
	}

	public void setContent(Map<String, Object> content) {
		this.content = content;
	}

	public Map<String, Integer> getUserIds() {
		return userIds;
	}

	public void setUserIds(Map<String, Integer> userIds) {
		this.userIds = userIds;
	}

	public boolean sendEmail() {
		return sendEmail;
	}

	public void sendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public boolean sendSMS() {
		return sendSMS;
	}

	public void sendSMS(boolean sendSMS) {
		this.sendSMS = sendSMS;
	}

	public boolean sendPushMessage() {
		return sendPushMessage;
	}

	public void sendPushMessage(boolean sendPushMessage) {
		this.sendPushMessage = sendPushMessage;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}

	public Map<String, Object> getSubjectContent() {
		return subjectContent;
	}

	public void setSubjectContent(Map<String, Object> subjectContent) {
		this.subjectContent = subjectContent;
	}

}
