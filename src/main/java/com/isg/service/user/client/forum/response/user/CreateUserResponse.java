package com.isg.service.user.client.forum.response.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateUserResponse implements Response {
	@JsonProperty("forum_profile_id")
	String forumProfileId;

	public String getForumProfileId() {
		return forumProfileId;
	}

	public void setForumProfileId(String forumProfileId) {
		this.forumProfileId = forumProfileId;
	}

}
