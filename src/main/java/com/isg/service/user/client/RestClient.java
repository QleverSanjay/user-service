package com.isg.service.user.client;

import java.text.MessageFormat;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class RestClient {

	private static final Logger log = Logger.getLogger(RestClient.class);

	public ResponseEntity<String> executeRequest(Request request,
					ServiceConfiguration serviceConfiguration)
					throws JsonProcessingException {
		return executeRequest(request, serviceConfiguration, null);
	}

	public ResponseEntity<String> executeRequest(Request request,
					ServiceConfiguration serviceConfiguration, Object[] urlParameters)
					throws JsonProcessingException {
		return executeRequest(request, serviceConfiguration, urlParameters, null);
	}

	public ResponseEntity<String> executeRequest(ServiceConfiguration serviceConfiguration, Object[] urlParameters, Map<String, String> header)
					throws JsonProcessingException {
		return executeRequest(null, serviceConfiguration, urlParameters, header);

	}

	public ResponseEntity<String> executeRequest(Request request,
					ServiceConfiguration serviceConfiguration, Object[] urlParameters, Map<String, String> header)
					throws JsonProcessingException {

		RestTemplate restTemplate = new RestTemplate();

		ObjectMapper mapper = new ObjectMapper();
		// Convert object to JSON string
		String jsonInString = "";
		if (request != null) {
			jsonInString = mapper.writeValueAsString(request);
		}
		//log.debug("RestClient request json body :: " + jsonInString);

		String url = serviceConfiguration.getUrl();
		HttpHeaders headers = getHeader();

		if (header != null && header.size() > 0) {

			for (Map.Entry<String, String> entry : header.entrySet()) {
				headers.set(entry.getKey(), entry.getValue());
			}
		}

		HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);
		if (urlParameters != null) {
			url = getUrl(getThreadSafeUrl(url), urlParameters);
		}

		return restTemplate.exchange(
						url,
						serviceConfiguration.getMethod(), entity, String.class);

	}

	public ResponseEntity<String> executeRequest(ServiceConfiguration serviceConfiguration, Map<String, String> urlParameters)
					throws JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(getHeader());
		return restTemplate.exchange(getThreadSafeUrl(serviceConfiguration.getUrl()), serviceConfiguration.getMethod(), entity, String.class, urlParameters);
	}

	public void executeDeleteRequest(ServiceConfiguration serviceConfiguration, Map<String, String> urlParameters)
					throws JsonProcessingException {

		RestTemplate restTemplate = new RestTemplate();

		restTemplate.delete(
						getThreadSafeUrl(serviceConfiguration.getUrl()),
						urlParameters);

	}

	private String getThreadSafeUrl(String nonThreadSafeUrl) {
		return new String(nonThreadSafeUrl);
	}

	private HttpHeaders getHeader() {

		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
		log.debug("request headers>>>" + headers.toString());
		return headers;
	}

	private String getUrl(String url, Object[] urlParameters) {
		MessageFormat mf = new MessageFormat(url);
		return mf.format(urlParameters);
	}

}