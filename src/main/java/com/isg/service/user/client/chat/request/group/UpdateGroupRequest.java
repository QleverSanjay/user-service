package com.isg.service.user.client.chat.request.group;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateGroupRequest implements Request {

	@JsonProperty(value = "group_id")
	@NotNull
	String groupId;

	@JsonProperty(value = "add_occupants_ids")
	String addOccupantsIds;

	@JsonProperty(value = "remove_occupants_ids")
	String removeOccupantsIds;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getAddOccupantsIds() {
		return addOccupantsIds;
	}

	public void setAddOccupantsIds(String addOccupantsIds) {
		this.addOccupantsIds = addOccupantsIds;
	}

	public String getRemoveOccupantsIds() {
		return removeOccupantsIds;
	}

	public void setRemoveOccupantsIds(String removeOccupantsIds) {
		this.removeOccupantsIds = removeOccupantsIds;
	}

	@Override
	public String toString() {
		return "UpdateGroup [groupId=" + groupId + ", addOccupantsIds="
						+ addOccupantsIds + ", removeOccupantsIds="
						+ removeOccupantsIds + "]";
	}

}
