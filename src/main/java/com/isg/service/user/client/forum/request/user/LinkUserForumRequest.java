package com.isg.service.user.client.forum.request.user;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkUserForumRequest implements Request {

	@JsonProperty(value = "group_id")
	String groupId;

	@JsonProperty(value = "user_id")
	Set<Integer> userIdList;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Set<Integer> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(Set<Integer> userIdList) {
		this.userIdList = userIdList;
	}

}
