package com.isg.service.user.client.payment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.isg.config.AppProperties;
import com.isg.config.AppProperties.PaymentUrlConfig;
import com.isg.service.user.client.ServiceConfiguration;

@Service
public class PaymentUrlConfiguration {

	@Autowired
	private AppProperties appProperties;
	
	public ServiceConfiguration createSessionConfig(){
		PaymentUrlConfig urlConfig = appProperties.getPaymentUrlConfig();
		System.out.println("Session URL >>>>>>>>>>>> " + urlConfig);
		return new ServiceConfiguration(
				urlConfig.getCreateSession(), HttpMethod.POST);
	}
	
	public ServiceConfiguration savePaymentDetailConfig(){
		PaymentUrlConfig urlConfig = appProperties.getPaymentUrlConfig();
		return new ServiceConfiguration(
				urlConfig.getSavePaymentDetail(), HttpMethod.POST);
	}
	
	public ServiceConfiguration getPaymentDetailConfig(){
		PaymentUrlConfig urlConfig = appProperties.getPaymentUrlConfig();
		return new ServiceConfiguration(
				urlConfig.getPaymentDetail(), HttpMethod.GET);
	}
	
	public ServiceConfiguration acknowledgePaymentProcessedByShopping(){
		PaymentUrlConfig urlConfig = appProperties.getPaymentUrlConfig();
		 return new ServiceConfiguration(
					urlConfig.getAcknowledgePayment(),
					HttpMethod.POST);
	}
	
	public ServiceConfiguration getBranchSpecificSchemeList(){
		PaymentUrlConfig urlConfig = appProperties.getPaymentUrlConfig();
		return new ServiceConfiguration(
				urlConfig.getBranchSpecificSchemeList(),
				HttpMethod.GET);
	}

	public ServiceConfiguration getGatewayConfigurationsBySchemeList(){
		PaymentUrlConfig urlConfig = appProperties.getPaymentUrlConfig();
		return new ServiceConfiguration(
				urlConfig.getGatewayConfigurationsBySchemeList(),
				HttpMethod.GET);
	}
	// live url 10.80.76.14:8081

	public ServiceConfiguration saveRefundFeePaymentConfig() {
		PaymentUrlConfig urlConfig = appProperties.getPaymentUrlConfig();
		return new ServiceConfiguration(
				urlConfig.getSaveRefundFeePayment(), HttpMethod.POST);
	}
}