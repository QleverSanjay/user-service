package com.isg.service.user.client.chat.response.group;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupResponse implements Response {

	@JsonProperty("add_request_status")
	public String occupantsAddRequestStatus = "NOT_APPLICABLE";

	@JsonProperty("remove_request_status")
	public String occupantsRemoveRequestStatus = "NOT_APPLICABLE";;

	public String getOccupantsAddRequestStatus() {
		return occupantsAddRequestStatus;
	}

	public void setOccupantsAddRequestStatus(String occupantsAddRequestStatus) {
		this.occupantsAddRequestStatus = occupantsAddRequestStatus;
	}

	public String getOccupantsRemoveRequestStatus() {
		return occupantsRemoveRequestStatus;
	}

	public void setOccupantsRemoveRequestStatus(String occupantsRemoveRequestStatus) {
		this.occupantsRemoveRequestStatus = occupantsRemoveRequestStatus;
	}

	@Override
	public String toString() {
		return "GroupResponse [occupantsAddRequestStatus="
						+ occupantsAddRequestStatus + ", occupantsRemoveRequestStatus="
						+ occupantsRemoveRequestStatus + "]";
	}

}
