package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.user.client.notification.NotificationClient;
import com.isg.service.user.dao.BranchDao;
import com.isg.service.user.dao.EventDao;
import com.isg.service.user.dao.FeeDetailsDao;
import com.isg.service.user.dao.FileDao;
import com.isg.service.user.dao.NotificationDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.AcademicYear;
import com.isg.service.user.model.BranchStudentParentDetail;
import com.isg.service.user.model.Contact;
import com.isg.service.user.model.Group;
import com.isg.service.user.model.NotificationCount;
import com.isg.service.user.model.NotificationRecipientDetail;
import com.isg.service.user.request.Notification;
import com.isg.service.user.request.SendNotfication;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.FileStorePathAssembler;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class NotificationDaoImpl extends NamedParameterJdbcDaoSupport implements NotificationDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private static final Logger log = Logger.getLogger(NotificationDaoImpl.class);

	@Autowired
	private FileDao fileDao;

	@Autowired
	@Lazy
	private UserDao userDao;

	@Autowired
	private BranchDao branchDao;

	@Autowired
	private FeeDetailsDao feeDetailsDAO;

	@Autowired
	private EventDao eventDAO;

	@Autowired
	private NotificationClient notificationClient;

	@Override
	public List<Notification> getNotification(String role, Integer userId, Integer entityId, Integer lastNotificationReceivedId) {
		String retriveNotificationsQuery = "select  DISTINCT n.id, n.title, n.description, n.priority,"
						+
						" DATE_FORMAT(n.date, '%Y-%m-%dT%TZ') as date , n.category, n.branch_id, n.image_url as imageUrl, "
						+
						" CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, "
						+
						" CONCAT(c1.firstname,' ',IFNULL(c1.lastname,'')) as forUser, ni.for_user_id as forUserId, IFNULL(nirs.read_status,'N') as readStatus "
						+
						" from notice n inner join notice_individual ni on ni.notice_id = n.id "
						+
						" inner join contact c on c.user_id = n.sent_by "
						+
						" inner join contact c1 on c1.user_id = ni.for_user_id "
						+
						" left join notice_individual_read_status nirs on nirs.notice_id=ni.notice_id and nirs.user_id=ni.user_id and nirs.for_user_id = ni.for_user_id ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);

		if ("PARENT".equalsIgnoreCase(role)) {
			retriveNotificationsQuery += " where  (ni.user_id = :userId and ni.for_user_id != :userId)";
		} else {
			retriveNotificationsQuery += " where  (ni.user_id = :userId and ni.for_user_id = :userId and n.sent_by != :userId ) ";

		}

		if (lastNotificationReceivedId != null && lastNotificationReceivedId != 0) {
			retriveNotificationsQuery += " and n.id > :lastNotificationReceivedId";
			queryParameters.put("lastNotificationReceivedId", lastNotificationReceivedId);
		}

		retriveNotificationsQuery += " and (n.category = 'news' or n.category = 'alert') order by n.id desc";

		// return getJdbcTemplate().query(retriveNotifications, new
		// BeanPropertyRowMapper<Notification>(Notification.class));

		List<Notification> personalNotifications = getNamedParameterJdbcTemplate().query(retriveNotificationsQuery, queryParameters,
						new BeanPropertyRowMapper<Notification>(Notification.class));

		// Get general notices to be displayed in case personalised notices are
		// not found for first time load.
		if (personalNotifications.size() == 0 && (lastNotificationReceivedId == null || lastNotificationReceivedId == 0)) {

			retriveNotificationsQuery = "select  DISTINCT n.id, n.title, n.description, n.priority, " +
							" DATE_FORMAT(n.date, '%Y-%m-%dT%TZ') as date , n.category, n.branch_id, n.image_url as imageUrl, 'Admin' as sent_by " +
							" from notice n where n.branch_id IS NULL and (n.category = 'news' or n.category = 'alert')" +
							" order by n.id desc limit 10";

			List<Notification> generalNotifications = getNamedParameterJdbcTemplate().query(retriveNotificationsQuery, queryParameters,
							new BeanPropertyRowMapper<Notification>(Notification.class));

			personalNotifications.addAll(generalNotifications);

		}

		return personalNotifications;

	}

	@Override
	public Notification getNotificationDetails(String role, Integer userId, Integer entityId, Integer notificationId) {

		String retriveNotificationsQuery = "select  n.id, n.title, n.description, n.priority, "
						+
						" DATE_FORMAT(n.date, '%d/%m/%Y %r') as date , n.category, n.branch_id, "
						+
						" n.image_url as imageUrl, n.detail_description, "
						+
						" CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, "
						+
						" CONCAT(c1.firstname,' ',IFNULL(c1.lastname,'')) as forUser, ni.for_user_id as forUserId, IFNULL(nirs.read_status,'N') as readStatus "
						+
						" from notice n inner join notice_individual ni on ni.notice_id = n.id "
						+
						" left join notice_individual_read_status nirs on nirs.notice_id=ni.notice_id and nirs.user_id=ni.user_id and nirs.for_user_id = ni.for_user_id "
						+
						" inner join contact c on c.user_id = n.sent_by " +
						" inner join contact c1 on c1.user_id = ni.for_user_id " +
						" where n.id = :notificationId and (n.branch_id iS NULL ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("notificationId", notificationId);
		queryParameters.put("userId", userId);

		if ("PARENT".equalsIgnoreCase(role)) {
			retriveNotificationsQuery += " or  (ni.user_id = :userId and ni.for_user_id != :userId)";
		} else {
			retriveNotificationsQuery += " or  (ni.user_id = :userId and ni.for_user_id = :userId) ";

		}

		retriveNotificationsQuery += ") ";

		List<Notification> notificatonList = getNamedParameterJdbcTemplate().query(retriveNotificationsQuery, queryParameters,
						new BeanPropertyRowMapper<Notification>(Notification.class));

		if (notificatonList.size() > 0) {
			return notificatonList.get(0);
		} else {
			return null;
		}

	}

	@Override
	public NotificationCount getUnreadMessageCount(String role, Integer roleId, int userId, int entityId) throws IOException, SQLException {

		NotificationCount notificationCount = new NotificationCount();
		// TODO: replace with ThreadPoolExecutor
		// Get unread chat counts first.
		// Retrieve current user chat profile.

		/*if (isChatProfileApplicable(role)) {
			String chatProfileQuery = " select * from user_chat_profile where user_id = :userId";

			Map<String, Object> queryParameters = new HashMap<String, Object>();
			queryParameters.put("userId", userId);

			List<UserChatProfile> userChatProfileList = getNamedParameterJdbcTemplate().query(chatProfileQuery, queryParameters,
							new RowMapper<UserChatProfile>() {
								@Override
								public UserChatProfile mapRow(ResultSet resultSet, int rowNum)
												throws SQLException {
									return new UserChatProfile(
													resultSet.getString("login"),
													resultSet.getString("password"),
													resultSet.getLong("chat_user_profile_id"));
								}
							});

			UserChatProfile userChatProfileobj = null;

			if (userChatProfileList != null && userChatProfileList.size() > 0) {
				userChatProfileobj = userChatProfileList.get(0);
				int chatUnreadMessageCount = chatDAO.getUnreadChatCount(userChatProfileobj.getLogin(), userChatProfileobj.getPassword());
				notificationCount.getUnreadNotifications().put("chat", new Integer(chatUnreadMessageCount));
			}

			notificationCount.getUnreadNotifications().put("chat", new Integer(0));

		}*/

		// Get fees dues count
		if (ROLES.PARENT.get().equalsIgnoreCase(role) || ROLES.STUDENT.get().equalsIgnoreCase(role)) {
			int feeDuesCount = feeDetailsDAO.getFeesDuesCount(entityId, role);
			notificationCount.getUnreadNotifications().put("payment", new Integer(feeDuesCount));
		}

		Map<String, Integer> unreadNotificationCounts = getUnreadNotificationCount(userId);
		for (String key : unreadNotificationCounts.keySet()) {
			notificationCount.getUnreadNotifications().put(key, unreadNotificationCounts.get(key));
		}

		Map<String, Integer> unreadEdiaryCounts = eventDAO.getUnreadEdiaryCount(userId);
		for (String key : unreadEdiaryCounts.keySet()) {
			notificationCount.getUnreadNotifications().put(key, unreadEdiaryCounts.get(key));
		}

		Map<String, Integer> unreadEventCounts = eventDAO.getUnreadEventCount(userId, roleId);
		for (String key : unreadEventCounts.keySet()) {
			notificationCount.getUnreadNotifications().put(key, unreadEventCounts.get(key));
		}

		return notificationCount;

	}

	private Map<String, Integer> getUnreadNotificationCount(Integer userId) {
		String retriveUnreadNotificationsQuery = "select n.category, " +
						" (" +
						" IFNULL(count(*),0) " +
						" - " +
						" IFNULL( (SELECT COUNT(*) FROM notice_individual_read_status nirs inner join notice n1 on n1.id = nirs.notice_id " +
						" WHERE  nirs.user_id=ni.user_id and n1.category = n.category and nirs.read_status = 'Y' group by n1.category),0) " +
						" ) AS unread_count " +
						" from notice_individual ni inner join notice n on ni.notice_id = n.id  " +
						" where ni.user_id = :userId and n.sent_by != ni.user_id " +
						" group by n.category ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);

		return getNamedParameterJdbcTemplate().query(retriveUnreadNotificationsQuery, queryParameters,
						new ResultSetExtractor<Map<String, Integer>>() {
							@Override
							public Map<String, Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
								Map<String, Integer> map = new HashMap<String, Integer>();

								while (rs.next()) {
									int count = (rs.getInt("unread_count") < 0) ? 0 : rs.getInt("unread_count");
									map.put(rs.getString("category").toLowerCase(), new Integer(count));
								}
								return map;
							}
						});

	}

	@Override
	public void sendNotification(Integer userid, Integer branchId, Integer instituteId, String role, SendNotfication sendNotfication)
					throws AuthorisationException,
					IOException, ParseException {

		List<BranchStudentParentDetail> branchStudentParentDetailList = saveNotification(userid, branchId, instituteId, sendNotfication);

		prepareAndSendNotification(sendNotfication, branchStudentParentDetailList);

	}

	@Transactional(readOnly = false)
	private List<BranchStudentParentDetail> saveNotification(Integer userid, Integer branchId, Integer instituteId, SendNotfication sendNotfication)
					throws AuthorisationException, IOException {
		if (sendNotfication.getGroupIds() != null && !sendNotfication.getGroupIds().isEmpty()) {
			// check if user has access to groups provided in request.
			if (!userDao.canUserAccessSpecifiedGroup(userid, sendNotfication.getGroupIds())) {
				throw new AuthorisationException("Function not allowed.");
			}

		}

		if (sendNotfication.getUserIds() != null && !sendNotfication.getUserIds().isEmpty()) {
			// check if user has access to groups provided in request.
			if (!userDao.canUserAccessSpecifiedUserProfiles(userid, branchId, sendNotfication.getUserIds())) {
				throw new AuthorisationException("Function not allowed.");
			}

		}

		AcademicYear activeAcademicYear = branchDao.getActiveAcademicYear(branchId);

		SimpleJdbcInsert insertNotice =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("notice")
										.usingColumns("title", "description", "priority", "date", "image_url", "category", "branch_id", "detail_description",
														"sent_by", "academic_year_id")
										.usingGeneratedKeyColumns("id");

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("title", sendNotfication.getTitle());
		parameters.put("description", sendNotfication.getDescription());
		parameters.put("priority", sendNotfication.getPriority());

		parameters.put("date", new java.sql.Date(System.currentTimeMillis()));

		if (sendNotfication.getFileAttachment() != null) {
			String randomFileName = FileStorePathAssembler.getRandomFileName(sendNotfication.getFileAttachment().getFileNameWithExtension());
			String noticeStoragePath = FileStorePathAssembler.getNoticeStoragePath(instituteId, branchId);
			fileDao.saveFile(noticeStoragePath, sendNotfication.getFileAttachment().getBytes(), randomFileName);

			String imageUrlPath = FileStorePathAssembler.getNoticeRelativePath(instituteId, branchId) + FileStorePathAssembler.PATH_SEPARATOR + randomFileName;
			parameters.put("image_url", imageUrlPath);
		} else {
			parameters.put("image_url", null);
		}

		parameters.put("category", sendNotfication.getCategory());
		parameters.put("branch_id", branchId);
		parameters.put("detail_description", sendNotfication.getDetailDescription());
		parameters.put("sent_by", userid);
		parameters.put("academic_year_id", activeAcademicYear.getId());

		Long noticeId = (Long) insertNotice.executeAndReturnKey(parameters);

		// Send notice to all gcm's of the users.
		List<BranchStudentParentDetail> branchStudentParentDetailList = new ArrayList<BranchStudentParentDetail>();
		Set<Integer> individualNotificationUserIds = new HashSet<Integer>();
		//Set<Integer> notifyToParentUserIds1 = new HashSet<Integer>();
		if (sendNotfication.getGroupIds() != null && !sendNotfication.getGroupIds().isEmpty()) {

			//gcmIdsMap.putAll(userDao.getGcmIdsOfGroupUsers(sendNotfication.getGroupIds()));

			String noticeGroup = "INSERT INTO notice_group (notice_id, group_id) VALUES (" + noticeId.intValue() + ", ?)";
			final List<Integer> groupIds = sendNotfication.getGroupIds();
			getJdbcTemplate().batchUpdate(noticeGroup, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Integer groupId = groupIds.get(i);
					ps.setInt(1, groupId);
				}

				@Override
				public int getBatchSize() {
					return groupIds.size();
				}
			});

			// Get students and teacher records only
			Set<Integer> groupUserIds = userDao.getGroupUsers(groupIds, true);

			//Get parent of all students in the group
			branchStudentParentDetailList = userDao.getBranchStudentParentDetailByStudentGroup(groupIds, branchId);

			individualNotificationUserIds.addAll(groupUserIds);

		}

		if (sendNotfication.getUserIds() != null && !sendNotfication.getUserIds().isEmpty()) {

			List<BranchStudentParentDetail> branchStudentParentDetailByStudentIdList = userDao.getBranchStudentParentDetailByStudent(sendNotfication
							.getUserIds(), branchId);

			if (!CollectionUtils.isEmpty(branchStudentParentDetailByStudentIdList)) {
				branchStudentParentDetailList.addAll(branchStudentParentDetailByStudentIdList);
			}

			individualNotificationUserIds.addAll(sendNotfication.getUserIds());
		}

		if (individualNotificationUserIds.size() > 0) {
			saveIndividualNotification(noticeId.intValue(), new ArrayList(individualNotificationUserIds));
		}

		if (branchStudentParentDetailList.size() > 0) {
			saveIndividualNotificationForParent(noticeId.intValue(), branchStudentParentDetailList);
		}
		return branchStudentParentDetailList;
	}

	@Override
	public List<Notification> getAssignment(String role, Integer userId, Integer entityId, Integer lastAssignmentReceivedId) {
		String retriveAssignmentsQuery = "select  DISTINCT n.id, n.title, n.description, n.priority,"
						+ " DATE_FORMAT(n.date, '%Y-%m-%dT%TZ') as date , n.category, n.branch_id, n.image_url as imageUrl, "
						+ " CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, "
						+ " CONCAT(c1.firstname,' ',IFNULL(c1.lastname,'')) as forUser, ni.for_user_id as forUserId, IFNULL(nirs.read_status,'N') as readStatus "
						+ " from notice n left join notice_individual ni on ni.notice_id = n.id "
						+ " left join notice_individual_read_status nirs on nirs.notice_id=ni.notice_id and nirs.user_id=ni.user_id and nirs.for_user_id = ni.for_user_id "
						+ " inner join contact c on c.user_id = n.sent_by "
						+ " inner join contact c1 on c1.user_id = ni.for_user_id "
						+ " where ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);

		if (Identity.ROLES.PARENT.get().equals(role)) {
			retriveAssignmentsQuery += " (ni.user_id = :userId and ni.for_user_id != :userId) ";
		} else {
			retriveAssignmentsQuery += " (ni.user_id = :userId and n.sent_by != :userId)";

		}

		if (lastAssignmentReceivedId != null && lastAssignmentReceivedId != 0) {
			retriveAssignmentsQuery += " and n.id > :lastAssignmentReceivedId";
			queryParameters.put("lastAssignmentReceivedId", lastAssignmentReceivedId);
		}

		retriveAssignmentsQuery += " and n.category = 'Assignment' order by n.id desc";

		List<Notification> assignments = getNamedParameterJdbcTemplate().query(retriveAssignmentsQuery, queryParameters,
						new BeanPropertyRowMapper<Notification>(Notification.class));

		return assignments;
	}

	private boolean isChatProfileApplicable(String role) {
		return ("PARENT".equalsIgnoreCase(role) || "TEACHER".equalsIgnoreCase(role) || "VENDOR".equalsIgnoreCase(role)) ? true : false;
	}

	private void saveIndividualNotification(final int noticeId, final List<Integer> userIds) {

		String noticeIndividual = "INSERT INTO notice_individual (notice_id, user_id, for_user_id) VALUES (?, ?, ?)";

		getJdbcTemplate().batchUpdate(noticeIndividual, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Integer userId = userIds.get(i);
				ps.setInt(1, noticeId);
				ps.setInt(2, userId);
				ps.setInt(3, userId);
			}

			@Override
			public int getBatchSize() {
				return userIds.size();
			}
		});
	}

	private void saveIndividualNotificationForParent(final int noticeId, final List<BranchStudentParentDetail> branchStudentParentDetailList) {

		String noticeIndividual = "INSERT INTO notice_individual (notice_id, user_id, for_user_id) VALUES (?, ?, ?)";

		getJdbcTemplate().batchUpdate(noticeIndividual, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				BranchStudentParentDetail branchStudentParentDetail = branchStudentParentDetailList.get(i);
				ps.setInt(1, noticeId);
				ps.setInt(2, branchStudentParentDetail.getParentUserId());
				ps.setInt(3, branchStudentParentDetail.getStudentUserId());
			}

			@Override
			public int getBatchSize() {
				return branchStudentParentDetailList.size();
			}
		});
	}

	private void assignNotificationToIndividualUser(final List<Integer> noticeIds, final int userId, final int studentUserId) {

		String noticeIndividual = "INSERT INTO notice_individual (notice_id, user_id,for_user_id) VALUES (?, ?, ?)";

		getJdbcTemplate().batchUpdate(noticeIndividual, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Integer noticeId = noticeIds.get(i);
				ps.setInt(1, noticeId);
				ps.setInt(2, userId);
				ps.setInt(3, studentUserId);
			}

			@Override
			public int getBatchSize() {
				return noticeIds.size();
			}
		});
	}

	@Override
	public void assignStudentNoticationsToParent(Long studentUserId, Integer parentUserId) {

		String selectNotifications = " select id from notice inner join notice_individual on notice.id = notice_individual.notice_id where notice_individual.user_id= :studentUserId"
						+ " and NOT EXISTS (select notice_id from notice_individual where notice_individual.notice_id = notice.id and notice_individual.user_id=:parentUserId)";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("studentUserId", studentUserId);
		queryParameters.put("parentUserId", parentUserId);

		List<Integer> noticeIdList = getNamedParameterJdbcTemplate().queryForList(selectNotifications, queryParameters, Integer.class);

		assignNotificationToIndividualUser(noticeIdList, parentUserId, studentUserId.intValue());

	}

	@Override
	public boolean canAccessNoticeDetail(Integer userId, Integer notificationId) {

		String selectNotificationQuery = " select id from notice n inner join notice_individual ni on ni.notice_id = n.id "
						+ " where  ni.user_id = :userId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);

		List<Integer> noticeIdList = getNamedParameterJdbcTemplate().queryForList(selectNotificationQuery, queryParameters, Integer.class);

		return (noticeIdList.size() > 0);
	}

	@Override
	public NotificationRecipientDetail getNotificationRecipientDetail(Integer notificationId) {

		NotificationRecipientDetail notificationRecipientDetail = new NotificationRecipientDetail();

		String noticeSentToGroupQuery = "select g.id as id, g.name as name, g.description as description from `group` g inner join notice_group ng on ng.group_id = g.id "
						+ " where ng.notice_id = :notificationId";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("notificationId", notificationId);

		List<Group> groupList = getNamedParameterJdbcTemplate().query(noticeSentToGroupQuery, queryParameters,
						new RowMapper<Group>() {
							@Override
							public Group mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new Group(rs.getLong("id"), rs.getString("name"), rs.getString("description"));
							}
						});

		notificationRecipientDetail.setGroups(groupList);

		String noticeSentToUserQuery = "select firstName, lastName,city, photo, email, phone, contact.user_id from contact " +
						" inner join notice_individual on notice_individual.user_id = contact.user_id " +
						" where notice_individual.notice_id = :notificationId  " +
						" and NOT EXISTS (" +
						" select user_id from group_user inner join notice_group on group_user.group_id = notice_group.group_id " +
						" where notice_group.notice_id=:notificationId and group_user.user_id=contact.user_id)";

		List<Contact> contactList = getNamedParameterJdbcTemplate().query(noticeSentToUserQuery, queryParameters,
						new RowMapper<Contact>() {
							@Override
							public Contact mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new Contact(rs.getString("firstname"), rs.getString("lastname"), rs.getString("city"), rs.getString("photo"),
												new Integer(rs.getInt("user_id")), rs.getString("email"), rs.getString("phone"));
							}
						});

		notificationRecipientDetail.setContacts(contactList);

		return notificationRecipientDetail;
	}

	private void prepareAndSendNotification(final SendNotfication notice, List<BranchStudentParentDetail> branchStudentParentDetailList) {

		/*String sql = "select distinct ur.user_id, ur.role_id, r.name as role, " +
						" b.name as branchName, i.name as instituteName " +
						" from user_role as ur " +
						" INNER JOIN role as r on ur.role_id = r.id " +
						" INNER JOIN notice_individual as ni on ur.user_id = ni.for_user_id " +
						" INNER JOIN notice as n on ni.notice_id = n.id and n.id = " + notice.getId() +
						" INNER JOIN branch as b on b.id = n.branch_id " +
						" INNER JOIN institute as i on i.id = b.institute_id" +
						" WHERE ur.role_id != " + Identity.ROLES_ID.PARENT.get();

		System.out.println("SQL::: " + sql);
		final Map<String, Integer> userIds = new HashMap<>();
		final Map<String, Object> content = new HashMap<String, Object>();

		getJdbcTemplate().query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Integer userId = resultSet.getInt("user_id");
				Integer roleId = resultSet.getInt("role_id");

				userIds.put(resultSet.getString("role").toUpperCase() + "-" + userId, userId);

				if (content.size() == 0) {
					content.put("institute_name", resultSet.getString("instituteName"));
					content.put("branch_name", resultSet.getString("branchName"));
				}

				return null;
			}
		});

		// Add parent ids
		if (!CollectionUtils.isEmpty(branchStudentParentDetailList)) {
			String parentRoleName = Identity.ROLES.PARENT.get();
			for (BranchStudentParentDetail branchStudentParentDetail : branchStudentParentDetailList) {
				userIds.put(parentRoleName + "-" + branchStudentParentDetail.getParentUserId(), branchStudentParentDetail.getParentUserId());
			}
		}

		content.put("push-title", "NOTICE");
		content.put("push-message", notice.getTitle());
		String additionalData = "{"
						+ "\"type\":\"" + (notice.getCategory().equalsIgnoreCase("Assignment")
										? notice.getCategory().toUpperCase() : "NOTICE") + "\","
						+ "\"id\":" + notice.getId() + "}";

		content.put("push-detailJson", additionalData);*/

		// Create notification object to send to notification-service.
		com.isg.service.user.client.notification.Notification notification = new com.isg.service.user.client.notification.Notification();
		//notification.setContent(content);
		//notification.setUserIds(userIds);
		notification.setType("NOTICE");
		notification.setId(notice.getId());
		notification.sendEmail(true);
		notification.sendSMS(true);
		notification.sendPushMessage(true);
		notification.setTemplateCode(Constant.NotificationTemplateCode.CREATE_NOTICE.get());
		notification.setBatchEnabled(false);

		try {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println("NOTICE OBJECT:::::\n" + mapper.writeValueAsString(notification));
			notificationClient.sendMessage(mapper.writeValueAsString(notification));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}

	}

	@Override
	public Map<String, Integer> markAsRead(Integer notificationId, Integer userId, Integer forUserId) {
		String query = " INSERT INTO notice_individual_read_status (notice_id, user_id, for_user_id, read_status) " +
						" VALUES(:notificationId, :userId, :forUserId,'Y') ON DUPLICATE KEY UPDATE " +
						" notice_id = VALUES(notice_id), user_id = VALUES(user_id), for_user_id = VALUES(for_user_id), read_status = VALUES(read_status) ";

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("notificationId", notificationId);
		parameters.put("userId", userId);
		parameters.put("forUserId", forUserId);

		getNamedParameterJdbcTemplate().update(query, parameters);

		return getUnreadNotificationCount(userId);
	}
}