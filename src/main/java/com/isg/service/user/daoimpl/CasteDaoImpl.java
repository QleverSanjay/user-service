package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.CasteDao;
import com.isg.service.user.request.Caste;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class CasteDaoImpl extends JdbcDaoSupport implements CasteDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<Caste> getCastes() {
		String retriveCaste = "select * from caste where active='Y' order by display_priority";
		return getJdbcTemplate().query(retriveCaste, new BeanPropertyRowMapper<Caste>(Caste.class));
	}

	@Override
	public Caste getCasteById(int id) {
		String retriveCaste = "select * from caste where id = ? and active = 'Y'";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().queryForObject(retriveCaste, inputs, new BeanPropertyRowMapper<Caste>(Caste.class));
	}

	@Override
	public List<Caste> getCastesByName(String casteName) {
		String retriveCaste = "select * from caste where name like ? and active = 'Y'";
		String likeString = "%" + casteName + "%";
		Object[] inputs = new Object[] { likeString };
		return getJdbcTemplate().query(retriveCaste, inputs, new BeanPropertyRowMapper<Caste>(Caste.class));
	}

	@Override
	public List<Caste> getSubCastesByCasteId(Integer casteId) {
		String retriveCaste = "select * from sub_caste where caste_id = ? and active = 'Y' order by code";
		Object[] inputs = new Object[] { casteId };
		return getJdbcTemplate().query(retriveCaste, inputs, new BeanPropertyRowMapper<Caste>(Caste.class));
	}

}
