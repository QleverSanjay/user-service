package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.client.RestClient;
import com.isg.service.user.client.forum.ForumUrlConfiguration;
import com.isg.service.user.client.forum.request.user.CreateUserRequest;
import com.isg.service.user.client.forum.request.user.LinkUserForumRequest;
import com.isg.service.user.client.forum.response.user.CreateUserResponse;
import com.isg.service.user.dao.BranchDao;
import com.isg.service.user.dao.ForumDao;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.UserForumProfile;
import com.isg.service.user.request.BranchForumDetail;
import com.isg.service.user.request.Parent;
import com.isg.service.user.util.Constant;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class ForumDaoImpl extends NamedParameterJdbcDaoSupport implements ForumDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private static final Logger log = Logger.getLogger(ForumDaoImpl.class);

	@Autowired
	private BranchDao branchDao;

	@Autowired
	private ParentDao parentDao;

	@Autowired
	@Lazy
	private UserDao userDao;

	@Override
	public void setupAccountForParent(Integer studentId, Integer branchId, Integer parentId, List<String> forumIdList) throws IOException {

		UserForumProfile userForumProfile = null;

		Parent parent = parentDao.getParent(parentId);

		if (parent != null) {
			userForumProfile = userDao.getUserForumProfileDetails(parent.getUser_id());
		}

		if (parent != null && userForumProfile == null) {
			String usernameOrEmail = RandomStringUtils.random(20, true, true) + "@" + Constant.EDUQFIX_DOMAIN;
			String password = RandomStringUtils.random(20, true, true);

			BranchForumDetail branchForumDetail = branchDao.getbranchForumDetail(branchId);
			CreateUserResponse createUserResponse = createUser(usernameOrEmail, password, usernameOrEmail, branchForumDetail.getReadOnlyGroupId(),
							branchForumDetail.getCategoryId());

			SimpleJdbcInsert insertUser =
							new SimpleJdbcInsert(getDataSource())
											.withTableName("user_forum_profile")
											.usingColumns("login", "password", "forum_profile_id", "user_id")
											.usingGeneratedKeyColumns("id");
			Map<String, Object> parameters = new HashMap<String, Object>(4);
			parameters.put("login", usernameOrEmail);
			parameters.put("password", password);
			parameters.put("forum_profile_id", createUserResponse.getForumProfileId());
			parameters.put("user_id", parent.getUser_id());
			insertUser.execute(parameters);
			log.info("Parent forum profile registered.");

			Set<Integer> forumUserIdList = new HashSet<Integer>();
			forumUserIdList.add(Integer.parseInt(createUserResponse.getForumProfileId()));

			// Link to existing forum.
			if (!CollectionUtils.isEmpty(forumIdList)) {
				for (String forumId : forumIdList) {

					RestClient restClient = new RestClient();
					LinkUserForumRequest linkUserForumRequest = new LinkUserForumRequest();
					linkUserForumRequest.setGroupId(forumId);
					linkUserForumRequest.setUserIdList(forumUserIdList);

					ResponseEntity<String> linkForumUserResponse = restClient
									.executeRequest(linkUserForumRequest,
													ForumUrlConfiguration.LINK_USER_TO_FORUM_CONFIG);

					if (linkForumUserResponse.getStatusCode() != HttpStatus.OK) {
						throw new ApplicationException("Link user to existing forums rquest failed with status."
										+ linkForumUserResponse.getBody());
					}

				}
			}

		}

	}

	private CreateUserResponse createUser(String username, String password, String email, int userForumGroupId, int branchId) throws IOException {
		CreateUserResponse createUserResponse = null;

		RestClient restClient = new RestClient();
		CreateUserRequest userRequest = new CreateUserRequest();
		userRequest.setEmail(email);
		userRequest.setUsername(username);
		userRequest.setPassword(password);
		userRequest.setSchoolId(branchId);

		ResponseEntity<String> createForumUserResponse = restClient
						.executeRequest(userRequest,
										ForumUrlConfiguration.CREATE_USER_CONFIG);

		if (createForumUserResponse.getStatusCode() == HttpStatus.OK) {
			ObjectMapper mapper = new ObjectMapper();
			createUserResponse = mapper.readValue(
							createForumUserResponse.getBody(), CreateUserResponse.class);

		} else {
			throw new ApplicationException("Forum User create rquest failed with status."
							+ createForumUserResponse.getBody());

		}

		return createUserResponse;
	}

}