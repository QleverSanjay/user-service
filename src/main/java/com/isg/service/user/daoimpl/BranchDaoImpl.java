package com.isg.service.user.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.user.dao.BranchDao;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.AcademicYear;
import com.isg.service.user.model.BranchWorkingDay;
import com.isg.service.user.model.HolidayResponse;
import com.isg.service.user.model.HolidayRowCallbackHandler;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.BranchForumDetail;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class BranchDaoImpl extends NamedParameterJdbcDaoSupport implements BranchDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private static final Logger log = Logger.getLogger(BranchDaoImpl.class);

	private final String RETRIEVE_HOLIDAY_FOR_PARENT_ROLE_QUERY = "select DATE_FORMAT(h.from_date, '%d-%m-%Y') as from_date, "
					+ " DATE_FORMAT(h.to_date, '%d-%m-%Y') as to_date, h.id, h.title, h.description, h.branch_id,"
					+ " s.first_name as profile_name, s.gender as gender, r.name as role_name ,s.user_id as user_id, s.id as entity_id "
					+ " from student s "
					+ " inner join holidays h on h.branch_id= s.branch_id "
					+ " inner join user u on u.id = s.user_id "
					+ " inner join user_role ur on ur.user_id = u.id "
					+ " inner join role r on r.id = ur.role_id and (ur.role_id= 2) "
					+ " inner join branch_student_parent bsp on bsp.student_id = s.id and bsp.branch_id = h.branch_id and bsp.isDelete = 'N'"
					+ " where bsp.parent_id = :entityId "
					+ " and h.current_academic_year in (select id from academic_year where academic_year.branch_id=s.branch_id and academic_year.is_current_active_year = 'Y') "
					+ " order by h.from_date asc";

	private final String RETRIEVE_HOLIDAY_FOR_STUDENT_ROLE_QUERY = "select DATE_FORMAT(h.from_date, '%d-%m-%Y') as from_date, "
					+ " DATE_FORMAT(h.to_date, '%d-%m-%Y') as to_date, h.id, h.title, h.description, h.branch_id,"
					+ " s.first_name as profile_name, s.gender as gender, r.name as role_name ,s.user_id as user_id, s.id as entity_id "
					+ " from student s "
					+ " inner join holidays h on h.branch_id= s.branch_id "
					+ " inner join user u on u.id = s.user_id "
					+ " inner join user_role ur on ur.user_id = u.id "
					+ " inner join role r on r.id = ur.role_id and (ur.role_id= 2) "
					+ " where s.id = :entityId  "
					+ " and h.current_academic_year in (select id from academic_year where academic_year.branch_id=s.branch_id and academic_year.is_current_active_year = 'Y') "
					+ " order by h.from_date asc";

	private final String RETRIEVE_HOLIDAY_FOR_TEACHER_ROLE_QUERY = "select DATE_FORMAT(h.from_date, '%d-%m-%Y') as from_date, "
					+ " DATE_FORMAT(h.to_date, '%d-%m-%Y') as to_date, h.id, h.title, h.description, h.branch_id,"
					+ " t.firstName as profile_name, t.gender as gender, r.name as role_name ,t.user_id as user_id, t.id as entity_id "
					+ " from teacher t "
					+ " inner join holidays h on h.branch_id= t.branch_id "
					+ " inner join user u on u.id = t.user_id "
					+ " inner join user_role ur on ur.user_id = u.id "
					+ " inner join role r on r.id = ur.role_id and (ur.role_id= 4) "
					+ " where t.id = :entityId  "
					+ " and h.current_academic_year in (select id from academic_year where academic_year.branch_id=t.branch_id and academic_year.is_current_active_year = 'Y') "
					+ " order by h.from_date asc";

	private final String RETRIEVE_ALL_ACADEMIC_YEAR = "select DATE_FORMAT(from_date, '%d-%m-%Y') as from_date, DATE_FORMAT(to_date, '%d-%m-%Y') as to_date, id,  "
					+ " CONCAT(DATE_FORMAT(from_date, '%Y'),' - ', DATE_FORMAT(to_date, '%Y')) as display_name from academic_year where branch_id=:branchId and is_delete='N' ";

	private final String RETRIEVE_CURRENT_ACTIVE_ACADEMIC_YEAR = RETRIEVE_ALL_ACADEMIC_YEAR + " and is_current_active_year ='Y'";

	private final String RETRIEVE_HOLIDAY_FOR_BRANCH_QUERY = "select DATE_FORMAT(h.from_date, '%d-%m-%Y') as from_date, "
					+ " DATE_FORMAT(h.to_date, '%d-%m-%Y') as to_date, h.id, h.title, h.description, h.branch_id, 'ADMIN' as role_name , 0 as user_id, 0 as entity_id  "
					+ " from holidays h "
					+ " where h.branch_id = :branchId "
					+ " and h.current_academic_year in (select id from academic_year where academic_year.branch_id=h.branch_id and academic_year.is_current_active_year = 'Y') "
					+ " order by h.from_date asc";

	private final String RETRIEVE_WORKING_DAYS_FOR_BRANCH_QUERY = "select wd.* from working_days wd where wd.branch_id=:branchId "
					+ " and wd.current_academic_year in (select id from academic_year where academic_year.branch_id=wd.branch_id and academic_year.is_current_active_year = 'Y') ";

	@Override
	public List<Branch> getAll() {
		String retriveBranches = "select * from branch where is_delete ='N' and active='Y'";
		return getJdbcTemplate().query(retriveBranches, new BeanPropertyRowMapper<Branch>(Branch.class));
	}

	@Override
	public Branch getBranchDetails(Integer branchId) {
		String retriveBranch = "select * from branch where id=?";
		Object[] inputs = new Object[] { branchId };
		return getJdbcTemplate().queryForObject(retriveBranch, inputs, new BeanPropertyRowMapper<Branch>(Branch.class));
	}

	@Override
	public BranchForumDetail getbranchForumDetail(Integer branchId) {
		String retriveBranch = "select * from branch_forum_detail where branch_id=?";
		Object[] inputs = new Object[] { branchId };

		return getJdbcTemplate().queryForObject(retriveBranch, inputs, new RowMapper<BranchForumDetail>() {
			@Override
			public BranchForumDetail mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				return new BranchForumDetail(
								new Integer(resultSet.getInt("branch_id")),
								new Integer(resultSet.getInt("forum_category_id")),
								new Integer(resultSet.getInt("read_write_group_id")),
								new Integer(resultSet.getInt("read_only_group_id")));
			}
		});
	}

	@Override
	public Collection<HolidayResponse> getHolidays(Integer userId, Integer entityId, String role) throws AuthorisationException {
		return getHolidays(userId, entityId, role, null);
	}

	@Override
	public Collection<HolidayResponse> getHolidays(Integer userId, Integer entityId, String role, Integer branchId) throws AuthorisationException {
		String query = "";

		if ("PARENT".equalsIgnoreCase(role)) {
			query = RETRIEVE_HOLIDAY_FOR_PARENT_ROLE_QUERY;
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			query = RETRIEVE_HOLIDAY_FOR_TEACHER_ROLE_QUERY;
		} else if ("STUDENT".equalsIgnoreCase(role)) {
			query = RETRIEVE_HOLIDAY_FOR_STUDENT_ROLE_QUERY;
		} else if (ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(role) || ROLES.ADMIN.get().equalsIgnoreCase(role)) {
			query = RETRIEVE_HOLIDAY_FOR_BRANCH_QUERY;
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		HashMap<Integer, HolidayResponse> holidayResponseMap = new HashMap<Integer, HolidayResponse>();
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("entityId", entityId);
		parameters.put("userId", userId);
		parameters.put("branchId", branchId);

		log.debug("getHolidays query :: " + query);
		log.debug("getHolidays query parameters :: " + parameters);
		HolidayRowCallbackHandler holidayRowCallbackHandler = new HolidayRowCallbackHandler(holidayResponseMap);
		getNamedParameterJdbcTemplate().query(query, parameters, holidayRowCallbackHandler);

		holidayResponseMap = holidayRowCallbackHandler.getPopulatedMap();

		List<HolidayResponse> holidayResponseList = new ArrayList<HolidayResponse>();

		holidayResponseList.addAll(holidayResponseMap.values());
		return holidayResponseList;
	}

	@Override
	public AcademicYear getActiveAcademicYear(Integer branchId) {
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("branchId", branchId);

		return getNamedParameterJdbcTemplate().queryForObject(RETRIEVE_CURRENT_ACTIVE_ACADEMIC_YEAR, parameters, new RowMapper<AcademicYear>() {
			@Override
			public AcademicYear mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				AcademicYear academicYear = new AcademicYear();
				academicYear.setId(resultSet.getInt("id"));
				academicYear.setFromDate(resultSet.getString("from_date"));
				academicYear.setToDate(resultSet.getString("to_date"));
				return academicYear;
			}
		});

	}

	@Override
	public BranchWorkingDay getWorkingDay(Integer branchId) {

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("branchId", branchId);
		return getNamedParameterJdbcTemplate().queryForObject(RETRIEVE_WORKING_DAYS_FOR_BRANCH_QUERY, parameters, new RowMapper<BranchWorkingDay>() {
			@Override
			public BranchWorkingDay mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				BranchWorkingDay branchWorkingDay = new BranchWorkingDay();
				branchWorkingDay.setIsMondayWorking(resultSet.getString("monday").charAt(0));
				branchWorkingDay.setIsTuesdayWorking(resultSet.getString("tuesday").charAt(0));
				branchWorkingDay.setIsWednesdayWorking(resultSet.getString("wednesday").charAt(0));
				branchWorkingDay.setIsThursdayWorking(resultSet.getString("thursday").charAt(0));
				branchWorkingDay.setIsFridayWorking(resultSet.getString("friday").charAt(0));
				branchWorkingDay.setIsSaturdayWorking(resultSet.getString("saturday").charAt(0));
				branchWorkingDay.setIsSundayWorking(resultSet.getString("sunday").charAt(0));
				branchWorkingDay.setIsRoundRobinTimetable(resultSet.getString("skip_timetable_on_holiday").charAt(0));

				return branchWorkingDay;
			}
		});
	}

	@Override
	public List<AcademicYear> getAcademicYearList(Integer branchId) {

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("branchId", branchId);

		return getNamedParameterJdbcTemplate().query(RETRIEVE_ALL_ACADEMIC_YEAR, parameters, new RowMapper<AcademicYear>() {
			@Override
			public AcademicYear mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				AcademicYear academicYear = new AcademicYear();
				academicYear.setId(resultSet.getInt("id"));
				academicYear.setFromDate(resultSet.getString("from_date"));
				academicYear.setToDate(resultSet.getString("to_date"));
				academicYear.setDisplayName(resultSet.getString("display_name"));
				return academicYear;
			}
		});
	}



	/*@Override
	public List<Branch> searchBranch(Integer branchId) {
		String searchQuery = "SELECT * FROM branch b inner join institute i on i.id = b.institute_id where b.id =?";
		Object[] inputs = new Object[] { "%"+branchId+"%" };
		return getJdbcTemplate().query(searchQuery, inputs, new BeanPropertyRowMapper<Branch>(Branch.class));
		
	}*/

}