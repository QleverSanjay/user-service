package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.ReligionDao;
import com.isg.service.user.request.Religion;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class ReligionDaoImpl extends JdbcDaoSupport implements ReligionDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public List<Religion> getReligions() {
		String retriveReligion = "select * from religion where active = 'Y'";
		return getJdbcTemplate().query(retriveReligion, new BeanPropertyRowMapper<Religion>(Religion.class));
	}

	@Override
	public List<Religion> getReligionsById(int id) {
		String retriveReligion = "select * from religion where id = ? and active = 'Y'";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().query(retriveReligion, inputs,new BeanPropertyRowMapper<Religion>(Religion.class));
	}

	@Override
	public List<Religion> getReligionsByName(String religionName) {
		String retriveReligion = "select * from religion where name like ? and active = 'Y'";
		String likeString = "%" + religionName +"%";
		Object[] inputs = new Object[] {likeString};
		return getJdbcTemplate().query(retriveReligion, inputs,new BeanPropertyRowMapper<Religion>(Religion.class));
	}

}
