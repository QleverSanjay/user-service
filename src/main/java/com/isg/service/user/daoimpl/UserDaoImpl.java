package com.isg.service.user.daoimpl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isg.service.identity.model.Identity;
import com.isg.service.user.client.notification.NotificationClient;
import com.isg.service.user.client.social.response.UserInfoResponse;
import com.isg.service.user.dao.SocialProfileCommunicationDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.InvalidLoginCredentialsException;
import com.isg.service.user.model.BranchStudentParentDetail;
import com.isg.service.user.model.Contact;
import com.isg.service.user.model.Group;
import com.isg.service.user.model.GroupUser;
import com.isg.service.user.model.OTPDetails;
import com.isg.service.user.model.PaymentAccount;
import com.isg.service.user.model.UserChatProfile;
import com.isg.service.user.model.UserForumProfile;
import com.isg.service.user.request.ChangePassword;
import com.isg.service.user.request.ParentOtp;
import com.isg.service.user.request.Passbook;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.User;
import com.isg.service.user.response.OtpResponse;
import com.isg.service.user.response.UserDetail;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.Constant.EmailTemplate;
import com.isg.service.user.util.Constant.PASSBOOK_TRANSACTION_TYPE;
import com.isg.service.user.util.Constant.SmsTemplate;
import com.isg.service.user.util.SecurityUtil;
import com.jolbox.bonecp.BoneCPDataSource;
import com.mysql.jdbc.Statement;

@Slf4j
@Repository
public class UserDaoImpl extends NamedParameterJdbcDaoSupport implements UserDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private static final Logger log = Logger.getLogger(UserDaoImpl.class);

	@Autowired
	@Lazy
	private StudentDao studentDAO;

	@Autowired
	private SocialProfileCommunicationDao socialProfileCommunicationDao;

	@Autowired
	private NotificationClient notificationClient;

	@Override
	public User getUserDetails(User user) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidLoginCredentialsException {
		String query = "select u.id as id, u.username as username, u.temp_password as plain_password, u.password as encrypted_password, u.password_salt as password_salt, u.gcm_id as gcm_id, u.device as device, r.name as role_name "
						+ " from user u inner join user_role ur on ur.user_id = u.id "
						+ " inner join role r on  r.id = ur.role_id "
						+ " where u.username=? and u.active='Y' and u.isDelete = 'N'";
		//+ " and authentication_type=?";

		User userInDb = loadUser(user.getUsername(), Constant.AUTHENTICATION_TYPE.APPLICATION.get(), query);

		verifyPassword(user.getPassword(), userInDb);

		return userInDb;

	}
	
	

	private void verifyPassword(String passwordEnteredByUser, User userInDb) throws NoSuchAlgorithmException, InvalidKeySpecException,
					InvalidLoginCredentialsException {
		// Validate password based on salt
		byte[] passwordSalt = userInDb.getPasswordSalt();
		byte[] encryptedPassword = userInDb.getEncryptedPassword();

		//		boolean authenticatePassword = SecurityUtil.authenticate(passwordEnteredByUser, encryptedPassword, passwordSalt);
		log.debug("Password entered >>>>>>>" + passwordEnteredByUser);
		boolean authenticatePassword = passwordEnteredByUser.equals(userInDb.getPlainPassword());
		if (!authenticatePassword) {
			throw new InvalidLoginCredentialsException("Invalid username / password specified.");
		}
	}

	@Override
	public UserInfoResponse getUserSocialProfileDetails(String socialProfileProvider, String accessToken) throws JsonProcessingException {
		return socialProfileCommunicationDao.getAccountInfo(socialProfileProvider, accessToken);
	}

	@Override
	public User getUserDetailsBySocialProfile(UserInfoResponse userInfoResponse) throws SQLException, JsonProcessingException {
		String query = "select u.id as id, u.username as username, u.password as encrypted_password, u.password_salt as password_salt, u.gcm_id as gcm_id, u.device as device, r.name as role_name "
						+ " from user u inner join user_role ur on ur.user_id = u.id" +
						"   inner join role r on  r.id = ur.role_id "
						+ " where u.username=? and u.active='Y' and u.isDelete = 'N' ";
		return loadUser(userInfoResponse.getEmail(), Constant.AUTHENTICATION_TYPE.SOCIAL.get(), query);
	}

	private User loadUser(String username, String autheticationtype, String query) throws SQLException {

		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;
		User userObj = null;

		try {
			conn = getJdbcTemplate().getDataSource().getConnection();
			stmt = conn.prepareStatement(query);

			stmt.setString(1, username);

			/*if (Constant.AUTHENTICATION_TYPE.SOCIAL.get().equals(autheticationtype)) {
				stmt.setString(1, username);
			} else {
				stmt.setString(1, username);
				// stmt.setString(2, password);
				stmt.setString(2, autheticationtype);
			}*/

			log.debug("Query ::" + stmt.toString());

			rs = stmt.executeQuery();

			userObj = new User();

			while (rs.next()) {
				userObj.setId(rs.getInt("id"));
				userObj.setUsername(rs.getString("username"));
				userObj.setEncryptedPassword(rs.getBytes("encrypted_password"));
				userObj.setPasswordSalt(rs.getBytes("password_salt"));
				userObj.setGcmId(rs.getString("gcm_id"));
				userObj.setDeviceType(rs.getString("device"));
				userObj.getRoles().add(rs.getString("role_name"));
				userObj.setPlainPassword(rs.getString("plain_password"));
			}

		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

		return userObj;
	}

	@Override
	public UserChatProfile getUserChatProfileDetails(Integer userId) {

		String query = "select * from user_chat_profile where user_id=?";
		Object[] inputs = new Object[] { userId };
		List<UserChatProfile> userChatProfileList = getJdbcTemplate().query(query, inputs,
						new RowMapper() {
							@Override
							public Object mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								return new UserChatProfile(
												resultSet.getString("login"),
												resultSet.getString("password"),
												resultSet.getLong("chat_user_profile_id"));
							}
						});

		UserChatProfile userChatProfileobj = null;

		if (userChatProfileList != null && userChatProfileList.size() > 0) {
			userChatProfileobj = userChatProfileList.get(0);
		}

		return userChatProfileobj;
	}

	@Override
	public UserForumProfile getUserForumProfileDetails(Integer userId) {

		String query = "select * from user_forum_profile where user_id=?";
		Object[] inputs = new Object[] { userId };
		List<UserForumProfile> userForumProfileList = getJdbcTemplate().query(query, inputs,
						new RowMapper<UserForumProfile>() {
							@Override
							public UserForumProfile mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								return new UserForumProfile(
												resultSet.getString("login"),
												resultSet.getString("password"),
												resultSet.getLong("forum_profile_id"));
							}
						});

		UserForumProfile userForumProfileObj = null;

		if (userForumProfileList != null && userForumProfileList.size() > 0) {
			userForumProfileObj = userForumProfileList.get(0);
		}

		return userForumProfileObj;
	}

	@Override
	public UserChatProfile getUserChatProfileDetailsbyParentId(Integer parentId) {

		String query = "select * from user_chat_profile where user_id= (select user_id from parent where id =?)";
		Object[] inputs = new Object[] { parentId };
		List<UserChatProfile> userChatProfileList = getJdbcTemplate().query(query, inputs,
						new RowMapper() {
							@Override
							public Object mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								return new UserChatProfile(
												resultSet.getString("login"),
												resultSet.getString("password"),
												resultSet.getLong("chat_user_profile_id"));
							}
						});

		UserChatProfile userChatProfileobj = null;

		if (userChatProfileList != null && userChatProfileList.size() > 0) {
			userChatProfileobj = userChatProfileList.get(0);
		}

		return userChatProfileobj;
	}

	@Override
	public void updateGroupUser(Integer userId, Integer groupId, boolean isSuggestedGroup) {

		SimpleJdbcInsert insertUser =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("group_user")
										.usingColumns("group_id", "user_id", "is_suggsted_group");

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("group_id", groupId);
		parameters.put("user_id", userId);
		parameters.put("is_suggsted_group", isSuggestedGroup ? "Y" : "N");

		insertUser.execute(parameters);
	}

	@Override
	public void updateGroupIndividual(Integer userId, Integer groupId, Integer roleId) {

		SimpleJdbcInsert insertGroupIndividual =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("group_individual")
										.usingColumns("group_id", "user_id", "role_id");

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("group_id", groupId);
		parameters.put("user_id", userId);
		parameters.put("role_id", roleId);

		insertGroupIndividual.execute(parameters);
	}

	@Override
	public OtpResponse generateOTP(String email, String primaryContactNumber, boolean checkUserExists) {

		OtpResponse otpResponse = new OtpResponse();
		String query = "";
		Object[] inputs = null;
		String otp = null;
		if (primaryContactNumber != null) {
			if (checkUserExists && !isUserExist(email)) {
				throw new ApplicationException("Can not process request due to security reasons.");
			}

			String queryForOtp = "select * from otp where primaryContact=?";
			Object[] inputsParam = new Object[] { primaryContactNumber };

			List<ParentOtp> parentOtpList = getJdbcTemplate()
							.query(queryForOtp, inputsParam,
											new BeanPropertyRowMapper<ParentOtp>(
															ParentOtp.class));

			otp = RandomStringUtils.random(6, true, true).toUpperCase();

			if (parentOtpList.size() == 0) {

				query = "insert into otp(primaryContact,otp,active) values(?,?,?)";
				inputs = new Object[] { primaryContactNumber,
								otp, 'Y' };
				int[] types = new int[] { Types.VARCHAR, Types.VARCHAR,
								Types.CHAR };
				try {
					getJdbcTemplate().update(query, inputs, types);
					otpResponse.setMessage("" + otp);
					otpResponse.setCode("Success");
				} catch (Exception e) {
					otpResponse.setMessage("Service not available");
					otpResponse.setCode("Error");
				}
			} else if (parentOtpList.size() == 1) {
				query = "update otp set otp = ?, active='Y' where primaryContact = ?";
				inputs = new Object[] { otp, primaryContactNumber };
				int[] types = new int[] { Types.VARCHAR, Types.VARCHAR };
				try {
					getJdbcTemplate().update(query, inputs, types);
					otpResponse.setMessage("" + otp);
					otpResponse.setCode("Success");
				} catch (Exception e) {
					otpResponse.setMessage("Service not available");
					otpResponse.setCode("Error");
				}

			}
		}
		return otpResponse;
	}

	@Override
	public boolean isEmailAvailable(String email) {

		boolean result = false;
		if (email != null) {
			String queryForOtp = "select * from parent where email=?";
			Object[] inputsParam = new Object[] { email };

			List<ParentOtp> parentOtpList = getJdbcTemplate()
							.query(queryForOtp, inputsParam,
											new BeanPropertyRowMapper<ParentOtp>(
															ParentOtp.class));

			if (parentOtpList.size() == 0) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public boolean updateUserGCM(Integer userId, String gcmId, String deviceType) {
		boolean result = false;
		Connection conn = null;
		String query = "update user set gcm_id = ?, device=? where id = ?";
		try {
			conn = getDataSource().getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, gcmId);
			ps.setString(2, deviceType);
			ps.setLong(3, userId);
			if (ps.executeUpdate() == 0) {
				throw new RuntimeException("User does not exist.");
			}

			ps.close();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return result;
	}

	@Override
	public boolean verifyOTP(String primaryContactNumber, String otp) {

		if (primaryContactNumber != null) {
			String query = "update otp set active = 'N' where primaryContact = ? and otp = ? and active = 'Y'";
			log.debug("Parameters : primaryContactNumber" + primaryContactNumber);
			log.debug("Parameters : otp" + otp);
			Object[] inputsParam = new Object[] { primaryContactNumber, otp };

			int UPDATE_STATUS = getJdbcTemplate().update(query, inputsParam);

			log.debug("UPDATE_STATUS:" + UPDATE_STATUS);
			if (UPDATE_STATUS == 1) {
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean verifyOTP(String otp) {

		if (otp != null) {
			String query = "update otp set active = 'N' where otp = ? and active = 'Y'";
			log.debug("Parameters : otp" + otp);
			Object[] inputsParam = new Object[] { otp };

			int UPDATE_STATUS = getJdbcTemplate().update(query, inputsParam);

			log.debug("UPDATE_STATUS:" + UPDATE_STATUS);
			if (UPDATE_STATUS == 1) {
				return true;
			}
		}

		return false;
	}

	/**
	 * username is the parentUserId
	 */
	@Override
	public boolean isUserExist(String username) {

		log.debug("check user already exists");
		String query = "select * from user where userName=? ";
		Object[] inputs = new Object[] { username };
		List<User> userList = getJdbcTemplate().query(query, inputs,
						new BeanPropertyRowMapper<User>(User.class));

		if (userList != null && userList.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isEntityExist(String email, String mobile) {

		log.debug("check user already exists :: email= " + email + ", phone = " + mobile);

		if (StringUtils.isEmpty(email) && StringUtils.isEmpty(mobile)) {
			return false;
		}

		String query = "select count(*) as count from contact where email = ? OR phone = ?";
		Object[] inputs = new Object[] { email, mobile };

		if (!StringUtils.isEmpty(email)) {
			query = "select count(*) as count from contact where email = ?";
			inputs = new Object[] { email };
		}
		else {
			query = "select count(*) as count from contact where phone = ?";
			inputs = new Object[] { mobile };
		}

		Integer count = getJdbcTemplate().queryForObject(query, inputs, Integer.class);

		if (count > 0) {
			return true;
		}
		return false;
	}

	@Override
	public User getUserByEmail(String email) {
		User user = null;
		log.debug("Get user by email");
		String query = "select * from user where userName=? and active='Y' and isDelete = 'N'";
		Object[] inputs = new Object[] { email };
		List<User> userList = getJdbcTemplate().query(query, inputs,
						new BeanPropertyRowMapper<User>(User.class));

		if (userList != null && userList.size() == 1) {
			user = userList.get(0);
		}

		return user;
	}

	@Override
	public User getUserByMobile(String mobile) {
		User user = null;
		log.debug("Get user by mobile");
		String query = "select * from user where id = (select user_id from contact where phone = ? ) AND active='Y' AND isDelete = 'N'";
		Object[] inputs = new Object[] { mobile };
		List<User> userList = getJdbcTemplate().query(query, inputs,
						new BeanPropertyRowMapper<User>(User.class));

		if (userList != null && userList.size() == 1) {
			user = userList.get(0);
		}

		return user;
	}

	@Override
	public User getUserByIdAndRole(Integer userId, String role) {
		log.debug("Get user by id");

		String query = "select u.id as id, u.username as username, u.password as encrypted_password, u.password_salt as password_salt, u.gcm_id as gcm_id, u.device as device, r.name as role_name "
						+ " from user u inner join user_role ur on ur.user_id = u.id" +
						"   inner join role r on  r.id = ur.role_id and ur.role_id= ? "
						+ " where u.id=? and u.active='Y' and u.isDelete = 'N' ";
		Object[] inputs = new Object[] { Identity.ROLES_ID.getRoleID(role), userId };
		return getJdbcTemplate().queryForObject(query, inputs,
						new RowMapper<User>() {

							@Override
							public User mapRow(ResultSet rs, int arg1) throws SQLException {
								User userObj = new User();
								userObj.setId(rs.getInt("id"));
								userObj.setUsername(rs.getString("username"));
								userObj.setEncryptedPassword(rs.getBytes("encrypted_password"));
								userObj.setPasswordSalt(rs.getBytes("password_salt"));
								userObj.setGcmId(rs.getString("gcm_id"));
								userObj.setDeviceType(rs.getString("device"));
								userObj.getRoles().add(rs.getString("role_name"));
								return userObj;
							}

						});

	}

	@Override
	public Long addEntryToPassbook(String selfExpenseFlag, float totalAmount, Integer initiatorUserId, String description, Integer receivingEntityUserId,
					String receivingEntityName, String transactionType)
					throws ParseException {
		Passbook passbook = new Passbook();
		passbook.setTitle(description);
		passbook.setDescription(description);
		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");
		passbook.setDate(displayFormat.format(new java.util.Date()));
		passbook.setSelfExpenseFlag(selfExpenseFlag);
		passbook.setTransactionType(transactionType);
		passbook.setAmount(totalAmount);

		if (receivingEntityUserId != null || receivingEntityUserId != 0) {
			passbook.setForEntityName(receivingEntityName);
			passbook.setForEntityUserId(receivingEntityUserId);
		}

		return addEntryToPassbook(initiatorUserId, passbook);
	}

	@Override
	@Transactional(readOnly = false)
	public Long addEntryToPassbook(Integer userId, Passbook passbook) throws ParseException {
		return addEntryToPassbook(userId, passbook, null);
	}

	private Long addEntryToPassbook(Integer userId, Passbook passbook, Integer parentTransactionId) throws ParseException {
		log.debug("Passbook request received for :::" + passbook.toString());
		Long passBookEntryId = null;

		SimpleJdbcInsert insertPassbook =
						new SimpleJdbcInsert(getJdbcTemplate().getDataSource())
										.withTableName("passbook")
										.usingColumns("title", "description", "date", "is_debit", "is_credit", "amount", "user_id", "parent_transaction_id",
														"for_entity_name", "created_date", "for_entity_user_id")
										.usingGeneratedKeyColumns("id");

		Map<String, Object> parameters = new HashMap<String, Object>(19);
		parameters.put("title", passbook.getTitle());
		parameters.put("description", passbook.getDescription());

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date transactionDateParsed = format.parse(passbook.getDate());
		parameters.put("date", new java.sql.Date(transactionDateParsed.getTime()));
		parameters.put("is_debit", "DEBIT".equalsIgnoreCase(passbook.getTransactionType()) ? "Y" : "N");
		parameters.put("is_credit", "CREDIT".equalsIgnoreCase(passbook.getTransactionType()) ? "Y" : "N");
		parameters.put("amount", passbook.getAmount());
		parameters.put("user_id", userId);
		parameters.put("parent_transaction_id", parentTransactionId);
		parameters.put("created_date", new java.sql.Date(System.currentTimeMillis()));
		parameters.put("for_entity_user_id", userId);
		// Default entoty nae is set as self payment
		parameters.put("for_entity_name", "Self");

		if (passbook.getOtherDetails() != null && passbook.getOtherDetails().get("id") != null) {
			Passbook passBookEntryForReceivingUser = new Passbook();
			passBookEntryForReceivingUser.setTitle(passbook.getTitle());
			passBookEntryForReceivingUser.setDescription(passbook.getDescription());
			passBookEntryForReceivingUser.setDate(passbook.getDate());
			passBookEntryForReceivingUser.setTransactionType(("DEBIT".equalsIgnoreCase(passbook.getTransactionType())) ? "CREDIT" : "DEBIT");
			passBookEntryForReceivingUser.setAmount(passbook.getAmount());
			Student student = null;
			// Get student user id.
			if (passbook.getReceivingEntityId() != null) {
				student = studentDAO.getUploadedStudentById(passbook.getReceivingEntityId().longValue());
				parameters.put("for_entity_name", student.getPersonalDetails().getName());
				parameters.put("for_entity_user_id", student.getUserId());
			} else {
				parameters.put("for_entity_name", passbook.getTitle());
			}

			passBookEntryId = (Long) insertPassbook.executeAndReturnKey(parameters);

			if ("no".equalsIgnoreCase(passbook.getSelfExpenseFlag())) {
				addEntryToPassbook(student.getUserId().intValue(), passBookEntryForReceivingUser, passBookEntryId.intValue());
			}
		} else {

			// If passbook request is coming from payment flow, check if payment
			// is done for student and set below field values
			if (passbook.getForEntityUserId() != null) {
				parameters.put("for_entity_user_id", passbook.getForEntityUserId());
				parameters.put("for_entity_name", passbook.getForEntityName());
			}

			passBookEntryId = (Long) insertPassbook.executeAndReturnKey(parameters);
		}

		return passBookEntryId;

	}

	@Override
	public List<Passbook> getPassbookDetails(Integer userId, long lastSyncTimeStamp) {
		String retrivePassbookQuery = "select id, title, DATE_FORMAT(date, '%Y-%m-%d') as transaction_date, is_debit, is_credit, amount, for_entity_name  " +
						" from passbook where user_id = :userId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);

		if (lastSyncTimeStamp != 0) {
			java.sql.Date lastSyncDate = new java.sql.Date(lastSyncTimeStamp);
			retrivePassbookQuery += " and (created_date > :lastSyncDate or (updated_date IS NOT NULL && updated_date > :lastSyncDate)) ";
			queryParameters.put("lastSyncDate", lastSyncDate);
		}

		retrivePassbookQuery += " order by date desc";
		log.debug("Retrive passbook query :: " + retrivePassbookQuery);
		List<Passbook> passbookDetails = getNamedParameterJdbcTemplate().query(retrivePassbookQuery, queryParameters,
						new RowMapper<Passbook>() {
							@Override
							public Passbook mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								Passbook passbook = new Passbook();
								passbook.setId(resultSet.getInt("id"));
								passbook.setIdentifier("" + passbook.getId());
								passbook.setTitle(resultSet.getString("title"));
								passbook.setDate(resultSet.getString("transaction_date"));
								String transactionType = resultSet.getString("is_debit").equals("Y") ? "DEBIT" : "CREDIT";
								passbook.setTransactionType(transactionType);
								passbook.setAmount(resultSet.getFloat("amount"));
								passbook.setForEntityName(resultSet.getString("for_entity_name"));
								passbook.setOtherDetails(new HashMap<String, Object>());
								return passbook;
							}
						});

		return passbookDetails;
	}

	@Override
	public boolean canUserAccessSpecifiedGroup(Integer userId, List<Integer> groupIds) {
		String query = "select group_id from group_user where user_id=:userId and group_id in(:groupIds)";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userId", userId);
		parameterSource.addValue("groupIds", groupIds);

		List<Integer> groupIdList = getNamedParameterJdbcTemplate().queryForList(query, parameterSource, Integer.class);
		log.debug("Groups size :: " + groupIdList.size());
		return (groupIdList.size() == groupIds.size()) ? true : false;

	}

	@Override
	public boolean canUserAccessSpecifiedUserProfiles(Integer userId, Integer branchId, List<Integer> userIds) {
		String query = "select u.id from user u inner join student s on s.user_id = u.id where u.id in (:userIds) and s.branch_id = :branchId";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userIds", userIds);
		parameterSource.addValue("branchId", branchId);

		List<Integer> availableUserIdList = getNamedParameterJdbcTemplate().queryForList(query, parameterSource, Integer.class);

		return (availableUserIdList.size() == userIds.size()) ? true : false;
	}

	@Override
	public List<Group> getAssignedGroups(Integer userId, String searchText, String exludeIds, String role, Integer branchId) {

		String query = "select g.id as id, g.name as name from `group` g inner join group_user gu on gu.group_id = g.id " +
						" where g.active = 'Y' AND g.isDelete = 'N' and gu.user_id=?";

		Object[] inputs = new Object[] { userId };

		//Override in case of school admin based on branch.
		if (Identity.ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(role) || Identity.ROLES.ADMIN.get().equalsIgnoreCase(role)) {
			query = "select g.id as id, g.name as name from `group` g where g.active = 'Y' AND g.isDelete = 'N' AND g.branch_id = ? ";
			inputs = new Object[] { branchId };
		}

		if (!StringUtils.isEmpty(searchText)) {
			query += " and g.name like ('%" + searchText + "%') ";
		}

		if (!StringUtils.isEmpty(exludeIds)) {
			query += " and g.id not in (" + exludeIds + ")";
		}

		log.debug("Retrive getAssignedGroups query :: " + query);

		List<Group> groupIdList = getJdbcTemplate().query(query, inputs, new RowMapper<Group>() {

			@Override
			public Group mapRow(ResultSet rs, int rownumber) throws SQLException {
				return new Group(rs.getLong("id"), rs.getString("name"));
			}
		});
		return groupIdList;
	}

	@Override
	public Map<String, List<String>> getGcmIdsOfGroupUsers(List<Integer> groupIds) {
		final Map<String, List<String>> map = new HashMap<String, List<String>>();

		String query = "select u.gcm_id, u.device from user u inner join group_user gu on u.id = gu.user_id " +
						" where gu.group_id in (:groupIds)";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("groupIds", groupIds);

		getNamedParameterJdbcTemplate().query(query, parameterSource, new RowMapper<String>() {

			@Override
			public String mapRow(ResultSet rs, int rownumber) throws SQLException {

				String device = rs.getString("device");
				List<String> gcmIds = null;

				if (map.containsKey(device)) {
					gcmIds = map.get(device);
				} else {
					gcmIds = new ArrayList<String>();
				}

				gcmIds.add(rs.getString("gcm_id"));
				map.put(device, gcmIds);
				return null;

			}
		});

		return map;

	}

	@Override
	public Map<String, List<String>> getGcmIdsOfUsers(List<Integer> userIds) {
		final Map<String, List<String>> map = new HashMap<String, List<String>>();

		String query = "select u.gcm_id, u.device from user u where id in(:userIds)";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userIds", userIds);

		getNamedParameterJdbcTemplate().query(query, parameterSource, new RowMapper<String>() {

			@Override
			public String mapRow(ResultSet rs, int rownumber) throws SQLException {

				String device = rs.getString("device");
				List<String> gcmIds = null;

				if (map.containsKey(device)) {
					gcmIds = map.get(device);
				} else {
					gcmIds = new ArrayList<String>();
				}

				gcmIds.add(rs.getString("gcm_id"));
				map.put(device, gcmIds);
				return null;

			}
		});

		return map;

	}

	@Override
	public Set<Integer> getGroupUsers(List<Integer> groupIds, boolean excludeParentRole, boolean excludeTeacherRole) {

		String query = "select gu.user_id from group_user gu ";

		if (excludeParentRole || excludeTeacherRole) {
			query += "inner join user_role ur on ur.user_id= gu.user_id ";
		}

		if (excludeParentRole) {
			query += " and ur.role_id !=3 ";
		}

		if (excludeTeacherRole) {
			query += " and ur.role_id !=4 ";
		}

		query += " where group_id in (:groupIds) ";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("groupIds", groupIds);

		List<Integer> groupUserIdList = getNamedParameterJdbcTemplate().queryForList(query, parameterSource, Integer.class);

		return new HashSet<Integer>(groupUserIdList);
	}

	@Override
	public Set<Integer> getGroupUsers(List<Integer> groupIds, boolean excludeParentRole) {

		return getGroupUsers(groupIds, excludeParentRole, false);
	}

	@Override
	public List<BranchStudentParentDetail> getBranchStudentParentDetailByStudent(List<Integer> studentUserId, Integer branchId) {

		String query = " select p.id as parent_id, p.user_id as parent_user_id, s.user_id as student_user_id, s.id as student_id, bsp.branch_id as branch_id "
						+ " from branch_student_parent bsp "
						+ " inner join parent p on p.id = bsp.parent_id "
						+ " inner join student s on s.id = bsp.student_id "
						+ " where bsp.isDelete = 'N' and bsp.branch_id =:branchId "
						+ " and bsp.student_id in (select id from student where user_id in (:studentUserId)) "
						+ " group by bsp.parent_id, bsp.student_id, bsp.branch_id";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("studentUserId", studentUserId);
		parameterSource.addValue("branchId", branchId);
		return getNamedParameterJdbcTemplate().query(query, parameterSource,
						new ResultSetExtractor<List<BranchStudentParentDetail>>() {
							@Override
							public List<BranchStudentParentDetail> extractData(ResultSet rs) throws SQLException, DataAccessException {
								List<BranchStudentParentDetail> list = new ArrayList<BranchStudentParentDetail>();

								while (rs.next()) {

									list.add(new BranchStudentParentDetail(rs.getInt("branch_id"), rs.getInt("parent_id"), rs.getInt("student_id"),
													rs.getInt("parent_user_id"), rs.getInt("student_user_id")));
								}
								return list;
							}
						});

	}

	@Override
	public List<BranchStudentParentDetail> getBranchStudentParentDetailByStudentGroup(List<Integer> groupIds, Integer branchId) {
		String query = " select bsp.parent_id as parent_id, p.user_id as parent_user_id, s.user_id as student_user_id, bsp.student_id as student_id, bsp.branch_id as branch_id "
						+ " from branch_student_parent bsp "
						+ " inner join parent p on p.id = bsp.parent_id "
						+ " inner join student s on s.id = bsp.student_id "
						+ " where bsp.isDelete = 'N' and bsp.branch_id =:branchId "
						+ " and bsp.student_id in (select distinct student.id from group_user inner join student on student.user_id = group_user.user_id where group_id in (:groupIds)) "
						+ " group by bsp.parent_id, bsp.student_id, bsp.branch_id";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("groupIds", groupIds);
		parameterSource.addValue("branchId", branchId);

		return getNamedParameterJdbcTemplate().query(query, parameterSource, new ResultSetExtractor<List<BranchStudentParentDetail>>() {
			@Override
			public List<BranchStudentParentDetail> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<BranchStudentParentDetail> list = new ArrayList<BranchStudentParentDetail>();

				while (rs.next()) {

					list.add(new BranchStudentParentDetail(rs.getInt("branch_id"), rs.getInt("parent_id"), rs.getInt("student_id"),
									rs.getInt("parent_user_id"), rs.getInt("student_user_id")));
				}
				return list;
			}
		});

	}

	@Override
	public PaymentAccount getPaymentAccount(Integer userId) {
		String query = "select * from user_payment_account where user_id = :userId and account_type='" + Constant.PaymentAccountType.QFIXPAY.get() + "'";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userId", userId);

		return getNamedParameterJdbcTemplate().queryForObject(query, parameterSource, new RowMapper<PaymentAccount>() {

			@Override
			public PaymentAccount mapRow(ResultSet rs, int arg1) throws SQLException {
				return new PaymentAccount(rs.getInt("user_id"), rs.getString("account_type"), rs.getLong("account_number"));
			}

		});

	}

	@Override
	public boolean changePassword(Integer userId, ChangePassword changePassword) throws NoSuchAlgorithmException, InvalidKeySpecException,
					InvalidLoginCredentialsException {

		String query = "select  u.username as username, u.temp_password as plain_password, u.password as encrypted_password, u.password_salt as password_salt from user u "
						+ " where u.id=:userId and u.active='Y' and u.isDelete = 'N' ";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userId", userId);

		User userObjInDb = getNamedParameterJdbcTemplate().queryForObject(query, parameterSource, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int arg1) throws SQLException {
				User userObj = new User();
				userObj.setUsername(rs.getString("username"));
				userObj.setEncryptedPassword(rs.getBytes("encrypted_password"));
				userObj.setPasswordSalt(rs.getBytes("password_salt"));
				userObj.setPlainPassword(rs.getString("plain_password"));
				return userObj;
			}

		});

		verifyPassword(changePassword.getCurrentPassword(), userObjInDb);

		query = "update user set password = :password, password_salt= :password_salt, temp_password= :temp_password where id = :userId and active = 'Y'";

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("userId", userId);
		int UPDATE_STATUS = updateUserCredential(parameters, changePassword.getNewPassword(), query);

		if (UPDATE_STATUS == 1) {

			return true;
		}
		return false;
	}

	private int updateUserCredential(Map<String, Object> parameters, String newPassword, String query) throws NoSuchAlgorithmException {
		byte[] randomSalt = SecurityUtil.generateSalt();
		byte[] passwordHash = SecurityUtil.hashPassword(newPassword, randomSalt);

		parameters.put("password", passwordHash);
		parameters.put("password_salt", randomSalt);
		parameters.put("temp_password", newPassword);

		return getNamedParameterJdbcTemplate().update(query, parameters);
	}

	@Override
	@Transactional(readOnly = false)
	public void saveForgotPasswordOtp(String username, String email) {
		String invalidateExistingOtpQuery = "update forgot_password_otp set active = 'N' where username = ?";
		Object[] inputsParam = new Object[] { username };

		int UPDATE_STATUS = getJdbcTemplate().update(invalidateExistingOtpQuery, inputsParam);

		log.debug("Forgot password Invalidated existing password." + UPDATE_STATUS);

		String otp = RandomStringUtils.random(6, false, true).toUpperCase();
		SimpleJdbcInsert insertForgotPasswordOtp =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("forgot_password_otp")
										.usingColumns("username", "otp", "expire_on", "active")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(4);
		parameters.put("username", username);
		parameters.put("otp", otp);
		parameters.put("expire_on", new Date(System.currentTimeMillis() + 5 * 60 * 1000));
		parameters.put("active", "Y");
		insertForgotPasswordOtp.execute(parameters);
		log.debug("Forgot password Otp generated.");
		log.info("Sending email to user");

		Contact contact = getContactDetails(username);
		Map<String, Object> content = new HashMap<String, Object>();
		content.put("OTP", otp);
		content.put("first_name", contact.getFirstName());

		notificationClient.sendEmail(contact.getEmail(), EmailTemplate.FORGOT_PASSWORD_EMAIL.get(), content);
		notificationClient.sendSMS(contact.getPhone(), SmsTemplate.FORGOT_PASSWORD_SMS.get(), content);
	}

	@Override
	@Transactional(readOnly = false)
	public boolean resetPassword(String otp, String newPassword) throws NoSuchAlgorithmException {
		boolean passwordResetFlag = false;

		String invalidateExistingOtpQuery = "update forgot_password_otp set active = 'N' where otp = ? and active = 'Y' and expire_on > now()";
		Object[] inputsParam = new Object[] { otp };

		int UPDATE_STATUS = getJdbcTemplate().update(invalidateExistingOtpQuery, inputsParam);

		if (UPDATE_STATUS == 1) {
			String updateUserQuery = "update user set password = :password, password_salt= :password_salt, temp_password= :temp_password where username = (select username from forgot_password_otp where otp = :otp) and active = 'Y' ";
			Map<String, Object> parameters = new HashMap<String, Object>(3);
			parameters.put("otp", otp);
			UPDATE_STATUS = updateUserCredential(parameters, newPassword, updateUserQuery);
			if (UPDATE_STATUS == 1) {
				passwordResetFlag = true;
			}
		}

		return passwordResetFlag;
	}

	@Override
	public List<GroupUser> retrieveUnassignedChatGroupsForParents(Integer parentId, Integer parentUserId, Integer studentId) {

		String query = " select distinct g.id as group_id, g.chat_dialogue_id as chat_dialogue_id " +
						" from `group` g inner join group_user gusr on gusr.group_id = g.id " +
						" where gusr.user_id in " +
						" (select student.user_id from student inner join branch_student_parent bsp on bsp.student_id = student.id " +
						" where bsp.parent_id= :parentId and bsp.student_id= :studentId) " +
						" and NOT EXISTS (select 1 from group_user gusr where gusr.group_id = g.id and gusr.user_id = :parentUserId)";

		Map<String, Integer> values = new HashMap<String, Integer>();
		values.put("parentUserId", parentUserId);
		values.put("studentId", studentId);
		values.put("parentId", parentId);

		return retrieveGroups(query, values);
	}

	@Override
	public List<String> retrieveUnassignedForumsForParents(Integer parentId, Integer parentUserId, Integer studentId) {

		String query = " select distinct gf.forum_group_id as forum_group_id " +
						" from `group` g inner join group_user gusr on gusr.group_id = g.id " +
						" left join group_forum gf on gf.group_id = g.id " +
						" where gusr.user_id in " +
						" (select student.user_id from student inner join branch_student_parent bsp on bsp.student_id = student.id " +
						" where bsp.parent_id= :parentId and bsp.student_id= :studentId) " +
						" and NOT EXISTS (select 1 from group_user gusr where gusr.group_id = g.id and gusr.user_id = :parentUserId)";

		Map<String, Integer> values = new HashMap<String, Integer>();
		values.put("parentUserId", parentUserId);
		values.put("studentId", studentId);
		values.put("parentId", parentId);

		SqlParameterSource namedParameters = new MapSqlParameterSource(values);

		return getNamedParameterJdbcTemplate().query(query,
						namedParameters, new RowMapper<String>() {
							@Override
							public String mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								return resultSet.getString("forum_group_id");
							}
						});
	}

	private List<GroupUser> retrieveGroups(String query, Map<String, ?> values) {

		SqlParameterSource namedParameters = new MapSqlParameterSource(values);

		return getNamedParameterJdbcTemplate().query(query,
						namedParameters, new RowMapper<GroupUser>() {
							@Override
							public GroupUser mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								return new GroupUser(
												resultSet.getString("chat_dialogue_id"),
												new Integer(resultSet.getInt("group_id")));
							}
						});
	}

	private Contact getContactDetails(String username) {
		String query = " select * from contact inner join user on user.id = contact.user_id where username=:username";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("username", username);
		return getNamedParameterJdbcTemplate().queryForObject(query,
						parameterSource, new RowMapper<Contact>() {
							@Override
							public Contact mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new Contact(rs.getString("firstname"), rs.getString("lastname"), rs.getString("city"), rs.getString("photo"),
												new Integer(rs.getInt("user_id")), rs.getString("email"), rs.getString("phone"));
							}
						});

	}

	@Override
	public void removeEntryFromPassbook(List<Integer> passbookId) {
		String query = "delete from passbook where id in (:passbookId)";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("passbookId", passbookId);
		getNamedParameterJdbcTemplate().update(query, parameterSource);
	}

	@Override
	public Passbook getPassbookDetails(Integer passbookId) {
		String retrivePassbookQuery = "select id, title, DATE_FORMAT(date, '%Y-%m-%d') as transaction_date, is_debit, is_credit, amount, for_entity_name, parent_transaction_id  from passbook where id = :passbookId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("passbookId", passbookId);

		log.debug("Retrive passbook query :: " + retrivePassbookQuery);
		Passbook passbookDetails = getNamedParameterJdbcTemplate().queryForObject(retrivePassbookQuery, queryParameters,
						new RowMapper<Passbook>() {
							@Override
							public Passbook mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								Passbook passbook = new Passbook();
								passbook.setId(resultSet.getInt("id"));
								passbook.setIdentifier("" + passbook.getId());
								passbook.setTitle(resultSet.getString("title"));
								passbook.setDate(resultSet.getString("transaction_date"));
								String transactionType = resultSet.getString("is_debit").equals("Y") ? "DEBIT" : "CREDIT";
								passbook.setTransactionType(transactionType);
								passbook.setAmount(resultSet.getFloat("amount"));
								passbook.setForEntityName(resultSet.getString("for_entity_name"));
								passbook.setParentTransactionId(resultSet.getInt(("parent_transaction_id")));
								passbook.setOtherDetails(new HashMap<String, Object>());
								return passbook;
							}
						});

		return passbookDetails;
	}

	@Override
	public List<UserDetail> searchIndividualUsers(Integer userId, String searchText, String excludeIds, String role, Integer branchId, Integer entityId) {
		String query = "select c.user_id as id, CONCAT_WS(' ',firstname,lastname) as name, r.name as role " +
						" from contact c " +
						" inner join user_role ur on ur.user_id= c.user_id and ur.role_id in (2,4) " +
						" inner join role r on r.id= ur.role_id " +
						" where (c.firstname like (:searchText) or c.lastname like (:searchText) or CONCAT_WS(' ',firstname,lastname) like (:searchText)) " +
						" and c.user_id != :userId ";

		//Override in case of school admin based on branch.
		if (Identity.ROLES.SCHOOL_ADMIN.get().equalsIgnoreCase(role)) {
			query += " and c.user_id= "
							+
							" ( CASE "
							+
							" WHEN r.id=2 "
							+
							" THEN (select student.user_id from student where student.branch_id = :branchId and student.user_id=c.user_id and student.isDelete = 'N') "
							+
							" ELSE  (select teacher.user_id from teacher where teacher.branch_id = :branchId and teacher.user_id=c.user_id and teacher.isDelete = 'N') "
							+
							" END " +
							" ) ";
		} else if (Identity.ROLES.TEACHER.get().equalsIgnoreCase(role)) {
			query += " and c.user_id= "
							+ " ( CASE "
							+ " WHEN r.id=2 "
							+ " THEN (select distinct s.user_id from student s inner join teacher_standard ts on s.standard_id= ts.standard_id and s.division_id = ts.division_id where s.branch_id = :branchId and s.user_id=c.user_id and ts.teacher_id=:entityId and s.isDelete = 'N' and ts.is_delete='N') "
							+ " ELSE  (select distinct teacher.user_id from teacher where teacher.branch_id = :branchId and teacher.user_id=c.user_id and teacher.isDelete = 'N') "
							+ " END "
							+ " ) ";
		} else if (Identity.ROLES.ADMIN.get().equalsIgnoreCase(role)) {
			query += " and c.user_id= "
							+ " ( CASE "
							+ " WHEN r.id=2 "
							+ " THEN (select distinct s.user_id from student s where s.branch_id = :branchId and s.user_id=c.user_id and s.isDelete = 'N') "
							+ " ELSE  (select distinct teacher.user_id from teacher where teacher.branch_id = :branchId and teacher.user_id=c.user_id and teacher.isDelete = 'N') "
							+ " END "
							+ " ) ";
		}

		if (!StringUtils.isEmpty(excludeIds)) {
			query += " and c.user_id not in (:excludeIds)";
		}

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);
		queryParameters.put("searchText", "%" + searchText + "%");
		queryParameters.put("excludeIds", excludeIds);
		queryParameters.put("branchId", branchId);
		queryParameters.put("entityId", entityId);

		log.debug("Retrive searchIndividualUsers query :: " + query);
		log.debug("Retrive searchIndividualUsers queryParameters :: " + queryParameters);

		List<UserDetail> userDetails = getNamedParameterJdbcTemplate().query(query, queryParameters,
						new RowMapper<UserDetail>() {
							@Override
							public UserDetail mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								UserDetail userDetail = new UserDetail();
								userDetail.setId(resultSet.getInt("id"));
								userDetail.setName(resultSet.getString("name"));
								userDetail.setRole(resultSet.getString("role"));

								return userDetail;
							}
						});

		return userDetails;
	}

	@Override
	public boolean canUserAccessSpecifiedBranch(Integer userId, Integer branchId) {

		try {
			//	String query = "select b.* from branch b inner join user u on u.institute_id=b.institute_id where u.id=:userId and b.id=:branchId ";
			String query = "select * from branch_admin where user_id = :userId AND branch_id = :branchId AND is_delete = 'N'";

			Map<String, Object> queryParameters = new HashMap<String, Object>();
			queryParameters.put("userId", userId);
			queryParameters.put("branchId", branchId);

			getNamedParameterJdbcTemplate().queryForObject(query, queryParameters,
							new RowMapper<Integer>() {
								@Override
								public Integer mapRow(ResultSet resultSet, int rowNum)
												throws SQLException {
									return resultSet.getInt("branch_id");
								}
							});

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public Contact getContactDetailsByUserId(Integer userId) {
		String query = " select * from contact inner join user on user.id = contact.user_id where user.id=:userId";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userId", userId);
		return getNamedParameterJdbcTemplate().queryForObject(query,
						parameterSource, new RowMapper<Contact>() {
							@Override
							public Contact mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new Contact(rs.getString("firstname"), rs.getString("lastname"), rs.getString("city"), rs.getString("photo"),
												new Integer(rs.getInt("user_id")), rs.getString("email"), rs.getString("phone"));
							}
						});

	}

	@Override
	public String getLoginCount(User user) {
		logger.info("getLogincount method starts");

		String query = "select count(*) as loginCount "
						+ " from token t inner join user u on u.id=t.created_by " +
						" where u.username=? and u.active='Y' and u.isDelete = 'N'";
		Object[] inputs = new Object[] { user.getUsername() };
		String count = getJdbcTemplate().queryForObject(query, String.class, inputs);

		return count;
	}

	@Override
	public boolean isParentExist(String username) {
		logger.info("isParentExist Method Starts");
		if (StringUtils.isEmpty(username)) {
			return false;
		}

		String query = "select count(*) as count from user where username = ? and isDelete = 'N'";
		Object[] inputs = new Object[] { username };

		Integer count = getJdbcTemplate().queryForObject(query, inputs, Integer.class);

		if (count > 0) {
			return true;
		}
		log.info("isParentExist Method ends");
		return false;
	}

	@Override
	public void logInvalidLoginAttempt(User user) {
		try {
			SimpleJdbcInsert insertInvalidUserLoginAtempts =
							new SimpleJdbcInsert(getDataSource())
											.withTableName("user_invalid_login_audit")
											.usingColumns("username", "password", "device");
			Map<String, Object> parameters = new HashMap<String, Object>(3);
			parameters.put("username", user.getUsername());
			parameters.put("password", user.getPassword());
			parameters.put("device", user.getDeviceType());
			insertInvalidUserLoginAtempts.execute(parameters);
		} catch (Exception e) {
			log.error("", e);
		}
	}
	
	/*Ameya OTP login*/
	
	public boolean checkUserExist(String username){
		if(username.equals("") || username == null)
			return false;
		if(username.contains("\'"))
			return false;
		
		String query = "select count(*) from user where active='Y' and isDelete='N' and username='" + username +"'";
		Integer count = getJdbcTemplate().queryForObject(query, Integer.class);
		log.info("Username exist");
		if(count > 0)
			return true;
		return false;
	}
	
	public Integer getUserIdbyUsername(String username){
		String query = "select id from user where username = '" + username + "'";
		Integer userId = getJdbcTemplate().queryForObject(query, Integer.class);
		return userId;
	}
	
	/*public OTPDetails getOTPDetailsById(Integer id){
		String query = " select * from otp_details where id=:id";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("id", id);
		return getNamedParameterJdbcTemplate().queryForObject(query,
						parameterSource, new RowMapper<OTPDetails>() {
							@Override
							public OTPDetails mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new OTPDetails(rs.getString("firstname"), rs.getString("lastname"), rs.getString("city"), rs.getString("photo"),
												new Integer(rs.getInt("user_id")), rs.getString("email"), rs.getString("phone"));
							}
						});
	}*/
	
	private void deleteOldOTP(String username){
		try{
			String sql = "delete from otp_details where username = '" + username + "'";
			log.info("deleing existing user otp details " + sql);
			getJdbcTemplate().update(sql);
		}catch(Exception e){
			log.error("Error while delete existing otp details for username " + username);
			e.printStackTrace();
		}
	}
	
	public Integer generateLoginOTP(final String username) throws SQLException{
		
		//Integer userId = getUserIdbyUsername(username);
		deleteOldOTP(username);
		final Contact contact = getContactDetails(username);
		if((contact.getPhone() == null || contact.getPhone().equals("")) 
				&& (contact.getEmail() == null || contact.getEmail().equals(""))){
			return null;
		}
		Random random = new Random();
		final Integer random_otp = 100000 + random.nextInt(900000);
		Integer id = 0;
		
		final String insertLoginOtp = "insert into otp_details(username, contact, otp_number, email_address, otp_end_date, active) "
				+ "values(?, ?, ?, ?, ?, ?)";

		KeyHolder holder = new GeneratedKeyHolder();
		Date date = new Date(System.currentTimeMillis() + 5 * 60 * 1000);
		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement stmt = connection.prepareStatement(
						insertLoginOtp, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, username);
				stmt.setString(2, contact.getPhone());
				stmt.setInt(3, random_otp);
				stmt.setString(4, contact.getEmail());
				stmt.setTimestamp(5, new Timestamp(System.currentTimeMillis() + 5 * 60 * 1000));//;(5, new Date(System.currentTimeMillis() + 5 * 60 * 1000));
				stmt.setString(6, "Y");
				return stmt;
			}
		}, holder);
		
		id = holder.getKey().intValue();
		log.info("OTP generated for username"  + username + " is " +  random_otp);
		Map<String, Object> content = new HashMap<String, Object>();
		content.put("OTP", random_otp);
		content.put("first_name", contact.getFirstName());

		String emailtemp = EmailTemplate.GENERATE_OTP_EMAIL.get();
		String smstemp = SmsTemplate.GENERATE_OTP_SMS.get();
		
		notificationClient.sendEmail(contact.getEmail(), EmailTemplate.GENERATE_OTP_EMAIL.get(), content);
		notificationClient.sendSMS(contact.getPhone(), SmsTemplate.GENERATE_OTP_SMS.get(), content);
		return id;	
	}
	
	public OTPDetails getOTPDetails(Integer id, String username){
		String query = " select * from otp_details where username=:username and id=:id";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("username", username);
		parameterSource.addValue("id", id);
		return getNamedParameterJdbcTemplate().queryForObject(query,
						parameterSource, new RowMapper<OTPDetails>() {
							@Override
							public OTPDetails mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new OTPDetails(rs.getInt("id"), rs.getString("username"), rs.getString("contact"), rs.getInt("otp_number"),
												rs.getString("email_address"), rs.getString("otp_creation_date"), rs.getString("otp_end_date"), rs.getString("active"));
							}
						});
	}
	
	public OTPDetails getOTPDetails(String username, Integer otpNumber){
		OTPDetails otpDetails = null;
		String query = " select * from otp_details where username=:username and otp_number=:otpNumber";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("username", username);
		parameterSource.addValue("otpNumber", otpNumber);
		try{
			otpDetails = getNamedParameterJdbcTemplate().queryForObject(query,
					parameterSource, new RowMapper<OTPDetails>() {
				@Override
				public OTPDetails mapRow(ResultSet rs, int rowNum)
								throws SQLException {
					return new OTPDetails(rs.getInt("id"), rs.getString("username"), rs.getString("contact"), rs.getInt("otp_number"),
									rs.getString("email_address"), rs.getString("otp_creation_date"), rs.getString("otp_end_date"), rs.getString("active"));
				}
			});
		}catch(Exception e){
			log.error("No otp details exist " + e.getMessage());
			return otpDetails;
		}
		return otpDetails;
	}



	@Override
	public String getMaskedContact(OTPDetails otpDetails) {
		String otpResponse = "";
		String numberResponse = "";
		String emailResponse = "";
		String emailMasked = "";
		String number = "";
		
		if(otpDetails.getContact() != null && !otpDetails.getContact().equals("") && otpDetails.getContact().length() > 0){
			number = otpDetails.getContact().substring(6);
			numberResponse = "OTP has been sent to xxxxxx" + number;
			//return otpResponse;
		}
		
		if(otpDetails.getEmailAddress() != null && !otpDetails.getEmailAddress().equals("") 
				&& otpDetails.getEmailAddress().length() > 3){
			String[] email = otpDetails.getEmailAddress().split("@");
			String emailStart = email[0].substring(0, 3);
			String emailEnd = "@"+email[1];
			String mask = "";
			for(int i = emailStart.length(); i <= (email[0].length() - emailStart.length()); i++)
				mask += "x";
			
			emailMasked = emailStart + mask +emailEnd;
			emailResponse = "OTP has been sent to email " + emailMasked;
		}
		
		if(!numberResponse.equals("") && emailResponse.equals(""))
			return numberResponse;
		else if(numberResponse.equals("") && !emailResponse.equals(""))
			return emailResponse;
		else{
			otpResponse = "OTP has been sent to xxxxxx"
						  + number + " and email address " + emailMasked;
			return otpResponse;
		}
		
	}
	
	public boolean verifyOTP(String username, Integer otp) throws ParseException{
		/*String query = "select count(*) from otp_details where username = '"+username+"' and otp_number = "+otp;
		log.info("Verifying OTP for username " + username + " sql " + query);
		Integer count = getJdbcTemplate().queryForObject(query, Integer.class);
		log.debug("Username exist");*/
		OTPDetails otpDetails = getOTPDetails(username, otp);
		if(otpDetails == null)
			return false;
		log.info(otpDetails.getOtpCreationDate() + " " + otpDetails.getOtpEndDate());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date otpEndDate = formatter.parse(otpDetails.getOtpEndDate());
		Date currentDate = new Date();
		if(currentDate.compareTo(otpEndDate) > 0)
			return false;
		
		return true;
	}
	
	public User getUserByUsername(String username) throws SQLException{
		
		String query = "select u.id as id, u.username as username, u.temp_password as plain_password, u.password as encrypted_password, u.password_salt as password_salt, u.gcm_id as gcm_id, u.device as device, r.name as role_name "
				+ " from user u inner join user_role ur on ur.user_id = u.id "
				+ " inner join role r on  r.id = ur.role_id "
				+ " where u.username=? and u.active='Y' and u.isDelete = 'N'";
//+ " and authentication_type=?";

		User userInDb = loadUser(username, Constant.AUTHENTICATION_TYPE.APPLICATION.get(), query);

		return userInDb;
	}
}
