package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.CourseDao;
import com.isg.service.user.request.Courses;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class CourseDaoImpl extends JdbcDaoSupport implements CourseDao{

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public List<Courses> getAll() {
		String retriveCouse = "select * from course where active = 'Y'";
		return getJdbcTemplate().query(retriveCouse, new BeanPropertyRowMapper<Courses>(Courses.class));
	}

	@Override
	public List<Courses> getAllByBranchId(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<Courses> getCourseNamesForInstitutes(Integer instituteId, Integer branchId) {
		String retriveCourses = "select c.* from course c, branch b, institute i where c.active = 'Y' and b.id = ? and c.branch_id = b.id and i.id = b.institute_id and i.id = ?";
		Object[] inputs = new Object[] { branchId, instituteId };
		return getJdbcTemplate().query(retriveCourses, inputs, new BeanPropertyRowMapper<Courses>(Courses.class));
	}

}
