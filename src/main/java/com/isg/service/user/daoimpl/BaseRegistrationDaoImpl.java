package com.isg.service.user.daoimpl;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.isg.service.user.client.chat.response.session.UserResponse;
import com.isg.service.user.dao.ChatDao;
import com.isg.service.user.model.PaymentAccount;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.SecurityUtil;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class BaseRegistrationDaoImpl extends NamedParameterJdbcDaoSupport {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private static final Logger log = Logger.getLogger(BaseRegistrationDaoImpl.class);

	@Autowired
	@Lazy
	public ChatDao chatDao;

	Long createUser(String username, String password, int roleId, String authenticationType) throws NoSuchAlgorithmException {

		SimpleJdbcInsert insertUser =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("user")
										.usingColumns("username", "password", "temp_password", "authentication_type", "password_salt")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("username", username);

		byte[] randomSalt = SecurityUtil.generateSalt();
		byte[] passwordHash = SecurityUtil.hashPassword(password, randomSalt);

		parameters.put("password", passwordHash);
		parameters.put("authentication_type", authenticationType);
		parameters.put("password_salt", randomSalt);
		parameters.put("temp_password", password);

		Long userId = (Long) insertUser.executeAndReturnKey(parameters);
		log.debug("createUser -> registered user id::" + userId);

		SimpleJdbcInsert insertUserRole =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("user_role")
										.usingColumns("user_id", "role_id")
										.usingGeneratedKeyColumns("id");
		parameters = new HashMap<String, Object>(3);
		parameters.put("user_id", userId);
		parameters.put("role_id", roleId);
		insertUserRole.execute(parameters);
		log.debug("User role registered for::" + userId);

		return userId;

	}

	void createPaymentAccount(String paymentAccountType, Long userId) {
		SimpleJdbcInsert insertUserPaymentAccount =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("user_payment_account")
										.usingColumns("account_type", "account_number", "user_id");
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("account_type", paymentAccountType);
		parameters.put("account_number", PaymentAccount.generateAccountNumberWithCheckDigit());
		parameters.put("user_id", userId);
		insertUserPaymentAccount.execute(parameters);
		log.debug("Registered QfixPay account for user ::" + userId);
	}

	Long createChatAccount(String firstName, String lastName, Long userId) {

		// Remove whitespace between a word character and ( or ) (firstName.replaceAll("([\\() ])", "") + (lastName != null ? lastName.replaceAll("([\\() ])", "") : "")
		String randomEmail = RandomStringUtils.random(20, true, true) + "."
						+ Constant.CHAT_PROFILE_APP_NAME + "@" + Constant.EDUQFIX_DOMAIN;

		UserResponse userResponse = chatDao.createChatAccount(randomEmail, firstName, lastName, "");

		SimpleJdbcInsert insertUserChatProfile =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("user_chat_profile")
										.usingColumns("login", "password", "email", "chat_user_profile_id", "user_id")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(5);
		parameters.put("login", userResponse.getLogin());
		parameters.put("password", userResponse.getPassword());
		parameters.put("email", userResponse.getEmail());
		parameters.put("chat_user_profile_id", userResponse.getChatUserProfileId());
		parameters.put("user_id", userId);
		Long chatProfileId = (Long) insertUserChatProfile.executeAndReturnKey(parameters);
		log.debug("Registered chatProfileId ::" + chatProfileId);
		return userResponse.getChatUserProfileId();
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException {
		String password = "test@1234";
		byte[] randomSalt = SecurityUtil.generateSalt();
		System.out.println(new String(randomSalt));

		byte[] passwordHash1 = SecurityUtil.hashPassword(password, randomSalt);
		byte[] passwordHash2 = SecurityUtil.hashPassword(password, randomSalt);

		for (int i = 0; i < passwordHash1.length; i++) {
			if ((char) passwordHash1[i] == (char) passwordHash2[i])
				System.out.println(passwordHash1.length == passwordHash2.length);
			else
				System.out.println("M");
		}

	}
}
