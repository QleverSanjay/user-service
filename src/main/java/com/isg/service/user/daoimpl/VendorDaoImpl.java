package com.isg.service.user.daoimpl;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.isg.service.identity.model.Identity.ROLES_ID;
import com.isg.service.user.dao.ShoppingDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.dao.VendorDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.CustomBeanPropertyRowMapper;
import com.isg.service.user.request.Vendor;
import com.isg.service.user.request.VendorMerchantDetail;
import com.isg.service.user.util.Constant;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class VendorDaoImpl extends BaseRegistrationDaoImpl implements VendorDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private static final Logger log = Logger.getLogger(VendorDaoImpl.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private ShoppingDao shoppingDao;

	@Override
	@Transactional(readOnly = false)
	public Vendor register(Vendor vendor) throws NoSuchAlgorithmException {
		log.info("vendor registration process starts ");
		log.debug("Registration process start vendor object is :: " + vendor.toString());

		log.debug("Registration process start :: check user already exists");

		if (userDao.isUserExist(vendor.getEmail())) {
			throw new ApplicationException("User already exists in database.");
		}

		Long userId = createUser(vendor.getEmail(), vendor.getPassword(), ROLES_ID.VENDOR.get(), Constant.AUTHENTICATION_TYPE.APPLICATION.get());
		log.debug("vendor user account created.");
		createPaymentAccount(Constant.PaymentAccountType.QFIXPAY.get(), userId);
		log.debug("vendor payment account created.");
		SimpleJdbcInsert insertParent =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("vendor")
										.usingColumns("contact_first_name", "contact_last_name", "email", "user_id", "active", "is_registration_approved")
										.usingGeneratedKeyColumns("id");

		Map<String, Object> parameters = new HashMap<String, Object>(4);
		parameters.put("contact_first_name", vendor.getContactFirstName());
		parameters.put("contact_last_name", vendor.getContactLastName());
		parameters.put("email", vendor.getEmail());
		parameters.put("user_id", userId);
		parameters.put("active", vendor.getStatus());
		parameters.put("is_registration_approved", vendor.getIsRegistrationApproved());

		Long vendorId = (Long) insertParent.executeAndReturnKey(parameters);
		log.debug("vendor record created successfully with id::" + vendorId);

		createChatAccount(vendor.getContactFirstName(), vendor.getContactLastName(), userId);
		log.debug("vendor chat account created.");
		shoppingDao.createShoppingProfile(vendor.getEmail(), vendor.getPassword(), userId.intValue());
		log.debug("vendor shoppong account created.");

		vendor.setId(vendorId.intValue());
		log.info("vendor registration finished successfully.");
		return vendor;
	}

	@Override
	@Transactional(readOnly = false)
	public Vendor updateMerchantDetails(VendorMerchantDetail vendorMerchantDetail) throws NoSuchAlgorithmException {
		log.info("Vendor updation starts ");
		log.debug("Registration process start vendor object is :: " + vendorMerchantDetail.toString());

		String query = "update vendor set merchant_legal_name=:merchant_legal_name, marketing_name=:marketing_name, address=:address, " +
						" area=:area, city=:city, pincode=:pincode, phone=:phone, selling_category=:selling_category, mobile=:mobile, " +
						" active=:active, is_registration_approved=:is_registration_approved where email=:email";

		Map<String, Object> parameters = new HashMap<String, Object>(12);
		parameters.put("merchant_legal_name", vendorMerchantDetail.getMerchantLegalName());
		parameters.put("marketing_name", vendorMerchantDetail.getMarketingName());
		parameters.put("address", vendorMerchantDetail.getAddressLine());
		parameters.put("area", vendorMerchantDetail.getArea());
		parameters.put("city", vendorMerchantDetail.getCity());
		parameters.put("pincode", vendorMerchantDetail.getPincode());
		parameters.put("phone", vendorMerchantDetail.getPhone());
		parameters.put("selling_category", vendorMerchantDetail.getSellingCategory());
		parameters.put("mobile", vendorMerchantDetail.getMobile());
		parameters.put("active", "" + vendorMerchantDetail.getStatus());
		parameters.put("is_registration_approved", "" + vendorMerchantDetail.getIsRegistrationApproved());
		parameters.put("email", vendorMerchantDetail.getEmail());

		getNamedParameterJdbcTemplate().update(query, parameters);
		log.debug("vendor record updated successfully ");

		Vendor vendor = getByEmail(vendorMerchantDetail.getEmail());

		query = "update user_mall_profile set shopping_profile_id=:shopping_profile_id where user_id=:userId";
		parameters = new HashMap<String, Object>(12);
		parameters.put("shopping_profile_id", vendorMerchantDetail.getShopping_profile_id());
		parameters.put("userId", vendor.getUserId());
		getNamedParameterJdbcTemplate().update(query, parameters);

		log.info("vendor updation finished successfully.");
		return vendor;
	}

	@Override
	public Vendor getById(Integer entityId) {
		String query = "select * from vendor where id=:entityId";

		Map<String, Object> parameters = new HashMap<String, Object>(13);
		parameters.put("entityId", entityId);

		return getNamedParameterJdbcTemplate().queryForObject(query, parameters, new CustomBeanPropertyRowMapper<Vendor>(Vendor.class));
	}

	private Vendor getByEmail(String email) {
		String query = "select * from vendor where email=:email";

		Map<String, Object> parameters = new HashMap<String, Object>(13);
		parameters.put("email", email);

		return getNamedParameterJdbcTemplate().queryForObject(query, parameters, new CustomBeanPropertyRowMapper<Vendor>(Vendor.class));
	}
}