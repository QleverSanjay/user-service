package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES_ID;
import com.isg.service.user.client.notification.Notification;
import com.isg.service.user.client.notification.NotificationClient;
import com.isg.service.user.dao.BranchDao;
import com.isg.service.user.dao.EventDao;
import com.isg.service.user.dao.FileDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.AcademicYear;
import com.isg.service.user.model.BranchStudentParentDetail;
import com.isg.service.user.model.BranchWorkingDay;
import com.isg.service.user.model.EdiaryExtractingRowCallbackHandler;
import com.isg.service.user.model.EventExtractingRowCallbackHandler;
import com.isg.service.user.model.EventRecurrencePattern;
import com.isg.service.user.model.EventResponse;
import com.isg.service.user.model.HolidayResponse;
import com.isg.service.user.model.PersonalEventExtractingRowCallbackHandler;
import com.isg.service.user.model.UserEvent;
import com.isg.service.user.request.Ediary;
import com.isg.service.user.request.Event;
import com.isg.service.user.request.EventOtherDetails;
import com.isg.service.user.request.Holiday;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.TimeTableEntry;
import com.isg.service.user.request.Timetable;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.FileStorePathAssembler;
import com.isg.service.user.util.TimeTableGenerator;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class EventDaoImpl extends NamedParameterJdbcDaoSupport implements EventDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private static final Logger log = Logger.getLogger(EventDaoImpl.class);

	@Autowired
	private FileDao fileDao;

	@Autowired
	@Lazy
	private UserDao userDao;

	@Autowired
	BranchDao branchDao;

	@Autowired
	private NotificationClient notificationClient;

	@Autowired
	@Lazy
	private StudentDao studentDao;

	private final String RETRIEVE_EVENTS_DATE_FILTER_CRITERIA = " and ("
					+ " (e.startdate between :startDate and :endDate) "
					+ " or (e.enddate between :startDate and :endDate) "
					+ " or (e.startdate < :startDate  and e.enddate is null) "
					+ " or (e.startdate < :startDate  and e.enddate > :endDate)"
					+ " ) "
					+ " and (e.frequency != 'MONTHLY' or e.frequency IS NULL or (e.frequency = 'MONTHLY' && ((DAY(:startDate) <=  e.by_date && DAY(:endDate) >  e.by_date) or (DAY(:startDate) > DAY(:endDate) && DAY(:startDate) <=  e.by_date))))"
					+ " and e.status != 'D' "
					+ " group by ue.for_user_id,ue.role_id,	ue.entity_id,ue.event_id,ue.status "
					+ " order by e.startdate,e.starttime";

	private final String SYNC_EVENTS_DATE_FILTER_CRITERIA = " and "
					+ " (e.created_date > :lastSynchronisedDate or e.updated_date > :lastSynchronisedDate) "
					+ " group by ue.user_id,ue.role_id,	ue.entity_id,ue.event_id,ue.status "
					+ " order by e.startdate,e.starttime";

	private final String RETRIEVE_PERSONAL_EVENTS_DATE_FILTER_CRITERIA = " and ("
					+
					" (pt.startdate between :startDate and :endDate) "
					+ " or (pt.enddate between  :startDate and :endDate) "
					+ " or (pt.startdate <  :startDate  and pt.enddate is null) "
					+ " or (pt.startdate <  :startDate  and pt.enddate > :endDate)"
					+ " ) "
					+ " and (pt.frequency != 'MONTHLY' or pt.frequency IS NULL or (pt.frequency = 'MONTHLY' && ((DAY(:startDate) <=  pt.by_date && DAY(:endDate) >=  pt.by_date) or (DAY(:startDate) > DAY(:endDate) && DAY(:startDate) <=  pt.by_date)))) "
					+ " order by pt.startdate,pt.starttime";

	private final String SYNC_PERSONAL_EVENTS_DATE_FILTER_CRITERIA = " and "
					+ " (pt.created_date > :lastSynchronisedDate or pt.updated_date > :lastSynchronisedDate) "
					+ " order by pt.startdate,pt.starttime";

	private final String RETRIEVE_EDIARY_EVENTS_DATE_FILTER_CRITERIA = " and e.date between :startDate and :endDate "
					+ " and e.status != 'D' "
					+ " order by e.date";

	private final String SYNC_EDIARY_DATE_FILTER_CRITERIA = " and (e.created_date > :lastSynchronisedDate or e.updated_date > :lastSynchronisedDate) "
					+ " order by e.date";

	/******************************************** EVENTS QUERY *******************************/

	private final String EVENTS_FOR_PARENTS_BASE_QUERY = "select DATE_FORMAT(e.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(e.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(e.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(e.enddate, '%d-%m-%Y') as event_end_date, ue.for_user_id as user_id, "
					+ " e.*, "
					+ " CASE WHEN role.name = 'PARENT' THEN ue.entity_id ELSE student.id END as entity_id , "
					+ " ue.status as event_status, student.first_name as student_name, "
					+ " parent.firstname as profile_name, student.gender as gender, role.name as role_name, "
					+ " student.photo as student_photo, parent.photo as profile_photo, ue.for_user_id as for_user_id, uers.read_status as read_status "
					+ " from event e "
					+ " inner join user_event ue on e.id = ue.event_id "
					+ " left join parent on ue.entity_id = parent.id "
					+ " left join student on ue.for_user_id = student.user_id "
					+ " inner join user_role ur on ur.user_id = ue.for_user_id "
					+ " inner join role on role.id = ur.role_id and (ur.role_id= 2 or ur.role_id= 3)"
					+ " left join user_event_read_status uers on uers.user_id=ue.user_id and uers.role_id=ue.role_id "
					+ " and uers.entity_id = ue.entity_id and uers.event_id = ue.event_id and uers.for_user_id = ue.for_user_id "
					+ " where ("
					+ "  ue.user_id = :userId and ue.role_id=3 "
					//+ "  or  ue.for_user_id in (select user_id from student inner join branch_student_parent bsp where student.id= bsp.student_id and bsp.parent_id = :entityId and bsp.isDelete 'N')"
					+ " )";

	private final String RETRIEVE_EVENTS_FOR_PARENTS_QUERY = EVENTS_FOR_PARENTS_BASE_QUERY + RETRIEVE_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_EVENTS_FOR_PARENTS_QUERY = EVENTS_FOR_PARENTS_BASE_QUERY + SYNC_EVENTS_DATE_FILTER_CRITERIA;

	/*********************/

	private final String EVENTS_FOR_STUDENT_BASE_QUERY = "select DATE_FORMAT(e.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(e.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(e.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(e.enddate, '%d-%m-%Y') as event_end_date, ue.user_id as user_id, "
					+ " e.*,ue.entity_id, ue.status as event_status, student.first_name as profile_name, "
					+ " student.gender as gender, role.name as role_name, "
					+ " student.photo as profile_photo , ue.for_user_id as for_user_id, uers.read_status as read_status "
					+ " from event e "
					+ " inner join user_event ue on e.id = ue.event_id "
					+ " inner join student on ue.entity_id = student.id "
					+ " inner join user_role ur on ur.user_id = ue.user_id and ur.role_id= 2 "
					+ " inner join role on role.id = ur.role_id "
					+ " left join user_event_read_status uers on uers.user_id=ue.user_id and uers.role_id=ue.role_id "
					+ " and uers.entity_id = ue.entity_id and uers.event_id = ue.event_id and uers.for_user_id = ue.for_user_id "
					+ " where ue.user_id = :userId and ue.role_id = 2";

	private final String RETRIEVE_EVENTS_FOR_STUDENT_QUERY = EVENTS_FOR_STUDENT_BASE_QUERY + RETRIEVE_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_EVENTS_FOR_STUDENT_QUERY = EVENTS_FOR_STUDENT_BASE_QUERY + SYNC_EVENTS_DATE_FILTER_CRITERIA;

	/*********************/

	private final String RETRIEVE_EVENTS_FOR_TEACHER_BASE_QUERY = "select DATE_FORMAT(e.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(e.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(e.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(e.enddate, '%d-%m-%Y') as event_end_date, ue.user_id as user_id, "
					+ " e.*,ue.entity_id, ue.status as event_status, teacher.firstName as profile_name, "
					+ " teacher.gender as gender, role.name as role_name, "
					+ " teacher.photo as profile_photo , ue.for_user_id as for_user_id, uers.read_status as read_status "
					+ " from event e "
					+ " inner join user_event ue on e.id = ue.event_id "
					+ " inner join teacher on ue.entity_id = teacher.id "
					+ " inner join user_role ur on ur.user_id = ue.user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 4 "
					+ " left join user_event_read_status uers on uers.user_id=ue.user_id and uers.role_id=ue.role_id "
					+ " and uers.entity_id = ue.entity_id and uers.event_id = ue.event_id and uers.for_user_id = ue.for_user_id "
					+ " where ue.user_id = :userId and ue.role_id = 4 ";

	private final String RETRIEVE_EVENTS_FOR_TEACHER_QUERY = RETRIEVE_EVENTS_FOR_TEACHER_BASE_QUERY + RETRIEVE_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_EVENTS_FOR_TEACHER_QUERY = RETRIEVE_EVENTS_FOR_TEACHER_BASE_QUERY + SYNC_EVENTS_DATE_FILTER_CRITERIA;

	/*********************/

	private final String RETRIEVE_EVENTS_FOR_VENDOR_BASE_QUERY = "select DATE_FORMAT(e.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(e.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(e.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(e.enddate, '%d-%m-%Y') as event_end_date, ue.user_id as user_id, "
					+ " e.*,ue.entity_id, ue.status as event_status, vendor.merchant_legal_name as profile_name, "
					+ " 'N/A' as gender, role.name as role_name, "
					+ " 'N/A' as profile_photo , ue.for_user_id as for_user_id, uers.read_status as read_status "
					+ " from event e "
					+ " inner join user_event ue on e.id = ue.event_id "
					+ " inner join vendor on ue.entity_id = vendor.id "
					+ " inner join user_role ur on ur.user_id = ue.user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 5 "
					+ " left join user_event_read_status uers on uers.user_id=ue.user_id and uers.role_id=ue.role_id "
					+ " and uers.entity_id = ue.entity_id and uers.event_id = ue.event_id and uers.for_user_id = ue.for_user_id "
					+ " where ue.user_id = :userId and ur.role_id = 5 ";

	private final String RETRIEVE_EVENTS_FOR_VENDOR_QUERY = RETRIEVE_EVENTS_FOR_VENDOR_BASE_QUERY + RETRIEVE_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_EVENTS_FOR_VENDOR_QUERY = RETRIEVE_EVENTS_FOR_VENDOR_BASE_QUERY + SYNC_EVENTS_DATE_FILTER_CRITERIA;

	/******************************************** PERSONAL EVENTS QUERY *******************************/

	private final String PERSONAL_EVENTS_FOR_PARENTS_BASE_QUERY = "select DATE_FORMAT(pt.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(pt.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(pt.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(pt.enddate, '%d-%m-%Y') as event_end_date,"
					+ " pt.*, "
					+ " parent.firstname as profile_name, student.gender, role.name as role_name, parent.photo as profile_photo, "
					+ " pt.user_id as for_user_id, 'Y' as read_status "
					+ " from personal_task pt "
					+ " left join student on pt.student_id = student.id "
					+ " left join parent on pt.entity_id = parent.id "
					+ " inner join user_role ur on ur.user_id = pt.user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 3 "
					+ " where pt.user_id = :userId ";

	private final String RETRIEVE_PERSONAL_EVENTS_FOR_PARENTS_QUERY = PERSONAL_EVENTS_FOR_PARENTS_BASE_QUERY + RETRIEVE_PERSONAL_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_PERSONAL_EVENTS_FOR_PARENTS_QUERY = PERSONAL_EVENTS_FOR_PARENTS_BASE_QUERY + SYNC_PERSONAL_EVENTS_DATE_FILTER_CRITERIA;

	/*********************/

	private final String PERSONAL_EVENTS_FOR_STUDENT_BASE_QUERY = "select DATE_FORMAT(pt.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(pt.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(pt.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(pt.enddate, '%d-%m-%Y') as event_end_date,"
					+ " pt.*, "
					+ " student.first_name as profile_name, student.gender, role.name as role_name, student.photo as profile_photo, "
					+ " pt.user_id as for_user_id, 'Y' as read_status "
					+ " from personal_task pt "
					+ " inner join student on pt.entity_id = student.id "
					+ " inner join user_role ur on ur.user_id = pt.user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 2 "
					+ " where pt.user_id = :userId ";

	private final String RETRIEVE_PERSONAL_EVENTS_FOR_STUDENT_QUERY = PERSONAL_EVENTS_FOR_STUDENT_BASE_QUERY + RETRIEVE_PERSONAL_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_PERSONAL_EVENTS_FOR_STUDENT_QUERY = PERSONAL_EVENTS_FOR_STUDENT_BASE_QUERY + SYNC_PERSONAL_EVENTS_DATE_FILTER_CRITERIA;

	/*********************/

	private final String PERSONAL_EVENTS_FOR_TEACHER_BASE_QUERY = "select DATE_FORMAT(pt.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(pt.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(pt.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(pt.enddate, '%d-%m-%Y') as event_end_date,"
					+ " pt.*, "
					+ " teacher.firstName as profile_name, teacher.gender, role.name as role_name, teacher.photo as profile_photo, "
					+ " pt.user_id as for_user_id, 'Y' as read_status "
					+ " from personal_task pt "
					+ " inner join teacher on pt.entity_id = teacher.id "
					+ " inner join user_role ur on ur.user_id = pt.user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 4 "
					+ " where pt.user_id = :userId ";

	private final String RETRIEVE_PERSONAL_EVENTS_FOR_TEACHER_QUERY = PERSONAL_EVENTS_FOR_TEACHER_BASE_QUERY + RETRIEVE_PERSONAL_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_PERSONAL_EVENTS_FOR_TEACHER_QUERY = PERSONAL_EVENTS_FOR_TEACHER_BASE_QUERY + SYNC_PERSONAL_EVENTS_DATE_FILTER_CRITERIA;

	/*********************/

	private final String PERSONAL_EVENTS_FOR_VENDOR_BASE_QUERY = "select DATE_FORMAT(pt.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(pt.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(pt.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(pt.enddate, '%d-%m-%Y') as event_end_date,"
					+ " pt.*, "
					+ " vendor.merchant_legal_name as profile_name, 'N/A' as gender, role.name as role_name, 'N/A' as profile_photo, "
					+ " pt.user_id as for_user_id, 'Y' as read_status "
					+ " from personal_task pt "
					+ " inner join vendor on pt.entity_id = vendor.id "
					+ " inner join user_role ur on ur.user_id = pt.user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 5 "
					+ " where pt.user_id = :userId ";

	private final String RETRIEVE_PERSONAL_EVENTS_FOR_VENDOR_QUERY = PERSONAL_EVENTS_FOR_VENDOR_BASE_QUERY + RETRIEVE_PERSONAL_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_PERSONAL_EVENTS_FOR_VENDOR_QUERY = PERSONAL_EVENTS_FOR_VENDOR_BASE_QUERY + SYNC_PERSONAL_EVENTS_DATE_FILTER_CRITERIA;

	/******************************************** EDIARY QUERY *******************************/

	private final String EDIARY_EVENTS_FOR_STUDENT_BASE_QUERY = "select DATE_FORMAT(e.date, '%d-%m-%Y') as event_start_date,  "
					+ " DATE_FORMAT(e.date, '%d-%m-%Y') as event_end_date,"
					+ "  e.*, s.id as entity_id, s.first_name as profile_name, s.gender as gender, role.name as role_name,  "
					+ " s.photo as profile_photo, 'N/A' as event_head, "
					+ " CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, "
					+ " ue.for_user_id as for_user_id, uers.read_status as read_status "
					+ " from ediary e "
					+ " inner join user_ediary ue on e.id = ue.ediary_id "
					+ " inner join student s on ue.user_id = s.user_id "
					+ " inner join user u on u.id = ue.user_id "
					+ " inner join user_role ur on ur.user_id = ue.user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 2 "
					+ " inner join contact c on c.user_id = e.created_by "
					+ " left join user_ediary_read_status uers on uers.user_id=ue.user_id and uers.ediary_id = ue.ediary_id and uers.for_user_id = ue.for_user_id "
					+ " where ue.user_id =:userId ";

	private final String RETRIEVE_EDIARY_EVENTS_FOR_STUDENT_QUERY = EDIARY_EVENTS_FOR_STUDENT_BASE_QUERY + RETRIEVE_EDIARY_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_RETRIEVE_EDIARY_EVENTS_FOR_STUDENT_QUERY = EDIARY_EVENTS_FOR_STUDENT_BASE_QUERY + SYNC_EDIARY_DATE_FILTER_CRITERIA;

	/*********************/

	private final String EDIARY_EVENTS_FOR_PARENT_BASE_QUERY = "select DATE_FORMAT(e.date, '%d-%m-%Y') as event_start_date,  "
					+ " DATE_FORMAT(e.date, '%d-%m-%Y') as event_end_date,"
					+ "  e.*, s.id as entity_id, s.first_name as profile_name, s.gender as gender, role.name as role_name,  "
					+ " s.photo as profile_photo, 'N/A' as event_head, "
					+ " CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, "
					+ " ue.for_user_id as for_user_id, uers.read_status as read_status "
					+ " from ediary e "
					+ " inner join user_ediary ue on e.id = ue.ediary_id "
					+ " inner join student s on ue.for_user_id = s.user_id "
					+ " inner join user u on u.id = ue.user_id "
					+ " inner join user_role ur on ur.user_id = ue.for_user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 2 "
					+ " inner join contact c on c.user_id = e.created_by "
					+ " left join user_ediary_read_status uers on uers.user_id=ue.user_id and uers.ediary_id = ue.ediary_id and uers.for_user_id = ue.for_user_id "
					+ " where (ue.user_id =:userId and ue.for_user_id !=:userId)";

	private final String RETRIEVE_EDIARY_EVENTS_FOR_PARENT_QUERY = EDIARY_EVENTS_FOR_PARENT_BASE_QUERY + RETRIEVE_EDIARY_EVENTS_DATE_FILTER_CRITERIA;

	private final String SYNCHRONISE_RETRIEVE_EDIARY_EVENTS_FOR_PARENT_QUERY = EDIARY_EVENTS_FOR_PARENT_BASE_QUERY + SYNC_EDIARY_DATE_FILTER_CRITERIA;

	/*********************/

	private final String RETRIEVE_EDIARY_EVENTS_FOR_TEACHER_QUERY = "select * from (" +
					" select DATE_FORMAT(e.date, '%d-%m-%Y') as event_start_date, " +
					" DATE_FORMAT(e.date, '%d-%m-%Y') as event_end_date, " +
					" e.*, t.id as entity_id, t.firstName as profile_name, s.first_name as event_head, t.gender as gender, 'TEACHER' as role_name, " +
					" s.photo as profile_photo, " +
					" CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, " +
					" 0 as for_user_id, 'Y' as read_status " +
					" from ediary e " +
					" inner join user_ediary ue on e.id = ue.ediary_id " +
					" inner join student s on ue.user_id = s.user_id " +
					" inner join user u on u.id = ue.user_id " +
					" inner join teacher t on t.user_id = e.created_by " +
					" inner join contact c on c.user_id = e.created_by " +
					" where e.created_by=:userId and e.group_entry = 'N' and date between :startDate and :endDate and status != 'D' " +
					" UNION " +
					" select DATE_FORMAT(e.date, '%d-%m-%Y') as event_start_date, " +
					" DATE_FORMAT(e.date, '%d-%m-%Y') as event_end_date, " +
					" e.*, t.id as entity_id, t.firstName as profile_name, g.name as event_head, t.gender as gender, 'TEACHER' as role_name, " +
					" 'N/A' as profile_photo, " +
					" CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, " +
					" 0 as for_user_id, 'Y' as read_status " +
					" from ediary e " +
					" inner join ediary_group eg on e.id = eg.ediary_id " +
					" inner join `group` g on g.id = eg.group_id " +
					" inner join teacher t on t.user_id = e.created_by " +
					" inner join contact c on c.user_id = e.created_by " +
					" where e.created_by=:userId and e.group_entry = 'Y' and date between :startDate and :endDate and status != 'D' " +
					" ) ediary_result " +
					" order by date";

	private final String SYNCHRONISE_RETRIEVE_EDIARY_EVENTS_FOR_TEACHER_QUERY = "select * from (" +
					" select DATE_FORMAT(e.date, '%d-%m-%Y') as event_start_date, " +
					" DATE_FORMAT(e.date, '%d-%m-%Y') as event_end_date, " +
					" e.*, t.id as entity_id, t.firstName as profile_name, s.first_name as event_head, t.gender as gender, 'TEACHER' as role_name, " +
					" s.photo as profile_photo, " +
					" CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, " +
					" 0 as for_user_id, 'Y' as read_status " +
					" from ediary e " +
					" inner join user_ediary ue on e.id = ue.ediary_id " +
					" inner join student s on ue.user_id = s.user_id " +
					" inner join user u on u.id = ue.user_id " +
					" inner join teacher t on t.user_id = e.created_by " +
					" inner join contact c on c.user_id = e.created_by " +
					" where e.created_by=:userId and e.group_entry = 'N' and " +
					" (e.created_date > :lastSynchronisedDate or e.updated_date > :lastSynchronisedDate) " +
					" UNION " +
					" select DATE_FORMAT(e.date, '%d-%m-%Y') as event_start_date, " +
					" DATE_FORMAT(e.date, '%d-%m-%Y') as event_end_date, " +
					" e.*, t.id as entity_id, t.firstName as profile_name, g.name as event_head, t.gender as gender, 'TEACHER' as role_name, " +
					" 'N/A' as profile_photo, " +
					" CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy, " +
					" 0 as for_user_id, 'Y' as read_status " +
					" from ediary e " +
					" inner join ediary_group eg on e.id = eg.ediary_id " +
					" inner join `group` g on g.id = eg.group_id " +
					" inner join teacher t on t.user_id = e.created_by " +
					" inner join contact c on c.user_id = e.created_by " +
					" where e.created_by=:userId and e.group_entry = 'Y' and " +
					" (e.created_date > :lastSynchronisedDate or e.updated_date > :lastSynchronisedDate) " +
					" ) ediary_result " +
					" order by date";

	/*********************/

	private final String RETRIEVE_EVENTS_DETAILS_QUERY = "select distinct e.id, DATE_FORMAT(e.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(e.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(e.startdate, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(e.enddate, '%d-%m-%Y') as event_end_date, e.* "
					+ " from event e inner join user_event ue on e.id = ue.event_id "
					+ " where e.id=:eventId and ("
					+ "  ue.user_id = :userId "
					//+ "  or  ue.user_id in (select user_id from student where student.parent_id= :entityId)"
					+ " ) ";

	private final String RETRIEVE_EDIARY_DETAIL_QUERY = "select e.id, e.description, DATE_FORMAT(e.date, '%d-%m-%Y') as date, e.image_url, " +
					" CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy " +
					" from ediary e inner join user_ediary ue on e.id = ue.ediary_id "
					+ " inner join contact c on c.user_id = e.created_by "
					+ " where e.id=:ediaryId and ("
					+ "  ue.user_id = :userId "
					//+ "  or  ue.user_id in (select user_id from student where student.parent_id= :entityId)"
					+ " ) group by e.id";

	/************************/

	private final String RETRIEVE_TIMETABLE_DETAILS_QUERY = "select tdetail._id as id, DATE_FORMAT(tdetail.starttime, '%h:%i %p') as formatted_start_time, "
					+ " DATE_FORMAT(tdetail.endtime, '%h:%i %p') as formatted_end_time,"
					+ " DATE_FORMAT(tdetail.date, '%d-%m-%Y') as event_start_date, "
					+ " DATE_FORMAT(tdetail.date, '%d-%m-%Y') as event_end_date, s.id as entity_id, s.user_id as user_id, "
					+ " 'N' as alldayevent, 'N' as reminderSet, 0 as reminderbefore, '' as image_url, tdetail.title as title, "
					+ "  '' as venue, tdetail.title as description, '' as summary, tdetail.status as event_status, "
					+ " 'N' as is_schedule_updated, 'DAILY' as frequency, '' as by_day, 0 as by_date, "
					+ " s.first_name as student_name, s.first_name as profile_name, s.gender as gender, role.name as role_name, "
					+ " s.photo as student_photo, s.photo as profile_photo, "
					+ " s.user_id as for_user_id, 'Y' as read_status "
					+ " from timetable_detail tdetail "
					+ " inner join timetable t on t.id = tdetail.timetable_id "
					+ " left join timetable_division td on td.timetable_id = t.id and td.is_delete = 'N' "
					+ " inner join student s on s.standard_id = t.standard_id and (td.division_id IS NULL or ( s.division_id = td.division_id)) "
					+ " inner join user_role ur on ur.user_id = s.user_id "
					+ " inner join role on role.id = ur.role_id and ur.role_id= 2 ";

	private final String RETRIEVE_TIMETABLE_DATE_FILTER_CRITERIA = " and tdetail.`date` between :startDate and :endDate " +
					" and tdetail.is_delete = 'N' group by s.user_id,s.id,tdetail.id,tdetail.status " +
					" order by tdetail.date,tdetail.starttime, user_id ";

	private final String RETRIEVE_TIMETABLE_DETAILS_FOR_PARENT_QUERY = RETRIEVE_TIMETABLE_DETAILS_QUERY
					+ " where s.user_id in (select user_id from student inner join branch_student_parent bsp where student.id= bsp.student_id and bsp.parent_id = :entityId  and s.branch_id = bsp.branch_id and bsp.isDelete = 'N') "
					+ RETRIEVE_TIMETABLE_DATE_FILTER_CRITERIA;

	private final String RETRIEVE_TIMETABLE_DETAILS_FOR_STUDENT_QUERY = RETRIEVE_TIMETABLE_DETAILS_QUERY
					+ " where s.user_id =:userId "
					+ RETRIEVE_TIMETABLE_DATE_FILTER_CRITERIA;

	private final String SYNC_TIMETABLE_DATE_FILTER_CRITERIA = " and "
					+ " (tdetail.created_date > :lastSynchronisedDate or tdetail.updated_date > :lastSynchronisedDate) "
					+ " group by s.user_id,s.id,tdetail.id,tdetail.status "
					+ " order by tdetail.date,tdetail.starttime, user_id ";

	private final String SYNC_RETRIEVE_TIMETABLE_DETAILS_FOR_PARENT_QUERY = RETRIEVE_TIMETABLE_DETAILS_QUERY
					+ " where s.user_id in (select user_id from student inner join branch_student_parent bsp where student.id= bsp.student_id and bsp.parent_id = :entityId and s.branch_id = bsp.branch_id and bsp.isDelete = 'N') "
					+ SYNC_TIMETABLE_DATE_FILTER_CRITERIA;

	private final String SYNC_RETRIEVE_TIMETABLE_DETAILS_FOR_STUDENT_QUERY = RETRIEVE_TIMETABLE_DETAILS_QUERY
					+ " where s.user_id =:userId "
					+ SYNC_TIMETABLE_DATE_FILTER_CRITERIA;

	/***********************************/

	@Override
	public void createPersonalTask(Integer roldeId, Event event, Integer userId, Integer entityId, String defaultRole) throws ParseException {

		log.debug("createPersonalTask received for :::" + event.toString());
		SimpleJdbcInsert insertPersonalTask =
						new SimpleJdbcInsert(getJdbcTemplate().getDataSource())
										.withTableName("personal_task")
										.usingGeneratedKeyColumns("id")
										.usingColumns("title", "description", "venue", "startdate", "enddate", "starttime", "endtime", "alldayevent",
														"reminder", "reminderbefore", "forstudent", "student_id", "recurring", "frequency", "by_day",
														"by_date", "role_id", "user_id", "entity_id", "parent_task_id");

		Map<String, Object> parameters = new HashMap<String, Object>(19);
		parameters.put("title", event.getTitle());
		parameters.put("description", event.getDescription());
		parameters.put("venue", event.getVenue());

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		java.util.Date startDateParsed = format.parse(event.getStartDate());
		parameters.put("startdate", new java.sql.Date(startDateParsed.getTime()));

		if (!StringUtils.isEmpty(event.getEndDate())) {
			java.util.Date endtDateParsed = format.parse(event.getEndDate());
			parameters.put("enddate", new java.sql.Date(endtDateParsed.getTime()));
		} else {
			parameters.put("enddate", null);
		}

		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");

		parameters.put("starttime", new java.sql.Time(parseFormat.parse(event.getStarttime()).getTime()));
		parameters.put("endtime", new java.sql.Time(parseFormat.parse(event.getEndtime()).getTime()));

		parameters.put("alldayevent", event.isAlldayevent() ? "Y" : "N");
		parameters.put("reminder", event.isRemindMe() ? "Y" : "N");
		parameters.put("reminderbefore", event.getRemindMeBefore() != 0 ? event.getRemindMeBefore() : null);

		// Retrieve role id and replace here
		EventOtherDetails otherDetails = event.getOtherDetails();

		if (otherDetails != null) {
			parameters.put("forstudent", otherDetails.isForStudent() ? "Y" : "N");
			parameters.put("student_id", otherDetails.getStudentId() != null ? otherDetails.getStudentId() : null);
		} else {
			parameters.put("forstudent", "N");
			parameters.put("student_id", null);

		}

		List<EventRecurrencePattern> recurrencePatternList = event.getEventRecurrencePattern();

		if (recurrencePatternList != null && recurrencePatternList.size() > 0) {
			EventRecurrencePattern eventRecurrencePattern = recurrencePatternList.get(0);
			parameters.put("frequency", eventRecurrencePattern.getFrequency());

			if (eventRecurrencePattern.getByDay() != null && eventRecurrencePattern.getByDay().length > 0
							&& "WEEKLY".equalsIgnoreCase(eventRecurrencePattern.getFrequency())) {
				parameters.put("by_day", StringUtils.arrayToCommaDelimitedString(eventRecurrencePattern.getByDay()));
			} else {
				parameters.put("by_day", null);
			}

			if ("MONTHLY".equalsIgnoreCase(eventRecurrencePattern.getFrequency())) {
				parameters.put("by_date", StringUtils.isEmpty(eventRecurrencePattern.getByDate()) ? null : eventRecurrencePattern.getByDate());
			} else {
				parameters.put("by_date", null);
			}
			parameters.put("recurring", "Y");
		} else {
			parameters.put("frequency", null);
			parameters.put("by_day", null);
			parameters.put("by_date", null);
			parameters.put("recurring", "N");
		}

		parameters.put("role_id", roldeId);
		parameters.put("user_id", userId);
		parameters.put("entity_id", entityId);
		parameters.put("parent_task_id", null);

		Number parentTaskId = insertPersonalTask.executeAndReturnKey(parameters);

		// In case of parent make entry for child also.
		if (Identity.ROLES.PARENT.get().equalsIgnoreCase(defaultRole) && otherDetails != null && otherDetails.getStudentId() != null
						&& otherDetails.getStudentId() != 0) {

			Student student = studentDao.getStudentProfileDetailsbyId(otherDetails.getStudentId());

			parameters.put("forstudent", "N");
			parameters.put("student_id", null);

			parameters.put("role_id", Identity.ROLES_ID.STUDENT.get());
			parameters.put("user_id", student.getUserId());
			parameters.put("entity_id", student.getPersonalDetails().getId());
			parameters.put("parent_task_id", parentTaskId.intValue());

			insertPersonalTask.execute(parameters);
		}

	}

	@Override
	public void updatePersonalTask(Event event, Integer userId, Integer entityId, String defaultRole) throws ParseException {

		event.setId(event.getId().replaceAll("PEROSNAL-EVENT-", ""));
		Event existingPersonalTaskDetails = getPersonalTaskDetail(Integer.parseInt(event.getId()));

		String perosnalTaskUpdateSQL = "update personal_task set title =? , description = ?, venue =? , startdate = ?, "
						+
						" enddate = ?, starttime = ?, endtime = ?, alldayevent =?, reminder = ?, reminderbefore =?, forstudent = ?, "
						+
						" student_id = ?, recurring = ?, frequency = ?,  by_day = ?, by_date = ? , updated_date = ?, is_schedule_updated = ?,  status = ? where id = ? and entity_id = ? and user_id=?";

		Object[] params = new Object[22];
		params[0] = event.getTitle();
		params[1] = event.getDescription();
		params[2] = event.getVenue();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		java.util.Date startDateParsed = format.parse(event.getStartDate());
		params[3] = new java.sql.Date(startDateParsed.getTime());

		if (!StringUtils.isEmpty(event.getEndDate())) {
			java.util.Date endtDateParsed = format.parse(event.getEndDate());
			params[4] = new java.sql.Date(endtDateParsed.getTime());
		} else {
			params[4] = null;
		}

		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");

		params[5] = new java.sql.Time(parseFormat.parse(event.getStarttime()).getTime());
		params[6] = new java.sql.Time(parseFormat.parse(event.getEndtime()).getTime());

		params[7] = event.isAlldayevent() ? "Y" : "N";
		params[8] = event.isRemindMe() ? "Y" : "N";
		params[9] = event.getRemindMeBefore() != 0 ? event.getRemindMeBefore() : null;

		// Retrieve role id and replace here
		EventOtherDetails otherDetails = event.getOtherDetails();

		if (otherDetails != null) {
			params[10] = otherDetails.isForStudent() ? "Y" : "N";
			params[11] = otherDetails.getStudentId() != null ? otherDetails.getStudentId() : null;
			params[12] = "N";
		} else {
			params[10] = "N";
			params[11] = null;
			params[12] = "N";
		}

		List<EventRecurrencePattern> recurrencePatternList = event.getEventRecurrencePattern();

		if (recurrencePatternList != null && recurrencePatternList.size() > 0) {
			EventRecurrencePattern eventRecurrencePattern = recurrencePatternList.get(0);
			params[12] = "Y";
			params[13] = eventRecurrencePattern.getFrequency();

			if (eventRecurrencePattern.getByDay() != null && eventRecurrencePattern.getByDay().length > 0
							&& "WEEKLY".equalsIgnoreCase(eventRecurrencePattern.getFrequency())) {
				params[14] = StringUtils.arrayToCommaDelimitedString(eventRecurrencePattern.getByDay());
			} else {
				params[14] = null;
			}

			if ("MONTHLY".equalsIgnoreCase(eventRecurrencePattern.getFrequency())) {
				params[15] = eventRecurrencePattern.getByDate() == 0 ? null : eventRecurrencePattern.getByDate();
			} else {
				params[15] = null;
			}
		} else {
			params[13] = null;
			params[14] = null;
			params[15] = null;
		}

		params[16] = new java.sql.Date(System.currentTimeMillis());
		params[17] = isEventScheduleChanged(event, existingPersonalTaskDetails) ? "Y" : "N";
		params[18] = "M";

		params[19] = Integer.parseInt(event.getId());
		params[20] = entityId;
		params[21] = userId;

		int[] types = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.DATE, Types.DATE, Types.TIME, Types.TIME, Types.CHAR, Types.CHAR, Types.INTEGER,
						Types.CHAR, Types.INTEGER, Types.CHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.DATE, Types.VARCHAR, Types.VARCHAR,
						Types.INTEGER, Types.INTEGER, Types.INTEGER
		};

		int UPDATE_STATUS = getJdbcTemplate().update(perosnalTaskUpdateSQL, params, types);

		// In case of parent has set task for child add / update it.
		if (UPDATE_STATUS == 1 && Identity.ROLES.PARENT.get().equalsIgnoreCase(defaultRole)
						&& existingPersonalTaskDetails.getParentEventId() != 0) {

			Student student = studentDao.getStudentProfileDetailsbyId(existingPersonalTaskDetails.getOtherDetails().getStudentId());

			//check if student is set as null.
			if (otherDetails == null || otherDetails.getStudentId() == null) {
				deletePersonalTaskCreatedByParentForStudent(existingPersonalTaskDetails.getParentEventId(), student.getUserId().intValue(), student
								.getPersonalDetails()
								.getId());
			} else {

				params[10] = "N";
				params[11] = null;
				params[16] = existingPersonalTaskDetails.getParentEventId();
				params[17] = student.getPersonalDetails().getId();
				params[18] = student.getUserId().intValue();
				getJdbcTemplate().update(perosnalTaskUpdateSQL, params, types);
			}

		} else if (UPDATE_STATUS == 1 && Identity.ROLES.PARENT.get().equalsIgnoreCase(defaultRole) && otherDetails != null
						&& otherDetails.getStudentId() != null
						&& otherDetails.getStudentId() != 0) {
			Student student = studentDao.getStudentProfileDetailsbyId(otherDetails.getStudentId());
			event.setOtherDetails(null);
			createPersonalTask(Identity.ROLES_ID.STUDENT.get(), event, student.getUserId().intValue(), student.getPersonalDetails().getId(),
							Identity.ROLES.STUDENT.get());

		}

	}

	@Override
	public void deletePersonalTask(Integer eventId, Integer userId, Integer entityId) {

		deletePersonalTaskCreatedByParentForStudent(eventId, userId, entityId);

		String deleteTaskUpdateSQL = "update personal_task set status = 'D', updated_date=? where id = ? and entity_id = ? and user_id=?";
		int[] types = { Types.DATE, Types.INTEGER, Types.INTEGER, Types.INTEGER };
		Object[] params = new Object[] { new java.sql.Date(System.currentTimeMillis()), eventId, entityId, userId };
		int UPDATE_STATUS = getJdbcTemplate().update(deleteTaskUpdateSQL, params, types);
	}

	private void deletePersonalTaskCreatedByParentForStudent(Integer eventId, Integer userId, Integer entityId) {
		String deleteTaskUpdateSQL = "update personal_task set status = 'D', updated_date=? where parent_task_id = (select id from personal_task where id = ? and entity_id = ? and user_id=?)";
		int[] types = { Types.DATE, Types.INTEGER, Types.INTEGER, Types.INTEGER };
		Object[] params = new Object[] { new java.sql.Date(System.currentTimeMillis()), eventId, entityId, userId };
		getJdbcTemplate().update(deleteTaskUpdateSQL, params, types);
	}

	@Override
	public Collection<EventResponse> getEventsForAEntity(String role, Integer userId, Integer entityId, String startDate, String endDate) throws SQLException,
					ParseException,
					AuthenticationException {

		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

		java.util.Date startDateParsed = format.parse(startDate);
		java.sql.Date fromDate = new java.sql.Date(startDateParsed.getTime());

		java.util.Date endDateParsed = format.parse(endDate);
		java.sql.Date toDate = new java.sql.Date(endDateParsed.getTime());

		HashMap<Integer, EventResponse> eventResponseMap = new HashMap<Integer, EventResponse>();

		String query = "";

		if ("PARENT".equalsIgnoreCase(role)) {
			query = RETRIEVE_EVENTS_FOR_PARENTS_QUERY;
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			query = RETRIEVE_EVENTS_FOR_TEACHER_QUERY;
		} else if ("STUDENT".equalsIgnoreCase(role)) {
			query = RETRIEVE_EVENTS_FOR_STUDENT_QUERY;
		} else if ("VENDOR".equalsIgnoreCase(role)) {
			query = RETRIEVE_EVENTS_FOR_VENDOR_QUERY;
		} else {
			throw new AuthenticationException("Function not allowed.");
		}

		Map<String, Object> parameters = new HashMap<String, Object>(4);
		parameters.put("userId", userId);
		parameters.put("entityId", entityId);
		parameters.put("startDate", fromDate);
		parameters.put("endDate", toDate);

		log.debug("getEventsForAEntity query :: " + query);
		log.debug("getEventsForAEntity parameters :: " + parameters);
		EventExtractingRowCallbackHandler eventExtractingRowCallbackHandler = new EventExtractingRowCallbackHandler(eventResponseMap, role);
		getNamedParameterJdbcTemplate().query(query, parameters, eventExtractingRowCallbackHandler);

		eventResponseMap = eventExtractingRowCallbackHandler.getPopulatedMap();

		// Collect Any personal tasks
		addPersonalTaskToEvents(role, entityId, eventResponseMap, parameters);

		//Add timetable events
		addTimeTableEvents(role, entityId, eventResponseMap, parameters);

		return eventResponseMap.values();

	}

	private void addTimeTableEvents(String role, Integer entityId, HashMap<Integer, EventResponse> eventResponseMap, Map<String, Object> parameters)
					throws AuthenticationException {
		// TODO Auto-generated method stub

		String query = "";

		if ("PARENT".equalsIgnoreCase(role)) {
			query = RETRIEVE_TIMETABLE_DETAILS_FOR_PARENT_QUERY;
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			return;
		} else if ("STUDENT".equalsIgnoreCase(role)) {
			query = RETRIEVE_TIMETABLE_DETAILS_FOR_STUDENT_QUERY;
		} else {
			throw new AuthenticationException("Function not allowed.");
		}

		EventExtractingRowCallbackHandler eventExtractingRowCallbackHandler = new EventExtractingRowCallbackHandler(eventResponseMap, role);
		getNamedParameterJdbcTemplate().query(query, parameters, eventExtractingRowCallbackHandler);

	}

	private void addPersonalTaskToEvents(String role, Integer entityId, HashMap<Integer, EventResponse> eventResponseMap,
					Map<String, Object> parameters)
					throws SQLException, AuthenticationException {

		String query = "";

		if ("PARENT".equalsIgnoreCase(role)) {
			query = RETRIEVE_PERSONAL_EVENTS_FOR_PARENTS_QUERY;
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			query = RETRIEVE_PERSONAL_EVENTS_FOR_TEACHER_QUERY;
		} else if ("STUDENT".equalsIgnoreCase(role)) {
			query = RETRIEVE_PERSONAL_EVENTS_FOR_STUDENT_QUERY;
		} else if ("VENDOR".equalsIgnoreCase(role)) {
			query = RETRIEVE_PERSONAL_EVENTS_FOR_VENDOR_QUERY;
		} else {
			throw new AuthenticationException("Function not allowed.");
		}

		PersonalEventExtractingRowCallbackHandler perosnalEventExtractingRowCallbackHandler = new PersonalEventExtractingRowCallbackHandler(eventResponseMap,
						entityId, role);
		getNamedParameterJdbcTemplate().query(query, parameters, perosnalEventExtractingRowCallbackHandler);
		eventResponseMap = perosnalEventExtractingRowCallbackHandler.getPopulatedMap();

	}

	@Override
	public Collection<EventResponse> syncEventsForAEntity(String role, Integer userId, Integer entityId, long lastSyncTimeStamp)
					throws ParseException, AuthorisationException {

		java.sql.Timestamp filterDate = new java.sql.Timestamp(lastSyncTimeStamp);

		HashMap<Integer, EventResponse> eventResponseMap = new HashMap<Integer, EventResponse>();

		String query = "";

		if ("PARENT".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_EVENTS_FOR_PARENTS_QUERY;
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_EVENTS_FOR_TEACHER_QUERY;
		} else if ("STUDENT".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_EVENTS_FOR_STUDENT_QUERY;
		} else if ("VENDOR".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_EVENTS_FOR_VENDOR_QUERY;
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("entityId", entityId);
		parameters.put("userId", userId);
		parameters.put("lastSynchronisedDate", filterDate);

		log.debug("syncEventsForAEntity query :: " + query);
		log.debug("syncEventsForAEntity query parameters :: " + parameters);
		EventExtractingRowCallbackHandler eventExtractingRowCallbackHandler = new EventExtractingRowCallbackHandler(eventResponseMap, role);
		getNamedParameterJdbcTemplate().query(query, parameters, eventExtractingRowCallbackHandler);

		eventResponseMap = eventExtractingRowCallbackHandler.getPopulatedMap();

		// Collect Any personal tasks
		syncPersonalTaskToEvents(role, entityId, eventResponseMap, parameters);

		//Add timetable events
		syncTimeTableEvents(role, entityId, eventResponseMap, parameters);

		return eventResponseMap.values();

	}

	private void syncTimeTableEvents(String role, Integer entityId, HashMap<Integer, EventResponse> eventResponseMap, Map<String, Object> parameters)
					throws AuthorisationException {
		String query = "";

		if ("PARENT".equalsIgnoreCase(role)) {
			query = SYNC_RETRIEVE_TIMETABLE_DETAILS_FOR_PARENT_QUERY;
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			return;
		} else if ("STUDENT".equalsIgnoreCase(role)) {
			query = SYNC_RETRIEVE_TIMETABLE_DETAILS_FOR_STUDENT_QUERY;
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		EventExtractingRowCallbackHandler eventExtractingRowCallbackHandler = new EventExtractingRowCallbackHandler(eventResponseMap, role);
		getNamedParameterJdbcTemplate().query(query, parameters, eventExtractingRowCallbackHandler);

	}

	private void syncPersonalTaskToEvents(String role, Integer entityId, HashMap<Integer, EventResponse> eventResponseMap, Map<String, Object> parameters)
					throws AuthorisationException {

		String query = "";

		if ("PARENT".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_PERSONAL_EVENTS_FOR_PARENTS_QUERY;
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_PERSONAL_EVENTS_FOR_TEACHER_QUERY;
		} else if ("STUDENT".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_PERSONAL_EVENTS_FOR_STUDENT_QUERY;
		} else if ("VENDOR".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_PERSONAL_EVENTS_FOR_VENDOR_QUERY;
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		PersonalEventExtractingRowCallbackHandler perosnalEventExtractingRowCallbackHandler = new PersonalEventExtractingRowCallbackHandler(eventResponseMap,
						entityId, role);
		getNamedParameterJdbcTemplate().query(query, parameters, perosnalEventExtractingRowCallbackHandler);
		eventResponseMap = perosnalEventExtractingRowCallbackHandler.getPopulatedMap();

	}

	private Event getPersonalTaskDetail(Integer taskId) {

		String query = "select * from personal_task where id= :taskId";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("taskId", taskId);

		Event personalTask = getNamedParameterJdbcTemplate().queryForObject(query, parameterSource, new RowMapper<Event>() {
			@Override
			public Event mapRow(ResultSet rs, int rowNum)
							throws SQLException {
				Event event = new Event();
				event.setId("" + rs.getInt("id"));
				event.setStartDate(rs.getString("startdate"));
				event.setStarttime(rs.getString("starttime"));
				event.setEndDate(rs.getString("enddate"));
				event.setEndtime(rs.getString("endtime"));
				event.setAlldayevent(rs.getString("alldayevent").equals("Y") ? true : false);
				event.setRemindMe(rs.getString("reminder").equals("Y") ? true : false);
				event.setRemindMeBefore(rs.getInt("reminderbefore"));
				event.setTitle(rs.getString("title"));
				event.setVenue(rs.getString("venue"));
				event.setDescription(rs.getString("description"));
				event.setStatus(rs.getString("status"));
				event.setStatus(rs.getString("is_schedule_updated"));
				event.setParentEventId(rs.getInt("parent_task_id"));

				EventRecurrencePattern eventRecurrencePattern = new EventRecurrencePattern();
				eventRecurrencePattern.setFrequency(rs.getString("frequency"));
				eventRecurrencePattern.setByDayStr(rs.getString("by_day"));
				eventRecurrencePattern.setByDate(rs.getInt("by_date"));

				List<EventRecurrencePattern> eventRecurrencePatternList = new ArrayList<EventRecurrencePattern>();
				eventRecurrencePatternList.add(eventRecurrencePattern);
				event.setEventRecurrencePattern(eventRecurrencePatternList);
				return event;
			}
		});

		return personalTask;

	}

	private boolean isEventScheduleChanged(Event newEvent, Event existingEvent) {

		boolean isEventScheduleUpdated = false;

		if (!newEvent.getStartDate().equals(existingEvent.getStartDate())) {
			isEventScheduleUpdated = true;
		} else if (!newEvent.getEndDate().equals(existingEvent.getEndDate())) {
			isEventScheduleUpdated = true;
		}

		List<EventRecurrencePattern> recurrencePatternListOfNewEvent = newEvent.getEventRecurrencePattern();
		List<EventRecurrencePattern> recurrencePatternListOfExistingEvent = existingEvent.getEventRecurrencePattern();

		boolean recurrencePatternExistsForNewEvent = (recurrencePatternListOfNewEvent != null && recurrencePatternListOfNewEvent.size() > 0) ? true : false;
		boolean recurrencePatternExistsForExistingEvent = (recurrencePatternListOfExistingEvent != null && recurrencePatternListOfExistingEvent.size() > 0) ? true
						: false;

		if (recurrencePatternExistsForNewEvent != recurrencePatternExistsForExistingEvent) {
			isEventScheduleUpdated = true;
		} else if (recurrencePatternExistsForNewEvent && recurrencePatternExistsForExistingEvent) {
			isEventScheduleUpdated = recurrencePatternListOfNewEvent.equals(recurrencePatternListOfExistingEvent);
		}

		return isEventScheduleUpdated;

	}

	@Override
	public Collection<EventResponse> getEdiaryDetails(String defaultRole, Integer userId, Integer entityId, String startDate, String endDate)
					throws ParseException {

		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

		java.util.Date startDateParsed = format.parse(startDate);
		java.sql.Date fromDate = new java.sql.Date(startDateParsed.getTime());

		java.util.Date endDateParsed = format.parse(endDate);
		java.sql.Date toDate = new java.sql.Date(endDateParsed.getTime());

		String query = RETRIEVE_EDIARY_EVENTS_FOR_STUDENT_QUERY;
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("startDate", fromDate);
		parameters.put("endDate", toDate);
		parameters.put("userId", userId);

		if ("PARENT".equalsIgnoreCase(defaultRole)) {
			query = RETRIEVE_EDIARY_EVENTS_FOR_PARENT_QUERY;
			parameters.put("entityId", entityId);
		} else if ("TEACHER".equalsIgnoreCase(defaultRole)) {
			query = RETRIEVE_EDIARY_EVENTS_FOR_TEACHER_QUERY;
			//parameters.put("userId", userId);
		}

		log.debug("getEdiaryDetails query :: " + query);
		log.debug("getEdiaryDetails query parameters :: " + parameters);

		parameters.put("startDate", fromDate);
		parameters.put("endDate", toDate);

		HashMap<Integer, EventResponse> eventResponseMap = new HashMap<Integer, EventResponse>();
		EdiaryExtractingRowCallbackHandler eventExtractingRowCallbackHandler = new EdiaryExtractingRowCallbackHandler(eventResponseMap, defaultRole);
		getNamedParameterJdbcTemplate().query(query, parameters, eventExtractingRowCallbackHandler);

		eventResponseMap = eventExtractingRowCallbackHandler.getPopulatedMap();

		return eventResponseMap.values();
	}

	@Override
	public Collection<EventResponse> syncEdiaryForAEntity(String role, Integer userId, Integer entityId, long lastSyncTimeStamp)
					throws ParseException, AuthorisationException {

		java.sql.Timestamp filterDate = new java.sql.Timestamp(lastSyncTimeStamp);

		String query = SYNCHRONISE_RETRIEVE_EDIARY_EVENTS_FOR_STUDENT_QUERY;
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("lastSynchronisedDate", filterDate);
		parameters.put("userId", userId);

		if ("PARENT".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_RETRIEVE_EDIARY_EVENTS_FOR_PARENT_QUERY;
			parameters.put("entityId", entityId);
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			query = SYNCHRONISE_RETRIEVE_EDIARY_EVENTS_FOR_TEACHER_QUERY;
			//parameters.put("userId", userId);
		}

		log.debug("syncEdiaryForAEntity query :: " + query);
		log.debug("syncEdiaryForAEntity query parameters :: " + parameters);

		HashMap<Integer, EventResponse> eventResponseMap = new HashMap<Integer, EventResponse>();
		EdiaryExtractingRowCallbackHandler eventExtractingRowCallbackHandler = new EdiaryExtractingRowCallbackHandler(eventResponseMap, role);
		getNamedParameterJdbcTemplate().query(query, parameters, eventExtractingRowCallbackHandler);

		eventResponseMap = eventExtractingRowCallbackHandler.getPopulatedMap();

		return eventResponseMap.values();

	}

	@Override
	public void addEdiaryEntry(Ediary ediary, Integer userId, Integer instituteId, Integer branchId, Integer entityId)
					throws AuthorisationException, IOException, ParseException {

		List<BranchStudentParentDetail> branchStudentParentDetailByStudentIdList = saveEdiary(ediary, userId, instituteId, branchId);

		prepareAndSendEdiaryNotification(ediary, branchStudentParentDetailByStudentIdList);
	}

	private List<BranchStudentParentDetail> saveEdiary(Ediary ediary, Integer userId, Integer instituteId, Integer branchId) throws AuthorisationException,
					ParseException, IOException {
		boolean sendToGroups = false;

		if (ediary.getGroupIds() != null && !ediary.getGroupIds().isEmpty()) {
			// check if user has access to groups provided in request.
			if (!userDao.canUserAccessSpecifiedGroup(userId, ediary.getGroupIds())) {
				throw new AuthorisationException("Function not allowed.");
			}

			sendToGroups = true;

		} else if (ediary.getUserIds() != null && !ediary.getUserIds().isEmpty()) {
			// check if user has access to groups provided in request.
			if (!userDao.canUserAccessSpecifiedUserProfiles(userId, branchId, ediary.getUserIds())) {
				throw new AuthorisationException("Function not allowed.");
			}

		}

		SimpleJdbcInsert insertEdiary =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("ediary")
										.usingColumns("description", "date", "image_url", "branch_id", "created_by",
														"group_entry")
										.usingGeneratedKeyColumns("id");

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("description", ediary.getDescription());

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date startDateParsed = format.parse(ediary.getDate());

		parameters.put("date", new java.sql.Date(startDateParsed.getTime()));

		// Derive downloaded image URL
		if (ediary.getFileAttachment() != null
						&& FileStorePathAssembler.isValidImageExtension(FileStorePathAssembler.ATTACHEMENT_PATTERN, ediary.getFileAttachment()
										.getFileNameWithExtension())) {
			String randomFileName = FileStorePathAssembler.getRandomFileName(ediary.getFileAttachment().getFileNameWithExtension());
			String noticeStoragePath = FileStorePathAssembler.getEdiaryStoragePath(instituteId, branchId);
			fileDao.saveFile(noticeStoragePath, ediary.getFileAttachment().getBytes(), randomFileName);

			String imageUrlPath = FileStorePathAssembler.getEdiaryRelativePath(instituteId, branchId) + FileStorePathAssembler.PATH_SEPARATOR + randomFileName;
			parameters.put("image_url", imageUrlPath);
		} else {
			parameters.put("image_url", null);
		}

		parameters.put("branch_id", branchId);
		parameters.put("created_by", userId);
		if (sendToGroups) {
			parameters.put("group_entry", "Y");
		} else {
			parameters.put("group_entry", "N");
		}

		Long ediaryId = (Long) insertEdiary.executeAndReturnKey(parameters);
		ediary.setId(ediaryId.intValue());

		if (sendToGroups) {
			return insertEdiaryGroupEntries(ediary, ediaryId, branchId);
		} else if (!CollectionUtils.isEmpty(ediary.getUserIds())) {

			//Add parents of indiviudal students
			List<BranchStudentParentDetail> branchStudentParentDetailByStudentIdList = userDao.getBranchStudentParentDetailByStudent(ediary.getUserIds(),
							branchId);

			Set<Integer> notifyToParentUserIds = new HashSet<Integer>();
			for (BranchStudentParentDetail branchStudentParentDetail : branchStudentParentDetailByStudentIdList) {
				notifyToParentUserIds.add(branchStudentParentDetail.getParentUserId());
			}

			Set<Integer> individualUserIds = new HashSet<Integer>(ediary.getUserIds());

			saveEdiaryIndividualEntries(ediary, individualUserIds);

			saveEdiaryEntriesForParent(ediary, branchStudentParentDetailByStudentIdList);

			return branchStudentParentDetailByStudentIdList;

		} else {
			throw new ApplicationException("Invalid request submitted.");
		}
	}

	private List<BranchStudentParentDetail> insertEdiaryGroupEntries(Ediary ediary, Long ediaryId, Integer branchId) {
		String noticeGroup = "INSERT INTO ediary_group (ediary_id, group_id) VALUES (" + ediaryId.intValue() + ", ?)";
		final List<Integer> groupIds = ediary.getGroupIds();
		getJdbcTemplate().batchUpdate(noticeGroup, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Integer groupId = groupIds.get(i);
				ps.setInt(1, groupId);
			}

			@Override
			public int getBatchSize() {
				return groupIds.size();
			}
		});

		Set<Integer> groupUserIds = userDao.getGroupUsers(groupIds, true, true);

		List<BranchStudentParentDetail> branchStudentParentDetailList = userDao.getBranchStudentParentDetailByStudentGroup(groupIds, branchId);

		if (!CollectionUtils.isEmpty(ediary.getUserIds())) {
			groupUserIds.addAll(ediary.getUserIds());
			//Add parents of individual students
			branchStudentParentDetailList.addAll(userDao.getBranchStudentParentDetailByStudent(ediary.getUserIds(), branchId));
		}

		saveEdiaryIndividualEntries(ediary, groupUserIds);

		saveEdiaryEntriesForParent(ediary, branchStudentParentDetailList);

		return branchStudentParentDetailList;

	}

	private void saveEdiaryIndividualEntries(final Ediary ediary, final Set<Integer> userIds) {

		if (!CollectionUtils.isEmpty(userIds)) {
			String noticeIndividual = "INSERT INTO user_ediary (ediary_id, user_id, for_user_id) VALUES (?, ?, ?)";

			final Integer[] userIdAry = userIds.toArray(new Integer[] {});

			getJdbcTemplate().batchUpdate(noticeIndividual, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Integer userId = userIdAry[i];
					ps.setInt(1, ediary.getId());
					ps.setInt(2, userId);
					ps.setInt(3, userId);
				}

				@Override
				public int getBatchSize() {
					return userIdAry.length;
				}
			});

		}
	}

	private void saveEdiaryEntriesForParent(final Ediary ediary, final List<BranchStudentParentDetail> branchStudentParentDetailList) {

		if (!CollectionUtils.isEmpty(branchStudentParentDetailList)) {
			String ediaryForParent = "INSERT INTO user_ediary (ediary_id, user_id, for_user_id) VALUES (?, ?, ?)";

			getJdbcTemplate().batchUpdate(ediaryForParent, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					BranchStudentParentDetail branchStudentParentDetail = branchStudentParentDetailList.get(i);
					ps.setInt(1, ediary.getId());
					ps.setInt(2, branchStudentParentDetail.getParentUserId());
					ps.setInt(3, branchStudentParentDetail.getStudentUserId());
				}

				@Override
				public int getBatchSize() {
					return branchStudentParentDetailList.size();
				}
			});

		}
	}

	@Override
	public Collection<Ediary> getEdiaryDetails(Integer userId) {

		String query = "select e.id, e.description, DATE_FORMAT(e.date, '%d-%m-%Y') as date, e.image_url, " +
						" CONCAT(c.firstname,' ',IFNULL(c.lastname,'')) as sentBy " +
						" from ediary e" +
						" inner join contact c on c.user_id = e.created_by " +
						" where created_by = ?";

		Object[] inputs = new Object[] { userId };
		return getJdbcTemplate().query(query, inputs,
						new RowMapper<Ediary>() {
							@Override
							public Ediary mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								return new Ediary(
												resultSet.getInt("id"),
												resultSet.getString("description"),
												resultSet.getString("date"),
												resultSet.getString("image_url"),
												resultSet.getString("sentBy"));
							}
						});

	}

	@Override
	public void assignStudentEventsToParent(Long studentUserId, Integer parentUserId, Integer parentId) {

		String userEvents = " select event_id, event_group_id, status from user_event ue where ue.user_id= :studentUserId"
						+
						" and NOT EXISTS (select id from user_event ue1 where ue1.user_id = :parentUserId and ue1.event_id= ue.event_id and ue1.for_user_id != :studentUserId)";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("studentUserId", studentUserId);
		queryParameters.put("parentUserId", parentUserId);

		List<UserEvent> userEventList = getNamedParameterJdbcTemplate().query(userEvents, queryParameters, new RowMapper<UserEvent>() {

			@Override
			public UserEvent mapRow(ResultSet rs, int arg1) throws SQLException {
				return new UserEvent(rs.getInt("event_id"), rs.getInt("event_group_id"), rs.getString("status"));
			}
		});

		assignEventsToIndividualUser(userEventList, parentUserId, parentId, ROLES_ID.PARENT.get(), studentUserId.intValue());

	}

	private void assignEventsToIndividualUser(final List<UserEvent> userEventList, final Integer userId, final Integer entityId, final int roleId,
					final int forUserId) {

		String noticeIndividual = "INSERT INTO user_event (user_id, role_id, entity_id, event_id, event_group_id, status, for_user_id) VALUES (?, ?, ?, ?, ?, ?, ?)";

		getJdbcTemplate().batchUpdate(noticeIndividual, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				UserEvent userEvent = userEventList.get(i);
				ps.setInt(1, userId);
				ps.setInt(2, roleId);
				ps.setInt(3, entityId);
				ps.setInt(4, userEvent.getEventId());
				ps.setInt(5, userEvent.getEventGroupId());
				ps.setString(6, userEvent.getStatus());
				ps.setInt(7, forUserId);
			}

			@Override
			public int getBatchSize() {
				return userEventList.size();
			}
		});
	}

	@Override
	public Event getEventDetails(Integer userId, Integer entityId, Integer eventId) {

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);
		queryParameters.put("eventId", eventId);
		queryParameters.put("entityId", entityId);

		return getNamedParameterJdbcTemplate().queryForObject(RETRIEVE_EVENTS_DETAILS_QUERY, queryParameters,
						new RowMapper<Event>() {
							@Override
							public Event mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								Event event = new Event();
								event.setId("EVENT-" + rs.getInt("id"));
								event.setStartDate(rs.getString("event_start_date"));
								event.setStarttime(rs.getString("formatted_start_time"));
								event.setEndDate(rs.getString("event_end_date"));
								event.setEndtime(rs.getString("formatted_end_time"));
								event.setAlldayevent(rs.getString("alldayevent").equals("Y") ? true : false);
								event.setRemindMe(rs.getString("reminderSet").equals("Y") ? true : false);
								event.setRemindMeBefore(rs.getInt("reminderbefore"));
								event.setImage(rs.getString("image_url"));
								event.setTitle(rs.getString("title"));
								event.setVenue(rs.getString("venue"));
								event.setDescription(rs.getString("description"));
								event.setSummary(rs.getString("summary"));

								event.setScheduleUpdated(rs.getString("is_schedule_updated"));

								EventRecurrencePattern eventRecurrencePattern = new EventRecurrencePattern();
								eventRecurrencePattern.setFrequency(rs.getString("frequency"));
								eventRecurrencePattern.setByDayStr(rs.getString("by_day"));
								eventRecurrencePattern.setByDate(rs.getInt("by_date"));

								List<EventRecurrencePattern> eventRecurrencePatternList = new ArrayList<EventRecurrencePattern>();
								eventRecurrencePatternList.add(eventRecurrencePattern);
								event.setEventRecurrencePattern(eventRecurrencePatternList);
								return event;
							}
						});

	}

	public void setNotificationClient(NotificationClient notificationClient) {
		this.notificationClient = notificationClient;
	}

	@Override
	public Ediary getEdiaryDetail(Integer userId, Integer entityId, Integer ediaryId, String defaultRole) {

		Map<String, Object> queryParameters = new HashMap<String, Object>(3);
		queryParameters.put("userId", userId);
		queryParameters.put("ediaryId", ediaryId);
		queryParameters.put("entityId", entityId);

		log.debug("getEdiaryDetails query :: " + RETRIEVE_EDIARY_DETAIL_QUERY);
		log.debug("getEdiaryDetails query parameters :: " + queryParameters);

		return getNamedParameterJdbcTemplate().queryForObject(RETRIEVE_EDIARY_DETAIL_QUERY, queryParameters,
						new RowMapper<Ediary>() {
							@Override
							public Ediary mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								return new Ediary(
												resultSet.getInt("id"),
												resultSet.getString("description"),
												resultSet.getString("date"),
												resultSet.getString("image_url"),
												resultSet.getString("sentBy"));

							}
						});

	}

	private void prepareAndSendEdiaryNotification(final Ediary ediary, List<BranchStudentParentDetail> branchStudentParentDetailList) {

		/*String sql = "select distinct ur.user_id, ur.role_id, r.name as role, " +
						" b.name as branchName, i.name as instituteName " +
						" from user_role as ur " +
						" INNER JOIN role as r on ur.role_id = r.id " +
						" INNER JOIN user_ediary as ue on ur.user_id = ue.for_user_id " +
						" INNER JOIN ediary as e on ue.ediary_id = e.id and e.id = " + ediary.getId() +
						" INNER JOIN branch as b on b.id = e.branch_id " +
						" INNER JOIN institute as i on i.id = b.institute_id" +
						" WHERE ur.role_id !=" + Identity.ROLES_ID.PARENT.get();

		System.out.println("SQL::: " + sql);
		final Map<String, Integer> userIds = new HashMap<>();
		final Map<String, Object> content = new HashMap<String, Object>();

		getJdbcTemplate().query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet resultSet, int arg1) throws SQLException {
				Integer userId = resultSet.getInt("user_id");
				Integer roleId = resultSet.getInt("role_id");

				userIds.put(resultSet.getString("role").toUpperCase() + "-" + userId, userId);

				if (content.size() == 0) {
					content.put("institute_name", resultSet.getString("instituteName"));
					content.put("branch_name", resultSet.getString("branchName"));
				}

				return null;
			}
		});

		// Add parent ids
		if (!CollectionUtils.isEmpty(branchStudentParentDetailList)) {
			String parentRoleName = Identity.ROLES.PARENT.get();
			for (BranchStudentParentDetail branchStudentParentDetail : branchStudentParentDetailList) {
				userIds.put(parentRoleName + "-" + branchStudentParentDetail.getParentUserId(), branchStudentParentDetail.getParentUserId());
			}
		}*/

		/*int descriptionLength = ediary.getDescription().length();

		content.put("push-title", "EDIARY");
		content.put("push-message", (descriptionLength > 40) ? ediary.getDescription().substring(0, 40) : ediary.getDescription());
		String additionalData = "{"
						+ "\"type\":\"EDIARY\","
						+ "\"id\":" + ediary.getId() + "}";

		content.put("push-detailJson", additionalData);
		*/
		// Create notification object to send to notification-service.
		Notification notification = new Notification();
		notification.setType("EDIARY");
		notification.setId(ediary.getId());
		notification.sendEmail(true);
		notification.sendSMS(true);
		notification.sendPushMessage(true);
		notification.setTemplateCode(Constant.NotificationTemplateCode.CREATE_EDIARY.get());
		notification.setBatchEnabled(false);

		try {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println("EDIARY OBJECT:::::\n" + mapper.writeValueAsString(notification));
			notificationClient.sendMessage(mapper.writeValueAsString(notification));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}

	}

	@Override
	@Transactional
	public void saveTimeTable(Timetable timetable, Integer branchId, Integer userId, String defaultRole)
					throws AuthorisationException {

		log.debug("insert Timetable.......");
		SimpleJdbcInsert insertTimetable =
						new SimpleJdbcInsert(getJdbcTemplate().getDataSource())
										.withTableName("timetable")
										.usingGeneratedKeyColumns("id")
										.usingColumns("standard_id", "json", "is_delete");

		Map<String, Object> parameters = new HashMap<String, Object>(19);
		parameters.put("standard_id", timetable.getStandardId());
		parameters.put("json", timetable.getJsonPayload());
		parameters.put("is_delete", "N");

		final Number timetableId = insertTimetable.executeAndReturnKey(parameters);

		saveTimeTableDetails(timetable, branchId, userId, defaultRole, timetableId);

	}

	private void saveTimeTableDetails(Timetable timetable, Integer branchId, Integer userId, String defaultRole, final Number timetableId)
					throws AuthorisationException {
		log.debug("insert divisions if applicable.......");

		Collection<HolidayResponse> holidayResponse = branchDao.getHolidays(userId, null, defaultRole, branchId);
		List<Holiday> holidays = null;

		if (holidayResponse.size() > 0) {
			holidays = holidayResponse.iterator().next().getHolidays();
			log.debug("holidays are ......." + holidays);
		} else {
			holidays = new ArrayList<Holiday>();
			log.debug("holidays are ......." + holidays);
		}

		BranchWorkingDay workingDays = branchDao.getWorkingDay(branchId);

		AcademicYear academicYear = branchDao.getActiveAcademicYear(branchId);

		if (!CollectionUtils.isEmpty(timetable.getDivision())) {

			//gcmIdsMap.putAll(userDao.getGcmIdsOfGroupUsers(sendNotfication.getGroupIds()));

			String timetableDivisionInsert = "INSERT INTO timetable_division (timetable_id, division_id) VALUES (?, ?)";
			final List<Integer> divisionList = timetable.getDivision();
			getJdbcTemplate().batchUpdate(timetableDivisionInsert, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Integer divisionId = divisionList.get(i);
					ps.setLong(1, timetableId.longValue());
					ps.setInt(2, divisionId);
				}

				@Override
				public int getBatchSize() {
					return divisionList.size();
				}
			});
		}

		log.debug("insert time table details.......");

		Date startDate = TimeTableGenerator.parseDate(academicYear.getFromDate(), TimeTableGenerator.formatterDDMMYYYY);
		Date endDate = TimeTableGenerator.parseDate(academicYear.getToDate(), TimeTableGenerator.formatterDDMMYYYY);

		if (!StringUtils.isEmpty(timetable.getStartDate())) {
			Date timeTableStartDate = TimeTableGenerator.parseDate(timetable.getStartDate(), TimeTableGenerator.formatterDDMMYYYY);
			if (timeTableStartDate.getTime() < startDate.getTime()) {
				throw new ApplicationException("Timetable start date cannt be before academic year start date");
			} else {
				startDate = timeTableStartDate;
				System.out.println("startDate override......." + startDate.toString());
			}
		}

		if (!StringUtils.isEmpty(timetable.getEndDate())) {
			Date timeTableEndDate = TimeTableGenerator.parseDate(timetable.getEndDate(), TimeTableGenerator.formatterDDMMYYYY);
			if (timeTableEndDate.getTime() > endDate.getTime()) {
				throw new ApplicationException("Timetable end date cannot be after academic year end date");
			} else {
				endDate = timeTableEndDate;
				System.out.println("endDate override......." + endDate.toString());
			}
		}

		final List<TimeTableEntry> timetableEntries = TimeTableGenerator.getTimeTableEntry(workingDays, holidays, timetable.getDetail(), startDate, endDate);

		if (!CollectionUtils.isEmpty(timetableEntries)) {

			//gcmIdsMap.putAll(userDao.getGcmIdsOfGroupUsers(sendNotfication.getGroupIds()));

			final SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm");

			String timetableDivisionInsert = "INSERT INTO timetable_detail (title, date, starttime, endtime, status, timetable_id, is_delete, _id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
			getJdbcTemplate().batchUpdate(timetableDivisionInsert, new BatchPreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					TimeTableEntry timetableEntry = timetableEntries.get(i);
					//System.out.println("timetableEntry >> " + timetableEntry.toString());
					ps.setString(1, timetableEntry.getTitle());
					ps.setDate(2, new java.sql.Date(timetableEntry.getDate().getTime()));

					try {
						//Time startTime = new Time(parseFormat.parse(timetableEntry.getStarttime()).getTime());
						//Time endTime = new Time(parseFormat.parse(timetableEntry.getEndtime()).getTime());
						//System.out.println("startTime >> " + startTime.toString());
						//System.out.println("endTime >> " + endTime.toString());
						ps.setTime(3, Time.valueOf(timetableEntry.getStarttime() + ":00"));
						ps.setTime(4, Time.valueOf(timetableEntry.getEndtime() + ":00"));
					} catch (Exception e) {
						e.printStackTrace();
						new SQLException();
					}

					ps.setString(5, "N");
					ps.setLong(6, timetableId.longValue());
					ps.setString(7, "N");
					ps.setString(8, RandomStringUtils.random(11, false, true));
				}

				@Override
				public int getBatchSize() {
					return timetableEntries.size();
				}
			});
		}
	}

	public void setBranchDAO(BranchDao branchDao) {
		this.branchDao = branchDao;
	}

	@Override
	@Transactional
	public void updateTimeTable(Timetable timetable, Integer timetableId, Integer branchId, Integer userId, String defaultRole) throws AuthorisationException {

		String updateSql = "update timetable set json= ? where id=?";
		int[] types = { Types.VARCHAR, Types.INTEGER };
		Object[] params = new Object[] { timetable.getJsonPayload(), timetableId };

		int UPDATE_STATUS = getJdbcTemplate().update(updateSql, params, types);

		if (UPDATE_STATUS == 1) {
			updateSql = "update timetable_division set is_delete= ? where timetable_id=?";
			params = new Object[] { "Y", timetableId };
			UPDATE_STATUS = getJdbcTemplate().update(updateSql, params, types);

			updateSql = "update timetable_detail set updated_date=?, is_delete= ? where timetable_id=?";
			types = new int[] { Types.DATE, Types.VARCHAR, Types.INTEGER };
			params = new Object[] { new java.sql.Date(System.currentTimeMillis()), "Y", timetableId };
			UPDATE_STATUS = getJdbcTemplate().update(updateSql, params, types);

			//Save new entries
			saveTimeTableDetails(timetable, branchId, userId, defaultRole, timetableId);
		}

	}

	@Override
	@Transactional
	public void deletTimetable(Integer timetableId) {
		String updateSql = "update timetable set is_delete= ? where id=?";
		int[] types = { Types.VARCHAR, Types.INTEGER };
		Object[] params = new Object[] { "Y", timetableId };
		getJdbcTemplate().update(updateSql, params, types);

		updateSql = "update timetable_division set is_delete= ? where timetable_id=?";
		params = new Object[] { "Y", timetableId };
		getJdbcTemplate().update(updateSql, params, types);

		updateSql = "update timetable_detail set updated_date=?, is_delete= ? where timetable_id=?";
		types = new int[] { Types.DATE, Types.VARCHAR, Types.INTEGER };
		params = new Object[] { new java.sql.Date(System.currentTimeMillis()), "Y", timetableId };
		getJdbcTemplate().update(updateSql, params, types);

	}

	@Override
	public Timetable getTimeTableForSchool(Integer standardId, Integer divisionId) {
		return getTimetable(standardId, divisionId, null);

	}

	private Timetable getTimetable(Integer standardId, Integer divisionId, Integer branchId) {
		String query = "select t.*,GROUP_CONCAT(td.division_id) as divisionList from timetable t left join timetable_division td on td.timetable_id = t.id " +
						" inner join standard s on s.id = t.standard_id " +
						" inner join academic_year ay on ay.branch_id = s.branch_id and ay.id=s.academic_year_id and ay.is_current_active_year ='Y'" +
						" where t.standard_id =:standardId and t.is_delete ='N' ";

		if (divisionId != null && divisionId.intValue() != 0) {
			query += " and td.division_id = :divisionId and td.is_delete = 'N' ";
		} else {
			query += " and td.division_id IS NULL ";
		}

		if (branchId != null && branchId.intValue() != 0) {
			query += " and ay.branch_id = :branchId ";
		}

		Map<String, Object> queryParameters = new HashMap<String, Object>(3);
		queryParameters.put("standardId", standardId);
		queryParameters.put("divisionId", divisionId);
		queryParameters.put("branchId", branchId);

		log.debug("getTimeTable query :: " + query);
		log.debug("getTimeTable query parameters :: " + queryParameters);

		return getNamedParameterJdbcTemplate().queryForObject(query, queryParameters,
						new RowMapper<Timetable>() {
							@Override
							public Timetable mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {

								Timetable timetable = null;

								if (resultSet.getInt("id") != 0) {
									timetable = new Timetable();
									timetable.setStandardId(resultSet.getInt("standard_id"));
									timetable.setJsonPayload(resultSet.getString("json"));

									String division = resultSet.getString("divisionList");
									if (!StringUtils.isEmpty(division)) {
										List<Integer> divisionList = new ArrayList<Integer>();

										String[] divisonId = division.split(",");

										for (String id : divisonId) {
											divisionList.add(new Integer(id));
										}

										timetable.setDivision(divisionList);
									}
								}

								return timetable;

							}
						});
	}

	@Override
	public Timetable getTimeTableForStudent(Integer branchId, Integer studentId) {
		Student student = studentDao.getStudentProfileDetailsbyId(studentId.longValue());

		Integer standardId = student.getEducationDetails().getStandard();
		Integer divisionId = student.getEducationDetails().getDivision();
		return getTimetable(standardId, divisionId, branchId);

	}

	@Override
	public boolean canUserUpdateSpecifiedTimetable(Integer branchId, Integer timetableId) {
		try {
			String query = "select t.id from timetable t inner join standard s on t.standard_id=s.id and s.branch_id = :branchId  where t.id=:timetableId ";
			Map<String, Object> queryParameters = new HashMap<String, Object>();
			queryParameters.put("timetableId", timetableId);
			queryParameters.put("branchId", branchId);

			getNamedParameterJdbcTemplate().queryForObject(query, queryParameters,
							new RowMapper<Integer>() {
								@Override
								public Integer mapRow(ResultSet resultSet, int rowNum)
												throws SQLException {
									return resultSet.getInt("id");
								}
							});

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	@Override
	public Map<String, Integer> markEdiaryAsRead(Integer ediaryId, Integer userId, Integer forUserId) {
		String query = " INSERT INTO user_ediary_read_status (ediary_id, user_id, for_user_id, read_status) " +
						" VALUES(:ediaryId, :userId, :forUserId,'Y') ON DUPLICATE KEY UPDATE " +
						" ediary_id = VALUES(ediary_id), user_id = VALUES(user_id), for_user_id = VALUES(for_user_id), read_status = VALUES(read_status) ";

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("ediaryId", ediaryId);
		parameters.put("userId", userId);
		parameters.put("forUserId", forUserId);

		getNamedParameterJdbcTemplate().update(query, parameters);

		return getUnreadEdiaryCount(userId);

	}

	@Override
	public Map<String, Integer> markEventAsRead(Integer eventId, Integer userId, Integer forUserId, Integer entityId, Integer roleId) {
		String query = " INSERT INTO user_event_read_status (event_id, user_id, for_user_id, read_status,entity_id,role_id ) " +
						" VALUES(:eventId, :userId, :forUserId,'Y',:entityId, :roleId) ON DUPLICATE KEY UPDATE " +
						" event_id = VALUES(event_id), user_id = VALUES(user_id), for_user_id = VALUES(for_user_id), read_status = VALUES(read_status), " +
						" entity_id = VALUES(entity_id), role_id = VALUES(role_id) ";

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("eventId", eventId);
		parameters.put("userId", userId);
		parameters.put("forUserId", forUserId);
		parameters.put("entityId", entityId);
		parameters.put("roleId", roleId);

		getNamedParameterJdbcTemplate().update(query, parameters);

		return getUnreadEventCount(userId, roleId);
	}

	@Override
	public Map<String, Integer> getUnreadEdiaryCount(Integer userId) {

		String retriveUnreadEdiaryQuery = "select " +
						" (" +
						" IFNULL(count(*),0) " +
						" - " +
						" IFNULL( (SELECT COUNT(*) FROM user_ediary_read_status uers WHERE uers.user_id=ue.user_id and uers.read_status = 'Y'),0) " +
						" ) AS unread_count " +
						" from user_ediary ue inner join ediary e on e.id= ue.ediary_id " +
						" where ue.user_id = :userId and e.created_by != :userId";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);

		return getNamedParameterJdbcTemplate().query(retriveUnreadEdiaryQuery, queryParameters,
						new ResultSetExtractor<Map<String, Integer>>() {
							@Override
							public Map<String, Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
								Map<String, Integer> map = new HashMap<String, Integer>();

								while (rs.next()) {
									int count = (rs.getInt("unread_count") < 0) ? 0 : rs.getInt("unread_count");
									map.put("ediary", new Integer(count));
								}
								return map;
							}
						});

	}

	@Override
	public Map<String, Integer> getUnreadEventCount(Integer userId, Integer roleId) {

		String retriveUnreadEdiaryQuery = "select " +
						" (" +
						" IFNULL(count(*),0) " +
						" - " +
						" IFNULL( (SELECT COUNT(*) FROM user_event_read_status uers WHERE uers.user_id=ue.user_id and uers.read_status = 'Y'),0) " +
						" ) AS unread_count " +
						" from user_event ue " +
						" where ue.user_id = :userId and ue.role_id = :roleId and ue.status != 'D' " +
						" group by ue.user_id ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("userId", userId);
		queryParameters.put("roleId", roleId);

		return getNamedParameterJdbcTemplate().query(retriveUnreadEdiaryQuery, queryParameters,
						new ResultSetExtractor<Map<String, Integer>>() {
							@Override
							public Map<String, Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
								Map<String, Integer> map = new HashMap<String, Integer>();

								while (rs.next()) {
									int count = (rs.getInt("unread_count") < 0) ? 0 : rs.getInt("unread_count");
									map.put("event", new Integer(count));
								}
								return map;
							}
						});

	}
}
