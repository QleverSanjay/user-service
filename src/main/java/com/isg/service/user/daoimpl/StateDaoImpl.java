package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.StateDao;
import com.isg.service.user.request.State;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class StateDaoImpl extends JdbcDaoSupport implements StateDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<State> getStates() {
		String retriveState = "select * from state where active = 'Y'";
		return getJdbcTemplate().query(retriveState, new BeanPropertyRowMapper<State>(State.class));
	}

	@Override
	public State findStateById(int id) {
		String retriveState = "select * from state where id = ? and active='Y'";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().queryForObject(retriveState, inputs,new BeanPropertyRowMapper<State>(State.class));
}

}
