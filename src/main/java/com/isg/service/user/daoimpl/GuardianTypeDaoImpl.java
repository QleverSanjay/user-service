package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.GuardianTypeDao;
import com.isg.service.user.request.GuardianType;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class GuardianTypeDaoImpl extends JdbcDaoSupport implements GuardianTypeDao{
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<GuardianType> getAll() {
		String retriveGuardian = "select * from guardian_type";
		return getJdbcTemplate().query(retriveGuardian, new BeanPropertyRowMapper<GuardianType>(GuardianType.class));
	}

}
