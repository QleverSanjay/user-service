package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.identity.model.Identity.ROLES_ID;
import com.isg.service.user.dao.ChatDao;
import com.isg.service.user.dao.EventDao;
import com.isg.service.user.dao.FeeDetailsDao;
import com.isg.service.user.dao.ForumDao;
import com.isg.service.user.dao.NotificationDao;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.FeesSchedule;
import com.isg.service.user.model.GroupUser;
import com.isg.service.user.model.PaymentAccount;
import com.isg.service.user.model.StandardDivision;
import com.isg.service.user.model.StudentDetail;
import com.isg.service.user.model.StudentRegistration;
import com.isg.service.user.model.onlineadmission.Category;
import com.isg.service.user.request.Division;
import com.isg.service.user.request.Goal;
import com.isg.service.user.request.Parent;
import com.isg.service.user.request.Passbook;
import com.isg.service.user.request.PaymentTransaction;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.StudentAddress;
import com.isg.service.user.request.StudentBasicDetails;
import com.isg.service.user.request.StudentDirect;
import com.isg.service.user.request.StudentEducationDetails;
import com.isg.service.user.request.StudentPersonDetails;
import com.isg.service.user.request.StudentRowMapper;
import com.isg.service.user.response.FindStudentProfileMatchesForAParent;
import com.isg.service.user.response.StudentBasicProfile;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.DateFormatter;
import com.isg.service.user.util.DateUtil;
import com.isg.service.user.util.FileStorePathAssembler;
import com.isg.service.user.util.SecurityUtil;
import com.jolbox.bonecp.BoneCPDataSource;


@Slf4j
@Repository
public class StudentDaoImpl extends BaseRegistrationDaoImpl implements StudentDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private static final Logger log = Logger.getLogger(StudentDaoImpl.class);

	private static final String RETRIEVE_GOAL_DETAIL = "select g.*, s.first_name as student_name, s.gender as student_gender, s.user_id as student_user_id, " +
					" DATE_FORMAT(g.start_date, '%d-%m-%Y') as formatted_start_date, "
					+ " DATE_FORMAT(g.end_date, '%d-%m-%Y') as formatted_end_date, DATE_FORMAT(g.created_date, '%d-%m-%Y') as formatted_created_date " +
					" from goals g inner join student s on g.student_id= s.id ";

	@Autowired
	@Lazy
	private ChatDao chatDao;

	@Autowired
	private ForumDao forumDao;

	@Autowired
	private ParentDao parentDao;

	@Autowired
	@Lazy
	private UserDao userDao;

	@Autowired
	private NotificationDao notificationDao;

	@Autowired
	private EventDao eventDao;

	@Autowired 
	FeeDetailsDao feeDetailDao;
	/**
	 * This method is called to get list of students based on parent id.
	 */
	@Override
	public List<FindStudentProfileMatchesForAParent> getUnlinkedStudentByParentId(Integer id) {
		List<FindStudentProfileMatchesForAParent> returnStudentList = new ArrayList<>();
		/**
		 * Method called to get parent object based on parent id.
		 */
		Parent parent = parentDao.getParent(id);

		if (parent != null && parent.getEmail() != null &&
						parent.getPrimaryContact() != null) {
			/**
			 * Query student upload table to get list of students based on
			 * parent mobile or parent email address.
			 */
			String query = "select s.id as id,CONCAT(first_name,' ', last_name) as name, "
							+ " b.name as institute_name, std.displayName as standard_name,"
							+ " d.displayName as division_name, s.roll_number as roll_number, "
							+ " exists (select 1 from student where mobile = :mobileNumber and id=s.id) as mobile_match_found,"
							+ " exists (select 1 from student where email_address = :email and id=s.id) as email_match_found"
							+ " from student s inner join branch b on s.branch_id= b.id "
							+ " inner join standard std on s.standard_id = std.id "
							+ " inner join division d on d.id = s.division_id"
							+ " where (mobile = :mobileNumber or email_address = :email) "
							+ " and s.isDelete != 'Y' "
							+ " and NOT EXISTS (select student_id from branch_student_parent bsp where bsp.student_id != s.id and bsp.branch_id!=s.branch_id and bsp.parent_id!=:entityId)";

			Map<String, Object> queryParameters = new HashMap<String, Object>();
			queryParameters.put("mobileNumber", parent.getPrimaryContact());
			queryParameters.put("email", parent.getEmail());
			queryParameters.put("entityId", id);

			SqlParameterSource namedParameters = new MapSqlParameterSource(queryParameters);

			returnStudentList = getNamedParameterJdbcTemplate().query(query, namedParameters, new RowMapper<FindStudentProfileMatchesForAParent>() {
				@Override
				public FindStudentProfileMatchesForAParent mapRow(ResultSet resultSet, int rowNum)
								throws SQLException {
					return new FindStudentProfileMatchesForAParent(
									resultSet.getString("id"),
									resultSet.getString("name"),
									resultSet.getString("institute_name"),
									resultSet.getString("standard_name"),
									resultSet.getString("division_name"),
									resultSet.getString("roll_number"),
									resultSet.getInt("mobile_match_found"),
									resultSet.getInt("email_match_found"));
				}
			});
		}

		return returnStudentList;
	}

	@Override
	public Student getUploadedStudentById(Long studentId) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		Student studentObj = null;
		if (studentId != null) {
			try {
				String query = "select student.*, branch.name as branch_name, standard.displayName as standard_name, division.displayName as division_name " +
								" from student " +
								" left join branch on student.branch_id = branch.id " +
								" left join standard on student.standard_id= standard.id " +
								" left join division on student.division_id = division.id " +
								" where student.id = ?";
				Object[] inputs = new Object[] { studentId };
				studentObj = jdbcTemplate.queryForObject(query, inputs,
								new StudentRowMapper(false));
			} catch (EmptyResultDataAccessException e) {
				throw new ApplicationException("Student is not available.");
			}
		}
		return studentObj;

	}

	/**
	 * This method is called to save student into student database.
	 * 
	 * @param student
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	@Override
	@Transactional(readOnly = false)
	@Deprecated
	public void saveStudent(Student student) throws IOException, NoSuchAlgorithmException {

		String username = student.getPersonalDetails().getName().replaceAll(" ", "") + RandomStringUtils.random(6, false, true)
						+ "@" + Constant.EDUQFIX_DOMAIN;
		Long userId = createUser(username, RandomStringUtils.random(8, true, true).toUpperCase(), ROLES_ID.STUDENT.get(),
						Constant.AUTHENTICATION_TYPE.APPLICATION.get());

		log.debug("Created new user ::" + userId);

		SimpleJdbcInsert insertStudent =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("student")
										.usingColumns("first_name", "last_name", "dob", "relation_with_parent", "email_address", "mobile", "registration_code",
														"gender",
														"line1", "area", "city", "pincode", "religion", "caste_id", "parent_id", "photo", "branch_id",
														"standard_id", "division_id", "roll_number", "goal_child", "goal_parent",
														"hobbies", "student_upload_id", "user_id", "state_id", "district_id", "taluka_id")
										.usingGeneratedKeyColumns("id");

		Map<String, Object> parameters = new HashMap<String, Object>(25);

		String studentName = student.getPersonalDetails().getName();
		String[] name = studentName.split(" ");

		parameters.put("first_name", name[0]);

		if (name.length > 1) {
			parameters.put("last_name", name[1]);
		} else {
			parameters.put("last_name", "");
		}
		parameters.put("dob", DateUtil.parseDateInDBFormat(student.getPersonalDetails().getDob()));
		parameters.put("relation_with_parent", student.getPersonalDetails().getRelationWithParent());
		parameters.put("email_address", student.getPersonalDetails().getEmailAddress());
		parameters.put("mobile", student.getPersonalDetails().getMobile());
		parameters.put("registration_code", student.getPersonalDetails().getRegistrationCode());
		parameters.put("gender", student.getPersonalDetails().getGender());

		StudentAddress studentAddress = student.getPersonalDetails().getAddress();

		if (studentAddress != null) {
			parameters.put("line1", student.getPersonalDetails().getAddress().getLine1());
			parameters.put("area", student.getPersonalDetails().getAddress().getArea());
			parameters.put("city", student.getPersonalDetails().getAddress().getCity());
			parameters.put("pincode", student.getPersonalDetails().getAddress().getPincode());
		} else {
			parameters.put("line1", null);
			parameters.put("area", null);
			parameters.put("city", null);
			parameters.put("pincode", null);
		}

		parameters.put("religion", student.getPersonalDetails().getReligion());
		parameters.put("caste_id", student.getPersonalDetails().getCasteId());
		parameters.put("parent_id", student.getParentId());
		parameters.put("photo", FileStorePathAssembler.NO_PROFILE_IMAGE_RELATIVE_PATH);
		parameters.put("branch_id", student.getEducationDetails().getBranchId());
		parameters.put("standard_id", student.getEducationDetails().getStandard());
		parameters.put("division_id", student.getEducationDetails().getDivision());
		parameters.put("roll_number", student.getEducationDetails().getRollNumber());
		parameters.put("goal_child", student.getOtherDetails().getGoalChild());
		parameters.put("goal_parent", student.getOtherDetails().getGoalParent());
		parameters.put("goal_parent", student.getOtherDetails().getGoalParent());
		parameters.put("hobbies", student.getOtherDetails().getHobbies());
		parameters.put("student_upload_id", student.getStudentUploadId());
		parameters.put("user_id", userId);
		parameters.put("state_id", student.getPersonalDetails().getAddress().getStateId());
		parameters.put("district_id", student.getPersonalDetails().getAddress().getDistrictId());
		parameters.put("taluka_id", student.getPersonalDetails().getAddress().getTalukaId());
		log.debug("Save sttudent paremeters ::" + parameters);

		Long studentId = (Long) insertStudent.executeAndReturnKey(parameters);
		Integer branchId = student.getEducationDetails().getBranchId();

		if (student.getParentId() != null && branchId != null && branchId != 0) {
			// Attach chat groups to this parent
			try {
				/*
				 * List<GroupUser> groupUserList =
				 * userDao.retrieveUnassignedStudentGroupsForParents
				 * (student.getParentId());
				 * chatDao.assignStudentChatGroupsToParent(student,
				 * groupUserList);
				 * 
				 * // Create Forum Account for parent
				 * forumDao.setupAccountForParent(studentId.intValue(),
				 * branchId, student.getParentId(), groupUserList);
				 */
			} catch (Exception e) {
				// Catch any failures to assign groups to parent or forum's to
				// parent to avoid system failures. Business decision needs to
				// be taken to address this.
				log.error("Error during group assignment / forum assignment to parent ::" + e.getMessage(), e);

			}
		}

	}

	@Override
	public List<StudentBasicProfile> getStudentBasicProfileDetails(long parentId) {

		String query = "select s.* from student s inner join branch_student_parent bsp on bsp.student_id = s.id " +
						" where bsp.parent_id=? ";
		Object[] inputs = new Object[] { parentId };
		List<StudentBasicProfile> userChatProfileList = getJdbcTemplate().query(query, inputs,
						new RowMapper<StudentBasicProfile>() {
							@Override
							public StudentBasicProfile mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								return new StudentBasicProfile(
												resultSet.getLong("id"),
												resultSet.getString("first_name"),
												resultSet.getString("photo"),
												resultSet.getString("gender"));
							}
						});

		return userChatProfileList;
	}

	/**
	 * This method is used to get student object.
	 * 
	 * @param studentId
	 * @return
	 */
	@Override
	public Student getStudentProfileDetailsbyId(long studentId) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		List<Student> studentObj = new ArrayList<>();

		String query = "select u.username,s.*, b.name as branch_name, st.displayName as standard_name, d.displayName as division_name, " +
						" bc.show_logo_on_parent_portal as showBranchLogo, " +
						" (CASE WHEN bc.show_logo_on_parent_portal = 'Y' THEN b.logo_url ELSE null END) as branchLogoUrl "+
						" from student  s inner join user  u on u.id=s.user_id" +
						" left join branch b on s.branch_id = b.id" +
						" left join standard st on s.standard_id= st.id" +
						" left join division d on s.division_id = d.id" +
						" left join branch_configuration bc on b.id = bc.branch_id " + 
						" where s.id =? and s.isDelete='N'";
		Object[] inputs = new Object[] { studentId };
		studentObj = jdbcTemplate.query(query, inputs, new StudentRowMapper(true));

		return !studentObj.isEmpty() ? studentObj.get(0) : null;
	}

	/**
	 * This method is used to get student object.
	 * 
	 * @param studentId
	 * @return
	 */
	@Override
	public List<StudentDetail> getStudentNamebyIds(String studentIdsStr) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		List<StudentDetail> studentObj = new ArrayList<>();

		String students = org.apache.commons.lang.StringUtils.join(studentIdsStr.split(","));
		String query = "select concat(s.first_name, ' ', s.last_name) as name,st.displayName as standard, "
						+ "d.displayName as division,s.registration_code,s.roll_number, "
						+ "concat(if(st.displayName is null , '', concat(st.displayName,' - ')), "
						+ "if(d.displayName is null , '', concat(d.displayName,' - ')), ifnull(s.roll_number, ifnull(s.registration_code, ''))) as unique_number"
						+
						" from student  s inner join user  u on u.id=s.user_id" +
						" left join branch b on s.branch_id = b.id" +
						" left join standard st on s.standard_id= st.id" +
						" left join division d on s.division_id = d.id" +
						" where s.id in (" + studentIdsStr + ") and s.isDelete='N' and s.active= 'Y'";

		System.out.println(query);
		studentObj = jdbcTemplate.query(query, new RowMapper<StudentDetail>() {

			@Override
			public StudentDetail mapRow(ResultSet rs, int arg1) throws SQLException {
				StudentDetail sd = new StudentDetail();
				sd.setName(rs.getString("name"));
				sd.setDivision(rs.getString("division"));
				sd.setStandard(rs.getString("standard"));
				sd.setRegistrationCode(rs.getString("registration_code"));
				sd.setRollNo(rs.getString("roll_number"));
				sd.setCustomerUniqueNo(rs.getString("unique_number"));

				return sd;
			}

		});

		return studentObj;
	}

	@Override
	public List<Student> listByParentId(Integer parentId) {
		String query = "select s.*, branch.name as branch_name, standard.displayName as standard_name, division.displayName as division_name, bc.show_logo_on_parent_portal as showBranchLogo, branch.logo_url as branchLogoUrl " +
						" from student s " +
						" inner join branch_student_parent bsp on bsp.student_id = s.id " +
						" join branch_configuration bc on bc.branch_id = s.branch_id "+
						" left join branch on s.branch_id = branch.id " + 
						" left join standard on s.standard_id= standard.id " +
						" left join division on s.division_id = division.id " +
						" where bsp.parent_id=? and bsp.isDelete='N' and s.isDelete='N'";
		Object[] inputs = new Object[] { parentId };
		List<Student> studentList = getJdbcTemplate().query(query, inputs, new StudentRowMapper(false));
		return studentList;
	}


	@Override
	@Transactional(readOnly = false)
	public void updateStudent(Student student, String defaultRole, Integer entityId, Integer userId) throws IOException {

		log.debug("Student record for update :::" + student.toString());

		String updateStudentQuery = "update student set first_name = ?, last_name=?," +
						"dob=?, relation_with_parent=?, email_address= ?, mobile =?, " +
						"registration_code =?, gender =?, line1 =?, area=?, city=?, pincode=?, religion=?, " +
						"caste_id=?, photo=?, branch_id=?, standard_id =?, " +
						"division_id=?, roll_number=?, goal_child=?, goal_parent=?, hobbies=?, parent_id=?, state_id=?, district_id =?, taluka_id =? " +
						" where id = ?";

		String addressLine1 = null;
		String area = null;
		String city = null;
		String pincode = null;

		StudentAddress studentAddress = student.getPersonalDetails().getAddress();

		if (studentAddress != null) {
			addressLine1 = student.getPersonalDetails().getAddress().getLine1();
			area = student.getPersonalDetails().getAddress().getArea();
			city = student.getPersonalDetails().getAddress().getCity();
			pincode = student.getPersonalDetails().getAddress().getPincode();
		}

		String name[] = student.getPersonalDetails().getName().split(" ");

		/*String firstName = name[0];
		String lastName = null;

		/*if (name.length > 1) {
			log.debug("student last name" + name[1]);
			lasteName = name[1];
		}*/

		Object[] inputs = new Object[] {
						student.getPersonalDetails().getFirstName(),
						student.getPersonalDetails().getLastName(),
						StringUtils.isEmpty(student.getPersonalDetails().getDob()) ? null : student.getPersonalDetails().getDob(),
						student.getPersonalDetails().getRelationWithParent(),
						StringUtils.isEmpty(student.getPersonalDetails().getEmailAddress()) ? null : student.getPersonalDetails().getEmailAddress(),
						StringUtils.isEmpty(student.getPersonalDetails().getMobile()) ? null : student.getPersonalDetails().getMobile(),
						student.getPersonalDetails().getRegistrationCode(),
						student.getPersonalDetails().getGender(),
						addressLine1,
						area,
						city,
						pincode,
						student.getPersonalDetails().getReligion(),
						student.getPersonalDetails().getCasteId(),
						(student.getPersonalDetails().getPhoto() == null) ? FileStorePathAssembler.NO_PROFILE_IMAGE_RELATIVE_PATH : student
										.getPersonalDetails().getPhoto(),
						student.getEducationDetails().getBranchId(),
						student.getEducationDetails().getStandard(),
						student.getEducationDetails().getDivision(),
						student.getEducationDetails().getRollNumber(),
						student.getOtherDetails().getGoalChild(),
						student.getOtherDetails().getGoalParent(),
						student.getOtherDetails().getHobbies(),
						(student.getParentId() != null && student.getParentId() == 0) ? null : student.getParentId(),
						student.getPersonalDetails().getAddress().getStateId(),
						student.getPersonalDetails().getAddress().getDistrictId(),
						student.getPersonalDetails().getAddress().getTalukaId(),
						student.getPersonalDetails().getId()
		};
		for (Object input : inputs) {
			log.debug("inputs for update :::" + ((input == null) ? null : input.toString()));
		}

		int[] dataType = new int[] {
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.INTEGER,
						Types.VARCHAR,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.VARCHAR,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER,
						Types.INTEGER

		};

		getJdbcTemplate().update(updateStudentQuery, inputs, dataType);

		Integer branchId = student.getEducationDetails().getBranchId();

		if (branchId != null && branchId.intValue() != 0 && "PARENT".equalsIgnoreCase(defaultRole)) {

			Integer parentId = entityId;
			Integer parentUserId = userId;

			// Insert mapping for branch student and parent combination if not exists
			boolean isStudentAlreadyLinkedToParent = isStudentAlreadyLinkedToParent(parentId, branchId, student.getPersonalDetails().getId());

			if (!isStudentAlreadyLinkedToParent) {

				createParentStudentBranchMapping(parentId, branchId, student.getPersonalDetails().getId());

				// Attach chat groups to this parent
				try {
					List<GroupUser> groupUserList = userDao
									.retrieveUnassignedChatGroupsForParents(parentId, parentUserId, student.getPersonalDetails().getId());

					List<String> forumList = userDao.retrieveUnassignedForumsForParents(parentId, parentUserId, student.getPersonalDetails().getId());

					if (!CollectionUtils.isEmpty(groupUserList)) {
						chatDao.assignStudentChatGroupsToParent(parentId, parentUserId, groupUserList);
					}

					// Create Forum Account for parent
					if (!CollectionUtils.isEmpty(forumList)) {
						forumDao.setupAccountForParent(student.getPersonalDetails().getId(), branchId, parentId, forumList);
					}

					// Assign notifications if any
					notificationDao.assignStudentNoticationsToParent(student.getUserId(), parentUserId);

					// Assign any events
					eventDao.assignStudentEventsToParent(student.getUserId(), parentUserId, parentId);

				} catch (Exception e) {
					// Catch any failures to assign groups to parent or forum's to
					// parent to avoid system failures. Business decision needs to
					// be taken to address this.
					log.error("Error during group assignment / forum assignment to parent ::" + e.getMessage(), e);

				}

			}

		}

		log.info("Student updated successfully");

	}

	private void createParentStudentBranchMapping(Integer parentId, Integer branchId, Integer studentId) {

		SimpleJdbcInsert insertParentStudentBranch =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("branch_student_parent")
										.usingColumns("branch_id", "student_id", "parent_id");

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("branch_id", branchId);
		parameters.put("student_id", studentId);
		parameters.put("parent_id", parentId);

		insertParentStudentBranch.execute(parameters);

	}

	private boolean isStudentAlreadyLinkedToParent(Integer parentId, Integer branchId, Integer studentId) {
		boolean isStudentAlreadyLinkedToParent = false;
		String query = "select id from branch_student_parent bsp where bsp.student_id=? and bsp.parent_id = ? and bsp.branch_id = ? and bsp.isDelete = 'N'";

		Object[] inputs = new Object[] { studentId, parentId, branchId };
		List<Integer> studentList = getJdbcTemplate().query(query, inputs, new RowMapper<Integer>() {

			@Override
			public Integer mapRow(ResultSet rs, int rownumber) throws SQLException {
				return rs.getInt("id");
			}
		});

		if (!CollectionUtils.isEmpty(studentList)) {
			isStudentAlreadyLinkedToParent = true;
		}

		return isStudentAlreadyLinkedToParent;
	}

	@Override
	public boolean canAccessDetails(Integer entityId, Integer studentId) {
		String query = "select id from branch_student_parent bsp where bsp.student_id=? and bsp.parent_id = ? and bsp.isDelete = 'N'";

		Object[] inputs = new Object[] { studentId, entityId };
		List<Integer> studentList = getJdbcTemplate().query(query, inputs, new RowMapper<Integer>() {

			@Override
			public Integer mapRow(ResultSet rs, int rownumber) throws SQLException {
				return rs.getInt("id");
			}
		});

		return (studentList != null && studentList.size() > 0) ? true : false;
	}

	@Override
	public List<StudentBasicDetails> getAssociatedStudentList(Integer branchId, String searchText, String exludeIds) {

		searchText = searchText + "%";

		String query = "select s.*,d.displayName as divison_name, std.displayName as standard_name " +
						" from student s " +
						" inner join division d on s.division_id = d.id" +
						" inner join standard std on s.standard_id = std.id where s.branch_id = :branchId " +
						" and (s.first_name like (:searchText) or s.last_name like (:searchText))";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("branchId", branchId);
		queryParameters.put("searchText", searchText);

		if (!StringUtils.isEmpty(exludeIds)) {
			query += " and user_id not in (" + exludeIds + ")";
		}
		log.debug("Query :: " + query);
		List<StudentBasicDetails> studentList = getNamedParameterJdbcTemplate().query(query, queryParameters,
						new RowMapper<StudentBasicDetails>() {
							@Override
							public StudentBasicDetails mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								StudentBasicDetails studentBasicDetails = new StudentBasicDetails();
								studentBasicDetails.setName(resultSet.getString("first_name") + " " + (resultSet.getString("last_name") != null ? resultSet
												.getString("last_name") : ""));
								studentBasicDetails.setDivision(resultSet.getString("divison_name"));
								studentBasicDetails.setStandard_name(resultSet.getString("standard_name"));
								studentBasicDetails.setId(resultSet.getInt("user_id"));
								studentBasicDetails.setRollNumber(resultSet.getString("roll_number"));
								return studentBasicDetails;
							}
						});

		return studentList;

	}

	@Override
	public StudentBasicDetails getStudentDetailsForNotification(Integer studentId) {

		String query = "select s.*,d.displayName as divison_name, std.displayName as standard_name, i.logo_url, b.name as branch_name" +
						" from student s " +
						" inner join branch b on s.branch_id = b.id" +
						" inner join institute i on b.institute_id = i.id" +
						" inner join division d on s.division_id = d.id" +
						" inner join standard std on s.standard_id = std.id where s.id = :studentId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("studentId", studentId);

		log.debug("Query :: " + query);
		StudentBasicDetails studentBasicDetails = getNamedParameterJdbcTemplate().queryForObject(query, queryParameters,
						new RowMapper<StudentBasicDetails>() {
							@Override
							public StudentBasicDetails mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								StudentBasicDetails studentBasicDetails = new StudentBasicDetails();
								studentBasicDetails.setName(resultSet.getString("first_name") + " " + (resultSet.getString("last_name") != null ? resultSet
												.getString("last_name") : ""));
								studentBasicDetails.setDivision(resultSet.getString("divison_name"));
								studentBasicDetails.setStandard_name(resultSet.getString("standard_name"));
								studentBasicDetails.setId(resultSet.getInt("user_id"));
								studentBasicDetails.setRollNumber(resultSet.getString("roll_number"));
								studentBasicDetails.setRegistrationCode(resultSet.getString("registration_code"));
								studentBasicDetails.setMobile(resultSet.getString("mobile"));
								studentBasicDetails.setEmail(resultSet.getString("email_address"));
								studentBasicDetails.setBranchName(resultSet.getString("branch_name"));
								studentBasicDetails.setInstituteLogoUrl(resultSet.getString("logo_url"));
								return studentBasicDetails;
							}
						});

		return studentBasicDetails;

	}

	@Override
	public List<Goal> getGoalDetails(String role, Integer entityId, Integer studentId, long lastSyncTimeStamp) throws AuthorisationException {
		String query = RETRIEVE_GOAL_DETAIL;
		Map<String, Object> queryParameters = new HashMap<String, Object>();

		if ("PARENT".equalsIgnoreCase(role)) {
			query += " where g.parent_id = :entityId ";
			queryParameters.put("entityId", entityId);
			if (studentId != null && canAccessDetails(entityId, studentId)) {
				query += " and g.student_id = :studentId ";
				queryParameters.put("studentId", studentId);
			}
		} else if ("STUDENT".equalsIgnoreCase(role)) {
			query += " where g.student_id = :entityId ";
			queryParameters.put("entityId", entityId);
		} else {
			throw new AuthorisationException("Function not allowed.");
		}

		if (lastSyncTimeStamp != 0) {
			java.sql.Date lastSyncDate = new java.sql.Date(lastSyncTimeStamp);
			query += " and (g.created_date > :lastSyncDate or (g.updated_date IS NOT NULL && g.updated_date > :lastSyncDate)) ";
			queryParameters.put("lastSyncDate", lastSyncDate);
		}

		query += "order by g.student_id, g.created_date, g.updated_date";

		log.debug("Query :: " + query);

		List<Goal> goalList = getNamedParameterJdbcTemplate().query(query, queryParameters,
						new RowMapper<Goal>() {
							@Override
							public Goal mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								Goal goal = new Goal();
								goal.setId(resultSet.getInt("id"));
								goal.setTitle(resultSet.getString("title"));
								goal.setDescription(resultSet.getString("description"));
								goal.setStartDate(resultSet.getString("formatted_start_date"));
								goal.setEndDate(resultSet.getString("formatted_end_date"));
								goal.setFeedback(resultSet.getString("feedback"));
								goal.setAmount(resultSet.getFloat("reward_amount"));
								goal.setRewardType(resultSet.getString("reward_type"));
								goal.setRewardDescription(resultSet.getString("reward_description"));
								goal.setStatus(resultSet.getString("status_code"));
								goal.setCreatedDate(resultSet.getString("formatted_created_date"));
								goal.setStudentId(resultSet.getInt("student_id"));
								goal.setStudentName(resultSet.getString("student_name"));
								goal.setStudentGender(resultSet.getString("student_gender"));
								return goal;
							}
						});

		return goalList;

	}

	@Override
	public void saveGoal(Integer entityId, Goal goal) throws ParseException {
		SimpleJdbcInsert insertGoal =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("goals")
										.usingColumns("title", "description", "start_date", "end_date", "reward_type", "reward_amount", "reward_description",
														"student_id", "parent_id", "status_code", "created_date");

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("title", goal.getTitle());
		parameters.put("description", goal.getDescription());

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		java.util.Date startDateParsed = format.parse(goal.getStartDate());
		java.sql.Date fromDate = new java.sql.Date(startDateParsed.getTime());

		java.util.Date endDateParsed = format.parse(goal.getEndDate());
		java.sql.Date toDate = new java.sql.Date(endDateParsed.getTime());

		parameters.put("start_date", fromDate);
		parameters.put("end_date", toDate);
		parameters.put("reward_type", goal.getRewardType());
		parameters.put("reward_amount", goal.getAmount());
		parameters.put("reward_description", goal.getRewardDescription());
		parameters.put("student_id", goal.getStudentId());
		parameters.put("parent_id", entityId);
		parameters.put("status_code", "ASSIGNED");
		parameters.put("created_date", new java.sql.Timestamp(System.currentTimeMillis()));

		insertGoal.execute(parameters);

	}

	@Override
	public void updateGoal(String defaultRole, Integer entityId, Integer userId, Integer goalId, String feedback, String status) throws ParseException {

		String query = "update goals set status_code = :status, updated_date= :updatedDate ";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("status", status);
		parameters.put("updatedDate", new java.sql.Timestamp(System.currentTimeMillis()));

		if (!StringUtils.isEmpty(feedback)) {
			query += ", feedback= :feedback ";
			parameters.put("feedback", feedback);
		}

		if ("PARENT".equalsIgnoreCase(defaultRole)) {
			query += "where parent_id = :entityId ";
		} else {
			query += "where student_id = :entityId ";
		}

		parameters.put("entityId", entityId);

		query += "and id= :goalId";
		parameters.put("goalId", goalId);

		getNamedParameterJdbcTemplate().update(query, parameters);

		// Retrieve goal details and add a pass book entry if reward type is
		// award

		if ("ACCEPTED".equalsIgnoreCase(status)) {

			Goal goal = getGoalDetail(goalId);

			if (goal.getRewardType().equalsIgnoreCase("AMOUNT")) {
				Passbook passbook = new Passbook();
				passbook.setTitle("Reward " + (StringUtils.isEmpty(goal.getDescription()) ? "" : (" : " + goal.getDescription())));
				passbook.setDate(DateFormatter.getSystemDateTime(DateFormatter.DATE_FORMAT_YYYYMMDD_HYPHEN_SEPARATED));
				passbook.setSelfExpenseFlag("no");
				passbook.setTransactionType("DEBIT");
				passbook.setAmount(goal.getAmount());
				Map<String, Object> otherDetails = new HashMap<String, Object>();
				otherDetails.put("id", goal.getStudentId());
				passbook.setOtherDetails(otherDetails);
				userDao.addEntryToPassbook(userId, passbook);
			}
		}
	}

	@Override
	public boolean deleteGoal(String defaultRole, Integer entityId, Integer goalId) {
		String query = "Delete from goals where id=:goalId and parent_id=:entityId and status_code = 'ASSIGNED'";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("goalId", goalId);
		parameters.put("entityId", entityId);

		int status = getNamedParameterJdbcTemplate().update(query, parameters);

		if (status == 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public Goal getGoalDetail(Integer goalId) {

		String query = RETRIEVE_GOAL_DETAIL + " where g.id=:goalId";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("goalId", goalId);

		return getNamedParameterJdbcTemplate().queryForObject(query, parameters,
						new RowMapper<Goal>() {
							@Override
							public Goal mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								Goal goal = new Goal();
								goal.setId(resultSet.getInt("id"));
								goal.setTitle(resultSet.getString("title"));
								goal.setDescription(resultSet.getString("description"));
								goal.setStartDate(resultSet.getString("formatted_start_date"));
								goal.setEndDate(resultSet.getString("formatted_end_date"));
								goal.setFeedback(resultSet.getString("feedback"));
								goal.setAmount(resultSet.getFloat("reward_amount"));
								goal.setRewardType(resultSet.getString("reward_type"));
								goal.setRewardDescription(resultSet.getString("reward_description"));
								goal.setStatus(resultSet.getString("status_code"));
								goal.setCreatedDate(resultSet.getString("formatted_created_date"));
								goal.setStudentId(resultSet.getInt("student_id"));
								goal.setStudentName(resultSet.getString("student_name"));
								goal.setStudentGender(resultSet.getString("student_gender"));
								goal.setStudentUserId(resultSet.getInt("student_user_id"));
								return goal;
							}
						});
	}

	@Override
	public StudentDirect searchStudent(Integer branchId,String uniqueId,String uniqueIdName) throws SQLException{
		
		
String searchQuery=null;
		String baseQuery= "select s.*, b.name as branchName, st.name as state_name, dt.name as district_name, "
				+ " std.displayName as standard, d.displayName as division from student s "
				+ " left join branch b on s.branch_id =b.id"
				+ " left join standard std on s.standard_id= std.id and std.active='Y'"
				+ " left join division d  on s.division_id = d.id and d.active='Y'"
				+ " left join state st on st.id=s.state_id"
                + " left join district dt on dt.id = s.district_id";
		
		if(uniqueIdName.equalsIgnoreCase("mobile")){
			searchQuery	= baseQuery + " where s.mobile="+uniqueId+" and s.branch_id="+branchId;
				
		}
		else if(uniqueIdName.equalsIgnoreCase("Registration Number")){
			searchQuery	= baseQuery + " where s.registration_code="+uniqueId+" and s.branch_id="+branchId;
			
		}
		else if(uniqueIdName.equalsIgnoreCase("roll_number")){
			
			searchQuery	= baseQuery + " where s.roll_number="+uniqueId+" and s.branch_id="+branchId;
		}
		else if(uniqueIdName.equalsIgnoreCase("email")){
			
			searchQuery	= baseQuery + " where s.email_address='"+uniqueId+"' and s.branch_id="+branchId;
		}
		else if(uniqueIdName.equalsIgnoreCase("username")){
			
			searchQuery	= baseQuery + " where s.user_id=(select id from user where username='"+uniqueId+"') and s.branch_id="+branchId;
			
		}
		log.info(searchQuery);
		
		StudentDirect s=getJdbcTemplate().queryForObject(searchQuery, new RowMapper<StudentDirect>() {
			 
            public StudentDirect mapRow(ResultSet rs, int rowNum) throws SQLException {
            	StudentPersonDetails personalDetails = new StudentPersonDetails();
        		StudentAddress address = new StudentAddress();
        		StudentEducationDetails educationalDetails = new StudentEducationDetails();
        		StudentDirect student = new StudentDirect();
        		personalDetails.setId(rs.getInt("id"));
        		personalDetails.setName(rs.getString("first_name") + " " + (StringUtils.isEmpty(rs.getString("last_name")) ? "" : rs.getString("last_name")));
        		personalDetails.setFirstName(rs.getString("first_name"));
        		personalDetails.setLastName(rs.getString("last_name"));
        		String dob = rs.getDate("dob") != null ? rs.getDate("dob").toString() : "";
        		personalDetails.setDob(dob);
        		personalDetails.setEmailAddress(rs.getString("email_address"));
        		personalDetails.setMobile(rs.getString("mobile"));
        		personalDetails.setRegistrationCode(rs.getString("registration_code"));
        		address.setLine1(rs.getString("line1"));
        		address.setArea(rs.getString("area"));
        		address.setCity(rs.getString("city"));
        		address.setPincode(rs.getString("pincode"));
        		address.setDistrictId(rs.getInt("district_id") == 0 ? null : rs.getInt("district_id"));
        		address.setTalukaId(rs.getInt("taluka_id") == 0 ? null : rs.getInt("taluka_id"));
        		address.setStateId(rs.getInt("state_id") == 0 ? null : rs.getInt("state_id"));
        		educationalDetails.setBranchId(rs.getInt("branch_id") == 0 ? null : rs.getInt("branch_id"));
        		educationalDetails.setStandard(rs.getInt("standard_id") == 0 ? null : rs.getInt("standard_id"));
        		educationalDetails.setDivision(rs.getInt("division_id") == 0 ? null : rs.getInt("division_id"));
        		educationalDetails.setRollNumber(rs.getString("roll_number"));
        		educationalDetails.setBranchName(rs.getString("branchName"));
        		educationalDetails.setStandardName(rs.getString("standard"));
        		educationalDetails.setDivisionName(rs.getString("division"));
        		address.setStateName(rs.getString("state_name"));
        		address.setDistrictName(rs.getString("district_name"));
        		student.setEducationDetails(educationalDetails);
        		personalDetails.setAddress(address);
        		student.setPersonalDetails(personalDetails);
        		student.setUserId(rs.getInt("user_id"));
        		List<PaymentTransaction> feeDues = new ArrayList<PaymentTransaction>();
        		feeDues = feeDetailDao.getFeeDues(rs.getInt("id"), ROLES.STUDENT.get());
        		feeDues.addAll(feeDetailDao.getPaidPaymentTransactions(rs.getInt("id"), rs.getInt("user_id"), ROLES.STUDENT.get()));
        		student.setFeesInfo(feeDues);

                return student;
            }
            
        });
		System.out.println(s);
		return s; 
		
}
	
	@Override
	public List<StandardDivision> getStandardList(final Integer branchId) {	
		final List<StandardDivision>stDivList=new ArrayList<StandardDivision>();
		String getStandards = "select * from standard where active = 'Y' AND branch_id = "+branchId +" " +
				" AND academic_year_id = (select id from academic_year where is_current_active_year = 'Y' AND branch_id = "+branchId+")";
		
	
		
	 getJdbcTemplate().query(getStandards, new RowMapper<StandardDivision>(){

			@Override
			public StandardDivision mapRow(ResultSet rs, int arg1)
					throws SQLException {
				
				while(true){
					StandardDivision stDiv =new StandardDivision();
					stDiv.setStandard(rs.getInt("id"));
					stDiv.setStandardName(rs.getString("displayName"));
					stDiv.setDivision(getDivisionByStandard(rs.getInt("id"),branchId));
					stDivList.add(stDiv);
					
					if(!rs.next()){
						
						break;
					}
				}
				
				return null;
			}
			
			
		});	
		
	
		
		return stDivList;
	}
	
	public List<Division> getDivisionByStandard(Integer standardId,Integer branchId){
		
		final List<Division> divisionList=new ArrayList<Division>();
		
		String getDivisions="select * from division where active = 'Y' AND id in " +
				"(select division_id from standard_division where standard_id = ? )" +
				" AND academic_year_id = (select id from academic_year where is_current_active_year = 'Y' AND branch_id =?)";
		//List<Division> divisionList=new ArrayList<Division>();
		getJdbcTemplate().query(getDivisions,new Object[]{standardId,branchId},new RowMapper<Division>(){
			
			@Override
			public Division mapRow(ResultSet rs, int arg1)
					throws SQLException {
				
				while(true){
					Division div=new Division();
					div.setId(rs.getInt("id"));
					div.setDisplayName(rs.getString("displayName"));
					divisionList.add(div);
					
					if(!rs.next())
						break;
				}
			
				return null;
			}
		
		
		});
		
			return divisionList;
	}


	@Override
	public List<Category> getCategoryList() {
		String getCategory = "select * from caste";
		
		final List<Category> categoryList=new ArrayList<Category>();
	
		
	 getJdbcTemplate().query(getCategory, new RowMapper<Category>(){

			@Override
			public Category mapRow(ResultSet rs, int arg1)
					throws SQLException {
				
				while(true){
					Category category =new Category();
					category.setId(rs.getLong("id"));
					category.setName(rs.getString("name"));
					
					categoryList.add(category);
					
					if(!rs.next())
						break;
				}
				
				return null;
			}
			
			
		});	
		
	
		
		return categoryList;
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Integer insertStudent(final StudentRegistration student) throws Exception {
		
		/*autoMap FeeCodes*/
		Integer userId=null;
		try{
		List<Integer>feesList=student.getFeesCodeId();//return of aditional feeids
		
		userId = insertIntoUser(student);
		
		System.out.println("studentUPLOAD start");

		// insert into student_upload table with userId.
		final int studentUploadId = insertIntoStudentUpload(student, userId);
		System.out.println("Student upload idUUUUUUUUUUUU:::::"+studentUploadId);
		student.setUserId(userId);
		if (studentUploadId != 0) {
			// insert into student table.
			final int studentId = insertIntoStudent(student, studentUploadId,
					userId);
			student.setId(studentId);
			
			createPaymentAccount(userId);

			int standardId = student.getStandardId() != null ? student
					.getStandardId() : 0;
			int divisionId = student.getDivisionId() != null ? student
					.getDivisionId() : 0;
			int casteId = student.getCasteId() != null ? student.getCasteId()
					: 0;
			

			updateFeesSchedule(standardId, divisionId, casteId, feesList,
					student.getUserId(), student.getBranchId(), studentId);
			

			
		}
		
		}catch(Exception e){
			e.printStackTrace();
			
		}
		return userId;

		
		
		
	}


	@Transactional
	public void updateFeesSchedule(int standardId, int divisionId,
			int casteId, List<Integer> feesIdList, final Integer userId,
			Integer branchId, Integer studentId) {
		final List<FeesSchedule> feeSchedules = new ArrayList<>();

		List<FeesSchedule> feesList = getAdditionalApplicableFees(feesIdList);
		if(feesList!=null&&feesList.size()>0){
		for (FeesSchedule fs : feesList) {
			FeesSchedule schedule = new FeesSchedule(0, fs.getFeesId(), userId,
					fs.getBaseAmount(), fs.getBaseAmount(), fs.getDueDate(),
					Constant.PaymentStatus.UNPAID.get(),
					fs.getReminderStartDate(), null, fs.getFeesStartDate());
			feeSchedules.add(schedule);
		}
		}
		List<FeesSchedule> tempFeesSchedule = getApplicableFees(standardId,
				divisionId, casteId, branchId);

		for (FeesSchedule fs : tempFeesSchedule) {
			FeesSchedule schedule = new FeesSchedule(0, fs.getFeesId(), userId,
					fs.getBaseAmount(), fs.getBaseAmount(), fs.getDueDate(),
					Constant.PaymentStatus.UNPAID.get(),
					fs.getReminderStartDate(), null, fs.getFeesStartDate());
			feeSchedules.add(schedule);

		}
		System.out.println("feeSchedules>>>>>>>>>>>" + feeSchedules);
		insertIntoFeesSchedule(feeSchedules);
	}

	public void insertIntoFeesSchedule(final List<FeesSchedule> feeSchedules) {

		if (feeSchedules != null && !feeSchedules.isEmpty()) {

			String sql = "insert into fees_schedule (fees_id, user_id, base_amount, "
					+ " late_payment_charges, total_amount, due_date, status, "
					+ " reminder_start_date, fee_for_duration, fee_start_date) "
					+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			getJdbcTemplate().batchUpdate(sql,
					new BatchPreparedStatementSetter() {

						@Override
						public void setValues(PreparedStatement ps, int i)
								throws SQLException {
							FeesSchedule schedule = feeSchedules.get(i);
							ps.setInt(1, schedule.getFeesId());
							ps.setInt(2, schedule.getUserId());
							ps.setDouble(3, schedule.getBaseAmount());
							ps.setDouble(4, 0.0);
							ps.setDouble(5, schedule.getTotalAmount());
							ps.setDate(6, schedule.getDueDate());
							ps.setString(7, Constant.PaymentStatus.UNPAID.get());
							ps.setDate(8, schedule.getReminderStartDate());
							ps.setString(9, schedule.getFeeForDuration());
							ps.setDate(10, schedule.getFeesStartDate());
						}

						@Override
						public int getBatchSize() {
							return feeSchedules.size();
						}
					});
		}
	}
	
	
	
	// @Transactional
		private void createPaymentAccount(final Integer userId) throws SQLException {

			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(
						Connection connection) throws SQLException {
					String sql = "insert into user_payment_account "
							+ "(account_type, user_id, account_number) values(?, ?, ?)";
					PreparedStatement preparedStmt = connection
							.prepareStatement(sql);
					preparedStmt.setString(1, "QFIXPAY");

					if (userId == null) {
						preparedStmt.setNull(2, Types.NULL);
					} else {
						preparedStmt.setInt(2, userId);
					}
					preparedStmt.setLong(3,
							PaymentAccount.generateAccountNumberWithCheckDigit());
					return preparedStmt;
				}
			});
			logger.debug("payment Account created..." + userId);
		}
	
	private Integer insertIntoUser(final StudentRegistration student)
			throws ApplicationException, NoSuchAlgorithmException {
		final String userSql = "insert into user (username, password, password_salt, temp_password, active, isDelete) "
				+ "values (?, ?, ?, ?, ?, ?)";
		final String password = generatePasswordByNameAndDOB(
				student.getFirstname(), student.getDateOfBirth());

		final byte[] randomSalt = SecurityUtil.generateSalt();
		final byte[] passwordHash = SecurityUtil.hashPassword(password,
				randomSalt);

		System.out.println("username :::: " + student.getUsername());
		// insert into user table.
		KeyHolder holder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatementUser = connection.prepareStatement(
						userSql, Statement.RETURN_GENERATED_KEYS);
				preparedStatementUser.setString(1, student.getStudentUserId());
				preparedStatementUser.setBytes(2, passwordHash);
				preparedStatementUser.setBytes(3, randomSalt);
				preparedStatementUser.setString(4, password);
				preparedStatementUser.setString(5, ("Y".equals(student
						.getCreateStudentLogin()) ? "Y" : "N"));
				preparedStatementUser.setString(6, "N");
				return preparedStatementUser;
			}
		}, holder);
		logger.debug("insert student started..." + student.getStudentUserId());
		student.setPassword(password);
		// get latest insetred user_id from user table.
		final Integer userId = holder.getKey().intValue();
		// insert into user_role
		getJdbcTemplate().update(
				"insert into user_role values(" + userId + ", "
						+ Constant.ROLE_ID_STUDENT + ")");

		return userId;
	}
	
	
	
	// @Transactional
	private int insertIntoStudent(final StudentRegistration student,
			final int studentUploadId, final Integer userId) {
		System.out.println("inside inserInto student tabble");
		final String studentSql = "insert into student (first_name,last_name, "
				+ "gender, dob , email_address, mobile, line1, area, pincode, "
				+ "caste_id, standard_id, division_id,active,registration_code,user_id, "
				+ " branch_id, student_upload_id, isDelete, state_id, district_id,create_login,roll_number) "
				+ "values( ?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

		KeyHolder holder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatement = connection.prepareStatement(
						studentSql, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, student.getFirstname());
				preparedStatement.setString(2, student.getSurname());
				preparedStatement.setString(3, student.getGender());
				preparedStatement.setString(4, StringUtils.isEmpty(student
						.getDateOfBirth()) ? null : student.getDateOfBirth());
			
				preparedStatement.setString(5, student.getEmailAddress());
				preparedStatement.setString(6, student.getMobile());
				preparedStatement.setString(7, student.getAddressLine1());
				preparedStatement.setString(8, student.getArea());
				preparedStatement.setString(9, student.getPincode());

				if (student.getCasteId() == null) {
					preparedStatement.setNull(10, Types.NULL);
				} else {
					preparedStatement.setInt(10, student.getCasteId());
				}

				if (student.getStandardId() == null) {
					preparedStatement.setNull(11, Types.NULL);
				} else {
					preparedStatement.setInt(11, student.getStandardId());
				}

				if (student.getDivisionId() == null) {
					preparedStatement.setNull(12, Types.NULL);
				} else {
					preparedStatement.setInt(12, student.getDivisionId());
				}

				
				preparedStatement.setString(13, (StringUtils.isEmpty(student
						.getActive()) ? "Y" : student.getActive()));
				preparedStatement.setString(14, student.getRegistrationCode());

				if (userId == null) {
					preparedStatement.setNull(15, Types.NULL);
				} else {
					preparedStatement.setInt(15, userId);
				}

			
				preparedStatement.setInt(16, student.getBranchId());
				preparedStatement.setInt(17, studentUploadId);
				preparedStatement.setString(18, "N");

				if (student.getStateId() == null) {
					preparedStatement.setNull(19, Types.NULL);
				} else {
					preparedStatement.setInt(19, student.getStateId());
				}

				if (student.getDistrictId() == null) {
					preparedStatement.setNull(20, Types.NULL);
				} else {
					preparedStatement.setInt(20, student.getDistrictId());
				}
				preparedStatement.setString(21, student.getCreateStudentLogin());

				if (student.getRollNumber() == null) {
					preparedStatement.setNull(22, Types.NULL);
				} else {
					preparedStatement.setString(22, student.getRollNumber());
				}
				return preparedStatement;
			}
		}, holder);
		int studentId = holder.getKey().intValue();
		
		insertIntoContact(student.getFirstname(), student.getSurname(),
				student.getMobile(), student.getEmailAddress(), null, null,
				userId);
		insertFeescodeStudent(student.getFeesCodeId(), studentId);

		return studentId;
	}
	
	public void insertIntoContact(String firstname, String surname,
			String mobile, String emailAddress, String city, String photo,
			Integer userId) {
		String sql = "insert into contact (email, phone, user_id, photo, firstname, lastname, city) values (?, ?, ?, ?, ?, ?, ?)";

		Object[] args = { emailAddress, mobile, userId, photo, firstname,
				surname, city };

		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
				Types.NULL, Types.VARCHAR, Types.VARCHAR, Types.NULL };
		getJdbcTemplate().update(sql, args, types);

		System.out.println("Insert contact done ....");
	}

	
	
	// @Transactional
	private int insertIntoStudentUpload(final StudentRegistration student,
			final Integer userId) {
		
		System.out.println("inside student upload");
		final String studentuploadSql = "insert into student_upload(first_name, last_name, gender, "
				+ "dob , email_address, mobile, line1, area, pincode, "
				+ " caste_id, standard_id, division_id, roll_number, active, registration_code, user_id, "
				+ " branch_id, isDelete, hash_code)"
				+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		KeyHolder holder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatement = connection.prepareStatement(
						studentuploadSql, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, student.getFirstname());
				
				preparedStatement.setString(2, student.getSurname());
				preparedStatement.setString(3, student.getGender());
				preparedStatement.setString(4, StringUtils.isEmpty(student
						.getDateOfBirth()) ? null : student.getDateOfBirth());
			
				preparedStatement.setString(5, student.getEmailAddress());
				preparedStatement.setString(6, student.getMobile());
				preparedStatement.setString(7, student.getAddressLine1());
				preparedStatement.setString(8, student.getArea());
				preparedStatement.setString(9, student.getPincode());
			

				if (student.getCasteId() == null) {
					preparedStatement.setNull(10, Types.NULL);
				} else {
					preparedStatement.setInt(10, student.getCasteId());
				}
				if (student.getStandardId() == null) {
					preparedStatement.setNull(11, Types.NULL);
				} else {
					preparedStatement.setInt(11, student.getStandardId());
				}
				if (student.getDivisionId() == null) {
					preparedStatement.setNull(12, Types.NULL);
				} else {
					preparedStatement.setInt(12, student.getDivisionId());
				}
				preparedStatement.setString(13, student.getRollNumber());
				preparedStatement.setString(14, (StringUtils.isEmpty(student
						.getActive()) ? "Y" : student.getActive()));
				preparedStatement.setString(15, student.getRegistrationCode());

				if (userId == null) {
					preparedStatement.setNull(16, Types.NULL);
				} else {
					preparedStatement.setInt(16, userId);
				}
				preparedStatement.setInt(17, student.getBranchId());
				preparedStatement.setString(18, "N");

				// if (student.getFeesCodeId() == null) {
				// preparedStatement.setNull(25, Types.NULL);
				// } else {
				// preparedStatement.setInt(25, student.getFeesCodeId());
				// }
				preparedStatement.setInt(19, student.hashCode());
				// preparedStatement.setString(26,
				// student.getCreateStudentLogin());
				return preparedStatement;
			}
		}, holder);
		int studentUploadId = holder.getKey().intValue();
		
		System.out.println("insert into student upload complete");
		return studentUploadId;
	}

	private String generatePasswordByNameAndDOB(String firstName,
			String dateOfBirth) {
		String generatedPassword = null;

		Calendar calendar = Calendar.getInstance();

		try {
			calendar.setTime(StringUtils.isEmpty(dateOfBirth) ? new Date()
					: Constant.DATABASE_DATE_FORMAT.parse(dateOfBirth));
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}
		// calendar.setTime(dateOfBirth);
		generatedPassword = firstName + calendar.get(Calendar.DAY_OF_MONTH)
				+ (calendar.get(Calendar.MONTH) + 1)
				+ calendar.get(Calendar.YEAR);
		generatedPassword = generatedPassword.replaceAll(" ", "").toUpperCase();
		return generatedPassword;
	}
	
	
	private void insertFeescodeStudent(List<Integer> feesList,
			final int studentId) {
		final String studenFeesCodeDelSql = "delete from feescode_student where student_id =? ";

		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement preparedStatement = connection
						.prepareStatement(studenFeesCodeDelSql,
								Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setInt(1, studentId);
				return preparedStatement;
			}
		});

		final String studenFeesCodeSql = "insert into feescode_student ( fees_code_configuration_id, student_id, is_active ) "
				+ "values( ?, ?, 'Y')";
		
		List<FeesSchedule>Fees=getAdditionalApplicableFees(feesList);
		if(Fees!=null && Fees.size()>0){
		for (final FeesSchedule fs : Fees) {
			getJdbcTemplate().update(new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(
						Connection connection) throws SQLException {
					PreparedStatement preparedStatement = connection
							.prepareStatement(studenFeesCodeSql,
									Statement.RETURN_GENERATED_KEYS);
					preparedStatement.setInt(1, fs.getFeesCodeConfigurationId());
					preparedStatement.setInt(2, studentId);
					return preparedStatement;
				}
			});
		}
		}

	}
	public List<FeesSchedule> getApplicableFees(Integer standardId,
			Integer divisionId, Integer casteId, Integer branchId) {
		String baseQuery = "select f.fees_code_configuration_id as feeCodeConfigId ,f.id as feesId,f.amount as baseAmount,f.duedate as dueDate,f.fromdate as feesStartDate,f.reminder_start_date as reminderStartDate"
				+ " from fees as f  inner join head as h on f.head_id=h.id ";

		String getFeesByStandard = baseQuery
				+ " where f.standard_id=? and "
				+ " f.branch_id=? and f.caste_id is null and f.division_id is null and f.is_Delete='N'";

		String getFeesBystdDiv = baseQuery
				+ " where f.standard_id=? and "
				+ " f.branch_id=? and f.caste_id is null and f.division_id=? and f.is_Delete='N'";

		String getFeesByStDivCat = baseQuery
				+ " where f.standard_id=? and f.branch_id=? and f.caste_id=?"
				+ " and f.division_id=? and f.is_Delete='N'";

		String getFeesByStdCate = baseQuery
				+ " where f.standard_id=?"
				+ " and f.branch_id=? and caste_id=?  and f.division_id is null and f.is_Delete='N'";

		String query = null;
		Object[] param;

		List<FeesSchedule> feesList = new ArrayList<FeesSchedule>();

		if (standardId != null && divisionId == null && casteId == null) {
			query = getFeesByStandard;
			param = new Object[] { standardId, branchId };
			feesList = getJdbcTemplate().query(query, param,
					new BeanPropertyRowMapper(FeesSchedule.class));

		} else if (standardId != null && divisionId != null && casteId == null) {

			query = getFeesBystdDiv;
			param = new Object[] { standardId, branchId, divisionId };
			feesList = getJdbcTemplate().query(query, param,
					new BeanPropertyRowMapper(FeesSchedule.class));
			if (feesList.size() <= 0) {
				query = getFeesByStandard;
				param = new Object[] { standardId, branchId };
				feesList = getJdbcTemplate().query(query, param,
						new BeanPropertyRowMapper(FeesSchedule.class));

			}
		} else if (standardId != null && divisionId != null && casteId != null) {
			query = getFeesByStDivCat;
			param = new Object[] { standardId, branchId, casteId, divisionId };
			feesList = getJdbcTemplate().query(query, param,
					new BeanPropertyRowMapper(FeesSchedule.class));
			if (feesList.size() <= 0) {

				query = getFeesBystdDiv;
				param = new Object[] { standardId, branchId, divisionId };
				feesList = getJdbcTemplate().query(query, param,
						new BeanPropertyRowMapper(FeesSchedule.class));
				if (feesList.size() <= 0) {
					query = getFeesByStdCate;
					param = new Object[] { standardId,branchId,casteId };

					feesList = getJdbcTemplate().query(query, param,
							new BeanPropertyRowMapper(FeesSchedule.class));
					if (feesList.size() <= 0) {
						query = getFeesByStandard;
						param = new Object[] { standardId, branchId };
						feesList = getJdbcTemplate().query(query, param,
								new BeanPropertyRowMapper(FeesSchedule.class));

					}
				}
			}

		} else {
			query = getFeesByStdCate;
			param = new Object[] { standardId, branchId, casteId };
			feesList = getJdbcTemplate().query(query, param,new BeanPropertyRowMapper(FeesSchedule.class));
			if (feesList.size() <= 0) {
				query = getFeesByStandard;
				param = new Object[] { standardId, branchId };
				feesList = getJdbcTemplate().query(query, param,new BeanPropertyRowMapper(FeesSchedule.class));

			}
		}
		return feesList;
	}

	public List<FeesSchedule> getAdditionalApplicableFees(List<Integer> feesIds) {
		System.out.println("getAdditionalApplicableFees" + feesIds);

		String query = "select f.fees_code_configuration_id as feesCodeConfigurationId,f.id as feesId,"
				+ " f.amount as baseAmount,f.duedate as dueDate,f.fromdate as feesStartDate,f.reminder_start_date as reminderStartDate from fees as f where id in("
				+ org.apache.commons.lang.StringUtils.join(feesIds, ",")
				+ ")"
				+ " and f.is_delete='N'";

		System.out.println("additionalFeesCodeQuery:::::::::::::::::::::"
				+ query);
		if(feesIds.size()>0){
		return getJdbcTemplate().query(query,new BeanPropertyRowMapper(FeesSchedule.class));

		}
		else{
			
			return null;
		}
	}

}


