package com.isg.service.user.daoimpl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.user.dao.FeeDetailsDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.FeeToBePaidDetail;
import com.isg.service.user.model.FeesCharges;
import com.isg.service.user.model.FeesInfo;
import com.isg.service.user.request.DisplayLayout;
import com.isg.service.user.request.FeeDetails;
import com.isg.service.user.request.FeesCode;
import com.isg.service.user.request.PaymentTransaction;
import com.isg.service.user.request.Student;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.Constant.PASSBOOK_TRANSACTION_TYPE;
import com.isg.service.user.util.Constant.PaymentStatus;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class FeeDetailsDaoImpl extends NamedParameterJdbcDaoSupport implements FeeDetailsDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private static final Logger log = Logger.getLogger(FeeDetailsDaoImpl.class);

	/********************************************** FEES DUES **********************************************************************/

	/*	PREVIOUS QUERY
	 private static final String GET_FEES_DUES_BASE_QUERY = "select f.id as fees_id, h.name as fees_head, f.is_split_payment as is_split_payment, " +
					" f.is_partial_payment_allowed as is_partial_payment_allowed, f.description as fees_description, " +
					" s.id as student_id,CONCAT(s.first_name,' ', IFNULL(s.last_name,'')) as student_name, " +
					" DATE_FORMAT(f.duedate, '%d/%m/%Y') as fees_due_date,  DATE_FORMAT(pmnt.paid_date, '%d/% m/%Y') as payment_date, " +
					" pmnt.payment_audit_id as  payment_audit_id, " +
					" (f.amount-IFNULL(sum(pmnt.amount),0) + IFNULL(slf.late_payment_charges,0) -IFNULL(slf.discount_amount,0)) as  amount, " +
					" 0 as ispaid, pmnt.id as  payment_id, slf.late_payment_charges as late_payment_charges, slf.discount_amount as discount_amount " +
					" from fees f " +
					" inner join head h on h.id=f.head_id " +
					" inner join branch b on f.branch_id = b.id " +
					" inner join student s on s.branch_id = b.id and s.active='Y' and s.isDelete='N' " +
					" left join payments pmnt on pmnt.fees_id = f.id and pmnt.student_id = s.id " +
					" left join student_late_fee_payment slf on slf.student_id = s.id and slf.fees_id = f.id and slf.is_paid='N' ";
	*/

	private static final String GET_FEES_DUES_BASE_QUERY = "select f.id as fees_id, h.name as fees_head, f.is_split_payment as is_split_payment, "
		+ " f.is_partial_payment_allowed as is_partial_payment_allowed, IFNULL(f.description, h.name) as fees_description, "
		+ " s.id as student_id,CONCAT(s.first_name,' ', IFNULL(s.last_name,'')) as student_name, fs.fee_for_duration, "
		+ " DATE_FORMAT(fs.due_date, '%d/%m/%Y') as fees_due_date,  slfp.late_payment_charges as student_late_payment_charges, "
		+ " slfp.is_student_specific, slfp.discount_amount, f.amount as  feeAmount, "
		+ " (select IFNULL(sum(amount), 0.0) from payments where fees_schedule_id = fs.id) as paidAmount, "
		+ " sum(prd.amount) as refundedAmount, payments.id as paymentId, "
		+ " (select IFNULL(sum(late_payment_charges), 0.0) from fees_schedule where late_payment_for_fees_schedule = fs.id) as totalLatePaymentCharges, "
		+ " fs.id as fee_schedule_id, " +
		" 0 as ispaid, fs.can_pay as can_pay, f.allow_user_entered_amount, f.allow_repeat_entries, currency.icon_css_class " +
		" from fees f " +
		" INNER JOIN fees_schedule as fs on fs.fees_id = f.id " +
		" inner join head h on h.id=f.head_id " +
		" inner join currency on currency.code = f.currency " +
		" inner join branch b on f.branch_id = b.id " +
		" inner join student s on s.user_id = fs.user_id AND s.branch_id = b.id and s.active='Y' and s.isDelete='N' ";

	private static final String GET_FEES_DUES_FOR_PARENT_QUERY = GET_FEES_DUES_BASE_QUERY
					+ " inner join branch_student_parent bsp on bsp.student_id = s.id and bsp.branch_id = b.id and bsp.isDelete = 'N' "
					+ " left join student_late_fee_payment slfp on slfp.fees_schedule_id = fs.id "
					+ " left join payments on payments.fees_schedule_id = fs.id "
					+ " left join payment_refund_detail as prd on prd.payment_id = payments.id AND prd.status = :refundStatus "
					+ " where bsp.parent_id = :entityId "
					+ " and f.is_delete = 'N' AND fs.is_delete = 'N' "
					+ " AND fs.late_payment_for_fees_schedule is null AND fs.parent_fees_schedule_id is null ";

	private static final String GET_FEES_DUES_FOR_STUDENT_QUERY = GET_FEES_DUES_BASE_QUERY
					+ " left join student_late_fee_payment slfp on slfp.fees_schedule_id = fs.id "
					+ " left join payments on payments.fees_schedule_id = fs.id "
					+ " left join payment_refund_detail as prd on prd.payment_id = payments.id  AND prd.status = :refundStatus "
					+ " where f.is_delete = 'N' AND fs.is_delete = 'N'"
					+ " AND fs.late_payment_for_fees_schedule is null AND fs.parent_fees_schedule_id is null ";

	/********************************************** PAID FEES + OTHER TRANSACTIONS **************************************************************/

	private static final String GET_PAID_FEES_TRANSACTION_BASE_QUERY = " select fs.id as fee_schedule_id, fs.fee_for_duration, " +
					"f.id as fees_id, pmnt.amount as amount,  h.name as head, " +
					" IFNULL(f.description, h.name) as description, f.is_split_payment as is_split_payment, " +
					" f.is_partial_payment_allowed as is_partial_payment_allowed," +
					" s.id as student_id,CONCAT(s.first_name,' ', IFNULL(s.last_name,'')) as student_name," +
					" DATE_FORMAT(f.duedate, '%d/%m/%Y') as due_date, " +
					" DATE_FORMAT(pmnt.paid_date, '%d/%m/%Y') as payment_date, " +
					" 1 as ispaid," +
					" pmnt.payment_audit_id as  payment_audit_id, " +
					" pmnt.id as  payment_id,  pmnt.created_by as created_by, currency.icon_css_class, if(prd.created_at is null, 'N' , 'Y') as isRefundInitiated " +
					" from fees f " +
					" INNER JOIN fees_schedule as fs on fs.fees_id = f.id" +
					" inner join head h on h.id=f.head_id " +
					" inner join currency on currency.code = f.currency " +
					" inner join branch b on f.branch_id = b.id " +
					" inner join student s on s.user_id = fs.user_id AND s.branch_id = b.id " +
					" inner join payments pmnt on pmnt.fees_schedule_id = fs.id ";

	private static final String GET_PAID_FEES_TRANSACTION_FOR_PARENT = GET_PAID_FEES_TRANSACTION_BASE_QUERY +
					" inner join branch_student_parent bsp on bsp.student_id = s.id and bsp.branch_id = b.id and bsp.isDelete = 'N'" +
					" LEFT JOIN payment_refund_detail as prd on prd.payment_id = pmnt.id and prd.status = :refundStatus " +
					" where bsp.parent_id = :entityId AND fs.late_payment_for_fees_schedule is null AND "
					+ " (fs.status != :status OR f.allow_user_entered_amount = 'Y') group by f.id, pmnt.id, fs.id ";

	private static final String GET_PAID_FEES_TRANSACTION_FOR_STUDENT = GET_PAID_FEES_TRANSACTION_BASE_QUERY +
					" LEFT JOIN payment_refund_detail as prd on prd.payment_id = pmnt.id and prd.status = :refundStatus " +
					" where s.id = :entityId AND fs.late_payment_for_fees_schedule is null AND "
					+ " (fs.status != :status OR f.allow_user_entered_amount = 'Y') group by f.id, pmnt.id, fs.id ";

	private static final String GET_PAID_OTHER_TRANSACTION = " select null as fee_schedule_id, null as fee_for_duration, pmnt.fees_id as fees_id,  "
					+ "pmnt.amount as amount, '' as head, " +
					"  pmnt_audit.description as description, 'N' as is_split_payment, 'N' as is_partial_payment_allowed, " +
					" NULL as student_id,'' as student_name, " +
					" '' as due_date, " +
					" DATE_FORMAT(pmnt.paid_date, '%d/%m/%Y') as payment_date, " +
					" 1 as ispaid," +
					" pmnt.payment_audit_id as  payment_audit_id, " +
					" pmnt.id as  payment_id,  pmnt.created_by as created_by, '' as icon_css_class, 'N' as isRefundInitiated " +
					" from payments pmnt " +
					" inner join payment_audit pmnt_audit on pmnt_audit.id = pmnt.payment_audit_id " +
					" where pmnt.created_by=:userId and pmnt.fees_id IS NULL ";

	private static final String GET_PAID_TRANSACTION_FOR_PARENT_QUERY = " Select * from (" +
					GET_PAID_FEES_TRANSACTION_FOR_PARENT + " union " + GET_PAID_OTHER_TRANSACTION +
					" ) paid_results order by payment_date ";

	private static final String GET_PAID_TRANSACTION_FOR_STUDENT_QUERY = " Select * from (" +
					GET_PAID_FEES_TRANSACTION_FOR_STUDENT + " union " + GET_PAID_OTHER_TRANSACTION +
					" ) paid_results order by payment_date ";

	/***************************************************** FEES ACCESS ALLOWED ***************************************************************/

	private static final String IS_FEES_ACCESS_ALLOWED_PARENT_QUERY = "select f.id as fees_id from fees f "
					+ " inner join branch b on f.branch_id = b.id "
					+ " inner join student s on s.branch_id = b.id and s.active='Y' and s.isDelete='N' "
					+ " inner join branch_student_parent bsp on bsp.student_id = s.id and bsp.branch_id = b.id and bsp.isDelete = 'N'"
					+ " where f.id = :feesId and s.id= :studentId and bsp.parent_id = :entityId  "
					+ " and f.is_delete = 'N' and (f.caste_id = s.caste_id or (f.caste_id is NULL))  "
					+ " and (f.fees_code_configuration_id = s.fees_code_configuration_id or f.fees_code_configuration_id is NULL ) "
					+ " and ( ((f.standard_id = s.standard_id or f.standard_id IS NULL) and (f.division_id IS NULL or f.division_id=s.division_id) and f.student_id IS NULL) "
					+ " OR ((f.standard_id = s.standard_id or f.standard_id IS NULL) and (f.division_id IS NULL or f.division_id=s.division_id) and f.student_id = :studentId)) ";

	private static final String IS_FEES_ACCESS_ALLOWED_STUDENT_QUERY = "select f.id as fees_id from fees f "
					+ " inner join branch b on f.branch_id = b.id "
					+ " inner join student s on s.branch_id = b.id and s.active='Y' and s.isDelete='N' "
					+ " where f.id = :feesId and f.is_delete = 'N' and s.id= :entityId "
					+ " and (f.caste_id = s.caste_id or (f.caste_id is NULL)) "
					+ " and (f.fees_code_configuration_id = s.fees_code_configuration_id or f.fees_code_configuration_id is NULL) "
					+ " and ( ((f.standard_id = s.standard_id or f.standard_id IS NULL) and (f.division_id IS NULL or f.division_id=s.division_id) and f.student_id IS NULL) "
					+ " OR ((f.standard_id = s.standard_id or f.standard_id IS NULL) and (f.division_id IS NULL or f.division_id=s.division_id) and f.student_id = :studentId)) ";

	/********************************************************************************************************************************************/

	@Autowired
	@Lazy
	UserDao userDao;

	@Autowired
	@Lazy
	StudentDao studentDao;

	@Override
	public List<FeeDetails> getFeeDetails() {
		String retriveFees = "select * from fees";
		return getJdbcTemplate().query(retriveFees, new BeanPropertyRowMapper<FeeDetails>(FeeDetails.class));
	}

	@Override
	public FeeDetails getFeeDetails(Integer feesId) {
		String retriveFees = "select * from fees where id=:feesId";
		SqlParameterSource namedParameters = new MapSqlParameterSource("feesId", feesId);
		return getNamedParameterJdbcTemplate().queryForObject(retriveFees, namedParameters, new BeanPropertyRowMapper<FeeDetails>(FeeDetails.class));
	}

	@Override
	public FeeDetails getFeeDetailsByScheduleId(Long feeScheduleId) {
		String retriveFees = "select * from fees where id=(select fees_id from fees_schedule where id = :feeScheduleId)";
		SqlParameterSource namedParameters = new MapSqlParameterSource("feeScheduleId", feeScheduleId);
		FeeDetails feeDetails = getNamedParameterJdbcTemplate().queryForObject(retriveFees, namedParameters,
						new BeanPropertyRowMapper<FeeDetails>(FeeDetails.class));

		if (StringUtils.isEmpty(feeDetails.getDescription())) {
			String sql = "select name from head where id = " + feeDetails.getHead_id();
			String head = getJdbcTemplate().queryForObject(sql, String.class);
			feeDetails.setDescription(head);
		}
		return feeDetails;
	}

	@Override
	public List<FeeDetails> getFeeDetails(List<Integer> feesIdList) {
		String retriveFeesQuery = "select * from fees where id in (:feesIdList)";
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("feesIdList", feesIdList);
		log.debug("Query :: " + retriveFeesQuery);
		log.debug("queryParameters :: " + queryParameters.toString());

		return getNamedParameterJdbcTemplate().query(retriveFeesQuery, queryParameters, new BeanPropertyRowMapper<FeeDetails>(FeeDetails.class));
	}

	@Override
	public List<PaymentTransaction> getPaidPaymentTransactions(final Integer entityId, final Integer userId, String role) {

		String query = GET_PAID_TRANSACTION_FOR_STUDENT_QUERY;

		if (ROLES.PARENT.get().equalsIgnoreCase(role)) {
			query = GET_PAID_TRANSACTION_FOR_PARENT_QUERY;
		}

		Map<String, Object> parameters = new HashMap<String, Object>(2);
		parameters.put("entityId", entityId);
		parameters.put("userId", userId);
		parameters.put("status", PaymentStatus.UNPAID.get());
		parameters.put("refundStatus", "INITIATED");

		log.info("\n\n:::" + query);

		return getNamedParameterJdbcTemplate().query(query,
					parameters, new RowMapper<PaymentTransaction>() {
						@Override
						public PaymentTransaction mapRow(ResultSet resultSet, int rowNum) throws SQLException {
							int paymentAuditId = resultSet.getInt("payment_audit_id");
							boolean canPaymentBeMarkedAsUnPaid = (paymentAuditId != 0 ||
											(paymentAuditId == 0 && resultSet.getInt("created_by") != userId.intValue())) ? false : true;

							boolean isSplitPayment = resultSet.getString("is_split_payment").equalsIgnoreCase("Y") ? true : false;
							boolean isPartialPaymentAllowed = resultSet.getString("is_partial_payment_allowed").equalsIgnoreCase("Y") ? true : false;

							PaymentTransaction transaction = new PaymentTransaction(
											resultSet.getInt("fees_id"),
											resultSet.getString("amount"),
											resultSet.getString("head"),
											resultSet.getString("description"),
											resultSet.getInt("student_id"),
											resultSet.getString("student_name"),
											resultSet.getString("due_date"),
											resultSet.getString("payment_date"),
											resultSet.getInt("ispaid"),
											canPaymentBeMarkedAsUnPaid,
											resultSet.getInt("payment_id"),
											isSplitPayment,
											isPartialPaymentAllowed,
											resultSet.getLong("fee_schedule_id"));
							transaction.setIsRefunded(resultSet.getString("isRefundInitiated"));

							if("Y".equals(resultSet.getString("isRefundInitiated"))){
								transaction.setStatus("REFUND INITIATED");
							}else {
								transaction.setStatus("PAID");
							}

							transaction.setDuration(resultSet.getString("fee_for_duration"));
							transaction.setCurrencyCssClass(resultSet.getString("icon_css_class"));
							return transaction;
						}
					});
	}

	@Override
	public List<PaymentTransaction> getFeeDues(Integer entityId, String role) {

		Integer studentId = null;

		if (ROLES.STUDENT.get().equalsIgnoreCase(role)) {
			studentId = entityId;
		}

		return getFeeDues(entityId, role, studentId, null);

	}

	@Override
	public List<PaymentTransaction> getFeeDues(Integer entityId, String role, Integer studentId, Long feeScheduleId) {
		String query = GET_FEES_DUES_FOR_STUDENT_QUERY;

		if (ROLES.PARENT.get().equalsIgnoreCase(role)) {
			query = GET_FEES_DUES_FOR_PARENT_QUERY;
		}

		query += " AND fs.status != :status  ";

		Map<String, Object> parameters = new HashMap<String, Object>(2);
		parameters.put("entityId", entityId);
		parameters.put("status", PaymentStatus.PAID.get());
		parameters.put("refundStatus", "SUCCESS");

		if (feeScheduleId != null && feeScheduleId != 0) {
			query += "and fs.id =:feeScheduleId ";
			parameters.put("feeScheduleId", feeScheduleId);
		}

		if ((studentId != null && studentId != 0)) {
			query += "and s.id =:studentId ";
			parameters.put("studentId", studentId);
		}

		query += " group by fs.id ";

		log.debug("getFeeDues query ::" + query);
		log.debug("parameters ::" + parameters);
		System.out.println("::::"+query);
		query += " order by f.duedate ";
		return getFeeDueDetails(query, parameters);
	}

	private List<PaymentTransaction> getFeeDueDetails(String query, Map<String, Object> parameters) {
		return getNamedParameterJdbcTemplate().query(query,
						parameters, new ResultSetExtractor<List<PaymentTransaction>>() {
							@Override
							public List<PaymentTransaction> extractData(ResultSet rs) throws SQLException, DataAccessException {
								System.out.println("*********************");
								List<PaymentTransaction> list = new ArrayList<PaymentTransaction>();
								while (rs.next()) {
									boolean isSplitPayment = rs.getString("is_split_payment").equalsIgnoreCase("Y") ? true : false;
									boolean isPartialPaymentAllowed = rs.getString("is_partial_payment_allowed").equalsIgnoreCase("Y") ? true : false;

									Double amount = 0.0;
									if ("Y".equals(rs.getString("is_student_specific"))) {
										amount = rs.getDouble("feeAmount") + rs.getDouble("student_late_payment_charges") - rs.getDouble("discount_amount") - rs.getDouble("paidAmount");
									}
									else {
										amount = rs.getDouble("feeAmount") - rs.getDouble("paidAmount") + rs.getDouble("totalLatePaymentCharges") - rs.getDouble("discount_amount");
									}
									
									String isRefunded = rs.getDouble("refundedAmount") > 0 ? "Y" : "N";
									amount += rs.getDouble("refundedAmount");

									System.out.println("\n********Final Amount*************" + amount);

									if (amount > 0 || ("Y".equals(rs.getString("allow_user_entered_amount")))) {
										PaymentTransaction feeDues = new PaymentTransaction(
														rs.getInt("fees_id"),
														amount + "",
														rs.getString("fees_head"),
														rs.getString("fees_description"),
														rs.getInt("student_id"),
														rs.getString("student_name"),
														rs.getString("fees_due_date"),
														null,
														rs.getInt("ispaid"),
														true,
														0,
														rs.getFloat("student_late_payment_charges"),
														isSplitPayment,
														isPartialPaymentAllowed,
														rs.getFloat("discount_amount"),
														rs.getLong("fee_schedule_id"),
														"Y".equals(rs.getString("can_pay")));

										feeDues.setAllowUserEnteredAmount(rs.getString("allow_user_entered_amount"));
										feeDues.setAllowRepeatEntries(rs.getString("allow_repeat_entries"));
										feeDues.setDuration(rs.getString("fee_for_duration"));
										feeDues.setCurrencyCssClass(rs.getString("icon_css_class"));
										feeDues.setIsRefunded(isRefunded);

										if("Y".equals(isRefunded)){
											feeDues.setStatus("REFUNDED");
										}else {
											feeDues.setStatus("UNPAID");
										}

										list.add(feeDues);
										
									}
								}
								return list;
							}
						});
	}

	protected Double getMaxAmountWithLateFee(long feeScheduleId) {
		Double amount = null;
		String sql = "select (total_amount - IFNULL(sum(partial_paid_amount), 0) + IFNULL(sum(late_payment_charges), 0)) as total_amount " +
						" from fees_schedule where id = " + feeScheduleId + " OR late_payment_for_fees_schedule = " + feeScheduleId
						+ " OR parent_fees_schedule_id = " + feeScheduleId;

		List<Double> amountList = getJdbcTemplate().query(sql, new RowMapper<Double>() {
			@Override
			public Double mapRow(ResultSet rs, int arg1) throws SQLException {
				Double amount = rs.getDouble("total_amount");
				amount = amount > 0 ? amount : null;
				return amount;
			}
		});
		if (amountList != null && !amountList.isEmpty()) {
			amount = amountList.get(0);
		}
		return amount;
	}

	@Override
	public int getFeesDuesCount(Integer entityId, String role) throws SQLException {
		Integer studentId = null;

		if (ROLES.STUDENT.get().equalsIgnoreCase(role)) {
			studentId = entityId;
		}
		return (getFeeDues(entityId, role, studentId, null)).size();
	}

	@Override
	@Transactional
	public void markPaymentStatus(Integer userId, Integer entityId, int flag, Integer feesId, Integer studentId, Integer paymentAuditId, String amount,
					Integer paymentId, String description, String role, Long feeScheduleId, String qfixRefNumber, double chargesApplicableForFees, String paymentType)
					throws AuthorisationException, ParseException {

		try {
			log.debug("entityId ::" + entityId + " flag ::" + flag + " feesId ::" + feesId + " studentId ::" + studentId + " role::" + role);
			if (ROLES.ADMIN.get().equalsIgnoreCase(role)) {
				// Allow access to all payments
			} else if (feesId == null || !isValidFeeIdSpecified(entityId, feesId, studentId, role)) {
				throw new AuthorisationException("Function not allowed.");
			}

			log.debug("entityId ::" + entityId + " flag ::" + flag + " feesId ::" + feesId + " studentId ::" + studentId + " role::" + role);

			// Mark fees as paid or unpaid.
			if (flag == 0) {
				//				markAsUnpaid(userId, feesId, studentId, paymentId);
			} else if (flag == 1) {
				System.out.println("MARK FEES STARTED-=======================----------------------===================---------------------------");

				Map<String, Object> feesConfigMap = getFeesAllowedConfig(feesId);

				String allowUserEnteredAmount = (String) feesConfigMap.get("allow_user_entered_amount");
				String allowRepeatEntries = (String) feesConfigMap.get("allow_repeat_entries");

				Integer insertedPaymentId = markAsPaid(userId, feesId, studentId, paymentAuditId, amount, description, qfixRefNumber, paymentType,
								feeScheduleId, chargesApplicableForFees);
				System.out.println("Marking fees PAID::::==========================:::::::" + insertedPaymentId);

				updateFeesSchedule(feeScheduleId, insertedPaymentId, amount);

				if("Y".equals(allowUserEnteredAmount) && "Y".equals(allowRepeatEntries)){
					insertIntoFeesSchedule(userId, feesId);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	//date:17_11_2017
	private void insertIntoFeesSchedule(Integer userId, Integer feesId) {
		// Get details needed of fee based on feesId, 
		// Add new entry inside fees_schedule table for userId parameter.
		
		
		System.out.println("my method:**************************");
		String sql="select * from fees where id="+feesId;
		
		FeeDetails feeDetails=getJdbcTemplate().queryForObject(sql,FeeDetails.class);
		
		System.out.println("Fees details for allow user entry"+feeDetails);
		
		
		String query="insert into fees_schedule(fees_id,user_id,base_amount,late_payment_charges,total_amount,due_date,status,partial_paid_amount,"
				+ " created_at,fee_for_duration,fee_start_date)"
				+ " values(:feesId,:userId,:baseAmount,:latePaymentCharges,:totalAmount,:dueDate,:status,:partialPaidAmount,:createdAt,:feeForDuration,:feeStartDate)";
		
		
		
		Map<String, Object> paramMap = new HashMap<>();
		//Long parentFeesSchedulId = rs.getLong("parent_fees_schedule_id");
		//parentFeesSchedulId = parentFeesSchedulId == 0 ? rs.getLong("id") : parentFeesSchedulId;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"dd MMM, yyyy");
		
		String feeForDuration=simpleDateFormat.format(feeDetails.getTodate()) + " - "
		+ simpleDateFormat.format(feeDetails.getFromdate());

		paramMap.put("feesId",feesId);
		paramMap.put("userId",userId );
		paramMap.put("baseAmount",0);
		paramMap.put("latePaymentCharges", 0);
		paramMap.put("totalAmount", 0);
		paramMap.put("dueDate", feeDetails.getDuedate());
		paramMap.put("status", PaymentStatus.UNPAID.get());
		paramMap.put("partialPaidAmount", 0);
		paramMap.put("createdAt",Constant.DATABASE_DATE_FORMAT.format(new Date()));
		paramMap.put("feeForDuration",feeForDuration);
		paramMap.put("feeStartDate", feeDetails.getFromdate());
		
		int count=getNamedParameterJdbcTemplate().update(query, paramMap);
		
		System.out.println(count);
		
		
		
	}

	private Map<String, Object> getFeesAllowedConfig(Integer feesId) {
		String sql = "select allow_user_entered_amount, allow_repeat_entries from fees where id = " + feesId;
		
		Map<String, Object> dataMap = getJdbcTemplate().queryForMap(sql);
		return dataMap;
	}

	private boolean getLateFeesApplied(Long feeScheduleId) {
		String sql = "select count(*) as count from fees_schedule where late_payment_for_fees_schedule = " + feeScheduleId
						+ " OR parent_fees_schedule_id = "
						+ feeScheduleId;

		int count = getJdbcTemplate().queryForObject(sql, Integer.class);
		if (count > 0) {
			return true;
		}
		return false;
	}

	private void updateFeesSchedule(final Long feeScheduleId, final Integer paymentId, final String amount) {
		log.debug("UPDATE FEES SCHEDULE STARTED :::::::");
		
		System.out.println("Inside update Fees_schedule*******************************");

		final boolean isLateFeeApplied = getLateFeesApplied(feeScheduleId);
		String sql = "select fs.*, sum(p.amount) as paidAmount, slfp.late_payment_charges as student_late_payment_charges, "
						+
						"(select IFNULL(sum(late_payment_charges), 0.0) from fees_schedule where late_payment_for_fees_schedule = fs.id) as totalLatePaymentCharges, "
						+ " slfp.is_student_specific, slfp.discount_amount as discount_amount "
						+ "	from fees_schedule as fs  "
						+ " LEFT JOIN student_late_fee_payment as slfp on slfp.fees_schedule_id = fs.id "
						+ "  LEFT JOIN payments as p on p.fees_schedule_id = " + feeScheduleId;

		if (isLateFeeApplied) {
			sql += " where fs.id = (select max(id) as id from fees_schedule "
							+ "where parent_fees_schedule_id = "
							+ feeScheduleId
							+ " OR late_payment_for_fees_schedule = "
							+ feeScheduleId
							+ ")";
		} else {
			sql += " where fs.id =" + feeScheduleId;
		}

		log.debug("SQL ::::: " + sql);
		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				System.out.println(rs.getDouble("base_amount") + ", PAID ::::: " + amount);
				double remainingAmount = (rs.getDouble("base_amount") + rs.getDouble("totalLatePaymentCharges") - rs.getDouble("paidAmount")) - rs.getDouble("discount_amount");

				if ("Y".equals(rs.getString("is_student_specific"))) {
					remainingAmount = rs.getDouble("base_amount")
									+ rs.getDouble("student_late_payment_charges")
									- rs.getDouble("discount_amount") - rs.getDouble("paidAmount");
				}

				String sql = "update fees_schedule set status = ?, pyment_id = ?, partial_paid_amount = ? "
								+ " WHERE (id = ? OR (parent_fees_schedule_id = ? OR late_payment_for_fees_schedule = ?)) AND status = ?";

				String status = remainingAmount > 0 ? Constant.PaymentStatus.PARTIAL_PAID
								.get() : Constant.PaymentStatus.PAID.get();

				Object[] params = { status, paymentId, amount, feeScheduleId,
								feeScheduleId, feeScheduleId,
								Constant.PaymentStatus.UNPAID };
				int[] types = { Types.VARCHAR, Types.INTEGER, Types.DOUBLE,
								Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.VARCHAR };

				System.out.println("update fees schedule >>>" + sql);
				System.out.println(params);

				int rowsAffcted = getJdbcTemplate().update(sql, params, types);

				logger.debug("Total Amount ::::: "
								+ rs.getDouble("total_amount") + ", PAID AMOUNT ::: "
								+ amount);
				if (remainingAmount > 0) {
					insertIntoFeesSchedule(feeScheduleId, null, rs, remainingAmount, Constant.PaymentStatus.UNPAID.get(), null);
				}
				else if (rowsAffcted == 0) {
					insertIntoFeesSchedule(feeScheduleId, paymentId, rs, remainingAmount, Constant.PaymentStatus.PAID.get(), amount);
				}
				if (remainingAmount == 0) {
					sql = "update fees_schedule set status = '" + status
									+ "' where id = " + feeScheduleId;
					getJdbcTemplate().update(sql);
				}
				logger.debug("UPDATE FEES SCHEDULE DONE :::::::");
				return null;
			}
		});
	}

	protected void insertIntoFeesSchedule(Long feeScheduleId, Integer paymentId, ResultSet rs, double remainingAmount, String status,
					String partialPaidAmount) throws SQLException {
		String sql = "insert into fees_schedule (fees_id, user_id, base_amount, "
						+ " late_payment_charges, total_amount, due_date, status, "
						+ " reminder_start_date, fee_for_duration, fee_start_date, parent_fees_schedule_id, pyment_id, partial_paid_amount) "
						+ " values (:feesId, :userId, :baseAmount, :latePaymentCharges, "
						+ " :totalAmount, :dueDate, :status, :reminderStartDate, :feeForDuration, :feeStartDate, " +
						":parentFeesScheduleId, :paymentId, :partialPaidAmount)";

		Map<String, Object> paramMap = new HashMap<>();
		Long parentFeesSchedulId = rs.getLong("parent_fees_schedule_id");
		parentFeesSchedulId = parentFeesSchedulId == 0 ? rs.getLong("id") : parentFeesSchedulId;

		paramMap.put("feesId", rs.getInt("fees_id"));
		paramMap.put("userId", rs.getInt("user_id"));
		paramMap.put("baseAmount", rs.getDouble("base_amount"));
		paramMap.put("latePaymentCharges", rs.getDouble("late_payment_charges"));
		paramMap.put("totalAmount", remainingAmount);
		paramMap.put("dueDate", rs.getDate("due_date"));
		paramMap.put("status", PaymentStatus.UNPAID.get());
		paramMap.put("reminderStartDate", rs.getDate("reminder_start_date"));
		paramMap.put("feeForDuration", rs.getString("fee_for_duration"));
		paramMap.put("feeStartDate", rs.getDate("fee_start_date"));
		paramMap.put("parentFeesScheduleId", parentFeesSchedulId);
		paramMap.put("paymentId", paymentId);
		paramMap.put("partialPaidAmount", partialPaidAmount);
		getNamedParameterJdbcTemplate().update(sql, paramMap);
	}

	@Deprecated
	private void markAsUnpaid(Integer userId, Integer feesId, Integer studentId, Integer paymentId) {
		/*String deleteFeesEntryQuery = "";
		Map<String, Object> parameters = new HashMap<String, Object>(4);

		Payment paymentDetails = paymentDao.getPaymentDetails(paymentId);

		if (paymentId != null) {
			deleteFeesEntryQuery = "delete from payments where id=:paymentId and created_by=:created_by and fees_id =:feesId and student_id=:studentId and payment_audit_id IS NULL";
			parameters.put("created_by", userId);
			parameters.put("feesId", feesId);
			parameters.put("studentId", studentId);
			parameters.put("paymentId", paymentId);
		} else {
			deleteFeesEntryQuery = "delete from payments where created_by=:created_by and fees_id =:feesId and student_id=:studentId and payment_audit_id IS NULL";
			parameters.put("created_by", userId);
			parameters.put("feesId", feesId);
			parameters.put("studentId", studentId);
		}

		int DELETE_STATUS = getNamedParameterJdbcTemplate().update(deleteFeesEntryQuery, parameters);

		if (DELETE_STATUS == 1 && paymentDetails != null && paymentDetails.getPassbookId() != null) {
			Passbook passbook = userDao.getPassbookDetails(paymentDetails.getPassbookId());
			List<Integer> passbookId = new ArrayList<Integer>();
			passbookId.add(passbook.getId());

			if (passbook.getParentTransactionId() != null) {
				passbookId.add(passbook.getParentTransactionId());
			}

			userDao.removeEntryFromPassbook(passbookId);
		}
*/
	}

	private Integer markAsPaid(Integer userId, Integer feesId, Integer studentId, Integer paymentAuditId, String amount, String description,
					String qfixRefNumber, String modeOfPayment, Long feesScheduleId, double chargesApplicableForFees)
					throws ParseException {
		Integer paymentId = null;
		Long passbookId = addEntryToPassbook(Float.parseFloat(amount), userId, description, studentId);
		log.debug("Entry Added in Passbook ::: ");
		SimpleJdbcInsert insertFeesPaymentTransaction =
						new SimpleJdbcInsert(getJdbcTemplate().getDataSource())
										.withTableName("payments")
										.usingColumns("created_by", "fees_id", "student_id", "paid_date", "payment_audit_id", "amount", "passbook_id",
														"paymentreference_details", "payment_details", "fees_schedule_id", "other_charges")
										.usingGeneratedKeyColumns("id");

		Map<String, Object> parameters = new HashMap<String, Object>(19);
		parameters.put("fees_id", feesId);
		parameters.put("student_id", studentId);
		parameters.put("paid_date", new java.sql.Date(System.currentTimeMillis()));
		parameters.put("payment_audit_id", paymentAuditId);
		parameters.put("amount", amount);
		parameters.put("passbook_id", passbookId);
		parameters.put("created_by", userId);
		parameters.put("paymentreference_details", qfixRefNumber);
		parameters.put("payment_details", modeOfPayment);
		parameters.put("other_charges", chargesApplicableForFees);
		parameters.put("fees_schedule_id", (feesScheduleId == 0) ? null : feesScheduleId);

		log.debug("Making entry inside payments ::::::::::::::::" + parameters);
		paymentId = insertFeesPaymentTransaction.executeAndReturnKey(parameters).intValue();
		log.debug("INSERTED into payment Id :::***************::::: " + paymentId);

		return paymentId;
	}

	private Long addEntryToPassbook(float totalAmount, Integer userId, String description, Integer studentId) throws ParseException {
		Integer receivingEntityUserId = null;
		String receivingEntityName = null;

		if (studentId != null && studentId != 0) {
			Student s = studentDao.getStudentProfileDetailsbyId(studentId);
			receivingEntityUserId = s.getUserId().intValue();
			receivingEntityName = s.getPersonalDetails().getName();
		}

		return userDao.addEntryToPassbook("yes", totalAmount, userId, description, receivingEntityUserId, receivingEntityName, PASSBOOK_TRANSACTION_TYPE.DEBIT.get());
	}

	private boolean isValidFeeIdSpecified(Integer entityId, Integer feesId, Integer studentId, String role) {

		String query = IS_FEES_ACCESS_ALLOWED_STUDENT_QUERY;

		if (ROLES.PARENT.get().equalsIgnoreCase(role)) {
			query = IS_FEES_ACCESS_ALLOWED_PARENT_QUERY;
		}

		Map<String, Object> parameters = new HashMap<String, Object>(19);
		parameters.put("entityId", entityId);
		parameters.put("feesId", feesId);
		parameters.put("studentId", studentId);

		List<Integer> feesList = getNamedParameterJdbcTemplate().query(query, parameters, new RowMapper<Integer>() {

			@Override
			public Integer mapRow(ResultSet rs, int rownumber) throws SQLException {
				return rs.getInt("fees_id");
			}
		});

		return (feesList != null && feesList.size() > 0) ? true : false;
	}

	@Override
	public Map<Integer, String> retrieveFeesSplitConfigurationDetails(final Map<Integer, FeeToBePaidDetail> feesIdsSelectedForPayment) {

		String query = "select * from fees_split_configuration where fees_id in (:feesIds)";

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("feesIds", feesIdsSelectedForPayment.keySet());

		return getNamedParameterJdbcTemplate().query(query, parameters, new ResultSetExtractor<Map<Integer, String>>() {
			@Override
			public Map<Integer, String> extractData(ResultSet rs) throws SQLException, DataAccessException {
				HashMap<Integer, String> mapRet = new HashMap<Integer, String>();
				float totalAmount = 0f;
				Integer schecmeCode = null;
				FeeToBePaidDetail feeToBePaidDetail = null;

				while (rs.next()) {
					schecmeCode = rs.getInt("scheme_code_id");
					feeToBePaidDetail = feesIdsSelectedForPayment.get(new Integer(rs.getInt("fees_id")));

					if (mapRet.containsKey(schecmeCode)) {
						totalAmount = Float.parseFloat(mapRet.get(schecmeCode));
					} else {
						totalAmount = 0f;
					}

					if (feeToBePaidDetail.isSplitPayment() && "N".equalsIgnoreCase(rs.getString("is_seed_account"))) {
						totalAmount = getFormattedAmount(
										totalAmount + rs.getFloat("amount") + feeToBePaidDetail.getLatePaymentCharges() - feeToBePaidDetail.getDiscountAmount(),
										2);
					} else if (feeToBePaidDetail.isSplitPayment() && "Y".equalsIgnoreCase(rs.getString("is_seed_account"))) {
						totalAmount = getFormattedAmount(totalAmount + rs.getFloat("amount"), 2);
					} else {
						totalAmount = getFormattedAmount(totalAmount + feeToBePaidDetail.getAmount(), 2);
					}

					mapRet.put(schecmeCode, String.valueOf(totalAmount));
				}
				return mapRet;
			}
		});
	}

	private float getFormattedAmount(float amount, int decimalPlaces) {
		BigDecimal value = new BigDecimal(amount);
		value = value.setScale(decimalPlaces, RoundingMode.HALF_EVEN);
		return value.floatValue();
	}

	public List<DisplayLayout> getDisplayLayouts(Integer feeId) {
		String sql = "select dh.*, dth.amount from display_head as dh " +
						" INNER JOIN display_template_head as dth on dth.display_head_id = dh.id " +
						" INNER JOIN display_template as dt on dt.id = dth.display_template_id " +
						" INNER JOIN fees as f on f.display_template_id = dt.id " +
						" WHERE dt.is_delete = 'N' AND dh.is_delete = 'N' AND f.id = " + feeId;

		final List<DisplayLayout> displayLayouts = new ArrayList<>();

		getJdbcTemplate().query(sql, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				DisplayLayout layout = new DisplayLayout();
				layout.setBranchId(rs.getInt("branch_id"));
				layout.setId(rs.getInt("id"));
				layout.setFeeAmount(rs.getDouble("amount"));
				layout.setFeeDescription(rs.getString("name"));
				layout.setOtherDescription(rs.getString("description"));
				displayLayouts.add(layout);
				return null;
			}
		});
		return displayLayouts;
	}

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public FeesInfo getFeeInfo(Integer entityId, String defaultRole, final Long feeScheduleId, final Long paymentId, final Long pgRefundId) {
		logger.debug("getFeeInfo method starts *************************");
		String sql = "select IFNULL(f.description, h.name) as description, f.branch_id as branch_id, f.amount as base_fee_amount, fs.*, " +
						" p.amount as paidAmount, p.paymentreference_details as reference_number, p.payment_details as payment_mode, " +
						" p.paid_date as paid_date, p.other_charges, sum(prd.amount) as refundAmount, prd.created_at as refundDate, prd.status as refundStatus, " +
						"(select sum(late_payment_charges) from fees_schedule where late_payment_for_fees_schedule = fs.id) as totalLatePaymentCharges, " +
						" slfp.late_payment_charges as studentLatePaymentCharges, slfp.is_student_specific, slfp.discount_amount, " +
						" concat(c1.firstname , ' ', c1.lastname ) as payment_for ," +
						" IFNULL(d.displayName,'NA') as division , IFNULL(st.displayName,'NA') as standard , IFNULL(s.registration_code,'NA') as registration_code, " +
						" CONCAT(CAST(YEAR(ay.from_date) AS CHAR),' - ',CAST(YEAR(ay.to_date) AS CHAR)) AS academicYear," +
						" concat(c.firstname , ' ', c.lastname ) as paid_by, currency.icon_css_class, f.currency, " + 
						" p.bank_name, p.bank_branch_name, p.ifsc_code, p.cheque_or_dd_number  " +
						" from fees_schedule as fs " + 
						" INNER JOIN fees as f on f.id = fs.fees_id " +
						" INNER JOIN currency on currency.code= f.currency " +
						" INNER JOIN student as s on s.user_id=fs.user_id " +
						" INNER JOIN head as h on h.id = f.head_id " +
						" INNER JOIN branch as b on b.id=s.branch_id " +
						" LEFT JOIN contact as c1 on c1.user_id = fs.user_id " +
						" LEFT JOIN standard as st on s.standard_id=st.id " +
						" LEFT JOIN division as d on s.division_id=d.id " +
						" LEFT JOIN  academic_year as ay on b.id=ay.branch_id and ay.is_current_active_year='Y' " +
						" LEFT JOIN payments as p on p.fees_schedule_id = fs.id " +
						" LEFT JOIN payment_refund_detail as prd on prd.payment_id = p.id " +
						" LEFT JOIN contact as c on c.user_id = p.created_by " +
						" LEFT JOIN student_late_fee_payment as slfp on slfp.fees_schedule_id = fs.id " +
						" where fs.id = " + feeScheduleId + " ";

		if(paymentId != null && paymentId != 0){
			sql += " AND p.id = "+ paymentId;
		}

		if(pgRefundId != null){
			sql += " AND prd.payment_gateway_refund_id = "+ pgRefundId;
		}

		logger.debug("getFeeInfo SQL ****" + sql);

		System.out.println("Getting fee info for fees receipt " + sql);

		FeesInfo feeInfo = null;
		List<FeesInfo> feesInfoList = getNamedParameterJdbcTemplate().query(sql, new RowMapper<FeesInfo>() {
			@Override
			public FeesInfo mapRow(ResultSet rs, int arg1) throws SQLException {
				FeesInfo feeInfo = new FeesInfo();
				feeInfo.setBaseFeeAmount(rs.getDouble("base_fee_amount"));
				feeInfo.setDescription(rs.getString("description"));
				feeInfo.setPaymentStatus(rs.getString("status"));
				feeInfo.setAmountPaid(rs.getDouble("paidAmount"));
				feeInfo.setReferenceNumber(rs.getString("reference_number"));
				feeInfo.setPaymentMode(rs.getString("payment_mode"));
				feeInfo.setPaymentFor(rs.getString("payment_for"));
				feeInfo.setAcademicYear(rs.getString("academicYear"));
				feeInfo.setStandard(rs.getString("standard"));
				feeInfo.setDivision(rs.getString("division"));
				feeInfo.setRegistrationCode(rs.getString("registration_code"));
				java.sql.Date paidDate = rs.getDate("paid_date");
				if (paidDate != null) {
					feeInfo.setPaidDate(simpleDateFormat.format(paidDate));
				}
				feeInfo.setPaidBy(rs.getString("paid_by"));
				feeInfo.setBranchId(rs.getInt("branch_id"));
				feeInfo.setDisplayLayouts(getDisplayLayouts(rs.getInt("fees_id")));
				feeInfo.setFeesCharges(getFeeCharges(feeScheduleId));

				if ("Y".equals(rs.getString("is_student_specific"))) {
					feeInfo.setTotalLateFeeAdded(rs.getDouble("studentLatePaymentCharges"));
					feeInfo.setDiscountAmount(rs.getDouble("discount_amount"));
				}
				else {
					feeInfo.setTotalLateFeeAdded(rs.getDouble("totalLatePaymentCharges"));
				}
				feeInfo.setOtherCharges(rs.getDouble("other_charges"));
				feeInfo.setCheckOrDDNumber(rs.getString("cheque_or_dd_number"));
				feeInfo.setIfscCode(rs.getString("ifsc_code"));
				feeInfo.setBankName(rs.getString("bank_name"));
				feeInfo.setBankBranchName(rs.getString("bank_branch_name"));
				feeInfo.setCurrencyIconCss(rs.getString("icon_css_class"));
				feeInfo.setCurrencyCode(rs.getString("currency"));

				java.sql.Date refundDate = rs.getDate("refundDate");
				if(refundDate != null){
					feeInfo.setRefundAmount(rs.getString("refundAmount"));
					feeInfo.setRefundStatus(rs.getString("refundStatus"));
					feeInfo.setRefundDate(simpleDateFormat.format(refundDate));
				}

				return feeInfo;
			}
		});

		if (feesInfoList != null && feesInfoList.size() > 0) {
			feeInfo = feesInfoList.get(0);
		}
		logger.debug("getFeeInfo method ends *************************");
		return feeInfo;
	}

	/*protected void calculateTotalFeeCharges(FeesInfo feeInfo) {
		List<FeesCharges> lateFeesCharges = feeInfo.getFeesCharges();
		if (lateFeesCharges != null && lateFeesCharges.size() > 0) {
			double totalLateFeeAdded = 0.0;
			double totalFeeAmountAdded = 0.0;

			for (FeesCharges charges : lateFeesCharges) {
				if (charges.isLateFee()) {
					totalLateFeeAdded += charges.getAmountAdded();
				}
				else {
					totalFeeAmountAdded += charges.getAmountAdded();
				}
			}
			feeInfo.setTotalFeeAmountAdded(totalFeeAmountAdded);
			feeInfo.setTotalLateFeeAdded(rs.getDo);
		}
	}*/

	protected List<FeesCharges> getFeeCharges(Long feeScheduleId) {
		final List<FeesCharges> lateFeesCharges = new ArrayList<FeesCharges>();
		String sql = "select * from fees_late_payment_schedule where parent_fees_schedule_id = :feeScheduleId";

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("feeScheduleId", feeScheduleId);
		log.debug("get charges SQL :::: " + sql);
		log.debug("Parameters:::::" + parameters);
		getNamedParameterJdbcTemplate().query(sql, parameters, new RowMapper<Object>() {
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				FeesCharges charges = new FeesCharges();
				String addedOn = rs.getDate("created_at") != null ?
								Constant.USER_DATE_FORMAT.format(rs.getDate("created_at")) : null;

				charges.setAddedOn(addedOn);

				charges.setAmountAdded(rs.getDouble("amount"));
				//				charges.setCarryForwardAmount(rs.getDouble("carry_forward_amount"));
				//				charges.setFeeForDuration(rs.getString("fee_for_duration"));

				lateFeesCharges.add(charges);
				return null;
			}
		});

		return lateFeesCharges;
	}

	@Override
	public List<FeesCode> getApplicableFees(Integer standardId,Integer divisionId,Integer casteId,Integer branchId) {
		String baseQuery="select f.id as feesId, h.name feeType,f.fees_code_configuration_id as feeCodeConfigId ,f.head_id as headId,f.description as description,f.duedate as dueDate,f.amount as amount"+ 
				 " from fees as f  inner join head as h on f.head_id=h.id ";
		
		String getFeesByStandard= baseQuery + " where f.standard_id=? and "
				+ " f.branch_id=? and f.caste_id is null and f.division_id is null and f.is_delete='N' ";
		
		
		String getFeesBystdDiv = baseQuery + " where f.standard_id=? and "
				+ " f.branch_id=? and f.division_id=?  and f.caste_id is null and f.is_delete='N'";
		
		String getFeesByStDivCat = baseQuery + " where f.standard_id=? and f.branch_id=? and f.caste_id=?"
				+ " and division_id=? and f.is_delete='N'";
		
		String getFeesByStdCate= baseQuery + " where f.standard_id=?"
				+ " and f.branch_id=? and caste_id=?  and f.division_id is null and f.is_delete='N'";
		
		String query=null;
		Object [] param;
		List feesCodeList=null;
		
		if(standardId!=null && divisionId==null && casteId==null){
			query=getFeesByStandard;
			param=new Object[]{standardId,branchId};
			feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
			
		}
		else if(standardId!=null && divisionId!=null && casteId==null){
			
			query=getFeesBystdDiv;
			param=new Object[]{standardId,branchId,divisionId};
			feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
			if(feesCodeList.size()<=0){
				query=getFeesByStandard;
				param=new Object[]{standardId,branchId};
				feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
				
			}
		}
		else if(standardId!=null && divisionId!=null && casteId!=null){
			query=getFeesByStDivCat;
			param=new Object[]{standardId,branchId,casteId,divisionId};
			feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
			if(feesCodeList.size()<=0){
				
				query=getFeesBystdDiv;
				param=new Object[]{standardId,branchId,divisionId};
				feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
				if(feesCodeList.size()<=0){
					query=getFeesByStdCate;	
					param=new Object[]{standardId,branchId,casteId};
					
					feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
					if(feesCodeList.size()<=0){
						query=getFeesByStandard;
						param=new Object[]{standardId,branchId};
						feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
						
					}
				}
			}
			
			
		}
		else{	
			query=getFeesByStdCate;
			param=new Object[]{standardId,branchId,casteId};
			feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
			if(feesCodeList.size()<=0){
				query=getFeesByStandard;
				param=new Object[]{standardId,branchId};
				feesCodeList=getJdbcTemplate().query(query,param, new BeanPropertyRowMapper(FeesCode.class));
				
			}
		}			
			return feesCodeList;	
	}


	@Override
	public List<FeesCode> getAdditionalFeeCode(Integer branchId) {
		String query="select f.id as feesId,h.name feeType,feesCode.id as feeCodeConfigId ,f.head_id as headId,f.description as description,f.duedate as dueDate,f.amount as amount"
				+ " from fees as f  right join fees_code_configuration as feesCode"
				+ " on f.fees_code_configuration_id=feesCode.id inner join head as h on h.id=f.head_id"
				+ " where f.branch_id=? and f.standard_id is null and f.division_id is null and f.caste_id is null and f.is_delete='N'";
		
		try{
			return getJdbcTemplate().query(query,new Object[]{branchId}, new BeanPropertyRowMapper(FeesCode.class));
		
		}catch(Exception e){
		e.printStackTrace();
		throw new ApplicationException("ApplicationException");
		
		}
	}
}