package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isg.service.identity.dao.IdentityDao;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.identity.model.Identity.ROLES_ID;
import com.isg.service.user.client.notification.NotificationClient;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.ShoppingDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.model.Contact;
import com.isg.service.user.model.CustomBeanPropertyRowMapper;
import com.isg.service.user.model.ShoppingProfile;
import com.isg.service.user.model.UserChatProfile;
import com.isg.service.user.request.ChatProfile;
import com.isg.service.user.request.Parent;
import com.isg.service.user.request.ParentRegistrationByAdmin;
import com.isg.service.user.response.AuthenticationResponse;
import com.isg.service.user.response.InviteResponse;
import com.isg.service.user.response.OtpResponse;
import com.isg.service.user.response.ParentRegistrationByAdminResponse;
import com.isg.service.user.response.RegistrationResponse;
import com.isg.service.user.util.Constant;
import com.isg.service.user.util.Constant.EmailTemplate;
import com.isg.service.user.util.Constant.SmsTemplate;
import com.isg.service.user.util.FileStorePathAssembler;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class ParentDaoImpl extends BaseRegistrationDaoImpl implements ParentDao {
	
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}


	private static final Logger log = Logger.getLogger(ParentDaoImpl.class);

	@Autowired
	@Lazy
	private UserDao userDAO;

	@Autowired
	private ShoppingDao shoppingDao;

	@Autowired
	private IdentityDao identityDao;

	@Autowired
	private NotificationClient notificationClient;

	@Override
	@Transactional(readOnly = false)
	public RegistrationResponse registerParent(Parent parent, String authenticationType) throws IOException, NoSuchAlgorithmException, AuthenticationException,
					SQLException {

		log.debug("Registration process start parent object is :: " + parent.toString());

		log.info("Parent registration process start :: check user already exists");

		if (userDAO.isUserExist(parent.getEmail())) {
			throw new ApplicationException("User already exists in our database.");
		}

		log.info("Verify OTP start :: verifyOTP");
		AuthenticationResponse authenticationResponse = new AuthenticationResponse();
		RegistrationResponse registrationResponse = new RegistrationResponse();

		if (Constant.AUTHENTICATION_TYPE.APPLICATION.get().equals(authenticationType) && !userDAO.verifyOTP(parent.getPrimaryContact(), parent.getOtp())) {
			throw new ApplicationException("Provided OTP is incorrect.");
		}

		DataSource dataSourceObj = getDataSource();

		Long userId = createUser(parent.getEmail(), parent.getPassword(), ROLES_ID.PARENT.get(), authenticationType);
		createPaymentAccount(Constant.PaymentAccountType.QFIXPAY.get(), userId);

		SimpleJdbcInsert insertParent =
						new SimpleJdbcInsert(dataSourceObj)
										.withTableName("parent")
										.usingColumns("firstname", "email", "primaryContact", "user_id", "photo")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(4);
		parameters.put("firstname", parent.getFirstname());
		parameters.put("email", parent.getEmail());
		parameters.put("primaryContact", parent.getPrimaryContact());
		parameters.put("user_id", userId);
		parameters.put("photo", FileStorePathAssembler.NO_PROFILE_IMAGE_RELATIVE_PATH);
		Long parentId = (Long) insertParent.executeAndReturnKey(parameters);
		log.debug("Registered parent id::" + parentId);
		log.info("Registered parenton system");

		createChatAccount(parent.getFirstname(), parent.getLastname(), userId);
		log.info("Chat profile created successfully.");

		parent.setUser_id(userId.intValue());

		ShoppingProfile shoppingProfile = shoppingDao.createShoppingAccount(parent);
		log.info("Shopping profile created successfully.");
		log.info("Parent successfully registerd on the system.");

		Map<String, Object> content = new HashMap<String, Object>();
		content.put("first_name", parent.getFirstname());
		content.put("username", parent.getEmail());
		content.put("password", parent.getPassword());

		notificationClient.sendEmail(parent.getEmail(), EmailTemplate.PARENT_CREATE_EMAIL.get(), content);
		notificationClient.sendSMS(parent.getPrimaryContact(), SmsTemplate.PARENT_CREATE_SMS.get(), content);

		// Send email and sms.

		authenticationResponse.setShoppingProfile(shoppingProfile);

		UserChatProfile userChatProfile = userDAO.getUserChatProfileDetails(userId.intValue());
		if (userChatProfile != null) {
			ChatProfile chatProfile = chatDao.getChatSessionDetails(userChatProfile.getLogin(), userChatProfile.getPassword(),
							userChatProfile.getChatProfileId());
			authenticationResponse.setChatProfile(chatProfile);
			registrationResponse.setChatProfile(chatProfile);
		}

		// authenticationResponse.setToken(token.getToken());
		List<String> roles = new ArrayList<String>();
		roles.add(ROLES.PARENT.get());
		// authenticationResponse.setRoles(roles);

		// return authenticationResponse;
		// TODO remove after release
		registrationResponse.setParentId(parentId.intValue());
		registrationResponse.setShoppingProfile(shoppingProfile);
		registrationResponse.setRoles(roles);
		// registrationResponse.setToken(token.getToken());
		registrationResponse.setUserId(userId.intValue());
		// registrationResponse.setStatus("");
		registrationResponse.setMessage("");
		return registrationResponse;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean updateParent(Parent parent) throws ParseException {
		String query = "";
		boolean result = false;
		Connection conn = null;
		query = "update parent set firstname = ?, middlename = ?, lastname = ?, gender = ?, dob = ?, secondaryContact = ?, addressLine = ?, area = ?, " +
						" state_id = ?, district_id = ?, taluka_id = ?, city = ?, pincode = ?, religion_id = ?, caste_id = ?, " +
						" email = ?, primaryContact = ? where id = ?";

		try {
			conn = getDataSource().getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, parent.getFirstname());
			ps.setString(2, parent.getMiddlename());
			ps.setString(3, parent.getLastname());
			ps.setString(4, parent.getGender());

			if (parent.getDob() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date dob = format.parse(parent.getDob());
				ps.setDate(5, new java.sql.Date(dob.getTime()));
			} else {
				ps.setNull(5, Types.NULL);
			}

			ps.setString(6, parent.getSecondaryContact());
			ps.setString(7, parent.getAddressLine());
			ps.setString(8, parent.getArea());
			if (parent.getStateId() == null) {
				ps.setNull(9, Types.NULL);
			} else {
				ps.setInt(9, parent.getStateId());
			}

			if (parent.getDistrictId() == null) {
				ps.setNull(10, Types.NULL);
			} else {
				ps.setInt(10, parent.getDistrictId());
			}

			if (parent.getTalukaId() == null) {
				ps.setNull(11, Types.NULL);
			} else {
				ps.setInt(11, parent.getTalukaId());
			}

			ps.setString(12, parent.getCity());
			ps.setString(13, parent.getPincode());

			if (parent.getReligionId() == null) {
				ps.setNull(14, Types.NULL);
			} else {
				ps.setInt(14, parent.getReligionId());
			}

			if (parent.getCasteId() == null) {
				ps.setNull(15, Types.NULL);
			} else {
				ps.setInt(15, parent.getCasteId());
			}
			if (StringUtils.isEmpty(parent.getEmail())) {

				ps.setNull(16, Types.NULL);
			}
			else {
				ps.setString(16, parent.getEmail());

			}
			if (StringUtils.isEmpty(parent.getPrimaryContact())) {

				ps.setNull(17, Types.NULL);
			}
			else {
				ps.setString(17, parent.getPrimaryContact());

			}

			ps.setInt(18, parent.getId());

			ps.executeUpdate();
			ps.close();
			updateParentUpload(parent);
			result = true;
		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					log.error(e);
				}
			}
		}

		return result;
	}

	private boolean updateParentUpload(Parent parent) throws ParseException {
		String query = "";
		boolean result = false;
		Connection conn = null;

		query = "update parent_upload set firstname = ?, middlename = ?, lastname = ?, gender = ?, dob = ?, " +
						" secondaryContact = ?, addressLine = ?, area = ?, state_id = ?, district_id = ?, " +
						" taluka_id = ?, city = ?, pincode = ?, religion_id = ?, caste_id = ?, email = ?, primaryContact = ? " +
						" where id in (select parent_upload_id from branch_student_parent where parent_id = ?)";

		try {
			conn = getDataSource().getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, parent.getFirstname());
			ps.setString(2, parent.getMiddlename());
			ps.setString(3, parent.getLastname());
			ps.setString(4, parent.getGender());

			if (parent.getDob() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date dob = format.parse(parent.getDob());
				ps.setDate(5, new java.sql.Date(dob.getTime()));
			} else {
				ps.setNull(5, Types.NULL);
			}

			ps.setString(6, parent.getSecondaryContact());
			ps.setString(7, parent.getAddressLine());
			ps.setString(8, parent.getArea());
			if (parent.getStateId() == null) {
				ps.setNull(9, Types.NULL);
			} else {
				ps.setInt(9, parent.getStateId());
			}

			if (parent.getDistrictId() == null) {
				ps.setNull(10, Types.NULL);
			} else {
				ps.setInt(10, parent.getDistrictId());
			}

			if (parent.getTalukaId() == null) {
				ps.setNull(11, Types.NULL);
			} else {
				ps.setInt(11, parent.getTalukaId());
			}

			ps.setString(12, parent.getCity());
			ps.setString(13, parent.getPincode());

			if (parent.getReligionId() == null) {
				ps.setNull(14, Types.NULL);
			} else {
				ps.setInt(14, parent.getReligionId());
			}

			if (parent.getCasteId() == null) {
				ps.setNull(15, Types.NULL);
			} else {
				ps.setInt(15, parent.getCasteId());
			}
			if (StringUtils.isEmpty(parent.getEmail())) {

				ps.setNull(16, Types.NULL);
			}
			else {
				ps.setString(16, parent.getEmail());

			}
			if (StringUtils.isEmpty(parent.getPrimaryContact())) {

				ps.setNull(17, Types.NULL);
			}
			else {
				ps.setString(17, parent.getPrimaryContact());

			}

			ps.setInt(18, parent.getId());

			ps.executeUpdate();
			ps.close();
			result = true;
		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					log.error(e);
				}
			}
		}

		return result;
	}

	@Override
	public boolean registerMobile(Integer entityId, String gcmId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Object[] inputs = null;
		if (entityId != null && gcmId != null) {
			String query = "update parent set gcm_id = ? where id = ?";
			inputs = new Object[] { gcmId, entityId };
			int[] types = new int[] { Types.VARCHAR, Types.INTEGER };
			try {
				jdbcTemplate.update(query, inputs, types);
			} catch (Exception e) {
				log.error(e);
				return false;
			}
		}

		return true;
	}

	/**
	 * This method is used to get parent object based on parent id. This is used
	 * in attach student while retrieving list of student based on parent id.
	 */
	@Override
	public Parent getParent(Integer id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		List<Parent> parent = new ArrayList<>(1);
		if (id != null) {
			String query = "select u.username as username, p.*, bc.show_logo_on_parent_portal as showBranchLogo, "
					+ " (CASE WHEN bc.show_logo_on_parent_portal = 'Y' THEN b.logo_url ELSE null END) as branchLogoUrl "
					+ " from parent as p "
					+ " inner join user as u on u.id=p.user_id "
					+ " join branch_student_parent bsp on bsp.parent_id = p.id "
					+ " join branch b on bsp.branch_id = b.id "
					+ " join branch_configuration bc on bc.branch_id = b.id "
					+ "where p.id = ?";
			Object[] inputsParam = new Object[] { id };
			parent = jdbcTemplate.query(query, inputsParam,
							new CustomBeanPropertyRowMapper<Parent>(Parent.class));
		}

		return parent.isEmpty() ? null : parent.get(0);
	}

	/**
	 * This method is used to get parent object based on user id.
	 * 
	 */
	@Override
	public Parent getParentByUserId(Integer id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		List<Parent> parent = new ArrayList<>(1);
		if (id != null) {
			String query = "select * from parent where user_id = ?";
			Object[] inputsParam = new Object[] { id };
			parent = jdbcTemplate.query(query, inputsParam,
							new CustomBeanPropertyRowMapper<Parent>(Parent.class));
		}

		return parent.isEmpty() ? null : parent.get(0);
	}

	public void setUserDAO(UserDao userDAO) {
		this.userDAO = userDAO;
	}

	public void setShoppingDao(ShoppingDao shoppingDao) {
		this.shoppingDao = shoppingDao;
	}

	public void setIdentityDao(IdentityDao identityDao) {
		this.identityDao = identityDao;
	}

	public void setNotificationClient(NotificationClient notificationClient) {
		this.notificationClient = notificationClient;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<ParentRegistrationByAdminResponse> registerParentByAdmin(ParentRegistrationByAdmin parentRegistrationByAdminObj)
					throws Exception {

		log.debug("Registration process start parent object is :: " + parentRegistrationByAdminObj.toString());

		log.info("Admin Parent registration process start :: check user already exists");

		boolean isUserExists = false;
		String username = "";
		String firstname = parentRegistrationByAdminObj.getFirstname();
		String lastname = parentRegistrationByAdminObj.getLastname() != null ? parentRegistrationByAdminObj.getLastname() : "";

		List<Parent> parentInDatabaseList = retrieveParent(firstname, lastname, parentRegistrationByAdminObj.getParentUserId());

		if (!CollectionUtils.isEmpty(parentInDatabaseList)) {
			ParentRegistrationByAdminResponse parentRegistrationByAdminResponse = null;
			List<ParentRegistrationByAdminResponse> parentRegistrationByAdminResponseList = new ArrayList<ParentRegistrationByAdminResponse>();
			for (Parent parent : parentInDatabaseList) {
				parentRegistrationByAdminResponse = new ParentRegistrationByAdminResponse();
				parentRegistrationByAdminResponse.setUserId(parent.getUser_id());
				parentRegistrationByAdminResponse.setParentId(parent.getId());
				parentRegistrationByAdminResponseList.add(parentRegistrationByAdminResponse);
			}

			log.info("Number of Parents In database :::::: \n\n" + parentInDatabaseList.size());

			if (parentInDatabaseList.size() == 1) {
				updateParentContactDetails(parentRegistrationByAdminObj, parentInDatabaseList.get(0));
			}

			return parentRegistrationByAdminResponseList;
		}
		/*else if (userDAO.isEntityExist(parentRegistrationByAdminObj.getParentUserId(), parentRegistrationByAdminObj.getPrimaryContact())) {
			throw new Exception("user Id is already used of parent - " + parentRegistrationByAdminObj.getFirstname());
		}*/
		else if (userDAO.isParentExist(parentRegistrationByAdminObj.getParentUserId())) {

			throw new Exception("user Id is already used of parent - " + parentRegistrationByAdminObj.getFirstname());
		}

		return createParentLogin(parentRegistrationByAdminObj, isUserExists, firstname, lastname);

	}

	@Transactional(readOnly = false)
	private List<ParentRegistrationByAdminResponse> createParentLogin(ParentRegistrationByAdmin parentRegistrationByAdminObj, boolean isUserExists,
					String firstname, String lastname) throws NoSuchAlgorithmException, ParseException, JsonProcessingException, IOException {
		String username;
		if (!StringUtils.isEmpty(parentRegistrationByAdminObj.getParentUserId())) {
			isUserExists = userDAO.isUserExist(parentRegistrationByAdminObj.getParentUserId());
			username = parentRegistrationByAdminObj.getParentUserId();
			logger.debug("parentUserId" + parentRegistrationByAdminObj.getParentUserId());
		} else {

			username = firstname.replaceAll("([\\() \\.\\-_])", "") + (lastname != null ? lastname.replaceAll("([\\() \\.\\-_])", "") : "")
							+ RandomStringUtils.random(6, false, true) + "@" + Constant.EDUQFIX_DOMAIN;
		}

		ParentRegistrationByAdminResponse parentRegistrationByAdminResponse = null;
		if (isUserExists) {
			throw new ApplicationException("User Id specified is already taken by other user.");
		} else {

			String password = RandomStringUtils.random(8, true, true).toUpperCase();
			DataSource dataSourceObj = getDataSource();

			Long userId = createUser(username, password, ROLES_ID.PARENT.get(), Constant.AUTHENTICATION_TYPE.APPLICATION.get());
			createPaymentAccount(Constant.PaymentAccountType.QFIXPAY.get(), userId);

			SimpleJdbcInsert insertParent =
							new SimpleJdbcInsert(dataSourceObj)
											.withTableName("parent")
											.usingColumns("firstname", "lastname", "middlename", "dob", "gender", "email", "primaryContact",
															"secondaryContact", "user_id", "photo", "profession_id", "income_range_id")
											.usingGeneratedKeyColumns("id");
			Map<String, Object> parameters = new HashMap<String, Object>(4);
			parameters.put("firstname", parentRegistrationByAdminObj.getFirstname());
			parameters.put("lastname", lastname);
			parameters.put("middlename", parentRegistrationByAdminObj.getMiddlename());

			java.util.Date dob = null;
			if (parentRegistrationByAdminObj.getDob() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				dob = format.parse(parentRegistrationByAdminObj.getDob());
				parameters.put("dob", dob);
			} else {
				parameters.put("dob", null);
			}

			parameters.put("gender", parentRegistrationByAdminObj.getGender());
			parameters.put("email", parentRegistrationByAdminObj.getEmail());
			parameters.put("primaryContact", parentRegistrationByAdminObj.getPrimaryContact());
			parameters.put("secondaryContact", parentRegistrationByAdminObj.getSecondaryContact());
			parameters.put("user_id", userId);
			parameters.put("photo", FileStorePathAssembler.NO_PROFILE_IMAGE_RELATIVE_PATH);
			parameters.put("profession_id", parentRegistrationByAdminObj.getProfessionId());
			parameters.put("income_range_id", parentRegistrationByAdminObj.getIncomeId());
			Long parentId = (Long) insertParent.executeAndReturnKey(parameters);
			log.debug("Registered parent id::" + parentId);
			log.info("Registered parent on system");

			/*Commented on 11 Jan as now this will be done using scheduler job to reduce load on system in realtime
			Long chatProfileId = createChatAccount(parentRegistrationByAdminObj.getFirstname(),
							parentRegistrationByAdminObj.getLastname(),
							userId);
			log.info("Chat profile created successfully.");

			Parent parentObj = new Parent();
			parentObj.setDob(parentRegistrationByAdminObj.getDob());
			parentObj.setFirstname(parentRegistrationByAdminObj.getFirstname());
			parentObj.setLastname(lastname != null ? lastname : ".");
			parentObj.setEmail(username);
			parentObj.setPassword(password);
			parentObj.setPrimaryContact(parentRegistrationByAdminObj.getPrimaryContact());
			parentObj.setUser_id(userId.intValue());

			shoppingDao.createShoppingAccount(parentObj);
			log.info("Shopping profile created successfully.");
			*/

			log.info("Sending emails.");

			Map<String, Object> content = new HashMap<String, Object>();
			content.put("first_name", parentRegistrationByAdminObj.getFirstname());
			content.put("username", username);
			content.put("password", password);

			//notificationClient.sendEmail(parentRegistrationByAdminObj.getEmail(), EmailTemplate.PARENT_CREATE_EMAIL.get(), content);
			//notificationClient.sendSMS(parentRegistrationByAdminObj.getPrimaryContact(), SmsTemplate.PARENT_CREATE_SMS.get(), content);

			log.info("Parent successfully registerd.");

			parentRegistrationByAdminResponse = new ParentRegistrationByAdminResponse();
			parentRegistrationByAdminResponse.setUserId(userId.intValue());
			parentRegistrationByAdminResponse.setParentId(parentId.intValue());
			//parentRegistrationByAdminResponse.setChatProfileId(chatProfileId.intValue());

			List<ParentRegistrationByAdminResponse> parentRegistrationByAdminResponseList = new ArrayList<ParentRegistrationByAdminResponse>();
			parentRegistrationByAdminResponseList.add(parentRegistrationByAdminResponse);
			return parentRegistrationByAdminResponseList;

		}
	}

	private void updateParentContactDetails(ParentRegistrationByAdmin parentRegistrationByAdminObj, Parent parent) {

		String newEmail = StringUtils.isEmpty(parentRegistrationByAdminObj.getEmail()) ? "" : parentRegistrationByAdminObj.getEmail();
		String oldEmail = StringUtils.isEmpty(parent.getEmail()) ? "" : parent.getEmail();
		String newPhone = StringUtils.isEmpty(parentRegistrationByAdminObj.getPrimaryContact()) ? "" : parentRegistrationByAdminObj.getPrimaryContact();
		String oldPhone = StringUtils.isEmpty(parent.getPrimaryContact()) ? "" : parent.getPrimaryContact();

		String email = (StringUtils.isEmpty(oldEmail) || !oldEmail.equals(newEmail)) ? newEmail : oldEmail;
		String primaryContact = (StringUtils.isEmpty(oldPhone) || !oldPhone.equals(newPhone)) ? newPhone : oldPhone;
		boolean contactInformationChanged = (!newEmail.equalsIgnoreCase(oldEmail) || !newPhone.equalsIgnoreCase(oldPhone));

		String parentStudentCountQuery = "select count(*) as count from branch_student_parent where branch_student_parent.parent_id = :parentId and isDelete='N'";
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("parentId", parent.getId());

		Integer linkedStudentCount = getNamedParameterJdbcTemplate().queryForObject(parentStudentCountQuery, queryParameters,
						new RowMapper<Integer>() {
							@Override
							public Integer mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return rs.getInt("count");
							}
						});

		if (linkedStudentCount == 1) {
			String parentUpdateQuery = "update parent set firstname=:firstname, lastname=:lastname, middlename=:middlename,dob=:dob, " +
							" gender=:gender, email=:email, primaryContact=:primaryContact, secondaryContact=:secondaryContact,profession_id=:profession_id, " +
							" income_range_id = :income_range_id ";

			if (contactInformationChanged) {
				parentUpdateQuery += ",is_registration_email_sent='N' ";
			}

			parentUpdateQuery += " where id=:id";

			String lastname = parentRegistrationByAdminObj.getLastname() != null ? parentRegistrationByAdminObj.getLastname() : "";

			Map<String, Object> parameters = new HashMap<String, Object>(4);
			parameters.put("firstname", parentRegistrationByAdminObj.getFirstname());
			parameters.put("lastname", lastname);
			parameters.put("middlename", parentRegistrationByAdminObj.getMiddlename());

			java.util.Date dob = null;
			if (parentRegistrationByAdminObj.getDob() != null) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				try {
					dob = format.parse(parentRegistrationByAdminObj.getDob());
				} catch (ParseException e) {
					log.error(e);
					dob = null;
				}
				parameters.put("dob", dob);
			} else {
				parameters.put("dob", null);
			}

			parameters.put("gender", parentRegistrationByAdminObj.getGender());
			parameters.put("email", email);
			parameters.put("primaryContact", primaryContact);
			parameters.put("secondaryContact", parentRegistrationByAdminObj.getSecondaryContact());
			parameters.put("profession_id", parentRegistrationByAdminObj.getProfessionId());
			parameters.put("income_range_id", parentRegistrationByAdminObj.getIncomeId());
			parameters.put("id", parent.getId());
			getNamedParameterJdbcTemplate().update(parentUpdateQuery, parameters);
			log.debug("Updated parent record for id::" + parent.getId());

		}

	}

	private ParentRegistrationByAdminResponse getParentDetailsByUsername(String username) {
		String retriveParentQuery = "select p.id, p.user_id  from parent p " +
						" inner join user u on u.id = p.user_id " +
						" inner join user_role ur on ur.user_id = u.id and ur.role_id = 3 " +
						" where u.username = :username";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("username", username);

		log.debug("Retrive parent query :: " + retriveParentQuery);

		ParentRegistrationByAdminResponse parent = getNamedParameterJdbcTemplate().queryForObject(retriveParentQuery, queryParameters,
						new RowMapper<ParentRegistrationByAdminResponse>() {
							@Override
							public ParentRegistrationByAdminResponse mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								ParentRegistrationByAdminResponse parentRegistrationByAdminResponse = new ParentRegistrationByAdminResponse();
								parentRegistrationByAdminResponse.setUserId(resultSet.getInt("user_id"));
								parentRegistrationByAdminResponse.setParentId(resultSet.getInt("id"));

								return parentRegistrationByAdminResponse;
							}
						});

		return parent;
	}

	@Override
	public void sendInvite(String firstname, String email, String primaryContact, Integer userId) {

		OtpResponse otpResponse = userDAO.generateOTP(email, primaryContact, false);

		SimpleJdbcInsert inviteParentInsertQuery =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("account_invitation")
										.usingColumns("name", "email", "phone", "sent_by", "invite_code")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(5);
		parameters.put("name", firstname);
		parameters.put("email", email);
		parameters.put("phone", primaryContact);
		parameters.put("sent_by", userId);
		parameters.put("invite_code", otpResponse.getMessage());

		inviteParentInsertQuery.execute(parameters);
		Contact contact = userDAO.getContactDetailsByUserId(userId);
		Map<String, Object> content = new HashMap<String, Object>();
		content.put("first_name", firstname);
		content.put("invitation_code", otpResponse.getMessage());
		content.put("invitated_by", contact.getFirstName() + " " + contact.getLastName());

		if ("Success".equalsIgnoreCase(otpResponse.getCode())) {
			notificationClient.sendEmail(email, EmailTemplate.INVITE_PARENT_EMAIL.get(), content);
			notificationClient.sendSMS(primaryContact, SmsTemplate.INVITE_PARENT_SMS.get(), content);
		}
		log.debug("Invitation sent to parent ");

	}

	@Override
	public InviteResponse validateInvite(String otp) {

		userDAO.verifyOTP(otp);

		String retriveParentInviteQuery = "select * from account_invitation where invite_code=:otp";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("otp", otp);

		log.debug("Retrive parent query :: " + retriveParentInviteQuery);

		InviteResponse inviteResponse = getNamedParameterJdbcTemplate().queryForObject(retriveParentInviteQuery, queryParameters,
						new RowMapper<InviteResponse>() {
							@Override
							public InviteResponse mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {
								InviteResponse inviteResponse = new InviteResponse();
								inviteResponse.setName(resultSet.getString("name"));
								inviteResponse.setEmail(resultSet.getString("email"));
								inviteResponse.setPhone(resultSet.getString("phone"));
								return inviteResponse;
							}
						});

		OtpResponse otpResponse = userDAO.generateOTP(inviteResponse.getEmail(), inviteResponse.getPhone(), false);
		inviteResponse.setOtp(otpResponse.getMessage());
		return inviteResponse;
	}

	@Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true)
	private List<Parent> retrieveParent(String firstname, String lastname, String username) {

		log.debug("Check parent already exists" + username);

		String retriveParentQuery = "select * from parent where firstname=:firstname and lastname=:lastname and user_id in(select id from user where username=:username)";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("firstname", firstname);
		queryParameters.put("lastname", lastname);
		queryParameters.put("username", username);

		log.debug("Retrive parent query :: " + retriveParentQuery);
		log.debug("queryParameters :: " + queryParameters);

		List<Parent> parentList = getNamedParameterJdbcTemplate().query(retriveParentQuery, queryParameters,
						new CustomBeanPropertyRowMapper<Parent>(Parent.class));

		return parentList;
	}
}
