package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.DistrictDao;
import com.isg.service.user.request.District;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class DistrictDaoImpl extends JdbcDaoSupport implements DistrictDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public List<District> getDistricts() {
		String retriveDistrict = "select * from district where active = 'Y'";
		return getJdbcTemplate().query(retriveDistrict, new BeanPropertyRowMapper<District>(District.class));
	}

	@Override
	public List<District> getDistrictsByStateId(int id) {
		String retriveDistrict = "select * from district where active='Y' and state_id = ?";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().query(retriveDistrict, inputs, new BeanPropertyRowMapper<District>(District.class));
	}

}
