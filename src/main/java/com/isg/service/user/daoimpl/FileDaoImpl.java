package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.FileDao;
import com.isg.service.user.util.FileStorePathAssembler;

@Repository
public class FileDaoImpl implements FileDao {

	@Override
	public void saveFile(String localStorePath, byte[] bytes, String fileNameWithExtension) throws IOException {
		// Create missing parent hierarchy if any
		final Path parentFolderPath = Paths.get(localStorePath);

		if (parentFolderPath != null) {
			Files.createDirectories(parentFolderPath);
		}

		// Store file at specified path
		fileNameWithExtension = fileNameWithExtension.startsWith(FileStorePathAssembler.PATH_SEPARATOR) ? fileNameWithExtension
						: (FileStorePathAssembler.PATH_SEPARATOR + fileNameWithExtension);

		final Path fileStoragePath = Paths.get(localStorePath + fileNameWithExtension);
		Files.write(fileStoragePath, bytes);
	}

	@Override
	public void saveFile(String filePath, byte[] bytes) throws IOException {
		// Create missing parent hierarchy if any
		int indexOfLastFileSeparator = filePath.lastIndexOf("/");

		if (indexOfLastFileSeparator > 0) {
			String parentDirPath = filePath.substring(0, indexOfLastFileSeparator);

			final Path parentFolderPath = Paths.get(parentDirPath);

			if (parentFolderPath != null) {
				Files.createDirectories(parentFolderPath);
			}
		}

		final Path fileStoragePath = Paths.get(filePath);
		Files.write(fileStoragePath, bytes);
	}
}
