package com.isg.service.user.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.client.notification.Notification;
import com.isg.service.user.client.notification.NotificationClient;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.ServiceRequestDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.request.Parent;
import com.isg.service.user.request.ServiceRequest;
import com.isg.service.user.request.ServiceRequestType;
import com.isg.service.user.request.StudentBasicDetails;
import com.isg.service.user.util.Constant.EmailTemplate;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class ServiceRequestDaoImpl extends BaseRegistrationDaoImpl implements ServiceRequestDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private static final Logger log = Logger.getLogger(ServiceRequestDaoImpl.class);
	
	@Autowired
	private StudentDao studentDao;
	
	@Autowired
	private ParentDao parentDao;
	
	@Autowired
	private NotificationClient notificationClient;

	@Override
	public List<ServiceRequestType> getServiceRequestTypeByBranchId(Integer branchId) {
		
		String retriveServiceReuqest = "select * from service_request where active='Y' and branch_id = "+branchId+" order by request_type";
		return getJdbcTemplate().query(retriveServiceReuqest, new BeanPropertyRowMapper<ServiceRequestType>(ServiceRequestType.class));
	}
	
	@Override
	public ServiceRequestType getServiceRequestType(Integer id) {
		
		String query = "select * from service_request where active='Y' and id = :id";
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("id", id);

		ServiceRequestType serviceRequestType= getNamedParameterJdbcTemplate().queryForObject(query, queryParameters,
		new RowMapper<ServiceRequestType>() {
			@Override
			public ServiceRequestType mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				ServiceRequestType serviceRequestType = new ServiceRequestType();
				serviceRequestType.setEmail(resultSet.getString("email"));
				serviceRequestType.setEmailResponse(resultSet.getString("email_response"));
				serviceRequestType.setRequestType(resultSet.getString("request_type"));
				return serviceRequestType;
			}
		});
		return serviceRequestType;
	}

	@Override
	public void sendServiceRequestNotification(ServiceRequest serviceRequest) {
		
		// TODO Auto-generated method stub
		Parent parent = null;
		StudentBasicDetails student = null;
		ServiceRequestType requestType = null;
		Integer studentId = serviceRequest.getStudentId();
		Integer parentId = serviceRequest.getParentId();
		Integer serviceRequestTypeId  = serviceRequest.getServiceRequestId();
		String emailBody = serviceRequest.getEmailBody();
		
		if(parentId != null) {
			parent = parentDao.getParent(parentId);
		}
		
		if(studentId != null) {
			student = studentDao.getStudentDetailsForNotification(studentId);
		}
		
		if(serviceRequestTypeId != null) {
			requestType = getServiceRequestType(serviceRequestTypeId);
		}
	
		
		if(emailBody != null && requestType != null) {
			if(parent != null && student != null) {
					
				Notification notification = new Notification();
				notification.setTemplateCode(EmailTemplate.SERVICE_REQUEST_PARENT.get());
				Map<String, Object> subjectContent = new HashMap<String, Object>();
				subjectContent.put("request_type", requestType.getRequestType());
				
				String senderName = parent.getFirstname() + " "+ parent.getLastname();

				Map<String, Object> content = new HashMap<String, Object>();
				content.put("sender_name", senderName);
				content.put("request_body", emailBody);
				
				setStudentDetails(content, student);
				
				content.put("parent_name", senderName);
				content.put("parent_mobile", parent.getPrimaryContact());
				content.put("parent_email", parent.getEmail());
				notification.setContent(content);
				notification.setType("EMAIL");
				notification.setTo(requestType.getEmail());

				notification.setSubjectContent(subjectContent);
				
				//send to school
				try {
					String jsonSend = new ObjectMapper().writeValueAsString(notification);
					log.debug(jsonSend);
					notificationClient.sendMessage(jsonSend);
				} catch (Exception e) {
					log.error(e);
				}
				
				//send to parent for reference
				try {
					content.put("sender_name", student.getBranchName());
					content.put("email_response", requestType.getEmailResponse());
					notification.setTo(parent.getEmail());
					notification.setTemplateCode(EmailTemplate.SERVICE_REQUEST_RESPONSE_PARENT.get());
					String jsonSend = new ObjectMapper().writeValueAsString(notification);
					log.debug(jsonSend);
					notificationClient.sendMessage(jsonSend);
				} catch (Exception e) {
					log.error(e);
				}
				
			} else if(student != null) {
				
				String senderName = student.getName();
				Notification notification = new Notification();
				notification.setTemplateCode(EmailTemplate.SERVICE_REQUEST_STUDENT.get());
				Map<String, Object> subjectContent = new HashMap<String, Object>();
				subjectContent.put("request_type", requestType.getRequestType());

				Map<String, Object> content = new HashMap<String, Object>();
				content.put("sender_name", senderName);
				content.put("request_body", emailBody);
				
				setStudentDetails(content, student);
				
				notification.setContent(content);
				notification.setType("EMAIL");
				notification.setTo(requestType.getEmail());

				notification.setSubjectContent(subjectContent);

				//send to school
				try {
					String jsonSend = new ObjectMapper().writeValueAsString(notification);
					log.debug(jsonSend);
					notificationClient.sendMessage(jsonSend);
				} catch (Exception e) {
					log.error(e);
				}
				
				//send to student for reference
				if(student.getEmail() != null) {
					try {
						content.put("sender_name", student.getBranchName());
						content.put("email_response", requestType.getEmailResponse());
						notification.setTo(student.getEmail());
						notification.setTemplateCode(EmailTemplate.SERVICE_REQUEST_RESPONSE_STUDENT.get());
						String jsonSend = new ObjectMapper().writeValueAsString(notification);
						log.debug(jsonSend);
						notificationClient.sendMessage(jsonSend);
					} catch (Exception e) {
						log.error(e);
					}
				}

			} else {
				throw new ApplicationException("Student or Parent details must be sepcified");
			}
		} else{
			throw new ApplicationException("Request Body and Service Type is required.");
		}
	}
	
	private void setStudentDetails(Map<String, Object> content, StudentBasicDetails student) {
		
		//request_body
		//student_name
		/*standard
		division
		roll_number
		registration_code
		student_mobile
		student_email*/
		content.put("student_name", student.getName());
		content.put("standard", student.getStandard_name());
		content.put("division", student.getDivision());
		content.put("roll_number", student.getRollNumber());
		content.put("registration_code", student.getRegistrationCode());
		content.put("student_mobile", student.getMobile());
		content.put("student_email", student.getEmail());
		content.put("logo_url", student.getInstituteLogoUrl());
		
	}
	
}
