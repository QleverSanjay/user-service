package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.InstituteDao;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.Institute;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class InstituteDaoImpl extends JdbcDaoSupport implements InstituteDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<Institute> getAll() {
		String retriveInstitute = "select * from institute";
		return getJdbcTemplate().query(retriveInstitute, new BeanPropertyRowMapper<Institute>(Institute.class));

	}

	@Override
	public Institute getById(Integer id) {
		String retriveInstitute = "select * from institute where id= ?";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().queryForObject(retriveInstitute, inputs, new BeanPropertyRowMapper<Institute>(Institute.class));

	}

	@Override
	public List<Branch> getBranchesByInstituteId(int id) {
		String retriveBranches = " Select b.*,bc.branch_id,bc.student_data_available,bc.unique_identifier_lable,bc.unique_identifier_name from branch as b left join branch_configuration as bc on b.id=bc.branch_id where b.active='Y' and b.institute_id=?";

		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().query(retriveBranches, inputs, new BeanPropertyRowMapper<Branch>(Branch.class));
	}

	@Override
	public Branch getBranchDetails(Integer id) {
		String retriveBranches = "Select * from branch where active='Y' and id =?";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().queryForObject(retriveBranches, inputs, new BeanPropertyRowMapper<Branch>(Branch.class));
	}

	@Override
	public List<Institute> searchInstitute(String name) {
		String query = "SELECT id, name FROM institute WHERE name LIKE ? and allowed_paydirect='Y'";
		Object[] inputs = new Object[] { "%"+name+"%" };
		return getJdbcTemplate().query(query, inputs, new BeanPropertyRowMapper<Institute>(Institute.class));
	}

}
