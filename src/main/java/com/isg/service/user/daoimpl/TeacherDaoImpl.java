package com.isg.service.user.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.TeacherDao;
import com.isg.service.user.request.Division;
import com.isg.service.user.request.Standard;
import com.isg.service.user.request.Teacher;
import com.isg.service.user.request.TeacherRowMapper;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class TeacherDaoImpl extends NamedParameterJdbcDaoSupport implements TeacherDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public Teacher getDetails(Integer teacherId) {
		String query = "select * from teacher where id = :teacherId";
		Map<String, Object> parameters = new HashMap<String, Object>(19);
		parameters.put("teacherId", teacherId);
		return getNamedParameterJdbcTemplate().queryForObject(query, parameters, new TeacherRowMapper());
	}

	@Override
	public boolean update(Teacher teacher) {
		String updateSql = "update teacher set firstName=:firstname, lastName=:lasteName, dateOfBirth=:dob,emailAddress=:email, gender=:gender, "
						+
						" primaryContact=:primartContact, secondaryContact=:secondaryContact, addressLine=:addressLine, area=:area, city=:city, pinCode=:pincode "
						+
						" , state_id=:stateId, district_id =:districtId, taluka_id = :talukaId where id= :id";

		Map<String, Object> parameters = new HashMap<String, Object>(12);
		parameters.put("firstname", teacher.getFirstName());
		parameters.put("lasteName", teacher.getLastName());
		parameters.put("dob", teacher.getDob());
		parameters.put("email", teacher.getEmail());
		parameters.put("gender", teacher.getGender());
		parameters.put("primartContact", teacher.getPrimaryContactNumber());
		parameters.put("secondaryContact", teacher.getSecondContactNumber());
		parameters.put("addressLine", teacher.getAddressLine());
		parameters.put("area", teacher.getArea());
		parameters.put("city", teacher.getCity());
		parameters.put("pincode", teacher.getPincode());
		parameters.put("id", teacher.getId());
		parameters.put("stateId", teacher.getStateId());
		parameters.put("districtId", teacher.getDistrictId());
		parameters.put("talukaId", teacher.getTalukaId());

		int status = getNamedParameterJdbcTemplate().update(updateSql, parameters);

		if (status == 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public List<Standard> getStandard(Integer entityId) {
		String query = "select s.* from teacher_standard ts inner join standard s on s.id = ts.standard_id " +
						" where ts.teacher_id = :entityId and ts.is_delete = 'N' and s.active='Y' group by s.id";
		Map<String, Object> parameters = new HashMap<String, Object>(19);
		parameters.put("entityId", entityId);

		return getNamedParameterJdbcTemplate().query(query, parameters,
						new RowMapper<Standard>() {
							@Override
							public Standard mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {

								Standard standard = new Standard();
								standard.setId(resultSet.getInt("id"));
								standard.setDisplayName(resultSet.getString("displayName"));

								return standard;

							}
						});

	}

	@Override
	public List<Division> getDivision(Integer entityId, Integer standardId) {
		String query = "select d.* from teacher_standard ts inner join division d on d.id = ts.division_id " +
						" where ts.teacher_id = :entityId and ts.standard_id=:standardId and ts.is_delete = 'N' and d.active='Y' group by d.id";
		Map<String, Object> parameters = new HashMap<String, Object>(19);
		parameters.put("entityId", entityId);
		parameters.put("standardId", standardId);

		return getNamedParameterJdbcTemplate().query(query, parameters,
						new RowMapper<Division>() {
							@Override
							public Division mapRow(ResultSet resultSet, int rowNum)
											throws SQLException {

								Division division = new Division();
								division.setId(resultSet.getInt("id"));
								division.setDisplayName(resultSet.getString("displayName"));

								return division;

							}
						});
	}
}
