package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.isg.service.user.client.shopping.EcommerceRestClient;
import com.isg.service.user.client.shopping.ShoppingUrlConfiguration;
import com.isg.service.user.client.shopping.response.session.CreateCustomerResponse;
import com.isg.service.user.dao.ShoppingDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.ShoppingProfile;
import com.isg.service.user.model.shopping.OrderDetails;
import com.isg.service.user.model.shopping.OrderPaymentDetails;
import com.isg.service.user.request.Parent;
import com.isg.service.user.util.DateFormatter;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class ShoppingDaoImpl extends NamedParameterJdbcDaoSupport implements ShoppingDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	private static final Logger log = Logger.getLogger(ShoppingDaoImpl.class);

	// static XmlMapper mapper = null;

	@Override
	public ShoppingProfile getShoppingSessionDetails(String shoppingAccountUsername, String shoppingAccountPassword) {
		ShoppingProfile shoppingProfile = null;
		try {

			MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<String, String>();
			valueMap.add("username", shoppingAccountUsername);
			valueMap.add("password", shoppingAccountPassword);

			EcommerceRestClient restClient = new EcommerceRestClient();

			ResponseEntity<String> createSessionResponse = restClient
							.executeRequest(valueMap,
											ShoppingUrlConfiguration.CREATE_SESSION_CONFIG, false);

			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				shoppingProfile = new ShoppingProfile();
				shoppingProfile.setCookies(restClient.getCookies(createSessionResponse));
				shoppingProfile.setCookieExpiresOn(restClient.getCookieExpireTime(createSessionResponse.getHeaders().toString()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return shoppingProfile;
	}

	@Override
	public ShoppingProfile createShoppingAccount(Parent parent) throws IOException {
		ShoppingProfile shoppingProfile = null;

		MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<String, String>();

		if (parent.getDob() != null) {
			valueMap.add("birthdate", DateFormatter.formatDate(parent.getDob(), DateFormatter.DATE_FORMAT_YYYYMMDD_HYPHEN_SEPARATED,
							DateFormatter.DATE_FORMAT_DDMMYYYY_FORWARD_SLASH_SEPARATED));
		}

		valueMap.add("firstname", parent.getFirstname());
		valueMap.add("lastname",
						(parent.getLastname() != null ? parent.getLastname() : "."));
		valueMap.add("email", parent.getEmail());
		valueMap.add("password", parent.getPassword());
		valueMap.add("confirmation", parent.getPassword());
		valueMap.add("is_subscribed", "1");
		valueMap.add("mobile", parent.getPrimaryContact());

		EcommerceRestClient restClient = new EcommerceRestClient();

		try {
			ResponseEntity<String> createCustomerProfileResponse = restClient
							.executeRequest(valueMap,
											ShoppingUrlConfiguration.CREATE_CUSTOMER_CONFIG, true);
			if (createCustomerProfileResponse.getStatusCode() == HttpStatus.OK) {

				CreateCustomerResponse createCustomerResponse = getXmlMapper().readValue(createCustomerProfileResponse.getBody(), CreateCustomerResponse.class);

				createShoppingProfile(parent.getEmail(), parent.getPassword(), parent.getUser_id(), createCustomerResponse.getCustomer_id());

				// Get session details
				shoppingProfile = getShoppingSessionDetails(parent.getEmail(), parent.getPassword());

			}

			if (createCustomerProfileResponse.getStatusCode() != HttpStatus.OK || shoppingProfile == null)
			{
				log.error("Exception occured in shopping profile registration ::: " + createCustomerProfileResponse.getBody());
				//throw new ApplicationException("Exception occured in shopping profile registration ::: ");
			}
		} catch (Exception e) {
			log.error("Exception occured in shopping profile registration ::: ", e);
		}
		return shoppingProfile;

	}

	@Override
	public ShoppingProfile getShoppingProfileDetails(Integer userId) {
		// List<ShoppingProfile> shoppingProfile = new
		// ArrayList<ShoppingProfile>(0);

		ShoppingProfile shoppingProfile = null;

		if (userId != null) {
			String query = "select * from user_mall_profile where user_id = ?";
			Object[] inputsParam = new Object[] { userId };
			try {
				shoppingProfile = getJdbcTemplate().queryForObject(query, inputsParam,
								new RowMapper<ShoppingProfile>() {
									@Override
									public ShoppingProfile mapRow(ResultSet rs, int arg1) throws SQLException {
										return new ShoppingProfile(rs.getString("username"), rs.getString("password"), rs.getInt("shopping_profile_id"));

									}
								}
								);
			} catch (Exception e) {
				log.error("No matching shopping profile found in database. Ignoring silently", e);
			}
		}

		return shoppingProfile;

	}

	@Override
	public ShoppingProfile getShoppingProfileDetails(String username) {

		ShoppingProfile shoppingProfile = null;

		if (username != null) {
			String query = "select * from user_mall_profile where username = ?";
			Object[] inputsParam = new Object[] { username };
			try {
				shoppingProfile = getJdbcTemplate().queryForObject(query, inputsParam,
								new RowMapper<ShoppingProfile>() {
									@Override
									public ShoppingProfile mapRow(ResultSet rs, int arg1) throws SQLException {
										return new ShoppingProfile(rs.getString("username"), rs.getString("password"), rs.getInt("shopping_profile_id"), rs
														.getInt("user_id"));

									}
								}
								);
			} catch (Exception e) {
				log.error("No matching shopping profile found in database. Ignoring silently", e);
			}
		}

		return shoppingProfile;

	}

	@Override
	public void createShoppingProfile(String username, String password, Integer userId) {
		createShoppingProfile(username, password, userId, null);
	}

	@Override
	public void createShoppingProfile(String username, String password, Integer userId, Integer shoppingProfileId) {
		Map<String, Object> parameters = new HashMap<String, Object>(3);
		SimpleJdbcInsert insertUserShoppingProfile = new SimpleJdbcInsert(
						getDataSource()).withTableName("user_mall_profile")
						.usingColumns("user_id", "username", "password", "shopping_profile_id")
						.usingGeneratedKeyColumns("id");

		parameters.put("user_id", userId);
		parameters.put("username", username);
		parameters.put("password", password);
		parameters.put("shopping_profile_id", shoppingProfileId);
		insertUserShoppingProfile.execute(parameters);

	}

	public com.fasterxml.jackson.dataformat.xml.XmlMapper getXmlMapper() {
		JacksonXmlModule module = new JacksonXmlModule();
		module.setDefaultUseWrapper(true);
		XmlMapper mapper = new XmlMapper(module);
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}

	@Override
	public OrderPaymentDetails getShoppingCartDetails(ShoppingProfile shoppingProfile, String orderId) throws IOException {

		OrderPaymentDetails cartDetails = null;

		EcommerceRestClient client = new EcommerceRestClient();

		MultiValueMap<String, String> valueMap = new
						LinkedMultiValueMap<String, String>();

		valueMap.add("username", shoppingProfile.getUsername());
		valueMap.add("password", shoppingProfile.getPassword());

		String cookieText = client.getCookies(valueMap);

		Object[] obj = new Object[] { orderId };
		ResponseEntity<String> response = client.executeRequest(null, cookieText, ShoppingUrlConfiguration.GET_ORDER_DETAIL_CONFIG, obj, true);

		if (response.getStatusCode() == HttpStatus.OK) {
			OrderDetails orderDetails = getXmlMapper().readValue(response.getBody(), OrderDetails.class);
			cartDetails = orderDetails.getTotals();
		} else {
			log.error("Exception occured in shopping cart details retieval ::: " + response.getBody());
			throw new ApplicationException("Exception occured in shopping cart details retieval ::: ");
		}

		return cartDetails;
	}

	@Override
	public boolean acknowledge(ShoppingProfile shoppingProfile, Integer entityId, Integer userId, String orderId) {
		boolean paymentAcknowledged = false;

		EcommerceRestClient client = new EcommerceRestClient();

		MultiValueMap<String, String> valueMap = new
						LinkedMultiValueMap<String, String>();

		valueMap.add("username", shoppingProfile.getUsername());
		valueMap.add("password", shoppingProfile.getPassword());

		String cookieText;
		try {
			cookieText = client.getCookies(valueMap);

			valueMap = new LinkedMultiValueMap<String, String>();
			valueMap.add("id", String.valueOf(orderId));
			valueMap.add("status", "PROCESS");

			client.executeRequest(valueMap, cookieText, ShoppingUrlConfiguration.ACKNOWLEDGE_PAYMENT_CONFIG, null, true);

			paymentAcknowledged = true;
		} catch (JsonProcessingException e) {
			log.error("Exception occured in shopping payment acknowledgement ::: ", e);
		}

		return paymentAcknowledged;

	}

	@Override
	public boolean linkCustomerAccounts(Integer parentUserId, Integer childUserId) {
		boolean accountsLinked = false;

		ShoppingProfile parentRecord = getShoppingProfileDetails(parentUserId);
		ShoppingProfile childRecord = getShoppingProfileDetails(childUserId);

		EcommerceRestClient client = new EcommerceRestClient();

		MultiValueMap<String, String> valueMap = new
						LinkedMultiValueMap<String, String>();

		valueMap.add("username", parentRecord.getUsername());
		valueMap.add("password", parentRecord.getPassword());

		String cookieText;
		try {
			cookieText = client.getCookies(valueMap);

			valueMap = new LinkedMultiValueMap<String, String>();
			valueMap.add("parent_customer_id", String.valueOf(parentRecord.getShoppingProfileId()));
			valueMap.add("child_customer_id", String.valueOf(childRecord.getShoppingProfileId()));

			client.executeRequest(valueMap, cookieText, ShoppingUrlConfiguration.LINK_ACCOUNTS_CONFIG, null, true);

			accountsLinked = true;
		} catch (JsonProcessingException e) {
			log.error("Exception occured in shopping payment acknowledgement ::: ", e);
		}

		return accountsLinked;

	}
}
