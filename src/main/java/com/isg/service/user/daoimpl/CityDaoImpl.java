package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.CityDao;
import com.isg.service.user.request.City;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class CityDaoImpl extends JdbcDaoSupport implements CityDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public List<City> getCities() {
		String retriveCity = "select * from city where active = 'Y'";
		return getJdbcTemplate().query(retriveCity, new BeanPropertyRowMapper<City>(City.class));
	}

	@Override
	public List<City> getCitiesByTalukaId(int id) {
		String retriveCity = "select * from city where active='Y' and taluka_id = ?";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().query(retriveCity, inputs, new BeanPropertyRowMapper<City>(City.class));
	}

	@Override
	public List<City> getCitiesByName(String name) {
		String retriveCity = "select * from city where active='Y' and name LIKE ?";
		String cityName = "%"+name+"%";
		Object[] inputs = new Object[] { cityName };
		return getJdbcTemplate().query(retriveCity, inputs, new BeanPropertyRowMapper<City>(City.class));
	}

}
