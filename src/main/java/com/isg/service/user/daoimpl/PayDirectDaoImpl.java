package com.isg.service.user.daoimpl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.PayDirectDao;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.Institute;

@Repository
public class PayDirectDaoImpl extends BaseRegistrationDaoImpl implements PayDirectDao{

	

	@Override
	public List<Branch> getBranchesByInstituteId(int id) {
		String retriveBranches = " Select b.*,bc.branch_id,bc.student_data_available,bc.unique_identifier_lable,bc.unique_identifier_name from branch as b left join branch_configuration as bc on b.id=bc.branch_id where b.active='Y' and b.is_delete = 'N' and b.institute_id=? and bc.is_paydirect_enabled='Y'";

		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().query(retriveBranches, inputs, new BeanPropertyRowMapper<Branch>(Branch.class));
	}

	@Override
	public List<Institute> searchInstitute(String name) {
		String query = "SELECT id, name FROM institute WHERE name LIKE ? AND isDelete = 'N' LIMIT 5";
		Object[] inputs = new Object[] { "%"+name+"%" };
		return getJdbcTemplate().query(query, inputs, new BeanPropertyRowMapper<Institute>(Institute.class));
	}


}
