package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.isg.service.user.dao.FileDao;
import com.isg.service.user.dao.ManageProfileDao;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.TeacherDao;
import com.isg.service.user.dao.VendorDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.model.FileAttachment;
import com.isg.service.user.request.Parent;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.Teacher;
import com.isg.service.user.request.Vendor;
import com.isg.service.user.util.FileStorePathAssembler;
import com.isg.service.user.util.FileStorePathAssembler.PROFILE_FOLDER_TYPE;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class ManageProfileDaoImpl extends NamedParameterJdbcDaoSupport implements ManageProfileDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Autowired
	StudentDao studentDao;

	@Autowired
	FileDao fileDao;

	@Autowired
	ParentDao parentDao;

	@Autowired
	TeacherDao teacherDao;

	@Autowired
	VendorDao vendorDao;

	@Override
	public String updateProfilePicture(Integer entityId, Integer instituteId, Integer branchId, String role, FileAttachment fileAttachment) throws IOException {

		String uploadedPirctureUrl = "";
		if ("STUDENT".equalsIgnoreCase(role)) {
			uploadedPirctureUrl = updateStudentProfilePicture(entityId, instituteId, branchId, fileAttachment);
		} else if ("PARENT".equalsIgnoreCase(role)) {
			uploadedPirctureUrl = updateParentProfilePicture(entityId, instituteId, branchId, fileAttachment);
		} else if ("TEACHER".equalsIgnoreCase(role)) {
			uploadedPirctureUrl = updateTeacherProfilePicture(entityId, instituteId, branchId, fileAttachment);
		} else if ("VENDOR".equalsIgnoreCase(role)) {
			uploadedPirctureUrl = updateVendorProfilePicture(entityId, instituteId, branchId, fileAttachment);
		} else {
			throw new ApplicationException("Unsupported operation");
		}

		return "{\"url\":\"" + uploadedPirctureUrl + "\"}";
	}

	private String updateStudentProfilePicture(Integer entityId, Integer instituteId, Integer branchId, FileAttachment fileAttachment) throws IOException {
		Student student = studentDao.getStudentProfileDetailsbyId(entityId);
		String fileNameWithExtension = student.getPersonalDetails().getPhoto();
		String updateSql = "update student set photo=:photoUrl where id=:entityId";
		return updateProfilePicture(entityId, instituteId, branchId, fileAttachment, fileNameWithExtension, PROFILE_FOLDER_TYPE.STUDENT_PROFILE, updateSql);

	}

	private String updateParentProfilePicture(Integer entityId, Integer instituteId, Integer branchId, FileAttachment fileAttachment) throws IOException {
		Parent parent = parentDao.getParent(entityId);
		String fileNameWithExtension = parent.getPhoto();
		String updateSql = "update parent set photo=:photoUrl where id=:entityId";
		return updateProfilePicture(entityId, instituteId, branchId, fileAttachment, fileNameWithExtension, PROFILE_FOLDER_TYPE.PARENT_PROFILE, updateSql);
	}

	private String updateTeacherProfilePicture(Integer entityId, Integer instituteId, Integer branchId, FileAttachment fileAttachment) throws IOException {
		Teacher teacher = teacherDao.getDetails(entityId);
		String fileNameWithExtension = teacher.getPhoto();
		String updateSql = "update teacher set photo=:photoUrl where id=:entityId";
		return updateProfilePicture(entityId, instituteId, branchId, fileAttachment, fileNameWithExtension, PROFILE_FOLDER_TYPE.TEACHER_PROFILE, updateSql);
	}

	private String updateVendorProfilePicture(Integer entityId, Integer instituteId, Integer branchId, FileAttachment fileAttachment) throws IOException {
		Vendor vendor = vendorDao.getById(entityId);
		String fileNameWithExtension = vendor.getPhoto();
		String updateSql = "update vendor set photo=:photoUrl where id=:entityId";
		return updateProfilePicture(entityId, instituteId, branchId, fileAttachment, fileNameWithExtension, PROFILE_FOLDER_TYPE.VENDOR_PROFILE, updateSql);
	}

	private String updateProfilePicture(Integer entityId, Integer instituteId, Integer branchId, FileAttachment fileAttachment,
					String fileNameWithExtension, PROFILE_FOLDER_TYPE profileType, String updateSql) throws IOException {
		boolean updateProfilePhotoUrlInDb = false;
		String profileUrl = fileNameWithExtension;
		String localStoreFilePath = null;

		if (StringUtils.isEmpty(fileNameWithExtension) || FileStorePathAssembler.NO_PROFILE_IMAGE_RELATIVE_PATH.equals(fileNameWithExtension)) {
			fileNameWithExtension = FileStorePathAssembler.getRandomFileName(fileAttachment.getFileNameWithExtension());
			updateProfilePhotoUrlInDb = true;
			localStoreFilePath = FileStorePathAssembler.getProfileStoragePath(instituteId, branchId, profileType);
			profileUrl = FileStorePathAssembler.getProfileStorageRelativePath(instituteId, branchId, profileType)
							+ FileStorePathAssembler.PATH_SEPARATOR + fileNameWithExtension;
		} else {
			localStoreFilePath = FileStorePathAssembler.getFileFolderBasePath();
		}

		fileDao.saveFile(localStoreFilePath, fileAttachment.getBytes(), fileNameWithExtension);

		if (updateProfilePhotoUrlInDb) {
			updateProfileInDb(entityId, instituteId, branchId, profileType, profileUrl, updateSql);
		}

		return profileUrl;
	}

	private void updateProfileInDb(Integer entityId, Integer instituteId, Integer branchId, PROFILE_FOLDER_TYPE profileFolderType,
					String profileUrl, String updateSql) {
		Map<String, Object> parameters = new HashMap<String, Object>(2);
		parameters.put("photoUrl", profileUrl);
		parameters.put("entityId", entityId);
		getNamedParameterJdbcTemplate().update(updateSql, parameters);
	}

}