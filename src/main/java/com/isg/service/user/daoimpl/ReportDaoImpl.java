package com.isg.service.user.daoimpl;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.ReportDao;
import com.isg.service.user.model.ExpenseChart;
import com.isg.service.user.model.ExpenseDetail;
import com.isg.service.user.model.FeeHeadChart;
import com.isg.service.user.model.FeeHeadStats;
import com.isg.service.user.model.StudentResult;
import com.isg.service.user.model.Subject;
import com.isg.service.user.model.pdf.PDFReportGenerator;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.StudentRowMapper;
import com.isg.service.user.util.DateUtil;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class ReportDaoImpl extends NamedParameterJdbcDaoSupport implements ReportDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private static final Logger log = Logger.getLogger(ReportDaoImpl.class);

	private final String RETRIEVE_EXPENSE_QUERY = "SELECT YEAR(date) as year, MONTH(date) as month, WEEK(date)+1 as week, "
					+
					" sum(amount) as sum, for_entity_user_id as forUserId, for_entity_name as forUser"
					+
					" FROM passbook "
					+
					" WHERE user_id= :userId ";

	private final String RETRIEVE_EXPENSE_QUERY_WHERE = " and is_debit = 'Y' and date between :startDate and :endDate "
					+
					" GROUP BY WEEK(date),for_entity_user_id order by for_entity_user_id asc";

	@Override
	public ExpenseChart getExpenseChart(Integer userId,
					int month, int year, int forEntityUserId) throws ParseException {
		// TODO Auto-generated method stub
		SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd");

		String startDate = DateUtil.getStartDate(month, year);
		String endDate = DateUtil.getEndDate(month, year);
		Integer[] weeks = DateUtil.getWeeksOfMonth(month, year);
		log.debug("userId:: " + userId + " startDate:: " + startDate + " endDate:: " + endDate);

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("userId", userId);
		parameters.put("startDate", displayFormat.parse(startDate));
		parameters.put("endDate", displayFormat.parse(endDate));

		String query = RETRIEVE_EXPENSE_QUERY;

		if (forEntityUserId > 0) {
			query = query + " and for_entity_user_id = :forEntityUserId ";
			parameters.put("forEntityUserId", forEntityUserId);
		}
		query = query + RETRIEVE_EXPENSE_QUERY_WHERE;
		log.debug("query:: " + query);

		List<ExpenseDetail> expenseDetailList = getNamedParameterJdbcTemplate().query(query, parameters, new RowMapper<ExpenseDetail>() {
			@Override
			public ExpenseDetail mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				ExpenseDetail detail = new ExpenseDetail();
				detail.setYear(resultSet.getInt("year"));
				detail.setMonth(resultSet.getInt("month"));
				detail.setWeek(resultSet.getInt("week"));
				detail.setSum(resultSet.getInt("sum"));
				detail.setForUserId(resultSet.getInt("forUserId"));
				detail.setForUser(resultSet.getString("forUser"));
				log.debug("ExpenseDetail :: " + detail.toString());
				return detail;
			}
		});
		log.debug("expenseDetailList size :: " + expenseDetailList.size());
		return createExpenseChart(expenseDetailList, weeks);
	}

	private ExpenseChart createExpenseChart(List<ExpenseDetail> expenseDetailList, Integer[] weeks) {

		ExpenseChart chart = new ExpenseChart();

		ArrayList<String> userList = new ArrayList();
		ArrayList<Integer> userIdList = new ArrayList();
		ArrayList<String> weekList = new ArrayList();

		// set User names
		for (ExpenseDetail detail : expenseDetailList) {
			String forUser = detail.getForUser();
			int forUserId = detail.getForUserId();
			if (!userIdList.contains(forUserId)) {
				userIdList.add(forUserId);
				userList.add(forUser);
			}
		}
		String[] userArr = new String[userList.size()];
		userArr = userList.toArray(userArr);

		// set week names
		for (int j = 0; j < weeks.length; j++) {
			weekList.add("w" + (j + 1));
		}
		String[] weekArr = new String[weekList.size()];
		weekArr = weekList.toArray(weekArr);
		chart.setWeeks(weekArr);

		log.debug("userList :: " + userList);
		log.debug("userIdList :: " + userIdList);

		// set amount per week per user
		int[][] userAmount = new int[userArr.length][weeks.length];

		for (int i = 0; i < userIdList.size(); i++) {

			int userId = userIdList.get(i);
			for (int j = 0; j < weeks.length; j++) {

				int currentWeek = weeks[j];
				for (ExpenseDetail detail : expenseDetailList) {

					int week = detail.getWeek();
					int forUserId = detail.getForUserId();

					if (week == currentWeek && userId == forUserId) {
						userAmount[i][j] = detail.getSum();
					}
				}
			}
		}
		chart.setAmount(userAmount);
		chart.setUsers(userArr);
		log.debug("chart details:: " + chart.toString());
		return chart;
	}

	@Override
	public List<StudentResult> getStudentResults(Integer studentId) {

		final String sql = "select sr.id,CONCAT(s.first_name , ' ' ,s.father_name,' ',s.last_name ) as studentName,"
						+ " sr.result, sr.final_grade, sr.position, sr.general_remark, sr.gross_total from student as s,"
						+ " student_result as sr where sr.student_id = s.id "
						+ " and s.id = :studentId";

		/*final String schoolSql = "select b.id as branchId, b.name as branchName, i.id as instituteId, "
						+ " i.name as instituteName, std.id as standardId, std.displayName as standard, "
						+ " sum(sub.total_marks) as totalMarks, sub.semester "
						+ " from branch as b, institute as i, standard as std,subject as sub "
						+ " where b.id = sub.branch_id AND i.id = b.institute_id AND std.id = sub.standard_id"
						+ " AND sub.id in (select subject_id from student_marks where student_result_id = :student_result_id )";*/

		final String schoolSql = "select b.id as branchId, b.name as branchName, i.id as instituteId, "
						+ " i.name as instituteName, std.id as standardId, std.displayName as standard, "
						+ " sum(sub.theory + sub.practical) as totalMarks "
						+ " from branch as b, institute as i, standard as std, standard_subject as ss , subject as sub "
						+ " where b.id = sub.branch_id AND ss.standard_id = std.id AND ss.subject_id =  sub.id AND i.id = b.institute_id "
						+ " AND sub.id in (select subject_id from student_marks where student_result_id = :student_result_id )";

		/*
		 * select b.id as branchId, b.name as branchName, i.id as instituteId,
		i.name as instituteName, std.id as standardId, std.displayName as standard, 
		sum(sub.theory + sub.practical) as totalMarks  from branch as b, institute as
		i, standard as std,  standard_subject as ss , subject as sub
		where b.id = sub.branch_id 
		AND ss.standard_id = std.id
		AND ss.subject_id =  sub.id
		AND i.id = b.institute_id 
		AND sub.id in 
		(select subject_id from student_marks where student_result_id = 1)*/

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("studentId", studentId);

		List<StudentResult> studentResultList = getNamedParameterJdbcTemplate().query(sql, parameters, new RowMapper<StudentResult>() {
			@Override
			public StudentResult mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				final StudentResult result = new StudentResult();
				result.setId(resultSet.getInt("id"));
				result.setStudentName(resultSet.getString("studentName"));
				result.setResult(resultSet.getString("result"));
				result.setFinalGrade(resultSet.getString("final_grade"));
				result.setPosition(resultSet.getString("position"));
				result.setGeneralRemark(resultSet.getString("general_remark"));
				result.setGrossTotal(resultSet.getString("gross_total"));

				Map<String, Object> parametersSchool = new HashMap<String, Object>(1);
				parametersSchool.put("student_result_id", result.getId());

				getNamedParameterJdbcTemplate().queryForObject(schoolSql, parametersSchool, new RowMapper<StudentResult>() {
					@Override
					public StudentResult mapRow(ResultSet resultSet, int rowNum)
									throws SQLException {
						result.setBranchName(resultSet.getString("branchName"));
						result.setBranchId(resultSet.getInt("branchId"));
						result.setInstituteName(resultSet.getString("instituteName"));
						result.setInstituteId(resultSet.getInt("instituteId"));
						result.setStandard(resultSet.getString("standard"));
						result.setTotalMarks(resultSet.getString("totalMarks"));
						//result.setSemester(resultSet.getString("semester"));
						return result;
					}
				});
				return result;
			}
		});
		return studentResultList;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<ExpenseDetail> expenseDetailList = new ArrayList();
		ExpenseDetail detail1 = new ExpenseDetail();
		detail1.setYear(2016);
		detail1.setMonth(1);
		detail1.setWeek(4);
		detail1.setSum(2570);
		detail1.setForUser("Vivan");
		detail1.setForUserId(6);
		expenseDetailList.add(detail1);
		ExpenseDetail detail2 = new ExpenseDetail();
		detail2.setYear(2016);
		detail2.setMonth(1);
		detail2.setWeek(4);
		detail2.setSum(200);
		detail2.setForUser("Self");
		detail2.setForUserId(9);
		expenseDetailList.add(detail2);

		Integer[] weeks = DateUtil.getWeeksOfMonth(0, 2016);
		ReportDaoImpl impl = new ReportDaoImpl();
		impl.createExpenseChart(expenseDetailList, weeks);

	}

	@Override
	public HashMap getReportById(final Integer resultId) throws Exception {

		HashMap reportDetails = null;

		final String sql = "select sr.id,CONCAT(s.first_name , ' ' ,s.last_name ) as studentName,"
						+ " s.roll_number, sr.result, sr.final_grade, sr.position, sr.general_remark, sr.created_date, " +
						"sr.gross_total, sr.result_type_id, sr.title as title from student as s, student_result as sr where sr.student_id = s.id " +
						"and s.id = sr.student_id AND sr.id = :resultId ";

		log.debug("Query :: " + sql);

		/*final String schoolSql = "select b.id as branchId, b.name as branchName, i.id as instituteId, i.logo_url,"
						+
						"i.name as instituteName, std.id as standardId, std.displayName as standard,"
						+ " sum(sub.total_marks) as totalMarks, sub.semester, year(ay.from_date) as startyear, year(ay.to_date) as endyear "
						+ " from branch as b, institute as i, standard as std,"
						+ " subject as sub, academic_year ay where b.id = sub.branch_id AND i.id = b.institute_id AND std.id = sub.standard_id"
						+ " AND std.academic_year_id = ay.id AND sub.id in (select subject_id from student_marks where student_result_id = :student_result_id )";*/

		final String subjectSQl = "SELECT sm.*, s.name as subjectName, s.category_id " +
						" FROM `student_marks` as sm , subject as s where s.id = sm.subject_id " +
						" and sm.student_result_id = :resultId group by s.id";

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("resultId", resultId);

		List<StudentResult> studentResultList = getNamedParameterJdbcTemplate().query(sql, parameters, new RowMapper<StudentResult>() {
			@Override
			public StudentResult mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				final StudentResult result = new StudentResult();
				result.setId(resultSet.getInt("id"));
				result.setStudentName(resultSet.getString("studentName"));
				result.setResult(resultSet.getString("result"));
				result.setTitle(resultSet.getString("title"));
				result.setFinalGrade(resultSet.getString("final_grade"));
				result.setPosition(resultSet.getString("position"));
				result.setGeneralRemark(resultSet.getString("general_remark"));
				result.setGrossTotal(resultSet.getString("gross_total"));
				result.setRoleNumber(resultSet.getString("roll_number"));

				Map<String, Object> parametersSchool = new HashMap<String, Object>(1);
				parametersSchool.put("student_result_id", result.getId());

				final String schoolSql = " select b.id as branchId, b.name as branchName, i.id as instituteId, i.logo_url, "
								+ " i.name as instituteName, std.id as standardId, std.displayName as standard,d.displayName as division, "
								+ " year(ay.from_date) as startyear, year(ay.to_date) as endyear "
								+ " from branch as b, institute as i, standard as std,division d, "
								+ " academic_year ay, student_result sr, student s "
								+ " where b.id = s.branch_id AND i.id = b.institute_id "
								+ " AND sr.student_id = s.id AND s.standard_id = std.id "
								+ " AND std.academic_year_id = ay.id "
								+ " AND d.id = s.division_id "
								+ " AND sr.id = :student_result_id";
				log.debug("Query for school :: " + schoolSql);

				getNamedParameterJdbcTemplate().queryForObject(schoolSql, parametersSchool, new RowMapper<StudentResult>() {
					@Override
					public StudentResult mapRow(ResultSet resultSet, int rowNum)
									throws SQLException {
						result.setBranchName(resultSet.getString("branchName"));
						result.setBranchId(resultSet.getInt("branchId"));
						result.setInstituteName(resultSet.getString("instituteName"));
						result.setInstituteId(resultSet.getInt("instituteId"));
						result.setStandard(resultSet.getString("standard"));
						result.setDivision(resultSet.getString("division"));
						//result.setTotalMarks(resultSet.getString("totalMarks"));
						//result.setSemester(resultSet.getString("semester"));
						result.setYear(resultSet.getString("startyear") + "-" + resultSet.getString("endyear"));
						return result;
					}
				});

				Map<String, Object> parametersSubject = new HashMap<String, Object>(1);
				parametersSubject.put("resultId", resultId);

				List<Subject> subjectList = getNamedParameterJdbcTemplate().query(subjectSQl, parametersSubject, new RowMapper<Subject>() {
					@Override
					public Subject mapRow(ResultSet resultSet, int rowNum)
									throws SQLException {
						final Subject subject = new Subject();
						subject.setId(resultSet.getInt("subject_id"));
						subject.setName(resultSet.getString("subjectName"));
						subject.setTheory(resultSet.getString("theory"));
						subject.setPractical(resultSet.getString("practical"));
						subject.setObtainedMarks(resultSet.getString("obtained"));
						//subject.setTotalMarks(resultSet.getString("total_marks"));
						subject.setCategory(resultSet.getString("category_id"));
						subject.setRemarks(resultSet.getString("remarks"));
						return subject;
					}
				});
				result.setSubjectList(subjectList);
				return result;
			}
		});

		if (studentResultList != null && studentResultList.size() > 0) {
			ByteArrayOutputStream output = null;
			StudentResult result = studentResultList.get(0);
			String resultFileName = "Result";
			if (result.getStandard() != null) {
				resultFileName = resultFileName + "-" + result.getStandard();
				if (result.getSemester() != null) {
					resultFileName = resultFileName + "-" + result.getSemester();
				}
			}
			log.debug("Result ::" + result.toString());
			output = new PDFReportGenerator().generatePDFReport(result);
			reportDetails = new HashMap<>();
			reportDetails.put(resultFileName, output);
		}
		// return output;
		return reportDetails;
	}

	@Override
	public List<Student> getStudentsDueForFeePayment(Integer feeHeadId,
					Integer standardId) throws Exception {

		List<Student> studentList = new ArrayList<>();
		String query = "SELECT student.*, branch.name as branch_name, standard.displayName as standard_name, division.displayName as division_name " +
						// fees.*,
						// payments.fees_id
						" FROM fees LEFT OUTER JOIN payments ON fees.id = payments.fees_id " +
						" LEFT JOIN student ON (    student.branch_id   = fees.branch_id " +
						" AND student.standard_id = fees.standard_id) " +
						" left join branch on student.branch_id = branch.id " +
						" left join standard on student.standard_id= standard.id " +
						" left join division on student.division_id = division.id " +
						// " AND student.caste_id    = fees.caste_id) "+
						" WHERE payments.fees_id IS NULL " +
						// TODO hardcorded. Need to be replaced by academic
						// start and end date
						" AND   fees.fromdate BETWEEN '2016-01-01' AND '2016-12-31' " +
						" AND   fees.head_id   =  :feeHeadId ";
		if (standardId != null && standardId > 0) {
			query = query + " AND   fees.standard_id = :standardId ";
		}
		query = query + " ORDER BY student.first_name,  student.last_name";

		Map<String, Object> parameters = null;
		// Object[] inputs = null;
		if (standardId != null && standardId > 0) {
			parameters = new HashMap<String, Object>(2);
			parameters.put("feeHeadId", feeHeadId);
			parameters.put("standardId", standardId);
			// inputs = new Object[] { feeHeadId, standardId };
		} else {
			// inputs = new Object[] { feeHeadId };
			parameters = new HashMap<String, Object>(1);
			parameters.put("feeHeadId", feeHeadId);
		}
		log.debug("input array:: " + parameters.toString());
		studentList = getNamedParameterJdbcTemplate().query(query, parameters, new StudentRowMapper(false));
		return studentList;
	}

	@Override
	public FeeHeadChart getFeeHeadStats(Integer feeHeadId,
					Integer standardId) throws Exception {

		FeeHeadChart chart = new FeeHeadChart();

		String query = " SELECT fees.head_id head_id, " +
						" fees.description head_description, " +
						" COUNT(1) total_count, " +
						" SUM(IF(payments.fees_id > 0, 1, 0)) total_paid," +
						" SUM(IFNULL(payments.fees_id, 1))    total_due " +
						" FROM fees LEFT OUTER JOIN payments ON fees.id = payments.fees_id " +
						" LEFT JOIN student ON (    student.branch_id   = fees.branch_id " +
						" AND student.standard_id = fees.standard_id) " +
						// " AND student.caste_id    = fees.caste_id) "+
						" WHERE payments.fees_id IS NULL " +
						// TODO hardcorded. Need to be replaced by academic
						// start and end year date
						" AND fees.fromdate BETWEEN '2016-01-01' AND '2016-12-31' ";

		if (feeHeadId != null && feeHeadId > 0) {
			query = query + " AND   fees.head_id   =  :feeHeadId ";
		}
		if (standardId != null && standardId > 0) {
			query = query + " AND   fees.standard_id = :standardId ";
		}
		query = query + " GROUP BY fees.head_id, fees.description";

		log.debug("Query:: " + query);
		Map<String, Object> parameters = null;
		// Object[] inputs = null;
		if (standardId != null && standardId > 0) {
			parameters = new HashMap<String, Object>(2);
			parameters.put("feeHeadId", feeHeadId);
			parameters.put("standardId", standardId);
			// inputs = new Object[] { feeHeadId, standardId };
		} else if (feeHeadId != null && feeHeadId > 0) {
			// inputs = new Object[] { feeHeadId };
			parameters = new HashMap<String, Object>(1);
			parameters.put("feeHeadId", feeHeadId);
		}

		List<FeeHeadStats> feeHeadStatsList = getNamedParameterJdbcTemplate().query(query, parameters, new RowMapper<FeeHeadStats>() {
			@Override
			public FeeHeadStats mapRow(ResultSet resultSet, int rowNum)
							throws SQLException {
				final FeeHeadStats result = new FeeHeadStats();
				result.setHeadId(resultSet.getInt("head_id"));
				result.setHeadDescription(resultSet.getString("head_description"));
				result.setTotalCount(resultSet.getInt("total_count"));
				result.setTotalPaidCount(resultSet.getInt("total_paid"));
				result.setTotalDueCount(resultSet.getInt("total_due"));
				return result;
			}
		});
		log.debug("input array:: " + feeHeadStatsList.toString());
		if (feeHeadStatsList.size() > 0) {
			String[] headArr = new String[feeHeadStatsList.size()];
			for (int j = 0; j < headArr.length; j++) {
				headArr[j] = feeHeadStatsList.get(j).getHeadDescription();
			}

			// set amount per week per user
			int[][] data = new int[chart.getStatDescArr().length][headArr.length];

			for (int j = 0; j < feeHeadStatsList.size(); j++) {
				FeeHeadStats stats = feeHeadStatsList.get(j);
				data[0][j] = stats.getTotalCount();
				data[1][j] = stats.getTotalPaidCount();
				data[2][j] = stats.getTotalDueCount();
			}
			chart.setData(data);
			chart.setHeads(headArr);
		}
		log.debug("chart details:: " + chart.toString());
		return chart;

	}
}
