package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.client.RestClient;
import com.isg.service.user.client.social.FacebookUrlConfiguration;
import com.isg.service.user.client.social.GoogleUrlConfiguration;
import com.isg.service.user.client.social.response.FacebookUserInfoResponse;
import com.isg.service.user.client.social.response.GoogleUserInfoResponse;
import com.isg.service.user.client.social.response.UserInfoResponse;
import com.isg.service.user.dao.SocialProfileCommunicationDao;
import com.isg.service.user.util.Constant;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class SocialProfileCommunicationDaoImpl extends NamedParameterJdbcDaoSupport implements SocialProfileCommunicationDao {
	private static final Logger log = Logger.getLogger(SocialProfileCommunicationDaoImpl.class);

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public UserInfoResponse getAccountInfo(String socialProfileProvider, String accessToken) throws JsonProcessingException {
		UserInfoResponse userInfoResponse = null;
		switch (socialProfileProvider) {
		case "FACEBOOK":
			log.debug("Get facebook social profile details.");
			userInfoResponse = getFacebookAccountInfo(accessToken);
			break;
		case "GOOGLE":
			log.debug("Get google social profile details.");
			userInfoResponse = getGoogleAccountInfo(accessToken);
			break;
		default:
			log.error("Social profile request not supported.");
			break;

		}
		return userInfoResponse;
	}

	private UserInfoResponse getGoogleAccountInfo(String accessToken) throws JsonProcessingException {
		GoogleUserInfoResponse userInfoResponse = null;
		RestClient restClient = new RestClient();
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", accessToken);

		try {
			ResponseEntity<String> response =
							restClient.executeRequest(GoogleUrlConfiguration.TOKEN_INFO, params);

			if (response.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				userInfoResponse = mapper.readValue(
								response.getBody(), GoogleUserInfoResponse.class);

				userInfoResponse.setProvider(Constant.SOCIAL_PROFILE_TYPE.GOOGLE);

				// Revoke the token for user safety.
				restClient.executeRequest(GoogleUrlConfiguration.REVOKE_TOKEN, params);

			} else {
				log.error("Google service return error for token verification" + response.getBody());

			}

		} catch (IOException e) {
			log.error(e);
		}

		return userInfoResponse;

	}

	private UserInfoResponse getFacebookAccountInfo(String accessToken) throws JsonProcessingException {
		FacebookUserInfoResponse userInfoResponse = null;
		RestClient restClient = new RestClient();
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", accessToken);

		try {
			ResponseEntity<String> response =
							restClient.executeRequest(FacebookUrlConfiguration.TOKEN_INFO, params);

			if (response.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				userInfoResponse = mapper.readValue(
								response.getBody(), FacebookUserInfoResponse.class);

				userInfoResponse.setProvider(Constant.SOCIAL_PROFILE_TYPE.FACEBOOK);

				// TODO Revoke the token for user safety.
				// restClient.executeRequest(GoogleUrlConfiguration.REVOKE_TOKEN,
				// params);

			} else {
				log.error("Facebook service return error for token verification" + response.getBody());

			}

		} catch (IOException e) {
			log.error(e);
		}

		return userInfoResponse;

	}

}
