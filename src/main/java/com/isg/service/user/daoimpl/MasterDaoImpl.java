package com.isg.service.user.daoimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.MasterDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.request.Division;
import com.isg.service.user.request.Standard;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class MasterDaoImpl extends NamedParameterJdbcDaoSupport implements MasterDao {

	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public List<Standard> getStandards(Integer branchId) {
		String query = "select id, displayName from standard";

		Map<String, Object> parameters = new HashMap<String, Object>(1);

		if (branchId != null) {
			query += " where branch_id = :branchId";
			parameters.put("branchId", branchId);
		}

		return getNamedParameterJdbcTemplate().query(query, parameters, new BeanPropertyRowMapper<Standard>(Standard.class));
	}

	@Override
	public List<Division> getDivisions(Integer standardId) {
		String query = "select d.id, d.displayName from division d inner join standard_division sd on sd.division_id = d.id ";

		Map<String, Object> parameters = new HashMap<String, Object>(1);

		if (standardId != null) {
			query += " where sd.standard_id = :standardId";
			parameters.put("standardId", standardId);
		}

		return getNamedParameterJdbcTemplate().query(query, parameters, new BeanPropertyRowMapper<Division>(Division.class));
	}

	@Override
	public Integer getRoleId(String role) {
		String query = "select id from role where name= :role";

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("role", role);
		return getNamedParameterJdbcTemplate().queryForObject(query, parameters, Integer.class);
	}

	@Override
	public void saveAppVersion(String appCode, String versionInfo) {
		String query = " INSERT INTO app_version (code, version_information) " +
						" VALUES(:code, :version_information) ON DUPLICATE KEY UPDATE " +
						" code = VALUES(code), version_information = VALUES(version_information)";

		Map<String, Object> parameters = new HashMap<String, Object>(3);
		parameters.put("code", appCode);
		parameters.put("version_information", versionInfo);
		getNamedParameterJdbcTemplate().update(query, parameters);

	}

	@Override
	public Map<String, String> getAppVersion() {
		String query = "select * from app_version";
		final Map<String, String> dataMap = new HashMap<>();
		getJdbcTemplate().query(query, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int arg) throws SQLException {

				dataMap.put(rs.getString("name"), rs.getString("version"));
				return null;
			}
		});
		return dataMap;
	}

	@Override
	public Integer generateSerialNumber(Integer academicYearId, Integer branchId, String identifier) {
		try {
			final SimpleJdbcCall funcCall = new SimpleJdbcCall(getJdbcTemplate())
							.withFunctionName("getSerialConfig");

			MapSqlParameterSource in = new MapSqlParameterSource();
			in.addValue("academicYearId", academicYearId);
			in.addValue("branchId", branchId);
			in.addValue("identifierString", identifier);
			System.out.println("calculate form number>>>>>>>>>>>>>>>>>>>>>>>>> 5001+");
			Integer value = funcCall.executeFunction(Integer.class, in);
			return value;
		} catch (Exception e) {
			//log.error("Error in form number generation", e);
			throw new ApplicationException("Error in form number generation", e);
		}

	}

}