package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.security.PrivateKey;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.crypto.SecretKey;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.ROLES;
import com.isg.service.user.client.RestClient;
import com.isg.service.user.client.notification.Notification;
import com.isg.service.user.client.notification.NotificationClient;
import com.isg.service.user.client.notification.Sms;
import com.isg.service.user.client.payment.PaymentUrlConfiguration;
import com.isg.service.user.client.payment.request.AcknowledgePaymentProcessedRequest;
import com.isg.service.user.client.payment.request.LoginRequest;
import com.isg.service.user.client.payment.request.RefundDetail;
import com.isg.service.user.client.payment.request.RefundPaymentRequest;
import com.isg.service.user.client.payment.request.RefundStatus;
import com.isg.service.user.client.payment.request.SavePaymentDetailRequest;
import com.isg.service.user.client.payment.response.LoginResponse;
import com.isg.service.user.client.payment.response.SavePaymentDetailResponse;
import com.isg.service.user.dao.FeeDetailsDao;
import com.isg.service.user.dao.OnlineAdmissionDao;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.PaymentDao;
import com.isg.service.user.dao.ShoppingDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.factory.validator.onlineadmission.Validator;
import com.isg.service.user.factory.validator.onlineadmission.ValidatorFactory;
import com.isg.service.user.model.Contact;
import com.isg.service.user.model.FeeToBePaidDetail;
import com.isg.service.user.model.FeesPayload;
import com.isg.service.user.model.OnlineAdmissionFormDetail;
import com.isg.service.user.model.Payment;
import com.isg.service.user.model.PaymentAudit;
import com.isg.service.user.model.PaymentDetailResponse;
import com.isg.service.user.model.PaymentMoreInfo;
import com.isg.service.user.model.PaymentSetting;
import com.isg.service.user.model.RefundConfig;
import com.isg.service.user.model.ShoppingPayment;
import com.isg.service.user.model.ShoppingProfile;
import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.model.onlineadmission.State;
import com.isg.service.user.model.shopping.OrderPaymentDetails;
import com.isg.service.user.model.shopping.TaxItem;
import com.isg.service.user.request.BranchConfiguration;
import com.isg.service.user.request.FeeDetails;
import com.isg.service.user.request.OnlineAdmissionFee;
import com.isg.service.user.request.Parent;
import com.isg.service.user.request.PaymentBranchScheme;
import com.isg.service.user.request.PaymentTransaction;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.StudentAddress;
import com.isg.service.user.response.OnlineAdmissionConfiguration;
import com.isg.service.user.util.Constant.EmailTemplate;
import com.isg.service.user.util.Constant.PASSBOOK_TRANSACTION_TYPE;
import com.isg.service.user.util.Constant.PaymentStatus;
import com.isg.service.user.util.Constant.SmsTemplate;
import com.isg.service.user.util.EncryptionDecryptionUtil;
import com.isg.service.user.util.JsonUtil;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class PaymentDaoImpl extends NamedParameterJdbcDaoSupport implements PaymentDao {
	private static final Logger log = Logger.getLogger(PaymentDaoImpl.class);

	private static int PAYMENT_TYPE_FEES = 1;
	private static int PAYMENT_TYPE_SHOPPING = 2;

	private static String CHANNEL_SHOPPING = "SHOPPING-PORTAL";

	@Autowired
	private BoneCPDataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Autowired
	FeeDetailsDao feeDetailsDao;
	
	@Autowired
	ParentDao parentDao;

	@Autowired
	UserDao userDao;

	@Autowired
	ShoppingDao shoppingDao;

	@Autowired
	StudentDao studentDao;

	@Autowired
	OnlineAdmissionDao onlineAdmissionDao;
	
	@Autowired
	private PaymentUrlConfiguration paymentUrlConfiguration;

	@Autowired 
	ValidatorFactory validatorFactory;

	@Autowired
	private NotificationClient notificationClient;
	
	private static String USER_NAME = "user-service";
	private static String PASSWORD = "gThZAbHvCtyla7jl7nhk";

	private static final String PAYMENT_TYPE_T1 = "T1";
	private static final String PAYMENT_TYPE_T2 = "T2";

	@Override
	public PaymentDetailResponse getPayment(String accessCode) {
		return getPaymentDetail(accessCode);
	}

	@Override
	@Transactional
	public String initiateFeesPayment(List<PaymentTransaction> feeDues, Integer entityId, Integer userId, String role, String channel)
					throws JsonProcessingException {

		Set<String> feesToBePaid = new HashSet<String>();
		Map<Integer, FeeToBePaidDetail> feesIdsSelectedForPayment = new HashMap<Integer, FeeToBePaidDetail>();
		Set<Integer> feeIds = new HashSet<>();

		//Remove duplicate fees if any based on feeId and student combination
		String hashedKey = null;
		for (PaymentTransaction feeDuesObj : feeDues) {
			hashedKey = "" + feeDuesObj.getFeeScheduleId() + "-" + feeDuesObj.getStudentId() + "-" + feeDuesObj.getAmount() + "-" + feeDuesObj.getFeesId();
			feeIds.add(feeDuesObj.getFeesId());

			if (feeDuesObj.getFeesId() != 0 && !feesToBePaid.contains(hashedKey)) {
				feesToBePaid.add(hashedKey);
			} else {
				throw new ApplicationException("Duplicate fee id specified for student in request.");
			}
		}

		if (feesToBePaid.size() == 0) {
			throw new ApplicationException("Required parameters are missing, can not process initiateFeesPayment request.");
		}

		List<PaymentTransaction> feesDuesInDb = null;
		Set<Integer> branchIdList = new HashSet<Integer>();
		Integer branchId = 0;
		float feesAmount = 0;

		List<String> feeDescriptionList = new ArrayList<String>();
		Integer studentId = null;
		Long feeScheduleId = null;
		Integer feeId = null;
		float amountToBePaid = 0f;
		FeeToBePaidDetail feeToBePaidDetail = null;
		String paymentForName = "";
		// Validate if valid fees id and amount submitted.
		for (String feeIDStudentIdStr : feesToBePaid) {
			String[] obj = feeIDStudentIdStr.split("-");
			feeScheduleId = Long.parseLong(obj[0]);
			studentId = Integer.parseInt(obj[1]);
			amountToBePaid = Float.parseFloat(obj[2]);
			feeId = Integer.parseInt(obj[3]);

			if (feeScheduleId.intValue() == 0 || studentId.intValue() == 0) {
				throw new ApplicationException("Required parameters are missing, can not process initiateFeesPayment request.");
			}

			feesDuesInDb = feeDetailsDao.getFeeDues(entityId, role, studentId, feeScheduleId);

			log.debug("feesDuesInDb >>>>>>>>" + feesDuesInDb);
			if (CollectionUtils.isEmpty(feesDuesInDb) || (feesDuesInDb.get(0).isSplitPayment() && feesToBePaid.size() > 1)
					|| (!"Y".equals(feesDuesInDb.get(0).getAllowUserEnteredAmount()) && !isValidFeesAmountSpecified(feesDuesInDb.get(0), feeDues))) {

				throw new ApplicationException("Invalid fee ids are specified (check amount / fee id's specified in the request, in case of split payment clubbing of fees is not allowed).");
			}

			FeeDetails feeDetails = feeDetailsDao.getFeeDetailsByScheduleId(feeScheduleId);

			if (feeDetails == null) {
				throw new ApplicationException("Fees data not found in database.");
			}

			paymentForName += (!StringUtils.isEmpty(paymentForName) ? ", " : "") + feesDuesInDb.get(0).getStudentName();

			// Set amount with late payment charges if any
			feeDetails.setAmount(feesDuesInDb.get(0).getAmount());
			if("Y".equals(feesDuesInDb.get(0).getAllowUserEnteredAmount())){
				feeDetails.setAmount(amountToBePaid + "");
			}

			branchIdList.add(feeDetails.getBranch_id());
			branchId = feeDetails.getBranch_id();
			feesAmount = setFeesDuesAmount(feeDetails, feeDues, feesAmount, feeDescriptionList, studentId);

			feeToBePaidDetail = new FeeToBePaidDetail();
			feeToBePaidDetail.setAmount(amountToBePaid);
			feeToBePaidDetail.setSplitPayment(feesDuesInDb.get(0).isSplitPayment());
			feeToBePaidDetail.setLatePaymentCharges(feesDuesInDb.get(0).getLatePaymentCharges());
			feeToBePaidDetail.setDiscountAmount(feesDuesInDb.get(0).getDiscountAmount());
			feesIdsSelectedForPayment.put(feeId, feeToBePaidDetail);
		}

		List<PaymentSetting> paymentDetailList = getPaymentSetting(branchIdList, branchId);

		float internetHandlingAmount = 0f;
		float internetHandlingPercentage = 0f;
		Integer paymentGatewayId = null;
		boolean offlinePaymentsEnabled = false;
		// Check whether only one Payment Gateway Configuration is being used.
		String sql = "select scheme_code_id from fees_split_configuration where fees_id in ("+StringUtils.join(feeIds, ",")+")";
		List<Integer> schemeCodeIds = getJdbcTemplate().queryForList(sql, Integer.class);

		List<Integer> gatewayIds = checkPgConfigsBasedOnSchemeCodes(schemeCodeIds);
		for (PaymentSetting ps : paymentDetailList) {
			internetHandlingAmount = ps.getInternetHandlingChargesAmount();
			internetHandlingPercentage = ps.getInternetHandlingChargesPercentage();
			paymentGatewayId = ps.getPaymentGatewayId();
			offlinePaymentsEnabled = ps.getOfflinePaymentsEnabled();
			if(gatewayIds .contains(paymentGatewayId)){
				break;
			}
		}

		Map<String, Object> currencyDetails = getCurrencyDetailsBasedOnFeeIds (feeIds);

		log.debug("internetHandlingAmount : " + internetHandlingAmount);
		log.debug("internetHandlingPercentage : " + internetHandlingPercentage);
		log.debug("paymentGatewayId : " + paymentGatewayId);
		log.debug("offlinePaymentsEnabled : " + offlinePaymentsEnabled);

		float internetHandlingFeesOnTransactionAmount = (feesAmount * internetHandlingPercentage) / 100;

		BigDecimal db = new BigDecimal(internetHandlingFeesOnTransactionAmount).setScale(2, BigDecimal.ROUND_HALF_UP);
		float covertedInternetHandlingFeesOnTransactionAmount = db.floatValue();
		log.debug("covertedInternetHandlingFeesOnTransactionAmount : " + covertedInternetHandlingFeesOnTransactionAmount);
		float internetHandlingCharges = (covertedInternetHandlingFeesOnTransactionAmount > internetHandlingAmount) ? covertedInternetHandlingFeesOnTransactionAmount
						: internetHandlingAmount;

		//Set split payment configurationDetails
		Map<Integer, String> feesSplitConfigurationDetails = feeDetailsDao.retrieveFeesSplitConfigurationDetails(feesIdsSelectedForPayment);
		// feeDues.setFeesDesription(feeDetails.getDescription());

		if (paymentForName != null && paymentForName.length() > 1000) {
			paymentForName = paymentForName.substring(0, 1000);
		}
		System.out.println("paymentForName*****************>>>>>>>>" + paymentForName);
		return processPayment(feesAmount, internetHandlingCharges, 0, StringUtils.join(feeDescriptionList, ","), entityId, userId,
						PAYMENT_TYPE_FEES,
						JsonUtil.convertObjectToJson(feeDues), paymentGatewayId, offlinePaymentsEnabled, role, channel, PAYMENT_TYPE_T1,
						feesSplitConfigurationDetails, paymentForName, currencyDetails);

	}

	private Map<String, Object> getCurrencyDetailsBasedOnFeeIds(Set<Integer> feeIds) {
		String sql = "select icon_css_class, currency.code from currency "
				+ " inner join fees as f on f.currency = currency.code "
				+ " where f.id in ("+StringUtils.join(feeIds, ",")+") group by currency.code ";
		final Map<String, Object> currencyIconClass = new HashMap<>();
		 getJdbcTemplate().query(sql, new RowMapper<Object> (){
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				currencyIconClass.put("icon_css_class", rs.getString("icon_css_class"));
				currencyIconClass.put("code", rs.getString("code"));
				return null;
			}
		});
		return currencyIconClass;
	}

	private List<Integer> checkPgConfigsBasedOnSchemeCodes(List<Integer> schemeCodeIds) {
		LoginResponse loginResponse = login(USER_NAME, PASSWORD);
		Map<String, String> httpHeader = new HashMap<String, String>();
		httpHeader.put("token", loginResponse.getToken());

		RestClient restClient = new RestClient();

		try {
			Object[] urlParemeter = new Object[] { StringUtils.join(schemeCodeIds, ",") };

			ResponseEntity<String> getBranchPaymentSchemeResponse = restClient
							.executeRequest(paymentUrlConfiguration.getGatewayConfigurationsBySchemeList(), urlParemeter, httpHeader);

			if (getBranchPaymentSchemeResponse.getStatusCode() == HttpStatus.OK) {
				log.info("branchscheme::::::::::     " + getBranchPaymentSchemeResponse.getBody());
				ObjectMapper mapper = new ObjectMapper();
				List<Integer> pgConfigs = mapper.readValue(getBranchPaymentSchemeResponse.getBody(),
								mapper.getTypeFactory().constructCollectionType(List.class, Integer.class));

				if (pgConfigs == null || pgConfigs.size() > 1)
				{
					log.error("Branch specific fee request failed with status " + getBranchPaymentSchemeResponse.getStatusCode());
					log.error(getBranchPaymentSchemeResponse.getBody());
					throw new ApplicationException("Combination of these payment transactions is not allowed.");
				}
				return pgConfigs;
			}
		} catch (IOException e) {
			log.error(e);
			throw new ApplicationException("Payment acknowledge requets failed.", e);
		}
		return null;
	}

	private List<PaymentSetting> getPaymentSetting(Set<Integer> branchIdList, Integer branchId) {
		// Start payment process
		Map<String, Object> queryParameters = new HashMap<String, Object>();

		String query = "select payment_setting.* from payment_setting where payment_setting.branch_id IS NULL ";
		if (branchIdList.size() == 1) {
			query += " or payment_setting.branch_id = :branchId ";
			queryParameters.put("branchId", branchId);
		}

		query += " order by payment_setting.branch_id desc";

		System.out.println("Get scheme Config :::: > " + query);
		
		log.debug("Payment settings query : " + query);

		List<PaymentSetting> paymentDetailList = getNamedParameterJdbcTemplate().query(query, queryParameters,
						new RowMapper<PaymentSetting>() {
							@Override
							public PaymentSetting mapRow(ResultSet rs, int rowNum) throws SQLException {
								Integer branchId = rs.getInt("branch_id");
								String sql = "select offline_payments_enabled from branch_configuration where branch_id = " + branchId;
								List<String> values = getJdbcTemplate().query(sql, new RowMapper<String>() {
									@Override
									public String mapRow(ResultSet rs, int arg1) throws SQLException {
										return rs.getString("offline_payments_enabled");
									}
								});

								String offlinePaymentsEnabled = "N";
								if (values != null && !values.isEmpty()) {
									offlinePaymentsEnabled = values.get(0);
								}
								System.out.println("branch >>" + branchId + ", Offline payment enabled >>" + offlinePaymentsEnabled);
								return new PaymentSetting(rs.getInt("gateway_id"),
												rs.getFloat("ic_amount"),
												rs.getFloat("ic_percentage"),
												branchId, "Y".equals(offlinePaymentsEnabled));
							}
						});

		log.debug("paymentDetailList.size : " + paymentDetailList.size());
		return paymentDetailList;
	}

	private boolean isValidFeesAmountSpecified(PaymentTransaction feesDuesInDb, List<PaymentTransaction> feeDuesRequest) {

		for (PaymentTransaction feeDuesObj : feeDuesRequest) {
			float amountToBePaid = StringUtils.isEmpty(feeDuesObj.getAmount()) ? 0 : Float.parseFloat(feeDuesObj.getAmount());

			// Amount to be paid cannot be zero
			if (amountToBePaid == 0) {
				return false;
			} else if ((feesDuesInDb.isSplitPayment() || !feesDuesInDb.isPartialPaymentAllowed())
							&& feeDuesObj.getFeesId().intValue() == feesDuesInDb.getFeesId().intValue()
							&& (amountToBePaid != Float.parseFloat(feesDuesInDb.getAmount()))) {
				//For split payment scenario, partial payment is not allowed
				return false;
			}

			/*log.debug("feeDuesObj >>>" + feeDuesObj);
			log.debug("feeDuesObj.getFeesId() == feesDuesInDb.getFeesId()" + (feeDuesObj.getFeesId().intValue() == feesDuesInDb.getFeesId().intValue()));
			log.debug("feeDuesObj.getStudentId() == feesDuesInDb.getStudentId() "
							+ (feeDuesObj.getStudentId().intValue() == feesDuesInDb.getStudentId().intValue()));
			log.debug("amountToBePaid <= Float.parseFloat(feesDuesInDb.getAmount()) " + (amountToBePaid <= Float.parseFloat(feesDuesInDb.getAmount())));*/

			// Amount to be paid cannot be more than pending due amount
			if (feeDuesObj.getFeesId().intValue() == feesDuesInDb.getFeesId().intValue() &&
							feeDuesObj.getStudentId().intValue() == feesDuesInDb.getStudentId().intValue()
							&& amountToBePaid <= Float.parseFloat(feesDuesInDb.getAmount())) {
				log.debug("Valid Fees Amount Specified true");
				return true;
			}

		}

		return false;
	}

	private float setFeesDuesAmount(FeeDetails feeDetails, List<PaymentTransaction> feeDues, float feesAmount, List<String> feeDescriptionList,
					Integer studentId) {

		for (PaymentTransaction FeeDuesObj : feeDues) {
			if (FeeDuesObj.getFeesId().intValue() == feeDetails.getId().intValue() && studentId.intValue() == FeeDuesObj.getStudentId().intValue()) {
				float amountToBePaid = StringUtils.isEmpty(FeeDuesObj.getAmount()) ? 0 : Float.parseFloat(FeeDuesObj.getAmount());
				float feesAmountInDb = StringUtils.isEmpty(feeDetails.getAmount()) ? 0 : Float.parseFloat(feeDetails.getAmount());

				float finalAmountToBePaid = (amountToBePaid == 0 || (feesAmountInDb < amountToBePaid)) ? feesAmountInDb : amountToBePaid;

				feesAmount += finalAmountToBePaid;
				feeDescriptionList.add(feeDetails.getDescription() + " (Amount:" + finalAmountToBePaid + ")");
				FeeDuesObj.setAmount(String.valueOf(finalAmountToBePaid));
				FeeDuesObj.setDescription(feeDetails.getDescription());
			}
		}

		return feesAmount;
	}

	@Override
	public String initiateShoppingPayment(ShoppingPayment shoppingPayment, Integer entityId, Integer userId, String role, String channel)
					throws IOException {

		ShoppingProfile shoppingProfile = shoppingDao.getShoppingProfileDetails(userId);
		OrderPaymentDetails orderPaymentDetails = shoppingDao.getShoppingCartDetails(shoppingProfile, shoppingPayment.getOrderId());
		log.debug(orderPaymentDetails.toString());
		float cartAmount = orderPaymentDetails.getAmount();
		float shippingCharges = orderPaymentDetails.getShippingCharges();
		float internetHandlingCharges = 0;
		TaxItem[] taxItems = orderPaymentDetails.getTax().getTaxItem();

		for (TaxItem taxItem : taxItems) {
			log.debug(taxItem.toString());
			if ("Internet Handling".equalsIgnoreCase(taxItem.getLabel())) {
				internetHandlingCharges = taxItem.getItem();
			}
		}

		String query = "select payment_setting.* from payment_setting where " +
						" payment_setting.branch_id IS NULL order by payment_setting.branch_id desc";
		Map<String, Object> queryParameters = new HashMap<String, Object>();

		PaymentSetting paymentDetail = getNamedParameterJdbcTemplate().queryForObject(query, queryParameters,
						new RowMapper<PaymentSetting>() {
							@Override
							public PaymentSetting mapRow(ResultSet rs, int rowNum) throws SQLException {
								return new PaymentSetting(rs.getInt("gateway_id"),
												rs.getFloat("ic_amount"),
												rs.getFloat("ic_percentage"),
												rs.getInt("branch_id"), false);
							}
						});

		Integer paymentGatewayId = paymentDetail.getPaymentGatewayId();

		return processPayment(cartAmount, internetHandlingCharges, shippingCharges, "SHOPPING PAYMENT", entityId, userId, PAYMENT_TYPE_SHOPPING,
						JsonUtil.convertObjectToJson(shoppingPayment), paymentGatewayId, false, role, channel, PAYMENT_TYPE_T2, null, null, null);
	}

	private String processPayment(float amount, float internetHandlingCharges, float shippingCharges, String description, Integer entityId, Integer userId,
					Integer paymentTypeId,
					String payload, Integer paymentGatewayId, boolean offlinePaymentsEnabled, String role, String channel, String paymentTaxType,
					Map<Integer, String> feesSplitConfigurationDetails, String paymentForName, Map<String, Object> currencyDetails)
					throws JsonProcessingException {

		String qfixRefNumber = RandomStringUtils.random(15, true, true).toUpperCase();
		Long paymentAuditId = savePaymentAuditDetail(amount, internetHandlingCharges, shippingCharges, 0, 0, description, entityId, userId, paymentTypeId,
						payload,
						qfixRefNumber, PaymentStatus.UNPAID.get(), userId);

		String customerName = "";
		String customerEmail = "";
		String customerMobile = "";
		String customerAddress = "";
		String customerState = "";
		String customerCountry = "IND";
		String customerCity = "";
		String customerPincode = "";

		if (ROLES.PARENT.get().equals(role)) {
			Parent parent = parentDao.getParent(entityId);
			customerName = parent.getFirstname() + " " + (parent.getLastname() != null ? parent.getLastname() : "");
			customerEmail = parent.getEmail();
			customerMobile = parent.getPrimaryContact();
			customerAddress = StringUtils.isEmpty(parent.getAddressLine()) ? "" : parent.getAddressLine();
			customerCity = StringUtils.isEmpty(parent.getCity()) ? "" : parent.getCity();
			customerState = "";
			customerPincode = StringUtils.isEmpty(parent.getPincode()) ? "" : parent.getPincode();
		} else if (ROLES.STUDENT.get().equals(role)) {
			Student student = studentDao.getStudentProfileDetailsbyId(entityId);
			customerName = student.getPersonalDetails().getName();
			customerEmail = student.getPersonalDetails().getEmailAddress();
			customerMobile = student.getPersonalDetails().getMobile();
			paymentForName = student.getPersonalDetails().getName();
			StudentAddress address = student.getPersonalDetails().getAddress();

			if (address != null) {
				customerAddress = StringUtils.isEmpty(address.getLine1()) ? "" :
								address.getLine1();
				customerCity = StringUtils.isEmpty(address.getCity()) ? "" : address.getCity();
				customerState = "";
				customerPincode = StringUtils.isEmpty(address.getPincode()) ? "" : address.getPincode();
			} else {
				customerAddress = "";
				customerCity = "";
				customerState = "";
				customerPincode = "";
			}
		}

		SavePaymentDetailResponse savePaymentDetailResponse = savePaymentDetails(amount, internetHandlingCharges, shippingCharges, description, qfixRefNumber,
						paymentGatewayId, offlinePaymentsEnabled,
						channel, customerName, customerEmail, customerMobile, customerAddress, customerCity, customerState, customerCountry, customerPincode,
						paymentTaxType, feesSplitConfigurationDetails, paymentForName, currencyDetails);

		int UPDATE_STATUS = updatePaymentAuditDetail(paymentAuditId, savePaymentDetailResponse.getId());

		if (UPDATE_STATUS != 1) {
			log.error("Error occured while updating payment audit record : " + paymentAuditId);
			throw new ApplicationException("Error occured while updating payment audit record : " + paymentAuditId);
		}

		return savePaymentDetailResponse.getAccessCode();
	}

	private int updatePaymentAuditDetail(Long paymentAuditId, int paymentDetailId) {
		String query = "update payment_audit set payment_detail_id = ? where id=?";
		Object[] inputsParam = new Object[] { paymentDetailId, paymentAuditId };
		int UPDATE_STATUS = getJdbcTemplate().update(query, inputsParam);
		return UPDATE_STATUS;
	}

	private Long savePaymentAuditDetail(float amount, float internetHandlingCharges, float shippingCharges, float totalAmount, float tax, String description,
					Integer entityId,
					Integer userId, Integer paymentTypeId, String payload, String qfixRefNumber, String status, Integer createdBy) {
		SimpleJdbcInsert insertPaymentAudit =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("payment_audit")
										.usingColumns("qfix_reference_number", "amount", "description", "entity_id", "user_id", "internet_handling_charges",
														"shipping_charges", "tax", "total_amount", "payment_type_id", "payload", "status", "created_by")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(7);
		parameters.put("qfix_reference_number", qfixRefNumber);
		parameters.put("amount", amount);
		parameters.put("internet_handling_charges", internetHandlingCharges);
		parameters.put("shipping_charges", shippingCharges);
		parameters.put("tax", tax);
		parameters.put("total_amount", totalAmount);
		parameters.put("description", description);
		parameters.put("entity_id", entityId);
		parameters.put("user_id", userId);
		parameters.put("payment_type_id", paymentTypeId);
		parameters.put("payload", payload);
		parameters.put("status", status);
		parameters.put("created_by", createdBy);

		Long paymentAuditId = (Long) insertPaymentAudit.executeAndReturnKey(parameters);
		return paymentAuditId;
	}

	private SavePaymentDetailResponse savePaymentDetails(float amount, float internetHandlingCharges, float shippingCharges, String description,
					String qfixRefNumber, Integer paymentGatewayId, boolean offlinePaymentsEnabled, String channel, String customerName,
					String customerEmail, String customerMobile, String customerAddress, String customerCity, String customerState,
					String customerCountry, String customerPincode, String paymentTaxType, Map<Integer, String> splitPaymentDetails, 
					String paymentForName, Map<String, Object> currencyDetails) {

		LoginResponse loginResponse = login(USER_NAME, PASSWORD);
		Map<String, String> httpHeader = new HashMap<String, String>();
		httpHeader.put("token", loginResponse.getToken());

		SavePaymentDetailResponse savePaymentDetailResponse = null;
		// String qfixRefNumber = RandomStringUtils.random(20, false, true);
		RestClient restClient = new RestClient();
		SavePaymentDetailRequest savePaymentDetailRequest = new SavePaymentDetailRequest();
		savePaymentDetailRequest.setAmount(amount);
		savePaymentDetailRequest.setCustomerEmail(customerEmail);
		savePaymentDetailRequest.setCustomerPhone(customerMobile);
		savePaymentDetailRequest.setCustomerName(customerName);
		savePaymentDetailRequest.setInternetHandlingCharges(internetHandlingCharges);
		savePaymentDetailRequest.setShippingCharges(shippingCharges);
		savePaymentDetailRequest.setPaymentGatewayConfigurationId(paymentGatewayId);
		savePaymentDetailRequest.setQfixReferenceNumber(qfixRefNumber);
		savePaymentDetailRequest.setChannel(channel);
		savePaymentDetailRequest.setPaymentTaxType(paymentTaxType);
		savePaymentDetailRequest.setPaymentForDescription(description);
		savePaymentDetailRequest.setCustomerAddress(customerAddress);
		savePaymentDetailRequest.setCustomerCity(customerCity);
		savePaymentDetailRequest.setCustomerState(customerState);
		savePaymentDetailRequest.setCustomerPincode(customerPincode);
		savePaymentDetailRequest.setCustomerCountry(customerCountry);
		savePaymentDetailRequest.setSplitPaymentDetail(splitPaymentDetails);
		savePaymentDetailRequest.setOfflinePaymentsEnabled(offlinePaymentsEnabled);
		
		savePaymentDetailRequest.setPaymentForName(paymentForName);
		if(currencyDetails != null){
			savePaymentDetailRequest.setCurrencyIconClass((String)currencyDetails.get("icon_css_class"));
			savePaymentDetailRequest.setCurrencyCode((String)currencyDetails.get("code"));
		}

		try {
			ResponseEntity<String> serviceResponse = restClient
							.executeRequest(savePaymentDetailRequest,
											paymentUrlConfiguration.savePaymentDetailConfig(), new Object[] {}, httpHeader);

			if (serviceResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				savePaymentDetailResponse = mapper.readValue(
								serviceResponse.getBody(), SavePaymentDetailResponse.class);

			} else {
				log.error("Save payment details service request failed with status " + serviceResponse.getStatusCode());
				log.error(serviceResponse.getBody());
				throw new ApplicationException("Save payment details service request failed with status "
								+ serviceResponse.getBody());

			}

		} catch (IOException e) {
			log.error("", e);
			e.printStackTrace();
			throw new ApplicationException("Login to payment service request failed.", e);
		}

		//System.out.println(">>>>>>>>>" + savePaymentDetailResponse.getAccessCode());
		return savePaymentDetailResponse;

	}

	private LoginResponse login(String username, String password) {

		LoginResponse loginResponse = null;

		RestClient restClient = new RestClient();
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsername(username);
		loginRequest.setPassword(password);

		try {
			ResponseEntity<String> createSessionResponse = restClient
							.executeRequest(loginRequest,
											paymentUrlConfiguration.createSessionConfig());

			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				loginResponse = mapper.readValue(
								createSessionResponse.getBody(), LoginResponse.class);

			} else {
				log.error("Login to payment service request failed with status " + createSessionResponse.getStatusCode());
				log.error(createSessionResponse.getBody());
				throw new ApplicationException("Login to payment service request failed with status."
								+ createSessionResponse.getBody());

			}

		} catch (IOException e) {
			log.error(e);
			throw new ApplicationException("Save payment details service request failed.", e);
		}

		//System.out.println(">>>>>>>>>" + loginResponse.getToken());
		return loginResponse;
	}

	@Override
	public PaymentDetailResponse acknowledgePayment(String accessCode, Integer entityId, String role) throws IOException, AuthorisationException, ParseException {

		PaymentDetailResponse paymentDetailResponse = getPaymentDetail(accessCode);

		String qfixRefNumber = paymentDetailResponse.getQfixReferenceNumber();
		String status = paymentDetailResponse.getPaymentStatus();

		if ("SUCCESS".equalsIgnoreCase(status)) {
			//Check here if fees schedule is unpaid if not return;

			PaymentAudit paymentAudit = getPaymentAudit(qfixRefNumber);
			if ("SUCCESS".equalsIgnoreCase(paymentAudit.getStatus())) {
				log.debug("***************************Already success returning back****,**" + paymentDetailResponse.getQfixReferenceNumber()
								+ "*****************************************");
				return paymentDetailResponse;
			}
			float tax = calculateTaxAmount(paymentDetailResponse.getTotalAmount(), paymentDetailResponse.getAmount(),
							paymentDetailResponse.getInternetHandlingCharges());

			String paymentOptionName = paymentDetailResponse.getPaymentOptionName();
			String paymentMode = paymentDetailResponse.getPaymentMode();
			
			String paymentType = "ONLINE";

			if(paymentAudit.getPaymentMode() != null && !paymentAudit.getPaymentMode().equalsIgnoreCase("O")){
				paymentOptionName = "Not available";
				String mode = paymentAudit.getPaymentMode();
				paymentMode = (mode.equals("C") ? "Cash" : (mode.equals("H") ? "Cheque" : (mode.equals("N") ? "NEFT/RTGS" : "DD")));
				paymentType = "OFFLINE";
			}

			updatePaymentAuditStatus(qfixRefNumber, status, tax, paymentDetailResponse.getTotalAmount(), paymentOptionName, 
					paymentMode, paymentDetailResponse.getTotalPaidAmount());

			// Request coding on behalf of Admin - usually in shopping
			if (Identity.ROLES.ADMIN.get().equals(role)) {
				entityId = paymentAudit.getEntityId();
			}
			
			float feeAmount = paymentDetailResponse.getTotalAmount();
			if (paymentAudit.getPaymentTypeId() == PAYMENT_TYPE_FEES) {
				markFeesAsPaid(paymentAudit.getPayload(), entityId, feeAmount, paymentAudit.getUserId(), paymentAudit.getId(),
								role, qfixRefNumber, paymentDetailResponse.getTotalPaidAmount(), paymentType);
				sendSuccessPaymentNotification(paymentDetailResponse.getTotalAmount(), qfixRefNumber, "Online Fees", false);

				if(paymentDetailResponse.getPaymentChannel().equalsIgnoreCase("PAY-DIRECT")){
					Integer userId = paymentAudit.getUserId();
					
					sendLoginNotification(userId);
					
				}
			} else {
				completeShoppingPaymentProcess(paymentAudit.getPayload(), entityId, paymentDetailResponse.getTotalAmount(), paymentAudit.getUserId(),
								paymentAudit.getId(), paymentAudit.getDescription(), role);
			}

		} else {
			throw new ApplicationException("Can not acknowldege this payment, invalid payment status reported.");
		}

		return paymentDetailResponse;
	}

	private PaymentDetailResponse getPaymentDetail(String accessCode) {
		LoginResponse loginResponse = login(USER_NAME, PASSWORD);
		Map<String, String> httpHeader = new HashMap<String, String>();
		httpHeader.put("token", loginResponse.getToken());

		RestClient restClient = new RestClient();
		PaymentDetailResponse paymentDetailResponse = null;

		try {
			Object[] urlParemeter = new Object[] { accessCode };
			System.out.println("<<<<<<<<<<< Getting PaymentDetail >>>>>>>>>>>>>>>>>");
			ResponseEntity<String> getPaymentDetailResponse = restClient
							.executeRequest(paymentUrlConfiguration.getPaymentDetailConfig(), urlParemeter, httpHeader);

			if (getPaymentDetailResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				paymentDetailResponse = mapper.readValue(
								getPaymentDetailResponse.getBody(), PaymentDetailResponse.class);

			}

			if (paymentDetailResponse == null)
			{
				log.error("Acknowledge payment request failed with status " + getPaymentDetailResponse.getStatusCode());
				log.error(getPaymentDetailResponse.getBody());
				throw new ApplicationException("Get payment details response failed with status."
								+ getPaymentDetailResponse.getBody());

			}
			System.out.println("\n\n>>>>>>" + new ObjectMapper().writeValueAsString(paymentDetailResponse));
		} catch (IOException e) {
			log.error(e);
			throw new ApplicationException("Payment acknowledge requets failed.", e);
		}

		//System.out.println(">>>>>>>>>" + loginResponse.getToken());
		return paymentDetailResponse;
	}

	private void completeShoppingPaymentProcess(String payload, Integer entityId, float amount, Integer userId, Integer paymentAuditId, String description,
					String role)
					throws IOException, ParseException, AuthorisationException {
		ShoppingPayment shoppingPayment = JsonUtil.convertJsonToObject(payload, ShoppingPayment.class);
		ShoppingProfile shoppingProfile = shoppingDao.getShoppingProfileDetails(userId);
		boolean status = shoppingDao.acknowledge(shoppingProfile, entityId, userId, shoppingPayment.getOrderId());

		if (status) {
			feeDetailsDao.markPaymentStatus(userId, entityId, 1, null, null, paymentAuditId, String.valueOf(amount), null, description, role, null, null, 0.0, "ONLINE");
		} else {
			// TODO : create a queue or table of payment done but not settled in
			// the system.
		}

	}

	private PaymentAudit getPaymentAudit(String qfixRefNumber) {

		String query = "select * from payment_audit where qfix_reference_number=:qfixRefNumber";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("qfixRefNumber", qfixRefNumber);

		PaymentAudit paymentAudit = getNamedParameterJdbcTemplate().queryForObject(query, parameterSource, new RowMapper<PaymentAudit>() {
			@Override
			public PaymentAudit mapRow(ResultSet rs, int arg1) throws SQLException {
				PaymentAudit paymentAudit = new PaymentAudit();
				paymentAudit.setId(rs.getInt("id"));
				paymentAudit.setPayload(rs.getString("payload"));
				paymentAudit.setPaymentTypeId(rs.getInt("payment_type_id"));
				paymentAudit.setEntityId(rs.getInt("entity_id"));
				paymentAudit.setUserId(rs.getInt("user_id"));
				paymentAudit.setAmount(rs.getFloat("amount"));
				paymentAudit.setDescription(rs.getString("description"));
				paymentAudit.setStatus(rs.getString("status"));
				paymentAudit.setPaymentMode(rs.getString("payment_mode"));
				return paymentAudit;
			}
		});

		return paymentAudit;
	}

	private void updatePaymentAuditStatus(String qfixRefNumber, String status, float tax, float totalAmount, String paymentOptionName, String paymentMode,
					String totalPaidAmount) {

		String query = "update payment_audit set status = :status, tax=:tax, total_amount=:totalAmount, option_name=:paymentOptionName, " +
						" mode_of_payment=:paymentMode, total_paid_amount=:totalPaidAmount where qfix_reference_number=:qfixRefNumber";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("qfixRefNumber", qfixRefNumber);
		parameterSource.addValue("status", status);
		parameterSource.addValue("tax", tax);
		parameterSource.addValue("totalAmount", totalAmount);
		parameterSource.addValue("paymentOptionName", StringUtils.isEmpty(paymentOptionName) ? "Not available" : paymentOptionName);
		parameterSource.addValue("paymentMode", StringUtils.isEmpty(paymentMode) ? "ONLINE" : paymentMode);
		parameterSource.addValue("totalPaidAmount", totalPaidAmount);

		int UPDATE_STATUS = getNamedParameterJdbcTemplate().update(query, parameterSource);

		if (UPDATE_STATUS != 1) {
			log.error("Error occured while trying to update payment audit status. qfixRefNumber :: " + qfixRefNumber);

			// Write to queue or send email to admin for fallback..........
		}

	}

	private void markFeesAsPaid(String payload, Integer entityId, float totalAmount, Integer userId, Integer paymentAuditId, String role,
					String qfixRefNumber, String totalPaidAmountStr, String paymentType) throws IOException, AuthorisationException, ParseException {

		ObjectMapper mapper = new ObjectMapper();
		List<PaymentTransaction> feeDuesList = Arrays.asList( mapper.readValue(payload.toString(), PaymentTransaction[].class) );
		log.debug("feeDuesList :: " + feeDuesList);
		int numberOfFeesPaid = feeDuesList.size();
		double chargesApplicableForFees = 0;
		double totalPaidAmount = StringUtils.isEmpty(totalPaidAmountStr) ? 0.0 : Double.parseDouble(totalPaidAmountStr);
		if (totalPaidAmount > 0) {
			double totalChargesApplicable = totalPaidAmount - totalAmount;
			if (totalChargesApplicable > 0) {
				chargesApplicableForFees = totalChargesApplicable / numberOfFeesPaid;
			}
		}

		for (PaymentTransaction feeDues : feeDuesList) {
			try {
				System.out.println("Marking fees PAID::::==========================:::::::" + feeDues);
				feeDetailsDao.markPaymentStatus(userId, entityId, 1, feeDues.getFeesId(), feeDues.getStudentId(), paymentAuditId, feeDues.getAmount(), null,
								feeDues.getDescription(), role, feeDues.getFeeScheduleId(), qfixRefNumber, chargesApplicableForFees, paymentType);

			} catch (Exception e) {
				// TODO : create a queue or table of payment done but not
				// settled in
				// the system.
				log.error("Problem while acknowledge payment ****>>>>", e);
				insertIntoPaymentTransactionAcknowledgementFailures(payload, entityId, userId, paymentAuditId, role, qfixRefNumber, feeDues.getFeeScheduleId(),
								feeDues, e);
			}
		}

	}

	private void insertIntoPaymentTransactionAcknowledgementFailures(String payload, Integer entityId, Integer userId, Integer paymentAuditId, String role,
					String qfixRefNumber, long feeScheduleId, PaymentTransaction feeDues, Exception cause) {

		try {
			SimpleJdbcInsert insertIntoPaymentTransactionStatus =
							new SimpleJdbcInsert(getJdbcTemplate())
											.withTableName("payment_transaction_acknowledgement_failures")
											.usingColumns("payload", "entity_id", "user_id", "payment_audit_id", "role", "qfix_reference_number",
															"fee_schedule_id", "fee_dues_obj", "error_stack");

			Map<String, Object> parameters = new HashMap<String, Object>(4);
			parameters.put("payload", payload);
			parameters.put("entity_id", entityId);
			parameters.put("user_id", userId);
			parameters.put("payment_audit_id", paymentAuditId);
			parameters.put("role", role);
			parameters.put("qfix_reference_number", qfixRefNumber);
			parameters.put("fee_schedule_id", (feeScheduleId == 0) ? null : feeScheduleId);
			parameters.put("error_stack", Arrays.asList(cause.getStackTrace()).toString());

			String feesDueStr = feeDues.toString();
			try {
				feesDueStr = new ObjectMapper().writeValueAsString(feeDues);
			} catch (Exception e) {
				e.printStackTrace();
			}
			parameters.put("fee_dues_obj", feesDueStr);
			insertIntoPaymentTransactionStatus.execute(parameters);
			log.error("insertIntoPaymentTransactionAcknowledgementFailures parameters >>>>>>>>>>>>" + parameters);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("insertIntoPaymentTransactionAcknowledgementFailures error >>>>>>>>>>>>" + e);
		}
	}

	private float calculateTaxAmount(float totalAmount, float amount, float internetHandlingCharges) {
		BigDecimal baseAmount = new BigDecimal(amount).add(new BigDecimal(internetHandlingCharges));
		BigDecimal db = (new BigDecimal(totalAmount).subtract(baseAmount)).setScale(2, BigDecimal.ROUND_HALF_UP);
		return db.floatValue();

	}

	@Override
	public void acknowledgePayment(String key, String payload, Integer createdBy) throws Exception {
		InputStream resourceInputStream = null;
		ObjectInputStream objectInputStream = null;

		try {

			byte[] decoded = Base64Utils.decode(key.getBytes());

			resourceInputStream = getClass().getResourceAsStream("/private.key");
			objectInputStream = new ObjectInputStream(resourceInputStream);
			final PrivateKey privateKey = (PrivateKey) objectInputStream.readObject();
			SecretKey secretKey = EncryptionDecryptionUtil.decryptSecretKey(decoded, privateKey);

			//TODO need to move to more secure public-private based information exchange.
			//final String decryptedPayload = EncryptionDecryptionUtil.decrypt(payload, secretKey);
			final String decryptedPayload = payload;

			log.debug("Decrypted payload :: " + decryptedPayload);

			ObjectMapper mapper = new ObjectMapper();
			AcknowledgePaymentProcessedRequest acknowledgePaymentProcessedRequest = mapper.readValue(
							decryptedPayload, AcknowledgePaymentProcessedRequest.class);

			acknowledgePaymentProcessedRequest.setChannel(CHANNEL_SHOPPING);
			acknowledgePaymentProcessedRequest.setPaymentTaxType(PAYMENT_TYPE_T2);

			ShoppingProfile shoppingProfile = shoppingDao.getShoppingProfileDetails(acknowledgePaymentProcessedRequest.getCustomerEmail());

			String qfixRefNumber = RandomStringUtils.random(15, true, true).toUpperCase();
			acknowledgePaymentProcessedRequest.setQfixReferenceNumber(qfixRefNumber);
			float tax = calculateTaxAmount(acknowledgePaymentProcessedRequest.getTotalAmount(), acknowledgePaymentProcessedRequest.getAmount(),
							acknowledgePaymentProcessedRequest.getInternetHandlingCharges());

			Long paymentAuditId = savePaymentAuditDetail(acknowledgePaymentProcessedRequest.getAmount(),
							acknowledgePaymentProcessedRequest.getInternetHandlingCharges(), acknowledgePaymentProcessedRequest.getShippingCharges(),
							acknowledgePaymentProcessedRequest.getTotalAmount(), tax,
							"SHOPPING PAYMENT", null, shoppingProfile.getUserId(), PAYMENT_TYPE_SHOPPING,
							decryptedPayload,
							qfixRefNumber, PaymentStatus.SUCCESS.get(), createdBy);

			LoginResponse loginResponse = login(USER_NAME, PASSWORD);
			Map<String, String> httpHeader = new HashMap<String, String>();
			httpHeader.put("token", loginResponse.getToken());

			SavePaymentDetailResponse savePaymentDetailResponse = null;

			RestClient restClient = new RestClient();
			ResponseEntity<String> serviceResponse = restClient
							.executeRequest(acknowledgePaymentProcessedRequest,
											paymentUrlConfiguration.acknowledgePaymentProcessedByShopping(), new Object[] {}, httpHeader);

			if (serviceResponse.getStatusCode() == HttpStatus.OK) {
				savePaymentDetailResponse = mapper.readValue(
								serviceResponse.getBody(), SavePaymentDetailResponse.class);

			} else {
				log.error("Acknowledge payment details request failed with status:" + serviceResponse.getStatusCode());
				log.error(serviceResponse.getBody());
				throw new ApplicationException("Acknowledge payment details request failed with status:"
								+ serviceResponse.getBody());

			}

			int UPDATE_STATUS = updatePaymentAuditDetail(paymentAuditId, savePaymentDetailResponse.getId());

			if (UPDATE_STATUS != 1) {
				log.error("Error occured while updating payment audit record after acknowledge payment call for payment audit record id: " + paymentAuditId);
			}

		} finally {

			if (resourceInputStream != null) {
				resourceInputStream.close();
			}

			if (objectInputStream != null) {
				objectInputStream.close();
			}
		}

	}

	@Override
	public Payment getPaymentDetails(Integer paymentId) {
		String retrivePassbookQuery = "select amount, DATE_FORMAT(paid_date, '%Y-%m-%d') as paid_date, payment_audit_id, passbook_id, fees_id  from payments where id = :paymentId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("paymentId", paymentId);

		log.debug("Retrive passbook query :: " + retrivePassbookQuery);
		Payment paymentDetails = getNamedParameterJdbcTemplate().queryForObject(retrivePassbookQuery, queryParameters,
						new RowMapper<Payment>() {
							@Override
							public Payment mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								Payment payment = new Payment();
								payment.setAmount(rs.getFloat("amount"));
								payment.setDate(rs.getString("paid_date"));
								payment.setFeesId(rs.getInt("fees_id"));
								payment.setPassbookId(rs.getInt("passbook_id"));
								payment.setPaymentAuditId(rs.getInt("payment_audit_id"));
								return payment;
							}
						});

		return paymentDetails;
	}

	@Override
	public Payment getPaymentDetailsWithAuditInformationForAdmin(Integer paymentId, Integer userId, boolean validateEntity) {
		String retrivePassbookQuery = "select fees.branch_id as branch_id, payments.amount, "
						+ "DATE_FORMAT(payments.paid_date, '%d-%m-%Y') as paid_date, payments.payment_audit_id, " +
						" payments.passbook_id, payments.fees_id, fees.description as fees_description, payment_audit.qfix_reference_number, " +
						" payment_audit.description as audit_description, payment_audit.option_name as payment_channel, " +
						" payment_audit.mode_of_payment as mode_of_payment, " +
						" CONCAT(IFNULL(student.first_name,''),' ', IFNULL(student.last_name,'')) as for_person, institute.logo_url as logo_url " +
						" from payments " +
						" left join fees on fees.id = payments.fees_id " +
						" left join student on student.id= payments.student_id " +
						" left join payment_audit on payment_audit.id = payments.payment_audit_id " +
						" left join passbook on passbook.id = payments.passbook_id " +
						" left join branch on branch.id = fees.branch_id " +
						" left join branch_admin on branch_admin.branch_id = branch.id " +
						" left join institute on institute.id = branch.institute_id " +
						" where payments.id = :paymentId ";
		if (validateEntity) {
			retrivePassbookQuery += " AND fees.branch_id in" +
							" ( " +
							"	select branch_id from branch_admin where user_id =:userId AND is_delete = 'N'" +
							" )";
		}

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("paymentId", paymentId);
		queryParameters.put("userId", userId);

		log.debug("Retrive passbook query :: " + retrivePassbookQuery);
		Payment paymentDetails = getNamedParameterJdbcTemplate().queryForObject(retrivePassbookQuery, queryParameters,
						new RowMapper<Payment>() {
							@Override
							public Payment mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								Payment payment = new Payment();
								payment.setAmount(rs.getFloat("amount"));
								payment.setDate(rs.getString("paid_date"));
								payment.setFeesId(rs.getInt("fees_id"));
								payment.setPassbookId(rs.getInt("passbook_id"));
								payment.setPaymentAuditId(rs.getInt("payment_audit_id"));
								payment.setFeesDescription(rs.getString("fees_description"));
								payment.setQfixRefNumber(rs.getString("qfix_reference_number"));
								payment.setAuditDescription(rs.getString("audit_description"));
								payment.setPaymentChannel(rs.getString("payment_channel"));
								payment.setModeOfPayment(rs.getString("mode_of_payment"));
								payment.setForPerson(rs.getString("for_person"));
								payment.setInstituteLogoUrl(rs.getString("logo_url"));
								payment.setBranchId(rs.getInt("branch_id"));
								return payment;
							}
						});

		return paymentDetails;
	}

	@Override
	public Payment getPaymentDetailsWithAuditInformation(Integer paymentId, Integer entityId, Integer userId) {
		String retrivePassbookQuery = "select fees.branch_id as branch_id , payments.amount, "
						+ "DATE_FORMAT(payments.paid_date, '%d-%m-%Y') as paid_date, payments.payment_audit_id, " +
						" payments.passbook_id, payments.fees_id, fees.description as fees_description, payment_audit.qfix_reference_number, " +
						" payment_audit.description as audit_description, payment_audit.option_name as payment_channel, " +
						" payment_audit.mode_of_payment as mode_of_payment, " +
						" CONCAT(IFNULL(student.first_name,''),' ', IFNULL(student.last_name,'')) as for_person, institute.logo_url as logo_url " +
						" from payments " +
						" left join fees on fees.id = payments.fees_id " +
						" left join student on student.id= payments.student_id " +
						" left join payment_audit on payment_audit.id = payments.payment_audit_id " +
						" left join passbook on passbook.id = payments.passbook_id " +
						" left join branch on branch.id = fees.branch_id " +
						" left join institute on institute.id = branch.institute_id " +
						" where payments.id = :paymentId and " +
						" (" +
						" payments.created_by=:userId " +
						" or " +
						" exists ( select parent_id from branch_student_parent where branch_student_parent.student_id= payments.student_id and " +
						" (branch_student_parent.parent_id=:entityId or branch_student_parent.student_id=:entityId) ) " +
						" )";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("paymentId", paymentId);
		queryParameters.put("entityId", entityId);
		queryParameters.put("userId", userId);

		log.debug("Retrive passbook query :: " + retrivePassbookQuery);
		Payment paymentDetails = getNamedParameterJdbcTemplate().queryForObject(retrivePassbookQuery, queryParameters,
						new RowMapper<Payment>() {
							@Override
							public Payment mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								Payment payment = new Payment();
								payment.setAmount(rs.getFloat("amount"));
								payment.setDate(rs.getString("paid_date"));
								payment.setFeesId(rs.getInt("fees_id"));
								payment.setPassbookId(rs.getInt("passbook_id"));
								payment.setPaymentAuditId(rs.getInt("payment_audit_id"));
								payment.setFeesDescription(rs.getString("fees_description"));
								payment.setQfixRefNumber(rs.getString("qfix_reference_number"));
								payment.setAuditDescription(rs.getString("audit_description"));
								payment.setPaymentChannel(rs.getString("payment_channel"));
								payment.setModeOfPayment(rs.getString("mode_of_payment"));
								payment.setForPerson(rs.getString("for_person"));
								payment.setInstituteLogoUrl(rs.getString("logo_url"));
								payment.setBranchId(rs.getInt("branch_id"));
								return payment;
							}
						});

		return paymentDetails;
	}

	@Override
	public String initiateOnlineAdmissionFeesPayment(OnlineAdmissionFee onlineAdmissionFee, OnlineAdmissionFormDetail onlineAdmissionFormDetails,
					String channel, OnlineAdmissionConfiguration configuration)
					throws JsonParseException, JsonMappingException, IOException {

		//float feesAmount = onlineAdmissionFee.getAmount();
		ObjectMapper mapper = new ObjectMapper();
		OnlineAdmission onlineAdmission = mapper.readValue(onlineAdmissionFormDetails.getFormPayload().replaceAll("\\\\", ""), OnlineAdmission.class);

		String payload = mapper.writeValueAsString(onlineAdmissionFee);

		//Retrieve admission amount for course.
		float feesAmount = 0.0f;
		if ("Y".equals(configuration.getAllowToSpecifyCustomAdmissionFeesAmount())) {
			feesAmount = (float) validateAndGetOnlineAdmissionFeeAmount(onlineAdmissionFee, onlineAdmission, configuration);
		} else {
			feesAmount = getOnlineAdmissionFeeAmount(onlineAdmissionFee, onlineAdmissionFormDetails.getCategory());
		}

		// Start payment process
		Map<String, Object> queryParameters = new HashMap<String, Object>();

		String query = "select payment_setting.* from payment_setting where payment_setting.branch_id IS NULL ";

		query += " or payment_setting.branch_id = :branchId ";
		queryParameters.put("branchId", onlineAdmissionFormDetails.getBranch().getId());

		query += " order by payment_setting.branch_id desc";

		log.debug("Payment settings query : " + query);

		List<PaymentSetting> paymentDetailList = getNamedParameterJdbcTemplate().query(query, queryParameters,
						new RowMapper<PaymentSetting>() {
							@Override
							public PaymentSetting mapRow(ResultSet rs, int rowNum) throws SQLException {
								return new PaymentSetting(rs.getInt("gateway_id"),
												rs.getFloat("ic_amount"),
												rs.getFloat("ic_percentage"),
												rs.getInt("branch_id"), false);
							}
						});

		log.debug("paymentDetailList.size : " + paymentDetailList.size());

		float internetHandlingAmount = 0f;
		float internetHandlingPercentage = 0f;

		String sql = "select scheme_code_id, currency.code, icon_css_class "
				+ " from online_admission_scheme_configuration "
				+ " INNER JOIN currency on currency.code = category "
				+ " where branch_id = "+onlineAdmissionFormDetails.getBranch().getId()
				+ " and category = '"+onlineAdmissionFormDetails.getCategory()+"' group by category"   ;

		List<Integer> schemeCodeIds = new ArrayList<>();
		System.out.println(sql);
		Map<String, Object> schemeCurrencyMap = getJdbcTemplate().queryForMap(sql);
		schemeCodeIds.add((Integer)schemeCurrencyMap.get("scheme_Code_id"));
		List<Integer> gatewayIds = checkPgConfigsBasedOnSchemeCodes(schemeCodeIds);

		Integer paymentGatewayId = null;

		for (PaymentSetting ps : paymentDetailList) {
			internetHandlingAmount = ps.getInternetHandlingChargesAmount();
			internetHandlingPercentage = ps.getInternetHandlingChargesPercentage();
			paymentGatewayId = ps.getPaymentGatewayId();
			if (gatewayIds.contains(paymentGatewayId)) {
				break;
			}
		}

		log.debug("internetHandlingAmount : " + internetHandlingAmount);
		log.debug("internetHandlingPercentage : " + internetHandlingPercentage);
		log.debug("paymentGatewayId : " + paymentGatewayId);

		float internetHandlingFeesOnTransactionAmount = (feesAmount * internetHandlingPercentage) / 100;

		BigDecimal db = new BigDecimal(internetHandlingFeesOnTransactionAmount).setScale(2, BigDecimal.ROUND_HALF_UP);
		float covertedInternetHandlingFeesOnTransactionAmount = db.floatValue();
		log.debug("covertedInternetHandlingFeesOnTransactionAmount : " + covertedInternetHandlingFeesOnTransactionAmount);
		float internetHandlingCharges = (covertedInternetHandlingFeesOnTransactionAmount > internetHandlingAmount) ? covertedInternetHandlingFeesOnTransactionAmount
						: internetHandlingAmount;

		String qfixRefNumber = RandomStringUtils.random(15, true, true).toUpperCase();
		String description = "Fees Payment By applicant id :" + onlineAdmissionFormDetails.getApplicantId();
		
		if(onlineAdmission.getCourseDetails().getCourse().getName().equalsIgnoreCase("Registration")){
			description="payment made for registration ";
			
		}
		else if(onlineAdmission.getCourseDetails().getCourse().getName().equalsIgnoreCase("Merchandise")){
			
			description="payment made for merchandise";
		}
		else if(onlineAdmission.getCourseDetails().getCourse().getName().equalsIgnoreCase("Donation")){
			
			description="payment made for donation";
		}
		
		else{
			description = "Fees Payment By applicant id :" + onlineAdmissionFormDetails.getApplicantId();
			
		}
		Long paymentAuditId = saveOnlineAdmissionFeePaymentAuditDetail(qfixRefNumber, feesAmount, internetHandlingCharges, 0.0f, 0.0f,
						description, PaymentStatus.UNPAID.get(), PAYMENT_TYPE_FEES, payload, onlineAdmission.getCourseDetails().getCourse().getId(),
						onlineAdmissionFormDetails.getApplicantId());

		Name studentName = onlineAdmission.getCandidateDetails().getName();

		String customerName = studentName.getFirstname() + " " + (studentName.getSurname() != null ? studentName.getSurname() : "");
		String customerEmail = onlineAdmission.getContactDetails().getPersonalEmail();
		String customerMobile = onlineAdmission.getContactDetails().getPersonalMobileNumber();
		String customerAddress = onlineAdmission.getAddressDetails().getLocalAddress().getAddressLine1();

		State state = onlineAdmission.getAddressDetails().getLocalAddress().getState();

		String customerState = (state != null) ? state.getName() : "";
		String customerCountry = "IND";
		String customerCity = onlineAdmission.getAddressDetails().getLocalAddress().getArea();
		String customerPincode = "" + onlineAdmission.getAddressDetails().getLocalAddress().getPincode();

		// TODO Update currencyIconClass value 
		SavePaymentDetailResponse savePaymentDetailResponse = savePaymentDetails(feesAmount, internetHandlingCharges, 0f, description, qfixRefNumber,
						paymentGatewayId, false, channel, customerName, customerEmail, customerMobile, customerAddress,
						customerCity, customerState, customerCountry, customerPincode, PAYMENT_TYPE_T1, null, null, schemeCurrencyMap);

		int UPDATE_STATUS = updateOnlineAdmissionPaymentAuditDetail(paymentAuditId, savePaymentDetailResponse.getId());

		if (UPDATE_STATUS != 1) {
			log.error("Error occured while updating payment audit record : " + paymentAuditId);
			throw new ApplicationException("Error occured while updating payment audit record : " + paymentAuditId);
		}

		return savePaymentDetailResponse.getAccessCode();
	}

	private double validateAndGetOnlineAdmissionFeeAmount(OnlineAdmissionFee onlineAdmissionFee, OnlineAdmission onlineAdmission,
					OnlineAdmissionConfiguration configuration) {
		String templateFactoryName = onlineAdmissionDao.getExportTemplateFactoryName(configuration.getBranchId(),
						onlineAdmission.getCourseDetails().getCourse().getId().intValue());

		Validator validator = validatorFactory.getPdfBuilderInstance(templateFactoryName);
		if (validator != null) {
			validator.validate(onlineAdmission, configuration);
		}
		return onlineAdmission.getCourseDetails().getPaymentAmount();
	}

	private int updateOnlineAdmissionPaymentAuditDetail(Long paymentAuditId, int paymentDetailId) {

		String query = "update online_admission_payment set payment_detail_id = ? where id=?";
		Object[] inputsParam = new Object[] { paymentDetailId, paymentAuditId };
		int UPDATE_STATUS = getJdbcTemplate().update(query, inputsParam);
		return UPDATE_STATUS;
	}

	private Long saveOnlineAdmissionFeePaymentAuditDetail(String qfixRefNumber, float amount, float internetHandlingCharges, float tax,
					float totalAmount, String description, String status, Integer paymentTypeId, String payload,
					Long courseId, String applicantId) {
		SimpleJdbcInsert insertPaymentAudit =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("online_admission_payment")
										.usingColumns("qfix_reference_number", "amount", "internet_handling_charges", "tax", "total_amount",
														"description", "status", "payment_type_id", "payload", "course_id", "applicant_id")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(11);
		parameters.put("qfix_reference_number", qfixRefNumber);
		parameters.put("amount", amount);
		parameters.put("internet_handling_charges", internetHandlingCharges);
		parameters.put("tax", tax);
		parameters.put("total_amount", totalAmount);
		parameters.put("description", description);
		parameters.put("status", status);
		parameters.put("payment_type_id", paymentTypeId);
		parameters.put("payload", payload);
		parameters.put("course_id", courseId);
		parameters.put("applicant_id", applicantId);

		Long paymentAuditId = (Long) insertPaymentAudit.executeAndReturnKey(parameters);
		return paymentAuditId;
	}

	@Override
	public PaymentDetailResponse acknowledgeOnlineAdmissionPayment(String accessCode) {
		PaymentDetailResponse paymentDetailResponse = getPaymentDetail(accessCode);

		String qfixRefNumber = paymentDetailResponse.getQfixReferenceNumber();
		String status = paymentDetailResponse.getPaymentStatus();

		if ("SUCCESS".equalsIgnoreCase(status)) {
			float tax = calculateTaxAmount(paymentDetailResponse.getTotalAmount(), paymentDetailResponse.getAmount(),
							paymentDetailResponse.getInternetHandlingCharges());

			updateOnlineAdmissionPaymentAuditStatus(qfixRefNumber, status, tax, paymentDetailResponse.getTotalAmount(),
							paymentDetailResponse.getPaymentOptionName(),
							paymentDetailResponse.getPaymentMode());
			sendSuccessPaymentNotification(paymentDetailResponse.getTotalAmount(), qfixRefNumber, "Online Form", true);
		} else {
			throw new ApplicationException("Can not acknowldege this payment, invalid payment status reported.");
		}

		return paymentDetailResponse;
	}

	/**
	 * This method is used to send email and sms to end user after successfull
	 * payment.
	 * 
	 * @param amount
	 *            The payment amount
	 * @param qfixRefNumber
	 *            The Qfix Reference Number by which payment is done.
	 */
	private void sendSuccessPaymentNotification(float amount, String qfixRefNumber, String subjectContentStr, boolean isAdmissionPayment) {

		Contact contact = isAdmissionPayment ? getContactDetailsForAdmission(qfixRefNumber) : getContactDetails(qfixRefNumber);
		Map<String, Object> content = new HashMap<String, Object>();
		content.put("qfix_ref_number", qfixRefNumber);
		content.put("amount", amount);
		content.put("sur_name", contact.getLastName());
		content.put("first_name", contact.getFirstName());
		content.put("subject_content", subjectContentStr);

		System.out.println("Email ::: "+ contact.getEmail() + ", MObile ::: "+ contact.getPhone()) ;
		if (!StringUtils.isEmpty(contact.getEmail())) {
			sendPaymentSuccessEmail(subjectContentStr, contact, content);
		}
		if (!StringUtils.isEmpty(contact.getPhone())) {
			sendPaymentSuccessSms(subjectContentStr, contact, content);
		}
	}

	private void sendPaymentSuccessSms(String subjectContentStr, Contact contact, Map<String, Object> content) {

		Sms sms = new Sms();
		sms.setType("SMS");
		sms.setContactNumber(contact.getPhone());
		sms.setTemplateCode(SmsTemplate.PAYMENT_SUCCESS_SMS.get());
		sms.setSmsContent(content);

		String jsonSend;
		try {
			jsonSend = new ObjectMapper().writeValueAsString(sms);
			notificationClient.sendMessage(jsonSend);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendPaymentSuccessEmail(String subjectContentStr, Contact contact, Map<String, Object> content) {
		Notification notification = new Notification();
		notification.setContent(content);
		notification.setTemplateCode(EmailTemplate.PAYMENT_SUCCESS_EMAIL.get());
		notification.setType("EMAIL");
		notification.setTo(contact.getEmail());
		Map<String, Object> subjectContent = new HashMap<String, Object>();
		subjectContent.put("subject_content", subjectContentStr);
		notification.setSubjectContent(subjectContent);

		String jsonSend;
		try {
			jsonSend = new ObjectMapper().writeValueAsString(content);
			notificationClient.sendMessage(jsonSend);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Contact getContactDetails(String qfixRefNumber) {
		String query = " select * from contact as c " +
						" inner join payment_audit as pa on c.user_id = pa.user_id " +
						" where qfix_reference_number = :qfixRefNumber";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("qfixRefNumber", qfixRefNumber);
		return getNamedParameterJdbcTemplate().queryForObject(query,
						parameterSource, new RowMapper<Contact>() {
							@Override
							public Contact mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new Contact(rs.getString("firstname"), rs.getString("lastname"), rs.getString("city"), rs.getString("photo"),
												new Integer(rs.getInt("user_id")), rs.getString("email"), rs.getString("phone"));
							}
						});

	}

	private Contact getContactDetailsForAdmission(String qfixRefNumber) {
		String query = " select * from online_admission as oa " +
				" inner join online_admission_payment as oap on oa.applicant_id = oap.applicant_id " +
				" where qfix_reference_number = :qfixRefNumber";
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("qfixRefNumber", qfixRefNumber);
		return getNamedParameterJdbcTemplate().queryForObject(query,
				parameterSource, new RowMapper<Contact>() {
			@Override
			public Contact mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				return new Contact(rs.getString("applicant_firstname"), rs.getString("applicant_surname"), "" ,  "",
						null, rs.getString("applicant_email"), rs.getString("applicant_mobile"));
			}
		});
	}

	private void updateOnlineAdmissionPaymentAuditStatus(String qfixRefNumber, String status, float tax, float totalAmount, String paymentOptionName,
					String paymentMode) {

		log.debug("totalAmount :: " + totalAmount);
		log.debug("qfixRefNumber :: " + qfixRefNumber);
		log.debug("tax :: " + tax);
		log.debug("paymentOptionName :: " + paymentOptionName);
		log.debug("paymentMode :: " + paymentMode);

		String query = "update online_admission_payment set status = :status, tax=:tax, total_amount=:totalAmount, option_name=:paymentOptionName, " +
						" mode_of_payment=:paymentMode, paid_date=:paymentDate where qfix_reference_number=:qfixRefNumber";

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("qfixRefNumber", qfixRefNumber);
		parameterSource.addValue("status", status);
		parameterSource.addValue("tax", tax);
		parameterSource.addValue("totalAmount", totalAmount);
		parameterSource.addValue("paymentOptionName", paymentOptionName);
		parameterSource.addValue("paymentMode", paymentMode);
		parameterSource.addValue("paymentDate", status.equalsIgnoreCase("success") ? new Date() : null);

		int UPDATE_STATUS = getNamedParameterJdbcTemplate().update(query, parameterSource);

		if (UPDATE_STATUS != 1) {
			log.error("Error occured while trying to update payment audit status. qfixRefNumber :: " + qfixRefNumber);

			// Write to queue or send email to admin for fallback..........
		}

	}

	@Override
	public List<PaymentBranchScheme> getBranchPaymentSchemeList(Integer branchId) {

		Set<Integer> paymentConfigIdList = getPaymentConfigIdByBranch(branchId);
		LoginResponse loginResponse = login(USER_NAME, PASSWORD);
		Map<String, String> httpHeader = new HashMap<String, String>();
		httpHeader.put("token", loginResponse.getToken());

		RestClient restClient = new RestClient();
		List<PaymentBranchScheme> paymentBranchSchemes = null;

		try {
			Object[] urlParemeter = new Object[] { StringUtils.join(paymentConfigIdList, ",") };
			System.out.println("Getting scheme codes ------------------------------ \n " + urlParemeter.toString() +"\t config ids " 
			+ paymentConfigIdList.toString()+ " \n \t" + StringUtils.join(paymentConfigIdList, ","));
			ResponseEntity<String> getBranchPaymentSchemeResponse = restClient
							.executeRequest(paymentUrlConfiguration.getBranchSpecificSchemeList(), urlParemeter, httpHeader);
			
			if (getBranchPaymentSchemeResponse.getStatusCode() == HttpStatus.OK) {

				log.info("branchscheme::::::::::     " + getBranchPaymentSchemeResponse.getBody());
				ObjectMapper mapper = new ObjectMapper();
				//paymentDetailResponse = mapper.readValue(
				//	getBranchPaymentSchemeResponse.getBody(), PaymentDetailResponse.class);

				//List<PaymentBranchScheme> myObjects = mapper.readValues(getBranchPaymentSchemeResponse.getBody(), new TypeReference<List<PaymentBranchScheme>>(){});
				paymentBranchSchemes = mapper.readValue(getBranchPaymentSchemeResponse.getBody(),
								mapper.getTypeFactory().constructCollectionType(List.class, PaymentBranchScheme.class));
			}

			if (paymentBranchSchemes == null && paymentBranchSchemes.size() > 0)
			{
				log.error("Branch specific fee request failed with status " + getBranchPaymentSchemeResponse.getStatusCode());
				log.error(getBranchPaymentSchemeResponse.getBody());
				throw new ApplicationException("Get payment details response failed with status."
								+ getBranchPaymentSchemeResponse.getBody());
			}

		} catch (IOException e) {
			log.error(e);
			throw new ApplicationException("Payment acknowledge requets failed.", e);
		}

		//System.out.println(">>>>>>>>>" + loginResponse.getToken());
		return paymentBranchSchemes;
	}

	private Set<Integer> getPaymentConfigIdByBranch(Integer branchId) {
		Set<Integer> branchIdList = new HashSet<Integer>();
		branchIdList.add(branchId);
		List<PaymentSetting> paymentDetailList = getPaymentSetting(branchIdList, branchId);
		Set<Integer> gatewayIdList = new HashSet<>();
		for (PaymentSetting ps : paymentDetailList) {
			if (ps.getBranchId() != null) {
				gatewayIdList.add(ps.getPaymentGatewayId());
			}
		}
		if(gatewayIdList.size() > 0){
			return gatewayIdList;
		}
		log.error("Payment gateway configuration not found in payment_setting for branch :" + branchId);
		throw new ApplicationException("Invalid argument supplied, not able to load required details.");
	}

	private float getOnlineAdmissionFeeAmount(OnlineAdmissionFee onlineAdmissionFee, String category) {

		String retriveOnlineAdmissionFeeAmountQuery = "select amount from online_admission_course_fees where course_id = :courseId ";
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("courseId", onlineAdmissionFee.getCourseId());

		if (onlineAdmissionFee.getCasteId() != null && onlineAdmissionFee.getCasteId().intValue() != 0) {
			retriveOnlineAdmissionFeeAmountQuery += " and caste_id = :casteId ";
			queryParameters.put("casteId", onlineAdmissionFee.getCasteId());

		} else {
			retriveOnlineAdmissionFeeAmountQuery += " and caste_id IS NULL ";
		}

		if (onlineAdmissionFee.getSubcasteId() != null && onlineAdmissionFee.getSubcasteId().intValue() != 0) {
			retriveOnlineAdmissionFeeAmountQuery += " and subcaste_id = :subCasteId ";
			queryParameters.put("subCasteId", onlineAdmissionFee.getSubcasteId());

		} else {
			retriveOnlineAdmissionFeeAmountQuery += " and subcaste_id IS NULL ";
		}

		if(!StringUtils.isEmpty(category)){
			retriveOnlineAdmissionFeeAmountQuery += " AND currency = '"+category+"'" ;
		}

		log.debug("Retrive OnlineAdmissionFeeAmount query :: " + retriveOnlineAdmissionFeeAmountQuery);
		log.debug("Retrive queryParameters :: " + queryParameters);

		Double amount = null;
		try {
			amount = getNamedParameterJdbcTemplate().queryForObject(retriveOnlineAdmissionFeeAmountQuery, queryParameters,
							new RowMapper<Double>() {
								@Override
								public Double mapRow(ResultSet rs, int rowNum)
												throws SQLException {
									return rs.getDouble("amount");
								}
							});
		} catch (Exception e) {
			throw new ApplicationException("Invalid online admission course id specified.");
		}

		return amount.floatValue();
	}

	@Override
	public BranchConfiguration getBranchConfiguration(Integer branchId) {
		String sql = "select i.logo_url as logo_url, b.name as branchName,prc.*,CONCAT_WS(' ',b.address_line,b.area,b.city,b.pincode)as address " +
						" from branch b  " +
						" inner join institute as i on i.id = b.institute_id " +
						" left join  payment_receipt_configuration as prc on prc.branch_id = b.id " +
						" where b.id = " + branchId;

		List<BranchConfiguration> branchConfiguration = getJdbcTemplate().query(sql, new RowMapper<BranchConfiguration>() {

			@Override
			public BranchConfiguration mapRow(ResultSet rs, int rowNum) throws SQLException {
				BranchConfiguration cfg = new BranchConfiguration();
				cfg.setBranchId(rs.getInt("branch_id"));
				cfg.setHideLogo(rs.getString("hide_qfix_logo"));
				cfg.setUseSchoolLogAsWatermarkImage(rs.getString("watermark_image_url"));
				cfg.setTrailerRecord(rs.getString("trailer_record"));
				cfg.setAddress(rs.getString("address"));
				cfg.setBranchName(rs.getString("branchName"));
				cfg.setInstituteLogoUrl(rs.getString("logo_url"));
				return cfg;
			}

		});

		if (branchConfiguration.size() > 0) {

			return branchConfiguration.get(0);
		}
		else {

			return null;
		}
	}

	@Override
	public PaymentMoreInfo getPaymentMoreInfo(String qfixReferenceNumber) {
		String sql = "select payload from payment_audit where qfix_reference_number = '" + qfixReferenceNumber + "'";
		String payload = getJdbcTemplate().queryForObject(sql, String.class);
		PaymentMoreInfo info = new PaymentMoreInfo();
		try {
			List<FeesPayload> feesPayloadList = new ObjectMapper().readValue(payload,
							TypeFactory.defaultInstance().constructCollectionType(List.class,
											FeesPayload.class));

			FeesPayload feesPayload = feesPayloadList.get(0);
			sql = "select emailAddress from school_admin sc " +
							" INNER JOIN user as u on u.id = sc.user_id " +
							" INNER JOIN institute as i on u.institute_id = i.id " +
							" INNER JOIN branch as b on b.institute_id = i.id " +
							" INNER JOIN fees as f on f.branch_id = b.id WHERE f.id = " + feesPayload.getFeesId();

			String adminEmail = getJdbcTemplate().queryForObject(sql, String.class);

			info.setAdminEmail(adminEmail);
			List<Integer> studentIds = new ArrayList<>();
			for (FeesPayload feesPayloadObj : feesPayloadList) {
				studentIds.add(feesPayloadObj.getStudentId());
			}
			info.setStudentIds(studentIds);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return info;
	}

	@Override
	public PaymentDetailResponse updatePaymentMode(String paymentMode, String qfixReferenceNumber) {
		String sql = "update payment_audit set payment_mode= '" + paymentMode + "' where qfix_reference_number = '" + qfixReferenceNumber + "'";
		int updateCount = getJdbcTemplate().update(sql);
		if (updateCount <= 0) {
			throw new ApplicationException("Qfix Reference Number not found.");
		}
		return null;
	}
	
	private void sendLoginNotification(Integer userId){
		Notification notification = new Notification();

		Map<String, Object> additionalData = new HashMap<>();

		notification.setType("PAY_DIRECT_STUDENT_CREATE");
		notification.setBatchEnabled(false);
		notification.sendEmail(true);
		notification.sendSMS(true);

		additionalData.put("userId", userId);
		notification.setAdditionalData(additionalData);
		ObjectMapper mapper = new ObjectMapper();

		try {
			String jsonMessage = mapper.writeValueAsString(notification);
			notificationClient.sendMessage(jsonMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendLoginEmail( String subject,Contact  contact, Map<String,Object> content){


	}


	private void sendLoginSms( String subject,Contact  contact, Map<String,Object> content){


	}


	private RefundConfig getRefundConfigurationByBranchId(Integer branchId) {
		String sql = "select bc.*, b.institute_id from branch_configuration as bc "
				+ " inner join branch as b on bc.branch_id = b.id "
				+ " where branch_id = " + branchId;
		List<RefundConfig> refundConfigs = getJdbcTemplate().query(sql, new RowMapper<RefundConfig> (){
			@Override
			public RefundConfig mapRow(ResultSet rs, int arg1) throws SQLException {
				RefundConfig config = new RefundConfig(rs.getInt("branch_id"), 
						rs.getInt("institute_id"),
						rs.getString("allow_credit_card_refund"),
						rs.getString("allow_credit_card_partial_refund"),
						rs.getString("allow_debit_card_refund"),
						rs.getString("allow_debit_card_partial_refund"),
						rs.getString("allow_net_banking_refund"),
						rs.getString("allow_net_banking_partial_refund"),
						rs.getString("allow_neft_rtgs_refund"),
						rs.getString("allow_neft_rtgs_partial_refund"),
						rs.getString("allow_cheque_dd_refund"),
						rs.getString("allow_cheque_dd_partial_refund"),
						rs.getString("allow_cash_refund"),
						rs.getString("allow_cash_partial_refund")
						);
				return config;
			}});
		return refundConfigs.get(0);
	}


	@Override
	public void refundFeePayment(final Integer paymentId, final double amountToRefund, final Integer userId) {
		String sql = "select count(*) as count, sum(prd.amount) as totalRefundedAmount, "
				+ " pa.qfix_reference_number, pa.mode_of_payment, pa.payment_mode, f.branch_id, f.currency, p.amount as amountPaid "
				+ " from payments as p "
				+ " INNER JOIN payment_audit as pa on pa.id = p.payment_audit_id "
				+ " INNER JOIN fees as f on f.id = p.fees_id "
				+ " LEFT JOIN payment_refund_detail as prd on prd.payment_id = p.id "
				+ " where p.id = " + paymentId + " and prd.status != 'ERROR' and p.amount >= " + amountToRefund;

		getJdbcTemplate().query(sql,  new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				double amountPaid = rs.getDouble("amountPaid");
				if(rs.getInt("count") > 0 || amountPaid > 0){
					String qfixRefNumber = rs.getString("qfix_reference_number");
					Integer branchId = rs.getInt("branch_id");
					double totalRefundedAmount = rs.getDouble("totalRefundedAmount");
					totalRefundedAmount += amountToRefund;

					if(totalRefundedAmount <= amountPaid){
						String modeOfPaymentChar = rs.getString("payment_mode");
						String modeOfPayment = rs.getString("mode_of_payment");
						String currency = rs.getString("currency");
						RefundConfig config = getRefundConfigurationByBranchId(branchId);
						validateRefund(modeOfPayment, modeOfPaymentChar, amountToRefund, amountPaid, config);

						SavePaymentDetailResponse response = initiateFeeRefund(qfixRefNumber, amountToRefund, currency);
						saveRefundDetails(amountToRefund, paymentId, userId, response);
					}
					else {
						throw new ApplicationException("Amount to refund cannot be greater than amount paid, See previous refunds for this transaction.");
					}
				}else {
					throw new ApplicationException("Amount to refund cannot be greater than amount paid.");
				}
				return null;
			}
		});
	}


	private void saveRefundDetails(double amount, Integer paymentId, Integer userId, SavePaymentDetailResponse response) {
		SimpleJdbcInsert insertToken = new SimpleJdbcInsert(getDataSource())
				.withTableName("payment_refund_detail")
				.usingColumns("payment_id", "amount", "created_by", "status", "payment_gateway_transaction_id", "payment_gateway_refund_id")
				.usingGeneratedKeyColumns("id");

		Map<String, Object> parameters = new HashMap<String, Object>(5);
		parameters.put("amount", amount);
		parameters.put("payment_id", paymentId);
		parameters.put("created_by", userId);
		parameters.put("status", "IN PROGRESS");
		parameters.put("payment_gateway_transaction_id", response.getPgTransactionId());
		parameters.put("payment_gateway_refund_id", response.getTransactionRefundId());

		insertToken.execute(parameters);
		System.out.println("Inserted payment refund details.." + paymentId);

	}

	protected SavePaymentDetailResponse initiateFeeRefund(String qfixRefNumber, double amount, String currency) {
		RefundPaymentRequest request = new RefundPaymentRequest();
		request.setQfixReferenceNumber(qfixRefNumber);
		request.setAmount(amount);
		request.setCurrency(currency);

		LoginResponse loginResponse = login(USER_NAME, PASSWORD);
		Map<String, String> httpHeader = new HashMap<String, String>();
		httpHeader.put("token", loginResponse.getToken());

		RestClient restClient = new RestClient();

		SavePaymentDetailResponse response = null;
		try {
			ResponseEntity<String> serviceResponse = restClient
					.executeRequest(request,
						paymentUrlConfiguration.saveRefundFeePaymentConfig(), new Object[] {}, httpHeader);

			if (serviceResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				response = mapper.readValue(serviceResponse.getBody(), SavePaymentDetailResponse.class);
				System.out.println("\n\nREFUND PAYMENT RESPONSE :::: "+response);
			} else {
				log.error("Save payment details service request failed with status " + serviceResponse.getStatusCode());
				log.error(serviceResponse.getBody());

				System.out.println("Save fee refund server error :: " + serviceResponse.getBody());
				throw new ApplicationException("Could not process refund payment !");
			}
		} catch (IOException e) {
			log.error("", e);
			e.printStackTrace();
			throw new ApplicationException("Could not process refund payment !", e);
		}
		return response;
	}


	protected void validateRefund(String modeOfPayment, String modeOfPaymentChar, double amountToRefund, double amountPaid, RefundConfig config) {
		boolean refundAllowed = false;
		boolean partialRefundAllowed = false;

		if(config != null){
			if("O".equals(modeOfPaymentChar)){
				if("DEBIT CARD".equalsIgnoreCase(modeOfPayment)){
					refundAllowed = "Y".equals(config.getAllowDebitCardRefund());
					partialRefundAllowed = "Y".equals(config.getAllowDebitCardPartialRefund());
				}

				if("CREDIT CARD".equalsIgnoreCase(modeOfPayment)){
					refundAllowed = "Y".equals(config.getAllowCreditCardRefund());
					partialRefundAllowed = "Y".equals(config.getAllowCreditCardPartialRefund());
				}

				if("BANK".equalsIgnoreCase(modeOfPayment)){
					refundAllowed = "Y".equals(config.getAllowNetBankingRefund());
					partialRefundAllowed = "Y".equals(config.getAllowNetBankingPartialRefund());
				}
			}else {
				throw new ApplicationException("Refund is not allowed for offline payments.");
			}
		}

		if(amountToRefund > amountPaid){
			throw new ApplicationException("Amount to refund cannot be greater than amount paid.");
		}

		if(refundAllowed || partialRefundAllowed){
			if(!partialRefundAllowed && amountToRefund != amountPaid){
				throw new ApplicationException("Partial Refund not allowed at branch level.");
			}
		}else {
			throw new ApplicationException("Refund not allowed at branch level.");
		}
	}

	@Transactional
	@Override
	public void acknowledgeRefund(RefundDetail refundDetail) {
		List<RefundStatus> refundStatusList = refundDetail.getRefundDetails();
		Set<Integer> paymentIds = new HashSet<>();

		for(RefundStatus refundStatus : refundStatusList){
			try {
				String status = refundStatus.getRefundStatus();
				if("INITIATED".equals(status)){
					status = "IN PROGRESS";
				}
				if(refundStatus.getPaymentGatewayRefundId().equals("3607499")){
					refundStatus.setRefundStatus("SUCCESS");
				}
	
				Long passbookId = addRefundEntryToPassbook(refundStatus);
				if(passbookId != null){
					String sql = " update payment_refund_detail set status = :status, passbook_id = :passbook_id "
							+ " where payment_gateway_refund_id = :payment_gateway_refund_id and amount = :amount";
		
					Map<String, Object> parameters = new HashMap<>();
					parameters.put("payment_gateway_refund_id", refundStatus.getPaymentGatewayRefundId());
					parameters.put("status", refundStatus.getRefundStatus());
					parameters.put("passbook_id", passbookId);
					parameters.put("amount", refundStatus.getRefundAmount());
		
					getNamedParameterJdbcTemplate().update(sql, parameters);

					sql = "select payment_id from payment_refund_detail where payment_gateway_refund_id = "+ refundStatus.getPaymentGatewayRefundId();
					Integer paymentId = getJdbcTemplate().queryForObject(sql, Integer.class);
					paymentIds.add(paymentId);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		if(paymentIds.size() > 1){
			throw new ApplicationException("More than one transaction acknowledge found.");
		}else if(paymentIds.size() > 0){
			updatePaymentsAndFeesSchedule(paymentIds);
		}
	}


	private Long addRefundEntryToPassbook(final RefundStatus refundStatus) {

		String getRefundCount = "select count(*) as count, prd.*, fs.user_id, concat(c.firstname, ' ', c.lastname) as recieverName, p.title as passbookTitle "
				+ " from payment_refund_detail as prd "
				+ " INNER JOIN payments as pmnt on pmnt.id = prd.payment_id "
				+ " INNER JOIN fees_schedule as fs on fs.id = pmnt.fees_schedule_id "
				+ " INNER JOIN contact as c on c.user_id = fs.user_id "
				+ " INNER JOIN passbook as p on p.id = pmnt.passbook_id "
				+ " where payment_gateway_refund_id = :payment_gateway_refund_id ";

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("payment_gateway_refund_id", refundStatus.getPaymentGatewayRefundId());

		List<Long> passbookIds = getNamedParameterJdbcTemplate().query(getRefundCount, parameters, new RowMapper<Long>(){
			@Override
			public Long mapRow(ResultSet rs, int arg1) throws SQLException {
				if(!"SUCCESS".equals(rs.getString("status")) && "SUCCESS".equals(refundStatus.getRefundStatus())){
					String description = "Refund For : " + rs.getString("passbookTitle");
					try {
						return userDao.addEntryToPassbook("yes", Float.parseFloat(refundStatus.getRefundAmount()), rs.getInt("created_by"), 
								description, rs.getInt("user_id"), rs.getString("recieverName"), PASSBOOK_TRANSACTION_TYPE.CREDIT.get());
					} catch (Exception e) {
						e.printStackTrace();
						throw new ApplicationException(e.getMessage());
					}
				}
				return null;
			}
		});
		if(passbookIds != null && passbookIds.size() > 0){
			return passbookIds.get(0);
		}
		return null;
	}

	@Transactional
	@Override
	public PaymentDetailResponse markPaymentUnpaid(String accessCode, Integer entityId, String role) throws Exception {

		PaymentDetailResponse paymentDetailResponse = getPaymentDetail(accessCode);

		String qfixRefNumber = paymentDetailResponse.getQfixReferenceNumber();
		String status = paymentDetailResponse.getPaymentStatus();

		if (!"SUCCESS".equalsIgnoreCase(status)) {
			//Check here if fees schedule is success if not return;

			PaymentAudit paymentAudit = getPaymentAudit(qfixRefNumber);
			if (!"SUCCESS".equalsIgnoreCase(paymentAudit.getStatus())) {
				log.debug("***************************Already marked unpaid returning back****,**" + paymentDetailResponse.getQfixReferenceNumber()
								+ "*****************************************");
				return paymentDetailResponse;
			}

			String sql = "update payment_audit set status = '"+status+"' where qfix_reference_number = '"+qfixRefNumber+"'";
			getJdbcTemplate().update(sql);

			markFeesAsUnPaid(paymentAudit.getId());

		} else {
			throw new ApplicationException("Can not mark unpaid this payment, invalid payment status reported.");
		}

		return paymentDetailResponse;
	}


	private void markFeesAsUnPaid(Integer paymentAuditId) {
		String getPaymentIdsSql = "select GROUP_CONCAT(id) as paymentIds, GROUP_CONCAT(passbook_id) as passbookIds, "
				+ " GROUP_CONCAT(fees_schedule_id) as feeScheduleIds from payments where payment_audit_id = " + paymentAuditId;

		getJdbcTemplate().query(getPaymentIdsSql, new RowMapper<Object> (){
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				String paymentIdsStr = rs.getString("paymentIds");;
				if(paymentIdsStr != null){
					String sql = "update fees_schedule set status = 'UNPAID', pyment_id = null where pyment_id in("+paymentIdsStr+") ";
					getJdbcTemplate().update(sql);

					sql = "delete from payments where id in("+paymentIdsStr+") ";
					getJdbcTemplate().update(sql);
				}

				String passbookIdsStr = rs.getString("passbookIds");
				if(passbookIdsStr != null){
					String sql = "delete from passbook where id in("+passbookIdsStr+") ";
					getJdbcTemplate().update(sql);
				}
				return null;
			}
		});
	}

	private void updatePaymentsAndFeesSchedule(Set<Integer> paymentIds) {
		String paymentIdsStr = StringUtils.join(paymentIds, ",");
		String sql = "select sum(prd.amount) as refundedAmount, fees_schedule_id, p.amount as paidAmount "
				+ " from payments as p "
				+ " INNER JOIN payment_refund_detail as prd on prd.payment_id = p.id "
				+ " where p.id in ("+paymentIdsStr+") and prd.status = 'SUCCESS'";

		getJdbcTemplate().query(sql,  new RowMapper<Object>(){
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				double refundedAmount = rs.getDouble("refundedAmount");
				if(refundedAmount > 0){
					Long feesScheduleId = rs.getLong("fees_schedule_id");

					double remainingAmount = rs.getDouble("paidAmount") - refundedAmount;
					String status = "UNPAID";

					if(remainingAmount > 0){
						status = "PARTIAL_PAID";
					}

					String sql = "update fees_schedule set status = '"+status+"', partial_paid_amount = " + remainingAmount + " where id = " + feesScheduleId;
					getJdbcTemplate().update(sql);
				}
				return null;
			}
		});
	}
}