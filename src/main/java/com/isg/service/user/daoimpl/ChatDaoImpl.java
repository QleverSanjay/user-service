package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.client.Request;
import com.isg.service.user.client.RestClient;
import com.isg.service.user.client.ServiceConfiguration;
import com.isg.service.user.client.chat.ChatUrlConfiguration;
import com.isg.service.user.client.chat.request.group.CreatePrivateGroup;
import com.isg.service.user.client.chat.request.group.CreatePublicGroup;
import com.isg.service.user.client.chat.request.group.UpdateGroupRequest;
import com.isg.service.user.client.chat.request.session.SessionRequest;
import com.isg.service.user.client.chat.request.user.UserRequest;
import com.isg.service.user.client.chat.response.group.GroupResponse;
import com.isg.service.user.client.chat.response.session.UserResponse;
import com.isg.service.user.client.chat.response.user.UnreadMessageCountResponse;
import com.isg.service.user.dao.ChatDao;
import com.isg.service.user.dao.ParentDao;
import com.isg.service.user.dao.StudentDao;
import com.isg.service.user.dao.TeacherDao;
import com.isg.service.user.dao.UserDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.model.ChatGroup;
import com.isg.service.user.model.Contact;
import com.isg.service.user.model.Group;
import com.isg.service.user.model.GroupUser;
import com.isg.service.user.model.UserChatProfile;
import com.isg.service.user.request.ChatProfile;
import com.isg.service.user.request.Parent;
import com.isg.service.user.request.SearchContact;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.StudentAddress;
import com.isg.service.user.request.Teacher;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class ChatDaoImpl extends NamedParameterJdbcDaoSupport implements ChatDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private static final String RETRIEVE_SUGGESTED_GROUPS_QUERY = "select g.id, g.name, g.description " +
					" from `group` g " +
					" inner join group_tags gt on gt.group_id = g.id " +
					" where (g.branch_id IN (:branchIds) or g.branch_id is NULL) and g.active='Y' " +
					" and NOT EXISTS (select * from group_user gu where gu.user_id = :userId and gu.group_id = g.id) " +
					" and gt.tag in (:group_tags) " +
					" limit 50";

	private static final String RETRIEVE_CONTACT_BY_EMAIL_QUERY = "select * from contact " +
					" where contact.email = :email " +
					" and " +
					" NOT EXISTS (select * from user_contact where user_contact.user_id=:userId and user_contact.contact_user_id = contact.user_id) " +
					" AND  " +
					" NOT EXISTS (select * from user_contact where user_contact.user_id = contact.user_id and user_contact.contact_user_id=:userId) " +
					" limit 50";

	private static final String RETRIEVE_CONTACT_BY_PHONE_QUERY = "select * from contact " +
					" where contact.phone = :phone " +
					" and " +
					" NOT EXISTS (select * from user_contact where user_contact.user_id=:userId and user_contact.contact_user_id = contact.user_id) " +
					" AND  " +
					" NOT EXISTS (select * from user_contact where user_contact.user_id = contact.user_id and user_contact.contact_user_id=:userId) " +
					" limit 50";

	private static final Logger log = Logger.getLogger(ChatDaoImpl.class);

	@Autowired
	private UserDao userDAO;

	@Autowired
	private StudentDao studentDao;

	@Autowired
	private ParentDao parentDao;

	@Autowired
	private TeacherDao teacherDao;

	@Override
	@Transactional(readOnly = false)
	public void assignStudentChatGroupsToParent(Integer parentId, Integer parentUserId, List<GroupUser> groupUserList) {

		log.debug("Applicable chat dialog Ids for student by branch :::" + groupUserList.toString());
		// Add occupants to list
		if (!groupUserList.isEmpty()) {
			UserChatProfile userChatProfile = userDAO.getUserChatProfileDetailsbyParentId(parentId);

			UpdateGroupRequest updateGroupRequest = null;

			for (GroupUser groupUser : groupUserList) {
				updateGroupRequest = new UpdateGroupRequest();
				updateGroupRequest.setGroupId(groupUser.getChatDialogId());
				updateGroupRequest.setAddOccupantsIds(String.valueOf(userChatProfile.getChatProfileId()));
				GroupResponse response = updateChatGroup(ChatUrlConfiguration.UPDATE_GROUP_CONFIG, updateGroupRequest);

				if (response.getOccupantsAddRequestStatus().equalsIgnoreCase("SUCCESS")) {
					userDAO.updateGroupUser(parentUserId, groupUser.getGroupId(), false);
				}
			}
		}

	}

	@Override
	@Transactional(readOnly = false)
	public void joinGroup(Integer groupId, Integer userId, long chatProfileId, Integer roleId) {
		ChatGroup chatGroup = retriveChatGroupDetails(groupId);
		UpdateGroupRequest updateGroupRequest = new UpdateGroupRequest();
		updateGroupRequest.setGroupId(chatGroup.getChatDialogId());
		updateGroupRequest.setAddOccupantsIds(String.valueOf(chatProfileId));
		GroupResponse response = updateChatGroup(ChatUrlConfiguration.UPDATE_GROUP_CONFIG, updateGroupRequest);

		if (response.getOccupantsAddRequestStatus().equalsIgnoreCase("SUCCESS")) {
			userDAO.updateGroupUser(userId, groupId, true);
			userDAO.updateGroupIndividual(userId, groupId, roleId);
		}
	}

	@Override
	public ChatProfile getChatSessionDetails(String chatAccountUsername, String chatAccountPassword, long chatAccountProfileId) {
		ChatProfile chatProfile = null;
		try {
			SessionRequest sessionRequest = new SessionRequest();
			sessionRequest.setLogin(chatAccountUsername);
			sessionRequest.setPassword(chatAccountPassword);

			chatProfile = initiateChatSession(sessionRequest);
			chatProfile.setPassword(chatAccountPassword);
			chatProfile.setChatUserProfileId(chatAccountProfileId);
		} catch (Exception e) {
			log.error("Error in getChatSessionDetails for:");
			log.error("chatAccountUsername:" + chatAccountUsername);
			log.error("chatAccountProfileId:" + chatAccountProfileId);
		}
		return chatProfile;
	}

	@Override
	public UserResponse createChatAccount(String email, String firstname, String surname, String tags) {

		UserResponse userResponse = null;

		RestClient restClient = new RestClient();
		UserRequest userRequest = new UserRequest();
		userRequest.setEmail(email);
		userRequest.setFirstname(firstname);
		userRequest.setSurname(surname);
		userRequest.setTags(tags);

		try {
			ResponseEntity<String> createSessionResponse = restClient
							.executeRequest(userRequest,
											ChatUrlConfiguration.CREATE_USER_CONFIG);

			if (createSessionResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				userResponse = mapper.readValue(
								createSessionResponse.getBody(), UserResponse.class);

			} else {
				log.error("Create chat account request failed with status " + createSessionResponse.getStatusCode());
				log.error(createSessionResponse.getBody());
				throw new ApplicationException("Quickblox User create rquest failed with status."
								+ createSessionResponse.getBody());

			}

		} catch (IOException e) {
			log.error(e);
			log.error("Error in createChatAccount for:");
			log.error("email:" + email);
			log.error("firstname:" + firstname);
			log.error("surname:" + surname);
		}

		return userResponse;
	}

	@Override
	public int getUnreadChatCount(String username, String password) throws IOException {

		RestClient restClient = new RestClient();
		SessionRequest sessionRequest = new SessionRequest();
		sessionRequest.setLogin(username);
		sessionRequest.setPassword(password);

		ResponseEntity<String> createSessionResponse = restClient
						.executeRequest(sessionRequest,
										ChatUrlConfiguration.CHAT_UNREAD_COUNT_CONFIG);

		UnreadMessageCountResponse unreadMessageCountResponse = null;
		if (createSessionResponse.getStatusCode() == HttpStatus.OK) {

			ObjectMapper mapper = new ObjectMapper();
			unreadMessageCountResponse = mapper.readValue(createSessionResponse.getBody(),
							UnreadMessageCountResponse.class);

		} else {
			throw new ApplicationException(createSessionResponse.getBody());

		}

		return unreadMessageCountResponse.getTotal();

	}

	@Override
	public void createPrivateGroup(Integer userId, String username, String password, String chatWithOccupantIds) throws IOException {

		UserChatProfile chatWitPersonProfileDetails = userDAO.getUserChatProfileDetails(Integer.parseInt(chatWithOccupantIds));

		RestClient restClient = new RestClient();
		CreatePrivateGroup createPrivateGroup = new CreatePrivateGroup();
		createPrivateGroup.setGroupOwnerUsername(username);
		createPrivateGroup.setGroupOwnerPassword(password);
		createPrivateGroup.setOccupantsUserProfileIds(String.valueOf(chatWitPersonProfileDetails.getChatProfileId()));

		ResponseEntity<String> createSessionResponse = restClient
						.executeRequest(createPrivateGroup,
										ChatUrlConfiguration.CREATE_PRIVATE_GROUP_CONFIG);

		if (createSessionResponse.getStatusCode() != HttpStatus.OK) {
			log.error("Error in createPrivateGroup for:");
			log.error("username:" + username);
			log.error("chatWithOccupantIds:" + chatWithOccupantIds);
			log.error("userId:" + userId);
			throw new ApplicationException(createSessionResponse.getBody());
		}

		// Insert into user_contact
		SimpleJdbcInsert insertUserContact =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("user_contact")
										.usingColumns("user_id", "contact_user_id")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(4);
		parameters.put("user_id", userId);
		parameters.put("contact_user_id", Integer.parseInt(chatWithOccupantIds));

		insertUserContact.execute(parameters);
		log.info("Created private group on system.");
	}

	@Override
	public void createPublicGroup(Integer userId, String username, String password, String groupName, String chatWithOccupantIds) throws IOException {
		RestClient restClient = new RestClient();
		CreatePublicGroup createPublicGroup = new CreatePublicGroup();
		createPublicGroup.setGroupOwnerUsername(username);
		createPublicGroup.setGroupOwnerPassword(password);
		createPublicGroup.setName(groupName);
		createPublicGroup.setOccupantsUserProfileIds(chatWithOccupantIds);

		ResponseEntity<String> createSessionResponse = restClient
						.executeRequest(createPublicGroup,
										ChatUrlConfiguration.CREATE_GROUP_CONFIG);

		if (createSessionResponse.getStatusCode() != HttpStatus.OK) {
			log.error("Error in createPublicGroup for:");
			log.error("username:" + username);
			log.error("chatWithOccupantIds:" + chatWithOccupantIds);
			log.error("userId:" + userId);
			log.error("groupName:" + groupName);

			throw new ApplicationException(createSessionResponse.getBody());
		}
	}

	@Override
	public void deleteChatGroup(String username, String password, String dialogId) throws IOException {
		RestClient restClient = new RestClient();

		Map<String, String> params = new HashMap<String, String>();
		params.put("dialog_id", dialogId);
		params.put("owner_username", username);
		params.put("owner_password", password);

		restClient.executeDeleteRequest(ChatUrlConfiguration.DELETE_DIALOG_CONFIG, params);

	}

	@Override
	public List<Group> retrieveSuggestedGroup(String defaultRole, Integer entityId, Integer userId) throws AuthenticationException {

		if ("PARENT".equalsIgnoreCase(defaultRole)) {
			return retrieveSuggestedGroupListForParent(entityId, userId);
		} else if ("TEACHER".equalsIgnoreCase(defaultRole)) {
			return retrieveSuggestedGroupListForTeacher(entityId, userId);
		} else if ("STUDENT".equalsIgnoreCase(defaultRole)) {
			return retrieveSuggestedGroupListForStudent(entityId, userId);
		} else {
			throw new AuthenticationException("Function not allowed.");
		}

	}

	@Override
	public List<Contact> searchContact(SearchContact searchContact, Integer userId) {
		String query = RETRIEVE_CONTACT_BY_EMAIL_QUERY;
		if (!StringUtils.isEmpty(searchContact.getPhone())) {
			query = RETRIEVE_CONTACT_BY_PHONE_QUERY;
		}

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("email", searchContact.getEmail());
		queryParameters.put("phone", searchContact.getPhone());
		queryParameters.put("userId", userId);
		log.debug("Query :: " + query);
		log.debug("queryParameters :: " + queryParameters.toString());
		List<Contact> contactList = getNamedParameterJdbcTemplate().query(query, queryParameters,
						new RowMapper<Contact>() {
							@Override
							public Contact mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new Contact(rs.getString("firstname"), rs.getString("lastname"), rs.getString("city"), rs.getString("photo"),
												new Integer(rs.getInt("user_id")), rs.getString("email"), rs.getString("phone"));
							}
						});

		return contactList;

	}

	private List<Group> retrieveSuggestedGroupListForParent(Integer entityId, Integer userId) {

		Parent parent = parentDao.getParent(entityId);
		List<Student> studentList = studentDao.listByParentId(entityId);
		Set<String> groupTags = new HashSet<String>();
		Set<Integer> branchIds = new HashSet<Integer>();

		for (Student student : studentList) {

			String hobbies = student.getOtherDetails().getHobbies();
			String childGoal = student.getOtherDetails().getGoalChild();
			String parentGoal = student.getOtherDetails().getGoalParent();

			Integer branchId = student.getEducationDetails().getBranchId();

			if (branchId != null) {
				branchIds.add(branchId);
			}
			parseTags(groupTags, branchIds, student, hobbies);
			parseTags(groupTags, branchIds, student, childGoal);
			parseTags(groupTags, branchIds, student, parentGoal);

		}

		// Add city and pincode to groups tags
		if (!StringUtils.isEmpty(parent.getCity())) {
			groupTags.add(parent.getCity());
		}

		if (!StringUtils.isEmpty(parent.getPincode())) {
			groupTags.add(parent.getPincode());
		}

		return retrieveGroups(userId, groupTags, branchIds, RETRIEVE_SUGGESTED_GROUPS_QUERY);
	}

	private List<Group> retrieveSuggestedGroupListForStudent(Integer entityId, Integer userId) {

		Student student = studentDao.getStudentProfileDetailsbyId(entityId);

		Set<String> groupTags = new HashSet<String>();
		Set<Integer> branchIds = new HashSet<Integer>();

		if (student != null) {

			String hobbies = student.getOtherDetails().getHobbies();
			String childGoal = student.getOtherDetails().getGoalChild();
			Integer branchId = student.getEducationDetails().getBranchId();

			if (branchId != null) {
				branchIds.add(branchId);
			}
			parseTags(groupTags, branchIds, student, hobbies);
			parseTags(groupTags, branchIds, student, childGoal);

			// Add city and pincode to groups tags
			StudentAddress address = student.getPersonalDetails().getAddress();

			if (address != null && !StringUtils.isEmpty(address.getCity())) {
				groupTags.add(address.getCity());
			}

			if (address != null && !StringUtils.isEmpty(address.getPincode())) {
				groupTags.add(address.getPincode());
			}

		}

		return retrieveGroups(userId, groupTags, branchIds, RETRIEVE_SUGGESTED_GROUPS_QUERY);
	}

	private List<Group> retrieveSuggestedGroupListForTeacher(Integer entityId, Integer userId) {

		Teacher teacher = teacherDao.getDetails(entityId);

		Set<String> groupTags = new HashSet<String>();
		Set<Integer> branchIds = new HashSet<Integer>();

		if (teacher != null) {
			Integer branchId = teacher.getBranchId();

			if (branchId != null) {
				branchIds.add(branchId);
			}

			if (!StringUtils.isEmpty(teacher.getCity())) {
				groupTags.add(teacher.getCity());
			}

			if (!StringUtils.isEmpty(teacher.getPincode())) {
				groupTags.add(teacher.getPincode());
			}

		}

		return retrieveGroups(userId, groupTags, branchIds, RETRIEVE_SUGGESTED_GROUPS_QUERY);
	}

	private List<Group> retrieveGroups(Integer userId, Set<String> groupTags, Set<Integer> branchIds, String query) {
		List<Group> groupList = new ArrayList<Group>();
		if (branchIds.size() > 0 && groupTags.size() > 0) {
			Map<String, Object> queryParameters = new HashMap<String, Object>();
			queryParameters.put("branchIds", branchIds);
			queryParameters.put("group_tags", groupTags);
			queryParameters.put("userId", userId);
			log.debug("Query :: " + query);
			log.debug("queryParameters :: " + queryParameters.toString());
			groupList = getNamedParameterJdbcTemplate().query(query, queryParameters,
							new RowMapper<Group>() {
								@Override
								public Group mapRow(ResultSet rs, int rowNum)
												throws SQLException {
									return new Group(rs.getLong("id"), rs.getString("name"), rs.getString("description"));
								}
							});
		}
		return groupList;
	}

	private void parseTags(Set<String> groupTags, Set<Integer> branchIds, Student s, String value) {
		if (!StringUtils.isEmpty(value)) {
			String[] tags = value.split(",");
			branchIds.add(s.getEducationDetails().getBranchId());
			for (String tag : tags) {
				groupTags.add(tag.toLowerCase().trim());
			}
		}
	}

	private GroupResponse updateChatGroup(ServiceConfiguration serviceConfiguration, Request updateGroupRequest) {

		GroupResponse response = null;

		try {
			RestClient restClient = new RestClient();
			ResponseEntity<String> chatResponse = restClient
							.executeRequest(updateGroupRequest,
											serviceConfiguration);

			if (chatResponse.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				response = mapper.readValue(
								chatResponse.getBody(), GroupResponse.class);

			} else {
				log.error("Update chat group request failed with status " + chatResponse.getStatusCode());
				log.error(chatResponse.getBody());
				throw new ApplicationException("Quickblox rquest failed with status." + chatResponse.getBody());
			}

		} catch (IOException e) {
			log.error(e);
		}

		return response;

	}

	private ChatProfile initiateChatSession(SessionRequest sessionRequest)
					throws IOException {
		RestClient restClient = new RestClient();

		ResponseEntity<String> createSessionResponse = restClient
						.executeRequest(sessionRequest,
										ChatUrlConfiguration.CREATE_SESSION_CONFIG);

		ChatProfile chatProfile = null;
		if (createSessionResponse.getStatusCode() == HttpStatus.OK) {

			ObjectMapper mapper = new ObjectMapper();
			chatProfile = mapper.readValue(createSessionResponse.getBody(),
							ChatProfile.class);

		} else {
			log.error("Login to chat service request failed with status " + createSessionResponse.getStatusCode());
			log.error(createSessionResponse.getBody());
			throw new ApplicationException(createSessionResponse.getBody());

		}

		return chatProfile;
	}

	private ChatGroup retriveChatGroupDetails(Integer groupId) {

		String query = "select g.chat_dialogue_id, ucp.login, ucp.password from `group` g " +
						" inner join user_chat_profile ucp on g.created_by = ucp.user_id " +
						" where g.id = :groupId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("groupId", groupId);

		ChatGroup chatGroup = getNamedParameterJdbcTemplate().queryForObject(query, queryParameters,
						new RowMapper<ChatGroup>() {
							@Override
							public ChatGroup mapRow(ResultSet rs, int rowNum)
											throws SQLException {
								return new ChatGroup(
												rs.getString("chat_dialogue_id"),
												rs.getString("login"),
												rs.getString("password"));
							}
						});

		return chatGroup;

	}

}
