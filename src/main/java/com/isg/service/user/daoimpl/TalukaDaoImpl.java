package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.TalukaDao;
import com.isg.service.user.request.Taluka;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class TalukaDaoImpl extends JdbcDaoSupport implements TalukaDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<Taluka> getTalukas() {
		String retriveTaluka = "select * from taluka where active = 'Y'";
		return getJdbcTemplate().query(retriveTaluka, new BeanPropertyRowMapper<Taluka>(Taluka.class));
	}

	@Override
	public List<Taluka> getTalukasOrCitiesByDistrictId(int id) {
		String retriveTaluka = "select * from taluka where active='Y' and district_id = ?";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().query(retriveTaluka, inputs, new BeanPropertyRowMapper<Taluka>(Taluka.class));
	}

}
