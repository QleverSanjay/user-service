package com.isg.service.user.daoimpl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.client.notification.Notification;
import com.isg.service.user.client.notification.NotificationClient;
import com.isg.service.user.client.notification.Sms;
import com.isg.service.user.dao.FileDao;
import com.isg.service.user.dao.InstituteDao;
import com.isg.service.user.dao.MasterDao;
import com.isg.service.user.dao.OnlineAdmissionDao;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.factory.pdf.onlineadmission.PdfBuilder;
import com.isg.service.user.factory.pdf.onlineadmission.PdfFactory;
import com.isg.service.user.factory.validator.onlineadmission.Validator;
import com.isg.service.user.factory.validator.onlineadmission.ValidatorFactory;
import com.isg.service.user.model.FileAttachment;
import com.isg.service.user.model.OnlineAdmissionCourse;
import com.isg.service.user.model.OnlineAdmissionCourseSubjectPreference;
import com.isg.service.user.model.OnlineAdmissionDocument;
import com.isg.service.user.model.OnlineAdmissionFormDetail;
import com.isg.service.user.model.OnlineAdmissionFormRowMapper;
import com.isg.service.user.model.OnlineAdmissionSeedProgramme;
import com.isg.service.user.model.OnlineAdmissionUrlConfiguration;
import com.isg.service.user.model.OnlineAdmissionUrlConfigurationRowMapper;
import com.isg.service.user.model.Payment;
import com.isg.service.user.model.QualificationExam;
import com.isg.service.user.model.University;
import com.isg.service.user.model.onlineadmission.AcademicYear;
import com.isg.service.user.model.onlineadmission.CandidateDetails;
import com.isg.service.user.model.onlineadmission.ContactDetails;
import com.isg.service.user.model.onlineadmission.Document;
import com.isg.service.user.model.onlineadmission.Name;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.model.onlineadmission.OnlineAdmissionUserDocument;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.Institute;
import com.isg.service.user.request.OnlineAdmissionBasicProfileDetail;
import com.isg.service.user.request.OnlineAdmissionForm;
import com.isg.service.user.request.OnlineAdmissionLoginRequest;
import com.isg.service.user.response.OnlineAdmissionConfiguration;
import com.isg.service.user.response.OnlineAdmissionLoginResponse;
import com.isg.service.user.response.SaveOnlineAdmissionBasicProfileDetailResponse;
import com.isg.service.user.response.SaveOnlineAdmissionFormResponse;
import com.isg.service.user.response.UpdateOnlineAdmissionFormResponse;
import com.isg.service.user.util.Constant.EmailTemplate;
import com.isg.service.user.util.DateFormatter;
import com.isg.service.user.util.DateUtil;
import com.isg.service.user.util.FileStorePathAssembler;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class OnlineAdmissionDaoImpl extends NamedParameterJdbcDaoSupport
		implements OnlineAdmissionDao {

	@Autowired
	private BoneCPDataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	private static final Logger log = Logger
			.getLogger(OnlineAdmissionDaoImpl.class);

	@Autowired
	private NotificationClient notificationClient;

	@Autowired
	private FileDao fileDao;

	@Autowired
	MasterDao masterDao;
	private final SimpleDateFormat USER_DATE_FORMAT = new SimpleDateFormat(
			"dd/MM/yyyy");

	@Autowired
	InstituteDao instituteDao;

	@Autowired
	ValidatorFactory validatorFactory;

	@Override
	public List<OnlineAdmissionCourse> getOnlineAdmissionCourses(
			Integer branchId, String category) {
		String query = "select * from online_admission_course left JOIN online_admission_course_fees as oacf ON oacf.course_id = online_admission_course.id"
				+ "   where branch_id=:branch_id and active = 'Y' ";

		Map<String, Object> parameters = new HashMap<String, Object>(2);
		parameters.put("branch_id", branchId);

		if (!"all".equalsIgnoreCase(category)) {
			query += " and course_category=:course_category ";
			parameters.put("course_category", category);
		}

		query += " order by code";

		List<OnlineAdmissionCourse> onlineAdmissionCourseList = getNamedParameterJdbcTemplate()
				.query(query, parameters,
						new RowMapper<OnlineAdmissionCourse>() {
							@Override
							public OnlineAdmissionCourse mapRow(ResultSet rs,
									int rownumber) throws SQLException {
								OnlineAdmissionCourse onlineAdmissionCourse = new OnlineAdmissionCourse();
								onlineAdmissionCourse.setId(rs.getInt("id"));
								onlineAdmissionCourse.setCode(rs
										.getString("code"));
								onlineAdmissionCourse.setName(rs
										.getString("name"));
								onlineAdmissionCourse.setActive(!StringUtils
										.isEmpty(rs.getString("active"))
										&& rs.getString("active") == "Y" ? true
										: false);
								onlineAdmissionCourse.setCategory(rs
										.getString("course_category"));
								onlineAdmissionCourse.setCoordinator(rs.getString("coordinator"));
								onlineAdmissionCourse.setDate(rs.getString("date"));
								onlineAdmissionCourse.setDuration(rs.getString("duration"));
								onlineAdmissionCourse.setAmount(rs.getDouble("amount"));
								onlineAdmissionCourse.setCurrency(rs.getString("currency"));
								return onlineAdmissionCourse;
							}
						});

		return onlineAdmissionCourseList;
	}

	@Override
	public List<OnlineAdmissionSeedProgramme> getOnlineAdmissionSeedProgramme(
			Integer courseId, boolean optional) {
		String query = "select * from online_admission_course_seed_programme_subject where online_admission_course_id=:courseId "
				+ "and active = 'Y' and optional = :optional order by code";

		Map<String, Object> parameters = new HashMap<String, Object>(2);
		parameters.put("courseId", courseId);
		parameters.put("optional", optional ? "Y" : "N");

		List<OnlineAdmissionSeedProgramme> onlineAdmissionSeedProgrammeList = getNamedParameterJdbcTemplate()
				.query(query, parameters,
						new RowMapper<OnlineAdmissionSeedProgramme>() {

							@Override
							public OnlineAdmissionSeedProgramme mapRow(
									ResultSet rs, int rownumber)
									throws SQLException {
								OnlineAdmissionSeedProgramme onlineAdmissionSeedProgramme = new OnlineAdmissionSeedProgramme();
								onlineAdmissionSeedProgramme.setId(rs
										.getInt("id"));
								onlineAdmissionSeedProgramme.setCode(rs
										.getString("code"));
								onlineAdmissionSeedProgramme.setName(rs
										.getString("name"));
								onlineAdmissionSeedProgramme
										.setActive((!StringUtils.isEmpty(rs
												.getString("active")) && rs
												.getString("active")
												.equals("Y")) ? true : false);
								onlineAdmissionSeedProgramme
										.setOptionl((!StringUtils.isEmpty(rs
												.getString("optional")) && rs
												.getString("optional").equals(
														"Y")) ? true : false);
								return onlineAdmissionSeedProgramme;
							}
						});

		return onlineAdmissionSeedProgrammeList;
	}

	@Override
	public Map<String, List<OnlineAdmissionCourseSubjectPreference>> getOnlineAdmissionSubjectPreference(
			Integer courseId) {
		String query = "select * from online_admission_course_subject_preference where course_id=:courseId and active = 'Y' order by display_order";

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("courseId", courseId);

		return getNamedParameterJdbcTemplate()
				.query(query,
						parameters,
						new ResultSetExtractor<Map<String, List<OnlineAdmissionCourseSubjectPreference>>>() {
							@Override
							public Map<String, List<OnlineAdmissionCourseSubjectPreference>> extractData(
									ResultSet rs) throws SQLException,
									DataAccessException {
								Map<String, List<OnlineAdmissionCourseSubjectPreference>> map = new LinkedHashMap<String, List<OnlineAdmissionCourseSubjectPreference>>();
								List<OnlineAdmissionCourseSubjectPreference> subjectPreferenceList = null;
								while (rs.next()) {
									String key = rs.getString("type");
									if (map.containsKey(key)) {
										subjectPreferenceList = map.get(key);
									} else {
										subjectPreferenceList = new ArrayList<OnlineAdmissionCourseSubjectPreference>();
									}

									OnlineAdmissionCourseSubjectPreference onlineAdmissionCourseSubjectPreference = new OnlineAdmissionCourseSubjectPreference();
									onlineAdmissionCourseSubjectPreference
											.setId(rs.getInt("id"));
									onlineAdmissionCourseSubjectPreference
											.setCode(rs.getString("code"));
									onlineAdmissionCourseSubjectPreference
											.setName(rs.getString("name"));
									onlineAdmissionCourseSubjectPreference
											.setActive((!StringUtils.isEmpty(rs
													.getString("active")) && rs
													.getString("active")
													.equals("Y")) ? true
													: false);
									onlineAdmissionCourseSubjectPreference
											.setType(key);
									subjectPreferenceList
											.add(onlineAdmissionCourseSubjectPreference);
									map.put(key, subjectPreferenceList);
								}

								return map;
							}
						});
	}

	@Override
	public List<QualificationExam> getOnlineAdmissionQualifyingExam(
			Integer branchId) {
		String query = "select * from online_admission_qualifying_exam where branch_id=:branchId and active = 'Y' order by code";

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("branchId", branchId);

		List<QualificationExam> qualificationExamList = getNamedParameterJdbcTemplate()
				.query(query, parameters, new RowMapper<QualificationExam>() {

					@Override
					public QualificationExam mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						QualificationExam qualificationExam = new QualificationExam();
						qualificationExam.setCode(rs.getString("code"));
						qualificationExam.setName(rs.getString("name"));
						qualificationExam.setActive((!StringUtils.isEmpty(rs
								.getString("active")) && rs.getString("active")
								.equals("Y")) ? true : false);

						return qualificationExam;
					}
				});

		return qualificationExamList;
	}

	@Override
	public List<University> getOnlineAdmissionUniversity() {
		String query = "select * from online_admission_exam_university where active = 'Y' order by code";

		List<University> universityList = getNamedParameterJdbcTemplate()
				.query(query, new RowMapper<University>() {

					@Override
					public University mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						University university = new University();
						university.setId(rs.getInt("id"));
						university.setCode(rs.getString("code"));
						university.setName(rs.getString("name"));
						university.setActive((!StringUtils.isEmpty(rs
								.getString("active")) && rs.getString("active")
								.equals("Y")) ? true : false);

						return university;
					}
				});

		return universityList;
	}

	@Override
	public List<OnlineAdmissionDocument> getOnlineAdmissionDocument(
			Integer courseId) {
		String query = "select * from online_admission_document where course_id=:courseId and active = 'Y' order by code";

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("courseId", courseId);

		List<OnlineAdmissionDocument> documentList = getNamedParameterJdbcTemplate()
				.query(query, parameters,
						new RowMapper<OnlineAdmissionDocument>() {

							@Override
							public OnlineAdmissionDocument mapRow(ResultSet rs,
									int rownumber) throws SQLException {
								OnlineAdmissionDocument onlineAdmissionDocument = new OnlineAdmissionDocument();
								onlineAdmissionDocument.setId(rs.getInt("id"));
								onlineAdmissionDocument.setCode(rs
										.getString("code"));
								onlineAdmissionDocument.setName(rs
										.getString("name"));
								onlineAdmissionDocument.setActive((!StringUtils
										.isEmpty(rs.getString("active")) && rs
										.getString("active").equals("Y")) ? true
										: false);
								return onlineAdmissionDocument;
							}
						});

		return documentList;
	}

	@Override
	public OnlineAdmissionUrlConfiguration getOnlineAdmissionDocumentUrlConfiguration(
			String urlIdentifier, String category) {

		OnlineAdmissionUrlConfiguration onlineAdmissionUrlConfiguration = null;

		String query = "select DATE_FORMAT(oauc.from_date, '%d-%m-%Y') as from_date, DATE_FORMAT(oauc.to_date, '%d-%m-%Y') as to_date, "
				+ " b.*, CONCAT(DATE_FORMAT(ay.from_date,'%Y'),'-',DATE_FORMAT(ay.to_date,'%Y')) as academic_year, "
				+ " oauc.id as url_configuration_id, ay.id as academic_year_id "
				+ " from online_admission_url_configuration oauc "
				+ " inner join branch b on b.id = oauc.branch_id "
				+ " inner join academic_year ay on ay.id= oauc.academic_year_id "
				+ " where oauc.url_identifier = :urlIdentifier and oauc.course_category = :category "
				+ " and CURDATE() between oauc.from_date and oauc.to_date";
		
		System.out.println("dsggfd>>>>>>>>>>." + urlIdentifier);
		System.out.println("dsggfd>>>>>>>>>>." + category);
		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put("urlIdentifier", urlIdentifier);
		parameters.put("category", category);
		try {
			onlineAdmissionUrlConfiguration = getNamedParameterJdbcTemplate()
					.queryForObject(query, parameters,
							new OnlineAdmissionUrlConfigurationRowMapper());
			onlineAdmissionUrlConfiguration
					.setOnlineAdmissionConfiguration(getAdmissionFormConfiguration(onlineAdmissionUrlConfiguration
							.getBranch().getId()));
		} catch (Exception e) {
			log.error(
					"Error occured during retrieval of getOnlineAdmissionDocumentUrlConfiguration.",
					e);
		}
		return onlineAdmissionUrlConfiguration;
	}

	@Override
	public SaveOnlineAdmissionBasicProfileDetailResponse saveOnlineAdmissionBasicProfileDetail(
			OnlineAdmissionBasicProfileDetail onlineAdmissionBasicProfileDetail) {
		return saveOnlineAdmissionBasicProfileDetail(
				onlineAdmissionBasicProfileDetail, null);
	}

	@Transactional
	private SaveOnlineAdmissionBasicProfileDetailResponse saveOnlineAdmissionBasicProfileDetail(
			OnlineAdmissionBasicProfileDetail onlineAdmissionBasicProfileDetail,
			AcademicYear academicYear) {

		log.debug("saveOnlineAdmissionBasicProfileDetail received for :::"
				+ onlineAdmissionBasicProfileDetail.toString());

		SaveOnlineAdmissionBasicProfileDetailResponse saveOnlineAdmissionBasicProfileDetailResponse = null;

		Integer urlConfigurationId = onlineAdmissionBasicProfileDetail
				.getUrlConfigurationId();

		OnlineAdmissionUrlConfiguration onlineAdmissionUrlConfiguration = retrieveOnlineAdmissionUrlConfiguration(urlConfigurationId);
		if (onlineAdmissionUrlConfiguration != null) {

			log.debug("onlineAdmissionUrlConfiguration :::"
					+ onlineAdmissionUrlConfiguration.toString());

			Integer academicYearId = (academicYear != null) ? academicYear
					.getId() : onlineAdmissionUrlConfiguration
					.getAcademicYearId();
					String formattedDob=null;
					if(onlineAdmissionBasicProfileDetail.getDob()!=null){
						formattedDob= DateFormatter.formatDate(
								onlineAdmissionBasicProfileDetail.getDob(),
								DateFormatter.DATE_FORMAT_DDMMYYYY_FORWARD_SLASH_SEPARATED,
								DateFormatter.DATE_FORMAT_YYYYMMDD_HYPHEN_SEPARATED);
					}

			String ayUniqueCode = RandomStringUtils.random(5, true,true);

		//	Integer uuid = UUID.randomUUID().clockSequence();

			String applicantId = "AY"
					+ onlineAdmissionUrlConfiguration.getAcademicYear() + "/" + ayUniqueCode;

			String tempAccessSessionId = RandomStringUtils.random(11, true,
					true);

			String username = getApplicantUsername(onlineAdmissionBasicProfileDetail);

			SimpleJdbcInsert insertOnlineAdmission = new SimpleJdbcInsert(
					getJdbcTemplate().getDataSource()).withTableName(
					"online_admission").usingColumns("branch_id",
					"academic_year_id", "applicant_firstname",
					"applicant_middlename", "applicant_surname",
					"applicant_gender", "applicant_dob", "applicant_mobile",
					"applicant_email", "applicant_id",
					"temp_access_session_id", "username");

			Map<String, Object> parameters = new HashMap<String, Object>(10);
			parameters.put("branch_id", onlineAdmissionUrlConfiguration
					.getBranch().getId());
			parameters.put("academic_year_id", academicYearId);
			parameters.put("applicant_firstname",
					onlineAdmissionBasicProfileDetail.getFirstName());
			parameters.put("applicant_middlename",
					onlineAdmissionBasicProfileDetail.getMiddleName());
			parameters.put("applicant_surname",
					onlineAdmissionBasicProfileDetail.getSurname());
			parameters.put("applicant_gender", ""
					+ onlineAdmissionBasicProfileDetail.getGender());
			parameters.put("applicant_dob",
					DateUtil.parseDateInDBFormat(formattedDob));
			parameters.put("applicant_mobile",
					onlineAdmissionBasicProfileDetail.getMobile());
			parameters.put("applicant_email",
					onlineAdmissionBasicProfileDetail.getEmail());
			parameters.put("applicant_id", applicantId);
			parameters.put("temp_access_session_id", tempAccessSessionId);
			parameters.put("username", username);

			insertOnlineAdmission.execute(parameters);

			saveOnlineAdmissionBasicProfileDetailResponse = new SaveOnlineAdmissionBasicProfileDetailResponse();
			saveOnlineAdmissionBasicProfileDetailResponse
					.setApplicantId(applicantId);
			saveOnlineAdmissionBasicProfileDetailResponse
					.setTempAccessId(tempAccessSessionId);

		} else {
			throw new ApplicationException(
					"Invalid url configuration id specified. Unable to process your request.");
		}

		return saveOnlineAdmissionBasicProfileDetailResponse;
	}

	private String getApplicantUsername(
			OnlineAdmissionBasicProfileDetail profileDetail) {
		String firstName = profileDetail.getFirstName();
		String lastName = profileDetail.getSurname();
		String mobile = profileDetail.getMobile();
		String dob = profileDetail.getDob();

		if (!StringUtils.isEmpty(mobile)) {
			return firstName.toLowerCase().concat(".")
					.concat(lastName.toLowerCase()).concat("-")
					.concat(RandomStringUtils.random(4, false, true));
		} else {
			SimpleDateFormat sdf=null;
			if(!StringUtils.isEmpty(dob)){
				sdf= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date date    = null;
			try {
				date = sdf.parse(dob);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			dob = calendar.get(Calendar.DAY_OF_MONTH) + ""
					+ calendar.get(Calendar.MONTH) + ""
					+ calendar.get(Calendar.YEAR);
			
			return firstName.toLowerCase().concat(".")
					.concat(lastName.toLowerCase()).concat("-").concat(dob);
			
			}

			return firstName.toLowerCase().concat(".")
					.concat(lastName.toLowerCase()).concat("-").concat(RandomStringUtils.random(4, false, true));
		}
	}

	private OnlineAdmissionUrlConfiguration retrieveOnlineAdmissionUrlConfiguration(
			Integer urlConfigurationId) {
		OnlineAdmissionUrlConfiguration onlineAdmissionUrlConfiguration = null;

		String query = "select DATE_FORMAT(oauc.from_date, '%d-%m-%Y') as from_date, DATE_FORMAT(oauc.to_date, '%d-%m-%Y') as to_date, "
				+ " b.*, CONCAT(DATE_FORMAT(ay.from_date,'%y'),'-',DATE_FORMAT(ay.to_date,'%y')) as academic_year, "
				+ " oauc.id as url_configuration_id, ay.id as academic_year_id "
				+ " from online_admission_url_configuration oauc "
				+ " inner join branch b on b.id = oauc.branch_id "
				+ " inner join academic_year ay on ay.id= oauc.academic_year_id "
				+ " where oauc.id=:id and CURDATE() between oauc.from_date and oauc.to_date";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("id", urlConfigurationId);

		try {
			onlineAdmissionUrlConfiguration = getNamedParameterJdbcTemplate()
					.queryForObject(query, queryParameters,
							new OnlineAdmissionUrlConfigurationRowMapper());
		} catch (Exception e) {
			// ignore gracefully
			e.printStackTrace();
		}

		return onlineAdmissionUrlConfiguration;

	}

	@Override
	@Transactional
	public SaveOnlineAdmissionFormResponse saveOnlineAdmissionForm(
			OnlineAdmissionForm onlineAdmissionForm)
			throws AuthorisationException, IOException {
		return saveOnlineAdmissionFormWithAttachment(onlineAdmissionForm, null,
				null, true, null);
	}

	@Override
	public OnlineAdmissionLoginResponse login(
			OnlineAdmissionLoginRequest onlineAdmissionLoginRequest)
			throws AuthenticationException {

		OnlineAdmissionFormDetail onlineAdmissionFormDetail = null;
		String query = "select oa.username, CONCAT(DATE_FORMAT(ay.from_date,'%Y'),'-',DATE_FORMAT(ay.to_date,'%Y')) as academic_year, "
				+ " oa.id as application_id, oa.applicant_firstname, oa.applicant_middlename, oa.applicant_surname, oa.applicant_gender, "
				+ " DATE_FORMAT(oa.applicant_dob,'%d-%m-%Y') as applicant_dob, oa.applicant_mobile, oa.applicant_email, "
				+ " oa.applicant_id, '' as applicant_access_password, oa.form_payload, oa.print_pdf_path, oa.photo_path, "
				+ " oa.signature_image_path, b.*, oa.category, IFNULL(oap.status,'UNPAID') as payment_status "
				+ " from online_admission oa "
				+ " inner join branch b on b.id = oa.branch_id "
				+ " inner join academic_year ay on ay.id= oa.academic_year_id "
				+ " left join online_admission_payment oap on oap.applicant_id = oa.applicant_id and oap.status='SUCCESS' "
				+ " where (oa.applicant_id=:applicantId or oa.username=:username) and oa.applicant_access_password=:password and oa.applicant_access_password IS NOT NULL ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("applicantId",
				onlineAdmissionLoginRequest.getApplicantId());
		queryParameters.put("password",
				onlineAdmissionLoginRequest.getPassword());
		queryParameters.put("username",
				onlineAdmissionLoginRequest.getApplicantId());

		try {
			onlineAdmissionFormDetail = getNamedParameterJdbcTemplate()
					.queryForObject(query, queryParameters,
							new OnlineAdmissionFormRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			throw new AuthenticationException(
					"Invalid applicant id / access code specified. Unable to process your request.");
		}

		String sessionToken = updateApplicationFormToken(onlineAdmissionFormDetail
				.getId());

		OnlineAdmissionLoginResponse onlineAdmissionLoginResponse = new OnlineAdmissionLoginResponse();
		onlineAdmissionLoginResponse
				.setOnlineAdmissionConfiguration(getAdmissionFormConfiguration(onlineAdmissionFormDetail
						.getBranch().getId()));
		onlineAdmissionLoginResponse
				.setOnlineAdmissionFormDetail(onlineAdmissionFormDetail);
		onlineAdmissionLoginResponse.setToken(sessionToken);
		onlineAdmissionLoginResponse.setPaymentStatus(onlineAdmissionFormDetail
				.getPaymentStatus().equalsIgnoreCase("SUCCESS") ? "PAID"
				: "UNPAID");
		return onlineAdmissionLoginResponse;

	}

	@Override
	public OnlineAdmissionConfiguration getAdmissionFormConfiguration(
			final Integer branchId) {
		String query = "select oac.*, oauc.home_page_relative_url "
				+ " from online_admission_configuration as oac "
				+ " INNER JOIN online_admission_url_configuration as oauc on oauc.branch_id = oac.branch_id "
				+ " where oac.branch_id = " + branchId + " limit 1";

		System.out.println("Retriving data");

		List<OnlineAdmissionConfiguration> configurations = getJdbcTemplate()
				.query(query, new RowMapper<OnlineAdmissionConfiguration>() {
					@Override
					public OnlineAdmissionConfiguration mapRow(ResultSet rs,
							int arg1) throws SQLException {
						OnlineAdmissionConfiguration configuration = new OnlineAdmissionConfiguration();
						String homePageRelativeUrl = rs
								.getString("home_page_relative_url");
						int indexOfFirstSlash = homePageRelativeUrl
								.indexOf("/");
						String admissionFormFor = homePageRelativeUrl
								.substring(indexOfFirstSlash + 1,
										homePageRelativeUrl.indexOf("/",
												indexOfFirstSlash + 1));
						configuration.setAdmissionFormEditEnabledForStudent(rs
								.getString("admission_form_edit_enabled_for_student"));
						configuration.setAllowToSpecifyCustomAdmissionFeesAmount(rs
								.getString("allow_to_specify_custom_admission_fees_amount"));
						configuration.setAdmissionFormRedirectToPayment(rs
								.getString("admission_form_redirect_to_payment"));
						configuration.setAdmissionFormPaymentEnabled(rs
								.getString("admission_form_payment_enabled"));
						configuration.setAdmissionFormFor(admissionFormFor);
						configuration.setBranchId(branchId);
						return configuration;
					}
				});

		return configurations.isEmpty() ? null : configurations.get(0);
	}

	@Override
	@Transactional
	public UpdateOnlineAdmissionFormResponse updateOnlineAdmissionForm(
			OnlineAdmissionForm onlineAdmissionForm,
			FileAttachment photoFileAttachment,
			FileAttachment signatureFileAttachment)
			throws AuthorisationException, IOException {

		OnlineAdmissionFormDetail onlineAdmissionFormDetail = retrieveOnlineFormDetail(onlineAdmissionForm
				.getTempAccessId());
		// Override form payload with latest submitted by user
		onlineAdmissionFormDetail.setFormPayload(onlineAdmissionForm
				.getFormPayload());
		updateApplicationForm(onlineAdmissionFormDetail, photoFileAttachment,
				signatureFileAttachment, true, false);
		String token = updateApplicationFormToken(onlineAdmissionFormDetail
				.getId());

		System.out.println("Token after form updated:::> " + token);
		UpdateOnlineAdmissionFormResponse updateOnlineAdmissionFormResponse = new UpdateOnlineAdmissionFormResponse();
		updateOnlineAdmissionFormResponse.setToken(token);

		onlineAdmissionFormDetail = retrieveOnlineFormDetails(onlineAdmissionFormDetail
				.getId());
		updateOnlineAdmissionFormResponse.setPayload(onlineAdmissionForm
				.getFormPayload());
		updateOnlineAdmissionFormResponse.setPhotoUrl(onlineAdmissionFormDetail
				.getPhotoPath());
		updateOnlineAdmissionFormResponse
				.setSignatureUrl(onlineAdmissionFormDetail.getSignaturePath());
		updateOnlineAdmissionFormResponse.setPayload(onlineAdmissionFormDetail
				.getFormPayload());
		return updateOnlineAdmissionFormResponse;
	}

	@Override
	public OnlineAdmissionFormDetail retrieveOnlineFormDetail(String token)
			throws AuthorisationException {
		Integer applicationFormId = retrieveApplicationFormId(token);
		return retrieveOnlineFormDetails(applicationFormId);
	}

	@Override
	public OnlineAdmissionLoginResponse getFormDetailsForAdmin(
			String applicationId) throws AuthenticationException {

		OnlineAdmissionFormDetail onlineAdmissionFormDetail = null;
		String query = "select oa.username, CONCAT(DATE_FORMAT(ay.from_date,'%Y'),'-',DATE_FORMAT(ay.to_date,'%Y')) as academic_year, "
				+ " oa.id as application_id, oa.applicant_firstname, oa.applicant_middlename, oa.applicant_surname, oa.applicant_gender, "
				+ " DATE_FORMAT(oa.applicant_dob,'%d-%m-%Y') as applicant_dob, oa.applicant_mobile, oa.applicant_email, "
				+ " oa.applicant_id, '' as applicant_access_password, oa.form_payload, oa.print_pdf_path, oa.photo_path, "
				+ " oa.signature_image_path, b.*, oa.category, IFNULL(oap.status,'UNPAID') as payment_status "
				+ " from online_admission oa "
				+ " inner join branch b on b.id = oa.branch_id "
				+ " inner join academic_year ay on ay.id= oa.academic_year_id "
				+ " left join online_admission_payment oap on oap.applicant_id = oa.applicant_id and oap.status='SUCCESS' "
				+ " where oa.applicant_id=:applicationId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("applicationId", applicationId);
		try {
			onlineAdmissionFormDetail = getNamedParameterJdbcTemplate().queryForObject(query, queryParameters, new OnlineAdmissionFormRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			throw new AuthenticationException("Invalid applicant id / access code specified. Unable to process your request.");
		}

		String sessionToken = updateApplicationFormToken(onlineAdmissionFormDetail.getId());

		OnlineAdmissionLoginResponse onlineAdmissionLoginResponse = new OnlineAdmissionLoginResponse();
		OnlineAdmissionConfiguration configuration = getAdmissionFormConfiguration(onlineAdmissionFormDetail.getBranch().getId());
		configuration.setAdmissionFormEditEnabledForStudent("Y");
		onlineAdmissionLoginResponse.setOnlineAdmissionConfiguration(configuration);
		onlineAdmissionLoginResponse.setOnlineAdmissionFormDetail(onlineAdmissionFormDetail);
		onlineAdmissionLoginResponse.setToken(sessionToken);
		onlineAdmissionLoginResponse.setPaymentStatus(onlineAdmissionFormDetail.getPaymentStatus().equalsIgnoreCase("SUCCESS") ? "PAID" : "UNPAID");
		return onlineAdmissionLoginResponse;

	}

	private OnlineAdmissionFormDetail retrieveOnlineFormDetails(
			Integer applicationFormId) {
		String query = "select oa.username, CONCAT(DATE_FORMAT(ay.from_date,'%Y'),'-',DATE_FORMAT(ay.to_date,'%Y')) as academic_year, "
				+ " oa.id as application_id, oa.applicant_firstname, oa.applicant_middlename, oa.applicant_surname, oa.applicant_gender, "
				+ " DATE_FORMAT(oa.applicant_dob,'%d-%m-%Y') as applicant_dob, oa.applicant_mobile, oa.applicant_email, "
				+ " oa.applicant_id, oa.applicant_access_password, oa.form_payload, oa.print_pdf_path, oa.photo_path, oa.signature_image_path,"
				+ " IFNULL(oap.status,'UNPAID') as payment_status, b.*, oa.category "
				+ " from online_admission oa "
				+ " inner join branch b on b.id = oa.branch_id "
				+ " inner join academic_year ay on ay.id= oa.academic_year_id "
				+ " left join online_admission_payment oap on oap.applicant_id = oa.applicant_id and oap.status='SUCCESS' "
				+ " where oa.id=:id ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("id", applicationFormId);

		OnlineAdmissionFormDetail onlineAdmissionFormDetail = getAdmissionFormDetail(query, queryParameters);
		return onlineAdmissionFormDetail;
	}

	private SaveOnlineAdmissionFormResponse updateApplicationForm(
			OnlineAdmissionFormDetail onlineAdmissionFormDetail,
			FileAttachment photoFileAttachment,
			FileAttachment signatureFileAttachment,
			boolean createAdmissionFormPdf, boolean isFormInsert)
			throws IOException {

		SaveOnlineAdmissionFormResponse saveOnlineAdmissionFormResponse = null;

		if (onlineAdmissionFormDetail != null) {

			ObjectMapper mapper = new ObjectMapper();
			OnlineAdmission onlineAdmission = null;

			try {
				onlineAdmission = mapper.readValue(onlineAdmissionFormDetail
						.getFormPayload().replaceAll("\\\\", ""),
						OnlineAdmission.class);
			} catch (Exception e) {
				log.error(e);
			}

			logger.debug("Object to Update :::>>>"
					+ mapper.writeValueAsString(onlineAdmission));
			if (onlineAdmission == null) {
				throw new ApplicationException("Please check form data submitted, unable to parse json.");
			}
			ContactDetails contactDetails = onlineAdmission.getContactDetails();
			boolean isEmailChanged=false;
			boolean isMobileChanged =false;
			if(!StringUtils.isEmpty(onlineAdmissionFormDetail.getEmail())){
			isEmailChanged= !onlineAdmissionFormDetail.getEmail()
					.equals(contactDetails.getPersonalEmail());
			
			}
			
			if(!StringUtils.isEmpty(onlineAdmissionFormDetail.getEmail())){
				isMobileChanged = !onlineAdmissionFormDetail.getMobile()
					.equals(contactDetails.getPersonalMobileNumber());
			}
			if (!isFormInsert) {
				onlineAdmissionFormDetail.setEmail(contactDetails.getPersonalEmail());
				onlineAdmissionFormDetail.setMobile(contactDetails.getPersonalMobileNumber());
			}
			if ("Y".equals(onlineAdmission.getSchoolVerified())) {
				onlineAdmission.setVerifiedAt(USER_DATE_FORMAT
						.format(new Date()));
			} else {
				onlineAdmission.setVerifiedAt(null);
			}
			logger.debug("Object to Update FormDETAIL:::>>>" + onlineAdmissionFormDetail);
			onlineAdmission.setAcademicYear(onlineAdmissionFormDetail
					.getAcademicYear());
			String templateFactoryName = getExportTemplateFactoryName(
					onlineAdmissionFormDetail.getBranch().getId(),
					onlineAdmission.getCourseDetails().getCourse().getId()
							.intValue());

			OnlineAdmissionConfiguration admissionConfiguration = getAdmissionFormConfiguration(onlineAdmissionFormDetail
					.getBranch().getId());

			Validator validator = validatorFactory
					.getPdfBuilderInstance(templateFactoryName);
			onlineAdmission.setAdmissionId(onlineAdmissionFormDetail.getId());
			
			if (validator != null && isFormInsert) {
				validator.validate(onlineAdmission, admissionConfiguration);
			}

			String pdfFilePath = onlineAdmissionFormDetail.getPdfFilePath();
			String signatureImagePath = onlineAdmissionFormDetail
					.getSignaturePath();
			String photoPath = onlineAdmissionFormDetail.getPhotoPath();

			String onlineRegistrationFormDocBase = FileStorePathAssembler
					.getOnlineRegistrationFormDocBaseRelativePath(onlineAdmissionFormDetail
							.getBranch().getId());

			PdfBuilder pdfBuilder = PdfFactory
					.getPdfBuilderInstance(templateFactoryName);

			if (pdfBuilder != null && StringUtils.isEmpty(pdfFilePath)) {
				String pdfDocumentName = RandomStringUtils.random(20, true,
						true);
				pdfFilePath = FileStorePathAssembler
						.getOnlineRegistrationFormDocBaseRelativePath(onlineAdmissionFormDetail
								.getBranch().getId())
						+ pdfDocumentName + ".pdf";
				log.debug("pdfFilePath >>>" + pdfFilePath);
			}

			if (StringUtils.isEmpty(signatureImagePath)
					&& signatureFileAttachment != null) {
				String signatureFileExtension = FileStorePathAssembler
						.getFileExtension(signatureFileAttachment
								.getFileNameWithExtension());
				String signatureFileNameWithExtension = RandomStringUtils
						.random(20, true, true) + "." + signatureFileExtension;
				signatureImagePath = onlineRegistrationFormDocBase
						+ signatureFileNameWithExtension;
				log.debug("signatureImagePath >>>" + signatureImagePath);
			}

			if (signatureFileAttachment != null) {
				fileDao.saveFile(
						FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH
								+ signatureImagePath,
						signatureFileAttachment.getBytes());
			}

			if (StringUtils.isEmpty(photoPath) && photoFileAttachment != null) {
				String photoFileExtension = FileStorePathAssembler
						.getFileExtension(photoFileAttachment
								.getFileNameWithExtension());
				String photoFileNameWithExtension = RandomStringUtils.random(
						20, true, true) + "." + photoFileExtension;
				photoPath = onlineRegistrationFormDocBase
						+ photoFileNameWithExtension;
				log.debug("photoPath >>>" + photoPath);
			}

			if (photoFileAttachment != null) {
				fileDao.saveFile(
						FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH
								+ photoPath, photoFileAttachment.getBytes());
			}

			String password = onlineAdmissionFormDetail.getPassword();

			if (StringUtils.isEmpty(password)) {
				password = RandomStringUtils.random(8, true, true);
			}

			// Integer formNumber = masterDao.generateSerialNumber(1,
			// onlineAdmissionFormDetail.getBranch().getId(), "rosemine");
			String updateAdmissionFormQuery = "update online_admission set applicant_access_password = :password, form_payload = :payload, updated_date = now(),  "
					+ " print_pdf_path= :pdfFilePath, photo_path=:photoPath, signature_image_path=:signatureImagePath, "
					+ " online_admission_course_id =:courseId, form_number =:formNumber, unique_identifier = :uniqueIdentifier, category=:category";

			if (!isFormInsert && (isMobileChanged || isEmailChanged)) {
				updateAdmissionFormQuery += ", applicant_email = :applicantEmail, applicant_mobile = :applicantMobile";
			}

			updateAdmissionFormQuery += " where id=:id";
			Map<String, Object> queryParameters = new HashMap<String, Object>();
			queryParameters.put("id", onlineAdmissionFormDetail.getId());
			queryParameters.put("password", password);
			queryParameters.put("payload", mapper.writeValueAsString(onlineAdmission));
			queryParameters.put("pdfFilePath", pdfFilePath);
			queryParameters.put("photoPath", photoPath);
			queryParameters.put("signatureImagePath", signatureImagePath);
			queryParameters.put("courseId", onlineAdmission.getCourseDetails().getCourse().getId());
			queryParameters.put("formNumber", null);
			queryParameters.put("uniqueIdentifier", onlineAdmission.getUniqueIdentifier());
			queryParameters.put("applicantEmail", onlineAdmission.getContactDetails().getPersonalEmail());
			queryParameters.put("applicantMobile", onlineAdmission.getContactDetails().getPersonalMobileNumber());
			String category = onlineAdmission.getCategory();
			if(StringUtils.isEmpty(category)){
				category = "INR";
			}
			queryParameters.put("category", category);

			int UPDATE_STATUS = getNamedParameterJdbcTemplate().update(
					updateAdmissionFormQuery, queryParameters);

			if (UPDATE_STATUS == 1) {

				onlineAdmission
						.setBranch(onlineAdmissionFormDetail.getBranch());
				log.debug("onlineAdmissionFormDetail.getBranch() instituteid >>>"
						+ onlineAdmissionFormDetail.getBranch()
								.getInstituteId());
				Institute institue = instituteDao.getById(onlineAdmissionFormDetail.getBranch().getInstituteId());
				onlineAdmission.setApplicationId(onlineAdmissionFormDetail.getApplicantId());

				log.debug("institue.getLogo_url() >>>"+ FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH+ institue.getLogo_url());

				if (pdfBuilder != null) {
					if (!StringUtils.isEmpty(photoPath)) {
						photoPath = FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH + photoPath;
					}

					if (!StringUtils.isEmpty(signatureImagePath)) {
						signatureImagePath = FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH + signatureImagePath;
					}

					pdfBuilder.createPdf(onlineAdmission, FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH + pdfFilePath, photoPath,
							signatureImagePath, FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH + institue.getLogo_url());
				}
				// Send notification
				onlineAdmissionFormDetail.setPassword(password);

				String redirectionUrlRelativePath = null;

				if (!StringUtils.isEmpty(onlineAdmissionFormDetail.getEmail()) || !StringUtils.isEmpty(onlineAdmissionFormDetail.getMobile())) {
					redirectionUrlRelativePath = getRedirectionPath(onlineAdmission .getUrlConfigurationId());
				}

				if (!StringUtils.isEmpty(onlineAdmissionFormDetail.getEmail())
						&& (isFormInsert || (!isFormInsert && isEmailChanged))) {
					// send email only for dalmiya
					if (onlineAdmissionFormDetail.getBranch().getId() == 101) {

						sendEmailNotification(onlineAdmissionFormDetail,
								pdfFilePath, redirectionUrlRelativePath,
								onlineAdmission.getCourseDetails().getCourse()
										.getName(), admissionConfiguration);

					}

				}

				if (!StringUtils.isEmpty(onlineAdmissionFormDetail.getMobile())
						&& (isFormInsert || (!isFormInsert && isMobileChanged))) {
					sendSMSNotification(onlineAdmissionFormDetail, pdfFilePath,
							redirectionUrlRelativePath, admissionConfiguration);
				}

				saveOnlineAdmissionFormResponse = new SaveOnlineAdmissionFormResponse();
				saveOnlineAdmissionFormResponse
						.setApplicantId(onlineAdmissionFormDetail
								.getApplicantId());
				saveOnlineAdmissionFormResponse.setPassword(password);

			}

		} else {
			throw new ApplicationException(
					"Invalid applicant id / access code specified. Unable to process your request.");
		}

		return saveOnlineAdmissionFormResponse;
	}

	private Integer retrieveApplicationFormId(String token)
			throws AuthorisationException {
		Integer applicationFormId = null;

		String query = "select * from online_admission_session_token where session_token = :token ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("token", token);
		try {
			applicationFormId = getNamedParameterJdbcTemplate().queryForObject(
					query, queryParameters, new RowMapper<Integer>() {

						@Override
						public Integer mapRow(ResultSet rs, int rownumber)
								throws SQLException {
							return rs.getInt("online_admission_id");
						}
					});
		} catch (Exception e) {
			throw new AuthorisationException("Invalid token supplied.");
		}

		return applicationFormId;

	}

	private String updateApplicationFormToken(Integer applicationFormId) {

		String sessionToken = RandomStringUtils.random(20, true, true);

		String query = " INSERT INTO online_admission_session_token (online_admission_id, session_token, last_accessed_date_time) "
				+ " VALUES(:applicationFormId, :sessionToken, now()) ON DUPLICATE KEY UPDATE "
				+ " online_admission_id = VALUES(online_admission_id), session_token = VALUES(session_token), last_accessed_date_time = VALUES(last_accessed_date_time)";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("applicationFormId", applicationFormId);
		queryParameters.put("sessionToken", sessionToken);

		int UPDATE_STATUS = getNamedParameterJdbcTemplate().update(query,
				queryParameters);
		log.debug("updateApplicationFormToken UPDATE_STATUS >>" + UPDATE_STATUS);
		if (UPDATE_STATUS >= 1) {
			return sessionToken;

		} else {
			throw new ApplicationException(
					"Error occurred while updating session token, reverting the transaction. Please try again.");
		}

	}

	private OnlineAdmissionFormDetail getAdmissionFormDetail(String query,
			Map<String, Object> queryParameters) {
		OnlineAdmissionFormDetail onlineAdmissionFormDetail = null;

		try {
			log.debug("getAdmissionFormDetail query ::" + query);
			log.debug("getAdmissionFormDetail queryParameters ::"
					+ queryParameters);
			onlineAdmissionFormDetail = getNamedParameterJdbcTemplate()
					.queryForObject(query, queryParameters, new OnlineAdmissionFormRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return onlineAdmissionFormDetail;

	}

	private void sendSMSNotification(
			OnlineAdmissionFormDetail onlineAdmissionFormDetail,
			String pdfFilePath, String redirectionUrlRelativePath,
			OnlineAdmissionConfiguration admissionConfiguration) {

		Map<String, Object> content = new HashMap<String, Object>();
		content.put("first_name", onlineAdmissionFormDetail.getFirstName());
		content.put("middle_name", onlineAdmissionFormDetail.getMiddleName());
		content.put("sur_name", onlineAdmissionFormDetail.getSurname());
		content.put("user_id", onlineAdmissionFormDetail.getUsername());
		content.put("password", onlineAdmissionFormDetail.getPassword());
		content.put("admission_url", redirectionUrlRelativePath);
		content.put("institute_name", onlineAdmissionFormDetail.getBranch()
				.getName());
		content.put("payment_enabled",
				admissionConfiguration.getAdmissionFormPaymentEnabled());

		System.out.println(content);
		Sms sms = new Sms();
		sms.setType("SMS");
		sms.setContactNumber(onlineAdmissionFormDetail.getMobile());
		sms.setTemplateCode(EmailTemplate.ADMISSION_SUCCESS_SMS.get());
		sms.setSmsContent(content);
		try {
			String jsonSend = new ObjectMapper().writeValueAsString(sms);
			log.debug("SMS JSON >>>" + jsonSend);
			notificationClient.sendMessage(jsonSend);
		} catch (Exception e) {
			log.error(e);
		}

		/*
		 * Notification notification = new Notification();
		 * notification.setTemplateCode
		 * (EmailTemplate.ADMISSION_SUCCESS_SMS.get());
		 * 
		 * Map<String, Object> subjectContent = new HashMap<String, Object>();
		 * subjectContent.put("institute_name",
		 * onlineAdmissionFormDetail.getBranch().getName());
		 * 
		 * notification.setContent(content); notification.setType("SMS");
		 * notification.setTo(onlineAdmissionFormDetail.getMobile());
		 * 
		 * notification.setSubjectContent(subjectContent);
		 * 
		 * try { String jsonSend = new
		 * ObjectMapper().writeValueAsString(notification);
		 * //System.out.println(jsonSend);
		 * notificationClient.sendMessage(jsonSend); } catch (Exception e) {
		 * log.error(e); }
		 */

	}

	private void sendEmailNotification(
			OnlineAdmissionFormDetail onlineAdmissionFormDetail,
			String pdfFilePath, String redirectionUrlRelativePath,
			String courseName,
			OnlineAdmissionConfiguration admissionConfiguration) {
		Notification notification = new Notification();
		notification.setTemplateCode(EmailTemplate.ADMISSION_SUCCESS_EMAIL
				.get());
		Map<String, Object> subjectContent = new HashMap<String, Object>();
		subjectContent.put("institute_name", onlineAdmissionFormDetail
				.getBranch().getName());
		subjectContent.put("address_line", onlineAdmissionFormDetail
				.getBranch().getAddressLine());
		subjectContent.put("city", onlineAdmissionFormDetail.getBranch()
				.getCity());
		subjectContent.put("applicant_id",
				onlineAdmissionFormDetail.getApplicantId());

		Map<String, Object> content = new HashMap<String, Object>();
		content.put("first_name", onlineAdmissionFormDetail.getFirstName());
		content.put("middle_name", onlineAdmissionFormDetail.getMiddleName());
		content.put("sur_name", onlineAdmissionFormDetail.getSurname());
		content.put("user_id", onlineAdmissionFormDetail.getApplicantId());
		content.put("password", onlineAdmissionFormDetail.getPassword());
		content.put("admission_url", redirectionUrlRelativePath);
		content.put("payment_enabled",
				admissionConfiguration.getAdmissionFormPaymentEnabled());

		notification.setContent(content);
		notification.setType("EMAIL");
		notification.setTo(onlineAdmissionFormDetail.getEmail());

		if (!StringUtils.isEmpty(pdfFilePath)) {
			List<String> attachments = new ArrayList<>();
			attachments.add(FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH
					+ pdfFilePath);
			notification.setAttachments(attachments);
		}

		notification.setSubjectContent(subjectContent);

		try {

			String jsonSend = new ObjectMapper()
					.writeValueAsString(notification);
			notificationClient.sendMessage(jsonSend);

			// Send another notification to branch email
			/*
			 * if (!StringUtils.isEmpty(onlineAdmissionFormDetail.getBranch()
			 * .getContactemail())) { content = new HashMap<String, Object>();
			 * content.put("applicant_id",
			 * onlineAdmissionFormDetail.getApplicantId());
			 * content.put("course", courseName);
			 * notification.setContent(content); notification
			 * .setTemplateCode(EmailTemplate.ADMISSION_FORM_RECEIPT_EMAIL
			 * .get()); notification.setTo(onlineAdmissionFormDetail.getBranch()
			 * .getContactemail()); jsonSend = new
			 * ObjectMapper().writeValueAsString(notification);
			 * notificationClient.sendMessage(jsonSend); }
			 */// Has been commented by Pitabas on 24-AUG-17 to stop sending
				// mails to school
		} catch (Exception e) {
			log.error(e);
		}

	}

	@Override
	@Transactional
	public SaveOnlineAdmissionFormResponse saveOnlineAdmissionFormWithAttachment(
			OnlineAdmissionForm onlineAdmissionForm,
			FileAttachment photoFileAttachment,
			FileAttachment signatureFileAttachment,
			boolean createAdmissionFormPdf, List<Document> documents)
			throws AuthorisationException, IOException {
		String query = "select oa.username, CONCAT(DATE_FORMAT(ay.from_date,'%Y'),'-',DATE_FORMAT(ay.to_date,'%Y')) as academic_year, "
				+ " oa.id as application_id, oa.applicant_firstname, oa.applicant_middlename, oa.applicant_surname, oa.applicant_gender, "
				+ " DATE_FORMAT(oa.applicant_dob,'%d-%m-%Y') as applicant_dob, oa.applicant_mobile, oa.applicant_email, "
				+ " oa.applicant_id, oa.applicant_access_password, oa.form_payload, oa.print_pdf_path, oa.photo_path, oa.signature_image_path, "
				+ " IFNULL(oap.status,'UNPAID') as payment_status, b.*, oa.category"
				+ " from online_admission oa "
				+ " inner join branch b on b.id = oa.branch_id "
				+ " inner join academic_year ay on ay.id= oa.academic_year_id "
				+ " left join online_admission_payment oap on oap.applicant_id = oa.applicant_id and oap.status='SUCCESS' "
				+ " where oa.applicant_id=:applicantId and oa.temp_access_session_id=:tempAccessId and oa.applicant_access_password IS NULL ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters
				.put("applicantId", onlineAdmissionForm.getApplicantId());
		queryParameters.put("tempAccessId",
				onlineAdmissionForm.getTempAccessId());

		OnlineAdmissionFormDetail onlineAdmissionFormDetail = getAdmissionFormDetail(query, queryParameters);
		if (onlineAdmissionFormDetail != null) {

			onlineAdmissionFormDetail.setFormPayload(onlineAdmissionForm
					.getFormPayload());
			log.debug("onlineAdmissionFormDetail payload"
					+ onlineAdmissionFormDetail.getFormPayload());
			saveAllDocuments(documents, onlineAdmissionFormDetail.getBranch(),
					onlineAdmissionFormDetail.getId());
			return updateApplicationForm(onlineAdmissionFormDetail,
					photoFileAttachment, signatureFileAttachment,
					createAdmissionFormPdf, true);

		} else {
			throw new AuthorisationException("Invalid token supplied.");
		}

	}

	private void saveAllDocuments(List<Document> documents, Branch branch,
			Integer onlineAdmissionId) throws IOException {
		if (documents != null) {
			String sql = "insert into online_admission_user_document values (?, ?, ?)";
			for (Document doc : documents) {
				MultipartFile file = doc.getFile();
				if (file != null) {
					String fileName = file.getOriginalFilename();
					fileName = RandomStringUtils.random(10, true, true)
							+ fileName.substring(fileName.lastIndexOf("."),
									fileName.length());
					String path = "/home/userser/admission_documents/private/"
							+ branch.getInstituteId() + "/" + branch.getId()
							+ "/" + fileName;
					fileDao.saveFile(path, file.getBytes());
					doc.setFilePath(path);

					Object[] data = { onlineAdmissionId, doc.getId(), path };
					int[] argTypes = new int[] { Types.INTEGER, Types.INTEGER,
							Types.VARCHAR };
					getJdbcTemplate().update(sql, data, argTypes);
				}
			}
		}
	}

	@Override
	public List<OnlineAdmission> exportAdmissionReport(Integer branchId,
			Integer courseId, Integer academicYearId, boolean isExportReport) {

		String query = "select oa.id as admissionId, oa.created_date, oa.applicant_id as applicant_id, oa.form_payload, oap.qfix_reference_number, oap.total_amount, oap.status as payment_status, oap.mode_of_payment, "
				+ " DATE_FORMAT(oap.paid_date,'%d-%m-%Y') as paid_date "
				+ " from online_admission oa left outer join online_admission_payment oap on oap.applicant_id=oa.applicant_id and oap.status='SUCCESS' "
				+ " where oa.branch_id=:branchId and oa.online_admission_course_id=:courseId and oa.academic_year_id =:academicYearId ";
		
		System.out.println("Generating online admission report -->\t"+query);

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("branchId", branchId);
		queryParameters.put("courseId", courseId);
		queryParameters.put("academicYearId", academicYearId);

		final ObjectMapper mapper = new ObjectMapper();

		List<OnlineAdmission> onlineAdmissionList = getNamedParameterJdbcTemplate()
				.query(query, queryParameters,
						new RowMapper<OnlineAdmission>() {
							OnlineAdmission onlineAdmission = null;

							@Override
							public OnlineAdmission mapRow(ResultSet rs,
									int rownumber) throws SQLException {
								try {
									String payload = rs
											.getString("form_payload");

									onlineAdmission = !StringUtils
											.isEmpty(payload) ? (mapper
											.readValue(payload.replaceAll(
													"\\\\", ""),
													OnlineAdmission.class))
											: null;

									if (onlineAdmission != null) {
										onlineAdmission.setQfixReferenceNumber(rs
												.getString("qfix_reference_number"));
										onlineAdmission.setAmount(rs
												.getString("total_amount"));
										onlineAdmission.setPaymentStatus(StringUtils.isEmpty(rs
												.getString("payment_status")) ? "UNPAID"
												: "PAID");
										onlineAdmission.setPaymentDate(rs
												.getString("paid_date"));
										onlineAdmission.setModeOfPayment(rs
												.getString("mode_of_payment"));
										onlineAdmission.setApplicationId(rs
												.getString("applicant_id"));
										onlineAdmission.setCreatedAt(USER_DATE_FORMAT.format(rs
												.getDate("created_date")));
										onlineAdmission.setAdmissionId(rs
												.getInt("admissionId"));
									}

									return onlineAdmission;
								} catch (IOException e) {
									e.printStackTrace();
									return null;
								}
							}
						});

		log.debug("onlineAdmissionList size" + onlineAdmissionList.size());
		return onlineAdmissionList;
	}

	@Override
	public SaveOnlineAdmissionFormResponse saveOnlineAdmissionSingleStep(
			String onlineAdmissionFormStr, FileAttachment photoFileAttachment,
			FileAttachment signatureFileAttachment,
			List<MultipartFile> documents) throws AuthorisationException,
			IOException {

		OnlineAdmission onlineAdmission = null;
		int index = 0;
		try {
			ObjectMapper mapper = new ObjectMapper();

			onlineAdmission = mapper.readValue(
					onlineAdmissionFormStr.replaceAll("\\\\", ""),
					OnlineAdmission.class);
			for (MultipartFile file : documents) {
				Document doc = onlineAdmission.getDocuments().get(index);
				doc.setFile(file);
				index++;
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException(
					"Please check form data submitted, unable to parse json.",
					e);
		}

		// onlineAdmission.setFormNumber("RM/2017-18/5001");

		OnlineAdmissionBasicProfileDetail onlineAdmissionBasicProfileDetail = new OnlineAdmissionBasicProfileDetail();
		CandidateDetails candidateDetails = onlineAdmission
				.getCandidateDetails();
		ContactDetails contactDetails = onlineAdmission.getContactDetails();
		Name candidateName = candidateDetails.getName();

		onlineAdmissionBasicProfileDetail.setFirstName(candidateName
				.getFirstname());
		onlineAdmissionBasicProfileDetail.setMiddleName(!StringUtils
				.isEmpty(candidateName.getMiddlename()) ? candidateName
				.getMiddlename() : null);
		onlineAdmissionBasicProfileDetail.setSurname(!StringUtils
				.isEmpty(candidateName.getSurname()) ? candidateName
				.getSurname() : null);
		onlineAdmissionBasicProfileDetail.setDob(!StringUtils
				.isEmpty(candidateDetails.getDateofbirth()) ? candidateDetails
				.getDateofbirth() : null);
		System.out.print("DateOFBIrt***************************"
				+ candidateDetails.getDateofbirth());

		onlineAdmissionBasicProfileDetail.setEmail(contactDetails
				.getPersonalEmail());
		onlineAdmissionBasicProfileDetail.setMobile(contactDetails
				.getPersonalMobileNumber());
		if(!StringUtils.isEmpty(candidateDetails.getGender())){
	  		onlineAdmissionBasicProfileDetail.setGender(candidateDetails
	  				.getGender().getName().toUpperCase().charAt(0));
		}

		onlineAdmissionBasicProfileDetail.setUrlConfigurationId(onlineAdmission
				.getUrlConfigurationId());

		SaveOnlineAdmissionBasicProfileDetailResponse saveOnlineAdmissionBasicProfileDetailResponse = saveOnlineAdmissionBasicProfileDetail(
				onlineAdmissionBasicProfileDetail, onlineAdmission
						.getCandidateDetails().getAcademicYear());

		OnlineAdmissionForm onlineAdmissionForm = new OnlineAdmissionForm();
		onlineAdmissionForm
				.setApplicantId(saveOnlineAdmissionBasicProfileDetailResponse
						.getApplicantId());
		onlineAdmissionForm
				.setTempAccessId(saveOnlineAdmissionBasicProfileDetailResponse
						.getTempAccessId());
		onlineAdmissionForm.setFormPayload(onlineAdmissionFormStr);

		return saveOnlineAdmissionFormWithAttachment(onlineAdmissionForm,
				photoFileAttachment, signatureFileAttachment, false,
				onlineAdmission.getDocuments());

	}

	@Override
	public Payment getPaymentDetailsWithAuditInformation(String applicantId) {
		String query = "select total_amount, DATE_FORMAT(paid_date, '%d-%m-%Y') as paid_date, qfix_reference_number, description as audit_description, "
				+ " option_name as payment_channel, mode_of_payment as mode_of_payment, applicant_id as payment_audit_id "
				+ " from online_admission_payment "
				+ " where applicant_id = :applicantId and status = 'SUCCESS'";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("applicantId", applicantId);

		log.debug("Retrive online admission payment detail query :: " + query);
		log.debug("queryParameters :: " + queryParameters);

		Payment paymentDetails = getNamedParameterJdbcTemplate()
				.queryForObject(query, queryParameters,
						new RowMapper<Payment>() {
							@Override
							public Payment mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								Payment payment = new Payment();
								payment.setAmount(rs.getFloat("total_amount"));
								payment.setDate(rs.getString("paid_date"));
								// payment.setPaymentAuditId(rs.getString("payment_audit_id"));
								payment.setQfixRefNumber(rs
										.getString("qfix_reference_number"));
								payment.setPaymentChannel(rs
										.getString("payment_channel"));
								payment.setModeOfPayment(rs
										.getString("mode_of_payment"));
								payment.setAuditDescription(rs
										.getString("audit_description"));
								return payment;
							}
						});

		return paymentDetails;
	}

	@Override
	public String getExportTemplateFactoryName(Integer branchId,
			Integer courseId) {

		String templateFactoryName = null;

		String query = "select export_template_factory_name from online_admission_course where id = :courseId and branch_id=:branchId";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("courseId", courseId);
		queryParameters.put("branchId", branchId);
		try {
			templateFactoryName = getNamedParameterJdbcTemplate()
					.queryForObject(query, queryParameters,
							new RowMapper<String>() {

								@Override
								public String mapRow(ResultSet rs, int rownumber)
										throws SQLException {
									return rs
											.getString("export_template_factory_name");
								}
							});
		} catch (Exception e) {
			throw new ApplicationException(
					"Invalid courseId and branchid combination specified.");
		}

		return templateFactoryName;
	}

	private String getRedirectionPath(Integer urlConfigurationId) {
		String redirectionUrlRelativePath = null;

		String query = "select home_page_relative_url from online_admission_url_configuration where id = :urlConfigurationId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("urlConfigurationId", urlConfigurationId);

		try {
			redirectionUrlRelativePath = getNamedParameterJdbcTemplate()
					.queryForObject(query, queryParameters,
							new RowMapper<String>() {

								@Override
								public String mapRow(ResultSet rs, int rownumber)
										throws SQLException {
									return rs
											.getString("home_page_relative_url");
								}
							});
		} catch (Exception e) {
			log.error("Invalid url configuration id specified.");
		}

		return redirectionUrlRelativePath;
	}

	public static void main(String args[]) throws IOException {
		// String payload =
		// "{\"course_details\":{\"course\":{\"id\":23,\"code\":\"8\",\"name\":\"MCA\",\"active\":true,\"category\":\"PS\"},\"candidate_details\":{\"name\":{\"firstname\":\"rgtfjgh\",\"surname\":\"df\",\"middlename\":\"fg\"},\"dateofbirth\":\"01/01/0123\",\"educationalqualification\":{\"name\":\"Graduation/Btech\",\"id\":\"3\"},\"category\":{\"name\":\"General\",\"id\":\"2\"},\"gender\":{\"name\":\"Male\",\"id\":\"1\"}},\"father_details\":{\"name\":{\"firstname\":\"dfg\",\"middlename\":\"gb\",\"surname\":\"fgn\"}},\"mother_details\":{\"name\":{\"firstname\":\"gn\",\"middlename\":\"fhg\",\"surname\":\"hgh\"}},\"contact_details\":{\"personal_mobile_number\":\"9876574635\",\"personal_email\":\"fdg@erhfg.gh\"},\"url_configuration_id\":5,\"branch_detail\":{\"id\":1,\"name\":\"test\"}, \"application_id\":\"12345\"}";
		String payload = "{\"course_details\":{\"course\":{\"id\":3,\"code\":\"3\",\"name\":\"Diploma\",\"active\":true,\"category\":\"PS\"},\"subject_preferences\":[],"
				+ "\"course_preferences\":[]},\"candidate_details\":{\"academic_term\":1,\"name\":{\"firstname\":\"gtsdg\",\"surname\":\"sdggd\"},\"gender\":"
				+ "{\"name\":\"Male\",\"id\":\"1\"},\"dateofbirth\":\"27/02/2017\",\"nationality\":\"dgsg\",\"educationalqualifications\":{\"name\":\"10\","
				+ "\"id\":\"1\"},\"category\":{\"name\":\"OBC\",\"id\":\"1\"},\"location\":[{\"id\":6,\"name\":\"Rajasthan\",\"checked\":true},{\"id\":11,"
				+ "\"name\":\"Maharashtra\",\"checked\":true}]},\"father_details\":{\"employment_details\":{\"address\":\"geughefas\","
				+ "\"occupation\":\"wfigsuahbkdn\",\"annual_income\":\"64654654\"},\"name\":{\"firstname\":\"sdgdg\",\"surname\":\"dssdgdsg\"},"
				+ "\"primary_phone\":\"6546565478\",\"email\":\"hgshd@gyiwsd.co\"},\"mother_details\":{\"name\":{\"firstname\":\"dsgsgds\","
				+ "\"surname\":\"dgdsgdsg\"},\"primary_phone\":\"6544664565\",\"email\":\"gdsg@gdwhj.co\"},\"sibling_details\":{\"name\":{}},"
				+ "\"address_details\":{\"local_address\":{\"address_line1\":\"ihihfiohfipjsf\"}},\"general_comment\":\"rhyfdh\","
				+ "\"contact_details\":{\"personal_mobile_number\":\"6546565478\",\"personal_email\":\"hgshd@gyiwsd.co\"},\"url_configuration_id\":2,"
				+ "\"branch_detail\":{\"id\":1,\"name\":\"test\"}, \"application_id\":\"12345\"}";
		ObjectMapper mapper = new ObjectMapper();
		OnlineAdmission onlineAdmission = null;

		try {
			onlineAdmission = mapper.readValue(payload.replaceAll("\\\\", ""),
					OnlineAdmission.class);
		} catch (Exception e) {
			log.error(e);
		}

		System.out.println("onlineAdmission >>>" + onlineAdmission.toString());

		PdfBuilder pdfBuilder = PdfFactory.getPdfBuilderInstance("rosemine");

		if (pdfBuilder != null) {

			pdfBuilder
					.createPdf(
							onlineAdmission,
							FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH
									+ "/images/Qfix/documents/External/r12r12gistra123ion/1/ine.pdf",
							null, null);
		}

	}

	@Override
	public void verifyUnverifyAdmissionForm(String applicationId,
			boolean isMarkVerify, Integer branchId) throws Exception {
		OnlineAdmissionFormDetail onlineAdmissionFormDetail = null;
		String query = "select oa.username, CONCAT(DATE_FORMAT(ay.from_date,'%Y'),'-',DATE_FORMAT(ay.to_date,'%Y')) as academic_year, "
				+ " oa.id as application_id, oa.applicant_firstname, oa.applicant_middlename, oa.applicant_surname, oa.applicant_gender, "
				+ " DATE_FORMAT(oa.applicant_dob,'%d-%m-%Y') as applicant_dob, oa.applicant_mobile, oa.applicant_email, "
				+ " oa.applicant_id, '' as applicant_access_password, oa.form_payload, oa.print_pdf_path, oa.photo_path, "
				+ " oa.signature_image_path, b.*, oa.category, IFNULL(oap.status,'UNPAID') as payment_status "
				+ " from online_admission oa "
				+ " inner join branch b on b.id = oa.branch_id "
				+ " inner join academic_year ay on ay.id= oa.academic_year_id "
				+ " left join online_admission_payment oap on oap.applicant_id = oa.applicant_id and oap.status='SUCCESS' "
				+ " where oa.applicant_id=:applicationId ";

		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("applicationId", applicationId);
		try {
			onlineAdmissionFormDetail = getNamedParameterJdbcTemplate()
					.queryForObject(query, queryParameters,
							new OnlineAdmissionFormRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			throw new AuthenticationException(
					"Invalid applicant id / access code specified. Unable to process your request.");
		}
		String payload = onlineAdmissionFormDetail.getFormPayload();
		OnlineAdmission onlineAdmission = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			onlineAdmission = mapper.readValue(payload.replaceAll("\\\\", ""),
					OnlineAdmission.class);
		} catch (Exception e) {
			log.error(e);
		}
		System.out.println("School verified status :::>>>"
				+ onlineAdmission.getSchoolVerified());

		if (isMarkVerify) {
			onlineAdmission.setSchoolVerified("Y");
			onlineAdmission.setVerifiedAt(USER_DATE_FORMAT.format(new Date()));
		} else {
			onlineAdmission.setSchoolVerified("N");
			onlineAdmission.setVerifiedAt(null);
		}
		payload = mapper.writeValueAsString(onlineAdmission);

		try {
			onlineAdmission = mapper.readValue(payload.replaceAll("\\\\", ""),
					OnlineAdmission.class);
			System.out.println("School verified status :::>>>"
					+ onlineAdmission.getSchoolVerified());
		} catch (Exception e) {
			log.error(e);
		}

		String sql = "update online_admission set form_payload = :payload where applicant_id = :applicationId";
		if (branchId != null) {
			sql += " AND branch_id =:branchId";
		}
		queryParameters = new HashMap<String, Object>();
		queryParameters.put("applicationId", applicationId);
		queryParameters.put("payload", payload);
		queryParameters.put("branchId", branchId);
		int updateStatus = getNamedParameterJdbcTemplate().update(sql,
				queryParameters);
		if (updateStatus == 0) {
			throw new AuthorisationException(
					"Function not allowed for this profile, invalid branch.");
		}
	}

	@Override
	public List<OnlineAdmissionUserDocument> listOnlineAdmissionUserDocuments(
			Integer onlineAdmissionId) {

		String query = "SELECT * FROM online_admission_user_document WHERE online_admission_id=?";

		Object[] args = new Object[] { onlineAdmissionId };

		return getJdbcTemplate().query(
				query,
				args,
				new BeanPropertyRowMapper<OnlineAdmissionUserDocument>(
						OnlineAdmissionUserDocument.class));
	}

	@Override
	public OnlineAdmissionForm getOnlineAdmissionForm(Integer onlineAdmissionId) {
		String query = "SELECT * FROM online_admission WHERE id=?";

		Object[] objects = new Object[] { onlineAdmissionId };

		return getJdbcTemplate().queryForObject(
				query,
				objects,
				new BeanPropertyRowMapper<OnlineAdmissionForm>(
						OnlineAdmissionForm.class));
	}

	@Override
	public void updateAdmissionFormPath(String applicantId,
			String admissionForPdfPath) {
		String sql = "update online_admission set print_pdf_path = '"
				+ admissionForPdfPath + "' where applicant_id = '"
				+ applicantId + "'";
		System.out.println("Update PDF PATH SQL :::>> " + sql);
		getJdbcTemplate().update(sql);
	}

	/*
	 * @Override public String getOnlineAdmissionTransactionStatus(String
	 * applicantId){
	 * System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.."+applicantId);
	 * String query =
	 * "SELECT status FROM online_admission_payment WHERE applicant_id="
	 * +"'applicantId'"; return getJdbcTemplate().queryForObject(query,
	 * String.class);
	 * 
	 * 
	 * }
	 */
}
