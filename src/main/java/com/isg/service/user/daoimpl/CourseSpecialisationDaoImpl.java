package com.isg.service.user.daoimpl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.isg.service.user.dao.CourseSpecialisationDao;
import com.isg.service.user.request.CourseSpecialisation;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class CourseSpecialisationDaoImpl extends JdbcDaoSupport implements CourseSpecialisationDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public List<CourseSpecialisation> getAllByCourseId(int id) {
		String retriveDistrict = "select * from course_specialisation where active='Y' and course_id = ?";
		Object[] inputs = new Object[] { id };
		return getJdbcTemplate().query(retriveDistrict, inputs,
				new BeanPropertyRowMapper<CourseSpecialisation>(CourseSpecialisation.class));
	}

	@Override
	public List<CourseSpecialisation> getCourseSpecialisationForInstitutes(Integer instituteId, Integer branchId,
			Integer courseId) {
		String retriveDistrict = "select cs.* from course_specialisation cs, course c, branch b, institute i where cs.active = 'Y' and cs.course_id = c.id and c.id = ? and b.id = ? and c.branch_id = b.id and i.id = b.institute_id and i.id = ?;";
		Object[] inputs = new Object[] { courseId, branchId, instituteId };
		return getJdbcTemplate().query(retriveDistrict, inputs,
				new BeanPropertyRowMapper<CourseSpecialisation>(CourseSpecialisation.class));
	}
}
