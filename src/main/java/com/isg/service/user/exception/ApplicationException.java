package com.isg.service.user.exception;

import java.util.ArrayList;
import java.util.List;

import com.isg.service.user.model.ErrorDetail;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private List<ErrorDetail> errors = new ArrayList<ErrorDetail>();

	private boolean isThrowableSet = false;

	public ApplicationException(String message) {
		super(message);
	}

	public ApplicationException(String message, Throwable t) {
		super(message, t);
		this.isThrowableSet = true;
	}

	public ApplicationException(String message, List<ErrorDetail> errors, Throwable t) {
		super(message, t);
		this.errors = errors;
		this.isThrowableSet = true;
	}

	public List<ErrorDetail> getErrors() {
		return errors;
	}

	public boolean isThrowableSet() {
		return isThrowableSet;
	}

}
