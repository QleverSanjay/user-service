package com.isg.service.user.exception;

public class AuthorisationException extends Exception {

	private static final long serialVersionUID = 1L;

	public AuthorisationException(String message) {
		super(message);
	}

}
