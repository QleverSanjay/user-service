package com.isg.service.user.dao;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface ForumDao {

	void setupAccountForParent(Integer studentId, Integer branchId, Integer parentId, List<String> forumIdList) throws IOException;
}
