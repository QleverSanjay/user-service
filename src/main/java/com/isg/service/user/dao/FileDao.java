package com.isg.service.user.dao;

import java.io.IOException;

import org.springframework.stereotype.Repository;

@Repository
public interface FileDao {

	public static enum FILE_CATEGORY {
		NOTIFICATION("notices"), SCHOOL("school"), STUDENT("student"), VENODR("vendor"), TEACHER("teacher");

		private final String fileCategory;

		private FILE_CATEGORY(String fileCategory) {
			this.fileCategory = fileCategory;
		}

		public String getFileCategory() {
			return this.fileCategory;
		}

	}

	void saveFile(String fileCategory, byte[] bytes, String fileNameWithExtension) throws IOException;

	void saveFile(String localStorePath, byte[] bytes) throws IOException;

}
