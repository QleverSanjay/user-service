package com.isg.service.user.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.NotificationCount;
import com.isg.service.user.model.NotificationRecipientDetail;
import com.isg.service.user.request.Notification;
import com.isg.service.user.request.SendNotfication;

@Repository
public interface NotificationDao {

	List<Notification> getNotification(String role, Integer userId, Integer entityId, Integer lastNotificationReceivedId);

	Notification getNotificationDetails(String role, Integer userId, Integer entityId, Integer notificationId);

	NotificationCount getUnreadMessageCount(String role, Integer roleId, int userId, int entityId) throws IOException, SQLException;

	void sendNotification(Integer userid, Integer branchId, Integer instituteId, String role, SendNotfication sendNotfication)
					throws AuthorisationException,
					IOException, ParseException;

	List<Notification> getAssignment(String role, Integer userId, Integer entityId, Integer lastAssignmentReceivedId);

	void assignStudentNoticationsToParent(Long studentUserId, Integer parentUserId);

	boolean canAccessNoticeDetail(Integer userId, Integer notificationId);

	NotificationRecipientDetail getNotificationRecipientDetail(Integer notificationId);

	Map<String, Integer> markAsRead(Integer notificationId, Integer userId, Integer forUserId);

}
