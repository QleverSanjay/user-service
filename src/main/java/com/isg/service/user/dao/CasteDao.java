package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.Caste;

@Repository
public interface CasteDao {

	List<Caste> getCastes();

	Caste getCasteById(int id);

	List<Caste> getCastesByName(String casteName);

	List<Caste> getSubCastesByCasteId(Integer casteId);

}
