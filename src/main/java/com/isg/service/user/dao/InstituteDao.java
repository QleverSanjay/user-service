package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.Branch;
import com.isg.service.user.request.Institute;

@Repository
public interface InstituteDao {

	List<Institute> getAll();

	List<Branch> getBranchesByInstituteId(int id);

	Branch getBranchDetails(Integer id);

	Institute getById(Integer id);
	
	List<Institute> searchInstitute(String name);
}
