package com.isg.service.user.dao;

import java.io.IOException;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isg.service.user.model.ShoppingProfile;
import com.isg.service.user.model.shopping.OrderPaymentDetails;
import com.isg.service.user.request.Parent;

@Repository
public interface ShoppingDao {

	public ShoppingProfile getShoppingSessionDetails(
					String shoppingAccountUsername, String shoppingAccountPassword);

	public ShoppingProfile createShoppingAccount(Parent parent) throws JsonProcessingException, IOException;

	public ShoppingProfile getShoppingProfileDetails(Integer userId);

	void createShoppingProfile(String username, String password, Integer userId);

	public OrderPaymentDetails getShoppingCartDetails(ShoppingProfile shoppingProfile, String orderId) throws IOException;

	public boolean acknowledge(ShoppingProfile shoppingProfile, Integer entityId, Integer userId, String orderId) throws IOException;

	void createShoppingProfile(String username, String password, Integer userId, Integer shoppingProfileId);

	boolean linkCustomerAccounts(Integer parentUserId, Integer childUserId);

	ShoppingProfile getShoppingProfileDetails(String username);
}
