package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.CourseSpecialisation;

@Repository
public interface CourseSpecialisationDao {
	List<CourseSpecialisation> getAllByCourseId(int id);

	List<CourseSpecialisation> getCourseSpecialisationForInstitutes(Integer instituteId, Integer branchId,
			Integer courseId);
}
