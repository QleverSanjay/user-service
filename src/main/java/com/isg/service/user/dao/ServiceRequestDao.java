package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.ServiceRequest;
import com.isg.service.user.request.ServiceRequestType;

@Repository
public interface ServiceRequestDao {

	List<ServiceRequestType> getServiceRequestTypeByBranchId(Integer branchId);
	
	void sendServiceRequestNotification(ServiceRequest serviceRequest);

	ServiceRequestType getServiceRequestType(Integer id);

}
