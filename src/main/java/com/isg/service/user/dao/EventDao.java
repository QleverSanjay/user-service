package com.isg.service.user.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.EventResponse;
import com.isg.service.user.request.Ediary;
import com.isg.service.user.request.Event;
import com.isg.service.user.request.Timetable;

@Repository
public interface EventDao {

	Collection<EventResponse> getEventsForAEntity(String role, Integer userId, Integer entityId, String startDate, String endDate) throws SQLException,
					ParseException,
					AuthenticationException;

	void createPersonalTask(Integer roleId, Event event, Integer userId, Integer parentId, String defaultRole) throws ParseException;

	void updatePersonalTask(Event event, Integer userId, Integer entityId, String defaultRole) throws ParseException;

	void deletePersonalTask(Integer eventId, Integer userId, Integer entityId);

	Collection<EventResponse> syncEventsForAEntity(String role, Integer userId, Integer entityId, long lastSyncTimeStamp) throws ParseException,
					AuthorisationException;

	Collection<EventResponse> getEdiaryDetails(String defaultRole, Integer userId, Integer entityId, String startDate, String endDate) throws ParseException;

	void addEdiaryEntry(Ediary ediary, Integer userId, Integer instituteId, Integer branchId, Integer entityId)
					throws AuthorisationException, IOException, ParseException;

	Collection<EventResponse> syncEdiaryForAEntity(String role, Integer userId, Integer entityId, long lastSyncTimeStamp) throws ParseException,
					AuthorisationException;

	Collection<Ediary> getEdiaryDetails(Integer userId);

	void assignStudentEventsToParent(Long studentUserId, Integer parentUserId, Integer parentId);

	Event getEventDetails(Integer userId, Integer entityId, Integer eventId);

	Ediary getEdiaryDetail(Integer userId, Integer entityId, Integer ediaryId, String role);

	void saveTimeTable(Timetable timetable, Integer branchId, Integer userId, String defaultRole)
					throws AuthorisationException;

	void updateTimeTable(Timetable timetable, Integer timetableId, Integer branchId, Integer userId, String defaultRole) throws AuthorisationException;

	void deletTimetable(Integer timetableId);

	Timetable getTimeTableForSchool(Integer standardId, Integer divisionId);

	Timetable getTimeTableForStudent(Integer branchId, Integer studentId);

	boolean canUserUpdateSpecifiedTimetable(Integer branchId, Integer timetableId);

	Map<String, Integer> markEdiaryAsRead(Integer ediaryId, Integer userId, Integer forUserId);

	Map<String, Integer> markEventAsRead(Integer eventId, Integer userId, Integer forUserId, Integer entityId, Integer roleId);

	Map<String, Integer> getUnreadEdiaryCount(Integer userId);

	Map<String, Integer> getUnreadEventCount(Integer userId, Integer roleId);

}
