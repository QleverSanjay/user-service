package com.isg.service.user.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.request.Parent;
import com.isg.service.user.request.ParentRegistrationByAdmin;
import com.isg.service.user.response.InviteResponse;
import com.isg.service.user.response.ParentRegistrationByAdminResponse;
import com.isg.service.user.response.RegistrationResponse;

@Repository
public interface ParentDao {
	public RegistrationResponse registerParent(Parent p, String authenticationType) throws JsonProcessingException, IOException, NoSuchAlgorithmException,
					AuthenticationException, SQLException;

	public boolean registerMobile(Integer entityId, String gcmId);

	public Parent getParent(Integer id);

	public boolean updateParent(Parent p) throws ParseException;

	Parent getParentByUserId(Integer id);

	public List<ParentRegistrationByAdminResponse> registerParentByAdmin(ParentRegistrationByAdmin parent) throws Exception;

	public void sendInvite(String firstname, String email, String primaryContact, Integer userId);

	public InviteResponse validateInvite(String otp);

}
