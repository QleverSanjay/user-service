package com.isg.service.user.dao;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isg.service.user.client.social.response.UserInfoResponse;
import com.isg.service.user.exception.InvalidLoginCredentialsException;
import com.isg.service.user.model.BranchStudentParentDetail;
import com.isg.service.user.model.Contact;
import com.isg.service.user.model.Group;
import com.isg.service.user.model.GroupUser;
import com.isg.service.user.model.OTPDetails;
import com.isg.service.user.model.PaymentAccount;
import com.isg.service.user.model.UserChatProfile;
import com.isg.service.user.model.UserForumProfile;
import com.isg.service.user.request.ChangePassword;
import com.isg.service.user.request.Passbook;
import com.isg.service.user.request.User;
import com.isg.service.user.response.OtpResponse;
import com.isg.service.user.response.UserDetail;

@Repository
public interface UserDao {
	boolean isUserExist(String email);

	User getUserDetails(User user) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidLoginCredentialsException;

	User getUserDetailsBySocialProfile(UserInfoResponse userInfoResponse) throws SQLException, JsonProcessingException;

	UserChatProfile getUserChatProfileDetails(Integer userId);

	UserChatProfile getUserChatProfileDetailsbyParentId(Integer parentId);

	void updateGroupUser(Integer userId, Integer groupId, boolean isSuggestedGroup);

	OtpResponse generateOTP(String email, String primaryContactNumber, boolean checkUserExists);

	boolean verifyOTP(String primaryContactNumber, String otp);

	boolean isEmailAvailable(String emailId);

	boolean updateUserGCM(Integer userId, String gcmId, String deviceType);

	Long addEntryToPassbook(Integer userId, Passbook passbook) throws ParseException;

	List<Passbook> getPassbookDetails(Integer userId, long lastSyncTimeStamp) throws ParseException;

	boolean canUserAccessSpecifiedGroup(Integer userId, List<Integer> groupIds);

	boolean canUserAccessSpecifiedUserProfiles(Integer userId, Integer branchId, List<Integer> userIds);

	//List<Group> getAssignedGroups(Integer userId);

	Map<String, List<String>> getGcmIdsOfGroupUsers(List<Integer> groupIds);

	Map<String, List<String>> getGcmIdsOfUsers(List<Integer> userIds);

	//Set<Integer> getGroupUsers(List<Integer> groupIds);

	UserForumProfile getUserForumProfileDetails(Integer userId);

	PaymentAccount getPaymentAccount(Integer userId);

	UserInfoResponse getUserSocialProfileDetails(String socialProfileProvider, String accessToken) throws JsonProcessingException;

	boolean changePassword(Integer userId, ChangePassword changePassword) throws ParseException, NoSuchAlgorithmException, InvalidKeySpecException,
					InvalidLoginCredentialsException;

	User getUserByEmail(String email);

	void saveForgotPasswordOtp(String username, String email);

	boolean resetPassword(String otp, String newPassword) throws NoSuchAlgorithmException;

	void updateGroupIndividual(Integer userId, Integer groupId, Integer roleId);

	List<GroupUser> retrieveUnassignedChatGroupsForParents(Integer parentId, Integer parentUserId, Integer studentId);

	List<String> retrieveUnassignedForumsForParents(Integer parentId, Integer parentUserId, Integer studentId);

	void removeEntryFromPassbook(List<Integer> passbookId);

	Passbook getPassbookDetails(Integer passbookId);

	Long addEntryToPassbook(String selfExpenseFlag, float totalAmount, Integer initiatorUserId, String description, Integer receivingEntityUserId,
					String receivingEntityName, String transactionType) throws ParseException;

	List<BranchStudentParentDetail> getBranchStudentParentDetailByStudentGroup(List<Integer> groupId, Integer branchId);

	List<BranchStudentParentDetail> getBranchStudentParentDetailByStudent(List<Integer> studentUserId, Integer branchId);

	Set<Integer> getGroupUsers(List<Integer> groupIds, boolean excludeParentRole);

	User getUserByIdAndRole(Integer userId, String role);

	List<Group> getAssignedGroups(Integer userId, String searchText, String exludeIds, String role, Integer branchId);

	List<UserDetail> searchIndividualUsers(Integer userId, String searchText, String excludeIds, String role, Integer branchId, Integer entityId);

	boolean verifyOTP(String otp);

	boolean canUserAccessSpecifiedBranch(Integer userId, Integer branchId);

	Contact getContactDetailsByUserId(Integer userId);

	Set<Integer> getGroupUsers(List<Integer> groupIds, boolean excludeParentRole, boolean excludeTeacherRole);

	User getUserByMobile(String email);

	boolean isEntityExist(String email, String mobile);

	boolean isParentExist(String username);

	String getLoginCount(User user);

	void logInvalidLoginAttempt(User user);
	
	boolean checkUserExist(String username);
	
	Integer generateLoginOTP(String username) throws SQLException;
	
	OTPDetails getOTPDetails(Integer id, String username);
	
	String getMaskedContact(OTPDetails otpDetails);
	
	boolean verifyOTP(String username, Integer otp)  throws ParseException;
	
	User getUserByUsername(String username) throws SQLException;
}
