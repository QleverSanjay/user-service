package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.State;

@Repository
public interface StateDao {

	List<State> getStates();
	
	State findStateById(int id);
}
