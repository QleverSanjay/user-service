package com.isg.service.user.dao;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isg.service.user.client.social.response.UserInfoResponse;

@Repository
public interface SocialProfileCommunicationDao {

	UserInfoResponse getAccountInfo(String socialProfileProvider, String accessToken) throws JsonProcessingException;

}
