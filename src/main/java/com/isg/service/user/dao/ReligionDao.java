package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.Religion;

@Repository
public interface ReligionDao {

	List<Religion> getReligions();
	
	List<Religion> getReligionsById(int id);
	
	List<Religion> getReligionsByName(String religionName);
}
