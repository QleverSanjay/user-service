package com.isg.service.user.dao;

import java.io.IOException;

import org.springframework.stereotype.Repository;

import com.isg.service.user.model.FileAttachment;

@Repository
public interface ManageProfileDao {

	String updateProfilePicture(Integer entityId, Integer instituteId, Integer branchId, String role, FileAttachment fileAttachment) throws IOException;

}
