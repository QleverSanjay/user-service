package com.isg.service.user.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.StandardDivision;
import com.isg.service.user.model.StudentDetail;
import com.isg.service.user.model.StudentRegistration;
import com.isg.service.user.model.onlineadmission.Category;
import com.isg.service.user.request.Goal;
import com.isg.service.user.request.Student;
import com.isg.service.user.request.StudentBasicDetails;
import com.isg.service.user.request.StudentDirect;
import com.isg.service.user.response.FindStudentProfileMatchesForAParent;
import com.isg.service.user.response.StudentBasicProfile;

@Repository
public interface StudentDao {
	// OtpResponse generateOTP(String email, String primaryContactNumber);

	List<FindStudentProfileMatchesForAParent> getUnlinkedStudentByParentId(
			Integer id);

	public void saveStudent(Student student) throws IOException,
			NoSuchAlgorithmException;

	List<StudentBasicProfile> getStudentBasicProfileDetails(long parentId);

	/**
	 * This method is used to get student object from student table based on
	 * student id.
	 * 
	 * @param studentId
	 * @return
	 */

	Student getStudentProfileDetailsbyId(long studentId);

	/**
	 * This method is used to get student object from student_upload table based
	 * on student id.
	 * 
	 * @param studentId
	 * @return
	 */

	Student getUploadedStudentById(Long studentId);

	List<Student> listByParentId(Integer parentId);

	void updateStudent(Student student, String defaultRole, Integer entityId,
			Integer userId) throws IOException;

	boolean canAccessDetails(Integer entityId, Integer studentId);

	List<StudentBasicDetails> getAssociatedStudentList(Integer branchId,
			String searchText, String exludeIds);

	List<Goal> getGoalDetails(String role, Integer entityId, Integer studentId,
			long lastSyncTimeStamp) throws AuthorisationException;

	void saveGoal(Integer entityId, Goal goal) throws ParseException;

	void updateGoal(String defaultRole, Integer entityId, Integer userId,
			Integer goalId, String feedback, String status)
			throws ParseException;

	boolean deleteGoal(String defaultRole, Integer entityId, Integer goalId);

	Goal getGoalDetail(Integer goalId);

	StudentBasicDetails getStudentDetailsForNotification(Integer studentId);

	List<StudentDetail> getStudentNamebyIds(String studentId);

	StudentDirect searchStudent(Integer branchId, String uniqueId,
			String uniqueIdLable) throws SQLException;

	List<StandardDivision> getStandardList(Integer branchId);
	List<Category> getCategoryList();
	Integer insertStudent(StudentRegistration student) throws Exception;

	
	
	

}
