package com.isg.service.user.dao;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.AcademicYear;
import com.isg.service.user.model.BranchWorkingDay;
import com.isg.service.user.model.HolidayResponse;
import com.isg.service.user.model.StandardDivision;
import com.isg.service.user.model.onlineadmission.Category;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.BranchForumDetail;

@Repository
public interface BranchDao {

	public List<Branch> getAll();

	Branch getBranchDetails(Integer branchId);

	BranchForumDetail getbranchForumDetail(Integer branchId);

	Collection<HolidayResponse> getHolidays(Integer userId, Integer entityId, String defaultRole) throws AuthorisationException;

	AcademicYear getActiveAcademicYear(Integer branchId);

	BranchWorkingDay getWorkingDay(Integer branchId);

	Collection<HolidayResponse> getHolidays(Integer userId, Integer entityId, String role, Integer branchId) throws AuthorisationException;

	List<AcademicYear> getAcademicYearList(Integer branchId);
	

	
}
