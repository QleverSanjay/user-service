package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.Courses;

@Repository
public interface CourseDao {

	List<Courses> getAll();
	
	List<Courses> getAllByBranchId(Integer id);

	List<Courses> getCourseNamesForInstitutes(Integer instituteId, Integer branchId);
}
