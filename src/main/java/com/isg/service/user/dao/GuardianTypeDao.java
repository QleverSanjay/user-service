package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.GuardianType;

@Repository
public interface GuardianTypeDao {
	List<GuardianType> getAll();
}
