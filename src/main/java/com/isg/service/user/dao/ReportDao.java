package com.isg.service.user.dao;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.model.ExpenseChart;
import com.isg.service.user.model.FeeHeadChart;
import com.isg.service.user.model.StudentResult;
import com.isg.service.user.request.Student;

@Repository
public interface ReportDao {

	public ExpenseChart getExpenseChart(Integer entityId, int month, int year, int forEntityUserId) throws ParseException;

	public List<StudentResult> getStudentResults(Integer studentId);

	public HashMap getReportById(final Integer resultId) throws Exception;

	public List<Student> getStudentsDueForFeePayment(final Integer feeHeadId, final Integer standardId) throws Exception;

	public FeeHeadChart getFeeHeadStats(final Integer feeHeadId, final Integer standardId) throws Exception;

}
