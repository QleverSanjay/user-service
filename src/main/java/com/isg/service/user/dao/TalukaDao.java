package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.Taluka;

@Repository
public interface TalukaDao {

	List<Taluka> getTalukas();

	List<Taluka> getTalukasOrCitiesByDistrictId(int id);

}
