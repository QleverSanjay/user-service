package com.isg.service.user.dao;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.FileAttachment;
import com.isg.service.user.model.OnlineAdmissionCourse;
import com.isg.service.user.model.OnlineAdmissionCourseSubjectPreference;
import com.isg.service.user.model.OnlineAdmissionDocument;
import com.isg.service.user.model.OnlineAdmissionFormDetail;
import com.isg.service.user.model.OnlineAdmissionSeedProgramme;
import com.isg.service.user.model.OnlineAdmissionUrlConfiguration;
import com.isg.service.user.model.Payment;
import com.isg.service.user.model.QualificationExam;
import com.isg.service.user.model.University;
import com.isg.service.user.model.onlineadmission.Document;
import com.isg.service.user.model.onlineadmission.OnlineAdmission;
import com.isg.service.user.model.onlineadmission.OnlineAdmissionUserDocument;
import com.isg.service.user.request.OnlineAdmissionBasicProfileDetail;
import com.isg.service.user.request.OnlineAdmissionForm;
import com.isg.service.user.request.OnlineAdmissionLoginRequest;
import com.isg.service.user.response.OnlineAdmissionConfiguration;
import com.isg.service.user.response.OnlineAdmissionLoginResponse;
import com.isg.service.user.response.SaveOnlineAdmissionBasicProfileDetailResponse;
import com.isg.service.user.response.SaveOnlineAdmissionFormResponse;
import com.isg.service.user.response.UpdateOnlineAdmissionFormResponse;

@Repository
public interface OnlineAdmissionDao {

	List<OnlineAdmissionCourse> getOnlineAdmissionCourses(Integer branchId, String category);

	List<OnlineAdmissionSeedProgramme> getOnlineAdmissionSeedProgramme(Integer courseId, boolean optional);
	
	OnlineAdmissionForm getOnlineAdmissionForm(Integer onlineAdmissionId);
	
	Map<String, List<OnlineAdmissionCourseSubjectPreference>> getOnlineAdmissionSubjectPreference(Integer courseId);

	List<QualificationExam> getOnlineAdmissionQualifyingExam(Integer branchId);

	List<University> getOnlineAdmissionUniversity();

	List<OnlineAdmissionDocument> getOnlineAdmissionDocument(Integer courseId);

	OnlineAdmissionUrlConfiguration getOnlineAdmissionDocumentUrlConfiguration(String urlIdentifier, String category);

	SaveOnlineAdmissionBasicProfileDetailResponse saveOnlineAdmissionBasicProfileDetail(OnlineAdmissionBasicProfileDetail onlineAdmissionBasicProfileDetail);

	SaveOnlineAdmissionFormResponse saveOnlineAdmissionForm(OnlineAdmissionForm onlineAdmissionForm) throws AuthorisationException, IOException;

	OnlineAdmissionLoginResponse login(OnlineAdmissionLoginRequest onlineAdmissionLoginRequest) throws AuthenticationException;

	UpdateOnlineAdmissionFormResponse updateOnlineAdmissionForm(OnlineAdmissionForm onlineAdmissionForm, FileAttachment photoFileAttachment,
					FileAttachment signatureFileAttachment) throws AuthorisationException, IOException;

	OnlineAdmissionFormDetail retrieveOnlineFormDetail(String token) throws AuthorisationException;


	List<OnlineAdmission> exportAdmissionReport(Integer branchId, Integer courseId, Integer academicYearId, boolean b);

	SaveOnlineAdmissionFormResponse saveOnlineAdmissionSingleStep(String onlineAdmissionFormStr, FileAttachment photoFileAttachment,
					FileAttachment signatureFileAttachment, List<MultipartFile> documents)
					throws AuthorisationException, IOException;

	Payment getPaymentDetailsWithAuditInformation(String applicantId);

	String getExportTemplateFactoryName(Integer branchId, Integer courseId);

	OnlineAdmissionLoginResponse getFormDetailsForAdmin(String applicationId) throws AuthenticationException;

	void verifyUnverifyAdmissionForm(String applicationId, boolean isMarkVerify, Integer branchId) throws Exception;

	OnlineAdmissionConfiguration getAdmissionFormConfiguration(Integer branchId);
	
	List<OnlineAdmissionUserDocument> listOnlineAdmissionUserDocuments(Integer onlineAdmissionId);

	SaveOnlineAdmissionFormResponse saveOnlineAdmissionFormWithAttachment(
			OnlineAdmissionForm onlineAdmissionForm,
			FileAttachment photoFileAttachment,
			FileAttachment signatureFileAttachment,
			boolean createAdmissionFormPdf, List<Document> list)
			throws AuthorisationException, IOException;

	void updateAdmissionFormPath(String applicantId, String admissionForPdfPath);

	/*String getOnlineAdmissionTransactionStatus(String applicantId);*/

}
