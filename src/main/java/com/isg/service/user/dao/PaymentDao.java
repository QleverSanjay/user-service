package com.isg.service.user.dao;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.isg.service.user.client.payment.request.RefundDetail;
import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.OnlineAdmissionFormDetail;
import com.isg.service.user.model.Payment;
import com.isg.service.user.model.PaymentDetailResponse;
import com.isg.service.user.model.PaymentMoreInfo;
import com.isg.service.user.model.ShoppingPayment;
import com.isg.service.user.request.BranchConfiguration;
import com.isg.service.user.request.OnlineAdmissionFee;
import com.isg.service.user.request.PaymentBranchScheme;
import com.isg.service.user.request.PaymentTransaction;
import com.isg.service.user.response.OnlineAdmissionConfiguration;

@Repository
public interface PaymentDao {

	// String processPayment(float amount, float internetHandlingCharges, String
	// description, Integer userId) throws JsonProcessingException;

	String initiateFeesPayment(List<PaymentTransaction> feeDues, Integer entityId, Integer userId, String role, String channel) throws JsonProcessingException;

	PaymentDetailResponse acknowledgePayment(String accessCode, Integer entityId, String role) throws IOException, AuthorisationException, ParseException;

	String initiateShoppingPayment(ShoppingPayment shoppingPayment, Integer entityId, Integer userId, String defaultRole, String channel)
					throws IOException;

	PaymentDetailResponse getPayment(String accessCode);

	void acknowledgePayment(String key, String payload, Integer createdBy) throws IOException, ClassNotFoundException, Exception;

	Payment getPaymentDetails(Integer paymentId);

	Payment getPaymentDetailsWithAuditInformation(Integer paymentId, Integer entityId, Integer userId);

	String initiateOnlineAdmissionFeesPayment(OnlineAdmissionFee onlineAdmissionFee, OnlineAdmissionFormDetail onlineAdmissionFormDetails, String channel,
					OnlineAdmissionConfiguration configuration)
					throws JsonParseException, JsonMappingException, IOException;

	PaymentDetailResponse acknowledgeOnlineAdmissionPayment(String accessCode);

	List<PaymentBranchScheme> getBranchPaymentSchemeList(Integer branchId);

	Payment getPaymentDetailsWithAuditInformationForAdmin(Integer paymentId, Integer userId, boolean validateEntity);

	BranchConfiguration getBranchConfiguration(Integer branchId);

	PaymentMoreInfo getPaymentMoreInfo(String qfixReferenceNumber);

	PaymentDetailResponse updatePaymentMode(String paymentMode, String qfixReferenceNumber);

	void refundFeePayment(Integer paymentId, double amount, Integer integer);

	void acknowledgeRefund(RefundDetail refundDetail);

	PaymentDetailResponse markPaymentUnpaid(String accessCode, Integer entityId, String role) throws Exception;

}
