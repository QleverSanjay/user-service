package com.isg.service.user.dao;

import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.Vendor;
import com.isg.service.user.request.VendorMerchantDetail;

@Repository
public interface VendorDao {

	public Vendor register(Vendor vendor) throws NoSuchAlgorithmException;

	public Vendor getById(Integer entityId);

	Vendor updateMerchantDetails(VendorMerchantDetail vendorMerchantDetail) throws NoSuchAlgorithmException;
}
