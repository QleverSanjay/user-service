package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.District;

@Repository
public interface DistrictDao {

	List<District> getDistricts();
	
	List<District> getDistrictsByStateId(int id);
	
}
