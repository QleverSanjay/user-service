package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.Division;
import com.isg.service.user.request.Standard;
import com.isg.service.user.request.Teacher;

@Repository
public interface TeacherDao {

	public Teacher getDetails(Integer teacherId);

	boolean update(Teacher teacher);

	public List<Standard> getStandard(Integer entityId);

	public List<Division> getDivision(Integer entityId, Integer standardId);

}
