package com.isg.service.user.dao;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.client.chat.response.session.UserResponse;
import com.isg.service.user.exception.AuthenticationException;
import com.isg.service.user.model.Contact;
import com.isg.service.user.model.Group;
import com.isg.service.user.model.GroupUser;
import com.isg.service.user.request.ChatProfile;
import com.isg.service.user.request.SearchContact;

@Repository
public interface ChatDao {

	void assignStudentChatGroupsToParent(Integer parentId, Integer parentUserId, List<GroupUser> groupUserList);

	UserResponse createChatAccount(String email, String firstname, String surname, String tags);

	ChatProfile getChatSessionDetails(String chatAccountUsername, String chatAccountPassword, long chatAccountProfileId);

	int getUnreadChatCount(String username, String password) throws IOException;

	void createPrivateGroup(Integer userId, String username, String password, String chatWithOccupantIds) throws IOException;

	void deleteChatGroup(String username, String password, String dialogId) throws IOException;

	void createPublicGroup(Integer userId, String username, String password, String groupName, String chatWithOccupantIds) throws IOException;

	void joinGroup(Integer groupId, Integer userId, long chatProfileId, Integer roleId);

	List<Group> retrieveSuggestedGroup(String defaultRole, Integer entityId, Integer userId) throws AuthenticationException;

	List<Contact> searchContact(SearchContact searchContact, Integer userId);
}
