package com.isg.service.user.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.Division;
import com.isg.service.user.request.Standard;

@Repository
public interface MasterDao {

	List<Standard> getStandards(Integer branchId);

	List<Division> getDivisions(Integer standardId);

	Integer getRoleId(String roleCode);

	void saveAppVersion(String appCode, String versionInfo);

	Map<String, String> getAppVersion();

	Integer generateSerialNumber(Integer academicYearId, Integer branchId, String identifier);

}
