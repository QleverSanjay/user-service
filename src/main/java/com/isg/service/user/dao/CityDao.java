package com.isg.service.user.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.isg.service.user.request.City;

@Repository
public interface CityDao {

	List<City> getCities();

	List<City> getCitiesByTalukaId(int id);

	List<City> getCitiesByName(String name);

}
