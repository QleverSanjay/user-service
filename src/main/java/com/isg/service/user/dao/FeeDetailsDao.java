package com.isg.service.user.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.isg.service.user.exception.AuthorisationException;
import com.isg.service.user.model.FeeToBePaidDetail;
import com.isg.service.user.model.FeesInfo;
import com.isg.service.user.request.FeeDetails;
import com.isg.service.user.request.FeesCode;
import com.isg.service.user.request.PaymentTransaction;

@Repository
public interface FeeDetailsDao {

	public List<FeeDetails> getFeeDetails();

	List<PaymentTransaction> getFeeDues(Integer entityId, String role);

	int getFeesDuesCount(Integer entityId, String role) throws SQLException;

	void markPaymentStatus(Integer userId, Integer entityId, int flag, Integer feesId, Integer studentId, Integer paymentAuditId, String amount,
					Integer paymentId, String description, String role, Long feeScheduleId, String qfixRefNumber, double chargesApplicableForFees, String paymentType)
					throws AuthorisationException, ParseException;

	FeeDetails getFeeDetails(Integer id);

	List<FeeDetails> getFeeDetails(List<Integer> feesIdList);

	List<PaymentTransaction> getPaidPaymentTransactions(Integer parentId, Integer userId, String role);

	List<PaymentTransaction> getFeeDues(Integer entityId, String role, Integer studentIds, Long feeScheduleId);

	Map<Integer, String> retrieveFeesSplitConfigurationDetails(Map<Integer, FeeToBePaidDetail> feesIdsSelectedForPayment);

	public FeeDetails getFeeDetailsByScheduleId(Long feeScheduleId);
	
	List<FeesCode>getApplicableFees(Integer standardId,Integer divisionId,Integer casteId,Integer branchId);
	List<FeesCode>getAdditionalFeeCode(Integer branchId);

	FeesInfo getFeeInfo(Integer entityId, String defaultRole, Long feeScheduleId, Long paymentId, Long pgRefundId);

}
