package com.isg.service.user.model;

public class UserEvent {

	Integer eventId;

	Integer eventGroupId;

	String status;

	public UserEvent(Integer eventId, Integer eventGroupId, String status) {
		super();
		this.eventId = eventId;
		this.eventGroupId = eventGroupId;
		this.status = status;
	}

	public Integer getEventId() {
		return eventId;
	}

	public Integer getEventGroupId() {
		return eventGroupId;
	}

	public String getStatus() {
		return status;
	}

}
