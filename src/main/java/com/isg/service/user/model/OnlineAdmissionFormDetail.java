package com.isg.service.user.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.request.Branch;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineAdmissionFormDetail {

	Integer id;

	@JsonProperty("first_name")
	@NotNull
	@Size(max = 45)
	String firstName;

	@JsonProperty("middle_name")
	@NotNull
	@Size(max = 45)
	String middleName;

	@JsonProperty("surname")
	@NotNull
	@Size(max = 45)
	String surname;

	@JsonProperty("gender")
	@NotNull
	char gender;

	@JsonProperty("dob")
	@NotNull
	@Size(min = 10, max = 10)
	String dob;

	@JsonProperty("mobile")
	@NotNull
	@Size(max = 12)
	String mobile;

	@JsonProperty("email")
	@NotNull
	@Size(max = 100)
	String email;

	@JsonProperty("applicant_Id")
	@NotNull
	@Size(max = 45)
	String applicantId;

	String password;

	@JsonProperty("form_payload")
	@NotNull
	String formPayload;

	String academicYear;

	Branch branch;

	@JsonIgnore
	String pdfFilePath;

	// @JsonIgnore
	String photoPath;

	// @JsonIgnore
	String signaturePath;

	@JsonIgnore
	String paymentStatus;

	String username;

	String category;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@JsonProperty("form_number")
	public Integer getId() {
		return id;
	}

	@JsonIgnore
	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(String applicantId) {
		this.applicantId = applicantId;
	}

	@JsonIgnoreProperties
	public String getPassword() {
		return password;
	}

	@JsonProperty("password")
	@NotNull
	@Size(max = 45)
	public void setPassword(String password) {
		this.password = password;
	}

	public String getFormPayload() {
		return formPayload;
	}

	public void setFormPayload(String formPayload) {
		this.formPayload = formPayload;
	}

	@JsonProperty("academic_year")
	public String getAcademicYear() {
		return academicYear;
	}

	@JsonIgnore
	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	@JsonProperty("branch_detail")
	public Branch getBranch() {
		return branch;
	}

	@JsonIgnore
	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public String getPdfFilePath() {
		return pdfFilePath;
	}

	public void setPdfFilePath(String pdfFilePath) {
		this.pdfFilePath = pdfFilePath;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getSignaturePath() {
		return signaturePath;
	}

	public void setSignaturePath(String signaturePath) {
		this.signaturePath = signaturePath;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
