package com.isg.service.user.model.pdf;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.springframework.util.StringUtils;

import com.isg.service.user.model.Payment;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.BranchConfiguration;
import com.isg.service.user.util.FileStorePathAssembler;

public class PDFReceiptGenerator {

	// Hold the padding bottom of the document.
	private final static float PADDING_BOTTOM_OF_DOCUMENT = 70f;

	public static void main(String[] args) throws FileNotFoundException, IOException {

		PDFReceiptGenerator PDFReceiptGeneratorObj = new PDFReceiptGenerator();

		Payment payment = new Payment();
		payment.setAmount(1000);
		payment.setDate("10-08-2016");
		payment.setAuditDescription("Shopping Payment");
		payment.setForPerson("Praveen Choudhary");
		payment.setQfixRefNumber("1234566");
		payment.setPaymentChannel("Credit Card");
		payment.setModeOfPayment("Credit Card");
		payment.setInstituteLogoUrl("/images/images.jpg");
		BranchConfiguration bc = new BranchConfiguration();
		bc.setAddress("mumbai mumbai 400072 ");
		bc.setBranchName("PitabasTestBranch");
		bc.setTrailerRecord("thank u for payment");
		bc.setUseSchoolLogAsWatermarkImage("/images/Qfix/documents/External/no-images.png");
		Branch branch = new Branch();
		branch.setName("Dummy Branch");
		branch.setAddressLine("Mumbai");
		ByteArrayOutputStream output = PDFReceiptGeneratorObj.generatePaymentReceipt(payment, branch,
						"/images/Qfix/documents/External/145/roseminenewlogo.png",
						bc);

		try {
			OutputStream outputStream = new FileOutputStream("D:\\PDFWithText-UPDATED.pdf");
			output.writeTo(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*PDDocument realDoc = PDDocument.load("D:\\PDFWithText-UPDATED.pdf");
		//the above is the document you want to watermark                   

		//for all the pages, you can add overlay guide, indicating watermark the original pages with the watermark document.
		HashMap<Integer, String> overlayGuide = new HashMap<Integer, String>();
		for (int i = 0; i < realDoc.getPageCount(); i++) {
			//overlayGuide.put(i + 1, "D:\\pitabas\\QFIX-TEMP\\watermark.pdf");
			//watermark.pdf is the document which is a one page PDF with your watermark image in it. Notice here that you can skip pages from being watermarked.
		}
		Overlay overlay = new Overlay();
		overlay.setInputPDF(realDoc);
		overlay.setOutputFile("D:\\final.pdf");
		overlay.setOverlayPosition(Overlay.Position.BACKGROUND);
		try {
			overlay.overlay(overlayGuide, false);
		} catch (COSVisitorException e) {
			e.printStackTrace();
		}*/
	}

	public ByteArrayOutputStream generatePaymentReceipt(Payment payment, Branch branch, String schoolLogoUrl, BranchConfiguration bc) {
		/*		Map<String, String> paymentMap = new LinkedHashMap<String, String>();
				paymentMap.put("Description", StringUtils.isEmpty(payment.getFeesDescription()) ? payment.getAuditDescription().toString() : payment
								.getFeesDescription()
								.toString());
				paymentMap.put("Payment Date", payment.getDate());
				paymentMap.put("Payment Amount", Float.toString(payment.getAmount()));
				paymentMap.put("Payment For", StringUtils.isEmpty(payment.getForPerson()) ? "" : payment.getForPerson());
				paymentMap.put("Qfix Reference Number", StringUtils.isEmpty(payment.getQfixRefNumber()) ? "" : payment.getQfixRefNumber());
				paymentMap.put("Payment channel", StringUtils.isEmpty(payment.getPaymentChannel()) ? "" : payment.getPaymentChannel());
				paymentMap.put("Mode Of Payment", StringUtils.isEmpty(payment.getModeOfPayment()) ? "" : payment.getModeOfPayment());
		*/
		List<List<String>> tableData = new ArrayList<List<String>>();

		List<String> parameter = new ArrayList<String>();

		parameter.add("Description");
		parameter.add("Payment Date");
		parameter.add("Payment Amount");
		parameter.add("Payment By");
		parameter.add("Qfix Reference Number");
		parameter.add("Payment channel");
		parameter.add("Mode Of Payment");
		List<String> values = new ArrayList<String>();

		values.add((!StringUtils.isEmpty(payment.getFeesDescription()) ? payment.getFeesDescription() : payment.getAuditDescription()));
		values.add(payment.getDate());
		values.add(payment.getAmount() + "");

		if (!StringUtils.isEmpty(payment.getForPerson())) {
			values.add(payment.getForPerson());
		} else {
			values.add("");
		}

		if (!StringUtils.isEmpty(payment.getQfixRefNumber())) {
			values.add(payment.getQfixRefNumber());
		} else {
			values.add("");
		}

		if (!StringUtils.isEmpty(payment.getPaymentChannel())) {
			values.add(payment.getPaymentChannel());
		} else {
			values.add("");
		}

		if (!StringUtils.isEmpty(payment.getPaymentChannel())) {
			values.add(payment.getModeOfPayment());
		} else {
			values.add("");
		}
		tableData.add(parameter);
		tableData.add(values);

		System.out.println(values.toString());

		return generate(tableData, null, schoolLogoUrl, bc, branch);
	}

	public ByteArrayOutputStream generate(List<List<String>> tableData, Map<String, String> paymentMap, String schoolLogoUrl, BranchConfiguration bc,
					Branch branch) {
		ByteArrayOutputStream output = null;
		PDDocument doc = null;
		PDPage page = null;
		try {
			doc = new PDDocument();
			page = new PDPage();

			doc.addPage(page);
			PDFont font = PDType1Font.HELVETICA_BOLD;

			PDPageContentStream content = new PDPageContentStream(doc, page);
			//draw logo image
			boolean flag = false;
			String branchName = branch.getName();
			String address = branch.getAddressLine() + ", " + (!StringUtils.isEmpty(branch.getArea()) ? branch.getArea() + ", " : "") + branch.getCity()
							+ " Pin-" + branch.getPincode();
			String trailorRecord = "Thanks for the payment.";
			if (bc != null) {
				flag = "Y".equals(bc.getHideLogo()) ? true : false;
				trailorRecord = bc.getTrailerRecord();
			}
			drawLogo(doc, content, schoolLogoUrl, flag);
			//draw branch name
			content.beginText();
			content.setFont(font, 14);
			content.moveTextPositionByAmount(180, 770);
			content.drawString(branchName);
			content.endText();
			//draw address line
			content.beginText();
			content.setFont(font, 9);
			content.moveTextPositionByAmount(200, 755);
			content.drawString(address);
			content.endText();

			content.beginText();
			content.setFont(font, 9);
			content.moveTextPositionByAmount(180, 500);
			content.drawString(trailorRecord);
			content.endText();

			//table for braek down
			InvoiceReceiptTableGenerator tableGenerator = new InvoiceReceiptTableGenerator();

			content = tableGenerator.generateTable(content, 690, tableData);

			content.close();

			output = new ByteArrayOutputStream();
			//doc.save("reciept.pdf");
			doc.save(output);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				try {
					doc.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return output;
	}

	private PDXObjectImage getImageObj(String image, PDDocument doc) throws IOException {
		PDXObjectImage xImage = null;
		InputStream in = null;
		try {
			if (image.toLowerCase().endsWith(".jpg"))
			{
				in = new FileInputStream(image);
				xImage = new PDJpeg(doc, in);
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (RandomAccess) new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}
		} catch (Exception e) {
			System.err.println("Image not Found at ::" + image);
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return xImage;
	}

	private void drawLogo(PDDocument doc, PDPageContentStream content, String schoolLogoUrl, boolean flag) throws IOException {
		try {
			PDXObjectImage xImageQfixLogo = null;
			PDXObjectImage xImageSchoolLogo = null;

			String schoolLogoPath = null;
			String qfixLogoPath = FileStorePathAssembler.QFIX_LOGO_IMAGE_RELATIVE_PATH;

			if (!StringUtils.isEmpty(schoolLogoUrl)) {
				schoolLogoPath = FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH + schoolLogoUrl;
			}

			xImageQfixLogo = getImageObj(qfixLogoPath, doc);
			if (xImageQfixLogo != null && !flag) {
				content.drawXObject(xImageQfixLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX()) - 120,
								(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 90, 140, 100);
			}
			if (!StringUtils.isEmpty(schoolLogoPath)) {
				xImageSchoolLogo = getImageObj(schoolLogoPath, doc);
				if (xImageSchoolLogo != null) {
					content.drawXObject(xImageSchoolLogo, (PDPage.PAGE_SIZE_A4.getUpperRightX()) - 595,
									(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 90, 140, 100);
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Logo not Found...");
			e.printStackTrace();
		}
	}

	// Note that this code works ONLY with jpg files
	public static void main1(String[] args) {
		PDDocument doc = null;
		try {
			/*
			 * Step 1: Prepare the document.
			 */
			doc = new PDDocument();
			PDPage page = new PDPage();
			doc.addPage(page);

			/*
			 * Step 2: Prepare the image PDJpeg is the class you use when
			 * dealing with jpg images. You will need to mention the jpg file
			 * and the document to which it is to be added Note that if you
			 * complete these steps after the creating the content stream the
			 * PDF file created will show "Out of memory" error.
			 */

			PDXObjectImage image = null;
			image = new PDJpeg(doc, new FileInputStream("c:\\Temp\\images\\Qfix\\documents\\External\\now.jpg"));

			/*
			 * Create a content stream mentioning the document, the page in the
			 * dcoument where the content stream is to be added. Note that this
			 * step has to be completed after the above two steps are complete.
			 */
			PDPageContentStream content = new PDPageContentStream(doc, page);

			/*
			 * Step 3: Add (draw) the image to the content stream mentioning the
			 * position where it should be drawn and leaving the size of the
			 * image as it is
			 */
			content.drawImage(image, 20, 20);
			content.close();

			/*
			 * Step 4: Save the document as a pdf file mentioning the name of
			 * the file
			 */

			doc.save("C:\\Temp\\images\\Qfix\\documents\\external\\PDFWithText.pdf");

			doc.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}