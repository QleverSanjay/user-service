package com.isg.service.user.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentResult {

	private Integer id;
	private String studentName;
	private String instituteName;
	private Integer instituteId;
	private String branchName;
	private Integer branchId;
	private String semester;
	private String standard;
	private Integer standardId;
	private String division;
	private Integer divisionId;
	private String totalMarks;
	private String grossTotal;//
	private String finalGrade;//
	private String result;//
	private String position;//
	private String generalRemark;//
	private String roleNumber;
	private List<Subject> subjectList;
	private String year;
	private Date createdDate;
	private String logoUrl;
	private boolean hasSubjectList = true;
	private Integer resultTypeId;
	private String title;//
	
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getResultTypeId() {
		return resultTypeId;
	}

	public void setResultTypeId(Integer resultTypeId) {
		this.resultTypeId = resultTypeId;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String imageUrl) {
		this.logoUrl = imageUrl;
	}

	public String getRoleNumber() {
		return roleNumber;
	}

	public void setRoleNumber(String roleNumber) {
		this.roleNumber = roleNumber;
	}

	public String getPosition() {
		return position;
	}

	public List<Subject> getSubjectList() {
		if(subjectList == null)
		{
			subjectList = new ArrayList<Subject>();
		}
		return subjectList;
	}

	public boolean hasSubjectList()
	{
		if(subjectList == null)
		{
			hasSubjectList = false;
			return hasSubjectList;
		}
		else
		{
			return hasSubjectList;
		}
	}
	public void setSubjectList(List<Subject> subjectList) {
		this.subjectList = subjectList;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		if (createdDate != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(createdDate);
			setYear(calendar.get(Calendar.YEAR) + "");
		}
		this.createdDate = createdDate;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getGeneralRemark() {
		return generalRemark;
	}

	public void setGeneralRemark(String generalRemark) {
		this.generalRemark = generalRemark;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	/*public String getTotalMarks() {
		return totalMarks;
	}*/

	public void setTotalMarks(String totalMarks) {
		this.totalMarks = totalMarks;
	}

	public String getGrossTotal() {
		return grossTotal;
	}

	public void setGrossTotal(String grossTotal) {
		this.grossTotal = grossTotal;
	}

	public String getFinalGrade() {
		return finalGrade;
	}

	public void setFinalGrade(String finalGrade) {
		this.finalGrade = finalGrade;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "StudentResult [id=" + id + ", studentName=" + studentName
				+ ", instituteName=" + instituteName + ", instituteId="
				+ instituteId + ", branchName=" + branchName + ", branchId="
				+ branchId + ", semester=" + semester + ", standard="
				+ standard + ", standardId=" + standardId + ", division="
				+ division + ", divisionId=" + divisionId + ", totalMarks="
				+ totalMarks + ", grossTotal=" + grossTotal + ", finalGrade="
				+ finalGrade + ", result=" + result + ", position=" + position
				+ ", generalRemark=" + generalRemark + ", roleNumber="
				+ roleNumber + ", subjectList=" + subjectList + ", year="
				+ year + ", createdDate=" + createdDate + "]";
	}

}
