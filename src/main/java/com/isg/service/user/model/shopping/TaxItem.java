package com.isg.service.user.model.shopping;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

public class TaxItem {

	@JacksonXmlProperty(isAttribute = true)
	private String label;

	@JacksonXmlText
	private String item;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public float getItem() {
		return (item != null) ? Float.parseFloat(item.substring(1, item.length()).replaceAll(",", "")) : 0;
	}

	public void setItem(String item) {
		this.item = item;
	}

	@Override
	public String toString() {
		return "TaxItem [label=" + label + ", item=" + item + "]";
	}

}
