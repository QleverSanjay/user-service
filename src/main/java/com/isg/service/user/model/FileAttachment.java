package com.isg.service.user.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.util.FileStorePathAssembler;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FileAttachment {

	@NotNull
	@Pattern(regexp = FileStorePathAssembler.ATTACHEMENT_PATTERN, message = "This type of file attached is not supported.")
	String fileNameWithExtension;

	@NotNull
	byte[] bytes;

	@JsonIgnore
	public String getFileNameWithExtension() {
		return fileNameWithExtension;
	}

	@JsonProperty("file_name_with_extension")
	public void setFileNameWithExtension(String fileNameWithExtension) {
		this.fileNameWithExtension = fileNameWithExtension;
	}

	@JsonIgnore
	public byte[] getBytes() {
		return bytes;
	}

	@JsonProperty("data")
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

}
