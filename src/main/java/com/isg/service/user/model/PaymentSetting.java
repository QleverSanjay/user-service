package com.isg.service.user.model;

public class PaymentSetting {

	Integer paymentGatewayId;

	float internetHandlingChargesAmount;

	float internetHandlingChargesPercentage;

	Integer branchId;
	boolean offlinePaymentsEnabled;

	public PaymentSetting(Integer paymentGatewayId, float internetHandlingChargesAmount, float internetHandlingChargesPercentage, Integer instituteId,
					boolean offlinePaymentsEnabled) {
		super();
		this.paymentGatewayId = paymentGatewayId;
		this.internetHandlingChargesAmount = internetHandlingChargesAmount;
		this.internetHandlingChargesPercentage = internetHandlingChargesPercentage;
		this.branchId = instituteId;
		this.offlinePaymentsEnabled = offlinePaymentsEnabled;
	}

	public Integer getPaymentGatewayId() {
		return paymentGatewayId;
	}

	public float getInternetHandlingChargesAmount() {
		return internetHandlingChargesAmount;
	}

	public float getInternetHandlingChargesPercentage() {
		return internetHandlingChargesPercentage;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public boolean getOfflinePaymentsEnabled() {
		return offlinePaymentsEnabled;
	}

}
