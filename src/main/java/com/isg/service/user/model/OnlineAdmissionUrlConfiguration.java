package com.isg.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.request.Branch;
import com.isg.service.user.response.OnlineAdmissionConfiguration;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineAdmissionUrlConfiguration {

	Branch branch;

	String fromDate;

	String toDate;

	String academicYear;

	Integer urlConfigurationId;

	@JsonIgnore
	Integer academicYearId;

	@JsonIgnore
	Integer courseId;

	@JsonProperty("online_admission_configuration")
	private OnlineAdmissionConfiguration onlineAdmissionConfiguration;

	public OnlineAdmissionConfiguration getOnlineAdmissionConfiguration() {
		return onlineAdmissionConfiguration;
	}

	public void setOnlineAdmissionConfiguration(OnlineAdmissionConfiguration onlineAdmissionConfiguration) {
		this.onlineAdmissionConfiguration = onlineAdmissionConfiguration;
	}

	@JsonProperty("branch_detail")
	public Branch getBranch() {
		return branch;
	}

	@JsonIgnore
	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@JsonProperty("from_date")
	public String getFromDate() {
		return fromDate;
	}

	@JsonIgnore
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	@JsonProperty("to_date")
	public String getToDate() {
		return toDate;
	}

	@JsonIgnore
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	@JsonProperty("academic_year")
	public String getAcademicYear() {
		return academicYear;
	}

	@JsonIgnore
	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	@JsonProperty("url_configuration_id")
	public Integer getUrlConfigurationId() {
		return urlConfigurationId;
	}

	@JsonIgnore
	public void setUrlConfigurationId(Integer urlConfigurationId) {
		this.urlConfigurationId = urlConfigurationId;
	}

	public Integer getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Integer academicYearId) {
		this.academicYearId = academicYearId;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

}
