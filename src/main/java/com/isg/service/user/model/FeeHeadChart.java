package com.isg.service.user.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeeHeadChart {
	
	@JsonProperty(value = "head")
	String[] heads;
	@JsonProperty(value = "statDesc")
	String[] statDescArr = {"Total", "Paid", "Due"};
	@JsonProperty(value = "data")
	int[][] data;
	
	public String[] getHeads() {
		return heads;
	}
	public void setHeads(String[] heads) {
		this.heads = heads;
	}
	public String[] getStatDescArr() {
		return statDescArr;
	}
	public void setStatDescArr(String[] statDescArr) {
		this.statDescArr = statDescArr;
	}
	public int[][] getData() {
		return data;
	}
	public void setData(int[][] data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "FeeHeadChart [heads=" + Arrays.toString(heads) + ", statDesc="
				+ Arrays.toString(statDescArr) + ", data=" + Arrays.deepToString(data)
				+ "]";
	}
	
	
	
}
