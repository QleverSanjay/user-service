package com.isg.service.user.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.util.StringUtils;

import com.isg.service.user.request.Event;
import com.isg.service.user.request.EventOtherDetails;
import com.isg.service.user.util.Constant;

public class PersonalEventExtractingRowCallbackHandler implements RowCallbackHandler {

	private static final Logger log = Logger.getLogger(PersonalEventExtractingRowCallbackHandler.class);

	private final HashMap<Integer, EventResponse> eventResponseMap;

	private final String defaultRole;

	public PersonalEventExtractingRowCallbackHandler(HashMap<Integer, EventResponse> eventResponseMap, Integer entityId, String defaultRole) {
		this.eventResponseMap = eventResponseMap;
		this.defaultRole = defaultRole;
	}

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		EventResponse eventResponse = null;
		List<Event> eventList = new ArrayList<Event>();
		List<EventRecurrencePattern> eventRecurrencePatternList = null;
		Event event = null;
		EventRecurrencePattern eventRecurrencePattern = null;
		EventOtherDetails otherDetails = null;
		int userId = 0;

		log.debug("Inside PersonalEventExtractingRowCallbackHandler");
		do {

			if (eventResponse == null) {
				userId = rs.getInt("user_id");
				eventResponse = new EventResponse();
				eventResponse.setType(defaultRole.equalsIgnoreCase(rs.getString("role_name")) ? "SELF" : rs.getString("role_name"));
				eventResponse.setIdentifier(rs.getString("role_name") + "-" + rs.getInt("entity_id"));
				eventResponse.setGender(rs.getString("gender"));
				eventResponse.setPhoto(rs.getString("profile_photo") != null ? rs.getString("profile_photo") : null);
				eventResponse.setBelongsToPersonName(rs.getString("profile_name") != null ? rs.getString("profile_name") : null);
			}

			event = new Event();
			event.setCategory(Constant.EventCategory.PERSONAL_TODO.get());
			event.setId("PEROSNAL-EVENT-" + rs.getInt("id"));
			event.setStartDate(rs.getString("event_start_date"));
			event.setStarttime(rs.getString("formatted_start_time"));
			event.setEndDate(rs.getString("event_end_date"));
			event.setEndtime(rs.getString("formatted_end_time"));
			event.setAlldayevent(rs.getString("alldayevent").equals("Y") ? true : false);
			event.setRemindMe(rs.getString("reminder").equals("Y") ? true : false);
			event.setRemindMeBefore(rs.getInt("reminderbefore"));
			event.setTitle(rs.getString("title"));
			event.setVenue(rs.getString("venue"));
			event.setDescription(rs.getString("description"));
			event.setStatus(rs.getString("status"));
			event.setScheduleUpdated(rs.getString("is_schedule_updated"));
			event.setForUserId(rs.getInt("for_user_id"));
			event.setReadStatus(StringUtils.isEmpty(rs.getString("read_status")) ? 'N' : rs.getString("read_status").charAt(0));

			eventRecurrencePattern = new EventRecurrencePattern();
			eventRecurrencePattern.setFrequency(rs.getString("frequency"));
			eventRecurrencePattern.setByDayStr(rs.getString("by_day"));
			eventRecurrencePattern.setByDate(rs.getInt("by_date"));

			eventRecurrencePatternList = new ArrayList<EventRecurrencePattern>();
			eventRecurrencePatternList.add(eventRecurrencePattern);
			event.setEventRecurrencePattern(eventRecurrencePatternList);

			otherDetails = new EventOtherDetails();
			otherDetails.setForStudent(rs.getString("forstudent").equals("Y") ? true : false);
			otherDetails.setStudentId(rs.getInt("student_id") != 0 ? new Integer(rs.getInt("student_id")) : null);
			otherDetails.setStudentGender(rs.getString("gender"));
			event.setOtherDetails(otherDetails);
			eventList.add(event);
		} while (rs.next());

		if (eventList.size() > 0) {
			EventResponse eventResponseObjInMap = eventResponseMap.get(userId);

			if (eventResponseObjInMap != null) {
				List<Event> alreadyFoundEventList = eventResponseObjInMap.getEvents();
				alreadyFoundEventList.addAll(eventList);
			} else {
				eventResponse.setEvents(eventList);
				eventResponseMap.put(userId, eventResponse);
			}
		}

	}

	public HashMap<Integer, EventResponse> getPopulatedMap() {
		return this.eventResponseMap;
	}

}
