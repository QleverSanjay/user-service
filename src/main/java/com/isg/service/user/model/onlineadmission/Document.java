package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "id", "name" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Document {

	@JsonProperty("id")
	@NotNull
	private Long id;

	@JsonProperty("name")
	@NotNull
	private String name;

	@JsonIgnore
	MultipartFile file;

	@JsonProperty("file_path")
	private String filePath;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Document() {
	}

	/**
	 * 
	 * @param id
	 * @param name
	 */
	public Document(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Document [id=" + id + ", name=" + name + ", file="
				+ (file != null ? file.getOriginalFilename() : "null") + "]";
	}

}
