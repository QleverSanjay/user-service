package com.isg.service.user.model;

import java.util.Arrays;

import javax.validation.constraints.NotNull;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventRecurrencePattern {

	public enum Frequency {
		DAILY, WEEKLY, MONTHLY
	}

	@JsonProperty(value = "frequency")
	@NotNull
	String frequency;

	@JsonProperty(value = "by_day")
	String[] byDay;

	@JsonProperty(value = "by_date")
	int byDate;

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String[] getByDay() {
		return byDay;
	}

	public void setByDayStr(String byDay) {
		if (StringUtils.isEmpty(byDay)) {
			this.byDay = new String[] {};
		} else if (byDay.indexOf(",") >= 0) {
			this.byDay = byDay.split(",");
		} else {
			this.byDay = new String[] { byDay };
		}
	}

	public void setByDay(String[] byDay) {
		this.byDay = byDay;
	}

	public int getByDate() {
		return byDate;
	}

	public void setByDate(int byDate) {
		this.byDate = byDate;
	}

	@Override
	public String toString() {
		return "EventRecurrencePattern [frequency=" + frequency + ", byDay=" + Arrays.toString(byDay) + ", byDate=" + byDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + byDate;
		result = prime * result + Arrays.hashCode(byDay);
		result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventRecurrencePattern other = (EventRecurrencePattern) obj;
		if (byDate != other.byDate)
			return false;
		if (!Arrays.equals(byDay, other.byDay))
			return false;
		if (frequency == null) {
			if (other.frequency != null)
				return false;
		} else if (!frequency.equals(other.frequency))
			return false;
		return true;
	}

}
