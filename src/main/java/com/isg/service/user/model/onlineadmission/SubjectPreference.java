package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.Valid;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"preference1",
				"preference2"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubjectPreference {

	@JsonProperty("preference1")
	@Valid
	private Preference preference1;

	@JsonProperty("preference2")
	@Valid
	private Preference preference2;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public SubjectPreference() {
	}

	/**
	 * 
	 * @param preference2
	 * @param preference1
	 */
	public SubjectPreference(Preference preference1, Preference preference2) {
		this.preference1 = preference1;
		this.preference2 = preference2;
	}

	/**
	 * 
	 * @return The preference1
	 */
	@JsonProperty("preference1")
	public Preference getPreference1() {
		return preference1;
	}

	/**
	 * 
	 * @param preference1
	 *            The preference1
	 */
	@JsonProperty("preference1")
	public void setPreference1(Preference preference1) {
		this.preference1 = preference1;
	}

	/**
	 * 
	 * @return The preference2
	 */
	@JsonProperty("preference2")
	public Preference getPreference2() {
		return preference2;
	}

	/**
	 * 
	 * @param preference2
	 *            The preference2
	 */
	@JsonProperty("preference2")
	public void setPreference2(Preference preference2) {
		this.preference2 = preference2;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
