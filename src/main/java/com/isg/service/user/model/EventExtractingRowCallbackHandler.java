package com.isg.service.user.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.util.StringUtils;

import com.isg.service.user.request.Event;
import com.isg.service.user.util.Constant;

public class EventExtractingRowCallbackHandler implements RowCallbackHandler {

	private static final Logger log = Logger.getLogger(EventExtractingRowCallbackHandler.class);

	private final HashMap<Integer, EventResponse> eventResponseMap;

	private final String defaultRole;

	public EventExtractingRowCallbackHandler(HashMap<Integer, EventResponse> eventResponseMap, String defaultRole) {
		this.eventResponseMap = eventResponseMap;
		this.defaultRole = defaultRole;
	}

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		EventResponse eventResponse = null;
		List<Event> eventList = null;
		List<EventRecurrencePattern> eventRecurrencePatternList = null;
		Event event = null;
		EventRecurrencePattern eventRecurrencePattern = null;

		log.debug("Inside EventExtractingRowCallbackHandler");
		do {
			int id = rs.getInt("entity_id");
			int userId = rs.getInt("user_id");

			if (eventResponseMap.containsKey(new Integer(userId))) {
				eventResponse = eventResponseMap.get(new Integer(userId));
			} else {
				eventResponse = new EventResponse();
				eventResponse.setType(defaultRole.equalsIgnoreCase(rs.getString("role_name")) ? "SELF" : rs.getString("role_name"));
				eventResponse.setIdentifier(rs.getString("role_name") + "-" + id);
				eventResponse.setGender(rs.getString("gender"));
				eventResponse.setPhoto(rs.getString("profile_photo"));
				eventResponse.setBelongsToPersonName(rs.getString("profile_name"));

				// Special case handling for parent and student retrieval.
				if ("STUDENT".equalsIgnoreCase(rs.getString("role_name"))) {

					// hack need to work out a proper solution to handle this
					// scenario.

					try {
						eventResponse.setBelongsToPersonName(rs.getString("student_name"));
					} catch (java.sql.SQLException e) {
						// ignore
					}

					try {
						eventResponse.setPhoto(rs.getString("student_photo"));
					} catch (java.sql.SQLException e) {
						// ignore
					}

				}

			}

			eventList = eventResponse.getEvents();

			event = new Event();
			event.setCategory(Constant.EventCategory.SCHOOL_SENT.get());
			event.setId("EVENT-" + rs.getLong("id"));
			event.setStartDate(rs.getString("event_start_date"));
			event.setStarttime(rs.getString("formatted_start_time"));
			event.setEndDate(rs.getString("event_end_date"));
			event.setEndtime(rs.getString("formatted_end_time"));
			event.setAlldayevent(rs.getString("alldayevent").equals("Y") ? true : false);
			event.setRemindMe(rs.getString("reminderSet").equals("Y") ? true : false);
			event.setRemindMeBefore(rs.getInt("reminderbefore"));
			event.setImage(rs.getString("image_url"));
			event.setTitle(rs.getString("title"));
			event.setVenue(rs.getString("venue"));
			event.setDescription(rs.getString("description"));
			event.setSummary(rs.getString("summary"));
			event.setStatus(rs.getString("event_status"));
			event.setScheduleUpdated(rs.getString("is_schedule_updated"));
			event.setForUserId(rs.getInt("for_user_id"));
			event.setReadStatus(StringUtils.isEmpty(rs.getString("read_status")) ? 'N' : rs.getString("read_status").charAt(0));

			eventRecurrencePattern = new EventRecurrencePattern();
			eventRecurrencePattern.setFrequency(rs.getString("frequency"));
			eventRecurrencePattern.setByDayStr(rs.getString("by_day"));
			eventRecurrencePattern.setByDate(rs.getInt("by_date"));

			eventRecurrencePatternList = new ArrayList<EventRecurrencePattern>();
			eventRecurrencePatternList.add(eventRecurrencePattern);
			event.setEventRecurrencePattern(eventRecurrencePatternList);

			eventList.add(event);

			eventResponse.setEvents(eventList);

			eventResponseMap.put(new Integer(userId), eventResponse);
		} while (rs.next());

	}

	public HashMap<Integer, EventResponse> getPopulatedMap() {
		return this.eventResponseMap;
	}

	private static boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();

		for (int x = 1; x <= columns; x++) {
			if (columnName.equals(rsmd.getColumnName(x))) {
				return true;
			}
		}
		return false;
	}

}
