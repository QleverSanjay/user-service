package com.isg.service.user.model;

public class BranchWorkingDay {

	char isMondayWorking;
	char isTuesdayWorking;
	char isWednesdayWorking;
	char isThursdayWorking;
	char isFridayWorking;
	char isSaturdayWorking;
	char isSundayWorking;
	char isRoundRobinTimetable;

	public char getIsMondayWorking() {
		return isMondayWorking;
	}

	public void setIsMondayWorking(char isMondayWorking) {
		this.isMondayWorking = isMondayWorking;
	}

	public char getIsTuesdayWorking() {
		return isTuesdayWorking;
	}

	public void setIsTuesdayWorking(char isTuesdayWorking) {
		this.isTuesdayWorking = isTuesdayWorking;
	}

	public char getIsWednesdayWorking() {
		return isWednesdayWorking;
	}

	public void setIsWednesdayWorking(char isWednesdayWorking) {
		this.isWednesdayWorking = isWednesdayWorking;
	}

	public char getIsThursdayWorking() {
		return isThursdayWorking;
	}

	public void setIsThursdayWorking(char isThursdayWorking) {
		this.isThursdayWorking = isThursdayWorking;
	}

	public char getIsFridayWorking() {
		return isFridayWorking;
	}

	public void setIsFridayWorking(char isFridayWorking) {
		this.isFridayWorking = isFridayWorking;
	}

	public char getIsSaturdayWorking() {
		return isSaturdayWorking;
	}

	public void setIsSaturdayWorking(char isSaturdayWorking) {
		this.isSaturdayWorking = isSaturdayWorking;
	}

	public char getIsSundayWorking() {
		return isSundayWorking;
	}

	public void setIsSundayWorking(char isSundayWorking) {
		this.isSundayWorking = isSundayWorking;
	}

	public char getIsRoundRobinTimetable() {
		return isRoundRobinTimetable;
	}

	public void setIsRoundRobinTimetable(char isRoundRobinTimetable) {
		this.isRoundRobinTimetable = isRoundRobinTimetable;
	}

}
