package com.isg.service.user.model;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventSyncResponse {

	Collection<EventResponse> eventResponse;

	long lastSyncTimeStamp;

	@JsonProperty(value = "data")
	public Collection<EventResponse> getEventResponse() {
		return eventResponse;
	}

	@JsonIgnore
	public void setEventResponse(Collection<EventResponse> eventResponse) {
		this.eventResponse = eventResponse;
	}

	@JsonProperty(value = "last_sync_timestamp")
	public long getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}

	@JsonIgnore
	public void setLastSyncTimeStamp(long lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}

}
