package com.isg.service.user.model.shopping;

import java.util.Arrays;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class OrderPaymentTaxDetails {

	@JacksonXmlProperty(localName = "item")
	@JacksonXmlElementWrapper(useWrapping = false)
	private TaxItem[] taxItem;

	public TaxItem[] getTaxItem() {
		return taxItem;
	}

	public void setTaxItem(TaxItem[] taxItem) {
		this.taxItem = taxItem;
	}

	@Override
	public String toString() {
		return "OrderPaymentTaxDetails [taxItem=" + Arrays.toString(taxItem) + "]";
	}

}
