package com.isg.service.user.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.request.Event;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventResponse {

	@JsonProperty(value = "for")
	@NotNull
	String belongsToPersonName;

	@JsonProperty(value = "type")
	@NotNull
	String type;

	@JsonProperty(value = "gender")
	@NotNull
	String gender;

	@JsonProperty(value = "identifier")
	@NotNull
	String identifier;

	@JsonProperty(value = "photo")
	@NotNull
	String photo;

	@JsonProperty(value = "events")
	List<Event> events = new ArrayList<Event>();

	public String getBelongsToPersonName() {
		return belongsToPersonName;
	}

	public void setBelongsToPersonName(String belongsToPersonName) {
		this.belongsToPersonName = belongsToPersonName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "EventResponse [belongsToPersonName=" + belongsToPersonName + ", type=" + type + ", gender=" + gender + ", identifier=" + identifier
						+ ", photo=" + photo + ", events=" + events + "]";
	}

}
