package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"surname",
				"firstname",
				"middlename"
})
public class Name {

	@JsonProperty("surname")
	private String surname;

	@JsonProperty("firstname")
	@NotNull
	private String firstname;

	@JsonProperty("middlename")
	private String middlename;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Name() {
	}

	/**
	 * 
	 * @param middlename
	 * @param surname
	 * @param firstname
	 */
	public Name(String surname, String firstname, String middlename) {
		this.surname = surname;
		this.firstname = firstname;
		this.middlename = middlename;
	}

	/**
	 * 
	 * @return The surname
	 */
	@JsonProperty("surname")
	public String getSurname() {
		return surname;
	}

	/**
	 * 
	 * @param surname
	 *            The surname
	 */
	@JsonProperty("surname")
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * 
	 * @return The firstname
	 */
	@JsonProperty("firstname")
	public String getFirstname() {
		return firstname;
	}

	/**
	 * 
	 * @param firstname
	 *            The firstname
	 */
	@JsonProperty("firstname")
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * 
	 * @return The middlename
	 */
	@JsonProperty("middlename")
	public String getMiddlename() {
		return middlename;
	}

	/**
	 * 
	 * @param middlename
	 *            The middlename
	 */
	@JsonProperty("middlename")
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
