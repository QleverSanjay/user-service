package com.isg.service.user.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.util.StringUtils;

import com.isg.service.user.request.Event;

public class EdiaryExtractingRowCallbackHandler implements RowCallbackHandler {

	private static final Logger log = Logger.getLogger(EdiaryExtractingRowCallbackHandler.class);

	private final HashMap<Integer, EventResponse> eventResponseMap;

	private final String defaultRole;

	public EdiaryExtractingRowCallbackHandler(HashMap<Integer, EventResponse> eventResponseMap, String defaultRole) {
		this.eventResponseMap = eventResponseMap;
		this.defaultRole = defaultRole;
	}

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		EventResponse eventResponse = null;
		List<Event> eventList = null;
		Event event = null;
		List<EventRecurrencePattern> eventRecurrencePatternList = null;
		EventRecurrencePattern eventRecurrencePattern = null;

		log.debug("Inside EdiaryExtractingRowCallbackHandler");
		do {

			int id = rs.getInt("entity_id");

			if (eventResponseMap.containsKey(new Integer(id))) {
				eventResponse = eventResponseMap.get(new Integer(id));
			} else {
				eventResponse = new EventResponse();
				eventResponse.setType(defaultRole.equalsIgnoreCase(rs.getString("role_name")) ? "SELF" : rs.getString("role_name"));
				eventResponse.setIdentifier(rs.getString("role_name") + "-" + id);
				eventResponse.setGender(rs.getString("gender"));
				eventResponse.setPhoto(rs.getString("profile_photo"));
				eventResponse.setBelongsToPersonName(rs.getString("profile_name"));

			}

			eventList = eventResponse.getEvents();

			event = new Event();

			event.setId("EVENT-" + rs.getInt("id"));
			event.setStartDate(rs.getString("event_start_date"));
			event.setEndDate(rs.getString("event_end_date"));

			event.setAlldayevent(false);
			event.setRemindMe(false);
			event.setImage(rs.getString("image_url"));
			event.setTitle(rs.getString("event_head"));
			// event.setVenue(rs.getString("venue"));
			event.setDescription(rs.getString("description"));
			// event.setSummary(rs.getString("summary"));
			event.setStatus(rs.getString("status"));
			event.setSentBy(rs.getString("sentBy"));
			// event.setScheduleUpdated(rs.getString("is_schedule_updated"));
			event.setForUserId(rs.getInt("for_user_id"));
			event.setReadStatus(StringUtils.isEmpty(rs.getString("read_status")) ? 'N' : rs.getString("read_status").charAt(0));

			eventRecurrencePattern = new EventRecurrencePattern();

			eventRecurrencePatternList = new ArrayList<EventRecurrencePattern>();
			eventRecurrencePatternList.add(eventRecurrencePattern);
			event.setEventRecurrencePattern(eventRecurrencePatternList);

			eventList.add(event);

			eventResponse.setEvents(eventList);

			eventResponseMap.put(new Integer(id), eventResponse);
		} while (rs.next());

	}

	public HashMap<Integer, EventResponse> getPopulatedMap() {
		return this.eventResponseMap;
	}

}
