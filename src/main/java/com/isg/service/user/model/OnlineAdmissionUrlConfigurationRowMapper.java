package com.isg.service.user.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.isg.service.user.request.Branch;

public class OnlineAdmissionUrlConfigurationRowMapper implements RowMapper<OnlineAdmissionUrlConfiguration> {

	@Override
	public OnlineAdmissionUrlConfiguration mapRow(ResultSet rs, int rowNum) throws SQLException {
		OnlineAdmissionUrlConfiguration onlineAdmissionUrlConfiguration = new OnlineAdmissionUrlConfiguration();
		onlineAdmissionUrlConfiguration.setFromDate(rs.getString("from_date"));
		onlineAdmissionUrlConfiguration.setToDate(rs.getString("to_date"));
		Branch branch = new Branch();
		branch.setId(rs.getInt("id"));
		branch.setName(rs.getString("name"));
		branch.setAddressLine(rs.getString("address_line"));
		branch.setArea(rs.getString("area"));
		branch.setCity(rs.getString("city"));
		branch.setPincode(rs.getString("pincode"));
		branch.setContactnumber(rs.getString("contactNumber"));
		branch.setContactemail(rs.getString("contactEmail"));
		branch.setWebsiteurl(rs.getString("websiteUrl"));
		onlineAdmissionUrlConfiguration.setBranch(branch);

		onlineAdmissionUrlConfiguration.setAcademicYear(rs.getString("academic_year"));
		onlineAdmissionUrlConfiguration.setUrlConfigurationId(rs.getInt("url_configuration_id"));
		onlineAdmissionUrlConfiguration.setAcademicYearId(rs.getInt("academic_year_id"));

		return onlineAdmissionUrlConfiguration;
	}

}
