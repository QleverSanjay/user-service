package com.isg.service.user.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ShoppingProfile {

	@JsonIgnore
	private String username;

	@JsonIgnore
	private String password;

	@JsonIgnore
	private Integer shoppingProfileId;

	@JsonIgnore
	private Integer userId;

	private String cookies;

	Date cookieExpiresOn;

	public ShoppingProfile() {

	}

	public ShoppingProfile(String username, String password, Integer shoppingProfileId) {
		super();
		this.username = username;
		this.password = password;
		this.shoppingProfileId = shoppingProfileId;
	}

	public ShoppingProfile(String username, String password, Integer shoppingProfileId, Integer userId) {
		super();
		this.username = username;
		this.password = password;
		this.shoppingProfileId = shoppingProfileId;
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getShoppingProfileId() {
		return shoppingProfileId;
	}

	public void setShoppingProfileId(Integer shoppingProfileId) {
		this.shoppingProfileId = shoppingProfileId;
	}

	@JsonProperty("cookies")
	public String getCookies() {
		return cookies;
	}

	@JsonIgnore
	public void setCookies(String cookies) {
		this.cookies = cookies;
	}

	public Date getCookieExpiresOn() {
		return cookieExpiresOn;
	}

	public void setCookieExpiresOn(Date cookieExpiresOn) {
		this.cookieExpiresOn = cookieExpiresOn;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "ShoppingProfile [shoppingProfileId=" + shoppingProfileId + ", cookies=" + cookies + "]";
	}

}
