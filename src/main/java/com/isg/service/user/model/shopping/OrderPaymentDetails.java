package com.isg.service.user.model.shopping;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "totals")
public class OrderPaymentDetails {
	@JacksonXmlProperty(localName = "subtotal")
	private String amount;

	@JacksonXmlProperty(localName = "shipping")
	private String shippingCharges;

	@JacksonXmlProperty(localName = "tax")
	@JacksonXmlElementWrapper(useWrapping = false)
	private OrderPaymentTaxDetails tax;

	public float getAmount() {
		return (amount != null) ? Float.parseFloat(amount.substring(1, amount.length()).replaceAll(",", "")) : 0;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public float getShippingCharges() {
		return (shippingCharges != null) ? Float.parseFloat(shippingCharges.substring(1, shippingCharges.length()).replaceAll(",", "")) : 0;
	}

	public void setShippingCharges(String shippingCharges) {
		this.shippingCharges = shippingCharges;
	}

	public OrderPaymentTaxDetails getTax() {
		return tax;
	}

	public void setTax(OrderPaymentTaxDetails tax) {
		this.tax = tax;
	}

	@Override
	public String toString() {
		return "OrderPaymentDetails [amount=" + amount + ", shippingCharges=" + shippingCharges + ", tax=" + tax + "]";
	}

}
