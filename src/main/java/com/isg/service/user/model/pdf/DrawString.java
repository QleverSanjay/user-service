package com.isg.service.user.model.pdf;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

public class DrawString {

	/**
	 * @param args
	 * @throws IOException
	 *             This method is use to draw string on contentStream
	 */
	public PDPageContentStream drawString(PDPageContentStream contentStream, float x, float y, PDFont font, float size, String text) throws IOException {
		contentStream.beginText();
		contentStream.moveTextPositionByAmount(x, y);
		contentStream.setFont(font, size);
		contentStream.drawString(text);
		contentStream.endText();
		return contentStream;
	}
}
