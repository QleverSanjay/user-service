package com.isg.service.user.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.client.Request;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentDetail implements Request {

	@JsonProperty(value = "id")
	Integer id;

	@JsonProperty(value = "qfix_reference_number")
	String qfixReferenceNumber;

	@JsonProperty(value = "amount")
	Double amount;

	@JsonProperty(value = "option_name")
	String optionName;

	@JsonProperty(value = "internet_handling_charges")
	Double internetHandlingCharges;

	@JsonProperty(value = "payment_gateway_configuration_id")
	Integer paymentGatewayConfigurationId;

	@JsonProperty(value = "customer_name")
	String customerName;

	@JsonProperty(value = "customer_email")
	String customerEmail;

	@JsonProperty(value = "access_code")
	String accessCode;

	@JsonProperty(value = "payment_status_code")
	String paymentStatusCode;

	@JsonProperty(value = "total_amount")
	String totalAmount;

	@JsonIgnore
	Map<String, String> additionalProperties = new HashMap<String, String>();

	@JsonProperty(value = "channel")
	String channel;

	@JsonProperty(value = "payment_tax_type")
	String paymentTaxType;

	@JsonProperty(value = "payment_tax_details")
	List<PaymentDetailTaxComponent> paymentTaxDetails;

	@JsonProperty(value = "shipping_charges")
	String shippingCharges;

	@JsonProperty(value = "payment_for_description")
	String paymentForDescription;

	@JsonProperty(value = "mode_of_payment")
	String modeOfPayment;

	@JsonProperty(value = "customer_phone")
	String customerPhone;

	@JsonProperty(value = "customer_address")
	String customerAddress;

	@JsonProperty(value = "customer_city")
	String customerCity;

	@JsonProperty(value = "customer_state")
	String customerState;

	@JsonProperty(value = "customer_pincode")
	String customerPincode;

	@JsonProperty(value = "customer_country")
	String customerCountry;

	@JsonProperty(value = "split_payment_detail_text")
	String splitPaymentDetailText;

	@JsonProperty(value = "offline_payments_enabled")
	boolean offlinePaymentsEnabled;

	@JsonProperty(value = "payment_mode")
	String paymentMode;

	@JsonProperty(value = "customer_unique_no")
	String customerUniqueNo;

	public String getCustomerUniqueNo() {
		return customerUniqueNo;
	}

	public void setCustomerUniqueNo(String customerUniqueNo) {
		this.customerUniqueNo = customerUniqueNo;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public boolean isOfflinePaymentsEnabled() {
		return offlinePaymentsEnabled;
	}

	public void setOfflinePaymentsEnabled(boolean offlinePaymentsEnabled) {
		this.offlinePaymentsEnabled = offlinePaymentsEnabled;
	}

	public String getSplitPaymentDetailText() {
		return splitPaymentDetailText;
	}

	public void setSplitPaymentDetailText(String splitPaymentDetailText) {
		this.splitPaymentDetailText = splitPaymentDetailText;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getCustomerPincode() {
		return customerPincode;
	}

	public void setCustomerPincode(String customerPincode) {
		this.customerPincode = customerPincode;
	}

	public String getCustomerCountry() {
		return customerCountry;
	}

	public void setCustomerCountry(String customerCountry) {
		this.customerCountry = customerCountry;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public String getShippingCharges() {
		return shippingCharges;
	}

	public void setShippingCharges(String shippingCharges) {
		this.shippingCharges = shippingCharges;
	}

	public String getPaymentTaxType() {
		return paymentTaxType;
	}

	public void setPaymentTaxType(String paymentTaxType) {
		this.paymentTaxType = paymentTaxType;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getPaymentStatusCode() {
		return paymentStatusCode;
	}

	public void setPaymentStatusCode(String paymentStatusCode) {
		this.paymentStatusCode = paymentStatusCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getQfixReferenceNumber() {
		return qfixReferenceNumber;
	}

	public void setQfixReferenceNumber(String qfixReferenceNumber) {
		this.qfixReferenceNumber = qfixReferenceNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getInternetHandlingCharges() {
		return internetHandlingCharges;
	}

	public void setInternetHandlingCharges(Double internetHandlingCharges) {
		this.internetHandlingCharges = internetHandlingCharges;
	}

	public Integer getPaymentGatewayConfigurationId() {
		return paymentGatewayConfigurationId;
	}

	public void setPaymentGatewayConfigurationId(
					Integer paymentGatewayConfigurationId) {
		this.paymentGatewayConfigurationId = paymentGatewayConfigurationId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public Map<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public List<PaymentDetailTaxComponent> getPaymentTaxDetails() {
		return paymentTaxDetails;
	}

	public void setPaymentTaxDetails(
					List<PaymentDetailTaxComponent> paymentTaxDetails) {
		this.paymentTaxDetails = paymentTaxDetails;
	}

	public String getPaymentForDescription() {
		return paymentForDescription;
	}

	public void setPaymentForDescription(String paymentForDescription) {
		this.paymentForDescription = paymentForDescription;
	}

	@Override
	public String toString() {
		return "PaymentDetail [id=" + id + ", qfixReferenceNumber="
						+ qfixReferenceNumber + ", amount=" + amount + ", optionName="
						+ optionName + ", internetHandlingCharges="
						+ internetHandlingCharges + ", paymentGatewayConfigurationId="
						+ paymentGatewayConfigurationId + ", customerName="
						+ customerName + ", customerEmail=" + customerEmail
						+ ", accessCode=" + accessCode + ", paymentStatusCode="
						+ paymentStatusCode + ", totalAmount=" + totalAmount
						+ ", additionalProperties=" + additionalProperties
						+ ", channel=" + channel + ", paymentTaxType=" + paymentTaxType
						+ ", paymentTaxDetails=" + paymentTaxDetails
						+ ", shippingCharges=" + shippingCharges
						+ ", paymentForDescription=" + paymentForDescription
						+ ", modeOfPayment=" + modeOfPayment + ", customerPhone="
						+ customerPhone + ", customerAddress=" + customerAddress
						+ ", customerCity=" + customerCity + ", customerState="
						+ customerState + ", customerPincode=" + customerPincode
						+ ", customerCountry=" + customerCountry
						+ ", splitPaymentDetailText=" + splitPaymentDetailText
						+ ", offlinePaymentsEnabled=" + offlinePaymentsEnabled
						+ ", paymentMode=" + paymentMode + ", customerUniqueNo="
						+ customerUniqueNo + "]";
	}

}