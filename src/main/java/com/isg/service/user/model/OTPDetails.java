package com.isg.service.user.model;

public class OTPDetails {
	private Integer id;
	private String username;
	private String contact;
	private Integer otpNumber;
	private String emailAddress;
	private String otpCreationDate;
	private String otpEndDate;
	private String active;
	
	
	
	public OTPDetails(Integer id, String username, String contact,
			Integer otpNumber, String emailAddress, String otpCreationDate,
			String otpEndDate, String active) {
		
		this.id = id;
		this.username = username;
		this.contact = contact;
		this.otpNumber = otpNumber;
		this.emailAddress = emailAddress;
		this.otpCreationDate = otpCreationDate;
		this.otpEndDate = otpEndDate;
		this.active = active;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public Integer getOtpNumber() {
		return otpNumber;
	}
	public void setOtpNumber(Integer otpNumber) {
		this.otpNumber = otpNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getOtpCreationDate() {
		return otpCreationDate;
	}
	public void setOtpCreationDate(String otpCreationDate) {
		this.otpCreationDate = otpCreationDate;
	}
	public String getOtpEndDate() {
		return otpEndDate;
	}
	public void setOtpEndDate(String otpEndDate) {
		this.otpEndDate = otpEndDate;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
}
