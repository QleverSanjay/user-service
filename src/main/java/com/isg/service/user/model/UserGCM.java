package com.isg.service.user.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserGCM {
	
	@JsonProperty(value="user_id")
	@NotNull
	private Long id;
	
	@JsonProperty(value="gcm_id")
	@NotNull
	private String gcmId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}
	
	@Override
	public String toString() {
		return "UserGCM :: user_id :"+id+" gcmId : "+gcmId;
	}
	

}
