package com.isg.service.user.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.isg.service.user.request.Holiday;

public class HolidayRowCallbackHandler implements RowCallbackHandler {

	private static final Logger log = Logger.getLogger(HolidayRowCallbackHandler.class);

	private final HashMap<Integer, HolidayResponse> holidayResponseMap;

	public HolidayRowCallbackHandler(HashMap<Integer, HolidayResponse> eventResponseMap) {
		this.holidayResponseMap = eventResponseMap;
	}

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		HolidayResponse holidayResponse = null;
		List<Holiday> holidayList = null;

		Holiday holiday = null;

		log.debug("Inside HolidayRowCallbackHandler");
		do {
			int id = rs.getInt("entity_id");
			int userId = rs.getInt("user_id");

			if (holidayResponseMap.containsKey(new Integer(userId))) {
				holidayResponse = holidayResponseMap.get(new Integer(userId));
			} else {
				holidayResponse = new HolidayResponse();
				holidayResponse.setType(rs.getString("role_name"));
				holidayResponse.setIdentifier(rs.getString("role_name") + "-" + id);
			}

			holidayList = holidayResponse.getHolidays();

			holiday = new Holiday();

			holiday.setId("HOLIDAY-" + rs.getInt("id"));
			holiday.setFromDate(rs.getString("from_date"));
			holiday.setToDate(rs.getString("to_date"));
			holiday.setTitle(rs.getString("title"));
			holiday.setDescription(rs.getString("description"));
			holiday.setBranchId(rs.getInt("branch_id"));

			holidayList.add(holiday);

			holidayResponse.setHolidays(holidayList);

			holidayResponseMap.put(new Integer(userId), holidayResponse);
		} while (rs.next());

	}

	public HashMap<Integer, HolidayResponse> getPopulatedMap() {
		return this.holidayResponseMap;
	}

}
