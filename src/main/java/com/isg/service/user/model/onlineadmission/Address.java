package com.isg.service.user.model.onlineadmission;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"address_line1",
				"house_number",
				"area",
				"pincode",
				"country",
				"state",
				"district",
				"taluka"
})
public class Address {

	@JsonProperty("address_line1")
	@NotNull
	private String addressLine1;

	@JsonProperty("house_number")
	private String houseNumber;

	@JsonProperty("area")
	private String area;

	@JsonProperty("city")
	private String city;

	@JsonProperty("pincode")
	private Long pincode;

	@JsonProperty("country")
	@Valid
	private Country country;

	@JsonProperty("state")
	@Valid
	private State state;

	@JsonProperty("district")
	@Valid
	private District district;

	@JsonProperty("taluka")
	@Valid
	private Taluka taluka;

	@JsonIgnore
	private final Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Address() {
	}

	/**
	 * 
	 * @param pincode
	 * @param taluka
	 * @param area
	 * @param state
	 * @param houseNumber
	 * @param district
	 * @param addressLine1
	 * @param country
	 */
	public Address(String addressLine1, String houseNumber, String area, Long pincode, Country country, State state, District district, Taluka taluka) {
		this.addressLine1 = addressLine1;
		this.houseNumber = houseNumber;
		this.area = area;
		this.pincode = pincode;
		this.country = country;
		this.state = state;
		this.district = district;
		this.taluka = taluka;
	}

	/**
	 * 
	 * @return The addressLine1
	 */
	@JsonProperty("address_line1")
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * 
	 * @param addressLine1
	 *            The address_line1
	 */
	@JsonProperty("address_line1")
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 
	 * @return The houseNumber
	 */
	@JsonProperty("house_number")
	public String getHouseNumber() {
		return houseNumber;
	}

	/**
	 * 
	 * @param houseNumber
	 *            The house_number
	 */
	@JsonProperty("house_number")
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	/**
	 * 
	 * @return The area
	 */
	@JsonProperty("area")
	public String getArea() {
		return area;
	}

	/**
	 * 
	 * @param area
	 *            The area
	 */
	@JsonProperty("area")
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * 
	 * @return The pincode
	 */
	@JsonProperty("pincode")
	public Long getPincode() {
		return pincode;
	}

	/**
	 * 
	 * @param pincode
	 *            The pincode
	 */
	@JsonProperty("pincode")
	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	/**
	 * 
	 * @return The country
	 */
	@JsonProperty("country")
	public Country getCountry() {
		return country;
	}

	/**
	 * 
	 * @param country
	 *            The country
	 */
	@JsonProperty("country")
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * 
	 * @return The state
	 */
	@JsonProperty("state")
	public State getState() {
		return state;
	}

	/**
	 * 
	 * @param state
	 *            The state
	 */
	@JsonProperty("state")
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * 
	 * @return The district
	 */
	@JsonProperty("district")
	public District getDistrict() {
		return district;
	}

	/**
	 * 
	 * @param district
	 *            The district
	 */
	@JsonProperty("district")
	public void setDistrict(District district) {
		this.district = district;
	}

	/**
	 * 
	 * @return The taluka
	 */
	@JsonProperty("taluka")
	public Taluka getTaluka() {
		return taluka;
	}

	/**
	 * 
	 * @param taluka
	 *            The taluka
	 */
	@JsonProperty("taluka")
	public void setTaluka(Taluka taluka) {
		this.taluka = taluka;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
