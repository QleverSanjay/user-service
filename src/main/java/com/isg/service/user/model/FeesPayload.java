package com.isg.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeesPayload {

	@JsonProperty("splitPayment")
	private Boolean splitPayment;
	@JsonProperty("partialPaymentAllowed")
	private Boolean partialPaymentAllowed;
	@JsonProperty("fees_id")
	private Integer feesId;
	@JsonProperty("amount")
	private String amount;
	@JsonProperty("head")
	private Object head;
	@JsonProperty("description")
	private String description;

	@JsonProperty("student_id")
	private Integer studentId;

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public Boolean getSplitPayment() {
		return splitPayment;
	}

	public void setSplitPayment(Boolean splitPayment) {
		this.splitPayment = splitPayment;
	}

	public Boolean getPartialPaymentAllowed() {
		return partialPaymentAllowed;
	}

	public void setPartialPaymentAllowed(Boolean partialPaymentAllowed) {
		this.partialPaymentAllowed = partialPaymentAllowed;
	}

	public Integer getFeesId() {
		return feesId;
	}

	public void setFeesId(Integer feesId) {
		this.feesId = feesId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Object getHead() {
		return head;
	}

	public void setHead(Object head) {
		this.head = head;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}