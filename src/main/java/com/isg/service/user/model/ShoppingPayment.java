package com.isg.service.user.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShoppingPayment {

	@NotNull
	@Size(max = 20)
	@JsonProperty("order_id")
	String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		return "ShoppingPayment [orderId=" + orderId + "]";
	}

}
