package com.isg.service.user.model.onlineadmission;

import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AcademicDetail {

	@JsonProperty("exam")
	@Size(min = 1)
	private String exam;

	@JsonProperty("passing_year")
	private String passingYear;

	@JsonProperty("school")
	private String school;

	@JsonProperty("board_university")
	private String boardUniversity;

	@JsonProperty("obtained_marks")
	private Long obtainedMarks;

	@JsonProperty("total_marks")
	private Long totalMarks;

	@JsonProperty("percentage")
	private Double percentage;

	@JsonProperty("subject_marks")
	private Map<String, Double> subjectMarks;
	
	@JsonProperty("department")
	private String department;
	
	
	@JsonProperty("yearOfPassing")
	private String year;
	
	
	

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public AcademicDetail() {
	}

	/**
	 * 
	 * @param school
	 * @param totalMarks
	 * @param percentage
	 * @param boardUniversity
	 * @param passingYear
	 * @param obtainedMarks
	 * @param exam
	 */
	public AcademicDetail(String exam, String passingYear, String school, String boardUniversity, Long obtainedMarks, Long totalMarks, Double percentage) {
		this.exam = exam;
		this.passingYear = passingYear;
		this.school = school;
		this.boardUniversity = boardUniversity;
		this.obtainedMarks = obtainedMarks;
		this.totalMarks = totalMarks;
		this.percentage = percentage;
	}

	public Map<String, Double> getSubjectMarks() {
		return subjectMarks;
	}

	public void setSubjectMarks(Map<String, Double> subjectMarks) {
		this.subjectMarks = subjectMarks;
	}

	/**
	 * 
	 * @return The exam
	 */
	@JsonProperty("exam")
	public String getExam() {
		return exam;
	}

	/**
	 * 
	 * @param exam
	 *            The exam
	 */
	@JsonProperty("exam")
	public void setExam(String exam) {
		this.exam = exam;
	}

	/**
	 * 
	 * @return The passingYear
	 */
	@JsonProperty("passing_year")
	public String getPassingYear() {
		return passingYear;
	}

	/**
	 * 
	 * @param passingYear
	 *            The passing_year
	 */
	@JsonProperty("passing_year")
	public void setPassingYear(String passingYear) {
		this.passingYear = passingYear;
	}

	/**
	 * 
	 * @return The school
	 */
	@JsonProperty("school")
	public String getSchool() {
		return school;
	}

	/**
	 * 
	 * @param school
	 *            The school
	 */
	@JsonProperty("school")
	public void setSchool(String school) {
		this.school = school;
	}

	/**
	 * 
	 * @return The boardUniversity
	 */
	@JsonProperty("board_university")
	public String getBoardUniversity() {
		return boardUniversity;
	}

	/**
	 * 
	 * @param boardUniversity
	 *            The board_university
	 */
	@JsonProperty("board_university")
	public void setBoardUniversity(String boardUniversity) {
		this.boardUniversity = boardUniversity;
	}

	/**
	 * 
	 * @return The obtainedMarks
	 */
	@JsonProperty("obtained_marks")
	public Long getObtainedMarks() {
		return obtainedMarks;
	}

	/**
	 * 
	 * @param obtainedMarks
	 *            The obtained_marks
	 */
	@JsonProperty("obtained_marks")
	public void setObtainedMarks(Long obtainedMarks) {
		this.obtainedMarks = obtainedMarks;
	}

	/**
	 * 
	 * @return The totalMarks
	 */
	@JsonProperty("total_marks")
	public Long getTotalMarks() {
		return totalMarks;
	}

	/**
	 * 
	 * @param totalMarks
	 *            The total_marks
	 */
	@JsonProperty("total_marks")
	public void setTotalMarks(Long totalMarks) {
		this.totalMarks = totalMarks;
	}

	/**
	 * 
	 * @return The percentage
	 */
	@JsonProperty("percentage")
	public Double getPercentage() {
		return percentage;
	}

	/**
	 * 
	 * @param percentage
	 *            The percentage
	 */
	@JsonProperty("percentage")
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
