package com.isg.service.user.model.onlineadmission;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SiblingDetail {

	@JsonProperty("name")
	private Name name;

	@JsonProperty("dateofbirth")
	private String dateofbirth;

	@JsonProperty("placeofbirth")
	private String placeofbirth;

	@JsonProperty("gender")
	private Gender gender;

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@JsonProperty("grade")
	private String grade;

	@JsonProperty("currentschool")
	private String currentSchool;

	@JsonProperty("otherschoolattended")
	private String otherschoolAttended;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public SiblingDetail() {
	}

	public SiblingDetail(Name name, String dateofbirth, String placeofbirth, String currentSchool, String otherschoolAttended) {
		super();
		this.name = name;
		this.dateofbirth = dateofbirth;
		this.placeofbirth = placeofbirth;
		this.currentSchool = currentSchool;
		this.otherschoolAttended = otherschoolAttended;

	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public String getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getPlaceofbirth() {
		return placeofbirth;
	}

	public void setPlaceofbirth(String placeofbirth) {
		this.placeofbirth = placeofbirth;
	}

	public String getCurrentSchool() {
		return currentSchool;
	}

	public void setCurrentSchool(String currentSchool) {
		this.currentSchool = currentSchool;
	}

	public String getOtherschoolAttended() {
		return otherschoolAttended;
	}

	public void setOtherschoolAttended(String otherschoolAttended) {
		this.otherschoolAttended = otherschoolAttended;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
