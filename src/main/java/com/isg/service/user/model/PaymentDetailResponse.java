package com.isg.service.user.model;

import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentDetailResponse {

	Integer id;

	@JsonProperty(value = "qfix_reference_number")
	@NotNull
	@Size(max = 20)
	String qfixReferenceNumber;

	@JsonProperty(value = "amount")
	@NotNull
	@Digits(integer = 11, fraction = 2)
	float amount;

	@JsonProperty(value = "internet_handling_charges")
	@NotNull
	@Digits(integer = 11, fraction = 2)
	float internetHandlingCharges;

	@JsonProperty(value = "total_amount")
	@NotNull
	@Digits(integer = 20, fraction = 2)
	float totalAmount;

	@JsonProperty(value = "payment_gateway_configuration_id")
	@NotNull
	@Digits(integer = 11, fraction = 0)
	Integer paymentGatewayConfigurationId;

	@JsonProperty(value = "customer_name")
	@NotNull
	@Size(max = 100)
	String customerName;

	@JsonProperty(value = "customer_email")
	@NotNull
	@Size(max = 100)
	String customerEmail;

	String paymentStatus;

	@JsonProperty(value = "payment_tax_details")
	List<PaymentDetailTaxComponent> paymentDetailTaxComponent;

	@JsonProperty(value = "access_code")
	@Size(max = 36)
	String accessCode;

	@Size(max = 100)
	String paymentOptionName;

	@Size(max = 20)
	String paymentMode;

	@JsonProperty(value = "transaction_error_code")
	private TransactionErrorCode transactionErrorCode;

	@JsonProperty("total_paid_amount")
	String totalPaidAmount;
	
	
	@JsonProperty(value = "channel")
	@NotNull
	@Size(max = 15)
	String paymentChannel;
	

	public String getPaymentChannel() {
		return paymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

	public String getTotalPaidAmount() {
		return totalPaidAmount;
	}

	public void setTotalPaidAmount(String totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@JsonProperty(value = "payment_status_code")
	public String getPaymentStatus() {
		return paymentStatus;
	}

	public String getQfixReferenceNumber() {
		return qfixReferenceNumber;
	}

	public void setQfixReferenceNumber(String qfixReferenceNumber) {
		this.qfixReferenceNumber = qfixReferenceNumber;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public float getInternetHandlingCharges() {
		return internetHandlingCharges;
	}

	public void setInternetHandlingCharges(float internetHandlingCharges) {
		this.internetHandlingCharges = internetHandlingCharges;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getPaymentGatewayConfigurationId() {
		return paymentGatewayConfigurationId;
	}

	public void setPaymentGatewayConfigurationId(Integer paymentGatewayConfigurationId) {
		this.paymentGatewayConfigurationId = paymentGatewayConfigurationId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public List<PaymentDetailTaxComponent> getPaymentDetailTaxComponent() {
		return paymentDetailTaxComponent;
	}

	public void setPaymentDetailTaxComponent(List<PaymentDetailTaxComponent> paymentDetailTaxComponent) {
		this.paymentDetailTaxComponent = paymentDetailTaxComponent;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	@JsonIgnoreProperties
	public String getPaymentOptionName() {
		return paymentOptionName;
	}

	@JsonProperty(value = "option_name")
	public void setPaymentOptionName(String paymentOptionName) {
		this.paymentOptionName = paymentOptionName;
	}

	@JsonIgnoreProperties
	public String getPaymentMode() {
		return paymentMode;
	}

	@JsonProperty(value = "mode_of_payment")
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	@Override
	public String toString() {
		return "";
	}

}
