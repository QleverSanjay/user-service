package com.isg.service.user.model.onlineadmission;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseDetails {

	@JsonProperty("course")
	@Valid
	private Course course;

	@JsonProperty("seed_programme")
	@Valid
	private SeedProgramme seedProgramme;

	@JsonProperty("subject_preference")
	@Valid
	private SubjectPreference subjectPreference;

	@JsonProperty("subject_preferences")
	@Valid
	private List<SubjectPreference> subjectPreferenceList;

	@JsonProperty("applied")
	private String applied;

	@JsonProperty("admitted")
	private String admitted;

	@JsonProperty("otherCoureDetail")
	String otherCourseDetail;

	@JsonProperty("course_preferences")
	@Valid
	private List<CoursePreference> coursePreferenceList;

	@JsonProperty("otherCoursePreference")
	private Map<String, String> otherCoursePreference;

	@JsonProperty("payment_amount")
	private Double paymentAmount;
	
	@JsonProperty("coordinator")
	private String coordinator;

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getOtherCourseDetail() {
		return otherCourseDetail;
	}

	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	public void setOtherCourseDetail(String otherCourseDetail) {
		this.otherCourseDetail = otherCourseDetail;
	}

	public Map<String, String> getOtherCoursePreference() {
		return otherCoursePreference;
	}

	public void setOtherCoursePreference(Map<String, String> otherCoursePreference) {
		this.otherCoursePreference = otherCoursePreference;
	}

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public CourseDetails() {
	}

	/**
	 * 
	 * @param seedProgramme
	 * @param course
	 * @param subjectPreference
	 */
	public CourseDetails(Course course, SeedProgramme seedProgramme, SubjectPreference subjectPreference, CoursePreference coursePreference) {
		this.course = course;
		this.seedProgramme = seedProgramme;
		this.subjectPreference = subjectPreference;
		//		this.coursePreference = coursePreference;
	}

	/**
	 * 
	 * @return The course
	 */
	@JsonProperty("course")
	public Course getCourse() {
		return course;
	}

	/**
	 * 
	 * @param course
	 *            The course
	 */
	@JsonProperty("course")
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * 
	 * @return The seedProgramme
	 */
	@JsonProperty("seed_programme")
	public SeedProgramme getSeedProgramme() {
		return seedProgramme;
	}

	/**
	 * 
	 * @param seedProgramme
	 *            The seed_programme
	 */
	@JsonProperty("seed_programme")
	public void setSeedProgramme(SeedProgramme seedProgramme) {
		this.seedProgramme = seedProgramme;
	}

	/**
	 * 
	 * @return The subjectPreference
	 */
	@JsonProperty("subject_preference")
	public SubjectPreference getSubjectPreference() {
		return subjectPreference;
	}

	/**
	 * 
	 * @param subjectPreference
	 *            The subject_preference
	 */
	@JsonProperty("subject_preference")
	public void setSubjectPreference(SubjectPreference subjectPreference) {
		this.subjectPreference = subjectPreference;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/**
	 * @return the subjectPreferenceList
	 */
	public List<SubjectPreference> getSubjectPreferenceList() {
		return subjectPreferenceList;
	}

	/**
	 * @param subjectPreferenceList
	 *            the subjectPreferenceList to set
	 */
	public void setSubjectPreferenceList(List<SubjectPreference> subjectPreferenceList) {
		this.subjectPreferenceList = subjectPreferenceList;
	}

	/**
	 * @return the applied
	 */
	public String getApplied() {
		return applied;
	}

	/**
	 * @param applied
	 *            the applied to set
	 */
	public void setApplied(String applied) {
		this.applied = applied;
	}

	/**
	 * @return the admitted
	 */
	public String getAdmitted() {
		return admitted;
	}

	/**
	 * @param admitted
	 *            the admitted to set
	 */
	public void setAdmitted(String admitted) {
		this.admitted = admitted;
	}

	/**
	 * @return the coursePreference
	 */
	public List<CoursePreference> getCoursePreferenceList() {
		return coursePreferenceList;
	}

	/**
	 * @param coursePreference
	 *            the coursePreference to set
	 */
	public void setCoursePreferenceList(List<CoursePreference> coursePreferenceList) {
		this.coursePreferenceList = coursePreferenceList;
	}

	/**
	 * @return the coursePreference
	 */
	/*	public CoursePreference getCoursePreference() {
			return coursePreference;
		}

		*//**
	 * @param coursePreference
	 *            the coursePreference to set
	 */
	/*
	public void setCoursePreference(CoursePreference coursePreference) {
	this.coursePreference = coursePreference;
	}*/

}
