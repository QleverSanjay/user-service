package com.isg.service.user.model;

public class ChatGroup {

	String chatDialogId;

	String ownerUsername;

	String ownerPassword;

	public ChatGroup() {
	}

	public ChatGroup(String chatDialogId, String ownerUsername, String ownerPassword) {
		super();
		this.chatDialogId = chatDialogId;
		this.ownerUsername = ownerUsername;
		this.ownerPassword = ownerPassword;
	}

	public String getChatDialogId() {
		return chatDialogId;
	}

	public void setChatDialogId(String chatDialogId) {
		this.chatDialogId = chatDialogId;
	}

	public String getOwnerUsername() {
		return ownerUsername;
	}

	public void setOwnerUsername(String ownerUsername) {
		this.ownerUsername = ownerUsername;
	}

	public String getOwnerPassword() {
		return ownerPassword;
	}

	public void setOwnerPassword(String ownerPassword) {
		this.ownerPassword = ownerPassword;
	}

}
