package com.isg.service.user.model;

public class GroupUser {

	String chatDialogId;

	String forumId;

	Integer groupId;

	Integer userId;

	public GroupUser(String chatDialogId, Integer groupId) {
		super();
		this.chatDialogId = chatDialogId;
		this.forumId = forumId;
		this.groupId = groupId;
		this.userId = userId;
	}

	public String getChatDialogId() {
		return chatDialogId;
	}

	public void setChatDialogId(String chatDialogId) {
		this.chatDialogId = chatDialogId;
	}

	public String getForumId() {
		return forumId;
	}

	public void setForumId(String forumId) {
		this.forumId = forumId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
