package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"name",
				"candidaterelation",
				"email",
				"primaryphone",
				"highestqualification",
				"employment_details"
})
public class GuardianDetails {

	@JsonProperty("name")
	@Valid
	private Name name;

	@JsonProperty("candidaterelation")
	@NotNull
	private String candidaterelation;

	@JsonProperty("relation")
	private String relation;

	@JsonProperty("email")
	@NotNull
	private String email;
	
	@JsonProperty("primary_phone")
	@NotNull
	private String primaryphone;
	
	@JsonProperty("highest_qualification")
	private String highestqualification;
	
	@JsonProperty("employment_details")
	@Valid
	private EmploymentDetails employmentDetails;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public GuardianDetails() {
	}

	/**
	 * 
	 * @param candidaterelation
	 * @param email
	 * @param name
	 */
	public GuardianDetails(Name name, String candidaterelation, String email) {
		this.name = name;
		this.candidaterelation = candidaterelation;
		this.email = email;
		this.primaryphone = primaryphone;
		this.highestqualification = highestqualification;
		this.employmentDetails = employmentDetails;
	}
	@JsonProperty("employment_details")
	public EmploymentDetails getEmploymentDetails() {
		return employmentDetails;
	}

	/**
	 * 
	 * @param employmentDetails
	 *            The employment_details
	 */
	@JsonProperty("employment_details")
	public void setEmploymentDetails(EmploymentDetails employmentDetails) {
		this.employmentDetails = employmentDetails;
	}


	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public Name getName() {
		return name;
	}

	public String getHighestqualification() {
		return highestqualification;
	}

	public void setHighestqualification(String highestqualification) {
		this.highestqualification = highestqualification;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(Name name) {
		this.name = name;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getPrimaryphone() {
		return primaryphone;
	}

	public void setPrimaryphone(String primaryphone) {
		this.primaryphone = primaryphone;
	}

	/**
	 * 
	 * @return The candidaterelation
	 */
	@JsonProperty("candidaterelation")
	public String getCandidaterelation() {
		return candidaterelation;
	}

	/**
	 * 
	 * @param candidaterelation
	 *            The candidaterelation
	 */
	@JsonProperty("candidaterelation")
	public void setCandidaterelation(String candidaterelation) {
		this.candidaterelation = candidaterelation;
	}

	/**
	 * 
	 * @return The email
	 */
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 *            The email
	 */
	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
