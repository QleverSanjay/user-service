package com.isg.service.user.model.pdf;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.imageio.ImageIO;

import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;

import com.isg.service.user.model.PaymentDetail;

public abstract class ChallanPdfGenerator {

	protected static int PADDING_BETWEEN_RECEIPT = 80;

	protected static int NEFT_RTGS_BOX_HEIGHT = 319;

	protected static int CASH_BOX_HEIGHT = 330;

	protected static int CHEQUE_BOX_HEIGHT = 265;
	protected final float X0 = 25f;
	protected float CONTENT_FONT_SIZE = 7f;

	protected int STANDARD_PADDING = 15;

	PaymentDetail paymentDetail;
	PDDocument doc;
	DrawString drawString = new DrawString();

	protected void initializeData(PaymentDetail PaymentDetail, PDDocument doc) {
		paymentDetail = PaymentDetail;
		this.doc = doc;
	}

	public abstract ByteArrayOutputStream createPdf(PaymentDetail paymentDetail) throws IOException;

	public void createMainContent(PDPageContentStream contentStream, float YPosition,
					float rectangleHeight, boolean drawStudentDetails, String copyFor) throws IOException {

		if (!"N".equals(paymentDetail.getPaymentMode())) {

			contentStream.setStrokingColor(Color.black);
			float rectangleWidth = PDPage.PAGE_SIZE_A4.getUpperRightX() - X0 - 10;
			contentStream.addRect(X0, YPosition, rectangleWidth, rectangleHeight);
			contentStream.closeAndStroke();

			YPosition = drawHeader(contentStream, YPosition, rectangleHeight);

			if (drawStudentDetails) {
				drawString.drawString(contentStream, X0 + STANDARD_PADDING, YPosition -= STANDARD_PADDING, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 1,
								" Student ID: " + paymentDetail.getCustomerUniqueNo());
				drawString.drawString(contentStream, X0 + STANDARD_PADDING, YPosition -= STANDARD_PADDING, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 1,
								" Student Name: " + paymentDetail.getCustomerName());
			}
			else {
				YPosition -= STANDARD_PADDING * 2;
			}

			YPosition = drawFooterContent(contentStream, YPosition, rectangleHeight, copyFor, rectangleWidth);
			YPosition -= 30;
		}
	}

	private float drawHeader(PDPageContentStream contentStream, float YPosition, float rectangleHeight) throws IOException {
		drawImage("C:/Users/lenovo/Desktop/HDFC_Bank_logo.jpeg", X0 + STANDARD_PADDING, YPosition += rectangleHeight - 35, 100, 20,
						contentStream);

		drawString.drawString(contentStream, PDPage.PAGE_SIZE_A4.getUpperRightX() - 200, YPosition + 20, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 1,
						"CORPORATE PRODUCTS AND SERVICES");

		drawImage("C:/Temp/images/qfix-logo.png", X0 + STANDARD_PADDING - 2, YPosition -= 40, 80, 35, contentStream);

		drawString.drawString(contentStream, X0 + STANDARD_PADDING, YPosition -= STANDARD_PADDING, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 1,
						"Client Name:QFIX");

		if ("C".equals(paymentDetail.getPaymentMode())) {
			drawString.drawString(contentStream, X0 + 350, YPosition, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 1,
							"CASH COLLECTIONS");
		}
		drawString.drawString(contentStream, X0 + STANDARD_PADDING, YPosition -= STANDARD_PADDING, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 1,
						" Merchant Ref No.: " + paymentDetail.getQfixReferenceNumber());
		return YPosition;
	}

	private float drawFooterContent(PDPageContentStream contentStream, float YPosition, float rectangleHeight, String copyFor, float rectangleWidth)
					throws IOException {
		if (rectangleHeight == CASH_BOX_HEIGHT) {
			YPosition -= 130;
		}
		YPosition -= PDPage.PAGE_SIZE_A4.getUpperRightY() / 2 - rectangleHeight - 35;

		if ("C".equals(paymentDetail.getPaymentMode())) {
			drawString.drawString(contentStream, X0 + 10, YPosition, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 1,
							"TELLER'S Signature");
		}

		drawString.drawString(contentStream, rectangleWidth / 2 - 40, YPosition, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 1,
						"Depositor's Signature");

		contentStream.setLineDashPattern(new float[] { 3, 1 }, 0);
		int copyForLength = copyFor.length();
		drawString.drawString(contentStream, rectangleWidth - (copyForLength * (copyForLength > 10 ? 3.5f : 2)) + 5, YPosition -= 18,
						PDType1Font.HELVETICA,
						CONTENT_FONT_SIZE + 1,
						copyFor);
		contentStream.drawLine(X0, YPosition -= 12, X0 + rectangleWidth, YPosition);
		contentStream.setLineDashPattern(new float[] { 3, 0 }, 0);
		return YPosition;
	}

	public void drawImage(String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {
		try {
			PDXObject xImage = null;
			String image = path;
			System.out.println("File path to draw image ::: \n" + path);
			if (image.toLowerCase().endsWith(".jpg"))
			{
				xImage = new PDJpeg(doc, new FileInputStream(image));
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (RandomAccess) new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Logo not Found...");
			e.printStackTrace();
		}
	}

	protected PDPage addNewPage() throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}
}
