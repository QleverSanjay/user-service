package com.isg.service.user.model;

public class BranchStudentParentDetail {

	Integer branchId;

	Integer parentId;

	Integer studentId;

	Integer parentUserId;

	Integer studentUserId;

	public BranchStudentParentDetail() {

	}

	public BranchStudentParentDetail(Integer branchId, Integer parentId, Integer studentId, Integer parentUserId, Integer studentUserId) {
		super();
		this.branchId = branchId;
		this.parentId = parentId;
		this.studentId = studentId;
		this.parentUserId = parentUserId;
		this.studentUserId = studentUserId;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public Integer getParentUserId() {
		return parentUserId;
	}

	public void setParentUserId(Integer parentUserId) {
		this.parentUserId = parentUserId;
	}

	public Integer getStudentUserId() {
		return studentUserId;
	}

	public void setStudentUserId(Integer studentUserId) {
		this.studentUserId = studentUserId;
	}

	@Override
	public String toString() {
		return "BranchStudentParentDetail [branchId=" + branchId + ", parentId=" + parentId + ", studentId=" + studentId + "]";
	}

}
