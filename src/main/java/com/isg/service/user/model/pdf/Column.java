package com.isg.service.user.model.pdf;

import java.util.List;

public class Column {

	private List<String> name;
	private List<String> values;
	private float width;

	private String nameStr;

	private String headerName;

	public Column(List<String> name, float width) {
		this.name = name;
		this.width = width;
	}

	public Column(String headerName, List<String> values, float width) {
		this.values = values;
		this.width = width;
		this.headerName = headerName;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public String getHeaderName() {
		return headerName;
	}

	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

	public Column(String nameStr, float width) {
		this.nameStr = nameStr;
		this.width = width;
	}

	public List<String> getName() {
		return name;
	}

	public void setName(List<String> name) {
		this.name = name;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public String getNameStr() {
		return nameStr;
	}

	public void setNameStr(String nameStr) {
		this.nameStr = nameStr;
	}

}
