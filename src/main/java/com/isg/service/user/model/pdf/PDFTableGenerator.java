package com.isg.service.user.model.pdf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.isg.service.user.model.StudentResult;
import com.isg.service.user.model.Subject;

public class PDFTableGenerator {

	private static final PDRectangle PAGE_SIZE = PDPage.PAGE_SIZE_A4;
	private static final float MARGIN = 50;
	private static final boolean IS_LANDSCAPE = false;

	// Font configuration
	private static final PDFont TEXT_FONT = PDType1Font.HELVETICA;
	private static final float FONT_SIZE = 10;

	// Table configuration
	private static final float ROW_HEIGHT = 20;
	private static final float CELL_MARGIN = 2;
	public static float UPPER_CONTENT_HEIGHT;

	int remarksFlag = 0;
	float footerMarginYPosition;

	private Table createSubjectContent(StudentResult studentResult) {
		// Total size of columns must not be greater than table width.
		UPPER_CONTENT_HEIGHT = PDPage.PAGE_SIZE_A4.getUpperRightY() - 200;
		String[][] content;
		List<Column> columns = new ArrayList<Column>();
		// Start print logic
		List<Subject> subjectList = studentResult.getSubjectList();
		content = new String[subjectList.size()][7];
		int i = 0;
		int j = 0;
		int theorycounter = 0, practicalcounter = 0, totalcounter = 0, remarkscounter = 0;
		for (Subject subject : subjectList) {
			if (subject.getTheory() == null) //incrementing counter for checking and deleting column if NA is there in column
			{
				theorycounter++;
			}
			if (subject.getPractical() == null)
			{
				practicalcounter++;
			}
			if (subject.getTheory() == null)
			{
				totalcounter++;
			}
			if (subject.getRemarks() == null)
			{
				remarkscounter++;
			}
		}
		int lengthOfSubject = subjectList.size();
		for (Subject subject : subjectList) { //adding data to content condition - any on row of that column have data

			content[i][j] = (subject.getName() != null) ? ("" + subject.getName()) : "";
			if (theorycounter != lengthOfSubject)
				content[i][++j] = (subject.getTheory() != null) ? subject.getTheory() : "NA";

			if (practicalcounter != lengthOfSubject)
				content[i][++j] = (subject.getPractical() != null) ? subject.getPractical() : "NA";

			if (totalcounter != lengthOfSubject)
				content[i][++j] = (subject.getTheory() != null) ? (Double.parseDouble(subject.getTheory()) + (subject.getPractical() != null ? Double
								.parseDouble(subject.getPractical()) : 0)) + "" : "NA";

			if (remarkscounter != lengthOfSubject)
				content[i][++j] = (subject.getRemarks() != null) ? subject.getRemarks() : "NA";
			//Adjust array counter
			i++;
			j = 0;
		}
		int columnsLength;
		if (subjectList.size() != 0) //add column if it have value , if it contains NA then remove
		{
			columns.add(new Column("Subject Name", 100));
		}
		if (theorycounter == subjectList.size()) {
		} else {
			columns.add(new Column("Theory", 70));
		}
		if (practicalcounter == subjectList.size()) {
		}
		else {
			columns.add(new Column("Practical", 70));
		}
		if (totalcounter == subjectList.size()) {
		} else {
			columns.add(new Column("Total", 70));
		}
		if (remarkscounter == subjectList.size()) {
		} else {
			columns.add(new Column("Remarks", 150));
			remarksFlag = 1; //flag to indicate right alignment is only for remarks
		}

		//Code for make length of all column equal
		columnsLength = (columns.size() == 0) ? 1 : columns.size();
		int equalWidth = (500 / columnsLength); //adjust value according to text 
		//columns.remove(columnsLength-1);
		if (columns.size() != 0)
		{
			for (Column c : columns)
			{
				c.setWidth(equalWidth);
			}
		}

		float tableHeight = ROW_HEIGHT * (subjectList.size() + 1);

		Table table = new TableBuilder().setCellMargin(CELL_MARGIN)
						.setColumns(columns).setContent(content).setHeight(tableHeight)
						.setNumberOfRows(content.length).setRowHeight(ROW_HEIGHT)
						.setMargin(MARGIN).setPageSize(PAGE_SIZE)
						.setEqualWidth(equalWidth)
						.setLandscape(IS_LANDSCAPE).setTextFont(TEXT_FONT)
						.setFontSize(FONT_SIZE).build();
		return table;
	}

	private Table createCategoryContent(StudentResult studentResult) {

		// Total size of columns must not be greater than table width.      	
		List<Column> columns = new ArrayList<Column>();
		columns.add(new Column("Category Name", 120));
		columns.add(new Column("Theory", 70));
		columns.add(new Column("Practical", 70));
		columns.add(new Column("Total", 70));
		columns.add(new Column("Remarks", 150));

		// Start print logic
		List<Subject> subjectList = studentResult.getSubjectList();
		String[][] content = new String[subjectList.size() + 1][7];
		int i = 0;
		int j = 0;
		for (Subject subject : subjectList) {

			content[i][j] = (subject.getName() != null) ? ("" + subject.getName()) : "NA";
			content[i][++j] = (subject.getTheory() != null) ? subject.getTheory() : "NA";
			content[i][++j] = (subject.getPractical() != null) ? subject.getPractical() : "NA";
			content[i][++j] = (subject.getObtainedMarks() != null) ? subject.getObtainedMarks() : "NA";
			content[i][++j] = (subject.getRemarks() != null) ? subject.getRemarks() : "NA";
			//Adjust array counter
			i++;
			j = 0;
		}

		float tableHeight = ROW_HEIGHT * (subjectList.size() + 1);
		//  		UPPER_CONTENT_HEIGHT = PDPage.PAGE_SIZE_A4.getUpperRightY() - ROW_HEIGHT * (subjectList.size() + 2);
		// 		System.out.println("UPPER_CONTENT_HEIGHT = "+UPPER_CONTENT_HEIGHT);
		Table table = new TableBuilder().setCellMargin(CELL_MARGIN)
						.setColumns(columns).setContent(content).setHeight(tableHeight)
						.setNumberOfRows(content.length).setRowHeight(ROW_HEIGHT)
						.setMargin(MARGIN).setPageSize(PAGE_SIZE)
						.setLandscape(IS_LANDSCAPE).setTextFont(TEXT_FONT)
						.setFontSize(FONT_SIZE).build();
		return table;
	}

	// Configures basic setup for the table and draws it page by page
	public PDPage drawTable(PDDocument doc, StudentResult studentResult) throws IOException {
		Table table = createSubjectContent(studentResult);

		// Calculate pagination
		Integer rowsPerPage = table.getContent().length + 1; // subtract

		PDPage page = null;
		page = generatePage(doc, table);
		PDPageContentStream contentStream = generateContentStream(doc, page, table);
		String[][] currentPageContent = getContentForCurrentPage(table, rowsPerPage, 0);
		drawCurrentPage(table, currentPageContent, contentStream);

		//        table = createCategoryContent(studentResult);
		//        // Calculate pagination
		//        rowsPerPage = table.getContent().length + 1; // subtract
		//
		//        contentStream = generateContentStream(doc, page, table);
		//        currentPageContent = getContentForCurrentPage(table, rowsPerPage, 0);
		//        drawCurrentPage(table, currentPageContent, contentStream);

		return page;
	}

	// Draws current page table grid and border lines and content
	private void drawCurrentPage(Table table, String[][] currentPageContent, PDPageContentStream contentStream)
					throws IOException {
		//float tableTopY = table.isLandscape() ? table.getPageSize().getWidth() - table.getMargin() : table.getPageSize().getHeight() - table.getMargin();

		float tableTopY = UPPER_CONTENT_HEIGHT - 20;
		// Draws grid and borders
		drawTableGrid(table, currentPageContent, contentStream, tableTopY);

		// Position cursor to start drawing content
		float nextTextX = table.getMargin() + table.getCellMargin();
		// Calculate center alignment for text in cell considering font height
		float nextTextY = tableTopY - (table.getRowHeight() / 2)
						- ((table.getTextFont().getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * table.getFontSize()) / 4);

		// Write column headers
		writeContentLine(table.getColumnsNamesAsArray(), contentStream, nextTextX, nextTextY, table, PDType1Font.HELVETICA_BOLD, 10f, 1);
		nextTextY -= table.getRowHeight();
		nextTextX = table.getMargin() + table.getCellMargin();

		// Write content
		for (int i = 0; i < currentPageContent.length; i++) {
			writeContentLine(currentPageContent[i], contentStream, nextTextX, nextTextY, table, PDType1Font.HELVETICA, 8f, 0);
			nextTextY -= table.getRowHeight();
			nextTextX = table.getMargin() + table.getCellMargin();
		}

		contentStream.close();
	}

	// Writes the content for one line
	private void writeContentLine(String[] lineContent, PDPageContentStream contentStream, float nextTextX, float nextTextY,
					Table table, PDType1Font fontType, float fontSize, int columnIdentifies) throws IOException {
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			String text = lineContent[i];
			contentStream.beginText();
			contentStream.setFont(fontType,
							fontSize);
			int middleColumnIdentifierFlag = 0;
			if (i == (table.getNumberOfColumns() - 1) && remarksFlag == 1) //drawing for last column at right alignment
			{
				float textWidth = TEXT_FONT.getStringWidth(text) / 1000 * fontSize;
				/*float titleHeight = TEXT_FONT.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
				stream.moveTextPositionByAmount(, page.getMediaBox().getHeight - marginTop - titleheight);*/
				nextTextX += ((table.getEqualWidth() - textWidth) - (table.getCellMargin() + 3));//10 is just for adjusting pixel which reduce the onborder overlaping
				contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
				middleColumnIdentifierFlag = 1;
			}
			else if (i != 0 && middleColumnIdentifierFlag == 0) //Drawing for All middle Column for Center Alignment
			{
				int minus;
				minus = (columnIdentifies == 1) ? 20 : 10; //for Header Adjustment
				nextTextX += ((table.getEqualWidth() / 2) - minus);
				contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
				nextTextX -= ((table.getEqualWidth() / 2) - minus);
			}
			else //Drawing for First column for Left Alignment
			{
				contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
			}
			contentStream.drawString(text != null ? text : "NA");
			contentStream.endText();
			nextTextX += table.getColumns().get(i).getWidth();
		}
	}

	private void drawTableGrid(Table table, String[][] currentPageContent, PDPageContentStream contentStream, float tableTopY)
					throws IOException {
		// Draw row lines
		float nextY = tableTopY;
		int currentPageLength = currentPageContent.length;
		for (int i = 0; i <= currentPageLength + 1; i++) {
			contentStream.drawLine(table.getMargin(), nextY, table.getMargin() + table.getWidth(), nextY);
			nextY -= table.getRowHeight();
		}

		// Draw column lines
		final float tableYLength = table.getRowHeight() + (table.getRowHeight() * currentPageContent.length);
		final float tableBottomY = tableTopY - tableYLength;
		float nextX = table.getMargin();
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			contentStream.drawLine(nextX, tableTopY, nextX, tableBottomY);
			nextX += table.getColumns().get(i).getWidth();
		}
		footerMarginYPosition = tableBottomY;
		if (currentPageLength != 0)
		{
			contentStream.drawLine(nextX, tableTopY, nextX, tableBottomY);
		}
	}

	private String[][] getContentForCurrentPage(Table table, Integer rowsPerPage, int pageCount) {
		int startRange = pageCount * rowsPerPage;
		int endRange = (pageCount * rowsPerPage) + rowsPerPage;
		if (endRange > table.getNumberOfRows()) {
			endRange = table.getNumberOfRows();
		}
		return Arrays.copyOfRange(table.getContent(), startRange, endRange);
	}

	private PDPage generatePage(PDDocument doc, Table table) {
		PDPage page = new PDPage();
		page.setMediaBox(table.getPageSize());
		page.setRotation(table.isLandscape() ? 90 : 0);
		doc.addPage(page);
		return page;
	}

	float getFooterMarginYPosition()
	{
		return (footerMarginYPosition - 10);
	}

	private PDPageContentStream generateContentStream(PDDocument doc, PDPage page, Table table) throws IOException {
		PDPageContentStream contentStream = new PDPageContentStream(doc, page, false, false);
		// User transformation matrix to change the reference when drawing.
		// This is necessary for the landscape position to draw correctly
		if (table.isLandscape()) {
			contentStream.concatenate2CTM(0, 1, -1, 0, table.getPageSize().getWidth(), 0);
		}
		contentStream.setFont(table.getTextFont(), table.getFontSize());
		return contentStream;
	}
}
