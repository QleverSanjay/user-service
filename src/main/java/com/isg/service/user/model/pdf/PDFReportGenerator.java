package com.isg.service.user.model.pdf;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.imageio.ImageIO;

import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1CFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

import com.isg.service.user.model.StudentResult;
import com.isg.service.user.util.ReadPropertiesUtil;



/**
 * 
 * @author sivakumaran Kathamuttu
 * 
 * 
 */
public class PDFReportGenerator {

	// Hold the report font size.
	private final static float FONT_SIZE = 10f;

	// Hold the initial x position.
	private final static float X0 = 50f;
	private PDPage page;
	private PDDocument doc;
	float footerMarginYPosition = 0;
	// Hold the padding bottom of the document.
	private final static float PADDING_BOTTOM_OF_DOCUMENT = 40f;

	/**
	 * This method for include the footer to the each page in pdf document.
	 * 
	 * @param doc
	 *            Set the pdf document.
	 * @param reportName
	 *            Set the report name.
	 * @throws IOException
	 */
	public void addReportFooter(final PDDocument doc, StudentResult studentResult) throws IOException {

		PDPageContentStream footercontentStream = null;
		try {

				footercontentStream = new PDPageContentStream(doc, page, true,true);
				/************************************************/
				int minusYPosition = 20;
				if(studentResult.getGrossTotal() != null)
				{
				footercontentStream.beginText();
				footercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
				footercontentStream.moveTextPositionByAmount( X0, (footerMarginYPosition - minusYPosition));
				footercontentStream.drawString("Total Obtained Marks - ");
				footercontentStream.endText();
				
				footercontentStream.beginText();
				footercontentStream.setFont(PDType1Font.HELVETICA, FONT_SIZE);
				footercontentStream.moveTextPositionByAmount( X0 + 110, (footerMarginYPosition - 20));
				footercontentStream.drawString((studentResult.getGrossTotal() != null ) ? studentResult.getGrossTotal() : "");
				footercontentStream.endText();
				minusYPosition += 20;
				}
				/************************************************/
				if(studentResult.getFinalGrade() != null)
				{
				footercontentStream.beginText();
				footercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
				footercontentStream.moveTextPositionByAmount( X0, (footerMarginYPosition - minusYPosition));
				footercontentStream.drawString("Final Grade - ");
				footercontentStream.endText();
				
				footercontentStream.beginText();
				footercontentStream.setFont(PDType1Font.HELVETICA, FONT_SIZE);
				footercontentStream.moveTextPositionByAmount( X0 + 70, (footerMarginYPosition - minusYPosition));
				footercontentStream.drawString((studentResult.getFinalGrade() != null ) ? studentResult.getFinalGrade() : "");
				footercontentStream.endText();
				minusYPosition += 20;
				}
				/************************************************/
				if(studentResult.getPosition() != null)
				{
				footercontentStream.beginText();
				footercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
				footercontentStream.moveTextPositionByAmount( X0, (footerMarginYPosition - minusYPosition));
				footercontentStream.drawString("Position - ");
				footercontentStream.endText();
				
				footercontentStream.beginText();
				footercontentStream.setFont(PDType1Font.HELVETICA, FONT_SIZE);
				footercontentStream.moveTextPositionByAmount( X0 + 60, (footerMarginYPosition - minusYPosition));
				footercontentStream.drawString((studentResult.getPosition() != null ) ? studentResult.getPosition() : "");
				footercontentStream.endText();
				minusYPosition += 20;
				}
				
				/************************************************/
				if(studentResult.getGeneralRemark() != null)
				{
				footercontentStream.beginText();
				footercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
				footercontentStream.moveTextPositionByAmount( X0, (footerMarginYPosition - minusYPosition));
				footercontentStream.drawString("General Remark - ");
				footercontentStream.endText();
				
				footercontentStream.beginText();
				footercontentStream.setFont(PDType1Font.HELVETICA, FONT_SIZE);
				footercontentStream.moveTextPositionByAmount( X0 + 85, (footerMarginYPosition - minusYPosition));
				footercontentStream.drawString((studentResult.getGeneralRemark() != null ) ? studentResult.getGeneralRemark() : "");
				footercontentStream.endText();
				}
				footercontentStream.close();

			
		} catch (final IOException exception) {
			throw new RuntimeException(exception);
		} finally {

			if (footercontentStream != null) {
				try {
					footercontentStream.close();
				} catch (final IOException exception) {
					throw new RuntimeException(exception);
				}

			}
		}

	}


	public ByteArrayOutputStream generatePDFReport(StudentResult studentResult) throws Exception{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
        doc = new PDDocument();
        try {
        	PDFTableGenerator pdfTableGenerator = new PDFTableGenerator();
        	page = pdfTableGenerator.drawTable(doc, studentResult);
        	footerMarginYPosition = pdfTableGenerator.getFooterMarginYPosition();
			addReportHeader(studentResult);
	        addReportFooter(doc, studentResult);

	        doc.save(output);
		}finally {
            if (doc != null) {
                doc.close();
            }
        }
        //obj.addReportFooter(doc);
        return output;
	}
	
	
	public void addReportHeader(StudentResult studentResult) throws IOException {

		PDPageContentStream headercontentStream = null;
		try {
			headercontentStream = new PDPageContentStream(doc, page, true, true);
			if(studentResult.getLogoUrl() != null){
				try {
					PDXObjectImage xImage = null;
					String image = ReadPropertiesUtil.properties.getProperty("BASE_SYSTEM_PATH")+studentResult.getLogoUrl();
					if( image.toLowerCase().endsWith( ".jpg" ) )
                    {
						xImage = new PDJpeg(doc, new FileInputStream( image ) );
                    }
                    else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
                    {
                    	xImage = new PDCcitt(doc, (RandomAccess) new RandomAccessFile(new File(image),"r"));
                    }
                    else
                    {
                        BufferedImage awtImage = ImageIO.read( new File( image ) );
                        xImage = new PDPixelMap(doc, awtImage);

                    }
					 headercontentStream.drawXObject(xImage,  (PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 180,
					(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 30, 50, 40);
				}
				catch(FileNotFoundException e){
					System.err.println("Logo not Found...");
					e.printStackTrace();
				}
			}
			Color color =  new Color(196, 18, 47);
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.getStandardFont("Algerian"), 18);
			headercontentStream.setNonStrokingColor(color);
			headercontentStream.moveTextPositionByAmount((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) - 120, 
					(PDPage.PAGE_SIZE_A4.getUpperRightY()) - PADDING_BOTTOM_OF_DOCUMENT - 40);
			headercontentStream.drawString((studentResult.getInstituteName() != null )? 
					studentResult.getInstituteName().toUpperCase() : "");
			headercontentStream.endText();
			
			/****************************************************/
			headercontentStream.setNonStrokingColor(Color.BLACK);
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE + 2);
			headercontentStream.moveTextPositionByAmount(X0, (PDPage.PAGE_SIZE_A4.getUpperRightY() 
					- PADDING_BOTTOM_OF_DOCUMENT - 80));
			headercontentStream.drawString("Title - ");
			headercontentStream.endText();
			
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE + 2);
			headercontentStream.moveTextPositionByAmount(X0 + 50, (PDPage.PAGE_SIZE_A4.getUpperRightY() 
					- PADDING_BOTTOM_OF_DOCUMENT - 80));
			headercontentStream.drawString((studentResult.getTitle() ) != null ? studentResult.getTitle() : "");
			headercontentStream.endText();
					
			/****************************************************/
			headercontentStream.setNonStrokingColor(Color.BLACK);
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0, (PDPage.PAGE_SIZE_A4.getUpperRightY() 
					- PADDING_BOTTOM_OF_DOCUMENT - 100));
			headercontentStream.drawString("Branch - ");
			headercontentStream.endText();
			
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0 + 50, (PDPage.PAGE_SIZE_A4.getUpperRightY() 
					- PADDING_BOTTOM_OF_DOCUMENT - 100));
			headercontentStream.drawString((studentResult.getBranchName() ) != null ? studentResult.getBranchName() : "");
			headercontentStream.endText();
			
			/****************************************************/
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0, 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 120));
			headercontentStream.drawString("Standard - ");				
			headercontentStream.endText();
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0 + 55, (PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 120));
			headercontentStream.drawString((studentResult.getStandard() != null ) ? ( studentResult.getStandard() 
			+ (studentResult.getDivision() != null ? " / "+studentResult.getDivision() : "")) : "");
			headercontentStream.endText();
			
			/****************************************************/
			/*headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0, 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 80));
			headercontentStream.drawString("Semester - ");				
			headercontentStream.endText();
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0 + 60, 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 80));
			headercontentStream.drawString((studentResult.getSemester() != null)? studentResult.getSemester() : "");
			headercontentStream.endText();*/
			
			/****************************************************/
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0, 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 140));
			headercontentStream.drawString("Academic Year - ");				
			headercontentStream.endText();
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0 + 80,
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 140));
			headercontentStream.drawString((studentResult.getYear() != null) ? studentResult.getYear() : "");
			headercontentStream.endText();
			
			/****************************************************/
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2), 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 140));
			headercontentStream.drawString("Roll Number - ");				
			headercontentStream.endText();
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 80, 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 140));
			headercontentStream.drawString((studentResult.getRoleNumber() != null) ? studentResult.getRoleNumber() : "");
			headercontentStream.endText();
			
			/****************************************************/
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0, 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 160));
			headercontentStream.drawString("Student Name - ");				
			headercontentStream.endText();
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount(X0 + 80,
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 160));
			headercontentStream.drawString(studentResult.getStudentName());
			headercontentStream.endText();
			
			/****************************************************/
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2), 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 160));
			headercontentStream.drawString("Result - ");				
			headercontentStream.endText();
			
			headercontentStream.beginText();
			headercontentStream.setFont(PDType1Font.HELVETICA_BOLD, FONT_SIZE);
			headercontentStream.moveTextPositionByAmount((PDPage.PAGE_SIZE_A4.getUpperRightX() / 2) + 80, 
					(PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BOTTOM_OF_DOCUMENT - 160));
			headercontentStream.drawString(studentResult.getResult());
			headercontentStream.endText();
			
			/****************************************************/
			headercontentStream.close();				
		
		} catch (final IOException exception) {
			throw new RuntimeException(exception);
		} finally {

			if (headercontentStream != null) {
				try {
					headercontentStream.close();
				} catch (final IOException exception) {
					throw new RuntimeException(exception);
				}

			}
		}

	}

	
	
}
