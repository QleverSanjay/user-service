package com.isg.service.user.model;

public class FeeToBePaidDetail {

	float amount;

	float latePaymentCharges;

	float discountAmount;

	boolean isSplitPayment;

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public boolean isSplitPayment() {
		return isSplitPayment;
	}

	public void setSplitPayment(boolean isSplitPayment) {
		this.isSplitPayment = isSplitPayment;
	}

	public float getLatePaymentCharges() {
		return latePaymentCharges;
	}

	public void setLatePaymentCharges(float latePaymentCharges) {
		this.latePaymentCharges = latePaymentCharges;
	}

	public float getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(float discountAmount) {
		this.discountAmount = discountAmount;
	}

}
