package com.isg.service.user.model;

public class StudentDetail {

	private String name;
	private String rollNo;
	private String division;
	private String standard;
	private String registrationCode;
	private String customerUniqueNo;

	public String getCustomerUniqueNo() {
		return customerUniqueNo;
	}

	public void setCustomerUniqueNo(String customerUniqueNo) {
		this.customerUniqueNo = customerUniqueNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	@Override
	public String toString() {
		return "StudentDetail [name=" + name + ", rollNo=" + rollNo + ", division=" + division + ", standard=" + standard + ", registrationCode="
						+ registrationCode + ", customerUniqueNo=" + customerUniqueNo + "]";
	}

}
