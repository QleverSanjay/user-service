package com.isg.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Group {

	long id;

	String name;

	String description;

	@JsonIgnore
	String chatDialogId;

	@JsonIgnore
	String branchId;

	public Group(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Group(long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Group(long id, String name, String description, String branchId) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.branchId = branchId;
	}

	public Group(long id, String name, String description, String branchId, String chatDialogId) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.chatDialogId = chatDialogId;
		this.branchId = branchId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getChatDialogId() {
		return chatDialogId;
	}

	public void setChatDialogId(String chatDialogId) {
		this.chatDialogId = chatDialogId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", name=" + name + ", description="
						+ description + ", chatDialogId=" + chatDialogId
						+ ", branchId=" + branchId + "]";
	}

}
