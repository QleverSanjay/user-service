package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.Valid;

import lombok.Data;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Data
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"local_address",
				"permanent_address","distanceFromSchool"
})
public class AddressDetails {

	@JsonProperty("local_address")
	@Valid
	private Address localAddress;

	@JsonProperty("permanent_address")
	@Valid
	private Address permanentAddress;
	
	@JsonProperty("native_place_address")
	private String nativePlaceAddress;
	
	@JsonProperty("distanceFromSchool")
	private String distanceFromSchool;
	
	@JsonProperty("schoolLocatedCity")
	private String schoolLocatedCity;
	
	

	public String getSchoolLocatedCity() {
		return schoolLocatedCity;
	}

	public void setSchoolLocatedCity(String schoolLocatedCity) {
		this.schoolLocatedCity = schoolLocatedCity;
	}

	public String getDistanceFromSchool() {
		return distanceFromSchool;
	}

	public void setDistanceFromSchool(String distanceFromSchool) {
		this.distanceFromSchool = distanceFromSchool;
	}

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public AddressDetails() {
	}

	/**
	 * 
	 * @param localAddress
	 * @param permanentAddress
	 */
	public AddressDetails(Address localAddress, Address permanentAddress) {
		this.localAddress = localAddress;
		this.permanentAddress = permanentAddress;
	}

	/**
	 * 
	 * @return The localAddress
	 */
	@JsonProperty("local_address")
	public Address getLocalAddress() {
		return localAddress;
	}

	/**
	 * 
	 * @param localAddress
	 *            The local_address
	 */
	@JsonProperty("local_address")
	public void setLocalAddress(Address localAddress) {
		this.localAddress = localAddress;
	}

	/**
	 * 
	 * @return The permanentAddress
	 */
	@JsonProperty("permanent_address")
	public Address getPermanentAddress() {
		return permanentAddress;
	}

	/**
	 * 
	 * @param permanentAddress
	 *            The permanent_address
	 */
	@JsonProperty("permanent_address")
	public void setPermanentAddress(Address permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
