package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"employer_name",
				"designation",
				"occupation",
				"annual_income",
				"checkno"
})
public class EmploymentDetails {

	@JsonProperty("employer_name")
	private String employerName;

	@JsonProperty("designation")
	private String designation;

	@JsonProperty("annual_income")
	private String annualIncome;

	@JsonProperty("occupation")
	private String occupation;

	@JsonProperty("address")
	private String address;

	@JsonProperty("city")
	private String city;

	@JsonProperty("pincode")
	private String pincode;
	
	@JsonProperty("checkno")
	private String checkno;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public EmploymentDetails() {
	}

	/**
	 * 
	 * @param employerName
	 * @param designation
	 */
	public EmploymentDetails(String employerName, String designation, String occupation, String annualIncome, String address) {
		this.employerName = employerName;
		this.designation = designation;
		this.occupation = occupation;
		this.annualIncome = annualIncome;
		this.address = address;
		this.checkno = checkno;
		
	}

	/**
	 * 
	 * @return The employerName
	 */
	@JsonProperty("employer_name")
	public String getEmployerName() {
		return employerName;
	}

	public String getCheckno() {
		return checkno;
	}

	public void setCheckno(String checkno) {
		this.checkno = checkno;
	}

	/**
	 * 
	 * @param employerName
	 *            The employer_name
	 */
	@JsonProperty("employer_name")
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	/**
	 * 
	 * @return The designation
	 */
	@JsonProperty("designation")
	public String getDesignation() {
		return designation;
	}

	/**
	 * 
	 * @param designation
	 *            The designation
	 */
	@JsonProperty("designation")
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(String annualIncome) {
		this.annualIncome = annualIncome;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
