package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"exam",
				"passing_year",
				"passing_month",
				"school_college",
				"board_university",
				"obtained_marks",
				"total_marks",
				"percentage",
				"education_gap",
				"grade",
				"exam_seat_no",
				"previous_stream",
				"applicantfrom",
				"student_type",
				"school_city"
				
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class QualifyingExamDetails {

	@JsonProperty("exam")
	@Valid
	private Exam exam;

	@JsonProperty("passing_year")
	@NotNull
	private String passingYear;

	@JsonProperty("passing_month")
	@NotNull
	private String passingMonth;

	@JsonProperty("school_college")
	@NotNull
	private String schoolCollege;

	@JsonProperty("board_university")
	@Valid
	private BoardUniversity boardUniversity;

	@JsonProperty("obtained_marks")
	@NotNull
	private Long obtainedMarks;

	@JsonProperty("total_marks")
	@NotNull
	private Long totalMarks;

	@JsonProperty("percentage")
	private Double percentage;

	@JsonProperty("education_gap")
	@NotNull
	private String educationGap;

	@JsonProperty("grade")
	private String grade;

	@JsonProperty("address")
	private String address;

	@JsonProperty("dates_attend")
	private String datesAttend;

	@JsonProperty("exam_seat_no")
	@NotNull
	private String examSeatNo;

	@JsonProperty("previous_stream")
	private PreviousStream previousStream;

	@JsonProperty("applicantfrom")
	@NotNull
	private String applicantfrom;

	@JsonProperty("language_instruction")
	private String languageInstruction;
	
	@JsonProperty("student_type")
	private String studentType;
	
	@JsonProperty("school_city")
	private String schoolCity;
	
	public String getSchoolCity() {
		return schoolCity;
	}

	public void setSchoolCity(String schoolCity) {
		this.schoolCity = schoolCity;
	}

	public String getStudentType() {
		return studentType;
	}

	public void setStudentType(String studentType) {
		this.studentType = studentType;
	}

	public String getLanguageInstruction() {
		return languageInstruction;
	}

	public void setLanguageInstruction(String languageInstruction) {
		this.languageInstruction = languageInstruction;
	}

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public QualifyingExamDetails() {
	}

	/**
	 * 
	 * @param schoolCollege
	 * @param percentage
	 * @param examSeatNo
	 * @param passingYear
	 * @param educationGap
	 * @param previousStream
	 * @param passingMonth
	 * @param totalMarks
	 * @param boardUniversity
	 * @param grade
	 * @param applicantfrom
	 * @param obtainedMarks
	 * @param exam
	 */
	public QualifyingExamDetails(Exam exam, String passingYear, String passingMonth, String schoolCollege, BoardUniversity boardUniversity, Long obtainedMarks,
					Long totalMarks, Double percentage, String educationGap, String grade, String examSeatNo, PreviousStream previousStream,
					String applicantfrom) {
		this.exam = exam;
		this.passingYear = passingYear;
		this.passingMonth = passingMonth;
		this.schoolCollege = schoolCollege;
		this.boardUniversity = boardUniversity;
		this.obtainedMarks = obtainedMarks;
		this.totalMarks = totalMarks;
		this.percentage = percentage;
		this.educationGap = educationGap;
		this.grade = grade;
		this.examSeatNo = examSeatNo;
		this.previousStream = previousStream;
		this.applicantfrom = applicantfrom;
		this.studentType =studentType;
	}

	/**
	 * 
	 * @return The exam
	 */
	@JsonProperty("exam")
	public Exam getExam() {
		return exam;
	}

	/**
	 * 
	 * @param exam
	 *            The exam
	 */
	@JsonProperty("exam")
	public void setExam(Exam exam) {
		this.exam = exam;
	}

	/**
	 * 
	 * @return The passingYear
	 */
	@JsonProperty("passing_year")
	public String getPassingYear() {
		return passingYear;
	}

	/**
	 * 
	 * @param passingYear
	 *            The passing_year
	 */
	@JsonProperty("passing_year")
	public void setPassingYear(String passingYear) {
		this.passingYear = passingYear;
	}

	/**
	 * 
	 * @return The passingMonth
	 */
	@JsonProperty("passing_month")
	public String getPassingMonth() {
		return passingMonth;
	}

	/**
	 * 
	 * @param passingMonth
	 *            The passing_month
	 */
	@JsonProperty("passing_month")
	public void setPassingMonth(String passingMonth) {
		this.passingMonth = passingMonth;
	}

	/**
	 * 
	 * @return The schoolCollege
	 */
	@JsonProperty("school_college")
	public String getSchoolCollege() {
		return schoolCollege;
	}

	/**
	 * 
	 * @param schoolCollege
	 *            The school_college
	 */
	@JsonProperty("school_college")
	public void setSchoolCollege(String schoolCollege) {
		this.schoolCollege = schoolCollege;
	}

	/**
	 * 
	 * @return The boardUniversity
	 */
	@JsonProperty("board_university")
	public BoardUniversity getBoardUniversity() {
		return boardUniversity;
	}

	/**
	 * 
	 * @param boardUniversity
	 *            The board_university
	 */
	@JsonProperty("board_university")
	public void setBoardUniversity(BoardUniversity boardUniversity) {
		this.boardUniversity = boardUniversity;
	}

	/**
	 * 
	 * @return The obtainedMarks
	 */
	@JsonProperty("obtained_marks")
	public Long getObtainedMarks() {
		return obtainedMarks;
	}

	/**
	 * 
	 * @param obtainedMarks
	 *            The obtained_marks
	 */
	@JsonProperty("obtained_marks")
	public void setObtainedMarks(Long obtainedMarks) {
		this.obtainedMarks = obtainedMarks;
	}

	/**
	 * 
	 * @return The totalMarks
	 */
	@JsonProperty("total_marks")
	public Long getTotalMarks() {
		return totalMarks;
	}

	/**
	 * 
	 * @param totalMarks
	 *            The total_marks
	 */
	@JsonProperty("total_marks")
	public void setTotalMarks(Long totalMarks) {
		this.totalMarks = totalMarks;
	}

	/**
	 * 
	 * @return The percentage
	 */
	@JsonProperty("percentage")
	public Double getPercentage() {
		return percentage;
	}

	/**
	 * 
	 * @param percentage
	 *            The percentage
	 */
	@JsonProperty("percentage")
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	/**
	 * 
	 * @return The educationGap
	 */
	@JsonProperty("education_gap")
	public String getEducationGap() {
		return educationGap;
	}

	/**
	 * 
	 * @param educationGap
	 *            The education_gap
	 */
	@JsonProperty("education_gap")
	public void setEducationGap(String educationGap) {
		this.educationGap = educationGap;
	}

	/**
	 * 
	 * @return The grade
	 */
	@JsonProperty("grade")
	public String getGrade() {
		return grade;
	}

	/**
	 * 
	 * @param grade
	 *            The grade
	 */
	@JsonProperty("grade")
	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDatesAttend() {
		return datesAttend;
	}

	public void setDatesAttend(String datesAttend) {
		this.datesAttend = datesAttend;
	}

	/**
	 * 
	 * @return The examSeatNo
	 */
	@JsonProperty("exam_seat_no")
	public String getExamSeatNo() {
		return examSeatNo;
	}

	/**
	 * 
	 * @param examSeatNo
	 *            The exam_seat_no
	 */
	@JsonProperty("exam_seat_no")
	public void setExamSeatNo(String examSeatNo) {
		this.examSeatNo = examSeatNo;
	}

	/**
	 * 
	 * @return The previousStream
	 */
	@JsonProperty("previous_stream")
	public PreviousStream getPreviousStream() {
		return previousStream;
	}

	/**
	 * 
	 * @param previousStream
	 *            The previous_stream
	 */
	@JsonProperty("previous_stream")
	public void setPreviousStream(PreviousStream previousStream) {
		this.previousStream = previousStream;
	}

	/**
	 * 
	 * @return The applicantfrom
	 */
	@JsonProperty("applicantfrom")
	public String getApplicantfrom() {
		return (applicantfrom == null) ? "" : applicantfrom;
	}

	/**
	 * 
	 * @param applicantfrom
	 *            The applicantfrom
	 */
	@JsonProperty("applicantfrom")
	public void setApplicantfrom(String applicantfrom) {
		this.applicantfrom = applicantfrom;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
