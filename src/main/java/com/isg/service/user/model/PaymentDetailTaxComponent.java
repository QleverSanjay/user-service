package com.isg.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentDetailTaxComponent {

	@JsonProperty(value = "code")
	String code;

	@JsonProperty(value = "amount")
	float amount;

	String displayLabel;

	int displayOrder;

	@JsonIgnore
	Integer paymentDetailId;

	public double getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Integer getPaymentDetailId() {
		return paymentDetailId;
	}

	public void setPaymentDetailId(Integer paymentDetailId) {
		this.paymentDetailId = paymentDetailId;
	}

	public String getCode() {
		return code;
	}

	@JsonIgnore
	public void setCode(String code) {
		this.code = code;

	}

	@JsonProperty(value = "display_label")
	public String getDisplayLabel() {
		return displayLabel;
	}

	@JsonIgnore
	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}

	@JsonProperty(value = "display_order")
	public int getDisplayOrder() {
		return displayOrder;
	}

	@JsonIgnore
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

}
