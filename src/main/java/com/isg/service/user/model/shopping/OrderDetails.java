package com.isg.service.user.model.shopping;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "order_details")
public class OrderDetails {

	@JacksonXmlProperty(localName = "totals")
	@JacksonXmlElementWrapper(useWrapping = false)
	private OrderPaymentDetails totals;

	public OrderPaymentDetails getTotals() {
		return totals;
	}

	public void setTotals(OrderPaymentDetails totals) {
		this.totals = totals;
	}

}
