package com.isg.service.user.model;

public class ExpenseDetail {
	
	int year;
	int month;
	int week;
	int sum;
	int forUserId;
	String forUser;
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}

	public int getWeek() {
		return week;
	}
	public void setWeek(int week) {
		this.week = week;
	}
	public int getSum() {
		return sum;
	}
	public void setSum(int sum) {
		this.sum = sum;
	}
	public int getForUserId() {
		return forUserId;
	}
	public void setForUserId(int forUserId) {
		this.forUserId = forUserId;
	}
	public String getForUser() {
		return forUser;
	}
	public void setForUser(String forUser) {
		this.forUser = forUser;
	}
	
	@Override
	public String toString() {
		return "ExpenseDetail [year=" + year + ", month=" + month + ", week="
				+ week + ", sum=" + sum + ", forUserId=" + forUserId
				+ ", forUser=" + forUser + "]";
	}

	
	
}
