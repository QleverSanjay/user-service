package com.isg.service.user.model.pdf;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class InvoiceReceiptTableGenerator {

	/**
	 * @param args
	 */
	PDDocument doc = null;
	PDPage page = null;
	public int RECTANGLE_WIDTH = 535;
	public int RECTANGLE_HEIGHT = 12;
	public float X0 = 40f;
	public float marginX = 5f;
	public float CONTENT_FONT_SIZE = 8;
	public float ROW_WIDTH = 535;
	private final float ROW_HEIGHT = 20;
	int remarksFlag = 0;
	private final PDRectangle PAGE_SIZE = PDPage.PAGE_SIZE_A4;
	private final float MARGIN = 145;
	private final boolean IS_LANDSCAPE = false;
	float LASTHEADERLINEYPOSITION = 0;
	float LASTROWLINEYPOSITION = 0;
	public int headerMaxSize = 0;
	float previousContentY;
	float nextTextY;
	// Font configuration
	private final PDFont TEXT_FONT = PDType1Font.HELVETICA;
	private final float FONT_SIZE = 8;

	// Table configuration
	private final float CELL_MARGIN = 2;
	int rowCounter = 0;
	int maxRowCounter = 0;
	public float UPPER_CONTENT_HEIGHT;
	float titleHeight;
	List<Float> columnSplitPosition = new ArrayList<Float>();
	List<Float> rowYPosition = new ArrayList<Float>();

	public PDPageContentStream generateTable(PDPageContentStream contentStream, float previousContentY, List<List<String>> tableData) throws IOException {

		this.previousContentY = previousContentY - 20;
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream = drawTable(contentStream, tableData);
		return contentStream;
	}

	private PDPageContentStream generateContentStream(PDPageContentStream contentStream, Table table) throws IOException {
		// User transformation matrix to change the reference when drawing.
		// This is necessary for the landscape position to draw correctly
		if (table.isLandscape()) {
			contentStream.concatenate2CTM(0, 1, -1, 0, table.getPageSize().getWidth(), 0);
		}
		contentStream.setFont(table.getTextFont(), table.getFontSize());
		return contentStream;
	}

	private Table createSubjectContent(List<List<String>> tableData) {

		// Total size of columns must not be greater than table width.
		UPPER_CONTENT_HEIGHT = PDPage.PAGE_SIZE_A4.getUpperRightY() - 200;
		String[][] content;
		List<Column> columns = new ArrayList<Column>();

		// Start print logic
		List<String> parameter = tableData.get(0);
		List<String> values = tableData.get(1);
		content = new String[parameter.size()][tableData.size()];
		int i = 0;
		int j = 0;
		for (String data : parameter) {
			content[i][j] = data;
			i++;
		}
		i = 0;
		j = 1;
		for (String data : values) {
			content[i][j] = data;
			i++;
		}

		List<String> firstColumn = new ArrayList<String>();
		firstColumn.add(parameter.get(0));
		List<String> secondColumn = new ArrayList<String>();
		secondColumn.add(values.get(0));

		System.out.println("Size of second column length " + values.get(0).length());

		columns.add(new Column(firstColumn, 120));
		columns.add(new Column(secondColumn, 210));
		columnSplitPosition.add(30f);
		columnSplitPosition.add(50f);

		float tableHeight = ROW_HEIGHT * (2);
		Table table = new TableBuilder().setCellMargin(CELL_MARGIN)
						.setColumns(columns).setContent(content).setHeight(tableHeight)
						.setNumberOfRows(content.length).setRowHeight(ROW_HEIGHT)
						.setMargin(MARGIN).setPageSize(PAGE_SIZE)
						.setEqualWidth(0)
						.setLandscape(IS_LANDSCAPE).setTextFont(TEXT_FONT)
						.setFontSize(FONT_SIZE).build();
		List<Column> columns1 = table.getColumns();
		for (Column column : columns1) {
			headerMaxSize = (column.getName().size() > headerMaxSize) ? column.getName().size() : headerMaxSize;
		}
		return table;
	}

	public PDPageContentStream drawTable(PDPageContentStream contentStream, List<List<String>> tableData) throws IOException {
		Table table = createSubjectContent(tableData);

		// Calculate pagination
		Integer rowsPerPage = table.getContent().length; // subtract
		contentStream = generateContentStream(contentStream, table);
		String[][] currentPageContent = getContentForCurrentPage(table, rowsPerPage, 0);
		drawCurrentPage(table, currentPageContent, contentStream, previousContentY);

		return contentStream;
	}

	private String[][] getContentForCurrentPage(Table table, Integer rowsPerPage, int pageCount) {
		int startRange = pageCount * rowsPerPage;
		int endRange = (pageCount * rowsPerPage) + rowsPerPage;
		if (endRange > table.getNumberOfRows()) {
			endRange = table.getNumberOfRows();
		}
		return Arrays.copyOfRange(table.getContent(), startRange, endRange);
	}

	private void drawCurrentPage(Table table, String[][] currentPageContent, PDPageContentStream contentStream, float previousContentY)
					throws IOException {
		float nextTextX = MARGIN + marginX;
		nextTextY = previousContentY - 30;
		float tableTopY = previousContentY - 20;
		//writeHeaderContentLine(table.getColumns(), contentStream, nextTextX, nextTextY, table, PDType1Font.HELVETICA , 8, 0);
		LASTHEADERLINEYPOSITION = nextTextY;
		rowYPosition.add(LASTHEADERLINEYPOSITION + 10);
		for (int i = 0; i < currentPageContent.length; i++) {

			writeContentLine(currentPageContent[i], contentStream, nextTextX, LASTHEADERLINEYPOSITION, table, PDType1Font.HELVETICA, 9f, 0);
			//nextTextY += table.getRowHeight();
			nextTextX = MARGIN + marginX;
		}

		drawTableGrid(table, currentPageContent, contentStream, tableTopY);
	}

	private void writeContentLine(String[] lineContent, PDPageContentStream contentStream, float nextTextX, float nextTextY1,
					Table table, PDType1Font fontType, float fontSize, int columnIdentifies) throws IOException {

		int rowMaxSize = 0;
		maxRowCounter = 0;
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			rowCounter = 0;
			String text = lineContent[i];
			List<String> splitedText = stringSplit(text, columnSplitPosition.get(i).intValue(), 0);
			contentStream.beginText();
			contentStream.setFont(fontType, fontSize);
			contentStream.moveTextPositionByAmount(nextTextX, nextTextY1);
			rowMaxSize = (splitedText.size() > rowMaxSize) ? splitedText.size() : rowMaxSize;
			for (String lineBylineText : splitedText) {
				contentStream.drawString(lineBylineText != null ? lineBylineText : " ");
				contentStream.appendRawCommands("T*\n");
				contentStream.moveTextPositionByAmount(0, -12);
				rowCounter++;
			}
			maxRowCounter = (rowCounter > maxRowCounter) ? rowCounter : maxRowCounter;
			nextTextX += table.getColumns().get(i).getWidth();
			contentStream.endText();
		}
		nextTextY -= (maxRowCounter * 12);//+ (titleHeight * rowMaxSize);
		LASTHEADERLINEYPOSITION = nextTextY;
		rowYPosition.add(LASTHEADERLINEYPOSITION + 10);
		previousContentY = LASTHEADERLINEYPOSITION + 10;
		LASTROWLINEYPOSITION = nextTextY + 10;
	}

	private void drawTableGrid(Table table, String[][] currentPageContent, PDPageContentStream contentStream, float tableTopY)
					throws IOException {
		// Draw row lines
		float nextY = tableTopY;

		int header = 0;
		List<Column> columns = table.getColumns();
		for (Column column : columns) {
			header = (column.getName().size() > header) ? column.getName().size() : header;
		}
		int currentPageLength = currentPageContent.length;
		for (int i = 0; i <= currentPageLength; i++) {
			contentStream.drawLine(table.getMargin(), rowYPosition.get(0), table.getMargin() + table.getWidth(), rowYPosition.get(0));
			if (i == 0) {
				contentStream.drawLine(table.getMargin(), nextY, table.getMargin() + table.getWidth(), nextY);
				nextY -= (table.getRowHeight());
			}
			else {
				contentStream.drawLine(table.getMargin(), rowYPosition.get(i), table.getMargin() + table.getWidth(), rowYPosition.get(i));
			}
		}

		// Draw column lines
		final float tableYLength = table.getRowHeight() + (table.getRowHeight() * (currentPageLength - 1));
		final float tableBottomY = (tableTopY - tableYLength) + table.getRowHeight();
		float nextX = table.getMargin();
		for (int i = 0; i < table.getNumberOfColumns(); i++) {
			contentStream.drawLine(nextX, tableTopY, nextX, LASTROWLINEYPOSITION);
			nextX += table.getColumns().get(i).getWidth();
		}

		if (currentPageLength != 0)
		{
			contentStream.drawLine(nextX, tableTopY, nextX, LASTROWLINEYPOSITION);
		}
	}

	/*
	 * This method is use to split the text and return it into list
	 * 
	 * */
	int flag;
	public List<String> list = new ArrayList<String>();

	public List<String> stringSplit(String text, int limit, int flag) // method to split text or wrap text
	{

		if (flag == 0) {
			list.clear();
			flag = 1;
		}
		int thislimit = limit;
		int spacePosition = 0, z = 0;
		if (text != null && text.length() > thislimit) {
			if (text.substring(0, thislimit).contains(" ")) {
				for (int i = z; i < thislimit; i++) {
					if (text.charAt(i) == ' ') {
						spacePosition = i;
					}
				}
				if (text.charAt(spacePosition) == ' ') {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition + 1, text.length()), thislimit, 1);
				} else {
					list.add(text.substring(0, spacePosition));
					stringSplit(text.substring(spacePosition, text.length()), thislimit, 1);
				}
			} else {
				list.add(text.substring(0, thislimit));
				stringSplit(text.substring(thislimit, text.length()), thislimit, 1);
			}
		} else {
			list.add(text);
		}
		return list;
		//}

	}

}
