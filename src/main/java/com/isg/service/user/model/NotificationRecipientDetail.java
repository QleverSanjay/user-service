package com.isg.service.user.model;

import java.util.List;

public class NotificationRecipientDetail {

	List<Group> groups;

	List<Contact> contacts;

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

}
