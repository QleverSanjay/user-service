package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"application_form_no",
				"mkcl_registration_no",
				"physical_form_no"
})
public class FormDetails {

	@JsonProperty("application_form_no")
	@NotNull
	private String applicationFormNo;

	@JsonProperty("mkcl_registration_no")
	@NotNull
	private String mkclRegistrationNo;

	@JsonProperty("physical_form_no")
	@NotNull
	private String physicalFormNo;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public FormDetails() {
	}

	/**
	 * 
	 * @param physicalFormNo
	 * @param mkclRegistrationNo
	 * @param applicationFormNo
	 */
	public FormDetails(String applicationFormNo, String mkclRegistrationNo, String physicalFormNo) {
		this.applicationFormNo = applicationFormNo;
		this.mkclRegistrationNo = mkclRegistrationNo;
		this.physicalFormNo = physicalFormNo;
	}

	/**
	 * 
	 * @return The applicationFormNo
	 */
	@JsonProperty("application_form_no")
	public String getApplicationFormNo() {
		return applicationFormNo;
	}

	/**
	 * 
	 * @param applicationFormNo
	 *            The application_form_no
	 */
	@JsonProperty("application_form_no")
	public void setApplicationFormNo(String applicationFormNo) {
		this.applicationFormNo = applicationFormNo;
	}

	/**
	 * 
	 * @return The mkclRegistrationNo
	 */
	@JsonProperty("mkcl_registration_no")
	public String getMkclRegistrationNo() {
		return mkclRegistrationNo;
	}

	/**
	 * 
	 * @param mkclRegistrationNo
	 *            The mkcl_registration_no
	 */
	@JsonProperty("mkcl_registration_no")
	public void setMkclRegistrationNo(String mkclRegistrationNo) {
		this.mkclRegistrationNo = mkclRegistrationNo;
	}

	/**
	 * 
	 * @return The physicalFormNo
	 */
	@JsonProperty("physical_form_no")
	public String getPhysicalFormNo() {
		return physicalFormNo;
	}

	/**
	 * 
	 * @param physicalFormNo
	 *            The physical_form_no
	 */
	@JsonProperty("physical_form_no")
	public void setPhysicalFormNo(String physicalFormNo) {
		this.physicalFormNo = physicalFormNo;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
