package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"id",
				"name",
				"other"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BoardUniversity {

	@JsonProperty("id")
	@NotNull
	private Long id;

	@JsonProperty("name")
	@NotNull
	private String name;

	@JsonProperty("other")
	private String other;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public BoardUniversity() {
	}

	/**
	 * 
	 * @param id
	 * @param other
	 * @param name
	 */
	public BoardUniversity(Long id, String name, String other) {
		this.id = id;
		this.name = name;
		this.other = other;
	}

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The other
	 */
	@JsonProperty("other")
	public String getOther() {
		return other;
	}

	/**
	 * 
	 * @param other
	 *            The other
	 */
	@JsonProperty("other")
	public void setOther(String other) {
		this.other = other;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
