package com.isg.service.user.model.pdf;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.model.FeesInfo;
import com.isg.service.user.model.PaymentDetail;
import com.isg.service.user.request.BranchConfiguration;

public class PaymentChallanGenerator extends ChallanPdfGenerator {

	public static void main(String a[]) throws IOException {
		PaymentChallanGenerator paymentRecipt = new PaymentChallanGenerator();
		String feeInfoStr = " {\"id\":7369,\"qfixReferenceNumber\":\"12495813201041882669\",\"amount\":100.0,\"internetHandlingCharges\":0.0,\"paymentGatewayConfigurationId\":9,\"customerName\":\"Tushar D`Souja\",\"customerEmail\":\"diliphajare10@gmail.com\",\"accessCode\":\"239286b2-f6d5-4085-87ac-a1bce72a2ccc\",\"paymentStatusCode\":\"NEW\",\"totalAmount\":\"100.0\",\"additionalProperties\":{},\"channel\":\"PARENT-WEB\",\"paymentTaxType\":\"T1\",\"paymentTaxDetails\":[],\"shippingCharges\":\"0.0\",\"paymentForDescription\":\"DAILY SATURDAY (Amount:100.0)\",\"customerPhone\":\"8097137625\",\"customerAddress\":\"\",\"customerCity\":\"\",\"customerState\":\"\",\"customerPincode\":\"\",\"customerCountry\":\"IND\",\"splitPaymentDetailText\":\"test_100.0_0.0\",\"offlinePaymentsEnabled\":true,\"paymentMode\":\"H\",\"customerUniqueNo\":\"Dummy First Standard - Dummy A - \"}";
		PaymentDetail info = new ObjectMapper().readValue(feeInfoStr, PaymentDetail.class);

		System.out.println("Pdf is saved in this path ::" + paymentRecipt.createPdf(info));
	}

	FeesInfo feeInfo;
	String schoolLogoUrl;
	BranchConfiguration branchConfiguration;

	@Override
	public ByteArrayOutputStream createPdf(PaymentDetail paymentDetail) throws IOException {

		ByteArrayOutputStream output = null;
		PDDocument doc = new PDDocument();
		PDPageContentStream contentStream = null;
		try {
			initializeData(paymentDetail, doc);
			PDPage page = new PDPage();
			page = addNewPage();
			contentStream = new PDPageContentStream(doc, page, true, true, true);
			contentStream.setLineWidth(0.5f);
			//			paymentDetail.setPaymentMode("N");

			if ("H".equals(paymentDetail.getPaymentMode()) || "D".equals(paymentDetail.getPaymentMode())) {
				createChequeOrDDPdf(paymentDetail, doc, contentStream);
			}
			else if ("C".equals(paymentDetail.getPaymentMode())) {
				createCashPdf(paymentDetail, doc, contentStream);
			}
			else if ("N".equals(paymentDetail.getPaymentMode())) {
				createNeftRtgsPdf(paymentDetail, doc, contentStream);
			}
			output = new ByteArrayOutputStream();
			doc.save(output);
			//			doc.save(new File("D:/neft_challan.pdf"));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (contentStream != null) {
				contentStream.close();
			}
			doc.close();
		}
		return output;
	}

	private void createNeftRtgsPdf(PaymentDetail paymentDetail, PDDocument doc, PDPageContentStream contentStream) throws IOException {
		//PDPage page;
		contentStream.setNonStrokingColor(Color.black);

		float rectangleWidth = PDPage.PAGE_SIZE_A4.getUpperRightX() - 130;

		float YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BETWEEN_RECEIPT - NEFT_RTGS_BOX_HEIGHT;
		createMainContentForNEFT(contentStream, rectangleWidth, YPosition, paymentDetail);
		YPosition += (NEFT_RTGS_BOX_HEIGHT - 29);
		createNeftTableContent(paymentDetail, contentStream, YPosition, rectangleWidth, false);
		createFooterForNeft(contentStream, YPosition, rectangleWidth);

		YPosition -= 187;
		createNeftTableContent(paymentDetail, contentStream, YPosition, rectangleWidth, true);

		// draw second box
		YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - (PADDING_BETWEEN_RECEIPT + 30) - NEFT_RTGS_BOX_HEIGHT * 2;
		createMainContentForNEFT(contentStream, rectangleWidth, YPosition, paymentDetail);
		YPosition += (NEFT_RTGS_BOX_HEIGHT - 29);
		createNeftTableContent(paymentDetail, contentStream, YPosition, rectangleWidth, false);
		createFooterForNeft(contentStream, YPosition, rectangleWidth);

		YPosition -= 187;
		createNeftTableContent(paymentDetail, contentStream, YPosition, rectangleWidth, true);

		contentStream.close();

		// draw third box
		PDPage page = addNewPage();
		contentStream = new PDPageContentStream(doc, page, true, true, true);
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setLineWidth(0.5f);

		YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - PADDING_BETWEEN_RECEIPT - NEFT_RTGS_BOX_HEIGHT;
		createMainContentForNEFT(contentStream, rectangleWidth, YPosition, paymentDetail);
		YPosition += (NEFT_RTGS_BOX_HEIGHT - 29);
		createNeftTableContent(paymentDetail, contentStream, YPosition, rectangleWidth, false);
		createFooterForNeft(contentStream, YPosition, rectangleWidth);

		YPosition -= 187;
		createNeftTableContent(paymentDetail, contentStream, YPosition, rectangleWidth, true);
		System.out.println("done");

		contentStream.close();
	}

	private void createMainContentForNEFT(PDPageContentStream contentStream, float rectangleWidth, float YPosition,
					PaymentDetail paymentDetail) throws IOException {
		contentStream.setStrokingColor(Color.BLACK);

		contentStream.addRect(X0 + 50, YPosition, rectangleWidth, NEFT_RTGS_BOX_HEIGHT);
		contentStream.closeAndStroke();

		contentStream.setNonStrokingColor(Color.BLACK);
		YPosition += (NEFT_RTGS_BOX_HEIGHT - 3);

		contentStream.drawLine(X0 + 50, YPosition, rectangleWidth + 130 / 2 + 10, YPosition);
		drawString.drawString(contentStream, rectangleWidth / 2 + 55, YPosition -= 10, PDType1Font.HELVETICA_BOLD, 8, "NEFT/RTGS");

		contentStream.drawLine(X0 + 50, YPosition -= 3, rectangleWidth + 130 / 2 + 10, YPosition);
		drawString.drawString(contentStream, X0 + 50 + 3, YPosition -= 10, PDType1Font.HELVETICA_BOLD, 7, "Date: ");
		contentStream.drawLine(X0 + 50, YPosition -= 3, rectangleWidth + 130 / 2 + 10, YPosition);

	}

	private void createFooterForNeft(PDPageContentStream contentStream, float yPosition, float rectangleWidth) throws IOException {
		yPosition -= 150;
		contentStream.drawLine(X0 + 50, yPosition, rectangleWidth + 75, yPosition);
		drawString.drawString(contentStream, X0 + 50 + 3, yPosition -= 8, PDType1Font.HELVETICA, 7,
						"Disclaimer: 1. It is the remitter's responsibility to ensure that RTGS/NEFT payment are made to the exact details as mentioned in the challan.");
		drawString.drawString(contentStream, X0 + 50 + 3, yPosition -= 8, PDType1Font.HELVETICA, 7,
						"In the event of any discrepancy, payment would not be considered and would be refunded to the account from which the payment is made");

		drawString.drawString(contentStream, X0 + 50 + 3, yPosition -= 8, PDType1Font.HELVETICA, 7,
						"2.Remitter is required to generate challan for every payment since the account number in challan is unique to the payment.");

		contentStream.drawLine(X0 + 50, yPosition -= 3, rectangleWidth + 75, yPosition);

		drawString.drawString(contentStream, PDPage.PAGE_SIZE_A4.getUpperRightX() - 132, yPosition -= 35, PDType1Font.HELVETICA_BOLD, 7,
						"Signature of Depositor");
		contentStream.drawLine(X0 + 50, yPosition -= 2, rectangleWidth + 75, yPosition);

		drawString.drawString(contentStream, X0 + 50 + 3, yPosition -= 8, PDType1Font.HELVETICA, 7,
						"(FOR BANK USE ONLY)");
		contentStream.drawLine(X0 + 50, yPosition -= 3, rectangleWidth + 75, yPosition);

		contentStream.drawLine(X0 + 50, yPosition -= 41, rectangleWidth + 75, yPosition);

		drawString.drawString(contentStream, PDPage.PAGE_SIZE_A4.getUpperRightX() - 129, yPosition -= 8, PDType1Font.HELVETICA_BOLD, 7,
						"Authorised Signatory");

		contentStream.drawLine(X0 + 50, yPosition -= 3, rectangleWidth + 75, yPosition);

		drawString.drawString(contentStream, PDPage.PAGE_SIZE_A4.getUpperRightX() - 105, yPosition -= 8, PDType1Font.HELVETICA_BOLD, 7,
						"Branch Stamp");

		contentStream.drawLine(X0 + 50, yPosition -= 3, rectangleWidth + 75, yPosition);
	}

	private void createNeftTableContent(PaymentDetail paymentDetail, PDPageContentStream contentStream, float YPosition, float rectangleWidth,
					boolean isBankData)
					throws IOException {
		ChallanTableGenerator tableGenerator = new ChallanTableGenerator();
		List<List<String>> tableData = new ArrayList<>();

		if (!isBankData) {
			tableData.add(prepareDataForNeftOrCash("Merchant Ref No.:", paymentDetail.getQfixReferenceNumber()));
			tableData.add(prepareDataForNeftOrCash("Beneficiary Name:", ""));
			tableData.add(prepareDataForNeftOrCash("Account No.:", ""));
			tableData.add(prepareDataForNeftOrCash("IFS Code:", ""));
			tableData.add(prepareDataForNeftOrCash("Bank:", ""));
			tableData.add(prepareDataForNeftOrCash("Branch:", ""));
			tableData.add(prepareDataForNeftOrCash("School No:", ""));
			tableData.add(prepareDataForNeftOrCash("Student ID:", paymentDetail.getCustomerUniqueNo()));
			tableData.add(prepareDataForNeftOrCash("Student Name", paymentDetail.getCustomerName()));
			tableData.add(prepareDataForNeftOrCash("School Name:", "Qfix"));
			tableData.add(prepareDataForNeftOrCash("Contact No:", ""));
			tableData.add(prepareDataForNeftOrCash("Amount:", paymentDetail.getTotalAmount()));
			YPosition += 38;
		}
		else {
			tableData.add(prepareDataForNeftOrCash("Rupees:", ""));
			tableData.add(prepareDataForNeftOrCash("Dr. Application A/c", ""));
			tableData.add(prepareDataForNeftOrCash("Remittance No:", ""));
		}

		List<Float> columnWidth = new ArrayList<>();
		columnWidth.add((rectangleWidth - 4) / 2);
		columnWidth.add((rectangleWidth - 4) / 2);

		tableGenerator.setMARGIN(X0 + 50 + 2);
		tableGenerator.setTEXT_FONT(PDType1Font.HELVETICA);
		tableGenerator.generateTable(contentStream, YPosition, tableData, columnWidth);

	}

	private void createCashPdf(PaymentDetail paymentDetail, PDDocument doc, PDPageContentStream contentStream) throws IOException {
		PDPage page;
		contentStream.setNonStrokingColor(Color.BLACK);

		float YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - CASH_BOX_HEIGHT - 75;
		createMainContent(contentStream, YPosition, CASH_BOX_HEIGHT,
						true, "Bank copy for CMS Department");

		createCashTableContent(paymentDetail, contentStream, YPosition);

		YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - CASH_BOX_HEIGHT * 2 - 115;
		createMainContent(contentStream, YPosition, CASH_BOX_HEIGHT,
						true, "Student Copy");

		createCashTableContent(paymentDetail, contentStream, YPosition);
		contentStream.close();

		page = addNewPage();
		contentStream = new PDPageContentStream(doc, page, true, true, true);
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setLineWidth(0.5f);

		YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - CASH_BOX_HEIGHT - 75;
		createMainContent(contentStream, YPosition, CASH_BOX_HEIGHT,
						true, "Bank Copy");

		createCashTableContent(paymentDetail, contentStream, YPosition);
		contentStream.close();
	}

	private void createChequeOrDDPdf(PaymentDetail paymentDetail, PDDocument doc, PDPageContentStream contentStream) throws IOException {
		PDPage page;
		contentStream.setNonStrokingColor(Color.BLACK);

		float YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - CHEQUE_BOX_HEIGHT - 75;
		createMainContent(contentStream, YPosition, CHEQUE_BOX_HEIGHT,
						false, "Depositor's Copy With Bank Acknowledgement");

		createChequeOrDDTableContent(paymentDetail, contentStream, YPosition);

		YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - CHEQUE_BOX_HEIGHT * 2 - 115;
		createMainContent(contentStream, YPosition, CHEQUE_BOX_HEIGHT,
						false, "Depositor's Copy With Bank Acknowledgement");

		createChequeOrDDTableContent(paymentDetail, contentStream, YPosition);
		contentStream.close();

		page = addNewPage();
		contentStream = new PDPageContentStream(doc, page, true, true, true);
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setLineWidth(0.5f);

		YPosition = PDPage.PAGE_SIZE_A4.getUpperRightY() - CHEQUE_BOX_HEIGHT - 75;
		createMainContent(contentStream, YPosition, CHEQUE_BOX_HEIGHT,
						false, "Bank Copy");

		createChequeOrDDTableContent(paymentDetail, contentStream, YPosition);
		contentStream.close();
	}

	private List<String> prepareDataForChequeOrDD(String colmn1, String colmn2, String colmn3, String colmn4, String colmn5,
					String colmn6, String colmn7, String colmn8, String colmn9) {
		List<String> rows = new ArrayList<>();
		rows.add(colmn1);
		rows.add(colmn2);
		rows.add(colmn3);
		rows.add(colmn4);
		rows.add(colmn5);
		rows.add(colmn6);
		rows.add(colmn7);
		rows.add(colmn8);
		rows.add(colmn9);
		return rows;
	}

	private void createCashTableContent(PaymentDetail paymentDetail, PDPageContentStream contentStream, float YPosition) throws IOException {
		ChallanTableGenerator tableGenerator = new ChallanTableGenerator();
		List<List<String>> tableData = new ArrayList<>();
		List<String> rows = new ArrayList<>();
		rows.add("");
		rows.add("Date: .............");
		rows.add("Deposit No: ...............");

		tableData.add(rows);

		rows = new ArrayList<>();
		rows.add("Client Code: ");
		rows.add("Pickup Location: ...............................");
		rows.add("No. Of Insts.: .................................");
		tableData.add(rows);

		List<Float> columnWidth = new ArrayList<>();
		columnWidth.add(180f);
		columnWidth.add(180f);
		columnWidth.add(180f);

		tableGenerator.setMARGIN(35f);
		tableGenerator.setTEXT_FONT(PDType1Font.HELVETICA_BOLD);
		tableGenerator.generateTable(contentStream, YPosition += 230, tableData, columnWidth);

		tableData = new ArrayList<>();

		tableData.add(prepareDataForNeftOrCash("DENOMINATION", "AMOUNT(Rs.)"));
		tableData.add(prepareDataForNeftOrCash("2000 X", ""));
		tableData.add(prepareDataForNeftOrCash("500 X", ""));
		tableData.add(prepareDataForNeftOrCash("100 X", ""));
		tableData.add(prepareDataForNeftOrCash("50 X", ""));
		tableData.add(prepareDataForNeftOrCash("20 X", ""));
		tableData.add(prepareDataForNeftOrCash("10 X", ""));
		tableData.add(prepareDataForNeftOrCash("Other", ""));
		tableData.add(prepareDataForNeftOrCash("TOTAL", paymentDetail.getTotalAmount()));

		columnWidth = new ArrayList<>();
		columnWidth.add(110f);
		columnWidth.add(100f);

		tableGenerator = new ChallanTableGenerator();
		tableGenerator.setMARGIN(PDPage.PAGE_SIZE_A4.getUpperRightX() / 2 + 60);
		tableGenerator.setTEXT_FONT(PDType1Font.HELVETICA);
		tableGenerator.generateTable(contentStream, YPosition -= 45, tableData, columnWidth);
	}

	private List<String> prepareDataForNeftOrCash(String colmn1, String colmn2) {
		List<String> rows = new ArrayList<>();
		rows.add(colmn1);
		rows.add(colmn2);
		return rows;
	}

	private void createChequeOrDDTableContent(PaymentDetail paymentDetail, PDPageContentStream contentStream, float YPosition) throws IOException {
		ChallanTableGenerator tableGenerator = new ChallanTableGenerator();
		List<List<String>> tableData = new ArrayList<>();
		List<String> rows = new ArrayList<>();
		rows.add("");
		rows.add("Date: .............");
		rows.add("Deposit No: ...............");

		tableData.add(rows);

		rows = new ArrayList<>();
		rows.add("Client Code: ");
		rows.add("Pickup Location: ...............................");
		rows.add("No. Of Insts.: .................................");
		tableData.add(rows);

		List<Float> columnWidth = new ArrayList<>();
		columnWidth.add(180f);
		columnWidth.add(180f);
		columnWidth.add(180f);

		tableGenerator.setMARGIN(35f);
		tableGenerator.setTEXT_FONT(PDType1Font.HELVETICA_BOLD);
		tableGenerator.generateTable(contentStream, YPosition += 190, tableData, columnWidth);

		tableData = new ArrayList<>();

		String type = "H".equals(paymentDetail.getPaymentMode()) ? "Cheque" : "DD";
		tableData.add(prepareDataForChequeOrDD("Sr.No.", type + " No.", type + " Date", "Student Name", "Student ID",
						"Drawee Bank", "Drawn on Location", "Amount (Rs.)", ""));

		tableData.add(prepareDataForChequeOrDD("1", "", "", paymentDetail.getCustomerName(), paymentDetail.getCustomerUniqueNo(), "", "", "", ""));

		tableData.add(prepareDataForChequeOrDD("2", "", "", "", "", "", "", "", ""));
		tableData.add(prepareDataForChequeOrDD("3", "", "", "", "", "", "", "", ""));
		tableData.add(prepareDataForChequeOrDD("4", "", "", "", "", "", "", "", ""));
		tableData.add(prepareDataForChequeOrDD("5", "", "", "", "", "", "", "", ""));

		columnWidth = new ArrayList<>();
		columnWidth.add(28f);
		columnWidth.add(70f);
		columnWidth.add(70f);
		columnWidth.add(70f);
		columnWidth.add(70f);
		columnWidth.add(70f);
		columnWidth.add(70f);
		columnWidth.add(70f);
		columnWidth.add(28f);
		tableGenerator = new ChallanTableGenerator();

		tableGenerator.setTEXT_FONT(PDType1Font.HELVETICA);
		tableGenerator.setMARGIN(32f);
		tableGenerator.generateTable(contentStream, YPosition -= 30, tableData, columnWidth);

		tableGenerator = new ChallanTableGenerator();

		tableData = new ArrayList<>();
		rows = new ArrayList<>();
		rows.add("TOTAL");
		rows.add("" + paymentDetail.getTotalAmount());
		rows.add("");
		tableData.add(rows);

		columnWidth = new ArrayList<>();
		columnWidth.add(70f);
		columnWidth.add(70f);
		columnWidth.add(28f);
		tableGenerator.setROW_HEIGHT(30f);
		tableGenerator.setTEXT_FONT(PDType1Font.HELVETICA);
		tableGenerator.setMARGIN(32 + 28 + (70 * 5));
		tableGenerator.generateTable(contentStream, YPosition -= 72, tableData, columnWidth);
	}
}
