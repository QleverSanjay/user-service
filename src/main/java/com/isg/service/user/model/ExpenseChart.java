package com.isg.service.user.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExpenseChart {
	
	@JsonProperty(value = "weeks")
	String[] weeks;
	@JsonProperty(value = "users")
	String[] users;
	@JsonProperty(value = "amount")
	int[][] amount;
	
	public String[] getWeeks() {
		return weeks;
	}
	public void setWeeks(String[] weeks) {
		this.weeks = weeks;
	}
	public String[] getUsers() {
		return users;
	}
	public void setUsers(String[] users) {
		this.users = users;
	}
	public int[][] getAmount() {
		return amount;
	}
	public void setAmount(int[][] amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ExpenseChart [weeks=" + Arrays.toString(weeks) + ", users="
				+ Arrays.toString(users) + ", amount="
				+ Arrays.deepToString(amount) + "]";
	}
	
}
