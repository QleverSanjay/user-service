package com.isg.service.user.model;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

public class CustomBeanPropertyRowMapper<T> extends BeanPropertyRowMapper<T> {

	public CustomBeanPropertyRowMapper(Class<T> mappedClass) {
		super(mappedClass);
		super.setPrimitivesDefaultedForNullValue(true);
	}

}
