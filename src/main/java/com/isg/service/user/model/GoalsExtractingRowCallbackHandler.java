package com.isg.service.user.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.isg.service.user.request.Goal;

public class GoalsExtractingRowCallbackHandler implements RowCallbackHandler {
	private static final Logger log = Logger.getLogger(GoalsExtractingRowCallbackHandler.class);

	private final Map<Integer, List<Goal>> goalResponseMap = new HashMap<Integer, List<Goal>>();

	@Override
	public void processRow(ResultSet resultSet) throws SQLException {

		List<Goal> goalList = new ArrayList<Goal>();

		Goal goal = null;
		log.debug("Inside GoalsExtractingRowCallbackHandler");
		do {
			int studentId = resultSet.getInt("student_id");

			if (goalResponseMap.containsKey(new Integer(studentId))) {
				goalList = goalResponseMap.get(new Integer(studentId));
			}

			goal = new Goal();
			goal.setId(resultSet.getInt("id"));
			goal.setTitle(resultSet.getString("title"));
			goal.setDescription(resultSet.getString("description"));
			goal.setStartDate(resultSet.getString("formatted_start_date"));
			goal.setEndDate(resultSet.getString("formatted_end_date"));
			goal.setFeedback(resultSet.getString("feedback"));
			goal.setAmount(resultSet.getFloat("reward_amount"));
			goal.setRewardType(resultSet.getString("reward_type"));
			goal.setRewardDescription(resultSet.getString("reward_description"));
			goal.setStatus(resultSet.getString("status_code"));
			goal.setCreatedDate(resultSet.getString("status_code"));
			goal.setCreatedDate(resultSet.getString("formatted_created_date"));
			goal.setStudentId(resultSet.getInt("student_id"));
			goal.setStudentName(resultSet.getString("student_name"));
			goal.setStudentGender(resultSet.getString("student_gender"));
			goalList.add(goal);
			goalResponseMap.put(new Integer(studentId), goalList);
		} while (resultSet.next());

	}

	public Map<Integer, List<Goal>> getPopulatedMap() {
		return this.goalResponseMap;
	}

}
