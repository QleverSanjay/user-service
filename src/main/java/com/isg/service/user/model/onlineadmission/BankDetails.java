package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"bank_account",
				"bank_name",
				"transaction_type"
})
public class BankDetails {

	@JsonProperty("bank_account")
	@NotNull
	private Long bankAccount;

	@JsonProperty("bank_name")
	@NotNull
	private String bankName;

	@JsonProperty("transaction_type")
	@NotNull
	private String transactionType;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public BankDetails() {
	}

	/**
	 * 
	 * @param transactionType
	 * @param bankName
	 * @param bankAccount
	 */
	public BankDetails(Long bankAccount, String bankName, String transactionType) {
		this.bankAccount = bankAccount;
		this.bankName = bankName;
		this.transactionType = transactionType;
	}

	/**
	 * 
	 * @return The bankAccount
	 */
	@JsonProperty("bank_account")
	public Long getBankAccount() {
		return bankAccount;
	}

	/**
	 * 
	 * @param bankAccount
	 *            The bank_account
	 */
	@JsonProperty("bank_account")
	public void setBankAccount(Long bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 * 
	 * @return The bankName
	 */
	@JsonProperty("bank_name")
	public String getBankName() {
		return bankName;
	}

	/**
	 * 
	 * @param bankName
	 *            The bank_name
	 */
	@JsonProperty("bank_name")
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * 
	 * @return The transactionType
	 */
	@JsonProperty("transaction_type")
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * 
	 * @param transactionType
	 *            The transaction_type
	 */
	@JsonProperty("transaction_type")
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
