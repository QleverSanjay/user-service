package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"personal_mobile_number",
				"personal_email",
				"parent_contact_number"
})
public class ContactDetails {

	@JsonProperty("personal_mobile_number")
	@NotNull
	private String personalMobileNumber;

	@JsonProperty("personal_email")
	@NotNull
	private String personalEmail;

	@JsonProperty("parent_contact_number")
	private String parentContactNumber;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public ContactDetails() {
	}

	/**
	 * 
	 * @param personalMobileNumber
	 * @param personalEmail
	 * @param parentContactNumber
	 */
	public ContactDetails(String personalMobileNumber, String personalEmail, String parentContactNumber) {
		this.personalMobileNumber = personalMobileNumber;
		this.personalEmail = personalEmail;
		this.parentContactNumber = parentContactNumber;
	}

	/**
	 * 
	 * @return The personalMobileNumber
	 */
	@JsonProperty("personal_mobile_number")
	public String getPersonalMobileNumber() {
		return personalMobileNumber;
	}

	/**
	 * 
	 * @param personalMobileNumber
	 *            The personal_mobile_number
	 */
	@JsonProperty("personal_mobile_number")
	public void setPersonalMobileNumber(String personalMobileNumber) {
		this.personalMobileNumber = personalMobileNumber;
	}

	/**
	 * 
	 * @return The personalEmail
	 */
	@JsonProperty("personal_email")
	public String getPersonalEmail() {
		return personalEmail;
	}

	/**
	 * 
	 * @param personalEmail
	 *            The personal_email
	 */
	@JsonProperty("personal_email")
	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	/**
	 * 
	 * @return The parentContactNumber
	 */
	@JsonProperty("parent_contact_number")
	public String getParentContactNumber() {
		return parentContactNumber;
	}

	/**
	 * 
	 * @param parentContactNumber
	 *            The parent_contact_number
	 */
	@JsonProperty("parent_contact_number")
	public void setParentContactNumber(String parentContactNumber) {
		this.parentContactNumber = parentContactNumber;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
