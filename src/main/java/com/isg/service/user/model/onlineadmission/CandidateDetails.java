package com.isg.service.user.model.onlineadmission;

import java.util.List;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "name", "printed_name_on_marksheet", "gender",
		"marital_status", "dateofbirth", "placeofbirth", "caste_details",
		"physically_handicapped", "form_details", "mother_tounge",
		"foreign_student", "aadhaaar_card", "bank_details",
		"employment_details", "ncc_pref", "last_school_name", "sports_details",
		"social_reservation", "identification_mark", "hobbies", "blood_group",
		"native_place_address", "languages_known", "nationality",
		"educationalqualification", "category", "locations", "otherLocation",
		"otherCourse", "course" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class CandidateDetails {

	@JsonProperty("name")
	@Valid
	@NotNull
	private Name name;

	@JsonProperty("printed_name_on_marksheet")
	@Valid
	private Name printedNameOnMarksheet;

	@JsonProperty("gender")
	@Valid
	@NotNull
	private Gender gender;

	@JsonProperty("marital_status")
	@Valid
	private MaritalStatus maritalStatus;

	@JsonProperty("dateofbirth")
	private String dateofbirth;

	@JsonProperty("age_year")
	private String ageYear;

	@JsonProperty("age_month")
	private String ageMonth;

	@JsonProperty("placeofbirth")
	private String placeofbirth;

	@JsonProperty("caste_details")
	@Valid
	private CasteDetails casteDetails;

	@JsonProperty("physically_handicapped")
	private PhysicallyHandicapped physicallyHandicapped;

	@JsonProperty("form_details")
	@Valid
	private FormDetails formDetails;

	@JsonProperty("mother_tounge")
	@NotNull
	private String motherTounge;

	@JsonProperty("primary_language")
	private String primaryLanguage;

	@JsonProperty("english_year")
	private String engYear;

	@JsonProperty("ell_support")
	private String ellSupport;

	@JsonProperty("psycho_testing")
	private String psychoTesting;

	@JsonProperty("learning_disability")
	private String learningDisability;

	@JsonProperty("leraning_support")
	private String lerningSupport;

	@JsonProperty("immunisation")
	private String immunisation;

	@JsonProperty("medical_history")
	private String medicalHistory;

	@JsonProperty("foreign_student")
	@NotNull
	private String foreignStudent;

	@JsonProperty("aadhaaar_card")
	private String aadhaaarCard;

	@JsonProperty("bank_details")
	@Valid
	private BankDetails bankDetails;

	@JsonProperty("employment_details")
	private EmploymentDetails employmentDetails;

	@JsonProperty("ncc_pref")
	@NotNull
	private String nccPref;

	@JsonProperty("last_school_name")
	private String lastSchoolName;

	@JsonProperty("sports_details")
	private String sportsDetails;

	@JsonProperty("social_reservation")
	private SocialReservation socialReservation;

	@JsonProperty("identification_mark")
	private String identificationMark;

	@JsonProperty("hobbies")
	private String hobbies;

	@JsonProperty("blood_group")
	private String bloodGroup;

	@JsonProperty("native_place_address")
	private String nativePlaceAddress;

	@JsonProperty("languages_known")
	private String languagesKnown;

	@JsonProperty("academic_year")
	AcademicYear academicYear;

	@JsonProperty("academic_term")
	String academicTerm;

	@JsonProperty("nationality")
	private String nationality;

	@JsonProperty("educationalqualification")
	@Valid
	private EducationalQualifications educationalqualifications;

	@JsonProperty("category")
	@Valid
	private Category category;

	@JsonProperty("locations")
	private List<Location> location;

	@JsonProperty("course")
	Course course;

	@JsonProperty("otherLocation")
	private String otherLocation;
	
	public String getOtherLocation() {
		return otherLocation;
	}

	public void setOtherLocation(String otherLocation) {
		this.otherLocation = otherLocation;
	}

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public CandidateDetails() {
	}

	/**
	 * 
	 * @param printedNameOnMarksheet
	 * @param bloodGroup
	 * @param nccPref
	 * @param foreignStudent
	 * @param formDetails
	 * @param dateofbirth
	 * @param hobbies
	 * @param maritalStatus
	 * @param employmentDetails
	 * @param socialReservation
	 * @param motherTounge
	 * @param bankDetails
	 * @param lastSchoolName
	 * @param name
	 * @param physicallyHandicapped
	 * @param identificationMark
	 * @param sportsDetails
	 * @param gender
	 * @param placeofbirth
	 * @param nativePlaceAddress
	 * @param casteDetails
	 * @param aadhaaarCard
	 */
	public CandidateDetails(Name name, Name printedNameOnMarksheet,
			Gender gender, MaritalStatus maritalStatus, String dateofbirth,
			String placeofbirth, CasteDetails casteDetails,
			PhysicallyHandicapped physicallyHandicapped,
			FormDetails formDetails, String motherTounge,
			String foreignStudent, String aadhaaarCard, BankDetails bankDetails,
			EmploymentDetails employmentDetails, String nccPref,
			String lastSchoolName, String sportsDetails,
			SocialReservation socialReservation, String identificationMark,
			String hobbies, String bloodGroup, String nativePlaceAddress,
			String languagesKnown, String nationality,
			EducationalQualifications educationalqualifications,
			Category category, List<Location> location, Course course) {
		this.name = name;
		this.printedNameOnMarksheet = printedNameOnMarksheet;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.dateofbirth = dateofbirth;
		this.placeofbirth = placeofbirth;
		this.casteDetails = casteDetails;
		this.physicallyHandicapped = physicallyHandicapped;
		this.formDetails = formDetails;
		this.motherTounge = motherTounge;
		this.foreignStudent = foreignStudent;
		this.aadhaaarCard = aadhaaarCard;
		this.bankDetails = bankDetails;
		this.employmentDetails = employmentDetails;
		this.nccPref = nccPref;
		this.lastSchoolName = lastSchoolName;
		this.sportsDetails = sportsDetails;
		this.socialReservation = socialReservation;
		this.identificationMark = identificationMark;
		this.hobbies = hobbies;
		this.bloodGroup = bloodGroup;
		this.nativePlaceAddress = nativePlaceAddress;
		this.languagesKnown = languagesKnown;
		this.nationality = nationality;
		this.educationalqualifications = educationalqualifications;
		this.category = category;
		this.location = location;
		this.course = course;
	}

	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public Name getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(Name name) {
		this.name = name;
	}

	public String getAgeYear() {
		return ageYear;
	}

	public void setAgeYear(String ageYear) {
		this.ageYear = ageYear;
	}

	public String getAgeMonth() {
		return ageMonth;
	}

	public void setAgeMonth(String ageMonth) {
		this.ageMonth = ageMonth;
	}

	public String getPrimaryLanguage() {
		return primaryLanguage;
	}

	public void setPrimaryLanguage(String primaryLanguage) {
		this.primaryLanguage = primaryLanguage;
	}

	public String getEngYear() {
		return engYear;
	}

	public void setEngYear(String engYear) {
		this.engYear = engYear;
	}

	public String getEllSupport() {
		return ellSupport;
	}

	public void setEllSupport(String ellSupport) {
		this.ellSupport = ellSupport;
	}

	public String getPsychoTesting() {
		return psychoTesting;
	}

	public void setPsychoTesting(String psychoTesting) {
		this.psychoTesting = psychoTesting;
	}

	public String getLearningDisability() {
		return learningDisability;
	}

	public void setLearningDisability(String learningDisability) {
		this.learningDisability = learningDisability;
	}

	public String getLerningSupport() {
		return lerningSupport;
	}

	public void setLerningSupport(String lerningSupport) {
		this.lerningSupport = lerningSupport;
	}

	public String getImmunisation() {
		return immunisation;
	}

	public void setImmunisation(String immunisation) {
		this.immunisation = immunisation;
	}

	public String getMedicalHistory() {
		return medicalHistory;
	}

	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	/**
	 * 
	 * @return The printedNameOnMarksheet
	 */
	@JsonProperty("printed_name_on_marksheet")
	public Name getPrintedNameOnMarksheet() {
		return printedNameOnMarksheet;
	}

	/**
	 * 
	 * @param printedNameOnMarksheet
	 *            The printed_name_on_marksheet
	 */
	@JsonProperty("printed_name_on_marksheet")
	public void setPrintedNameOnMarksheet(Name printedNameOnMarksheet) {
		this.printedNameOnMarksheet = printedNameOnMarksheet;
	}

	/**
	 * 
	 * @return The gender
	 */
	@JsonProperty("gender")
	public Gender getGender() {
		return gender;
	}

	/**
	 * 
	 * @param gender
	 *            The gender
	 */
	@JsonProperty("gender")
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * 
	 * @return The maritalStatus
	 */
	@JsonProperty("marital_status")
	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	/**
	 * 
	 * @param maritalStatus
	 *            The marital_status
	 */
	@JsonProperty("marital_status")
	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	/**
	 * 
	 * @return The dateofbirth
	 */
	@JsonProperty("dateofbirth")
	public String getDateofbirth() {
		return dateofbirth;
	}

	/**
	 * 
	 * @param dateofbirth
	 *            The dateofbirth
	 */
	@JsonProperty("dateofbirth")
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	/**
	 * 
	 * @return The placeofbirth
	 */
	@JsonProperty("placeofbirth")
	public String getPlaceofbirth() {
		return placeofbirth;
	}

	/**
	 * 
	 * @param placeofbirth
	 *            The placeofbirth
	 */
	@JsonProperty("placeofbirth")
	public void setPlaceofbirth(String placeofbirth) {
		this.placeofbirth = placeofbirth;
	}

	/**
	 * 
	 * @return The casteDetails
	 */
	@JsonProperty("caste_details")
	public CasteDetails getCasteDetails() {
		return casteDetails;
	}

	/**
	 * 
	 * @param casteDetails
	 *            The caste_details
	 */
	@JsonProperty("caste_details")
	public void setCasteDetails(CasteDetails casteDetails) {
		this.casteDetails = casteDetails;
	}

	/**
	 * 
	 * @return The physicallyHandicapped
	 */
	@JsonProperty("physically_handicapped")
	public PhysicallyHandicapped getPhysicallyHandicapped() {
		return physicallyHandicapped;
	}

	/**
	 * 
	 * @param physicallyHandicapped
	 *            The physically_handicapped
	 */
	@JsonProperty("physically_handicapped")
	public void setPhysicallyHandicapped(
			PhysicallyHandicapped physicallyHandicapped) {
		this.physicallyHandicapped = physicallyHandicapped;
	}

	/**
	 * 
	 * @return The formDetails
	 */
	@JsonProperty("form_details")
	public FormDetails getFormDetails() {
		return formDetails;
	}

	/**
	 * 
	 * @param formDetails
	 *            The form_details
	 */
	@JsonProperty("form_details")
	public void setFormDetails(FormDetails formDetails) {
		this.formDetails = formDetails;
	}

	/**
	 * 
	 * @return The motherTounge
	 */
	@JsonProperty("mother_tounge")
	public String getMotherTounge() {
		return motherTounge;
	}

	/**
	 * 
	 * @param motherTounge
	 *            The mother_tounge
	 */
	@JsonProperty("mother_tounge")
	public void setMotherTounge(String motherTounge) {
		this.motherTounge = motherTounge;
	}

	/**
	 * 
	 * @return The foreignStudent
	 */
	@JsonProperty("foreign_student")
	public String getForeignStudent() {
		return foreignStudent;
	}

	/**
	 * 
	 * @param foreignStudent
	 *            The foreign_student
	 */
	@JsonProperty("foreign_student")
	public void setForeignStudent(String foreignStudent) {
		this.foreignStudent = foreignStudent;
	}

	/**
	 * 
	 * @return The aadhaaarCard
	 */
	@JsonProperty("aadhaaar_card")
	public String getAadhaaarCard() {
		return aadhaaarCard;
	}

	/**
	 * 
	 * @param aadhaaarCard
	 *            The aadhaaar_card
	 */
	@JsonProperty("aadhaaar_card")
	public void setAadhaaarCard(String aadhaaarCard) {
		this.aadhaaarCard = aadhaaarCard;
	}

	/**
	 * 
	 * @return The bankDetails
	 */
	@JsonProperty("bank_details")
	public BankDetails getBankDetails() {
		return bankDetails;
	}

	/**
	 * 
	 * @param bankDetails
	 *            The bank_details
	 */
	@JsonProperty("bank_details")
	public void setBankDetails(BankDetails bankDetails) {
		this.bankDetails = bankDetails;
	}

	/**
	 * 
	 * @return The employmentDetails
	 */
	@JsonProperty("employment_details")
	public EmploymentDetails getEmploymentDetails() {
		return employmentDetails;
	}

	/**
	 * 
	 * @param employmentDetails
	 *            The employment_details
	 */
	@JsonProperty("employment_details")
	public void setEmploymentDetails(EmploymentDetails employmentDetails) {
		this.employmentDetails = employmentDetails;
	}

	/**
	 * 
	 * @return The nccPref
	 */
	@JsonProperty("ncc_pref")
	public String getNccPref() {
		return nccPref;
	}

	/**
	 * 
	 * @param nccPref
	 *            The ncc_pref
	 */
	@JsonProperty("ncc_pref")
	public void setNccPref(String nccPref) {
		this.nccPref = nccPref;
	}

	/**
	 * 
	 * @return The lastSchoolName
	 */
	@JsonProperty("last_school_name")
	public String getLastSchoolName() {
		return lastSchoolName;
	}

	/**
	 * 
	 * @param lastSchoolName
	 *            The last_school_name
	 */
	@JsonProperty("last_school_name")
	public void setLastSchoolName(String lastSchoolName) {
		this.lastSchoolName = lastSchoolName;
	}

	/**
	 * 
	 * @return The sportsDetails
	 */
	@JsonProperty("sports_details")
	public String getSportsDetails() {
		return sportsDetails;
	}

	/**
	 * 
	 * @param sportsDetails
	 *            The sports_details
	 */
	@JsonProperty("sports_details")
	public void setSportsDetails(String sportsDetails) {
		this.sportsDetails = sportsDetails;
	}

	/**
	 * 
	 * @return The socialReservation
	 */
	@JsonProperty("social_reservation")
	public SocialReservation getSocialReservation() {
		return socialReservation;
	}

	/**
	 * 
	 * @param socialReservation
	 *            The social_reservation
	 */
	@JsonProperty("social_reservation")
	public void setSocialReservation(SocialReservation socialReservation) {
		this.socialReservation = socialReservation;
	}

	/**
	 * 
	 * @return The identificationMark
	 */
	@JsonProperty("identification_mark")
	public String getIdentificationMark() {
		return identificationMark;
	}

	/**
	 * 
	 * @param identificationMark
	 *            The identification_mark
	 */
	@JsonProperty("identification_mark")
	public void setIdentificationMark(String identificationMark) {
		this.identificationMark = identificationMark;
	}

	/**
	 * 
	 * @return The hobbies
	 */
	@JsonProperty("hobbies")
	public String getHobbies() {
		return hobbies;
	}

	/**
	 * 
	 * @param hobbies
	 *            The hobbies
	 */
	@JsonProperty("hobbies")
	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	/**
	 * 
	 * @return The bloodGroup
	 */
	@JsonProperty("blood_group")
	public String getBloodGroup() {
		return bloodGroup;
	}

	/**
	 * 
	 * @param bloodGroup
	 *            The blood_group
	 */
	@JsonProperty("blood_group")
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	/**
	 * 
	 * @return The nativePlaceAddress
	 */
	@JsonProperty("native_place_address")
	public String getNativePlaceAddress() {
		return nativePlaceAddress;
	}

	/**
	 * 
	 * @param nativePlaceAddress
	 *            The native_place_address
	 */
	@JsonProperty("native_place_address")
	public void setNativePlaceAddress(String nativePlaceAddress) {
		this.nativePlaceAddress = nativePlaceAddress;
	}

	public String getLanguagesKnown() {
		return languagesKnown;
	}

	public void setLanguagesKnown(String languagesKnown) {
		this.languagesKnown = languagesKnown;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public String getAcademicTerm() {
		return academicTerm;
	}

	public void setAcademicTerm(String academicTerm) {
		this.academicTerm = academicTerm;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public EducationalQualifications getEducationalqualifications() {
		return educationalqualifications;
	}

	public void setEducationalqualifications(
			EducationalQualifications educationalqualifications) {
		this.educationalqualifications = educationalqualifications;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/**
	 * @return the location
	 */
	public List<Location> getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(List<Location> location) {
		this.location = location;
	}

}
