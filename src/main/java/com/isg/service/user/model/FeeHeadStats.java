package com.isg.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeeHeadStats {

	private Integer headId;
	private String headDescription;
	private Integer totalCount;
	private Integer totalPaidCount;
	private Integer totalDueCount;
	public Integer getHeadId() {
		return headId;
	}
	public void setHeadId(Integer headId) {
		this.headId = headId;
	}
	public String getHeadDescription() {
		return headDescription;
	}
	public void setHeadDescription(String headDescription) {
		this.headDescription = headDescription;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Integer getTotalPaidCount() {
		return totalPaidCount;
	}
	public void setTotalPaidCount(Integer totalPaidCount) {
		this.totalPaidCount = totalPaidCount;
	}
	public Integer getTotalDueCount() {
		return totalDueCount;
	}
	public void setTotalDueCount(Integer totalDueCount) {
		this.totalDueCount = totalDueCount;
	}
	
	@Override
	public String toString() {
		return "FeeHeadStats [headId=" + headId + ", headDescription="
				+ headDescription + ", totalCount=" + totalCount
				+ ", totalPaidCount=" + totalPaidCount + ", totalDueCount="
				+ totalDueCount + "]";
	}
	

}
