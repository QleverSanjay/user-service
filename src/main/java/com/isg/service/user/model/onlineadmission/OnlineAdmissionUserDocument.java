package com.isg.service.user.model.onlineadmission;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OnlineAdmissionUserDocument {

	private Integer onlineAdmissionId;

	private Integer documentId;

	private String documentPath;

	public Integer getOnlineAdmissionId() {
		return onlineAdmissionId;
	}

	public void setOnlineAdmissionId(Integer onlineAdmissionId) {
		this.onlineAdmissionId = onlineAdmissionId;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

}
