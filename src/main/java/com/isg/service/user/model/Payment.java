package com.isg.service.user.model;

public class Payment {

	float amount;

	String date;

	Integer passbookId;

	Integer FeesId;

	Integer paymentAuditId;

	String feesDescription;

	String qfixRefNumber;

	String auditDescription;

	String paymentChannel;

	String modeOfPayment;

	String forPerson;

	String instituteLogoUrl;

	Integer branchId;

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getPassbookId() {
		return passbookId;
	}

	public void setPassbookId(Integer passbookId) {
		this.passbookId = passbookId;
	}

	public Integer getFeesId() {
		return FeesId;
	}

	public void setFeesId(Integer feesId) {
		FeesId = feesId;
	}

	public Integer getPaymentAuditId() {
		return paymentAuditId;
	}

	public void setPaymentAuditId(Integer paymentAuditId) {
		this.paymentAuditId = paymentAuditId;
	}

	public String getFeesDescription() {
		return feesDescription;
	}

	public void setFeesDescription(String feesDescription) {
		this.feesDescription = feesDescription;
	}

	public String getQfixRefNumber() {
		return qfixRefNumber;
	}

	public void setQfixRefNumber(String qfixRefNumber) {
		this.qfixRefNumber = qfixRefNumber;
	}

	public String getAuditDescription() {
		return auditDescription;
	}

	public void setAuditDescription(String auditDescription) {
		this.auditDescription = auditDescription;
	}

	public String getPaymentChannel() {
		return paymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getForPerson() {
		return forPerson;
	}

	public void setForPerson(String forPerson) {
		this.forPerson = forPerson;
	}

	public String getInstituteLogoUrl() {
		return instituteLogoUrl;
	}

	public void setInstituteLogoUrl(String instituteLogoUrl) {
		this.instituteLogoUrl = instituteLogoUrl;
	}

}
