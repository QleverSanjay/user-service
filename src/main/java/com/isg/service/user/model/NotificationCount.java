package com.isg.service.user.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationCount {

	@JsonProperty(value = "unread_notifications")
	private final Map<String, Integer> unreadNotifications = new HashMap<String, Integer>();

	public Map<String, Integer> getUnreadNotifications() {
		return unreadNotifications;
	}

	@Override
	public String toString() {
		return "NotificationCount [unreadNotifications=" + unreadNotifications + "]";
	}

}
