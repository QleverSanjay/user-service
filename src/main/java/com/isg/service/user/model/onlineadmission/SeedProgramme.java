package com.isg.service.user.model.onlineadmission;

import javax.validation.Valid;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
				"compulsory_subject",
				"optional_subject"
})
public class SeedProgramme {

	@JsonProperty("compulsory_subject")
	@Valid
	private CompulsorySubject compulsorySubject;

	@JsonProperty("optional_subject")
	@Valid
	private OptionalSubject optionalSubject;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public SeedProgramme() {
	}

	/**
	 * 
	 * @param optionalSubject
	 * @param compulsorySubject
	 */
	public SeedProgramme(CompulsorySubject compulsorySubject, OptionalSubject optionalSubject) {
		this.compulsorySubject = compulsorySubject;
		this.optionalSubject = optionalSubject;
	}

	/**
	 * 
	 * @return The compulsorySubject
	 */
	@JsonProperty("compulsory_subject")
	public CompulsorySubject getCompulsorySubject() {
		return compulsorySubject;
	}

	/**
	 * 
	 * @param compulsorySubject
	 *            The compulsory_subject
	 */
	@JsonProperty("compulsory_subject")
	public void setCompulsorySubject(CompulsorySubject compulsorySubject) {
		this.compulsorySubject = compulsorySubject;
	}

	/**
	 * 
	 * @return The optionalSubject
	 */
	@JsonProperty("optional_subject")
	public OptionalSubject getOptionalSubject() {
		return optionalSubject;
	}

	/**
	 * 
	 * @param optionalSubject
	 *            The optional_subject
	 */
	@JsonProperty("optional_subject")
	public void setOptionalSubject(OptionalSubject optionalSubject) {
		this.optionalSubject = optionalSubject;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
