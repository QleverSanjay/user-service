package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.Valid;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"name",
				"employment_details",
				"email","status"
})
public class ParentDetail {

	@JsonProperty("name")
	@Valid
	private Name name;

	@JsonProperty("employment_details")
	@Valid
	private EmploymentDetails employmentDetails;

	@JsonProperty("email")
	private String email;

	@JsonProperty("citizenship")
	private String citizenship;

	@JsonProperty("age")
	private int age;

	@JsonProperty("school_detail")
	private String schoolDetail;
	
	@JsonProperty("highest_qualification")
	private String highestQualification;

	public String getHighestQualification() {
		return highestQualification;
	}

	public void setHighestQualification(String highestQualification) {
		this.highestQualification = highestQualification;
	}

	@JsonProperty("college_detail")
	private String collegDetail;

	@JsonProperty("primary_phone")
	private String primaryPhone;

	@JsonProperty("secondary_phone")
	private String secondaryPhone;

	@JsonProperty("hobbies")
	private String hobbies;

	@JsonProperty("nationality")
	private String nationality;

	@JsonProperty("pan_number")
	private String panNumber;

	@JsonProperty("alumni_detail")
	private String alumniDetail;
	
	@JsonProperty("status")
	private String status;

	@JsonProperty("resi_land_line")
	private String resilandline;
	

	public String getResilandline() {
		return resilandline;
	}

	public void setResilandline(String resilandline) {
		this.resilandline = resilandline;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAlumniDetail() {
		return alumniDetail;
	}

	public void setAlumniDetail(String alumniDetail) {
		this.alumniDetail = alumniDetail;
	}

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public ParentDetail() {
	}

	/**
	 * 
	 * @param email
	 * @param name
	 * @param employmentDetails
	 */
	public ParentDetail(Name name, EmploymentDetails employmentDetails, String email) {
		this.name = name;
		this.employmentDetails = employmentDetails;
		this.email = email;
	}

	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public Name getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The employmentDetails
	 */
	@JsonProperty("employment_details")
	public EmploymentDetails getEmploymentDetails() {
		return employmentDetails;
	}

	/**
	 * 
	 * @param employmentDetails
	 *            The employment_details
	 */
	@JsonProperty("employment_details")
	public void setEmploymentDetails(EmploymentDetails employmentDetails) {
		this.employmentDetails = employmentDetails;
	}

	/**
	 * 
	 * @return The email
	 */
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 *            The email
	 */
	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSchoolDetail() {
		return schoolDetail;
	}

	public void setSchoolDetail(String schoolDetail) {
		this.schoolDetail = schoolDetail;
	}

	public String getCollegDetail() {
		return collegDetail;
	}

	public void setCollegDetail(String collegDetail) {
		this.collegDetail = collegDetail;
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getSecondaryPhone() {
		return secondaryPhone;
	}

	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
