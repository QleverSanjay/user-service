package com.isg.service.user.model;

import org.springframework.stereotype.Component;

@Component
public class Subject {

	private Integer id;
	private String name;
	private String theory;
	private String practical;
	private String obtainedMarks;
	private String totalMarks;
	private String category;
	private String remarks;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTheory() {
		return theory;
	}

	public void setTheory(String theory) {
		this.theory = theory;
	}

	public String getPractical() {
		return practical;
	}

	public void setPractical(String practical) {
		this.practical = practical;
	}

	public String getObtainedMarks() {
		return obtainedMarks;
	}

	public void setObtainedMarks(String obtainedMarks) {
		this.obtainedMarks = obtainedMarks;
	}

	/*public String getTotalMarks() {
		return totalMarks;
	}*/

	public void setTotalMarks(String totalMarks) {
		this.totalMarks = totalMarks;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "Subject [id=" + id + ", name=" + name + ", theory=" + theory
				+ ", practical=" + practical + ", obtainedMarks="
				+ obtainedMarks + ", totalMarks=" + totalMarks + ", category="
				+ category + ", remarks=" + remarks + "]";
	}

}
