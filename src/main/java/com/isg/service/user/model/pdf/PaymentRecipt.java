package com.isg.service.user.model.pdf;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.model.FeesInfo;
import com.isg.service.user.request.Branch;
import com.isg.service.user.request.BranchConfiguration;
import com.isg.service.user.request.DisplayLayout;
import com.isg.service.user.util.FileStorePathAssembler;

public class PaymentRecipt {

	public float previousContentY = 682; //here value of previousContentY is reduced because of the rectangle draw in upper direction
	private final float X0 = 40f;
	private final float RECTANGLE_WIDTH = 375;
	private final float ROW_WIDTH = 557;
	private final float RECTANGLE_HEIGHT = 100;
	private final float marginX = 5;
	private float CONTENT_FONT_SIZE = 8f;
	private float lastContentOfYForWrapping = 0f;
	private float firstLineYPosition = 0f;
	private float tempYPosition = 0f;
	DrawString drawString = new DrawString();
	private int NUMBER_OF_PAGES = 1;

	public static void main(String a[]) throws IOException {
		PaymentRecipt paymentRecipt = new PaymentRecipt();
		String feeInfoStr = " { \"branchId\":38, \"amount_paid\":12122.0, \"amount_Due\":0.0, \"due_date\":null, \"description\":\"Fees for Ashsih\", \"base_fee_amount\":12122.0, \"display_layouts\":[ { \"id\":1, \"feeDescription\":\"sports fees\", \"otherDescription\":null, \"feeAmount\":100.0, \"branchId\":38, \"instituteId\":null }, { \"id\":2, \"feeDescription\":\"health fees\", \"otherDescription\":\"This are compulsory fees.\", \"feeAmount\":500.0, \"branchId\":38, \"instituteId\":null }, { \"id\":3, \"feeDescription\":\"farewell fees\", \"otherDescription\":null, \"feeAmount\":500.0, \"branchId\":38, \"instituteId\":null }, { \"id\":3, \"feeDescription\":\"farewell fees\", \"otherDescription\":null, \"feeAmount\":500.0, \"branchId\":38, \"instituteId\":null }, { \"id\":3, \"feeDescription\":\"farewell fees\", \"otherDescription\":null, \"feeAmount\":500.0, \"branchId\":38, \"instituteId\":null }, { \"id\":3, \"feeDescription\":\"farewell fees\", \"otherDescription\":null, \"feeAmount\":500.0, \"branchId\":38, \"instituteId\":null }, { \"id\":3, \"feeDescription\":\"farewell fees\", \"otherDescription\":null, \"feeAmount\":500.0, \"branchId\":38, \"instituteId\":null }, { \"id\":3, \"feeDescription\":\"farewell fees\", \"otherDescription\":null, \"feeAmount\":500.0, \"branchId\":38, \"instituteId\":null }, { \"id\":3, \"feeDescription\":\"farewell fees\", \"otherDescription\":null, \"feeAmount\":500.0, \"branchId\":38, \"instituteId\":null }, { \"id\":4, \"feeDescription\":\"xyz\", \"otherDescription\":null, \"feeAmount\":1000.0, \"branchId\":38, \"instituteId\":null } ], \"total_late_fee_added\":0.0, \"total_fee_amount_added\":0.0, \"fees_charges\":[ ], \"payment_status\":\"PAID\", \"reference_number\":\"FFAE1212\", \"payment_mode\":\"check\", \"payment_for\":\"swati deshmukh\", \"paid_by\":null, \"paid_date\":\"2017-03-13 00:00:00.0\"}";
		FeesInfo info = new ObjectMapper().readValue(feeInfoStr, FeesInfo.class);
		Branch branch = new Branch();
		branch.setAddressLine("Room no 758, Jari Mari chowk, Katkari pada, Rabale");
		branch.setCity("Navi Mumbai");
		branch.setPincode("400701");
		String schoolLogoUrl = "\\images\\Qfix\\documents\\External\\1\\school-logo.jpg";

		System.out.println("Pdf is saved in this path ::" + paymentRecipt.createPdf(info, null));
	}

	FeesInfo feeInfo;
	String schoolLogoUrl;
	BranchConfiguration branchConfiguration;

	@SuppressWarnings("resource")
	public ByteArrayOutputStream createPdf(FeesInfo feeInfo, BranchConfiguration branchConfiguration) throws IOException {
		ByteArrayOutputStream output = null;
		PDDocument doc = new PDDocument();
		PDPageContentStream contentStream = null;
		try {

			PDPage page = new PDPage();
			page = addNewPage(doc);
			this.feeInfo = feeInfo;
			this.schoolLogoUrl = (branchConfiguration != null ? branchConfiguration.getInstituteLogoUrl() : null);
			this.branchConfiguration = branchConfiguration;

			contentStream = new PDPageContentStream(doc, page);
			contentStream = createImageHeaderAndSchoolDetails(doc, contentStream, PDType1Font.TIMES_BOLD, page);
			contentStream = createPaymentformation(contentStream);
			contentStream = createBreakdownContent(doc, contentStream, PDType1Font.TIMES_BOLD);
			contentStream = createFooter(contentStream);
			contentStream.drawLine(X0 - 30, previousContentY, X0 - 30, firstLineYPosition);
			contentStream.drawLine(X0 + marginX + ROW_WIDTH, previousContentY, X0 + marginX + ROW_WIDTH, firstLineYPosition);
			contentStream.close();
			output = new ByteArrayOutputStream();
			//			doc.save(new File("D:/temp.pdf"));
			doc.save(output);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (contentStream != null) {
				contentStream.close();
			}
			doc.close();
		}
		return output;
	}

	public PDPageContentStream createImageHeaderAndSchoolDetails(PDDocument doc, PDPageContentStream contentStream, PDFont font, PDPage page)
					throws IOException {
		//Drawing logo of Pathfindersps

		if (branchConfiguration != null && "N".equals(branchConfiguration.getHideLogo())) {
			drawImage(doc, FileStorePathAssembler.QFIX_LOGO_IMAGE_RELATIVE_PATH,
							(PDPage.PAGE_SIZE_A4.getUpperRightX() - 110), previousContentY, 100, 100, contentStream);
		}

		if (!StringUtils.isEmpty(schoolLogoUrl)) {
			drawImage(doc, FileStorePathAssembler.QFIX_FILE_FOLDER_BASE_PATH + schoolLogoUrl,
							(PDPage.PAGE_SIZE_A4.getUpperRightX() - 590), previousContentY, 100, 100, contentStream);
		}

		contentStream.setNonStrokingColor(Color.WHITE);

		List<String> splitedHeaderTextInstituteName = new ArrayList<String>();

		splitedHeaderTextInstituteName = branchConfiguration == null ? null : stringSplit(branchConfiguration.getBranchName(), 30, 0);

		contentStream.fillRect(X0 + 80, previousContentY, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE + 8);
		contentStream.moveTextPositionByAmount(getCenterTextX(RECTANGLE_WIDTH), previousContentY + 80);
		if (splitedHeaderTextInstituteName != null) {
			for (int i = 0; i < splitedHeaderTextInstituteName.size(); i++) {
				contentStream.drawString(splitedHeaderTextInstituteName.get(i));
				contentStream.appendRawCommands("T*\n");
				contentStream.moveTextPositionByAmount(0, -15);
				lastContentOfYForWrapping = (15 * i);
			}
		}
		contentStream.endText();

		List<String> splitedHeaderTextAddressName = new ArrayList<String>();
		String address = "";
		if (branchConfiguration != null) {
			address = (StringUtils.isEmpty(branchConfiguration.getAddress()) ? "" : branchConfiguration.getAddress());
		}
		splitedHeaderTextAddressName = stringSplit(address, 50, 0);
		contentStream.beginText();
		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		contentStream.moveTextPositionByAmount(getCenterTextX(RECTANGLE_WIDTH), (previousContentY + 60) - lastContentOfYForWrapping);
		for (int i = 0; i < splitedHeaderTextAddressName.size(); i++) {
			contentStream.drawString(splitedHeaderTextAddressName.get(i));
			contentStream.appendRawCommands("T*\n");
			contentStream.moveTextPositionByAmount(0, -12);
		}
		contentStream.endText();
		contentStream.drawLine(X0 - 30, previousContentY -= 20, X0 + marginX + ROW_WIDTH, previousContentY);
		firstLineYPosition = previousContentY;

		return contentStream;
	}

	public PDPageContentStream createPaymentformation(PDPageContentStream contentStream) throws IOException {
		drawString.drawString(contentStream, X0 + marginX, previousContentY -= 25, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE += 2, "Fee Description : ");
		drawString.drawString(contentStream, X0 + marginX + 90, previousContentY, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE, feeInfo.getDescription());
		int index = 0;
		Map<String, String> paymentMap = new LinkedHashMap<String, String>();
		paymentMap.put("Payment Date", feeInfo.getPaidDate());
		paymentMap.put("Mode Of Payment", feeInfo.getPaymentMode());
		paymentMap.put("Payment For", feeInfo.getPaymentFor());
		paymentMap.put("Academic Year", feeInfo.getAcademicYear());
		paymentMap.put("Standard/Course", feeInfo.getStandard());
		paymentMap.put("Division", feeInfo.getDivision());
		paymentMap.put("Student Registration Code", feeInfo.getRegistrationCode());
		paymentMap.put("Qfix Reference Number", feeInfo.getReferenceNumber());
		paymentMap.put("Fees Amount", feeInfo.getAmountPaid() + " "+feeInfo.getCurrencyCode());
		paymentMap.put("Charges", feeInfo.getOtherCharges() + "");

		if(!"ONLINE".equals(feeInfo.getPaymentMode())){
			paymentMap.put("Bank Name", feeInfo.getBankName());
			paymentMap.put("Branch Name", feeInfo.getBankBranchName());
			paymentMap.put("IFSC Code", feeInfo.getIfscCode());
			paymentMap.put((feeInfo.getPaymentMode().equals("Cheque") ? "Cheque" : "DD") + " Number", feeInfo.getCheckOrDDNumber());
		}

		if(!StringUtils.isEmpty(feeInfo.getRefundAmount())){
			paymentMap.put("Refund Amount", feeInfo.getRefundAmount());
			paymentMap.put("Refund Date", feeInfo.getRefundDate());
			paymentMap.put("Refund Status", feeInfo.getRefundStatus());
		}

		for (Map.Entry<String, String> entry : paymentMap.entrySet()) {
			index++;
			if (index % 2 != 0) {
				drawString.drawString(contentStream, X0 + marginX, previousContentY -= 25, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE, entry.getKey()
								+ " : " + entry.getValue());
			}
			else {
				drawString.drawString(contentStream, X0 + marginX + 300, previousContentY, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE, entry.getKey()
								+ " : " + entry.getValue());
			}
		}
		contentStream.drawLine(X0 - 30, previousContentY -= 20, X0 + marginX + ROW_WIDTH, previousContentY);
		return contentStream;
	}

	public PDPageContentStream createFooter(PDPageContentStream contentStream) throws IOException {
		String note="Please note: This is a transaction receipt & for all offline payments (i.e. Cheques/DD/RTGS/NEFT), the receipt is valid only upon successful realisation of funds in the merchant account.";
		List<String> noteList = stringSplit(note, 100, 0);
		for (int i = 0; i < noteList.size(); i++) {
			//contentStream.drawString(splitedHeaderTextAddressName.get(i));
			drawString.drawString(contentStream, X0 + marginX + 10, previousContentY -= 25, PDType1Font.TIMES_ITALIC,
							CONTENT_FONT_SIZE, noteList.get(i));
			contentStream.appendRawCommands("T*\n");
			//contentStream.moveTextPositionByAmount(0, -12);
		}	
		String trailorRecord = "Thanks for your payment.";
		if (branchConfiguration != null && !StringUtils.isEmpty(branchConfiguration.getTrailerRecord())) {
			trailorRecord = branchConfiguration.getTrailerRecord();
		}

		List<String> splitedHeaderTextAddressName = stringSplit(trailorRecord, 80, 0);

		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.setFont(PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE);
		//contentStream.moveTextPositionByAmount(getCenterTextX(RECTANGLE_WIDTH), (previousContentY) - lastContentOfYForWrapping);
		for (int i = 0; i < splitedHeaderTextAddressName.size(); i++) {
			//contentStream.drawString(splitedHeaderTextAddressName.get(i));
			drawString.drawString(contentStream, X0 + marginX + 10, previousContentY -= 25, PDType1Font.HELVETICA_BOLD,
							CONTENT_FONT_SIZE, splitedHeaderTextAddressName.get(i));
			contentStream.appendRawCommands("T*\n");
			//contentStream.moveTextPositionByAmount(0, -12);
		}

		contentStream.drawLine(X0 - 30, previousContentY -= 25, X0 + marginX + ROW_WIDTH, previousContentY);
		firstLineYPosition = previousContentY;

		return contentStream;
		/*drawString.drawString(contentStream, X0 + marginX + 90, previousContentY -= 25, PDType1Font.HELVETICA_BOLD,
						CONTENT_FONT_SIZE, trailorRecord);

		contentStream.drawLine(X0 - 30, previousContentY -= 25, X0 + marginX + ROW_WIDTH, previousContentY);
		return contentStream;*/
	}

	public PDPageContentStream createBreakdownContent(PDDocument doc, PDPageContentStream contentStream, PDFont font) throws IOException {
		List<DisplayLayout> displayLayouts = feeInfo.getDisplayLayouts();
		if (displayLayouts != null && displayLayouts.size() > 0) {
			drawString.drawString(contentStream, X0 + marginX, previousContentY -= 25, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE, "Description");
			float text_width = (font.getStringWidth("Amount") / 1000.0f) * CONTENT_FONT_SIZE;
			float nextTextX = PDPage.PAGE_SIZE_A4.getUpperRightX() - text_width;
			drawString.drawString(contentStream, nextTextX - 10, previousContentY, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE, "Amount");
			contentStream.drawLine(X0 - 30, previousContentY -= 20, X0 + marginX + ROW_WIDTH, previousContentY);
			tempYPosition = previousContentY;
			List<String> listOfFees = new ArrayList<String>();

			List<String> listOfAmmount = new ArrayList<String>();

			for (DisplayLayout displayLayout : displayLayouts) {
				listOfFees.add(displayLayout.getFeeDescription());
				listOfAmmount.add(displayLayout.getFeeAmount() + "");
			}
			int index = 0;
			for (String fees : listOfFees) {
				String amount = listOfAmmount.get(index);
				index++;
				contentStream = checkEndOfPage(contentStream, doc, tempYPosition, firstLineYPosition);
				previousContentY -= 25;
				drawString.drawString(contentStream, X0 + marginX, previousContentY, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE, fees);
				drawString.drawString(contentStream, nextTextX - 10, previousContentY, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE, amount);

				contentStream.drawLine(nextTextX - 115, previousContentY + 25, nextTextX - 115, previousContentY - 25);
			}
			//			contentStream.drawLine(nextTextX - 115, previousContentY + 75, nextTextX - 115, previousContentY);
			/*previousContentY += (25 * listOfFees.size());
			for (String ammount : listOfAmmount) {
				contentStream = checkEndOfPage(contentStream, doc, tempYPosition, firstLineYPosition);
				text_width = (font.getStringWidth(ammount) / 1000.0f) * CONTENT_FONT_SIZE;
				nextTextX = PDPage.PAGE_SIZE_A4.getUpperRightX() - text_width;
				drawString.drawString(contentStream, nextTextX - 10, previousContentY -= 25, PDType1Font.HELVETICA_BOLD, CONTENT_FONT_SIZE, ammount);
			}*/

			previousContentY += (25 * listOfAmmount.size());
			previousContentY -= (listOfAmmount.size() > listOfFees.size()) ? (25 * listOfAmmount.size()) : (25 * listOfFees.size());

			previousContentY -= 25;
			contentStream.drawLine(X0 - 30, previousContentY, X0 + marginX + ROW_WIDTH, previousContentY);
			/*			for (int i = 1; i < NUMBER_OF_PAGES; i++) {
							float Y = NUMBER_OF_PAGES - i == 1 ? previousContentY : 25f;
							contentStream.drawLine(X0 - 30, PDPage.PAGE_SIZE_A4.getUpperRightY() - 25, X0 + marginX + ROW_WIDTH, Y);
						}*/
		}
		return contentStream;
	}

	int flag;
	public List<String> list = new ArrayList<String>();

	public List<String> stringSplit(String text, int limit, int flag) // method to split text or wrap text
	{
		if (flag == 0) {
			list.clear();
			flag = 1;
		}
		//List<String> innerStringSplit(String text1, int limit1){
		if (!StringUtils.isEmpty(text)) {
			int thislimit = limit;
			int spacePosition = 0, z = 0;
			if (text.length() > thislimit) {
				if (text.substring(0, thislimit).contains(" ")) {
					for (int i = z; i < thislimit; i++) {
						if (text.charAt(i) == ' ') {
							spacePosition = i;
						}
					}
					if (text.charAt(spacePosition) == ' ') {
						list.add(text.substring(0, spacePosition));
						stringSplit(text.substring(spacePosition + 1, text.length()), thislimit, 1);
					} else {
						list.add(text.substring(0, spacePosition));
						stringSplit(text.substring(spacePosition, text.length()), thislimit, 1);
					}
				} else {
					list.add(text.substring(0, thislimit));
					stringSplit(text.substring(thislimit, text.length()), thislimit, 1);
				}
			} else {
				list.add(text);
			}
		}

		return list;
	}

	private static float getCenterTextX(float width) throws IOException {
		float widthCenter = (width / 2);
		return widthCenter;
	}

	public PDPageContentStream checkEndOfPage(PDPageContentStream contentStream, PDDocument doc, float tempYPosition, float firstLineYPosition)
					throws IOException {
		PDPageContentStream contentStream1 = contentStream;
		if (previousContentY < 56) {
			contentStream.close();
			contentStream1 = new PDPageContentStream(doc, addNewPage(doc), true, true, true);
			previousContentY = 780;
			NUMBER_OF_PAGES++;
		}
		return contentStream1;
	}

	private static PDPage addNewPage(PDDocument doc) throws IOException {
		PDPage page1 = new PDPage();
		doc.addPage(page1);
		return page1;
	}

	public void drawImage(PDDocument doc, String path, float x, float y, float width, float height, PDPageContentStream contentStream) throws IOException {
		try {
			PDXObject xImage = null;
			String image = path;
			System.out.println("File path to draw image ::: \n" + path);
			if (image.toLowerCase().endsWith(".jpg"))
			{
				xImage = new PDJpeg(doc, new FileInputStream(image));
			}
			else if (image.toLowerCase().endsWith(".tif") || image.toLowerCase().endsWith(".tiff"))
			{
				xImage = new PDCcitt(doc, (RandomAccess) new RandomAccessFile(new File(image), "r"));
			}
			else
			{
				BufferedImage awtImage = ImageIO.read(new File(image));
				xImage = new PDPixelMap(doc, awtImage);
			}

			contentStream.drawXObject(xImage, x, y, width, height);
		} catch (Exception e) {
			System.err.println("Logo not Found...");
			e.printStackTrace();
		}
	}
}
