package com.isg.service.user.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.request.FeesCode;

@Component
public class StudentRegistration {
	final static Logger logger = Logger.getLogger(StudentRegistration.class);
	private Integer id;

	@NotNull(message = "Student`s first name is mandatory")
	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	@Size(max = 45, message = "Student`s first name cannot be greater than 45 letters.")
	private String firstname;

	private String createStudentLogin;
	
	

	@Pattern(regexp = "^[\\p{L}\\s.’`\\-,]+$", message = "Must be valid name.")
	@NotNull(message = "Student`s surname is mandatory")
	@Size(max = 45, message = "Student`s surname cannot be greater than 45 letters.")
	private String surname;

	private String studentUserId;

	@Pattern(regexp = "$|^[FM]{1,1}$", message = "Must be valid gender.(e.g.: M or F)")
	private String gender;

	// @Pattern(regexp =
	// "$|^(\\d{4})-(0?[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])|(0[1-9]|[12]\\d|3[01])-(0?[1-9]|1[012])-(\\d{4})$",
	// message = "Must be valid date eg (15-10-2015)")
	@JsonProperty("dob")
	private String dateOfBirth;

	private String branch;

	private Integer branchId;
	
	private List<Integer> feesCodeId;
	
	
	public List<Integer> getFeesCodeId() {
		return feesCodeId;
	}

	public void setFeesCodeId(List<Integer> feesCodeId) {
		this.feesCodeId = feesCodeId;
	}

	private String feesCodeName;

	@Size(max = 45, message = "Student`s email is too long must not be graeter than 45 letters.")
	@Pattern(regexp = "$|^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w]{2,3}$", message = "Must be valid email address.")
	private String emailAddress;

	@Pattern(regexp = "$|^[0-9]{10,10}$", message = "Must be valid mobile number.")
	private String mobile;

	@Size(max = 300, message = "Must be less than 300 charecters.")
	private String addressLine1;

	private String area;

	

	@Pattern(regexp = "$|^[0-9]{6,6}$", message = "Must be valid pincode.")
	private String pincode;

	
	@Pattern(regexp = "$|^[A-Za-z,  () -]{2,45}$", message = "Must be valid caste.")
	// add - this for upload of student cast
	private String caste;

	private String otp;
	private String academicYear;
	private String branchName;
	private Integer standardId;
	@Pattern(regexp = "$|^[A-Z a-z 0-9]{1,35}$", message = "Must be valid standard.")
	private String standardName;

	private Integer divisionId;
	

	public String getStudentUserId() {
		return studentUserId;
	}

	public void setStudentUserId(String studentUserId) {
		this.studentUserId = studentUserId;
	}
	public String getFeesCodeName() {
		return feesCodeName;
	}

	public void setFeesCodeName(String feesCodeName) {
		this.feesCodeName = feesCodeName;
	}

	// @Pattern(regexp = "^[0-9]{1,45}$", message =
	// "Must be valid roll number.")
	@Size(max = 45, message = "Must not be greater than 45 charecters.")
	private String rollNumber;

	@Size(max = 45, message = "Must not be greater than 45 charecters.")
	private String studentRegistrationId;

	private Integer casteId;
	
	private Integer instituteId;

	@Size(max = 45, message = "Must not be greater than 45 charecters.")
	// @Pattern(regexp = "^[A-Z a-z 0-9]{2,10}$", message =
	// "Must be valid registration code.")
	private String registrationCode;

	private String active;
	private String isDelete;

	private String divisionName;

	private String password;
	
	private String name;

	private String username;

	private Integer stateId;
	private Integer districtId;
	

	private String state;
	private String district;
	




	


	private Integer userId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}



	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	
	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	

	

	

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Integer getStandardId() {
		return standardId;
	}

	public void setStandardId(Integer standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Integer divisionId) {
		this.divisionId = divisionId;
	}

	

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getStudentRegistrationId() {
		return studentRegistrationId;
	}

	public void setStudentRegistrationId(String studentRegistrationId) {
		this.studentRegistrationId = studentRegistrationId;
	}

	public Integer getCasteId() {
		return casteId;
	}

	public void setCasteId(Integer casteId) {
		this.casteId = casteId;
	}

	

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	
	

	
	public String getCreateStudentLogin() {
		return createStudentLogin;
	}

	public void setCreateStudentLogin(String createStudentLogin) {
		this.createStudentLogin = createStudentLogin;
	}



	@Override
	public String toString() {
		return "StudentRegistration [id=" + id + ", firstname=" + firstname
				+ ", createStudentLogin=" + createStudentLogin + ", surname="
				+ surname + ", studentUserId=" + studentUserId + ", gender="
				+ gender + ", dateOfBirth=" + dateOfBirth + ", branch="
				+ branch + ", branchId=" + branchId + ", feesCodeId="
				+ feesCodeId + ", feesCodeName=" + feesCodeName
				+ ", emailAddress=" + emailAddress + ", mobile=" + mobile
				+ ", addressLine1=" + addressLine1 + ", area=" + area
				+ ", pincode=" + pincode + ", caste=" + caste + ", otp=" + otp
				+ ", academicYear=" + academicYear + ", branchName="
				+ branchName + ", standardId=" + standardId + ", standardName="
				+ standardName + ", divisionId=" + divisionId + ", rollNumber="
				+ rollNumber + ", studentRegistrationId="
				+ studentRegistrationId + ", casteId=" + casteId
				+ ", instituteId=" + instituteId + ", registrationCode="
				+ registrationCode + ", active=" + active + ", isDelete="
				+ isDelete + ", divisionName=" + divisionName + ", password="
				+ password + ", name=" + name + ", username=" + username
				+ ", stateId=" + stateId + ", districtId=" + districtId
				+ ", state=" + state + ", district=" + district + ", userId="
				+ userId + ", getStudentUserId()=" + getStudentUserId()
				+ ", getFeesCodeId()=" + getFeesCodeId()
				+ ", getFeesCodeName()=" + getFeesCodeName() + ", getUserId()="
				+ getUserId() + ", getId()=" + getId() + ", getFirstname()="
				+ getFirstname() + ", getSurname()=" + getSurname()
				+ ", getGender()=" + getGender() + ", getDateOfBirth()="
				+ getDateOfBirth() + ", getBranch()=" + getBranch()
				+ ", getBranchId()=" + getBranchId() + ", getEmailAddress()="
				+ getEmailAddress() + ", getMobile()=" + getMobile()
				+ ", getAddressLine1()=" + getAddressLine1() + ", getArea()="
				+ getArea() + ", getPincode()=" + getPincode()
				+ ", getCaste()=" + getCaste() + ", getOtp()=" + getOtp()
				+ ", getAcademicYear()=" + getAcademicYear()
				+ ", getBranchName()=" + getBranchName() + ", getStandardId()="
				+ getStandardId() + ", getStandardName()=" + getStandardName()
				+ ", getDivisionId()=" + getDivisionId() + ", getRollNumber()="
				+ getRollNumber() + ", getStudentRegistrationId()="
				+ getStudentRegistrationId() + ", getCasteId()=" + getCasteId()
				+ ", getInstituteId()=" + getInstituteId()
				+ ", getRegistrationCode()=" + getRegistrationCode()
				+ ", getActive()=" + getActive() + ", getIsDelete()="
				+ getIsDelete() + ", getDivisionName()=" + getDivisionName()
				+ ", getPassword()=" + getPassword() + ", getName()="
				+ getName() + ", getUsername()=" + getUsername()
				+ ", getStateId()=" + getStateId() + ", getDistrictId()="
				+ getDistrictId() + ", getState()=" + getState()
				+ ", getDistrict()=" + getDistrict()
				+ ", getCreateStudentLogin()=" + getCreateStudentLogin()
				+ ", hashCode()=" + hashCode() + ", getClass()=" + getClass()
				+ ", toString()=" + super.toString() + "]";
	}

	@Override
	public int hashCode() {
		String conactenatedString = "";
		final int prime = 31;
		int result = 1;
		conactenatedString += ((studentUserId == null) ? "" : studentUserId
				.toLowerCase().replaceAll(" ", ""));
		conactenatedString += ((emailAddress == null) ? "" : emailAddress
				.toLowerCase().replaceAll(" ", ""));
		conactenatedString += ((firstname == null) ? "" : firstname
				.toLowerCase().replaceAll(" ", ""));
		conactenatedString += ((surname == null) ? "" : surname.toLowerCase()
				.replaceAll(" ", ""));
		conactenatedString += ((mobile == null) ? "" : mobile.toLowerCase()
				.replaceAll(" ", ""));

		conactenatedString += ((standardName == null) ? "" : standardName.toLowerCase()).replaceAll(" ", "");
		conactenatedString += ((divisionName == null) ? "" : divisionName.toLowerCase()).replaceAll(" ", "");
		conactenatedString += ((registrationCode == null) ? "" : registrationCode.toLowerCase()).replaceAll(" ", "");

		result = prime * conactenatedString.hashCode();
		return result;
	}
}
