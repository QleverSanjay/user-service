package com.isg.service.user.model;

public class UserChatProfile {

	String login;

	String password;

	long chatProfileId;

	public UserChatProfile(String login, String password, long chatProfileId) {
		this.login = login;
		this.password = password;
		this.chatProfileId = chatProfileId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getChatProfileId() {
		return chatProfileId;
	}

	public void setChatProfileId(long chatProfileId) {
		this.chatProfileId = chatProfileId;
	}

}
