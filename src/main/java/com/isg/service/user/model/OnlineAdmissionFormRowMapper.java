package com.isg.service.user.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.isg.service.user.request.Branch;

public class OnlineAdmissionFormRowMapper implements RowMapper<OnlineAdmissionFormDetail> {

	@Override
	public OnlineAdmissionFormDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
		OnlineAdmissionFormDetail onlineAdmissionFormDetail = new OnlineAdmissionFormDetail();
		onlineAdmissionFormDetail.setAcademicYear(rs.getString("academic_year"));
		onlineAdmissionFormDetail.setId(rs.getInt("application_id"));
		onlineAdmissionFormDetail.setFirstName(rs.getString("applicant_firstname"));
		onlineAdmissionFormDetail.setMiddleName(rs.getString("applicant_middlename"));
		onlineAdmissionFormDetail.setSurname(rs.getString("applicant_surname"));
		onlineAdmissionFormDetail.setGender(rs.getString("applicant_gender").charAt(0));
		onlineAdmissionFormDetail.setDob(rs.getString("applicant_dob"));
		onlineAdmissionFormDetail.setMobile(rs.getString("applicant_mobile"));
		onlineAdmissionFormDetail.setEmail(rs.getString("applicant_email"));
		onlineAdmissionFormDetail.setApplicantId(rs.getString("applicant_id"));
		onlineAdmissionFormDetail.setPassword(rs.getString("applicant_access_password"));
		onlineAdmissionFormDetail.setFormPayload(rs.getString("form_payload"));
		onlineAdmissionFormDetail.setPdfFilePath(rs.getString("print_pdf_path"));
		onlineAdmissionFormDetail.setPhotoPath(rs.getString("photo_path"));
		onlineAdmissionFormDetail.setSignaturePath(rs.getString("signature_image_path"));
		onlineAdmissionFormDetail.setPaymentStatus(rs.getString("payment_status"));
		onlineAdmissionFormDetail.setUsername(rs.getString("username"));
		onlineAdmissionFormDetail.setCategory(rs.getString("category"));

		Branch branch = new Branch();
		branch.setId(rs.getInt("id"));
		branch.setName(rs.getString("name"));
		branch.setAddressLine(rs.getString("address_line"));
		branch.setArea(rs.getString("area"));
		branch.setCity(rs.getString("city"));
		branch.setPincode(rs.getString("pincode"));
		branch.setContactnumber(rs.getString("contactNumber"));
		branch.setContactemail(rs.getString("contactEmail"));
		branch.setWebsiteurl(rs.getString("websiteUrl"));
		branch.setInstituteId(rs.getInt("institute_id"));
		onlineAdmissionFormDetail.setBranch(branch);

		return onlineAdmissionFormDetail;
	}
}
