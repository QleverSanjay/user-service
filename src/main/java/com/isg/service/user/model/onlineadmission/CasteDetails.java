package com.isg.service.user.model.onlineadmission;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
				"caste",
				"subcaste",
				"religion",
				"minority"
})
public class CasteDetails {

	@JsonProperty("caste")
	@Valid
	private Caste caste;

	@JsonProperty("subcaste")
	@Valid
	private Subcaste subcaste;

	@JsonProperty("religion")
	@Valid
	private Religion religion;

	@JsonProperty("minority")
	@NotNull
	private String minority;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public CasteDetails() {
	}

	/**
	 * 
	 * @param religion
	 * @param minority
	 * @param caste
	 * @param subcaste
	 */
	public CasteDetails(Caste caste, Subcaste subcaste, Religion religion, String minority) {
		this.caste = caste;
		this.subcaste = subcaste;
		this.religion = religion;
		this.minority = minority;
	}

	/**
	 * 
	 * @return The caste
	 */
	@JsonProperty("caste")
	public Caste getCaste() {
		return caste;
	}

	/**
	 * 
	 * @param caste
	 *            The caste
	 */
	@JsonProperty("caste")
	public void setCaste(Caste caste) {
		this.caste = caste;
	}

	/**
	 * 
	 * @return The subcaste
	 */
	@JsonProperty("subcaste")
	public Subcaste getSubcaste() {
		return subcaste;
	}

	/**
	 * 
	 * @param subcaste
	 *            The subcaste
	 */
	@JsonProperty("subcaste")
	public void setSubcaste(Subcaste subcaste) {
		this.subcaste = subcaste;
	}

	/**
	 * 
	 * @return The religion
	 */
	@JsonProperty("religion")
	public Religion getReligion() {
		return religion;
	}

	/**
	 * 
	 * @param religion
	 *            The religion
	 */
	@JsonProperty("religion")
	public void setReligion(Religion religion) {
		this.religion = religion;
	}

	/**
	 * 
	 * @return The minority
	 */
	@JsonProperty("minority")
	public String getMinority() {
		return minority;
	}

	/**
	 * 
	 * @param minority
	 *            The minority
	 */
	@JsonProperty("minority")
	public void setMinority(String minority) {
		this.minority = minority;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
