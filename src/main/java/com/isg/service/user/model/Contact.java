package com.isg.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact {

	String firstName;

	String lastName;

	String city;

	String photo;

	Integer userId;

	@JsonIgnoreProperties
	String email;

	@JsonIgnoreProperties
	String phone;

	public Contact() {

	}

	public Contact(String firstName, String lastName, String city, String photo, Integer userId, String email, String phone) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.photo = photo;
		this.userId = userId;
		this.email = email;
		this.phone = phone;
	}

	@JsonProperty(value = "first_name")
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty(value = "last_name")
	public String getLastName() {
		return (lastName != null) ? lastName : "";
	}

	@JsonProperty(value = "city")
	public String getCity() {
		return city;
	}

	@JsonProperty(value = "photo")
	public String getPhoto() {
		return photo;
	}

	@JsonProperty(value = "user_id")
	public Integer getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	@Override
	public String toString() {
		return "Contact [firstName=" + firstName + ", lastName=" + lastName + ", city=" + city + ", photo=" + photo + ", userId=" + userId + ", email=" + email
						+ ", phone=" + phone + "]";
	}

}
