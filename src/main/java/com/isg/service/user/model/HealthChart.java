package com.isg.service.user.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HealthChart {
	private String[] series;
	private String[] weeks;
	private List<Object[]> data = new ArrayList<Object[]>();

	public String[] getSeries() {
		return series;
	}

	public void setSeries(String[] series) {
		this.series = series;
	}

	public String[] getWeeks() {
		return weeks;
	}

	public void setWeeks(String[] weeks) {
		this.weeks = weeks;
	}

	public List<Object[]> getData() {
		return data;
	}

	public void setData(List<Object[]> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "HealthChart [series=" + Arrays.toString(series) + ", weeks=" + Arrays.toString(weeks) + ", data=" + data + "]";
	}

}
