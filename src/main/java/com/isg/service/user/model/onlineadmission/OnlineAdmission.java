
package com.isg.service.user.model.onlineadmission;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isg.service.user.request.Branch;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OnlineAdmission {

	@JsonProperty("form_number")
	@JsonIgnore
	String formNumber;

	@JsonProperty("admission_id")
	private Integer admissionId;

	@JsonProperty("url_configuration_id")
	@Digits(integer = 11, fraction = 0)
	Integer urlConfigurationId;

	@JsonProperty("applicatant_id")
	@NotNull
	private String applicationId;

	@JsonProperty("course_details")
	@Valid
	private CourseDetails courseDetails;

	@JsonProperty("academic_details")
	@Valid
	private List<AcademicDetail> academicDetails = new ArrayList<AcademicDetail>();

	@JsonProperty("qualifying_exam_details")
	@Valid
	private QualifyingExamDetails qualifyingExamDetails;

	@JsonProperty("candidate_details")
	@Valid
	@NotNull
	private CandidateDetails candidateDetails;

	@JsonProperty("contact_details")
	@Valid
	@NotNull
	private ContactDetails contactDetails;

	@JsonProperty("mother_details")
	@Valid
	private ParentDetail motherDetails;

	@JsonProperty("father_details")
	@Valid
	private ParentDetail fatherDetails;

	@JsonProperty("guardian_details")
	@Valid
	private GuardianDetails guardianDetails;

	@JsonProperty("address_details")
	@Valid
	private AddressDetails addressDetails;

	@JsonProperty("documents")
	@Valid
	private List<Document> documents = new ArrayList<Document>();

	@JsonProperty("branch_detail")
	private Branch branch;

	@JsonProperty("general_comment")
	private String generalComment;

	@JsonProperty("sibling_details")
	private List<SiblingDetail> siblingDetail;

	@JsonIgnoreProperties
	String paymentStatus;

	@JsonIgnoreProperties
	String qfixReferenceNumber;

	@JsonIgnoreProperties
	String paymentDate;

	@JsonIgnoreProperties
	String modeOfPayment;

	@JsonIgnoreProperties
	String amount;

	@JsonProperty("category")
	String category;

	@JsonProperty("schoolVerified")
	private String schoolVerified;

	@JsonProperty("verifiedAt")
	private String verifiedAt;

	private String uniqueIdentifier;
	private String academicYear;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("ispartofsports")
	private int ispartofsports;

	/**
	 * 
	 * @param courseDetails
	 * @param fatherDetails
	 * @param addressDetails
	 * @param candidateDetails
	 * @param documents
	 * @param gaurdianDetails
	 * @param applicationId
	 * @param qualifyingExamDetails
	 * @param contactDetails
	 * @param motherDetails
	 * @param academicDetails
	 */
	public OnlineAdmission(Integer urlConfigurationId, String applicationId, CourseDetails courseDetails,
			List<AcademicDetail> academicDetails, QualifyingExamDetails qualifyingExamDetails,
			CandidateDetails candidateDetails, ContactDetails contactDetails, ParentDetail motherDetails,
			ParentDetail fatherDetails, GuardianDetails guardianDetails, AddressDetails addressDetails,
			List<Document> documents, String generalComment) {
		this.urlConfigurationId = urlConfigurationId;
		this.applicationId = applicationId;
		this.courseDetails = courseDetails;
		this.academicDetails = academicDetails;
		this.qualifyingExamDetails = qualifyingExamDetails;
		this.candidateDetails = candidateDetails;
		this.contactDetails = contactDetails;
		this.motherDetails = motherDetails;
		this.fatherDetails = fatherDetails;
		this.guardianDetails = guardianDetails;
		this.addressDetails = addressDetails;
		this.documents = documents;
		this.generalComment = generalComment;
	}

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public OnlineAdmission() {
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getIspartofsports() {
		return ispartofsports;
	}

	public void setIspartofsports(int ispartofsports) {
		this.ispartofsports = ispartofsports;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getSchoolVerified() {
		return schoolVerified;
	}

	public void setSchoolVerified(String schoolVerified) {
		this.schoolVerified = schoolVerified;
	}

	public String getVerifiedAt() {
		return verifiedAt;
	}

	public void setVerifiedAt(String verifiedAt) {
		this.verifiedAt = verifiedAt;
	}

	public Integer getAdmissionId() {
		return admissionId;
	}

	public void setAdmissionId(Integer admissionId) {
		this.admissionId = admissionId;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}

	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}

	public Integer getUrlConfigurationId() {
		return urlConfigurationId;
	}

	public void setUrlConfigurationId(Integer urlConfigurationId) {
		this.urlConfigurationId = urlConfigurationId;
	}

	/**
	 * 
	 * @return The applicationId
	 */
	@JsonProperty("application_id")
	public String getApplicationId() {
		return applicationId;
	}

	/**
	 * 
	 * @param applicationId
	 *            The application_id
	 */
	@JsonProperty("application_id")
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * 
	 * @return The courseDetails
	 */
	@JsonProperty("course_details")
	public CourseDetails getCourseDetails() {
		return courseDetails;
	}

	/**
	 * 
	 * @param courseDetails
	 *            The course_details
	 */
	@JsonProperty("course_details")
	public void setCourseDetails(CourseDetails courseDetails) {
		this.courseDetails = courseDetails;
	}

	/**
	 * 
	 * @return The academicDetails
	 */
	@JsonProperty("academic_details")
	public List<AcademicDetail> getAcademicDetails() {
		return academicDetails;
	}

	/**
	 * 
	 * @param academicDetails
	 *            The academic_details
	 */
	@JsonProperty("academic_details")
	public void setAcademicDetails(List<AcademicDetail> academicDetails) {
		this.academicDetails = academicDetails;
	}

	/**
	 * 
	 * @return The qualifyingExamDetails
	 */
	@JsonProperty("qualifying_exam_details")
	public QualifyingExamDetails getQualifyingExamDetails() {
		return qualifyingExamDetails;
	}

	/**
	 * 
	 * @param qualifyingExamDetails
	 *            The qualifying_exam__details
	 */
	@JsonProperty("qualifying_exam_details")
	public void setQualifyingExamDetails(QualifyingExamDetails qualifyingExamDetails) {
		this.qualifyingExamDetails = qualifyingExamDetails;
	}

	/**
	 * 
	 * @return The candidateDetails
	 */
	@JsonProperty("candidate_details")
	public CandidateDetails getCandidateDetails() {
		return candidateDetails;
	}

	/**
	 * 
	 * @param candidateDetails
	 *            The candidate_details
	 */
	@JsonProperty("candidate_details")
	public void setCandidateDetails(CandidateDetails candidateDetails) {
		this.candidateDetails = candidateDetails;
	}

	/**
	 * 
	 * @return The contactDetails
	 */
	@JsonProperty("contact_details")
	public ContactDetails getContactDetails() {
		return contactDetails;
	}

	/**
	 * 
	 * @param contactDetails
	 *            The contact_details
	 */
	@JsonProperty("contact_details")
	public void setContactDetails(ContactDetails contactDetails) {
		this.contactDetails = contactDetails;
	}

	/**
	 * 
	 * @return The motherDetails
	 */
	@JsonProperty("mother_details")
	public ParentDetail getMotherDetails() {
		return motherDetails;
	}

	/**
	 * 
	 * @param motherDetails
	 *            The mother_details
	 */
	@JsonProperty("mother_details")
	public void setMotherDetails(ParentDetail motherDetails) {
		this.motherDetails = motherDetails;
	}

	/**
	 * 
	 * @return The fatherDetails
	 */
	@JsonProperty("father_details")
	public ParentDetail getFatherDetails() {
		return fatherDetails;
	}

	/**
	 * 
	 * @param fatherDetails
	 *            The father_details
	 */
	@JsonProperty("father_details")
	public void setFatherDetails(ParentDetail fatherDetails) {
		this.fatherDetails = fatherDetails;
	}

	/**
	 * 
	 * @return The gaurdianDetails
	 */
	@JsonProperty("gaurdian_details")
	public GuardianDetails getGuardianDetails() {
		return guardianDetails;
	}

	/**
	 * 
	 * @param gaurdianDetails
	 *            The gaurdian_details
	 */
	@JsonProperty("gaurdian_details")
	public void setGuardianDetails(GuardianDetails guardianDetails) {
		this.guardianDetails = guardianDetails;
	}

	/**
	 * 
	 * @return The addressDetails
	 */
	@JsonProperty("address_details")
	public AddressDetails getAddressDetails() {
		return addressDetails;
	}

	/**
	 * 
	 * @param addressDetails
	 *            The address_details
	 */
	@JsonProperty("address_details")
	public void setAddressDetails(AddressDetails addressDetails) {
		this.addressDetails = addressDetails;
	}

	/**
	 * 
	 * @return The documents
	 */
	@JsonProperty("documents")
	public List<Document> getDocuments() {
		return documents;
	}

	/**
	 * 
	 * @param documents
	 *            The documents
	 */
	@JsonProperty("documents")
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	
	

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getGeneralComment() {
		return generalComment;
	}

	public void setGeneralComment(String generalComment) {
		this.generalComment = generalComment;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getQfixReferenceNumber() {
		return qfixReferenceNumber;
	}

	public void setQfixReferenceNumber(String qfixReferenceNumber) {
		this.qfixReferenceNumber = qfixReferenceNumber;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public List<SiblingDetail> getSiblingDetail() {
		return siblingDetail;
	}

	public void setSiblingDetail(List<SiblingDetail> siblingDetail) {
		this.siblingDetail = siblingDetail;
	}

	/**
	 * @return the id
	 */
	public String getFormNumber() {
		return formNumber;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	public static void main(String args[]) throws JsonParseException, JsonMappingException, IOException {

		// String data =
		// "{\"course_details\":{\"course\":{\"id\":1,\"name\":\"FY
		// B.COM\"},\"seed_programme\":{\"compulsory_subject\":{\"id\":1,\"name\":\"Male\"},\"optional_subject\":{\"id\":1,\"name\":\"Male\"}},\"subject_preference\":{\"preference1\":{\"id\":1,\"name\":\"IT-SIP\"},\"preference2\":{\"id\":1,\"name\":\"French-Maths\"}}},\"academic_details\":[{\"exam\":\"SSC\",\"passing_year\":\"2015\",\"school\":\"SVJOshi\",\"board_university\":\"PUNE\",\"obtained_marks\":200,\"total_marks\":300,\"percentage\":80.20},{\"exam\":\"HSC\",\"passing_year\":\"2015\",\"school\":\"SVJOshi\",\"board_university\":\"PUNE\",\"obtained_marks\":200,\"total_marks\":300,\"percentage\":80.20}],\"qualifying_exam__details\":{\"exam\":{\"id\":1,\"name\":\"SSC\",\"other\":\"XYZ\"},\"passing_year\":\"2015\",\"passing_month\":\"January\",\"school_college\":\"COEP\",\"board_university\":{\"id\":1,\"name\":\"SSC\",\"other\":\"TINGTONHG\"},\"obtained_marks\":200,\"total_marks\":300,\"percentage\":80.02,\"education_gap\":\"YES\",\"grade\":\"YES\",\"exam_seat_no\":\"YES\",\"previous_stream\":{\"id\":1,\"name\":\"SSC\"},\"applicantfrom\":\"INSIDER\"},\"candidate_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"printed_name_on_marksheet\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"gender\":{\"id\":1,\"name\":\"Male\"},\"marital_status\":{\"id\":1,\"name\":\"MARRIED\"},\"dateofbirth\":\"03/05/2016\",\"placeofbirth\":\"Mumbai\",\"caste_details\":{\"caste\":{\"id\":1,\"name\":\"OPEN\"},\"subcaste\":{\"id\":1,\"name\":\"MARATHA\",\"other\":\"XYZ\"},\"religion\":{\"id\":1,\"name\":\"HINDU\"},\"minority\":\"YES\"},\"physically_handicapped\":{\"id\":1,\"name\":\"DEAF\"},\"form_details\":{\"application_form_no\":\"11111\",\"mkcl_registration_no\":\"11111\",\"physical_form_no\":\"11111\"},\"mother_tounge\":\"Marathi\",\"foreign_student\":\"NO\",\"aadhaaar_card\":12234,\"bank_details\":{\"bank_account\":12234,\"bank_name\":\"12234\",\"transaction_type\":\"OFFLINE\"},\"employment_details\":{\"employer_name\":\"TATA\",\"designation\":\"CLERK\"},\"ncc_pref\":\"NCC\",\"last_school_name\":\"DAV\",\"sports_details\":\"COLLEGE\",\"social_reservation\":{\"id\":1,\"name\":\"EBC\"},\"identification_mark\":\"ONLEFTLEG\",\"hobbies\":\"Cricket\",\"blood_group\":\"A+VE\",\"native_place_address\":\"Testtest\"},\"contact_details\":{\"personal_mobile_number\":\"11111\",\"personal_email\":\"test@test123.com\",\"parent_contact_number\":\"11111\"},\"mother_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"employment_details\":{\"employer_name\":\"TATA\",\"occupation\":\"EMPLOYEE\",\"designation\":\"CLERK\",\"annual_income\":\"10000\"},\"email\":\"test@test.com\"},\"father_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"employment_details\":{\"employer_name\":\"TATA\",\"occupation\":\"EMPLOYEE\",\"designation\":\"CLERK\",\"annual_income\":10000},\"email\":\"test@test.com\"},\"guardian_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"candidaterelation\":\"uncle\",\"email\":\"test@test.com\"},\"address_details\":{\"local_address\":{\"address_line1\":\"sawant\",\"house_number\":\"sushant\",\"area\":\"sadashiv\",\"pincode\":432022,\"country\":{\"id\":1,\"name\":\"A+VE\",\"other\":\"USA\"},\"state\":{\"id\":1,\"name\":\"A+VE\",\"other\":\"TEST\"},\"district\":{\"id\":1,\"name\":\"A+VE\",\"other\":\"TDGD\"},\"taluka\":{\"id\":1,\"name\":\"A+VE\",\"other\":\"XYSS\"}},\"permanent_address\":{\"address_line1\":\"sawant\",\"house_number\":\"sushant\",\"area\":\"sadashiv\",\"pincode\":432022,\"country\":{\"id\":1,\"name\":\"A+VE\",\"other\":\"USA\"},\"state\":{\"id\":1,\"name\":\"A+VE\",\"other\":\"TEST\"},\"district\":{\"id\":1,\"name\":\"A+VE\",\"other\":\"TDGD\"},\"taluka\":{\"id\":1,\"name\":\"A+VE\",\"other\":\"XYSS\"}}},\"documents\":[{\"id\":1,\"name\":\"ADmissionForm\"},{\"id\":1,\"name\":\"DeclarationForm\"}],\"branch_detail\":{\"id\":1,\"name\":\"HiranandaniFoundationSchool(Powai)\",\"addressLine\":\"RichmondStreet,HiranandaniGardens,Powai\",\"city\":\"Mumbai\",\"area\":\"NearL&TInfotechCompany\",\"pincode\":\"400001\",\"contactnumber\":\"9090909090\",\"contactemail\":\"hfs1@eduqfix.com\",\"websiteurl\":\"www.hfs1.com\",\"districtId\":null,\"talukaId\":null,\"instituteId\":null,\"stateId\":null,\"active\":null},\"applicatant_id\":\"AY2016-17/1234566\"}";

		// String data = "{\"address_details\": {\"local_address\":
		// {\"country\": {\"id\": 1,\"name\": \"INDIA\",\"other\":
		// null},\"state\": {\"id\": 1,\"code\": \"1\",\"name\": \"JAMMU AND
		// KASHMIR\",\"active\": true,\"other\": null},\"district\": {\"id\":
		// 4,\"code\": \"4\",\"name\": \"Srinagar\",\"stateId\": 1,\"active\":
		// true},\"taluka\": {\"id\": 16,\"code\": \"16\",\"name\":
		// \"Kangan\",\"districtId\": 4,\"active\": true,\"other\":
		// null},\"address_line1\": \"Ex distinctio. Labore pari\",\"area\":
		// \"Rerum non blanditiis maiores\",\"house_number\":
		// \"23333\",\"pincode\": 621234},\"permanent_address\": {\"country\":
		// {\"id\": 1,\"name\": \"INDIA\",\"other\": null},\"state\": {\"id\":
		// 7,\"code\": \"7\",\"name\": \"DELHI\",\"active\": true,\"other\":
		// null},\"district\": {\"id\": 11001,\"name\": \"OTHER\",\"other\":
		// \"delhieeee\"},\"taluka\": {\"id\": 12001,\"name\":
		// \"OTHER\",\"other\": \"tingtong\"},\"address_line1\": \"Dolor
		// voluptate fugit, volu\",\"area\": \"Ut id non itaque
		// mole\",\"house_number\": \"2334\",\"pincode\":
		// 761234}},\"isguardian\": \"1\",\"academic_details\": [{\"exam\":
		// \"SSC\",\"passing_year\": \"1975\",\"school\": \"Modi iure
		// in\",\"obtained_marks\": 10,\"total_marks\": 87,\"percentage\":
		// \"11.49\",\"board_university\": \"Impedit v\"}],\"sscexam\":
		// null,\"qualifying_exam__details\": {\"obtained_marks\":
		// 88,\"total_marks\": 200,\"percentage\": \"44.00\",\"education_gap\":
		// \"YES\",\"grade\": \"Illo mollitia qui minus explicabo Minim
		// tempore\",\"exam\": {\"code\": \"OTHER\",\"name\":
		// \"OTHER\",\"active\": true,\"other\":
		// \"otherexam\"},\"board_university\": {\"id\": 2,\"code\":
		// \"OTHER\",\"name\": \"OTHER\",\"active\": true},\"passing_month\":
		// \"February\",\"passing_year\": \"2008\",\"school_college\": \"Quia
		// aute et imp\",\"exam_seat_no\": \"222\",\"other_university_board\":
		// \"otherboard\"},\"guardian_details\": {\"name\": {\"surname\":
		// \"Hinton\",\"firstname\": \"Casey\",\"middlename\": \"Baker
		// Cantu\"},\"candidaterelation\": \"norela\"},\"father_details\":
		// {\"name\": {\"surname\": \"Kane\",\"firstname\":
		// \"Burke\",\"middlename\": \"Sydney Rosa\"},\"employment_details\":
		// {\"annual_income\": 63000,\"occupation\": \"Vero dolor est a
		// consequatur\",\"employer_name\": \"Excepteur blanditiis
		// occa\",\"designation\": \"Modi cumque distinctio\"},\"email\":
		// \"sojipiwywy@yahoo.com\"},\"course_details\": {\"course\": {\"id\":
		// 7,\"code\": \"37\",\"name\": \"FYJC ( COMMERCE )\",\"active\":
		// false,\"category\": \"JC\"},\"subject_preference\":
		// \"{\"preference1\": {\"id\":1,\"code\":\"IT – MATHS\",\"name\":\"IT –
		// MATHS\",\"active\":true,\"courseId\":null,\"type\":\"Preference1\"}\",\"preference2\":
		// \"{\"id\":7,\"code\":\"French – SP\",\"name\":\"French –
		// SP\",\"active\":true,\"courseId\":null,\"type\":\"Preference2\"}}\",\"seed_programme\":
		// {\"compulsory_subject\": {\"id\": 27,\"code\": \"Development
		// English\",\"name\": \"Development English\",\"active\":
		// true,\"courseId\": null,\"optionl\": false},\"optional_subject\":
		// {\"id\": 29,\"code\": \"Vedic Mathematics\",\"name\": \"Vedic
		// Mathematics\",\"active\": true,\"courseId\": null,\"optionl\":
		// true}}},\"candidate_details\": {\"name\": {\"surname\":
		// \"Curtis\",\"firstname\": \"Summeree\",\"middlename\": \"Daquan
		// Goodman\"},\"printed_name_on_marksheet\": {\"surname\":
		// \"Beach\",\"firstname\": \"Dalton\",\"middlename\": \"Raphael
		// Harrington\"},\"caste_details\": {\"minority\": \"YES\",\"caste\":
		// {\"id\": 10,\"identifier\": \"10\",\"code\": \"OPEN\",\"name\":
		// \"OPEN\",\"active\": true},\"religion\": {\"id\": 8,\"name\":
		// \"PARSI\"},\"subcaste\": {\"id\": 200,\"identifier\":
		// \"200\",\"code\": \"OTHER\",\"name\": \"OTHER\",\"active\":
		// true,\"other\": \"Test\"}},\"foreign_student\":
		// \"YES\",\"aadhaaar_card\": 92,\"bank_details\": {\"bank_name\":
		// \"Geoffrey Ayers\",\"bank_account\": 1,\"transaction_type\":
		// \"ONLINE\"},\"employment_details\": {\"employer_name\": \"Thor
		// Moran\",\"designation\": \"Fugiat saepe occaecat\"},\"ncc_pref\":
		// \"DLLE\",\"last_school_name\": \"Tyrone Fitzpatrick\",\"gender\":
		// {\"id\": 2,\"name\": \"FEMALE\"},\"marital_status\": {\"id\":
		// 2,\"name\": \"UNMARRIED\"},\"physically_handicapped\": {\"id\":
		// 9,\"name\": \"DYSLEXIA\"},\"sports_details\":
		// \"NATIONAL\",\"social_reservation\": {\"id\": 6,\"name\": \"FREEDOM
		// FIGHTER\"},\"blood_group\": \"AB -VE\",\"placeofbirth\": \"Proident
		// blanditii\",\"dateofbirth\":
		// \"2016-10-24T18:30:00.000Z\",\"form_details\":
		// {\"application_form_no\": \"2233333\",\"physical_form_no\":
		// \"333333\"},\"mother_tounge\": \"Laboris eius
		// ten\",\"identification_mark\": \"Aut dolore non conseq\",\"hobbies\":
		// \"A soluta non et incididunt es\",\"native_place_address\": \"Officia
		// dolores fugit\"},\"mother_details\": {\"name\": {\"firstname\":
		// \"Demetrius Avery\"},\"employment_details\": {\"occupation\": \"Neque
		// laboris quae non duis\",\"employer_name\": \"Expedita ut
		// consequat\",\"designation\": \"Ad eum iste obcaec\"}},\"isemployed\":
		// \"1\",\"ispartofsports\": \"1\",\"checkAddressFlag\":
		// true,\"contact_details\": {\"personal_email\":
		// \"govow@hotmail.com\",\"personal_mobile_number\":
		// \"2222222222\",\"parent_contact_number\":
		// \"22222222\"},\"confirmemail\": \"govow@hotmail.com\",\"documents\":
		// [{\"id\": 6,\"code\": \"14\",\"name\": \"ORIGINAL SCHOOL LEAVING
		// CERTIFICATE (10TH STD.)\",\"active\": true}, {\"id\": 4,\"code\":
		// \"17\",\"name\": \"FIRST AND LAST PAGE OF RATION CARD(ALL
		// STUDENTS)\",\"active\": true}, {\"id\": 7,\"code\": \"19\",\"name\":
		// \"PAY ORDER XEROX COPY\",\"active\": true}, {\"id\": 8,\"code\":
		// \"20\",\"name\": \"RESIDENCE PROOF (TELEPHONE BILL / ELECTRIC BILL /
		// PASSPORT / AADHAR CARD, ETC\",\"active\": true}, {\"id\": 2,\"code\":
		// \"1012\",\"name\": \"AUTHENTIC DOCUMENT OF BEING HINDI LINGUISTIC
		// (MINORITY QUOTA)\",\"active\": true}, {\"id\": 1,\"code\":
		// \"13\",\"name\": \"ADMISSION FORM\",\"active\": true}, {\"id\":
		// 5,\"code\": \"15\",\"name\": \"ORIGINAL MARKSHEET OF SSC
		// EXAM\",\"active\": true}],\"formType\": \"jc\",\"photo\":
		// {},\"signature\":
		// {},\"branch_detail\":{\"id\":1,\"name\":\"HiranandaniFoundationSchool(Powai)\",\"addressLine\":\"RichmondStreet,HiranandaniGardens,Powai\",\"city\":\"Mumbai\",\"area\":\"NearL&TInfotechCompany\",\"pincode\":\"400001\",\"contactnumber\":\"9090909090\",\"contactemail\":\"hfs1@eduqfix.com\",\"websiteurl\":\"www.hfs1.com\",\"districtId\":null,\"talukaId\":null,\"instituteId\":null,\"stateId\":null,\"active\":null},\"from_date\":\"01-10-2016\",\"to_date\":\"31-10-2016\",\"academic_year\":\"2016-2017\",\"url_configuration_id\":1}";

		String data = "{\"course_details\":{\"course\":{\"id\":1,\"name\":\"FYJC\"}},\"candidate_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"gender\":{\"id\":1,\"name\":\"Male\"},\"dateofbirth\":\"03/05/2016\",\"placeofbirth\":\"Mumbai\",\"last_school_name\":\"DAV\",\"languages_known\":\"english,hindi\"},\"mother_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"employment_details\":{\"employer_name\":\"TATA\",\"occupation\":\"EMPLOYEE\",\"address\":\"Officeaddress\"},\"email\":\"mother@test.com\",\"citizenship\":\"indian\",\"age\":40,\"school_detail\":\"Xeviers\",\"college_detail\":\"IIT\",\"primary_phone\":\"1234567890\",\"secondary_phone\":\"1234567890\",\"hobbies\":\"sports\"},\"father_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"employment_details\":{\"employer_name\":\"TATA\",\"occupation\":\"EMPLOYEE\",\"address\":\"Officeaddress\"},\"email\":\"mother@test.com\",\"citizenship\":\"indian\",\"age\":40,\"school_detail\":\"Xeviers\",\"college_detail\":\"IIT\",\"primary_phone\":\"1234567890\",\"secondary_phone\":\"1234567890\",\"hobbies\":\"sports\"},\"address_details\":{\"local_address\":{\"address_line1\":\"sawant\"}},\"branch_detail\":{\"id\":1,\"name\":\"HiranandaniFoundationSchool(Powai)\",\"addressLine\":\"RichmondStreet,HiranandaniGardens,Powai\",\"city\":\"Mumbai\",\"area\":\"NearL&TInfotechCompany\",\"pincode\":\"400001\",\"contactnumber\":\"9090909090\",\"contactemail\":\"hfs1@eduqfix.com\",\"websiteurl\":\"www.hfs1.com\",\"districtId\":null,\"talukaId\":null,\"instituteId\":null,\"stateId\":null,\"active\":null},\"general_comment\":\"Getbacktome\"}";

		String data1 = "{\"course_details\":{\"course\":{\"id\":1,\"name\":\"FYJC\"}},\"candidate_details\":{\"name\":{\"firstname\":\"sushant\"},\"gender\":{\"id\":1,\"name\":\"Male\"},\"dateofbirth\":\"10/10/2010\"},\"contact_details\":{\"personal_mobile_number\":\"11111\",\"personal_email\":\"test@test123.com\"},\"mother_details\":{\"name\":{\"firstname\":\"sushant\"}},\"father_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"employment_details\":{\"employer_name\":\"TATA\",\"occupation\":\"EMPLOYEE\",\"address\":\"Officeaddress\"},\"email\":\"mother@test.com\",\"citizenship\":\"indian\",\"age\":40,\"school_detail\":\"Xeviers\",\"college_detail\":\"IIT\",\"primary_phone\":\"1234567890\",\"secondary_phone\":\"1234567890\",\"hobbies\":\"sports\"},\"sibling_details\":{\"name\":{\"surname\":\"sawant\",\"firstname\":\"sushant\",\"middlename\":\"sadashiv\"},\"dateofbirth\":\"04/04/1980\",\"placeofbirth\":\"mumbai\",\"currentschool\":\"Donbasco\",\"otherschoolattended\":\"zeekidspreschool\"},\"address_details\":{\"local_address\":{\"address_line1\":\"sawant\"}},\"branch_detail\":{\"id\":1,\"name\":\"HiranandaniFoundationSchool(Powai)\",\"addressLine\":\"RichmondStreet,HiranandaniGardens,Powai\",\"city\":\"Mumbai\",\"area\":\"NearL&TInfotechCompany\",\"pincode\":\"400001\",\"contactnumber\":\"9090909090\",\"contactemail\":\"hfs1@eduqfix.com\",\"websiteurl\":\"www.hfs1.com\",\"districtId\":null,\"talukaId\":null,\"instituteId\":null,\"stateId\":null,\"active\":null},\"general_comment\":\"Getbacktome\",\"url_configuration_id\": 3}";
		ObjectMapper mapper = new ObjectMapper();
		OnlineAdmission onlineAdmission = mapper.readValue(data1, OnlineAdmission.class);

		System.out.println(">>>" + onlineAdmission);

		// ApplicationReport applicationReport = new ApplicationReport();

		/*
		 * applicationReport.createPdf(onlineAdmission,
		 * "C:/Temp/images/Qfix/documents/external/r12r12gistra123ion/1/abcd.pdf",
		 * "C:/Users/samavish/Pictures/annual-day.png",
		 * "C:/Users/samavish/Pictures/annual-day.png");
		 */

	}

}
