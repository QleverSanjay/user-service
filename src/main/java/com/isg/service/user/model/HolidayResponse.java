package com.isg.service.user.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.request.Holiday;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HolidayResponse {

	@JsonProperty(value = "type")
	@NotNull
	String type;

	@JsonProperty(value = "identifier")
	@NotNull
	String identifier;

	@JsonProperty(value = "holidays")
	List<Holiday> holidays = new ArrayList<Holiday>();

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public List<Holiday> getHolidays() {
		return holidays;
	}

	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}

	@Override
	public String toString() {
		return "HolidayResponse [type=" + type + ", identifier=" + identifier
						+ ", holidays=" + holidays + "]";
	}

}
