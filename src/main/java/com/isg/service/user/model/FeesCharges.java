package com.isg.service.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FeesCharges {

	@JsonProperty("amount_added")
	double amountAdded;

	@JsonProperty("added_on")
	String addedOn;

	@JsonProperty("fee_for_duration")
	String feeForDuration;

	@JsonProperty("is_late_fee")
	boolean isLateFee;

	@JsonProperty("carry_forward_amount")
	double carryForwardAmount;

	public double getAmountAdded() {
		return amountAdded;
	}

	public void setAmountAdded(double amountAdded) {
		this.amountAdded = amountAdded;
	}

	public String getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(String addedOn) {
		this.addedOn = addedOn;
	}

	public String getFeeForDuration() {
		return feeForDuration;
	}

	public void setFeeForDuration(String feeForDuration) {
		this.feeForDuration = feeForDuration;
	}

	public boolean isLateFee() {
		return isLateFee;
	}

	public void setLateFee(boolean isLateFee) {
		this.isLateFee = isLateFee;
	}

	public double getCarryForwardAmount() {
		return carryForwardAmount;
	}

	public void setCarryForwardAmount(double carryForwardAmount) {
		this.carryForwardAmount = carryForwardAmount;
	}

}
