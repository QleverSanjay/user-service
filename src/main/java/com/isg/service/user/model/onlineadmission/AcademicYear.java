package com.isg.service.user.model.onlineadmission;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
				"id",
				"name"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AcademicYear {

	@JsonProperty("id")
	@NotNull
	private Integer id;

	@JsonProperty("name")
	@NotNull(message = "Please specify academic year name.")
	@Size(min = 10)
	private String name;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public AcademicYear() {
	}

	/**
	 * 
	 * @param id
	 * @param name
	 */
	public AcademicYear(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * 
	 * @return The id
	 */
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The name
	 */
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
