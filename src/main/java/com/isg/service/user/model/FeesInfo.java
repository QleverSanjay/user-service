package com.isg.service.user.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.isg.service.user.request.DisplayLayout;

public class FeesInfo {

	Integer branchId;

	@JsonProperty("amount_paid")
	double amountPaid;

	@JsonProperty("amount_Due")
	double amountDue;

	@JsonProperty("due_date")
	String dueDate;

	@JsonProperty("description")
	String description;

	@JsonProperty("base_fee_amount")
	double baseFeeAmount;

	@JsonProperty("display_layouts")
	List<DisplayLayout> displayLayouts;

	@JsonProperty("total_late_fee_added")
	double totalLateFeeAdded;

	@JsonProperty("total_fee_amount_added")
	double totalFeeAmountAdded;

	@JsonProperty("fees_charges")
	List<FeesCharges> feesCharges;

	@JsonProperty("payment_status")
	String paymentStatus;

	@JsonProperty("reference_number")
	String referenceNumber;

	@JsonProperty("payment_mode")
	String paymentMode;

	@JsonProperty("payment_for")
	String paymentFor;

	@JsonProperty("paid_by")
	String paidBy;

	@JsonProperty("paid_date")
	String paidDate;

	@JsonProperty("discount_amount")
	double discountAmount;

	@JsonProperty("other_charges")
	double otherCharges;

	private String bankName;
	private String bankBranchName;
	private String ifscCode;
	private String checkOrDDNumber;

	private String standard;
	private String division;
	private String academicYear;
	private String registrationCode;

	@JsonProperty("currency_icon_css")
	private String currencyIconCss;

	@JsonProperty("currency_code")
	private String currencyCode;

	@JsonProperty("refund_amount")
	String refundAmount;

	@JsonProperty("refund_status")
	String refundStatus;

	@JsonProperty("refund_date")
	String refundDate;

	public String getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(String refundDate) {
		this.refundDate = refundDate;
	}

	public String getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyIconCss() {
		return currencyIconCss;
	}

	public void setCurrencyIconCss(String currencyIconCss) {
		this.currencyIconCss = currencyIconCss;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranchName() {
		return bankBranchName;
	}

	public void setBankBranchName(String bankBranchName) {
		this.bankBranchName = bankBranchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getCheckOrDDNumber() {
		return checkOrDDNumber;
	}

	public void setCheckOrDDNumber(String checkOrDDNumber) {
		this.checkOrDDNumber = checkOrDDNumber;
	}

	public double getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Integer getBranchId() {
		return branchId;
	}

	public void setBranchId(Integer branchid) {
		this.branchId = branchid;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPaymentFor() {
		return paymentFor;
	}

	public void setPaymentFor(String paymentFor) {
		this.paymentFor = paymentFor;
	}

	public String getPaidBy() {
		return paidBy;
	}

	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public double getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(double amountDue) {
		this.amountDue = amountDue;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getBaseFeeAmount() {
		return baseFeeAmount;
	}

	public void setBaseFeeAmount(double baseFeeAmount) {
		this.baseFeeAmount = baseFeeAmount;
	}

	public List<DisplayLayout> getDisplayLayouts() {
		return displayLayouts;
	}

	public void setDisplayLayouts(List<DisplayLayout> displayLayouts) {
		this.displayLayouts = displayLayouts;
	}

	public double getTotalLateFeeAdded() {
		return totalLateFeeAdded;
	}

	public void setTotalLateFeeAdded(double totalLateFeeAdded) {
		this.totalLateFeeAdded = totalLateFeeAdded;
	}

	public double getTotalFeeAmountAdded() {
		return totalFeeAmountAdded;
	}

	public void setTotalFeeAmountAdded(double totalFeeAmountAdded) {
		this.totalFeeAmountAdded = totalFeeAmountAdded;
	}

	public List<FeesCharges> getFeesCharges() {
		return feesCharges;
	}

	public void setFeesCharges(List<FeesCharges> feesCharges) {
		this.feesCharges = feesCharges;
	}

}
