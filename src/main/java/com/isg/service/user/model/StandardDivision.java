package com.isg.service.user.model;

import java.util.List;

import org.springframework.stereotype.Component;

import com.isg.service.user.request.Division;

@Component
public class StandardDivision {
	private String standardName;
	private Integer standard;
	private List<Division> division;

	public List<Division> getDivision() {
		return division;
	}

	public void setDivision(List<Division> division) {
		this.division = division;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	public Integer getStandard() {
		return standard;
	}

	public void setStandard(Integer standard) {
		this.standard = standard;
	}

	@Override
	public String toString() {
		return "StandardDivision [standardName=" + standardName + ", standard="
				+ standard + ", division=" + division + "]";
	}

	
	

}
