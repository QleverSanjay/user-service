package com.isg.service.user.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentMoreInfo {

	@JsonProperty("admin_email")
	String adminEmail;

	@JsonProperty("student_ids")
	List<Integer> studentIds;

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public List<Integer> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(List<Integer> studentIds) {
		this.studentIds = studentIds;
	}

}
