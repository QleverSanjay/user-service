package com.isg.service.user.model;

public class UserForumProfile {

	String login;

	String password;

	long forumProfileId;

	public UserForumProfile(String login, String password, long forumProfileId) {
		this.login = login;
		this.password = password;
		this.forumProfileId = forumProfileId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getForumProfileId() {
		return forumProfileId;
	}

	public void setForumProfileId(long forumProfileId) {
		this.forumProfileId = forumProfileId;
	}

}
