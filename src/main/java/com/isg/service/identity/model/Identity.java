package com.isg.service.identity.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Identity implements Serializable {

	private static final long serialVersionUID = 4234234234231L;

	public static enum ENTITY_KEYS {
		CHAT_PROFILE_ID, PARENT_ID, STUDENT_ID, TEACHER_ID, VENDOR_ID, BRANCH_ID, INSTITUTE_ID, BANK_PARTNER
	}

	public static enum BRANCH_CONFIGURATION_FEATURE {
		IS_PAYMENT_ENABLED;
	}

	public static enum ROLES {
		PARENT("PARENT"), TEACHER("TEACHER"), STUDENT("STUDENT"), VENDOR("VENDOR"), ADMIN("ADMIN"), SCHOOL_ADMIN("SCHOOL ADMIN"), BANK_PARTNER("BANK_PARTNER");

		private final String role;

		private ROLES(String role) {
			this.role = role;
		}

		public static ENTITY_KEYS getEntityKey(String role) {
			if (role.equalsIgnoreCase("PARENT")) {
				return ENTITY_KEYS.PARENT_ID;
			} else if (role.contains("STUDENT")) {
				return ENTITY_KEYS.STUDENT_ID;
			} else if (role.contains("TEACHER")) {
				return ENTITY_KEYS.TEACHER_ID;
			} else if (role.contains("VENDOR")) {
				return ENTITY_KEYS.VENDOR_ID;
			} else if (role.contains("BANK_PARTNER")) {
				return ENTITY_KEYS.BANK_PARTNER;
			}else {
				throw new IllegalArgumentException("Entity Key not found for current role.");
			}
		}

		public String get() {
			return this.role;
		}
	}

	public static enum ROLES_ID {

		ADMIN(1), STUDENT(2), PARENT(3), TEACHER(4), VENDOR(5), SCHOOL_ADMIN(6), BANK_PARTNER(8);

		private final int roleId;

		private ROLES_ID(int roleId) {
			this.roleId = roleId;
		}

		public int get() {
			return this.roleId;
		}

		public static int getRoleID(String role) {
			if (role.equalsIgnoreCase("PARENT")) {
				return ROLES_ID.PARENT.get();
			} else if (role.contains("STUDENT")) {
				return ROLES_ID.STUDENT.get();
			} else if (role.contains("TEACHER")) {
				return ROLES_ID.TEACHER.get();
			} else if (role.contains("VENDOR")) {
				return ROLES_ID.VENDOR.get();
			}else if (role.contains("BANK_PARTNER")) {
				return ROLES_ID.BANK_PARTNER.get();
			} else {
				throw new IllegalArgumentException("Entity Key not found for current role.");
			}
		}
	}

	public Integer userId;

	public Map<ENTITY_KEYS, Integer> entityDetails = new HashMap<ENTITY_KEYS, Integer>();

	public Map<BRANCH_CONFIGURATION_FEATURE, String> branchConfigurationDetails = new HashMap<BRANCH_CONFIGURATION_FEATURE, String>();

	public Set<String> roleDetails = new HashSet<String>();

	public Integer defaultRoleId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Map<ENTITY_KEYS, Integer> getEntityDetails() {
		return entityDetails;
	}

	public void setEntityDetails(Map<ENTITY_KEYS, Integer> entityDetails) {
		this.entityDetails = entityDetails;
	}

	public Set<String> getRoleDetails() {
		return roleDetails;
	}

	public void setRoleDetails(Set<String> roleDetails) {
		this.roleDetails = roleDetails;
	}

	public boolean isRoleExists(String role) {
		return roleDetails.contains(role);
	}

	public Map<BRANCH_CONFIGURATION_FEATURE, String> getBranchConfigurationDetails() {
		return branchConfigurationDetails;
	}

	public void setBranchConfigurationDetails(Map<BRANCH_CONFIGURATION_FEATURE, String> branchConfigurationDetails) {
		this.branchConfigurationDetails = branchConfigurationDetails;
	}

	@JsonIgnore
	public String getDefaultRole() {

		if (defaultRoleId == ROLES_ID.PARENT.get()) {
			return "PARENT";
		} else if (defaultRoleId == ROLES_ID.STUDENT.get()) {
			return "STUDENT";
		} else if (defaultRoleId == ROLES_ID.TEACHER.get()) {
			return "TEACHER";
		} else if (defaultRoleId == ROLES_ID.VENDOR.get()) {
			return "VENDOR";
		} else if (defaultRoleId == ROLES_ID.ADMIN.get()) {
			return "ADMIN";
		} else if (defaultRoleId == ROLES_ID.SCHOOL_ADMIN.get()) {
			return "SCHOOL ADMIN";
		} else if (defaultRoleId == ROLES_ID.BANK_PARTNER.get()) {
			return "BANK_PARTNER";
		}else {
			throw new IllegalArgumentException("Default role not available");
		}
	}

	public Integer getDefaultRoleId() {
		return defaultRoleId;
	}

	public void setDefaultRoleId(Integer defaultRoleId) {
		this.defaultRoleId = defaultRoleId;
	}

	@JsonIgnore
	public Integer getEntityId(String entityKey) {

		if (entityKey.equalsIgnoreCase("PARENT")) {
			return entityDetails.get(ENTITY_KEYS.PARENT_ID);
		} else if (entityKey.contains("STUDENT")) {
			return entityDetails.get(ENTITY_KEYS.STUDENT_ID);
		} else if (entityKey.contains("TEACHER")) {
			return entityDetails.get(ENTITY_KEYS.TEACHER_ID);
		} else if (entityKey.contains("VENDOR")) {
			return entityDetails.get(ENTITY_KEYS.VENDOR_ID);
		} else if (entityKey.contains("CHAT_PROFILE")) {
			return entityDetails.get(ENTITY_KEYS.CHAT_PROFILE_ID);
		} else {
			throw new IllegalArgumentException("Entity Id not found.");
		}

	}

	@JsonIgnore
	public Integer getEntityId() {
		String defaultRole = getDefaultRole();
		return getEntityId(defaultRole);
	}

	@Override
	public String toString() {
		return "Identity [userId=" + userId + ", entityDetails=" + entityDetails + ", roleDetails=" + roleDetails + ", getDefaultRole()=" + getDefaultRole()
						+ "]";
	}

}
