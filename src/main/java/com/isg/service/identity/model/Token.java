package com.isg.service.identity.model;

import java.sql.Date;

public class Token {

	String token;

	Date created;

	Identity identity;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	@Override
	public String toString() {
		return "Token [token=" + token + ", created=" + created + ", identity=" + identity + "]";
	}

}
