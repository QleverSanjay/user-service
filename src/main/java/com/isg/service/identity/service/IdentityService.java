package com.isg.service.identity.service;

import java.sql.SQLException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.isg.service.identity.dao.IdentityDao;
import com.isg.service.identity.model.Token;
import com.isg.service.user.exception.AuthenticationException;

@RestController
public class IdentityService {
	@Autowired
	IdentityDao identityDAO;

	public void setIdentityDAO(IdentityDao identityDAO) {
		this.identityDAO = identityDAO;
	}

	@RequestMapping(value = "/identity/validate", method = RequestMethod.POST)
	public Token validateToken(@Valid @RequestHeader("token") String token) throws SQLException, AuthenticationException {
		return identityDAO.validateToken(token);
	}

}
