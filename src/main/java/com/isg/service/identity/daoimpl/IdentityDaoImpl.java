package com.isg.service.identity.daoimpl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.isg.service.identity.dao.IdentityDao;
import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Identity.BRANCH_CONFIGURATION_FEATURE;
import com.isg.service.identity.model.Identity.ENTITY_KEYS;
import com.isg.service.identity.model.Token;
import com.isg.service.user.exception.ApplicationException;
import com.isg.service.user.exception.AuthenticationException;
import com.jolbox.bonecp.BoneCPDataSource;

@Repository
public class IdentityDaoImpl extends NamedParameterJdbcDaoSupport implements IdentityDao {
	
	@Autowired
	private BoneCPDataSource dataSource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	/* Get actual class name to be printed on */
	static Logger log = Logger.getLogger(IdentityDaoImpl.class.getName());

	public static final String CHAT_PROFILE_ID_KEY = "CHAT_PROFILE_ID";

	public static final String PARENT_ID_KEY = "PARENT_ID";

	public static final String STUDENT_ID_KEY = "STUDENT_ID";

	public static final String TEACHER_ID_KEY = "TEACHER_ID";

	public static final String VENDOR_ID_KEY = "VENDOR_ID";

	//Expire token after 7 days.
	static final Long TOKEN_EXPIRES_IN_MILLISECONDS = new Long(7 * 24 * 60 * 60 * 1000);

	@Override
	public Token generateToken(int userId, int roleId) throws AuthenticationException, SQLException {
		String token = RandomStringUtils.random(30, true, true);
		//log.debug("Token string is" + token);

		SimpleJdbcInsert insertToken =
						new SimpleJdbcInsert(getDataSource())
										.withTableName("token")
										.usingColumns("token", "created_at", "expires_at", "created_by", "role_id", "active")
										.usingGeneratedKeyColumns("id");
		Map<String, Object> parameters = new HashMap<String, Object>(6);
		parameters.put("token", token);
		parameters.put("created_at", new Date(System.currentTimeMillis()));
		parameters.put("expires_at", new Date(System.currentTimeMillis() + TOKEN_EXPIRES_IN_MILLISECONDS));
		parameters.put("created_by", userId);
		parameters.put("role_id", roleId);
		parameters.put("active", "Y");
		insertToken.execute(parameters);
		return validateToken(token);
	}

	@Override
	public Token validateToken(String tokenStr) throws AuthenticationException, SQLException {
		// TODO Add created_date plus valid_for > now codition to query.
		String selectToken = "select * from token where token = ? and expires_at > now() and active='Y'";
		Token token = null;
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			conn = getJdbcTemplate().getDataSource().getConnection();
			stmt = conn.prepareStatement(selectToken);
			stmt.setString(1, tokenStr);
			log.debug("Query ::" + stmt.toString());
			rs = stmt.executeQuery();

			while (rs.next()) {
				token = new Token();
				token.setToken(rs.getString("token"));
				token.setCreated(rs.getDate("created_at"));
				Integer userId = rs.getInt("created_by");
				Integer defaultRoleId = rs.getInt("role_id");
				token.setIdentity(getIdentity(userId, defaultRoleId));
			}

		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

		if (token == null) {
			throw new AuthenticationException("Invalid token supplied...");
		}
		return token;

	}

	// @Override
	private Identity getIdentity(int userId, int defaultRoleId) throws SQLException, AuthenticationException {

		Identity userIdentity = new Identity();

		String selectUser = "select u.id as user_id, uc.chat_user_profile_id , r.id as role_id, r.name from " +
						" user u inner join user_role ur on ur.user_id = u.id" +
						" inner join role r on  r.id = ur.role_id " +
						" left join user_chat_profile uc on uc.user_id = u.id " +
						" where u.id = ? and u.active='Y' and u.isDelete = 'N'";

		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			conn = getJdbcTemplate().getDataSource().getConnection();
			stmt = conn.prepareStatement(selectUser);
			stmt.setInt(1, userId);
			log.debug("getIdentity sql query :: " + stmt.toString());
			rs = stmt.executeQuery();

			while (rs.next()) {
				userIdentity.setUserId(rs.getInt("user_id"));
				userIdentity.getRoleDetails().add(rs.getString("name"));
				// userIdentity.setDefaultRoleId(rs.getInt("role_id"));
				userIdentity.getEntityDetails().put(ENTITY_KEYS.CHAT_PROFILE_ID, rs.getInt("chat_user_profile_id"));
			}

			userIdentity.setDefaultRoleId(defaultRoleId);

		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

		if (userIdentity.getUserId() == null) {
			throw new AuthenticationException("User profile not found in database.");
		}

		// Load other details
		switch (userIdentity.getDefaultRole().toUpperCase()) {
		case "PARENT":
			loadParentDetails(userIdentity);
			userIdentity.getBranchConfigurationDetails().put(Identity.BRANCH_CONFIGURATION_FEATURE.IS_PAYMENT_ENABLED, "Y");
			break;
		case "STUDENT":
			loadStudentDetails(userIdentity);
			loadBranchConfigurationDetails(userIdentity);
			break;
		case "TEACHER":
			loadTeacherDetails(userIdentity);
			userIdentity.getBranchConfigurationDetails().put(Identity.BRANCH_CONFIGURATION_FEATURE.IS_PAYMENT_ENABLED, "Y");
			break;
		case "VENDOR":
			loadVendorDetails(userIdentity);
			userIdentity.getBranchConfigurationDetails().put(Identity.BRANCH_CONFIGURATION_FEATURE.IS_PAYMENT_ENABLED, "N");
			break;
		case "ADMIN":
			break;
		case "BANK_PARTNER":
			break;
		default:
			log.error("No matching entity profile, ignore setting other identity details");
			break;
		}

		return userIdentity;
	}

	private void loadBranchConfigurationDetails(Identity userIdentity) {
		Integer branchId = userIdentity.getEntityDetails().get(Identity.ENTITY_KEYS.BRANCH_ID);

		String branchConfigurationQuery = "select * from branch_configuration where branch_id = :branchId";
		Map<String, Object> queryParameters = new HashMap<String, Object>();
		queryParameters.put("branchId", branchId);

		Map<BRANCH_CONFIGURATION_FEATURE, String> branchConfigurationDetails = getNamedParameterJdbcTemplate().query(branchConfigurationQuery,
						queryParameters, new ResultSetExtractor<Map<BRANCH_CONFIGURATION_FEATURE, String>>() {
							@Override
							public Map<BRANCH_CONFIGURATION_FEATURE, String> extractData(ResultSet resultSet) throws SQLException,
											DataAccessException {
								Map<BRANCH_CONFIGURATION_FEATURE, String> map = new HashMap<BRANCH_CONFIGURATION_FEATURE, String>();

								while (resultSet.next()) {
									map.put(BRANCH_CONFIGURATION_FEATURE.IS_PAYMENT_ENABLED, resultSet.getString("is_payment_enabled_for_student"));
								}

								return map;
							}
						});

		userIdentity.setBranchConfigurationDetails(branchConfigurationDetails);

	}

	private void loadStudentDetails(Identity userIdentity) throws SQLException {
		String studentSelectQuery = "select s.id as entity_id, s.branch_id, i.id as institue_id from student s  " +
						" left join branch b on b.id = s.branch_id " +
						" left join institute i on i.id = b.institute_id where user_id=?";
		loadEntityDetails(studentSelectQuery, userIdentity, Identity.ENTITY_KEYS.STUDENT_ID, true);
	}

	private void loadVendorDetails(Identity userIdentity) throws SQLException {
		String vendorSelectQuery = "select id as entity_id from vendor where user_id=?";
		loadEntityDetails(vendorSelectQuery, userIdentity, Identity.ENTITY_KEYS.VENDOR_ID, false);
	}

	private void loadTeacherDetails(Identity userIdentity) throws SQLException {
		String teacherSelectQuery = "select t.id as entity_id, t.branch_id, i.id as institue_id from teacher t  " +
						" left join branch b on b.id = t.branch_id  left join institute i on i.id = b.institute_id where user_id=?";
		loadEntityDetails(teacherSelectQuery, userIdentity, Identity.ENTITY_KEYS.TEACHER_ID, true);
	}

	private void loadParentDetails(Identity userIdentity) throws SQLException {
		String parentSelectQuery = "select id as entity_id from parent where user_id=?";
		loadEntityDetails(parentSelectQuery, userIdentity, Identity.ENTITY_KEYS.PARENT_ID, false);
	}

	private void loadEntityDetails(String selectQuery, Identity userIdentity, ENTITY_KEYS entityKeyHolder, boolean readBranch) throws SQLException {
		PreparedStatement stmt = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			conn = getJdbcTemplate().getDataSource().getConnection();
			stmt = conn.prepareStatement(selectQuery);
			stmt.setInt(1, userIdentity.getUserId());
			log.debug("loadEntityDetails query :: " + stmt.toString());
			rs = stmt.executeQuery();

			while (rs.next()) {
				userIdentity.getEntityDetails().put(entityKeyHolder, rs.getInt("entity_id"));

				if (readBranch) {
					userIdentity.getEntityDetails().put(Identity.ENTITY_KEYS.BRANCH_ID, rs.getInt("branch_id"));
					userIdentity.getEntityDetails().put(Identity.ENTITY_KEYS.INSTITUTE_ID, rs.getInt("institue_id"));
				}
			}

		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

	@Override
	public Identity changeIdentity(int userId, int roleId, String accessTokenStr) throws SQLException, AuthenticationException {
		String query = "update token set role_id= :roleId where token = :token";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("roleId", roleId);
		parameters.put("token", accessTokenStr);

		log.debug("changeIdentity query :: " + query);
		log.debug("changeIdentity parameters :: " + parameters);
		int status = getNamedParameterJdbcTemplate().update(query, parameters);

		if (status == 1) {
			return getIdentity(userId, roleId);

		} else {
			throw new ApplicationException("Identity switch failed.");
		}
	}

	@Override
	public void invalidateToken(String accessTokenStr) {
		String query = "update token set active= 'N' where token = :token";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("token", accessTokenStr);

		log.debug("invalidateToken query :: " + query);
		log.debug("invalidateToken parameters :: " + parameters);
		int status = getNamedParameterJdbcTemplate().update(query, parameters);

		if (status == 1) {
			log.debug("Token is invlaidated");

		}

	}
}
