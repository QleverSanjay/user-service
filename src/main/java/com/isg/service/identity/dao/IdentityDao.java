package com.isg.service.identity.dao;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.isg.service.identity.model.Identity;
import com.isg.service.identity.model.Token;
import com.isg.service.user.exception.AuthenticationException;

@Repository
public interface IdentityDao {

	Token generateToken(int userId, int roleId) throws AuthenticationException, SQLException;

	Token validateToken(String token) throws AuthenticationException, SQLException;

	Identity changeIdentity(int userId, int roleId, String accessTokenStr) throws SQLException, AuthenticationException;

	void invalidateToken(String accessTokenStr);

}
